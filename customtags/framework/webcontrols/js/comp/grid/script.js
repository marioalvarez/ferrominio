/* * * * * *  EL PROGRAMADOR LO VA AHACER MANUAL * * * * * * * * * * * *
function grdAgregarDatos(grid, valores, eliminables){
	for(var i=0; i<valores.length; i++)
		grd_crearRenglon(grid, valores[i], eliminables);
}
 * * * * * * * * * * * *  * * * * * * * * * * * *  * * * * * * * * * * */

/* Funcion auxiliar de un redgrid, es llamada al hacer doble clic sobre una
 * celda editable del grid */
function grd_EditarCelda(tabla, renglon, columna){
	$x(tabla + "_lbl_" + renglon + "_" + columna).style.display = "none";
	$x(tabla + "_txt_" + renglon + "_" + columna).style.display = "block";
	$x(tabla + "_txt_" + renglon + "_" + columna).focus();
}
/* Funcion auxiliar de un redgrid, es llamada con la perdida del foco del campo de texto
 * de edición de una celda del grid */
function grd_GuardarCelda(tabla, renglon, columna){
	$x(tabla + "_lbl_" + renglon + "_" + columna).style.display = "block";
	$x(tabla + "_txt_" + renglon + "_" + columna).style.display = "none";
	$x(tabla + "_txt_" + renglon + "_" + columna).value = $x(tabla + "_txt_" + renglon + "_" + columna).value.toUpperCase();
	$x(tabla + "_lbl_" + renglon + "_" + columna).innerHTML = $x(tabla + "_txt_" + renglon + "_" + columna).value;
	grd_GuardarValor(tabla, renglon, columna);
}
/* Funcion auxiliar de un redgrid, es llamada con la perdida del foco del campo de texto
 * de edición de una celda del grid, asi como con la perdida de foco de un campo de texto de
 * una columna tipo text */
function grd_GuardarValor(tabla, renglon, columna){	$x(tabla + "_hdn_" + renglon + "_" + columna).value = $x(tabla + "_txt_" + renglon + "_" + columna).value; }
/* Anular el enter (intro) */
function grd_AnularEnter(e){ 
	var key; 
	if(window.event) { key = window.event.keyCode; } /*IE*/ else { key = e.which; } /* Firefox*/
	if(key == 13) { return false; } else { return true; }
}
/* Funcion auxiliar del paginado, es mandada llamar por los botones, 
 * siguiente y anterior */
function grd_Paginado(tabla, tipo, inicio, renglones, ultima, primera){
	var i = 1; var f = 1; inicio = inicio * 1; renglones = renglones * 1;
	if(tipo == "Siguiente") i = (inicio + renglones);
	if(tipo == "Anterior") {
		i = (inicio - renglones);
		$x(tabla + "_hdn_paginaActual").value = $x(tabla + "_hdn_paginaActual").value - 2;
	}
	if(ultima != null) 
		$x(tabla + "_hdn_paginaActual").value = ultima - 1;
	if(primera != null){
		$x(tabla + "_hdn_inicio").value       = 1;
		$x(tabla + "_hdn_fin").value          = renglones;
		$x(tabla + "_hdn_paginado").value     = "true";
		$x(tabla + "_hdn_paginaActual").value = 0;
	}
	else{
		f = i + renglones - 1;
		$x(tabla + "_hdn_inicio").value   = i;
		$x(tabla + "_hdn_fin").value      = f;
		$x(tabla + "_hdn_paginado").value = "true";
	}
	return true;
}
/* Funcion de agregado manual al grid */
function grd_crearRenglon(tabla, valores, eliminable){
	/* Incrementar el numero de renglones */
	var grdVisibles          = eval(tabla + "_Visibles");
	var grdRenglonesActuales = eval(tabla + "_RenglonesActuales");
	var grdRenglonesActivos  = eval(tabla + "_RenglonesActivos");
	var grdColumnas          = eval(tabla + "_Columnas");
	var grdImgDelete         = eval(tabla + "_ImgDelete");
	var grdAligns            = eval(tabla + "_Aligns");
	var grdTipos             = eval(tabla + "_Tipos");
	/* Aumentar las variables de renglones */
	grdRenglonesActuales = grdRenglonesActuales + 1;
	grdRenglonesActivos  = grdRenglonesActivos + 1;
	eval(tabla + "_RenglonesActuales = " + tabla + "_RenglonesActuales + 1");
	eval(tabla + "_RenglonesActivos = " + tabla + "_RenglonesActivos + 1");
	$x(tabla + "_hdn_numRenglones").value = grdRenglonesActuales;
	/* Crear el renglon */
	var myTable = $x("table_" + tabla);
	var oRow = myTable.insertRow(-1);
	var rowName = tabla + "_tr_" + grdRenglonesActuales;
	oRow.setAttribute("id"  ,  rowName);
	oRow.setAttribute("name",  rowName);
	//Generar las celdas de la fila
	for(var i=0; i<valores.length; i++){
		var oCell = oRow.insertCell(-1);
		
		var hdnId =  tabla + "_hdn_" + grdRenglonesActuales + "_" + (i + 1);
		var tdId  = tabla + "_td_" + grdRenglonesActuales + "_" + (i + 1);
		if(grdTipos[i]!= null && grdTipos[i]== "label"){
			oCell.innerHTML = valores[i] + "<input type='hidden' name='" + hdnId + "' id='" + hdnId + "' value='" + valores[i] + "' />";
		}
		if(grdTipos[i]!= null && grdTipos[i]== "text"){
			oCell.innerHTML = "<input type='text' name='" + hdnId + "' id='" + hdnId + "' value='" + valores[i] + "' style='width:90%; text-align:"+grdAligns[i]+";' />";
		}
		if(grdTipos[i]!= null && grdTipos[i]== "check"){
			oCell.innerHTML = "<input type='checkbox' name='" + hdnId + "' id='" + hdnId + "' " + ((valores[i])? "checked='checked'":"") + " 'style='width:100%; text-align:"+grdAligns[i]+";' />";
		}
		oCell.setAttribute("id"   , tdId);
		oCell.setAttribute("name" , tdId);
		if(grdVisibles[i] == true) oCell.setAttribute("style", "text-align:" + grdAligns[i] + "; padding:10px;");
		else oCell.setAttribute("style", "text-align:" + grdAligns[i] + "; display:none;");
		if(i==0){
			//Campo oculto de estatus
			var status = document.createElement("input");
			var statusId = tabla + "_hdn_status_R" + grdRenglonesActuales;
			status.setAttribute("type" , "hidden");
			status.setAttribute("name" , statusId);
			status.setAttribute("id"   , statusId);
			status.setAttribute("value", "true");
			oCell.appendChild(status);
		}
	}
	if(eliminable == null || eliminable == true){
		//Agregar las Acciones
		var oCell = oRow.insertCell(-1);
		oCell.setAttribute("style","text-align:center;");
		//Boton Eliminar
		var del = document.createElement("img");
		del.setAttribute("type"   , "image");
		del.setAttribute("src"    , grdImgDelete);
		del.setAttribute("alt"    , "Eliminar");
		del.setAttribute("width"  , "20px");
		del.setAttribute("height" , "20px");
		del.setAttribute("style"  , "cursor:pointer; width:20px; heigth:20px;");
		eval("del.onclick = function(){ grdEliminar('" + tabla + "', " + grdRenglonesActuales + " ); } ");
		oCell.appendChild(del);
	}
	grdGuardarEstado(tabla);
}
/* Elimina un renglon del grid */
function grdEliminar(tabla, renglon){
	$x(tabla + "_hdn_status_R" + renglon).value = "false";
	eval(tabla + "_RenglonesActivos = " + tabla + "_RenglonesActivos - 1");
	grdOcultarRenglonGrid(tabla, renglon);
	grdGuardarEstado(tabla);
	var onDelete = eval(tabla + "_OnDeleteRow");
	if(onDelete != "") {
		var dataRow = grdObtenerRenglon(tabla, renglon);
		var funcion = onDelete + "('"+dataRow.toString()+"', "+renglon+");";
		eval(funcion);
	}
}
/* redirecciona a una pagina en especifico, es mandado llamar 
   por los actioncolumn */
function grdIrA(pagina, confirmText){
	if(confirmText == "") location = pagina;
	else {
		if(confirm(confirmText)) location = pagina;
	}
}
/* Obtiene el valor de una celda especifica de un grid, siempre y 
   cuando no se template */
function grdObtenerCelda(grid, renglon, columna){
	return $('#' + grid + '_hdn_' + renglon + '_' + columna).val();
	/*
	var comp = $x(grid + "_td_" + renglon + "_" + columna);
	
	//var hdnId =  tabla + "_hdn_" + grdRenglonesActuales + "_" + (i + 1);
	
	for(var i=0; i<comp.children.length; i++){
		var temp = comp.children[i];
		if (temp.tagName == "DIV")
		{
			for(var j=0; j<temp.children.length; j++)
			{
				temp2= temp.children[j];
				// HGB 2011-08/08 Modificación, porque la funcion no encontraba el valor real de la celda sino un Normal
				if((temp2.getAttribute("type") == "hidden" || temp2.getAttribute("type") == "HIDDEN") && temp2.getAttribute("id").indexOf("status", 1) < 1 ) 
					return temp2.value;
				if(temp2.getAttribute("type") == "checkbox" || temp2.getAttribute("type") == "CHECKBOX") 
					return temp2.checked;
				if(temp2.getAttribute("type") == "text" || temp2.getAttribute("type") == "TEXT") 
					return temp2.value;
			}
		}
		else if(temp.tagName == "INPUT") 
		{	
			if((temp.getAttribute("type") == "hidden" || temp.getAttribute("type") == "HIDDEN") && temp.getAttribute("id").indexOf("status", 1) < 1 )
				return temp.value;
			if(temp.getAttribute("type") == "checkbox" || temp.getAttribute("type") == "CHECKBOX") 
				return temp.checked;
			if(temp.getAttribute("type") == "text" || temp.getAttribute("type") == "TEXT") 
				return temp.value;
		}
	}
	*/
}
/* Regresa una arrglo con todos los datos de un renglon del grid */
function grdObtenerRenglon(grid, renglon){
	var columnas = eval(grid + "_Columnas");
	var resultado = new Array(columnas);
	for(var i=0; i<columnas; i++){
		resultado[i] = grdObtenerCelda(grid, renglon*1, (i+1));
	}
	return resultado;
	/*
	var comp = $x(grid + "_tr_" + renglon);
	for(var j=0; j<comp.children.length; j++){
		var temp = comp.children[j];
		if(temp.tagName!=null && temp.tagName == "TD"){
			for(var i=0; i<temp.children.length; i++){
				var temp2 = temp.children[i];
				if(temp2.tagName!=null && temp2.tagName == "DIV"){
					for(var k=0; k<temp2.children.length; k++){
						var temp3 = temp2.children[k];
						if(temp3.getAttribute("type") == "hidden" || temp3.getAttribute("type") == "HIDDEN") resultado[j] = temp3.value;
						else if(temp3.getAttribute("type") == "checkbox" || temp3.getAttribute("type") == "CHECKBOX") resultado[j] = temp3.checked;
						
					}
				}
				else{
					if(temp2.tagName == "INPUT") 
					{	
						if(temp2.getAttribute("type") == "hidden" || temp2.getAttribute("type") == "HIDDEN") resultado[j] = temp2.value;
						else if(temp2.getAttribute("type") == "checkbox" || temp2.getAttribute("type") == "CHECKBOX") resultado[j] = temp2.checked;
					}
				}
			}
		}
	}
	
	return resultado;
	*/
}
/* Regresa en numero de renglones activos en un grid */
function grdObtenerNumeroRenglones(grid){ return eval(grid + "_RenglonesActivos"); }
/* Regresa el numero de renglones reales de un grid */
function grdObtenerNumeroRenglonesReales(grid){ return eval(grid + "_RenglonesActuales"); }
/* Regresa un arreglo bidimencional con todos los valores del grid */
function grdObtenerGrid(grid){
	var renglones = grdObtenerNumeroRenglones(grid);
	var arreglo = new Array(renglones);
	if(renglones >= 1){ for(var i=1; i<=renglones; i++) arreglo[i-1] = grdObtenerRenglon(grid, i); }
	return arreglo;
}
/* Ejecuta una funcion de un action column, mandandole como parametro
   un arreglo con todos los valores del renglon*/
function grdEjecutarFuncion(tabla, metodo, renglon){
	var array = grdObtenerRenglon(tabla, renglon);
	var newArray = "";
	for(var i=0; i<array.length; i++) newArray = newArray + "'"+array[i]+"',";
	newArray = newArray.substr(0, newArray.length-1);
	eval(metodo + "(new Array(" + newArray + "))");
}
/* Se ejecuta en el onload de la pagina para que el grid sea 
   persistente ante diversos eventos */
function grdComprobarPersistencia(grd){
	if($x(grd + "_hdn_persistencia").value == "")  grdGuardarEstado(grd);
	else {
		//Restablecer variables de Javascript
		var row = $x(grd + "_hdn_persistencia_Row").value;
		var act = $x(grd + "_hdn_persistencia_Act").value;
		var col = $x(grd + "_hdn_persistencia_Col").value;
		eval(grd + "_RenglonesActuales = " + row );
		eval(grd + "_RenglonesActivos = " + act );
		eval(grd + "_Columnas = " + col );
		
		//Regenerar el Grid
		$x("div_" + grd).innerHTML = $x(grd + "_hdn_persistencia").value;
	}
}
/* Guarda el estado actual de un grid, para poder llevar a cabo la 
   persistencia del mismo */
function grdGuardarEstado(grd){ 
	$x(grd + "_hdn_persistencia").value = $x("div_" + grd).innerHTML; 
	$x(grd + "_hdn_persistencia_Row").value = eval(grd + "_RenglonesActuales");
	$x(grd + "_hdn_persistencia_Act").value = eval(grd + "_RenglonesActivos");
	$x(grd + "_hdn_persistencia_Col").value = eval(grd + "_Columnas");
}
/* Se encarga de ocultar todos los renglones de un grid */
function grdOcultarGridCompleto(grd){
	var renglones = grdObtenerNumeroRenglones(grd);
	for(var i=1; i<=renglones; i++)	grdOcultarRenglonGrid(grd, i);
}
/* Se encarga de OCULTAR un renglon de un grid */
function grdOcultarRenglonGrid(grd, renglon){ if($x(grd + "_tr_" + renglon) != null) $x(grd + "_tr_" + renglon).style.display  = "none"; }
/* Se encarga de MOSTRAR un renglon de un grid */
function grdMostrarRenglonGrid(grd, renglon){ if($x(grd + "_tr_" + renglon) != null) $x(grd + "_tr_" + renglon).style.display  = "table-row"; }
/* Regresa true o false, si el renglon es visible */
function grdElRenglonEsVisible(grd, renglon){
	if($x(grd + "_tr_" + renglon) != null) 
		if( $x(grd + "_tr_" + renglon).style.display  != "none") return true;
		else return false;
	else return false;
}
/* Filtra una columna del grid por el valor recibido */
function grdFiltrarColumna(grd, columna, valor, tipoDeBusqueda){
	var grid = grdObtenerGrid(grd);
	if(tipoDeBusqueda == null || tipoDeBusqueda == "equal"){
		/* Filtrar */
		for(var i=0; i<grid.length; i++){
			if(grid[i][columna] == valor) grdMostrarRenglonGrid(grd, i+1);
			else grdOcultarRenglonGrid(grd, i+1);
		}
	}
	else{
		for(var i=0; i<grid.length; i++){
			if(grid[i][columna].toUpperCase().search(valor.toUpperCase()) >= 0) grdMostrarRenglonGrid(grd, i+1);
			else grdOcultarRenglonGrid(grd, i+1);
		}
	}
}
/* Filtra una columna del grid por el valor recibido sin perder los filtros anteriores */
function grdFiltrarColumnaAnidada(grd, columna, valor, tipoDeBusqueda){
	var grid = grdObtenerGrid(grd);
	if(tipoDeBusqueda == null || tipoDeBusqueda == "equal"){
		/* Filtrar */
		for(var i=0; i<grid.length; i++){
			if(grid[i][columna] == valor) continue;
			else grdOcultarRenglonGrid(grd, i+1);
		}
	}
	else{
		for(var i=0; i<grid.length; i++){
			if(grid[i][columna].toUpperCase().search(valor.toUpperCase()) >= 0) continue;
			else grdOcultarRenglonGrid(grd, i+1);
		}
	}
}
/* Obtener el id del componente principal de una columna */
function grdObtenerIdCelda(grid, renglon, columna){ return grid + "_hdn_" + renglon + "_" + columna; }
/* Comprueba si existe o no un valor determinado en alguna columna del 
   grid, el parametro columna es opcional */
function grdExisteValor(grid, valor, tipoDeBusqueda, columna){
	var datos = grdObtenerGrid(grid);
	for(var i=0; i<datos.length; i++){
		if(columna!=null){
			if(tipoDeBusqueda == null || tipoDeBusqueda == "equal"){
				if(valor == datos[i][columna]) {
					if(grdElRenglonEsVisible(grid, i)) return true;
					else continue;
				}
				else continue;
			}
			else {
				if(datos[i][columna].toUpperCase().search(valor.toUpperCase()) >= 0) {
					if(grdElRenglonEsVisible(grid, i)) return true;
					else continue;
				}
				else continue;
			}
		}
		else{
			for(var j=0; j<datos[i].length; j++){
				if(tipoDeBusqueda == null || tipoDeBusqueda == "equal"){
					if(valor == datos[i][j]) {
						if(grdElRenglonEsVisible(grid, i)) return true;
						else continue;
					}
					else continue;
				}
				else {
					if(datos[i][j].toUpperCase().search(valor.toUpperCase()) >= 0) {
						if(grdElRenglonEsVisible(grid, i)) return true;
						else continue;
					}
					else continue;
				}
			}
		}
	}
	return false;
}
/* Sirve para comprobar si el grid tiene informacion (en el caso de un back 
   y agregado dinamico con javascript) */
function grdExisteInformacion(grd){
	var myTable = $x("table_" + grd);
	return myTable.tBodies[0].rows.length > 0;
}






function fnc_gridValidate(grid, column, format, name, requiered)
{
	var rows     = eval(grid + "_RenglonesActuales");
	var allRight = true;
	var msjReq   = "La información acerca de {0} es requerida"; 
	var msjFor   = "El formato de {0} no es correcto"; 
	
	for(var i=1; i<=rows; i++){ //Recorrer Filas
		if(grdElRenglonEsVisible(grid, i)){ //Comprobar que no este eliminado el renglon o que sea visible
			var $comp = $('#' + grid + '_hdn_' + i + '_' + column);
			var value = $comp.val().trim();
			var id    = $comp.attr('id');
			switch(format){
				case 'text': 
					if(requiered == true && value == ""){
						msjReq = msjReq.replace("{0}", name);
						inlineMsg(id, msjReq, 2, false); 
						allRight = false;
					}
					break;

				case 'integer':
					if($comp.val() != ''){
						if(isNaN(value)) { 
							msjFor = msjFor.replace("{0}", name);
							inlineMsg(id, msjFor, 2, false); 
							allRight = false;
						}
						else{
							if(value%1!=0) { 
								msjFor = msjFor.replace("{0}", name);
								inlineMsg(id, msjFor, 2, false); 
								allRight = false;
							}
							else $comp.val($comp.val()*1);
						}
					}
					else{
						if(requiered == true){
							msjReq = msjReq.replace("{0}", name);
							inlineMsg(id, msjReq, 2, false); 
							allRight = false;
						}
					}
					break;
					
				case 'float':
					if(isNaN(value)) { 
						msjFor = msjFor.replace("{0}", name);
						inlineMsg(id, msjFor, 2, false); 
						allRight = false;
					}
					else{
						if(requiered == true && value == ""){
							msjReq = msjReq.replace("{0}", name);
							inlineMsg(id, msjReq, 2, false); 
							allRight = false;
						}
					}
					break;
					
				case 'money':
					var val = value.replace('$', ''); //Eliminar el caracter de moneda
					while(val.indexOf(',') > 0) val = val.replace(',', ''); //Eliminar el separador de miles
					if(isNaN(val)) { 
						msjFor = msjFor.replace("{0}", name);
						inlineMsg(id, msjFor, 2, false); 
						allRight = false;
					}
					else{
						if(requiered == true && value == ""){
							msjReq = msjReq.replace("{0}", name);
							inlineMsg(id, msjReq, 2, false); 
							allRight = false;
						}
					}
					//else comp.value = fnc_gridExcelMoneyFormat(val, decimals, decimalSep, thousandSep, moneyChar);
					break;
					
				case 'dateDMY':
					var ar = value.split("/");
					if(ar.length > 2){
						var dia  = ar[0];
						var mes  = ar[1];
						var anio = ar[2];
						if(!fnc_gridExcelCheckDate(dia, mes, anio)) { 
							msjFor = msjFor.replace("{0}", name);
							inlineMsg(id, msjFor, 2, false); 
							allRight = false;
						}
					}
					else { 
						if(requiered == true && value == ""){
							msjReq = msjReq.replace("{0}", name);
							inlineMsg(id, msjReq, 2, false); 
							allRight = false;
						}
						else{
							msjFor = msjFor.replace("{0}", name);
							inlineMsg(id, msjFor, 2, false); 
							allRight = false;
						}
					}
					break;
					
				case 'dateMDY':
					var ar = comp.value.split("/");
					if(ar.length > 2){
						var mes = ar[0];
						var dia = ar[1];
						var anio = ar[2];
						if(!fnc_gridExcelCheckDate(dia, mes, anio)) { 
							msjFor = msjFor.replace("{0}", name);
							inlineMsg(id, msjFor, 2, false); 
							allRight = false;
						}
					}
					else { 
						if(requiered == true && value == ""){
							msjReq = msjReq.replace("{0}", name);
							inlineMsg(id, msjReq, 2, false); 
							allRight = false;
						}
						else{
							msjFor = msjFor.replace("{0}", name);
							inlineMsg(id, msjFor, 2, false); 
							allRight = false;
						}
					}
					break;
			}
		}
		
		if(!allRight){
			return false;
		}
	}
	return true;
}

function fnc_gridExcelCheckDate(dia, mes, anio)
{
	if(isNaN(dia) || isNaN(mes) || isNaN(anio)) return false;
	else{
		if( (anio>1900 && anio<5000) && (mes>0&&mes<13) ){
			switch(mes){
				case 1: case 3: case 5: case 7: case 8: case 10: case 12:
					if(dia<32 && dia>0) break; //entre 1 y 31
					else return false;
				case 4: case 6: case 9: case 11:
					if(dia<31 && dia>0) break; //entre 1 y 30
					else return false;
				case 2:
					if(fnc_gridExcelIsleap(anio)){
						if(dia<30 && dia>0) break; // de 1 a 29
						else return false;
					}
					else{
						if(dia<29 && dia>0) break; // de 1 a 30
						else return false;
					}
			}
		}
		else return false;
	}
	return true;
}

function fnc_gridExcelIsleap(anio)
{
	var leap; 
	if(parseInt(anio)%4==0){ 
		if(parseInt(anio)%100==0){ 
			if(parseInt(anio)%400==0) leap=true; 
			else leap=false; 
		} 
		else leap=true; 
	} 
	else leap=false; 
	return leap; 
}

function grd_RealizarPaginado(grid, paginaActual, Accion){
	try{
		$('#' + grid + '_Paginado').val('true');
		$('#' + grid + '_PaginadoAccion').val(''+Accion);
		$('#' + grid + '_PaginaActual').val(''+paginaActual);
		return true;
	}
	catch(e){ return false; }
}




/* * * * * * * * * * * F U N C I O N E S    D E P R E C A D A S * * * * * * * * * * * * * * * * */
/* Anular el enter (intro) */
function grd_AnularEnter(e){ 
	var key; 
	if(window.event) { key = window.event.keyCode; } /*IE*/ else { key = e.which; } /* Firefox*/
	if(key == 13) { return false; } else { return true; }
}

/* Funcion auxiliar del paginado, es mandada llamar por los botones, 
 * siguiente y anterior */
function grd_Paginado(tabla, tipo, inicio, renglones, ultima, primera){
	var i = 1; var f = 1; inicio = inicio * 1; renglones = renglones * 1;
	if(tipo == "Siguiente") i = (inicio + renglones);
	if(tipo == "Anterior") {
		i = (inicio - renglones);
		$x(tabla + "_hdn_paginaActual").value = $x(tabla + "_hdn_paginaActual").value - 2;
	}
	if(ultima != null) 
		$x(tabla + "_hdn_paginaActual").value = ultima - 1;
	if(primera != null){
		$x(tabla + "_hdn_inicio").value       = 1;
		$x(tabla + "_hdn_fin").value          = renglones;
		$x(tabla + "_hdn_paginado").value     = "true";
		$x(tabla + "_hdn_paginaActual").value = 0;
	}
	else{
		f = i + renglones - 1;
		$x(tabla + "_hdn_inicio").value   = i;
		$x(tabla + "_hdn_fin").value      = f;
		$x(tabla + "_hdn_paginado").value = "true";
	}
	return true;
}