function makeScrollable(wrapper,scrollable)
{
	
	var wrapper = $(wrapper),
		scrollable=$(scrollable).hide();

	var loading = $('<div class="loading">Cargando...</div>').appendTo(wrapper); //el gif de carga
	
	/*cada cien milisegundos se ejecutara esta funcion*/
	var interval = setInterval(function()
	{
		//var images = scrollable.find('img'); /*se encuentran todas las imagenes*/
//		var completed = 0;
//		images.each(function()
//		{
//			if(this.complete)
//				completed++
//		});
			
		/*la carga de las imagenes*/
		/*if(completed==images.length)
		{
			*/	//clearInterval(interval);
				/*setTimeout(function()
				{*/	
					loading.hide();
					wrapper.css({overflow:'hidden'});
					scrollable.slideDown('slow',function()
					{
						enable();
					});
				/*},500)*/
		/*}*/
	},
	100); //fin interval
		
		
		function enable()
		{
			var inactiveMargin=99;
			var wrapperWidth=wrapper.width();
			var wrapperHeight=wrapper.height();
			var scrollableHeight=scrollable.outerHeight()+2*inactiveMargin;
			var tooltip=$('<div class="sc_menu_tooltip"></div>').css('opacity',0).appendTo(wrapper);
				
				scrollable.find('a').each(function()
				{
					$(this).data('tooltipText',this.title)
				});
				
			scrollable.find('a').removeAttr('title');
			scrollable.find('img').removeAttr('alt');
			var lastTarget;
			wrapper.mousemove(function(e){lastTarget=e.target;var wrapperOffset=wrapper.offset();
			var tooltipLeft=e.pageX-wrapperOffset.left;
			tooltipLeft=Math.min(tooltipLeft,wrapperWidth-75);
			var tooltipTop=e.pageY-wrapperOffset.top+wrapper.scrollTop()-40;
			if(e.pageY-wrapperOffset.top<wrapperHeight/2)
			{
				tooltipTop+=80
			}
			tooltip.css({top:tooltipTop,left:tooltipLeft});
			var top = (e.pageY-wrapperOffset.top)*(scrollableHeight-wrapperHeight)/(wrapperHeight-inactiveMargin);
			
				if(top<0)
				{
					top=0
				}
				
				wrapper.scrollTop(top)
			});
			
			var interval = setInterval(function()
			{
				if(!lastTarget)return;
				var currentText = tooltip.text();
				if(lastTarget.nodeName=='IMG')
				{
					var newText=$(lastTarget).parent().data('tooltipText');
					if(currentText!=newText)
					{
						tooltip.stop(true).css('opacity',0).text(newText).animate({opacity:1},1000)
					}
				}
			},200);
			
			wrapper.mouseleave(function()
			{
				lastTarget = false;
				tooltip.stop(true).css('opacity',0).text('');
			})
	}//fin enabled
}//fin makeScrollable


$(function()
{
	makeScrollable("div.sc_menu_wrapper","div.sc_menu")
});
