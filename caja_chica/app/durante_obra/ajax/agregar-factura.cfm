<!--- Armando Martinez [jmartinez@redrabbit.mx] --->
<!--- v1.0.0 [2015-04-22] --->

<!--- MATRIZ DE RASTREABILIDAD
    COMPONENTES:
        - cheques
        - libro_bancos
        - proveedores
        PROCEDIMIENTOS:
            -- proc_cat_ado_tipos_cheque_leer [leer_tipos]
            -- proc_ctl_ado_obras_cuentas_leer [cuentas]
            -- proc_suma_activos_pasivos [sum_activos_pasivos]
            -- proc_proveedores_con_oc [por_orden_compra]

    ARCHIVOS:
        - submit/agregar-cheque-credito.cfm
--->

<cfimport prefix="ct" taglib="../../customtags">
<cfajaxproxy cfc="#Application.adm_CfcPath#.funciones" jsclassname="jsFunciones">


<!--- LEER CUENTAS DE OBRA --->
<!--- <cfset arg = structnew()>
<cfset arg.id_empresa = #session.id_empresa#>
<cfset arg.id_obra = #session.id_obra#>
<cfset arg.id_cuenta = 1 >
<cftry>
    <cfinvoke   
        component="#Application.adm_CfcPath#.libro_bancos"
        method="cuentas"
        argumentcollection="#arg#"
        returnvariable="rsInvoke">
    
    <cfcatch><ct:error message="#cfcatch.message#"></cfcatch>
</cftry> 
<cfif #rsInvoke.tipoError# NEQ ""><!--- MANEJADOR DE ERROR --->
    <ct:error message="#rsInvoke.mensaje#" detail="#rsInvoke.detalleError#">
</cfif>
<cfif #rsInvoke.rs.recordCount# eq 0>
    <ct:error message="No puede ingresar a este apartado debido a que aun no tiene asignado una cuenta contable"/>
</cfif>


<cfset cuenta_contable = rsInvoke.rs> --->

<!--- DINERO DISPONIBLE EN LA CUENTA  --->
<!--- <cfset ap = structnew()>
<cfset ap.id_cuenta = #cuenta_contable.id_cuenta#>
<cfset ap.id_empresa = #session.id_empresa#>
<cfset ap.id_obra = #session.id_obra#>
<cftry>
    <cfinvoke   
        component="#Application.adm_CfcPath#.libro_bancos"
        method="suma_activos_pasivos"
        argumentcollection="#ap#"
        returnvariable="rsInvoke">
        
    <cfcatch><ct:error message="#cfcatch.message#"></cfcatch>
</cftry> 
<cfif #rsInvoke.tipoError# NEQ ""><!--- MANEJADOR DE ERRORES --->
    <ct:error message="#rsInvoke.mensaje#" detail="#rsInvoke.detalleError#">
</cfif>

<cfset sum_act_pas = rsInvoke.rs> --->

<!--- OBTENER TODAS LAS EMPRESAS --->

<cftry>
    <cfinvoke 
        component = "#Application.adm_CfcPath#.empresa"
        method = "leer"
        returnvariable = "rsInvoke"/>
        
    <cfcatch><ct:error message="#cfcatch.message#"></cfcatch>
</cftry>   
<cfif #rsInvoke.tipoerror# neq ""><!--- MANEJADOR DE ERROES --->
    <ct:error message="#rsInvoke.mensaje#" detail="#rsInvoke.DETALLEERROR#"/>
</cfif>
<cfquery name="RsEmpresas" dbtype="query">
    SELECT id_Empresa, nb_empresa FROM rsInvoke.rs
</cfquery>
<cfset empresas = RsEmpresas>
<cftry>
    <cfinvoke 
        component = "#Application.componentes#.iva"
        method = "get_listado_IVA"
        returnvariable = "rsIVA"
        id_IVA=""
        sn_Activo="1"/>
        
    <cfcatch><ct:error message="#cfcatch.message#"></cfcatch>
</cftry>
   
<cfif #rsIVA.SUCCESS# EQ false><!--- MANEJADOR DE ERROES --->
    <ct:error message="#rsIVA.MESSAGE#" detail="#rsIVA.TipoError#"/>
</cfif>
<cfset iva = rsIVA.rs>
<!--- OBTENER TODOS LOS PROVEEDORES QUE TIENEN ORDENES DE COMPRA EN LA OBRA --->
<!--- <cfset arg = structNew()>
<cfset arg.id_empresa = #session.id_empresa#>
<cfset arg.id_obra = #session.id_obra#>
<cftry>
    <cfinvoke 
        component = "#Application.adm_CfcPath#.proveedores"
        method = "proveedores_con_OC_PLAZA"
        argumentcollection="#arg#"
        returnvariable = "rsInvoke"/>
        
    <cfcatch><ct:error message="#cfcatch.message#"></cfcatch>
</cftry>   
<cfif #rsInvoke.tipoerror# neq ""><!--- MANEJADOR DE ERROES --->
    <ct:error message="#rsInvoke.mensaje#" detail="#rsInvoke.DETALLEERROR#"/>
</cfif>
<cfset proveedores = rsInvoke.rs>
 --->

<style type="text/css">
    #agregarFactura input[type=text],
    #agregarFactura select{
        width: 250px;
    }
</style>
<script type="text/javascript">
    var chequeController = function( $scope, $timeout ){
        var s = $scope;

       
        
        
        s.empresas = f.rsJson(<cfoutput>#serializeJson(empresas)#</cfoutput>);
        
        // for (var i = 0; i < s.proveedores.length; i++) {
        //     s.proveedores[i].id_obra = <cfoutput>#session.id_obra#</cfoutput>;
        //     s.proveedores[i].id_proveedor = parseInt(s.proveedores[i].id_proveedor);
        // };
        s.movimientosCheque = [];
        s.facturas = [];
        s.factura = {};
        s.shForm = function(){
            if( $(formCheque).is(':visible') )
                $(formCheque).slideUp();
            else
                $(formCheque).slideDown();
        };
        
        s.convertir_numero_cadena = function(){
            if (s.importe != "")
            {
                var numero = s.importe;
                var clsFunciones = new jsFunciones();
                s.des_importe = clsFunciones.converirNumeroALetras_remote(numero);
            }
        };
        
        s.seleccionarTipo = function(){
            s.cajaChica = s.tipo == 3 ? true : false;
            if( s.cajaChica == true && !s.proveedorFactura ){
                s.factura.rfcProveedor = '';
                //s.ordenesCompra = [{id_ordencompra:0, nombre:'SELECCIONE PROVEEDOR...'}];
                //s.ordenCompra = s.ordenesCompra[0];
                s.movimientos = [{id_inventariomovimiento:0, nombre:'SELECCIONE PROVEEDOR...'}];
                s.movimiento = s.movimientos[0];
            } else if(s.cajaChica == false && s.proveedor){
                s.cargarOC(s.proveedor);
            } else if( s.proveedorFactura ){
                s.cargarOC(s.proveedorFactura);
            }
            if( s.cajaChica == true )
                s.idProveedorCheque = 0;
            else
                s.idProveedorCheque = s.proveedor.id_proveedor;
        };
        
        s.formFacturas = function(){
            if( $('#agregarFactura').is(':visible') ){
                s.factura = {};
                $('#agregarFactura').slideUp();
            }else{
                if( !s.tipo || s.tipo  == '' ){
                    f.errorMessage( 'Debe seleccionar el tipo de cheque.', $('#idu_tipos_cheques') );
                    return false;
                }

                if( s.tipo != 3 ){
                    if( !s.proveedor || !s.proveedor.id_proveedor ){
                        f.errorMessage( 'Debe seleccionar el proveedor para el agregar facturas.', $('#id_proveedor_chosen') );
                        return false;
                    }
                    s.cargarOC(s.proveedor);
                } else if( !s.proveedoresCajaChica || s.proveedoresCajaChica.length < 1 ){
                    f.errorMessage( 'Debe seleccionar un proveedor para el agregar facturas.', $('#proveedores_factura_chosen') );
                    return false;
                }
                Date.prototype.yyyymmdd = function() {
                    var yyyy = this.getFullYear().toString();
                    var mm = (this.getMonth()+1).toString(); // getMonth() is zero-based
                    var dd  = this.getDate().toString();
                    return yyyy + '/' + (mm[1]?mm:"0"+mm[0]) + '/' + (dd[1]?dd:"0"+dd[0]); // padding
                };
                var d = new Date();

                s.factura.fecha = d.yyyymmdd();
                $('#agregarFactura').slideDown();
            }
        };

        s.crearArrayProveedordes = function(){
            s.proveedoresObj = [{nom_proveedor: 'SELECCIONE PROVEEDOR...'}];
            for (var i = 0; i < s.proveedoresCajaChica.length; i++) {
                for (var j = 0; j < s.proveedores.length; j++) {
                    if( s.proveedoresCajaChica[i] == s.proveedores[j].id_proveedor )
                        s.proveedoresObj.push(s.proveedores[j]);
                };
            };
            s.proveedorFactura = s.proveedoresObj[0];
            s.factura.rfcProveedor = '';
            //s.ordenesCompra = [{id_ordencompra:0, nombre:'SELECCIONE PROVEEDOR...'}];
            //s.ordenCompra = s.ordenesCompra[0];
            s.movimientos = [{id_inventariomovimiento:0, nombre:'SELECCIONE PROVEEDOR...'}];
            s.movimiento = s.movimientos[0];
        };


        s.cargarOC = function( empresa,obra,cuenta ){
                
                if (empresa && obra && cuenta){
                    s.dineroDisponible = [];
                    
                    s.dineroDisponible.sum_total = cuenta.sum_total;
                    s.rs = f.cfJson('app/componentes/ordenes_compra.cfc', 'leer_remoto', {id_empresa: empresa, id_obra: obra});

                    if( s.rs.TIPOERROR != '' ){
                        message.error(s.rs.MENSAJEERROR + '<br>' + s.rs.DETALLEERROR);
                        return false;
                    }



                    s.ordenesCompra = s.rs.data;
                    for (var i = 0; i < s.ordenesCompra.length; i++) {
                        s.ordenesCompra[i].nombre = 'Orden de Compra '+ s.ordenesCompra[i].id_ordencompra;
                    };
                    //s.ordenesCompra.splice(0,0,{id_ordencompra:0, nombre:'SELECCIONE O.C. ...'});
                    //s.ordenCompra = s.ordenesCompra[0];
                    s.movimientos = [{id_inventariomovimiento:0, nombre:'SELECCIONE O.C. ...'}];
                    s.movimiento = s.movimientos[0];


                }
                else
                {
                    s.ordenesCompra = [];
                    s.ordenCompra = [];
                    s.factura = [];
                    s.factura.orIva = 0;
                    // s.ordenCompra.nb_proveedor = '';
                    // s.ordenCompra.de_rfc = '';

                }



            
        };
        
        s.cargarMovimimientos = function(){
            var data = {
                id_empresa: s.ordenCompra.id_empresa,
                id_obra: s.ordenCompra.id_obra,
                id_ordencompra: s.ordenCompra.id_ordencompra,
                ids_movimientos: s.movimientosCheque.toString(),
            };
            s.rs = f.cfJson('app/componentes/inventario_movimientos.cfc', 'por_orden_compra_remoto', data);

            if( s.rs.TIPOERROR != '' ){
                message.error(s.rs.MENSAJEERROR + '<br>' + s.rs.DETALLEERROR);
                return false;
            }

            s.movimientos = s.rs.data;
            for (var i = 0; i < s.movimientos.length; i++) {
                s.movimientos[i].nombre = s.movimientos[i].nom_almacen +'-'+ s.movimientos[i].id_inventariomovimiento;
            };

            s.movimientos.splice(0,0,{id_inventariomovimiento:0, nombre:'SELECCIONE MOVIMIENTO...'});
            s.movimiento = s.movimientos[0];
        };

        s.cambiarImporte = function(){
            if(!s.factura.valeAzul && !s.factura.porcentajeIVA){
                event.preventDefault();
                f.errorMessage( 'Seleccione el porcentaje de iva.', $('#pj_iva') );
                return false;
            }
            s.factura.iva = (parseFloat(s.factura.importe) * parseFloat(s.factura.porcentajeIVA.pj_iva) / 100).toFixed(2);
            s.factura.total = parseFloat(s.factura.importe) + parseFloat(s.factura.iva);
            
            s.factura.total = s.factura.total.toFixed(2);
            s.calcularISR();
            s.calcularRetencionIva();
            // switch(s.factura.orIva){
            //     case '0': s.factura.retencionIva = ''; break;
            //     case '1': s.factura.retencionIva = (s.factura.importe * 0.106667).toFixed(2); s.factura.porcentajeRetencionIVA = 10.6667; break;
            //     case '2': s.factura.retencionIva = (s.factura.importe * 0.04).toFixed(2); s.factura.porcentajeRetencionIVA = 4; break;
            // }
        }

        s.calcularRetencionIva = function(){
            if( s.factura.importe ){
                s.factura.total = parseFloat(s.factura.importe) + parseFloat(s.factura.iva);
                s.calcularISR();
                if( s.factura.retencionIva ){
                   s.factura.total = parseFloat(s.factura.total) - parseFloat(s.factura.retencionIva);
                }
                // switch(s.factura.orIva){
                //     case '0': s.factura.retencionIva = ''; break;
                //     case '1': s.factura.retencionIva = (s.factura.importe * 0.106667).toFixed(2); s.factura.porcentajeRetencionIVA = 10.6667; break;
                //     case '2': s.factura.retencionIva = (s.factura.importe * 0.04).toFixed(2); s.factura.porcentajeRetencionIVA = 4; break;
                // }
                // if( s.factura.orIva != '0' )
                //     s.factura.total += parseFloat(s.factura.retencionIva);  
                s.factura.total = (parseFloat(s.factura.total)).toFixed(2);
            }
        }

        s.calcularISR = function(){
            
            if( s.factura.orIsr && s.factura.importe ){

                s.factura.retencionISR =  (s.factura.importe * 0.1).toFixed(2);
                s.factura.total += parseFloat(s.factura.retencionISR);  
            }else{
                if(s.factura.retencionISR){
                   s.factura.total = s.factura.total - parseFloat(s.factura.retencionISR);
                }
                s.factura.retencionISR = ''; 
            }
            s.factura.total = (parseFloat(s.factura.total)).toFixed(2);

        }

        s.calcularImportes = function(){

                s.iva = f.rsJson(<cfoutput>#serializeJson(iva)#</cfoutput>);
                var iva = parseFloat(s.ordenCompra.pj_iva).toFixed(2);
                //s.factura.porcentajeIVA = parseFloat(s.ordenCompra.pj_iva).toFixed(2);;
                s.factura.porcentajeRetencionIVA = 0;
                s.factura.porcentajeRetencionISR = 0;
                var data = {
                    id_empresa: s.ordenCompra.id_empresa,
                    id_obra: s.ordenCompra.id_obra,
                    idu_almacen: s.movimiento.idu_almacen,
                    idu_movimiento: s.movimiento.id_inventariomovimiento
                };

                // SI SE MARCAR COMO VALE AZUL QUITARL LOS IMPUESTOS Y ASIGNAR NOMBRE DE FACTURA
                if( s.factura.valeAzul ){
                    s.factura.orIva = '0';
                    s.factura.orIsr = false;
                    s.factura.numeroFactura = 'NDOC'+s.ordenCompra.id_ordencompra;
                    //s.factura.porcentajeIVA.pj_iva = 0;
                }

                s.factura.importeEntrada = parseFloat(0);
                s.factura.importe = parseFloat(0);
                

                // s.factura.importeEntrada = (s.ordenCompra.im_total).toFixed(2);
                s.factura.importeEntrada = parseFloat(s.ordenCompra.subtotal).toFixed(2);
                s.factura.importe = s.factura.importeEntrada;

                s.factura.iva = !s.factura.valeAzul ? (s.factura.importeEntrada * iva / 100).toFixed(2): '';
                s.factura.retencionIva = '';
                switch(s.factura.orIva){
                    case '0': s.factura.retencionIva = ''; break;
                    case '1': s.factura.retencionIva = (s.factura.importe * 0.106667).toFixed(2); s.factura.porcentajeRetencionIVA = 10.6667; break;
                    case '2': s.factura.retencionIva = (s.factura.importe * 0.04).toFixed(2); s.factura.porcentajeRetencionIVA = 4; break;
                }
                s.factura.retencionISR = s.factura.orIsr ? (s.factura.importe * 0.1).toFixed(2) : '';
                s.factura.porcentajeRetencionISR = s.factura.orIsr ? 10 : 0;
                
                s.factura.total = parseFloat(s.factura.importe);
                s.factura.total += !s.factura.iva || s.factura.iva == '' ? 0 : parseFloat(s.factura.iva);
                s.factura.total -= s.factura.retencionIva == '' ? 0 : parseFloat(s.factura.retencionIva);
                s.factura.total -= s.factura.retencionISR == '' ? 0 : parseFloat(s.factura.retencionISR);
                s.factura.total = (s.factura.total).toFixed(2);

                var totalFacturas = 0;
                totalFacturas = parseFloat(totalFacturas);
                for (var i = 0; i < s.facturas.length; i++) {
                    totalFacturas += parseFloat(s.facturas[i].total);
                };
                totalFacturas = parseFloat(totalFacturas);
                totalFacturas += parseFloat(s.factura.total);
                totalFacturas = (totalFacturas).toFixed(2);
                
                /*if( totalFacturas > parseFloat(s.dineroDisponible.sum_total) ){
                    // $('.alerta').text("No hay suficiente dinero en la cuenta.");
                    f.errorMessage( 'No hay suficiente dinero en la cuenta.', $('#id_Cuenta') );
                    s.factura = [];
                    s.factura.orIva = 0;
                    s.ordenCompra = [];
                    return false;
                }else{*/
                    //$('.alerta').text('');
                    s.cargarFacturasLigadas(s.empresa.id_empresa,s.obra.id_obra,s.ordenCompra.id_ordencompra);
                //}

            
        };

        s.cargarFacturasLigadas = function(id_empresa,id_obra,id_ordencompra){

            
            s.rs = f.cfJson('app/componentes/facturas.cfc', 'leer_facturas_ligadas_oc', {id_empresa:id_empresa ,id_obra:id_obra ,id_ordencompra: id_ordencompra}); 
            s.facturasLigadas = s.rs.data;

        };

        s.cargarObras = function(id_empresa){
           if( id_empresa ){
               s.rs = f.cfJson('app/componentes/obras.cfc', 'leer', {id_empresa: id_empresa}); 
               s.obras = s.rs.data;

           } else {
                s.obras = [];
                s.ordenesCompra = [];
                s.ordenCompra = [];
                // s.ordenCompra.nb_proveedor = '';
                // s.ordenCompra.de_rfc = '';
                s.cuentas = [];
           }
        };

        s.agregarFactura = function(){

            if( !s.ordenCompra.id_ordencompra || s.ordenCompra.id_ordencompra  == 0 ){
                event.preventDefault();
                f.errorMessage( 'Debe seleccionar orden de compra.', $('#orden_compra') );
                return false;
            }
            var totalFacturas = 0;
            totalFacturas = parseFloat(totalFacturas);
            for (var i = 0; i < s.facturas.length; i++) {
                totalFacturas += parseFloat(s.facturas[i].total);
            };
            totalFacturas = parseFloat(totalFacturas);
            totalFacturas += parseFloat(s.factura.total);
            totalFacturas = (totalFacturas).toFixed(2);;

            if( parseFloat(s.factura.total) >  parseFloat(s.dineroDisponible.sum_total).toFixed(2) ){
                event.preventDefault();
                f.errorMessage( 'No hay suficiente dinero en la cuenta.', $('#orden_compra') );
                return false;
            }  
            if( !s.factura.numeroFactura || s.factura.numeroFactura  == '' ){
                event.preventDefault();
                f.errorMessage( 'Debe ingresar el numero de factura.', $('#num_factura') );
                return false;
            }
            if( !s.factura.fecha || s.factura.fecha  == '' ){
                event.preventDefault();
                f.errorMessage( 'Debe ingresar la fecha.', $('#fechaFactura') );
                return false;
            }
            if(!s.factura.valeAzul && !s.factura.porcentajeIVA){
                event.preventDefault();
                f.errorMessage( 'Seleccione el porcentaje de iva.', $('#pj_iva') );
                return false;
            }

            if(!s.factura.valeAzul){
                var extPDF = ($('input[name=pdf]').val().substring($('input[name=pdf]').val().lastIndexOf("."))).toLowerCase()
            
                if( $('input[name=pdf]').val() == '' || extPDF != '.pdf' || !extPDF){
                    event.preventDefault();
                    f.errorMessage( 'Debe ingresar el archivo PDF.', $('#pdf') );
                    return false;
                }
                var extXML = ($('input[name=xml]').val().substring($('input[name=xml]').val().lastIndexOf("."))).toLowerCase()
                if( $('input[name=xml]').val()  == '' || extXML != '.xml' || !extXML){
                    event.preventDefault();
                    f.errorMessage( 'Debe ingresar el archivo XML.', $('#xml') );
                    return false;
                }
            }
             var extXML = ($('input[name=soporte]').val().substring($('input[name=soporte]').val().lastIndexOf("."))).toLowerCase()
            if( s.factura.valeAzul & !extXML ){
                event.preventDefault();
                f.errorMessage( 'Debe ingresar el soporte.', $('#soporte') );
                return false; 
            }

            s.factura.proveedorFactura = s.factura.idProveedor;
            s.factura.empresaOrigen = s.ordenCompra.id_empresa;
            s.factura.idOrdenCompra = s.ordenCompra.id_ordencompra;
            s.factura.movimiento = s.movimiento.idu_movimiento;
            s.factura.almacen = s.movimiento.idu_almacen;
            
            if(s.factura.iva == '')
                s.factura.iva = 0;
            if(!s.factura.retencionIva || s.factura.retencionIva == '')
                s.factura.retencionIva = 0;
            if(!s.factura.retencionISR || s.factura.retencionISR == '')
                s.factura.retencionISR = 0;
            
            if(!s.cajaChica){
                $('#id_prov').attr('class', 'hidden');
                $('#dis_proveedor').removeAttr('class').attr('class', 'readonly');
            }
            $("#idu_tipos_cheques").attr('disabled', 'disabled').attr('class', 'readonly');
            
            s.importe = !s.importe || s.importe == '' ? 0 : parseFloat(s.importe);
            s.importe += parseFloat(s.factura.total);
            s.importe = parseFloat(s.importe).toFixed(2);
            
            // if (s.importe != ""){
            //     var numero = s.importe;
            //     var clsFunciones = new jsFunciones();
            //     s.des_importe = clsFunciones.converirNumeroALetras_remote(numero);
            // }
            
            //s.facturas.push(s.factura);
            //s.factura = {};
            //$('#agregarFactura').slideUp();
            // s.moviemientos();

            //s.formCheque.submit();
        };
        
        s.eliminarFactura = function( i ){
            s.importe = parseFloat(s.importe) - parseFloat($("#imp_total"+i).val());
            s.importe = parseFloat(s.importe);
            s.importe.toFixed(2);

            if(s.importe == 0){
                s.des_importe = '';
            }else{
                var numero = s.importe;
                var clsFunciones = new jsFunciones();
                s.des_importe = clsFunciones.converirNumeroALetras_remote(numero);
            }
            
            s.facturas.splice(i, 1);
            s.movimientos.splice(i, 1);
            if( s.facturas.length == 0 ){
                s.proveedores.splice(1, s.proveedores.length -1);
                $('#id_prov').removeAttr('class', 'hidden');
                $('#dis_proveedor').removeAttr('class', 'readonly').attr('class', 'hidden');
                $("#idu_tipos_cheques").removeAttr('disabled').removeAttr('class');
            }
            s.moviemientos();
        };

        s.moviemientos = function(){
            s.movimientosCheque = [];
            for (var i = 0; i < s.facturas.length; i++) {
                s.movimientosCheque.push(s.facturas[i].movimiento);
            };
        };
        
        s.agregarCheque = function( event ){
            // if( !s.beneficiario || s.beneficiario  == '' ){
            //     event.preventDefault();
            //     f.errorMessage( 'Debe ingresar el nombre del beneficiario.', $('#nom_nombre_beneficiario') );
            //     return false;
            // }
            // if( !s.concepto_pago || s.concepto_pago  == '' ){
            //     event.preventDefault();
            //     f.errorMessage( 'Debe especificar el concepto de pago.', $('#des_concepto_pago') );
            //     return false;
            // }
            // if( !s.tipo || s.tipo  == '' ){
            //     event.preventDefault();
            //     f.errorMessage( 'Debe seleccionar el tipo de cheque.', $('#idu_tipos_cheques') );
            //     return false;
            // }
            // if( !s.importe || s.importe  == '' ){
            //     event.preventDefault();
            //     f.errorMessage( 'Debe agregar facturas al cheque.', $('#btnFacturas') );
            //     return false;
            // }
            $('[disabled]').removeAttr('disabled');
            f.main.submit( $(formCheque), 'submit/agregar-cheque-credito.cfm', true );
        };

        s.cargarCuentas = function( id_empresa,id_obra ){

            //s.cargarOC( id_empresa, id_obra )
            if( id_obra ){
                var rsCuentas = f.cfJson('app/componentes/libro_bancos.cfc', 'cuentas_activosPasivos', {id_empresa: id_empresa, id_obra: id_obra}); 
                s.cuentas = rsCuentas.data;
                
                for( var x = 0; x < s.cuentas.length; x ++ ){
                    var sum_total = s.cuentas[x].sum_total;
                    sum_total = parseFloat(sum_total).toFixed(2);

                    var miles=new RegExp("(-?[0-9]+)([0-9]{3})");
                    while(miles.test(sum_total)) {
                        sum_total=sum_total.replace(miles, "$1" + ',' + "$2");
                    }

                    s.cuentas[x].de_cuenta = s.cuentas[x].id_cuenta + 
                                             ' - ' + 
                                             s.cuentas[x].nb_cuenta + 
                                             ' - ' + 
                                             s.cuentas[x].de_banco +
                                             ' - ' + 
                                             s.cuentas[x].de_cuentabancaria +
                                             ' - $' + 
                                             sum_total;
                    s.cuentas[x].index = x;   
                }

                
            }else{
                s.cuentas = [];
            }

        }

        s.clonarArchivos = function ( index ){
            // Clonar el input file
            $('[name=pdf]').find('#file')
                .clone()
                .attr('id', 'clone')
                .appendTo($('[name=pdf]').find('#file').parent());
            $('[name=fpdf]')
                .clone()
                .attr('id', 'clone')
                .appendTo($('[name=fpdf]').find('#folfile').parent());
                
            $('[name=xml]').find('#file')
                .clone()
                .attr('id', 'clone')
                .appendTo($('[name=xml]').find('#file').parent());
            $('[name=fxml]')
                .clone()
                .attr('id', 'clone')
                .appendTo($('[name=fxml]').find('#folfile').parent());

            $('[name=soporte]').find('#file')
                .clone()
                .attr('id', 'clone')
                .appendTo($('[name=soporte]').find('#file').parent());
            $('[name=fsoporte]')
                .clone()
                .attr('id', 'clone')
                .appendTo($('[name=fsoporte]').find('#folfile').parent());
            
            // Cambiar el nombre del file que contiene el archivo y agregarlo al form
            $('[name=pdf]').find('#file')
                .attr('name', 'pdf'+index)
                .removeClass('hidden')
                .appendTo(formCheque)
                .attr('class', 'hidden');
            $('[name=fpdf]')
                .attr('name', 'fpdf'+index)
                .removeClass('hidden')
                .appendTo(formCheque)
                .attr('class', 'hidden');
                
            $('[name=xml]').find('#file')
                .attr('name', 'xml'+index)
                .removeClass('hidden')
                .appendTo(formCheque)
                .attr('class', 'hidden');
            $('[name=fxml]')
                .attr('name', 'fxml'+index)
                .removeClass('hidden')
                .appendTo(formCheque)
                .attr('class', 'hidden');

            $('[name=soporte]').find('#file')
                .attr('name', 'soporte'+index)
                .removeClass('hidden')
                .appendTo(formCheque)
                .attr('class', 'hidden');
            $('[name=fsoporte]')
                .attr('name', 'fsoporte'+index)
                .removeClass('hidden')
                .appendTo(formCheque)
                .attr('class', 'hidden');


            // Cambiar el id del file clone para que fincione como el original en el boton de archivo
            $('[name=pdf]').find('#clone')
                .attr('id', 'file');
            $('[name=fpdf]').find('#clone')
                .attr('id', 'folfile');
                
            $('[name=xml]').find('#clone')
                .attr('id', 'folfile');
            $('[name=fxml]').find('#clone')
                .attr('id', 'folfile');

            $('[name=soporte]').find('#clone')
                .attr('id', 'folfile');
            $('[name=fsoporte]').find('#clone')
                .attr('id', 'folfile');

            // Cambiar la etiqueta del boton de arvhivo
            $('[name=pdf]').find('label').text('Seleccione archivo...');
            $('[name=xml]').find('label').text('Seleccione archivo...');
            $('[name=soporte]').find('label').text('Seleccione archivo...');
            f.angularCompile( $("#pdf_file") );
            f.angularCompile( $("#xml_file") );
            f.angularCompile( $("#soporte_file") );
        };
        
    };
</script>




<div id="cheque" ng-controller="chequeController" ng-init="shForm()" class="form">
    <form name="formCheque" class="form hidden" ng-submit="agregarCheque($event)" action="?">

        <div id="agregarFactura" align="center" >
            <h3><i>Agregar factura</i></h3>
            <br>
            <table>
                 <tr>
                    <td align="right">Empresa :</td>
                    <td align="left" colspan="2">
                        <!--- <input type="text" id="id_empresa" name="id_empresa" class="hidden" ng-model="fa"> --->
                        <select style="width:100%" id="id_empresa" name="id_empresa"  ng-model="empresa" ng-options="empresa.nb_empresa for empresa in empresas track by empresa.id_empresa" ng-change="cargarObras(empresa.id_empresa)">
                            <option value="">Seleccione empresa...</option>
                        </select>
                        
                    </td>
                </tr>

                 <tr>
                    <td align="right">Obra :</td>
                    <td align="left" colspan="2" >
                        <!--- <input type="text" id="id_obra" name="id_obra"  class="hidden" ng-model="obra.id_obra"> --->
                        <select style="width:100%" id="id_obra" name="id_obra"   ng-model="obra" ng-options="obra.nombre_obra for obra in obras track by obra.id_obra" ng-change="cargarCuentas(empresa.id_empresa,obra.id_obra)" >
                            <option value="">Seleccione obra...</option>
                        </select>
                        
                    </td>
                </tr>
                <tr>
               <!---  cargarOC(empresa.id_empresa,obra.id_obra) --->    
                <td align="right" >Caja Chica:</td>
                <td>
                    <select style="width:100%" id="id_cajachica" name="id_cajachica"  ng-model="cuenta" ng-options="cuenta.de_cuenta for cuenta in cuentas track by cuenta.index"  ng-change="cargarOC(empresa.id_empresa,obra.id_obra,cuenta)">
                        <option value="">Seleccione una cuenta...</option>
                    </select>
                    
                    <input type="text" id="id_cuenta" name="id_cuenta" ng-model="cuenta.id_cuenta" class="hidden">
                    <input type="text" id="id_banco" name="id_banco" ng-model="cuenta.id_banco" class="hidden">
                    <input type="text" id="id_cuentabancaria" name="id_cuentabancaria" ng-model="cuenta.id_cuentabancaria" class="hidden">
                </td>
            </tr>
                <tr>
                    <td align="right">O.C.:</td>
                    <td align="left" colspan="2">
                        <input type="text" id="id_ordencompra" name="id_ordencompra" ng-model="ordenCompra.id_ordencompra" class="hidden">
                        <select style="width:100%" id="orden_compra" ng-model="ordenCompra" ng-options="oc.nb_orden_compra for oc in ordenesCompra" ng-change="calcularImportes(ordenesCompra.id_ordencompra)">
                            <option value="">Seleccione orden de compra...</option>
    
                        </select>
                    </td>
                </tr>
                 <tr >
                    <td align="right">
                        Proveedor:
                    </td>
                    <td align="left">
                        <input type="text" name="id_proveedor" ng-model="ordenCompra.id_proveedor" class="hidden">
                        <input type="text" id="nb_proveedor" name="nb_proveedor" ng-model="ordenCompra.nb_proveedor"  ng-readonly=" true" class="readonly">
                        
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        RFC:
                    </td>
                    <td align="left">
                        <input type="text" id="rfc_Proveedor" name="rfc_Proveedor" ng-model="ordenCompra.de_rfc" ng-readonly=" true" class="readonly" />
                    </td>
                </tr>
                
                <tr>

                        <td colspan="2" >

                        <table align="center" border="1" width="100%">

                            <tr class="encabezado" align="center">
                                <td colspan="4">
                                    Facturas ligadas a la Orden de Compra
                                </td>
                            </tr>
                            <tr class="encabezado">
                                <td>Factura</td>
                                <td>Fecha</td>
                                <td>Importe</td>
                                <td>Importe Notas Credito</td>
                            </tr>
                            
                            <tr style="font-size:12px; background-color:##FFFFFF;" onMouseOver="this.style.backgroundColor='##99CCCC'" onMouseOut="this.style.backgroundColor='##FFFFFF'" ng-repeat="factura in facturasLigadas">
                                <td>{{factura.nu_factura}}</td>
                                <td>{{factura.fh_factura | date: 'longDate' }}</td>
                                <td>{{factura.nu_importe_total}}</td>
                                <td>{{factura.im_totalnota}}</td>
                            </tr>
                        </table>
                        </td>                        
                </tr>
                <tr>
                    <td align="right">Importe O.C.: $</td>
                    <td align="left"><input type="text" name="importeOc" ng-model="factura.importeEntrada" ng-readonly="true" style="background-color: #ddd;"/></td>
                </tr>
                <tr>
                    <td align="right">No. Factura:</td>
                    <td align="left">
                        <input type="text" name="num_factura" id="num_factura" ng-model="factura.numeroFactura" ng-readonly="factura.valeAzul" ng-class="factura.valeAzul ? 'readonly':''"/>
                        <input name="sn_deducible" type="checkbox" ng-model="factura.valeAzul" ng-change="calcularImportes();" /> No deducible
                    </td>
                </tr>
                <tr>
                    <td align="right">Fecha:</td>
                    <td align="left">
                        <input type="text" name="fechaFactura" id="fechaFactura" ng-model="factura.fecha" date max-date="0">
                    </td>
                </tr>
                <tr>
                    <td align="right">%I.V.A.: </td>
                    <td align="left">
                        <!--- <input type="text" name="pj_iva" ng-model="factura.porcentajeIVA" class="hidden">
                        <input name="imp_iva" id="imp_iva" type="text" ng-model="factura.iva" ng-readonly="true" class="readonly" /> --->
                        <select name="pj_iva" id="pj_iva" ng-model="factura.porcentajeIVA" ng-options="i.pj_iva for i in iva track by i.pj_iva" ng-change="cambiarImporte()">
                            <option value="">Seleccione porcentaje de IVA...</option>
                        </select>
                    </td>
                </tr>
                <tr ng-show="ordenCompra">
                    <td align="right">Importe: $</td>
                    <td align="left">
                        <input name="imp_factura" id="imp_factura" type="text" ng-model="factura.importe" ng-blur="cambiarImporte()"/>
                    </td>
                </tr>
                
                <tr>
                    <td align="right">I.V.A.: $</td>
                    <td align="left">
                        <!--- <input type="text" name="pj_iva" ng-model="factura.porcentajeIVA" class="hidden"> --->
                        <input name="imp_iva" id="imp_iva" type="text" ng-model="factura.iva" ng-readonly="true" class="readonly" />
                    </td>
                </tr>
                <tr>
                    <td align="right">Retenci&oacute;n IVA: $</td>
                    <td align="left">
                         <input type="text" name="imp_retencion_iva" id="imp_retencion_iva" ng-model="factura.retencionIva"  numeric ng-blur="calcularRetencionIva()"/>
                        <!---<select name="op_retencion_iva" ng-model="factura.orIva" ng-change="calcularRetencionIva()" ng-readonly="factura.valeAzul"  ng-disabled="factura.valeAzul" ng-class="factura.valeAzul ? 'readonly':''" ng-init="factura.orIva = 0" style="width:130px;">
                            <option value="0">Factura</option>
                            <option value="1">Con retenci&oacute;n</option>
                            <option value="2">Flete</option>
                        </select> --->
                    </td>
                </tr>
                <tr>
                    <td align="right">Retenci&oacute;n ISR: $</td>
                    <td align="left">
                        <input type="text" name="imp_retencion_isr" id="imp_retencion_isr" ng-model="factura.retencionISR" ng-readonly="true" class="readonly" />
                        <input type="checkbox" name="op_retencion_isr" id="op_retencion_isr" ng-model="factura.orIsr" ng-change="calcularISR()" ng-disabled="factura.valeAzul" /> 10%  
                    </td>
                </tr>
                <tr>
                    <td align="right">Total: $</td>
                    <td align="left">
                        <input type="text" name="imp_total" id="imp_total" ng-model="factura.total" ng-readonly="true" class="readonly" />
                    </td>
                </tr>
                <tr id="pdf_file">
                    <td align="right">PDF:</td>
                    <td align="left">
                        <file name="pdf" ng-model="factura.pdf" format="application/pdf"/>
                    </td>
                </tr>
                <tr id="xml_file">
                    <td align="right">XML:</td>
                    <td align="left">
                        <file name="xml" ng-model="factura.xml" format="text/xml"/>
                    </td>
                </tr>
                <tr id="soporte_file">
                    <td align="right">Soporte:</td>
                    <td align="left">
                        <file name="soporte" ng-model="factura.soporte"/>
                    </td>
                </tr>
                <tr>
                    <td align="right">Observaci&oacute;nes:</td>
                    <td align="left">
                        <textarea type="text" name="des_soporte" id="des_soporte" ng-model="factura.des_soporte" style="width:250px;"></textarea>
                    </td>
                </tr>
                <tr>
                    <td align="right" colspan="2">
                        <label class="alerta" style="color:#FF0000;font-size:20px;float:left;"></label>
                        <button type="button" ng-click="formFacturas()">Cancelar</button>
                        <button ng-click="agregarFactura()" class="primary">Agregar</button>
                    </td>
                </tr>
            </table>
        </div>

    </form>
</div>



<script type="text/javascript">
    f.angularCompile( $("#cheque") );
</script>