/* Variables Globales */
var colorSelected	= '#fffccc';
var colorNoSelected	= 'transparent';
var lastGrid		= '';
var lastRow			= 0;
var lastFocus		= null;
var lastCellRow		= 0;
var lastCellCol		= 0;

function fnc_gridExcelShowFilterBar(grid)
{
	$('#' + grid + '_filterBarLink').slideUp();
	$('#' + grid + '_filterBar').slideDown();
	$x(grid + '_filterText').focus();
}

function fnc_grdExcelCellListener(e, grid, row, col)
{
	var keyCode		= 0;
	if(e) keyCode	= e.which;
	else keyCode	= event.keyCode ? event.keyCode : event.charCode; 
	var maxRow		= eval(grid + "_rows");
	var maxCol		= eval(grid + "_cols");
	var comp		= $x(grid + '_Cell_' + row + '_' + col);
	var start		= comp.selectionStart;
	var end			= comp.selectionEnd;
	/* F2 (Modo de edicion)*/
	if (keyCode == 113){ 
		comp.selectionStart = comp.selectionEnd = 0; 
		fnc_gridExcelDeleteFormat(grid, row, col);
		$("#" + comp.id).unbind('keyup').removeAttr('onkeyup').keyup(function(event) { 
			fnc_grdExcelCellEditListener(event, grid, row, col); 
		}); 
	}
	/* Enter */
	if (keyCode == 13) { 
		/* Se elimina el evento del enter */
		if(e) e.preventDefault();
		else {
			event.preventDefault();
			if(event.keyCode) event.keyCode = 0;
			else event.charCode = 0;
		}
		col++; fnc_gridExcelSetNextRight(grid, row, col, false);
	}
	/* Abajo */
	if (keyCode == 40) { row++; fnc_gridExcelSetNextFocusDown(grid, row, col); }
	/* Derecha */
	if (keyCode == 39) { col++; fnc_gridExcelSetNextRight(grid, row, col, true); }
	/* Izquierda */
	if (keyCode == 37) { col--; fnc_gridExcelSetNextLeft(grid, row, col); }
	/* Arriba */
	if (keyCode == 38) { row--; fnc_gridExcelSetNextFocusUp(grid, row, col); }
}

function fnc_gridExcelSetNextFocusUp(grid, row, col)
{
	var maxRow       = eval(grid + "_rows");
	var maxCol       = eval(grid + "_cols");
	var encontroFoco = false;
	
	/* Se busca un componente que se encuentre en la misma columna pero
	   que sea superior al componente que lanza el evento */
	for(var i=row; i>=1; i--){
		if(fnc_gridExcelIsRowVisible(grid, i)){
			$x(grid + '_Cell_' + i + '_' + col).focus(); 
			encontroFoco = true;
			break;
		}
	}
	
	/* Si no se encontro el foco se empezan a recorrer las columnas 
	   de derecha a izquierda hasta llegar al final del grid en busca 
	   de un componente que pueda obtener el foco */
	if(!encontroFoco){
		for(var i=col-1; i>=1; i++){//Columnas 
			for(var j=maxRow; j>=1; j--){ //Filas (Renglones)
				if(fnc_gridExcelIsRowVisible(grid, j)){
					$x(grid + '_Cell_' + j + '_' + i).focus(); 
					encontroFoco = true;
					break;
				}
			}
			if(encontroFoco) break;
		}
	}
	
	/* Si se llego al final del grid y no se encontro ningun
	   componente capaz de tomar el foco se regresa al final
	   del grid y se recorre de derecha a izquierda en busqueda de un 
	   componente que pueda recibir el foco */
	if(!encontroFoco){
		for(var i=maxCol; i>=1; i--){//Columnas 
			for(var j=maxRow; j>=1; j--){ //Filas (Renglones)
				if(fnc_gridExcelIsRowVisible(grid, j)){
					$x(grid + '_Cell_' + j + '_' + i).focus(); 
					encontroFoco = true;
					break;
				}
			}
			if(encontroFoco) break;
		}
	}
}

function fnc_gridExcelSetNextFocusDown(grid, row, col)
{
	var maxRow       = eval(grid + "_rows");
	var maxCol       = eval(grid + "_cols");
	var encontroFoco = false;

	/* Se busca un componente que se encuentre en la misma columna pero
	   que sea inferior al componente que lanza el evento */
	for(var i=row; i<=maxRow; i++){
		/* Se pregunta si el estatus es activo (1) */
		if(fnc_gridExcelIsRowVisible(grid, i)){
			$x(grid + '_Cell_' + i + '_' + col).focus(); 
			encontroFoco = true;
			break;
		}
	}
	
	/* Si no se encontro el foco se empezan a recorrer las columnas 
	   de izquierda a derecha hasta llegar al final del grid en busca 
	   de un componente que pueda obtener el foco */
	if(!encontroFoco){
		for(var i=col+1; i<=maxCol; i++){//Columnas 
			for(var j=1; j<=maxRow; j++){ //Filas (Renglones)
				if(fnc_gridExcelIsRowVisible(grid, j)){
					$x(grid + '_Cell_' + j + '_' + i).focus(); 
					encontroFoco = true;
					break;
				}
			}
			if(encontroFoco) break;
		}
	}
	
	/* Si se llego al final del grid y no se encontro ningun
	   componente capaz de tomar el foco se regresa al principio
	   del grid y se recorre de inicio a fin en busqueda de un 
	   componente que pueda recibir el foco */
	if(!encontroFoco){
		for(var i=1; i<=maxCol; i++){//Columnas 
			for(var j=1; j<maxRow; j++){ //Filas (Renglones)
				if(fnc_gridExcelIsRowVisible(grid, j)){
					$x(grid + '_Cell_' + j + '_' + i).focus(); 
					encontroFoco = true;
					break;
				}
			}
			if(encontroFoco) break;
		}
	}
}

function fnc_gridExcelSetNextRight(grid, row, col, newBegin)
{
	var maxRow       = eval(grid + "_rows");
	var maxCol       = eval(grid + "_cols");
	var encontroFoco = false;
	
	if(col <= maxCol) $x(grid + '_Cell_' + row + '_' + col).focus(); 
	else{
		row++;
		col = 1;
		for(var i=row; i<=maxRow; i++){
			if(fnc_gridExcelIsRowVisible(grid, i)){
				$x(grid + '_Cell_' + i + '_' + col).focus(); 
				encontroFoco = true;
				break;
			}
		}
		if(newBegin && !encontroFoco){
			for(var i=1; i<=row; i++){
				if(fnc_gridExcelIsRowVisible(grid, i)){
					$x(grid + '_Cell_' + i + '_' + col).focus(); 
					break;
				}
			}		
		}
		if(!newBegin && !encontroFoco){
			fnc_gridExcelAddRow(grid);
			$x(grid + '_Cell_' + (maxRow + 1) + '_1').focus(); 	
		}
	}
}

function fnc_gridExcelSetNextLeft(grid, row, col)
{
	var maxRow       = eval(grid + "_rows");
	var maxCol       = eval(grid + "_cols");
	var encontroFoco = false;
	
	if(col >= 1) $x(grid + '_Cell_' + row + '_' + col).focus(); 
	else{
		row--;
		col = maxCol;
		for(var i=row; i>= 1; i--){
			if(fnc_gridExcelIsRowVisible(grid, i)){
				$x(grid + '_Cell_' + i + '_' + col).focus(); 
				encontroFoco = true;
				break;
			}
		}
		if(!encontroFoco){
			for(var i=maxRow; i>=row; i--){
				if(fnc_gridExcelIsRowVisible(grid, i)){
					$x(grid + '_Cell_' + i + '_' + col).focus(); 
					break;
				}
			}		
		}
	}
}

function fnc_grdExcelCellEditListener(e, grid, row, col)
{
	var comp		= $x(grid + '_Cell_' + row + '_' + col);
	var keyCode		= e.keyCode;
	/* Enter o Esc */
	if (keyCode == 13 || keyCode == 27) {
		$("#" + comp.id).unbind('keyup').removeAttr('onkeyup').keyup(function(event) {
			fnc_grdExcelCellListener(event, grid, row, col);
		});
		//fnc_gridExcelValidarFormato(comp, grid, row, col);
		fnc_gridExcelSetFocus(comp, grid, row, col);
	}
}

function fnc_gridExcelAddRow(grid)
{
	var rows          = eval(grid + '_rows + 1');
	var cols          = eval(grid + '_cols');
	var widths        = eval(grid + '_widths');
	var aligns        = eval(grid + '_aligns');
	var btnDelete     = eval(grid + '_delete');
	var multiSelect   = eval(grid + '_multiSelect');
	var hotkeys       = eval(grid + '_hotKeys');
	var hkActions     = eval(grid + '_hotKeysActions');
	var blurActions   = eval(grid + '_blurActions');
	var changeActions = eval(grid + '_changeActions');
	var clickActions  = eval(grid + '_clickActions');
	var readOnly      = eval(grid + '_readOnly');
	var myTable       = $x('table_' + grid);
			
	/* Generar el renglon */
	var newRow  = myTable.insertRow(-1);
	newRow.setAttribute("id"  , "tr_" + grid + "_" + rows);
	
	/* Componente de estatus */
	var status = document.createElement("input");
	status.setAttribute("name"     , grid + "_Status_" + rows);
	status.setAttribute("id"       , grid + "_Status_" + rows);
	status.setAttribute("type"     , "hidden");
	status.setAttribute("value"    , "1");
	
	/* Generar la columna base */
	var baseCell = newRow.insertCell(-1);
	if(multiSelect){
		/* Si es multiselect se agrega un componente de checkbox
		   para cada renglon */
		var check = document.createElement("input");
		check.setAttribute("name"     , grid + "_Check_" + rows);
		check.setAttribute("id"       , grid + "_Check_" + rows);
		check.setAttribute("type"     , "checkbox");
		check.setAttribute("onclick"  , "fnc_gridExcelCheckClicked(this, '" + grid + "', " + rows + ")");
		baseCell.appendChild(check);
	}
	else{
		/* Si no es multiselect, se agrega la imagen del boton que 
		   permite seleccionar un renglon a la vez */
		baseCell.setAttribute("class"  , "btnLateral");
		baseCell.setAttribute("onclick", "fnc_gridExcelSelectRow('" + grid + "', " + rows + " )");
		baseCell.innerHTML = "&nbsp;";
	}
	baseCell.appendChild(status); //El status se agrega independientemente del tipo de columna base
	
	/* Crear las celdas de los componentes */
	for(var i=1; i<=cols; i++){
		var newCell = newRow.insertCell(-1);
		var temp    = document.createElement("input");
		temp.setAttribute("name"     , grid + "_Cell_" + rows + "_" + i);
		temp.setAttribute("id"       , grid + "_Cell_" + rows + "_" + i);
		temp.setAttribute("type"     , "text");
		temp.setAttribute("width"    , widths[i-1]);
		temp.setAttribute("value"    , "");
		temp.setAttribute("style"    , "text-align:" + aligns[i-1]);
		temp.setAttribute("onkeyup"  , "fnc_grdExcelCellListener(event, '" + grid + "', " + rows + ", " + i + ");");
		temp.setAttribute("onfocus"  , "fnc_gridExcelSetFocus(this, '" + grid + "', " + rows + ", " + i + ");");
		
		var onblur = blurActions[i-1]; /* EVENTO ONBLUR DEFINIDO POR EL USUARIO */
		if(onblur != ""){
			onblur = onblur.replace(/@row/gi, rows);
			temp.setAttribute("onblur", onblur);
		}
		var onchange = changeActions[i-1]; /* EVENTO ONCHANGE DEFINIDO POR EL USUARIO */
		if(onchange != ""){
			onchange = onchange.replace(/@row/gi, rows);
			temp.setAttribute("onchange", onchange);
		}
		var onclick = clickActions[i-1]; /* EVENTO ONCLICK DEFINIDO POR EL USUARIO */
		if(onclick != ""){
			onclick = onclick.replace(/@row/gi, rows);
			temp.setAttribute("onclick", onclick);
		}
		if(readOnly[i-1]){ /* READONLY DEFINIDO POR EL USUARIO */
			temp.setAttribute("readonly", "readonly");
			temp.setAttribute("style",    "background-color:#DDDDDD;");
		}
		newCell.appendChild(temp);
		
		/* Agregarle hotkey ne caso de que lleve la columna*/
		var hotkey = hotkeys[i-1];
		var action = hkActions[i-1]
		if(hotkey != ""){
			action = action.replace(/@row/gi, rows);
			eval("$('#" + grid + "_Cell_" + rows + "_" + i+ "').bind('keypress', hotkey, function(evt){ " + action + ";  return false; });");
		}
	}
	/* Agregar el boton eliminar al grid */
	if(btnDelete){
		var newCell = newRow.insertCell(-1);
		var span = document.createElement("span");
		var btn = document.createElement("button");
		span.setAttribute("class", "btnEliminar");
		span.innerHTML = "&nbsp;"
		btn.setAttribute("type"   , "button");
		btn.setAttribute("style"  , "background-color:transparent; border:none;");
		btn.setAttribute("onclick", "fnc_gridExcelDeleteRow('" + grid + "', '" + rows + "')");
		btn.appendChild(span);
		newCell.appendChild(btn);
	}
	/* Aumentar el numero de filas */
	eval(grid + "_rows = " + rows);
}

function fnc_gridExcelSetFocus(comp, grid, row, col)
{
	lastFocus = comp;
	lastCellRow = row;
	lastCellCol = col;
	comp.select();
}

function fnc_gridExcelSelectRow(grid, row)
{
	$x('tr_' + grid + '_' + row).style.backgroundColor = colorSelected;
	if(lastGrid != '' && lastRow != row)
		$x('tr_' + lastGrid + '_' + lastRow).style.backgroundColor = colorNoSelected;
	lastGrid = grid;
	lastRow = row;
}

function fnc_gridExcelMoneyFormat(money, decimals, decimal_sep, thousands_sep, moneyChar)
{    
	var n = money,   
	c = isNaN(decimals) ? 2 : Math.abs(decimals), //Default 2
	d = decimal_sep || ',', //Default ','
	t = (typeof thousands_sep === 'undefined') ? '.' : thousands_sep, //Default '.'
	sign = (n < 0) ? '-' : '',   //extraer el signo del numero
	i = parseInt(n = Math.abs(n).toFixed(c)) + '',    j = ((j = i.length) > 3) ? j % 3 : 0;    
	return sign + moneyChar + (j ? i.substr(0, j) + t : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : ''); 
}

function fnc_gridExcelDeleteFormat(grid, row, col)
{
	var formats		= eval(grid + "_formats");
	var moneyChar	= eval(grid + '_moneyChar');
	var thousandSep	= eval(grid + '_thousandSep');
	var comp		= $x(grid + '_Cell_' + row + '_' + col);
	if(comp.value != ''){
		switch(formats[col-1]){
			case 'money':
				var val = comp.value.replace(moneyChar, ''); //Eliminar el caracter de moneda
				while(val.indexOf(thousandSep) > 0) val = val.replace(thousandSep, ''); //Eliminar el separador de miles
				comp.value = val;
				break;
		}
	}
}

function fnc_gridExcelValidate(grid)
{
	var cols		= eval(grid + "_cols");
	var rows		= eval(grid + "_rows");
	var names		= eval(grid + "_names");
	var formats		= eval(grid + "_formats");
	var required	= eval(grid + "_requieredField");
	var allRight	= true;
	for(var i=1; i<=rows; i++){ //Recorrer Filas
		if(fnc_gridExcelIsRowVisible(grid, i)){ //Comprobar que no este eliminado el renglon o que sea visible
			for(var j=1; j<=cols; j++){ //Recorrer Columnas
				var comp = $x(grid + '_Cell_' + i + '_' + j);
				if(comp.value != ''){
					/* se prepara el mensaje del formato */
					var msg 		= eval(grid + '_formatMessage');
					var moneyChar	= eval(grid + '_moneyChar');
					var decimals	= eval(grid + '_decimals');
					var decimalSep	= eval(grid + '_decimalSep');
					var thousandSep	= eval(grid + '_thousandSep');
					msg = msg.replace('{0}', names[j-1]);
					
					switch(formats[j-1]){
						case 'text': break;
						case 'integer':
							if(comp.value != ''){
								if(isNaN(comp.value)) { inlineMsg(comp.id, msg, 2, false); return false; }
								else{
									if(comp.value%1!=0) { inlineMsg(comp.id, msg, 2, false); return false; }
									else comp.value = comp.value*1;
								}
							}
							else{ inlineMsg(comp.id, msg, 2, false); return false;}
							break;
						case 'float':
							if(isNaN(comp.value)) { inlineMsg(comp.id, msg, 2, false); return false;}
							break;
						case 'money':
							var val = comp.value.replace(moneyChar, ''); //Eliminar el caracter de moneda
							while(val.indexOf(thousandSep) > 0) val = val.replace(thousandSep, ''); //Eliminar el separador de miles
							if(isNaN(val)) { inlineMsg(comp.id, msg, 2, false); return false; }
							//else comp.value = fnc_gridExcelMoneyFormat(val, decimals, decimalSep, thousandSep, moneyChar);
							break;
						case 'dateDMY':
							var ar = comp.value.split("/");
							if(ar.length > 2){
								var dia = ar[0];
								var mes = ar[1];
								var anio = ar[2];
								if(!fnc_gridExcelCheckDate(comp, dia, mes, anio)) { inlineMsg(comp.id, msg, 2, false); return false; }
							}
							else { inlineMsg(comp.id, msg, 2, false); return false; }
							break;
						case 'dateMDY':
							var ar = comp.value.split("/");
							if(ar.length > 2){
								var mes = ar[0];
								var dia = ar[1];
								var anio = ar[2];
								if(!fnc_gridExcelCheckDate(comp, dia, mes, anio)) { inlineMsg(comp.id, msg, 2, false); return false; }
							}
							else { inlineMsg(comp.id, msg, 2, false); return false; }
							break;
					}
				} // Fin del if comp.value != ''
				else{
					if(required[i-1]){
						var msg	= eval(grid + '_requiredMessage');
						msg		= msg.replace('{0}', names[i-1]);
						inlineMsg(comp.id, msg, 2, false);
						return false; 
					}
				} //Fin else if comp.value != ''
			}//Fin for Columnas
		}//Fin if visible
	}//Fin for Filas
	return true;
}
		
function fnc_gridExcelCheckDate(comp, dia, mes, anio)
{
	if(isNaN(dia) || isNaN(mes) || isNaN(anio)) return false;
	else{
		if( (anio>1900 && anio<5000) && (mes>0&&mes<13) ){
			switch(mes){
				case 1: case 3: case 5: case 7: case 8: case 10: case 12:
					if(dia<32 && dia>0) break; //entre 1 y 31
					else return false;
				case 4: case 6: case 9: case 11:
					if(dia<31 && dia>0) break; //entre 1 y 30
					else return false;
				case 2:
					if(fnc_gridExcelIsleap(anio)){
						if(dia<30 && dia>0) break; // de 1 a 29
						else return false;
					}
					else{
						if(dia<29 && dia>0) break; // de 1 a 30
						else return false;
					}
			}
		}
		else return false;
	}
	return true;
}

function fnc_gridExcelIsleap(anio)
{
	var leap; 
	if(parseInt(anio)%4==0){ 
		if(parseInt(anio)%100==0){ 
			if(parseInt(anio)%400==0) leap=true; 
			else leap=false; 
		} 
		else leap=true; 
	} 
	else leap=false; 
	return leap; 
}

function fnc_gridExcelCheckClicked(comp, grid, row)
{
	if(comp.checked) $x('tr_' + grid + '_' + row).style.backgroundColor = colorSelected;
	else $x('tr_' + grid + '_' + row).style.backgroundColor = colorNoSelected;
}

function fnc_gridExcelCheckAll(comp, grid)
{
	var checked = comp.checked;
	var rows = eval(grid + "_rows");
	for(var i=1; i<=rows; i++) {
		if(fnc_gridExcelIsRowVisible(grid, i)){
			var temp = $x(grid + '_Check_' + i);
			temp.checked = checked;
			fnc_gridExcelCheckClicked(temp, grid, i);
		}
	}
}

function fnc_gridExcelDeleteRow(grid, row)
{
	var multiSelect = eval(grid + "_multiSelect");
	$x('tr_' + grid + '_' + row).style.display = "none";
	$x(grid + '_Status_' + row).value = "0";
	if(multiSelect) $x(grid + '_Check_' + row).checked = false;
	
	var onDeleteRow = eval(grid + "_onDeleteRow");
	if(onDeleteRow != "void(0);"){
		onDeleteRow = onDeleteRow.replace(/@row/gi, row);
		eval(onDeleteRow);
	}
}

function fnc_gridExcelSetValue(grid, row, col, val)
{
	try{ $x(grid + '_Cell_' + row + '_' + col).value = val; return true; }
	catch(e){ return false; }
}

function fnc_gridExcelGetValue(grid, row, col)
{
	try{ return $x(grid + '_Cell_' + row + '_' + col).value; }
	catch(e){ return null; }
}

function fnc_gridExcelGetControlId(grid, row, col)
{
	try{ return $x(grid + '_Cell_' + row + '_' + col).id; }
	catch(e){ return null; }
}

function fnc_gridExcelGetRowStatus(grid, row)
{
	try{ 
		var status = $x(grid + '_Status_' + row).value; 
		if(status == '0') return 'Eliminado';
		if(status == '1') return 'Nuevo';
		if(status == '2') return 'Precargado';
		return 'Desconocido';
	}
	catch(e){ return 'Desconocido'; }
}

function fnc_gridExcelGetRow(grid, row)
{
	var cols = eval(grid + "_cols");
	var data = new Array(cols);
	for(var i=1; i<=cols; i++){
		data[i-1] = $x(grid + '_Cell_' + row + '_' + i).value;
	}
	return data;
}

function fnc_gridExcelGetSelectedRow(grid)
{
	var rows           = eval(grid + "_rows");
	var cols           = eval(grid + "_cols");	
	var multiSelect    = eval(grid + "_multiSelect");
	var data           = new Array();
	var totalRenglones = 0;
	for(var i=1; i<=rows; i++){
		var temp = $x(grid + '_Check_' + i);
		if(temp.checked) {
			var renglon = new Array(cols);
			for(var j=1; j<=cols; j++){
				renglon[j-1] = $x(grid + '_Cell_' + i + '_' + j).value;
			}
			data[totalRenglones++] = renglon;
		}
	}
	return data;
}

function fnc_gridExcelFilter(grid, value, column, e)
{
	var rows     = eval(grid + "_rows");
	var cols     = eval(grid + "_cols");
	var firstRow = 0;
	if(!value)  value  = $x(grid + '_filterText').value;
	if(!column) column = $x(grid + '_filterOption').value;
	value = value.toUpperCase();
	
	for(var i=1; i<=rows; i++){ //Renglones
		var status = $x(grid + '_Status_' + i).value;
		if(status > 0){ //Se comprueba que el renglon no sea uno elliminado
			if(column == 0){ //NO se especifico columna
				var existValue = false;
				for(var j=1; j<=cols; j++){ //Columnas
					var contenido = $x(grid + '_Cell_' + i + '_' + j).value.toUpperCase();
					if(contenido.search(value) >= 0){
						existValue = true;
						break;
					}
				}
				if(existValue) {
					fnc_gridExcelShowRow(grid, i);
					if(firstRow == 0) firstRow = i;
				}
				else fnc_gridExcelHideRow(grid, i);
			}
			else{ //SI se especifico una columna
				var contenido = $x(grid + '_Cell_' + i + '_' + column).value.toUpperCase();
				if(contenido.search(value) >= 0) {
					fnc_gridExcelShowRow(grid, i);
					if(firstRow == 0) firstRow = i;
				}
				else fnc_gridExcelHideRow(grid, i);
			}
		}// if status
	}//for renglones
}

function fnc_gridExcelHideRow(grid, row)
{
	$x('tr_' + grid + '_' + row).style.display = "none";
}

function fnc_gridExcelShowRow(grid, row)
{
	$x('tr_' + grid + '_' + row).style.display = "block";
}

function fnc_gridExcelIsRowVisible(grid, row)
{
	return 	$x('tr_' + grid + '_' + row).style.display != "none";
}
