var redalert_agregado = false;
var redalert_respuesta = false;

function mostrarAlert(titulo, texto){
	if(!redalert_agregado){
		var div = document.createElement("div");
		div.setAttribute("id",    "redAlertDiv");
		div.setAttribute("style", "display:none;");
		div.innerHTML = "<span class='ui-icon ui-icon-alert' style='float:left; margin:0 7px 20px 0;'></span><span id='redAlertText'></span>";
		document.appendChild(div);
		redalert_agregado = true;
	}
	document.getElementById("redAlertDiv").title = titulo;
	document.getElementById("redAlertText").innerHTML = texto;
	$( "#redAlertDiv" ).dialog({
		resizable: false,
		modal: false,
		autoOpen: true,
		buttons: {
			"Aceptar": function() {
				$( this ).dialog( "close" );
			}
		}
	});
}

function mostrarConfirm(titulo, texto, ventana){
	if(ventana == null){
		if(!redalert_agregado){
			var div = document.createElement("div");
			div.setAttribute("id",    "redAlertDiv");
			div.setAttribute("style", "display:none;");
			div.innerHTML = "<span class='ui-icon ui-icon-alert' style='float:left; margin:0 7px 20px 0;'></span><span id='redAlertText'></span>";
			document.appendChild(div);
			redalert_agregado = true;
		}
		document.getElementById("redAlertDiv").title = titulo;
		document.getElementById("redAlertText").innerHTML = texto;
		var res = $( "#redAlertDiv" ).dialog({
			resizable: false,
			modal: false,
			autoOpen: true,
			buttons: {
				"Si": function() {
					$( this ).dialog( "close" );
					redalert_respuesta = true;
				},
				"No": function() {
					$( this ).dialog( "close" );
					redalert_respuesta = false;
				}
			}
		});
		return mostrarConfirm('', '', res);
	}
	else{
		if(ventana.dialog("isOpen")){
			for(var i=0; i<2000; i++){ i++;}
			return mostrarConfirm('', '', ventana);
		}
		else return redalert_respuesta;
	}
}