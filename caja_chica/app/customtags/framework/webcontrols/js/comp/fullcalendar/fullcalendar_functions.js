// JavaScript Document
//--------------------------------------------------------------------------------------//
function myFullCalendar(){
	//this.fnc_refetchEvents=fnc_refetchEvents();
	//this.mensaje_al_usuario=mensaje_al_usuario();
}
//--------------------------------------------------------------------------------------//
//Funcion que obtiene las columnas de un renglon especificado en los parametros del control jqGrid
//Parametros:
//	calendar_name='nombre del control fullCaledar'
function fnc_refetchEvents(calendar_name){
	$("#"+calendar_name).fullCalendar('refetchEvents');
	}
//--------------------------------------------------------------------------------------//
//Funcion para mostrar un mensaje al usuario en una ventana modal
//Parametros:
//	dialog_name: 'Nombre del dialogo a mostrar'
//	message: 'Mensaje a mostrar'
function mensaje_al_usuario(dialog_name, message){
	fnc_setMessage(dialog_name, message);
	fnc_openDialog(dialog_name);

	return true;
}