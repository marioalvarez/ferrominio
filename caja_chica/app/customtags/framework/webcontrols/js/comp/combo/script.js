(function ($) {
    $.widget("ui.combobox", {
        _create: function () {
            var self = this,
                select = this.element,
                selected = select.children(":selected"),
                value = selected.val() ? selected.text() : "";
            var input = this.input = $("<input>").insertAfter(select).val(value).autocomplete({
                delay: 0,
                minLength: 0,
                source: function (request, response) {
                    var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
                    response(select.children("option").map(function () {
                        var text = $(this).text();
                        if (this.value && (!request.term || matcher.test(text))) return {
                            label: text.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + $.ui.autocomplete.escapeRegex(request.term) + ")(?![^<>]*>)(?![^&;]+;)", "gi"), "<strong>$1</strong>"),
                            value: text,
                            option: this
                        }
                    }))
                },
                select: function (event, ui) {
                    ui.item.option.selected = true;
                    self._trigger("selected", event, {
                        item: ui.item.option
                    })
                },
                change: function (event, ui) {
                    if (!ui.item) {
                        var matcher = new RegExp("^" + $.ui.autocomplete.escapeRegex($(this).val()) + "$", "i"),
                            valid = false;
                        select.children("option").each(function () {
                            if ($(this).text().match(matcher)) {
                                this.selected = valid = true;
                                return false
                            }
                        });
                        if (!valid) {
                            $(this).val(selected.text());
                            select.val(selected.text());
                            input.data("autocomplete").term = selected.text();
                            return false
                        }
                    }
                }
            }).addClass("ui-widget ui-widget-content ui-corner-left");
            input.data("autocomplete")._renderItem = function (ul, item) {
                return $("<li></li>").data("item.autocomplete", item).append("<a>" + item.label + "</a>").appendTo(ul)
            };
			input.keydown(escanear_enter); //funciones.js
            this.button = $("<button type='button'>&nbsp;</button>").attr("tabIndex", -1).attr("title", "Mostrar todos los elementos").insertAfter(input).button({
                icons: {
                    primary: "ui-icon-triangle-1-s"
                },
                text: false
            }).removeClass("ui-corner-all").addClass("ui-corner-right ui-button-icon").click(function () {
                if (input.autocomplete("widget").is(":visible")) {
                    input.autocomplete("close");
                    return
                }
                input.autocomplete("search", "");
                input.focus()
            })
        },
        destroy: function () {
            this.input.remove();
            this.button.remove();
            this.element.show();
            $.Widget.prototype.destroy.call(this)
        }
    })
})(jQuery);