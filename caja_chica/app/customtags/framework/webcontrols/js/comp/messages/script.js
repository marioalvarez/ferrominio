// START OF MESSAGE SCRIPT //
var MSGTIMER = 20;
var MSGSPEED = 5;
var MSGOFFSET = 3;
var MSGHIDE = 3;

// build out the divs, set attributes and call the fade function //
function inlineMsg(target,string,autohide,nofocus) {
	try{
	  var msg;
	  var msgcontent;
	  if(!document.getElementById('msg')) {
		msg = document.createElement('div');
		msg.id = 'msg';
		msgcontent = document.createElement('div');
		msgcontent.id = 'msgcontent';
		document.body.appendChild(msg);
		msg.appendChild(msgcontent);
		msg.style.filter = 'alpha(opacity=0)';
		msg.style.opacity = 0;
		msg.alpha = 0;
	  } else {
		msg = document.getElementById('msg');
		msgcontent = document.getElementById('msgcontent');
	  }
	  msgcontent.innerHTML = string;
	  msg.style.display = 'block';
	  var msgheight = msg.offsetHeight;
	  var targetdiv;
	  var targetheight;
	  var targetwidth;
	  var topposition;
	  var leftposition;
	  
	  if(document.getElementById(target)!=null) targetdiv = document.getElementById(target);
	  else targetdiv = document.getElementsByName(target)[0];

	  try {
		  targetdiv.focus();
		  targetheight = targetdiv.offsetHeight;
	  	  targetwidth = targetdiv.offsetWidth;
	  	  topposition = topPosition(targetdiv) - ((msgheight - targetheight) / 2);
	   	  leftposition = leftPosition(targetdiv) + targetwidth + MSGOFFSET;
	  }
	  catch(e){
		  targetheight = -5;
	  	  targetwidth = 160;
	  	  topposition = topPosition(targetdiv) - ((msgheight - targetheight) / 2);
	   	  leftposition = leftPosition(targetdiv) + targetwidth + MSGOFFSET;
				/*Modificacion HGB: cuando un campo est� oculto se muestra cortado el mensaje*/
				topposition= 5;
				leftposition= 5;
					
	  }
	  msg.style.top = topposition + 'px';
	  msg.style.left = leftposition + 'px';
	  clearInterval(msg.timer);
	  msg.timer = setInterval("fadeMsg(1)", MSGTIMER);
	  if(!autohide) {
		autohide = MSGHIDE;  
	  }
	  window.setTimeout("hideMsg()", (autohide * 1000));
	}
	catch(e){}
}

// hide the form alert //
function hideMsg(msg) {
  var msg = document.getElementById('msg');
  if(!msg.timer) {
    msg.timer = setInterval("fadeMsg(0)", MSGTIMER);
  }
}

// face the message box //
function fadeMsg(flag) {
  if(flag == null) {
    flag = 1;
  }
  var msg = document.getElementById('msg');
  var value;
  if(flag == 1) {
    value = msg.alpha + MSGSPEED;
  } else {
    value = msg.alpha - MSGSPEED;
  }
  msg.alpha = value;
  msg.style.opacity = (value / 100);
  msg.style.filter = 'alpha(opacity=' + value + ')';
  if(value >= 99) {
    clearInterval(msg.timer);
    msg.timer = null;
  } else if(value <= 1) {
    msg.style.display = "none";
    clearInterval(msg.timer);
  }
}

// calculate the position of the element in relation to the left of the browser //
function leftPosition(target) {
  var left = 0;
  if(target.offsetParent) {
    while(1) {
      left += target.offsetLeft;
      if(!target.offsetParent) {
        break;
      }
      target = target.offsetParent;
    }
  } else if(target.x) {
    left += target.x;
  }
  return left;
}

// calculate the position of the element in relation to the top of the browser window //
function topPosition(target) {
  var top = 0;
  if(target.offsetParent) {
    while(1) {
      top += target.offsetTop;
      if(!target.offsetParent) {
        break;
      }
      target = target.offsetParent;
    }
  } else if(target.y) {
    top += target.y;
  }
  return top;
}

// preload the arrow //
if(document.images) {
  arrow = new Image(7,80); 
  arrow.src = "images/msg_arrow.gif"; 
}