(function(){

	var app = angular.module('app', []);
	
	app.controller('appController', ['$scope', '$http', '$compile', function( $scope, $http, $compile ){
		
		$scope.main = {
			_url: '',
			path: '',
			url: '',
			load: function( url, addLastPath ){
				this._url = !addLastPath ? url : this.path+url;
				url += url.indexOf('?') != -1 ? '&hash':'?hash';
				url += f.folgen();
				this.url = !addLastPath ? url : this.path+url;
				
				var path = url.split('/');
				var pathString = '';
				for( var i = 0; i < path.length -1; i++ )
					pathString += path[i]+'/';
				this.path = pathString;
				
				f.ajax.lastUrl = this._url;
				f.ajax.path = this.path;

				layout.mainIFrame.hide();

				if( layout.mainLoader )
					layout.mainLoader.show();

				layout.rightPannel.find('#loadingFrame').show();
				layout.rightPannel.find('#loadingMain').css({ display: 'table' });
			},
			reload: function(){ this.url = this._url+f.folgen(); },
			scan: function(){ f.main.scan(); $scope.afterLoad() },
			succesfullMessage: 'Se realiz\xF3 operaci\xF3n correctamente'
		};
		
		$scope.mainIFrame = {
			src: '',
			setSrc: function( url ){
				url += url.indexOf('?') != -1 ? '&hash':'?hash';
				this.src = url+f.folgen();
				layout.mainIFrame.show();
				if( layout.mainLoader )
					layout.mainLoader.hide();
				layout.toolbar.html('');
				layout.rightPannel.find('#loadingFrame').show();
				layout.rightPannel.find('#loadingMain').css({ display: 'table' });
			}
		};
		
		$scope.init = function(){
			this.header = 'templates/header.cfm?hash'+f.folgen();
			this.menuOptions = 'templates/menu-options.html';
			$scope.menu = 'templates/menu.cfm?'+f.folgen();
			$scope.mainIFrame.src = 'app/admin_obra/bienvenido.cfm';
		};
		
		$scope.afterSubmit = function(){
			layout.whiteFrame.hide();
			layout.loading.hide();
			var submitterBody = $(window.frames['Submitter'].document.getElementsByTagName("body")[0]);
			
			try{
				var jsonData = eval( "(" + submitterBody.html() + ")" );

				if( jsonData.error ){
					message.error(jsonData.message);
				} else {
					if( f.main.doReload ) this.main.reload();
					this.$apply(function(){ ; });
					//message.successfull();
					f.main.doReload = false;
				}
			} catch( error ) {
				// Prevent the eval error when is created
				if( $('#Submitter').attr('isCreated') == 'true' ){
					$('#Submitter').attr('isCreated', '');
					return false;
				}
				console.log(error);
				layout.mainLoader.html($(window.frames['Submitter'].document.getElementsByTagName("html")[0]).html());
				$scope.afterLoad();
				return;
			}
			// Remove submiter to prevent resend data.
			$('#Submitter').remove();
			// Create the new Submitter
			var newSubmitter = angular.element('<iframe name="Submitter" id="Submitter" class="hidden" ng-load="afterSubmit()" src="" isCreated="true"></iframe>');
			$('#SubmitterCont').append(newSubmitter);
			$compile(newSubmitter)($scope);
		};
		
		$scope.afterLoad = function(){
			layout.rightPannel.find('#loadingFrame').hide();
			layout.rightPannel.find('#loadingMain').hide();
			layout.submittingFrame.hide();
			layout.submittingMain.hide();
		}
		
		$scope.folgen = function(){
			return f.folgen();
		};
		
		$scope.showMenu = function( hardHide ){
			if( hardHide ){
				layout.appToolbar.find('#MenuOptions').css({ display: 'table-cell' });
				layout.rightPannel.css({ left: 0 });
				return false;
			}
			layout.appToolbar.find('#MenuOptions').attr('style', 'width: 30px;');
			layout.rightPannel.attr('style', '');
			if( layout.leftPannel.is(':visible') ){
				layout.appToolbar.find('#menuFrame').attr('style', '');
				layout.leftPannel.attr('style', '');
			} else {
				layout.leftPannel.show().css({ width: '80%', zIndex: 2 });
				layout.appToolbar.find('#menuFrame').show();
			}
		};

	}]);

	// This directive is for onload binding
	app.directive('ngLoad', [function(){
		return {
			scope: {
				callBack: '&ngLoad'
			},
			link: function(scope, element, attr){
				element.on('load', function(){
					return scope.callBack();
				});
			}
		}
	}]);

	app.directive('chosen', function($timeout){
		return {
			restrict: 'A',
			link: function( scope, element, attr ){

				// update the select when the model changes
				scope.$watch(attr.ngModel, function(oldVal, newVal) {
					element.trigger('chosen:updated');
					//$timeout( function(){ element.trigger('chosen:updated'); }, 100 );
				});

				// update the select when data is loaded
				scope.$watch(attr.chosen, function(oldVal, newVal) {
					element.trigger('chosen:updated');
					$timeout( function(){ element.trigger('chosen:updated'); }, 300 );
				});

				var options = {};
				if( attr.width && attr.width != '' )
					options.width = attr.width;
					
				element.chosen(options);

				$timeout( function(){ element.trigger('chosen:updated'); }, 500 );
			}
		};
	});

	app.directive('date', function() {
		return {
			require: 'ngModel',
			//scope:{ ngModel: '=?' },
			link: function(scope, element, attr, ngModel) {
				
				if( attr.width )$(element).width(attr.width);
				
				$(element).datepicker({
					format: "yyyy/mm/dd",
					todayBtn: true,
					startDate: attr.minDate,
					endDate: attr.maxDate,
					language: "es",
					autoclose: true,
					todayHighlight: true
				});

				scope.$watch(attr.ngModel, function( value ){
					$(element).datepicker('update');
				});

				// Listen element for ngModel
				$(element).change(function(){
					var value = $(this).val();
					scope.$apply(function(fn) {
						ngModel.$setViewValue(value);
					});
				});

				if( attr.ngMaxDate ){
					scope.$watch(attr.ngMaxDate, function(value){
						$(element).datepicker('setEndDate', value);
						var dateModel = new Date(ngModel.$viewValue);
						var dateValue = new Date(value);
						if( dateValue < dateModel ){
							ngModel.$setViewValue('');
							$(element).val('');
						}
					});
				}

				if( attr.ngMinDate ){
					scope.$watch(attr.ngMinDate, function(value){
						$(element).datepicker('setStartDate', value);
						var dateModel = new Date(ngModel.$viewValue);
						var dateValue = new Date(value);
						if( dateValue > dateModel ){
							ngModel.$setViewValue('');
							$(element).val('');
						}
					});
				}

				// Remove Expresion >>>>>>>>>>>>>>>>>>>>
				/*if( $(element).attr('ng-min-date') ){
					var minDate = $(element).attr('ng-min-date').replace(/{*//*,'');
					minDate = minDate.replace(/}+/,'');
					scope.$watch(minDate, function(value){
						$(element).datepicker('setStartDate', value);
						var start = new Date(value), end = new Date(ngModel.$viewValue);
						if( start > end ){
							$(element).val('');
							ngModel.$setViewValue('');
						}
					});
				}*/
			}
		};
	});

	app.directive('file', [function( $compile ){
		return {
			restrict: 'E',
			templateUrl: 'templates/file.html?hash'+f.folgen(),
			link: function(scope, element, attr, ngModel){
				// Set name to file input
				$(element).find('#file').attr('name', attr.name);
				$(element).find('.pick').attr('id', attr.name);
				// Get file path from value attribute
				if( attr.value ){
					attr.$observe('value', function(value){
						if( value && value.trim() != '' ){
							$(element).find('#download').show();
							if( !attr.download )
								$(element).find('#delete').show();
							$(element).find('#lastFilePath')
								.val(value);
						} else {
							$(element).find('#download').hide();
							$(element).find('#delete').hide();
						}
					});
				} else {
					$(element).find('#download').hide();
					$(element).find('#delete').hide();
				}
				// Hidden input for last file path
				$(element).find('#lastFilePath')
					.attr('name', 'h'+attr.name);
				// Hide button picker and delete when only download
				if( attr.download ){
					$(element).find('.pick').hide();
					$(element).find('#delete').hide();
				}
				// Set name to hidden input for the folfile
				$(element).find('#folfile')
					.attr('name', 'f'+attr.name)
					.val(f.folgen());
				// Open file browser when click's button
				$(element).find('.pick').click(function(){
					$(element).find('#file')[0].click();
				});
				// Set the file's name to show on button
				$(element).find('#file').change(function(){
					var fullPath = $(this).val();
					var inputModel = $(this).parent().find('#model').val(fullPath).trigger('input');
					//$(this).parent().find('#model').trigger($(this).parent().find('#model'));

					if (fullPath) {
						var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
						var filename = fullPath.substring(startIndex);
						if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
							filename = filename.substring(1);
						}
						$(element).find('label').text(filename);
					} else {
						$(element).find('label').text('Seleccione archivo...');
					}
				});
				$(element).find('#download').click(function(){
					window.open('app/upload/download/download.cfm?ruta='+attr.value);
				});
				$(element).find('#delete').click(function(){
					var message = 'Está seguro que desea eliminar el archivo?';
					if( confirm(message) ){
						$(element).find('#download').hide();
						$(element).find('#delete').hide();
						$(element).find('#lastFilePath').val('');
					}
				});
			},
			//replace: true,
			scope: { ngModel : '=', format: '@' }
		};
	}]);

	app.directive('float', function(){
		return {
			restrict: 'A',
			require: 'ngModel',
			link: function( scope, elem, attr, ngModel ){

				ngModel.$parsers.push(function (inputValue) {
					// Break when is empty
					if (inputValue == undefined || inputValue == '') return '';
					var lastChar = inputValue[ inputValue.length -1 ];
					var newValue = inputValue;
					if( isNaN( lastChar ) && lastChar != '.' ){
						newValue = inputValue.slice(0,-1);
					} else if( inputValue.indexOf('.') != -1 ){
						if( lastChar == '.' && inputValue.split('.').length > 2 )
							newValue = inputValue.slice(0,-1);
						else if( inputValue.split('.')[0].length == 0 ){
							newValue = '0' + inputValue;
						} else{
							var position =  attr.float != '' && !isNaN(attr.float) ? attr.float : 2;
							if( inputValue.split('.')[1].length > position )
								newValue = inputValue.slice(0,-1);
						}
					}

					if( newValue != inputValue ){
						ngModel.$setViewValue(newValue);
						ngModel.$render();
					}

					return newValue;
				});
			}
		};
	});

	app.directive('numeric', function(){
		return { // By: https://twitter.com/oskar
			require: 'ngModel',
			link: function(scope, element, attr, modelCtrl) {
				modelCtrl.$parsers.push(function (inputValue) {
					// this next if is necessary for when using ng-required on your input. 
					// In such cases, when a letter is typed first, this parser will be called
					// again, and the 2nd time, the value will be undefined
					if (inputValue == undefined) return '';
					var transformedInput = inputValue.replace(/[^0-9]/g, ''); 
					if (transformedInput!=inputValue) {
						modelCtrl.$setViewValue(transformedInput);
						modelCtrl.$render();
					}         

					return transformedInput;         
				});
			}
		};
	});

	app.factory('formDataObject', function() {
		return function(data) {
			var fd = new FormData();
			angular.forEach(data, function(value, key) {
				fd.append(key, value);
			});
			return fd;
		};
	});

	app.service('sharedObject', function () {
		var property = 'First';

		return {
			get: function () {
				return property;
			},
			set: function(value) {
				property = value;
			}
		};
	});

	layout = {
		appToolbar: $('#AppToolbar'),
		header: $('#Header'),
		submitter: $('#Submitter'),
		leftPannel: $('#LeftPannel'),
		loading: $('#loading'),
		menu: $('#AppMenu'),
		main: $('#AppMain'),
		mainIFrame: $('#MainIFrame'),
		mainLoader: null,
		options: null,
		rightPannel: $('#RightPannel'),
		submittingFrame: $('#submittingFrame'),
		submittingMain: $('#submittingMain'),
		toolbar: $('#Toolbar'),
		whiteFrame: $('#white-frame')
	};
})();
var layout;