var f = {};

(function(){

	f.ajax = {
		lastUrl: '',
		path: ''
	};

	f.angularCompile = function( element ){
		angular.element(document).injector().invoke(function($compile){
			var scope = angular.element(document).scope();
			$compile(element[0])(scope);
			if(!scope.$$phase) scope.$apply();
		});
	};

	f.cfJson = function( component, method, params ){
		var result = null; // SERÁ NUESTRO OBJETO DE RETORNO
		$.ajax({ // LLAMADA AJAX JQUERY
			url: component+'?method='+method+'&ReturnFormat=json&hash'+f.folgen() //&'+params // URL COMPONENTE, METODO, FORMATO, Y PARAMETROS
			,dataType: 'json' // TIPO DE DATOS DE LECTURA (AQUÍ INDICA QUE SE LEERÁ EN FORMATO JSON)
			,data: params
			,async : false // SE DEFINE LA LLAMADA AJAX DE MODO SINCRONO PARA QUE SE RETORNE EL RESULTADO
			,success: function(data){ // CACHAR DATOS DE LECTURA

				var obj = new Array(); // ARRAY DE OBJETOS JSON
				for( var i = 0; data.RS && i < data.RS.DATA.length; i++ ){ // RECORRER EL ARREGLO DE DATOS
					var strObj = '{ '; // STRING QUE CONTENDRÁ ESTRUCTURA DE UN JSON
					for( var j = 0; j < data.RS.COLUMNS.length; j++ ){ // RECORRER LAS COLUMNAS
						data.RS.DATA[i][j] = data.RS.DATA[i][j] == null ? '' : data.RS.DATA[i][j];
						var stringData = data.RS.DATA[i][j].toString();
						stringData = stringData.replace(/"/g, "&quot;"); // QUITAR TODAS LAS COMILLAS DOBLES PARA QUE NO GENEREN PROBLEMAS
						stringData = stringData.replace(/'/g, "&#39;"); // QUITAR TODAS LAS COMILLAS SIMPLES PARA QUE NO GENEREN PROBLEMAS
						stringData = stringData.replace(/\r?\n/g, " "); // QUITAR TODAS LOS SALTOS DE LINEA PARA QUE NO GENEREN PROBLEMAS
						// GENERAR ESTRUCTURA DE OBJETO CON EL NOMBRE DE COLUMNA Y SU DATO POR RENGLÓN
						strObj += '"'+ data.RS.COLUMNS[j].toLowerCase() +'" : "'+stringData+'", ';
					}
					var columns = ""
					for( var j = 0; j < data.RS.COLUMNS.length; j++ ){ // RECORRER LAS COLUMNAS
						columns += data.RS.COLUMNS[j].toLowerCase() +', ';
					}
					srtObj = '"columns": "'+columns+'"';
					strObj += '"_id" : "' + f.folgen() + '" }'; // ID DEL OBJETO
					var json = JSON.parse(strObj); // CONVERTIR STRING A JSON
					obj.push(json); // AGREGAR OBJETO AL FINAL DEL ARRAY PARA QUE ESTEN ODENADOS COMO LA CONSULTA
				}
				data.data = obj; // AGREGAR ARRAY DE OBETOS AL JSON DE LECTURA
				data.error = data.TIPOERROR == '' ? false : true;
				result = data; // ASIGNAR JSON DE LECTURA A OBJETO DE RETORNO
			}
			,error: function(xhr, ajaxOptions, thrownError){ // MANJADOR DE ERROR
				alert(xhr.status + ' - ' + thrownError); // MOSTRAR UN ALERT DE ERROR
			}
		});
		return result; // REGRESAR EL RESULTADO
	};

	f.errorMessage = function( message, element ){
		var time = 100 * message.length;
		$('#errorMessage').css({display: 'table'}).delay(time).fadeOut(function(){
			$('.wrong').removeClass('wrong');
		})
		$('#errorMessage div').html(message);
		if(element){
			element.addClass('wrong').focus();
		}
	};

	f.folgen = function(){
		return (new Date()).getTime();
	};
	
	f.index = function( arrayObject, selectores ){
		for (var i = 0; i < arrayObject.length; i++) {
			if( selectores ){
				for( var j = 0, id; j < selectores.length; j++, id = '' ){
					if( selectores[j][0] == '#' || selectores[j][0] == '.' ) id = selectores[j].slice(1, selectores[j].length);
					$(selectores[j]+arrayObject[i].index).attr('id', id+i);
				}
			}
			arrayObject[i].index = i;
		};
		return arrayObject;
	}

	f.load = function( url, data, element, callBack ){
		url = this.ajax.path + url;
		url += url.indexOf('?') != -1 ? '&hash':'?hash';
		url += f.folgen();

		element = element ? element : layout.main;

		$.ajax({
			url: url,
			data: data
		}).done(function( data ){
			element.html('');
			element.html(data);
			if( callBack ) callBack();
		}).error(function( xhr, txStatus, txError ){
			f.errorMessage(txError);
			layout.main.html(xhr.responseText);
		});
	};

	f.main = {};
	f.main.load = function( url, data ){
		url += url.indexOf('?') != -1 ? '&hash':'?hash';
		url += f.folgen();
		$.ajax({
			url: url,
			data: data
		}).done(function( data ){
			layout.main.html(data);
		}).error(function( xhr, txStatus, txError ){
			f.errorMessage(txError);
			layout.main.html(xhr.responseText);
		});
	}

	f.main.doReload =  false;

	f.main.submit = function( form, url, reload ){
		url += url.indexOf('?') != -1 ? '&hash'+f.folgen() : '?hash'+f.folgen();
		$(form)
			.attr("action",  f.ajax.path+url)
			.attr("method", "post")
			.attr("enctype", "multipart/form-data")
			.attr("encoding", "multipart/form-data")
			.attr("target", $('#Submitter').attr('name'));

		layout.whiteFrame.show();
		layout.submittingFrame.show();
		layout.submittingMain.css({ display: 'table' });
		this.doReload = reload;
	};

	f.main.scan = function( scope ){
		layout.mainLoader = $('#MainLoader');
		layout.toolbar.html('');
		layout.mainLoader.find('[toolbar]').appendTo(layout.toolbar);
	};

	f.rsJson = function( rs ){
		var s = rs || {};
		if( !s.COLUMNS && !s.DATA ){
			console.error("f.rsJson() >>  was not passed a coldfusion serialized object");
			return [];
		}
		//Create returned object
		var obj = [];
		//Loops through rs and matches the columns
		for(var i=0; i < s.DATA.length; i++){
			var temp = {};
			for(var j=0; j < s.COLUMNS.length; j++){
				temp[s.COLUMNS[j].toLowerCase()] = s.DATA[i][j];
			}
			// save the new row with column names
			obj.push(temp);
		}
		// Return the objects
		return obj;
	};

	f.validEmail = function( email, multiEmail ){
		/*Expresion regular para validar una direccion de correo*/
		var exreg  = /^([a-zA-Z0-9_.-])+@(([a-zA-Z0-9-])+.)+([a-zA-Z0-9]{2,4})+$/;
		
		/*Expresion regular para validar varios correos en un campo*/
		var exregvarios = /^(([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+)(([\s]*[;]+[\s]*(([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+))*)$/;
		
		if( multiEmail )
			return exregvarios.test(email);
		else
			return exreg.test(email);
	};

})();