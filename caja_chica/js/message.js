var message = {};

(function(){
	message.confirm = {};
	
	message.confirm.dlt = function(){
		return confirm( m.confirm.dlt );
	};
	message.confirm.save = function(){
		return confirm( m.confirm.save );
	};

	message.error = function( message, elem ){
		var time = 100 * message.length; // Calculate visible time
		$('#errorMessage')
			.css({display: 'table'})
			.delay(time)
			.fadeOut(function(){
				$('.wrong').removeClass('wrong');
			});
		$('#errorMessage div').html(message);
		// Set focus and remark element wrong
		if( elem ) elem.addClass('wrong').focus();
	};

	message.successfull = function(){
		var time = 2000;
		$('#mainSuccessfull').css({display: 'table'}).delay(time).fadeOut();
	};

})();
