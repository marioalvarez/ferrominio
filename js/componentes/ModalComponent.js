// JavaScript Document
var ModalComponent = function(params){
	this.name = params.name;
	this.title = params.title;
	this.url = params.url;
	this.height = params.height;
	//this.min_height = params.min_height;
	this.width = params.width;
	this.opacity = params.opacity ? params.opacity : 65;
	this.closeClass = params.closeClass ? params.closeClass : "simplemodal-close";
	this.zIndex = params.zIndex ? params.zIndex : "1000";
	this.closeOnClick = params.closeOnClick ? params.closeOnClick : false;
	this.disableEsc = params.disableEsc ? params.disableEsc : true;
	this.disableClose = params.disableClose ? params.disableClose : false;
	this.data = params.data;	
	this.events = {}
	
	scopeModal = this;
	
	
	this['OSX_'+ params.name] = {
		//scopeModal: null,
		container: null,
		init: function () {
				$("#" + scopeModal.name + "_Ventana").remove();	
				//Agregar al body la estructura del modal
				 $("body").append('<div class="modal_Ventana" id="' + scopeModal.name + '_Ventana">'+									  
									  '<div class="modal_Titulo" id="' + scopeModal.name +  '_Titulo">' + scopeModal.title + '</div>'+
									  '<div class="close"> <a href="##" class="' + scopeModal.closeClass + ' exit-modal">x</a> </div>'+
									  '<div class="modal_Contenido" id="' + scopeModal.name + '_Contenido">'+
									  	'<div id="' + scopeModal.name + '" ></div>'+
									  '</div>'+
								  '</div>');
				$("#" + scopeModal.name + "_Ventana").modal({
					overlayId: scopeModal.name + '_osx-overlay',
					containerId: scopeModal.name + '_osx-container',
					containerCss: 'osx-container',
					closeClass: scopeModal.closeClass,					
					escClose: scopeModal.disableEsc,
					/*autoResize: true,*/
					focus: true,
					zIndex: scopeModal.zIndex,
					overlayCss: {
						'background-color':'#000',
						'cursor': 'not-allowed'					
					},
					containerCss: {
						 'max-height' : scopeModal.height,
						 /*'min-width'  : scopeModal.width,*/
						 'overflow'  : 'auto'
					},  					
					/*minHeight: scopeModal.height,*/
					minWidth: scopeModal.width,
					opacity: scopeModal.opacity,
					position: ['0',],
					overlayClose: scopeModal.closeOnClick,
					onOpen: this.open,
					// onShow: OSX.onShow,
					onClose: this.close,
					scopeModal: scopeModal
				});							
		},
		open: function (d) {			
			//scopeModal = scopeModal;													
			$.ajax({
					type: 'GET',
					url: scopeModal.url,
					data: scopeModal.data,									
					cache: false,
					dataType: 'html',
					beforeSend: function (){
						
					}, 
					success:
						function(msg){
							// $("#attributes.name#_Titulo").html('CARGANDO DATOS');
							$("#" + scopeModal.name  + "_Contenido").html(msg);
							var self = this;
							self.container = d.container[0];
							d.overlay.fadeIn('fast', function () {
	
								$("#" + scopeModal.name + "_Ventana", self.container).show();
								var title = $("#" + scopeModal.name + "_Titulo", self.container);
								title.show();
								d.container.slideDown('fast', function () {
									setTimeout(function () {
										var h = $("#" + scopeModal.name + "_Contenido", self.container).height()
											+ title.height()
											+ 20; // padding
										d.container.animate(
											{height: h}, 
											200,
											function () {
												$("div.close", self.container).show();
												$("#" + scopeModal.name + "_Contenido", self.container).show();
												
											}
										);
									}, 100);
								});//Fin del fadeIn
							})
						},
					error:
						// onError de la carga ajax se maneja este metodo que hace muestra el codigo de error y mensaje retornado
						function(xhr, ajaxOptions, thrownError){
							if (xhr.status != ""){
								var msg = "<br>Ha ocurrido un error: ";
								$("#" + scopeModal.name + "").html('<div class="notification-wrap failuer" style="font-size:14px;width:70%;margin-left:auto;margin-right:auto;"><span class="icon-failure" style="width:50%;">Error:</span> '+ 
								xhr.status +' </strong><br><p>' + msg + " " + xhr.statusText + '</p></div>');
							}
						}
			})//Fin del ajax
		},//FIN OPEN		
		onShow:function(dialog){
					
		},
		close: function (d) {
			var self = this; // this = SimpleModal object	
			if(d){			
				d.container.animate(
					{top:"-" + (d.container.height() + 20)},
					500,
					function () {
						self.close(); // or $.modal.close();
						$("#" + scopeModal.name + "_Ventana").remove();		
						scopeModal.dispatchEvent("onClose");		
					}
				);
			}else{
				$("#" + scopeModal.name + "_osx-container").animate(
					{top:"-" + ($("#" + scopeModal.name + "_osx-container").height() + 20)},
					500,
					function () {						
						$("#" + scopeModal.name + "_Ventana").remove();	
						$("#" + scopeModal.name + "_osx-container").remove();	
						$("#" + scopeModal.name + "_osx-overlay").remove();	
						$("#simplemodal-placeholder").remove();							
						self.close(); // or $.modal.close();		
						scopeModal.dispatchEvent("onClose");						
					}
				);
			}						
		}
	};
	
	this.init = function(){
		var that = this;
		that['OSX_'+ that.name].init();
	}
	
	this.close = function(){	
		var that = this;
		that['OSX_'+ that.name].close();		
	}
}

ModalComponent.prototype.addEventListener = function (key, func) {
	if (!this.events.hasOwnProperty(key)) {
		this.events[key] = [];
	}
	this.events[key].push(func);
};

ModalComponent.prototype.removeEventListener = function (key, func) {
	if (this.events.hasOwnProperty(key)) {
		for (var i in this.events[key]) {
			if (this.events[key][i] === func) {
				this.events[key].splice(i, 1);
			}
		}
	}
};
ModalComponent.prototype.dispatchEvent = function (key, dataObj) {
	if (this.events.hasOwnProperty(key)) {
		dataObj = dataObj || {};
		dataObj.currentTarget = this;
		for (var i in this.events[key]) {
			this.events[key][i](dataObj);
		}
	}
};