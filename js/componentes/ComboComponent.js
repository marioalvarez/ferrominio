var ComboComponent = function(object){	
	this.id = object.id;						
	this.url = object.url;
	this.data = object.data;
	this.dataSet = [];
	this.selectedItem =-1;
	this.valueField = object.valueField;
	this.displayField = object.displayField;
	this.defaultText = object.defaultText;
	this.defaultSelected = object.defaultSelected;
	this.events = {}
	
	this.getRecord = function(){
		index = $("#" + this.id + " option:selected").index();
		return this.dataSet[index];
	}
	
	this.getText = function(){
		text = $("#" + this.id + " option:selected").text();
		return text;
	}
	
	this.getDataSet = function(){
		return this.dataSet;
	}
	
	this.setSelectedItem = function(index){
		this.selectedItem = index;
	}
	
	this.getSelectedItem = function(){
		return this.selectedItem;
	}
	
	this.init = function(){								
		scopeCombo = this;																
	}
	
	this.load = function(){				
		this.data = arguments[0] ? arguments[0] : this.data;
		var scopeCombo = this;
		$.ajax({
			url: scopeCombo.url,	  
			data: scopeCombo.data,
			dataType: 'json',
			success:function(response){
				if(response.SUCCESS == true){																	
					scopeCombo.fillDataSet(response);
					scopeCombo.dispatchEvent("onLoad",response);
				}else{
					scopeCombo.dispatchEvent("onError",response);
				}																			
			},
			error: function(functionxhr, ajaxOptions, thrownError){										
				scopeCombo.dispatchEvent("onError",xhr);
			}
		});
		/*ajaxRequest({
			config:{
				url: scopeCombo.url
			},
			data: scopeCombo.data,			
			onBeforeSend: function(){
				//$("#" + scopeCombo.id).mask(scopeCombo.loadingMessage);				
			},
			onSuccess: function(response){		
				//$("#" + scopeCombo.id).unmask();	
				if(response.SUCCESS == true){			
														
					scopeCombo.fillDataSet(response);
					scopeCombo.dispatchEvent("onLoad",response);
				}else{
					scopeCombo.dispatchEvent("onError",response);
				}
			},
			onError: function(xhr, ajaxOptions, thrownError){										
				scopeCombo.dispatchEvent("onError",xhr);
			}
		});*/
	};		
	
	this.fillDataSet = function(data){
		scopeCombo = this;
		if(data.RS.DATA.length > 0){
			this.setSelectedItem(-1);
			this.dataSet = new Array();				
			if(this.defaultText){
				this.dataSet.push(0);	
			}
			for(x=0;x<data.RS.DATA.length;x++){					
				var obj = {}
				for(i=0;i<data.RS.DATA[x].length;i++){
					var valor = data.RS.DATA[x][i];
					var propiedad = data.RS.COLUMNS[i];						
					obj[propiedad] = valor;						
				}
				/* GUARDAR EN UN OBJETO TODOS LOS VALORES DEVUELTOS */
				this.dataSet.push(obj);											
			}
			this.fillCombo();	
								
		}else{
			this.dataSet = new Array();
			this.emptyCombo();
		}								
	}
	
	this.fillCombo = function(){								
		$("#" + this.id).empty();	
		index = 0;
		if(this.defaultText){		
			optionDefault = $('<option />');
			optionDefault.attr('value', 0).text(this.defaultText);	
			$("#" + this.id).append(optionDefault);		
			index = 1;
		}	
		
		for(x= index;x<this.dataSet.length;x++){					
			record = this.dataSet[x];								
			option = $('<option />');
			value = record[this.valueField.toString().toUpperCase()];
			text = record[this.displayField.toString().toUpperCase()];
			option.attr('value', value).text(text);			
			
			if(this.defaultSelected == value)
				option.attr('selected', 'selected');
						
			$("#" + this.id).append(option);
		}	
	}				
	
	this.emptyCombo = function(){
	
	}		
	
	this.replaceString = function(string,stringToReplace,valueToReplace){	
			
		while(true){
			if(string.indexOf(stringToReplace) == -1) break;
			string = string.replace(stringToReplace,valueToReplace);
		}
		return string;
	}
};			

ComboComponent.prototype.addEventListener = function (key, func) {
	if (!this.events.hasOwnProperty(key)) {
		this.events[key] = [];
	}
	this.events[key].push(func);
};

ComboComponent.prototype.removeEventListener = function (key, func) {
	if (this.events.hasOwnProperty(key)) {
		for (var i in this.events[key]) {
			if (this.events[key][i] === func) {
				this.events[key].splice(i, 1);
			}
		}
	}
};
ComboComponent.prototype.dispatchEvent = function (key, dataObj) {
	if (this.events.hasOwnProperty(key)) {
		dataObj = dataObj || {};
		dataObj.currentTarget = this;
		for (var i in this.events[key]) {
			this.events[key][i](dataObj);
		}
	}
};