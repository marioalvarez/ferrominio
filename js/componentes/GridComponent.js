// JavaScript Document 
// Autor: Jorge Eduardo L�pez 
// Descripcion: Grid generico por javascript 
var GridComponent = function(object){	
	this.id = object.id;			
	this.total = null;
	this.columns = object.columns;	
	this.url = object.url;
	this.page = 1;
	this.data = object.data;
	this.dataSet = [];
	this.totalPages = 0;
	this.selectedItem = -1;
	this.loadingMessage = object.loadingMessage ? object.loadingMessage : "Cargando...";
	this.events = {};
    
	this.getRecord = function(index){
		return this.dataSet[index];
	}
	
	this.getDataSet = function(){
		return this.dataSet;
	}
	
	this.setSelectedItem = function(index){
		this.selectedItem = index;
	}
	
	this.getSelectedItem = function(){
		return this.selectedItem;
	}
	
	this.init = function(){								
		scopeGrid = this;				
		//Al final elemento agrega un footer con el navegador de las paginas
		$("#" + this.id).append('<tfoot>'+
									'<tr class="noborder">'+
										'<th colspan="' + this.columns.length + '">'+
											'<div style="width:100%;float:left;font-size:12px;">'+
												'<div style="width:20%;float:left;height:32px;">'+
													'<div class="startPage grid_'+ this.id +'" border="0"></div>'+
												'</div>'+
												'<div style="width:20%;float:left;height:32px;">'+
													'<div class="backPage grid_'+ this.id +'"></div>'+
												'</div>'+
												'<div style="width:20%;float:left;height:32px;white-space:nowrap;" class="displayCount grid_'+ this.id +'">'+
													'Registro  al  de '+
												'</div>'+
												'<div style="width:20%;float:left;height:32px;">'+
													'<div class="nextPage grid_'+ this.id +'"></div>'+
												'</div>'+
												'<div style="width:20%;float:left;height:32px;">'+													
													'<div class="endPage grid_'+ this.id +'"></div>'+
												'</div>'+
											'</div> '+
											'</th>'+
									   '</tr>' + 
									 '</tfoot>');
      
		$("#" + this.id + " .startPage.grid_"+ this.id).click(function(e){
			scopeGrid.start();
		});
		
		$("#" + this.id + " .nextPage.grid_"+ this.id).click(function(e){
			scopeGrid.next();
		});
		
		$("#" + this.id + " .backPage.grid_"+ this.id).click(function(e){
			scopeGrid.back();
		});
		
		$("#" + this.id + " .endPage.grid_"+ this.id).click(function(e){
			scopeGrid.end();
		});																									
	}
	
	this.load = function(){				
		this.data = arguments[0] ? arguments[0] : this.data;
		scopeGrid = this;		
		$.ajax({
			cache: false,
			type: 'POST',
			url: scopeGrid.url,
			dataType: 'json',
			data: scopeGrid.data,			
			beforeSend: function(){
				$("#" + scopeGrid.id).mask(scopeGrid.loadingMessage);
			},
			success: function(response){
				if(scopeGrid.data.page > 1){
					if(response.RS.DATA.length === 0){
						scopeGrid.data.page -= 1;
                        
						if(scopeGrid.data.page == 0) {
                            scopeGrid.data.page = 1;
						    scopeGrid.page = scopeGrid.data.page;
						    scopeGrid.load();
                        }
					}else{
						$("#" + scopeGrid.id).unmask();	
						if(response.SUCCESS == true){
							scopeGrid.fillDataSet(response);
							scopeGrid.dispatchEvent("onLoad",response);
						}else{
							scopeGrid.dispatchEvent("onError",response);
						}
					}
				}else{
					$("#" + scopeGrid.id).unmask();	
					if(response.SUCCESS == true){												
						scopeGrid.fillDataSet(response);
						scopeGrid.dispatchEvent("onLoad",response);
					}else{
						scopeGrid.dispatchEvent("onError",response);
					}
				}
				
			},
			error: function(response){
				scopeGrid.dispatchEvent("onError",response.status);
			}
		});
	}
	
	this.start = function(){
		this.data.page = 1;	
		this.page = 1;
		this.load();		
	}
	
	this.next = function(){							
		if((this.data.page + 1) <= this.totalPages){
			this.data.page = parseInt(this.data.page) + 1;				
			this.page = this.data.page;
			this.load();
		}
		if(typeof callback == 'function'){
      		callback.call(this, data);
    	}
	}
	
	this.end = function(){							
		this.data.page = this.totalPages;	
		this.page = this.totalPages;
		this.load();				
	}
	
	this.back = function(){
		if(this.data.page == 1) {
			this.data.page = 1;
			this.page = 1;
		}else{
			this.data.page = parseInt(this.data.page) - 1;
			this.page = this.data.page;
		}
		this.load();
	}
	
	this.fillDataSet = function(data){
		if(data.RS.DATA.length > 0){
			this.setSelectedItem(-1);
			this.dataSet = new Array();				
			this.total = data.RS.DATA[0][data.RS.COLUMNS.length-1];
			this.pageSize = this.data.pageSize || this.data.pagesize;
			base = this.total / this.pageSize;
			mod = this.total % this.pageSize;
			base + (mod > 0 ? 1 : 0);	
			
			base = Math.ceil(base)			
			base = parseInt(base);	
			this.totalPages = base;
				
			for(x=0;x<data.RS.DATA.length;x++){					
				var obj = {}
				for(i=0;i<data.RS.DATA[x].length;i++){
					var valor = data.RS.DATA[x][i];
					var propiedad = data.RS.COLUMNS[i];						
					obj[propiedad] = valor;						
				}
				/* GUARDAR EN UN OBJETO TODOS LOS VALORES DEVUELTOS */
				this.dataSet.push(obj);											
			}
			this.fillGrid();	
								
		}else{
			this.dataSet = new Array();
			this.emptyGrid();
		}								
	}
	/* M�todo para actualizar el DataSet de los campos input */
	/* Roberto 19/Nov/2013 */
	this.updateDataSet = function(){
		//inicia recorrido de las filas del data set
		for(x=0;x<this.dataSet.length;x++)
		{
			//por cada fila, recorrera las columnas
			for(i=0;i<this.columns.length;i++)
			{
				//si la columna es de tipo custom
				if (this.columns[i].type == 'custom')
				{
					//selecciona su contenido si es de tipo text
					if ($("#" + this.id + " tbody tr")[x].cells[i].children[0].type == "text")
					{
						//se obtiene el valor del textbox
						a = $("#" + this.id + " tbody tr")[x].cells[i].children[0].value;
						//se guarda el valor del textbox en la celda correspondiente del data set
						this.dataSet[x][this.columns[i].columnsReplace[0].toUpperCase()] = a;
					}
					else//selecciona su contenido si es de tipo checkbox
					{
						if ($("#" + this.id + " tbody tr")[x].cells[i].children[0].type == "checkbox")
						{
							//se obtiene el valor del checkbox
							a = $("#" + this.id + " tbody tr")[x].cells[i].children[0].checked;
							if ((a == "checked") || (a == true))
							{
								a = 1;
							}
							else
							{
								a = 0;
							}
							//se guarda el valor del checkbox en la celda correspondiente del data set
							this.dataSet[x][this.columns[i].columnsReplace[0].toUpperCase()] = a;
						}
					}
				}//ACTUALIZADO: 18-FEBRERO-2014 : Roberto Medrano..
				//Si la columna es de tipo "check".. (!)
				else if (this.columns[i].type == 'check')
				{
					var checkbox = $("#" + this.id + " tbody tr")[x].cells[i].children[0];
					if (checkbox.checked == "checked" || checkbox.checked == true)
					{
						this.dataSet[x].S = 1;
					}
					else
					{
						this.dataSet[x].S = 0;
					}
				}
			}
		}
	}
	this.fillGrid = function(){
		$("#" + this.id + " tbody").empty();				
		arrayCols = new Array();
		for(x=0;x<this.dataSet.length;x++){					
			record = this.dataSet[x];
			for(i=0;i<this.columns.length;i++){						
				if(this.columns[i].type == 'text'){
					value = record[this.columns[i].dataIndex.toUpperCase()];	
					css = this.columns[i].css ? 'style="' + this.columns[i].css + '"' :  "";						
					if(value == null) value = "";			
					column = '<td ' + css + '>'+value+'</td>';
				}
				//COLUMNAS DE TIPO CUSTOM:
				else if(this.columns[i].type == 'custom') {
					//SI LA COLUMNA CONTIENE LA PROPIEDAD columnsReplace: (columnsReplace se manda como array)
					if(this.columns[i].columnsReplace) {
						//AVERIGUAR CUANTOS ELEMENTOS TIENE EL ARRAY columnsReplace..
						if(this.columns[i].columnsReplace.length > 0) {
							//SI TIENE 1 O MAS VALORES, SE GUARDA LA PROPIEDAD render DE LA COLUMNA CUSTOM
							//EN LA VARIABLE tempColumn.. (render guarda como string codigo html para agregar al grid)
							tempColumn = this.columns[i].render;
							//INICIA EL RECORRIDO DE LOS ELEMENTOS columnsReplace
							for(j = 0; j < this.columns[i].columnsReplace.length; j++) {
								//SI columnsReplace ES index.. RETORNA EL INDICE DEL ROW..
								if(this.columns[i].columnsReplace[j] == 'index') {
									//INDICE DEL ROW
									replacedValue = x;
								}else{//SI ES DE CUALQUIER OTRO VALOR, SE CONSIDERA UNA REFERENCIA A UN CAMPO DEL DATASET..(SE OBTIENE EN MAYUSCULAS)
									replacedValue = record[this.columns[i].columnsReplace[j].toUpperCase()];
									switch (typeof replacedValue) {
										//SI ES DE TIPO STRING
										case 'string':
											replacedValue = "\'" + replacedValue + "\'";
										break;
									}
								}
								//CADENA QUE SE VA A REEMPLAZAR
								stringToReplace = "{" + this.columns[i].columnsReplace[j] +  "}";
								//REEMPLAZAR COLUMNA POR EL VALOR DEL DATA SET
								tempColumn = this.replaceString(tempColumn,stringToReplace,replacedValue);
								column = tempColumn;
							}
						}					
					}else{							
						column = this.columns[i].render;							
					}
				}else if(this.columns[i].type == 'action'){
					if (this.columns[i].switchShowItem == true)
					{
						if (record[this.columns[i].dataIndex.toUpperCase()] == true) {
							divAction = '<div id="actionToolbar' + x +'" class="settings-button">';
							divAction = divAction + '<div id="actionToolbar-options' + x +'" class="toolbar-icons" style="display: none;">';
							css = this.columns[i].css ? 'style="' + this.columns[i].css + '"' :  "";
							for(actions=0;actions<this.columns[i].actions.length;actions++){
								condicion='';
								if(this.columns[i].actions[actions].conditionExp)
								{
									replacedValue = "record[this.columns[i].actions[actions].conditionColumn[";
									stringToReplace =  "{";
									condicion= this.columns[i].actions[actions].conditionExp;
									condicion=this.replaceString(condicion,stringToReplace,replacedValue);			  
		
									replacedValue = "].toUpperCase()]";
									stringToReplace =  "}";
									condicion= condicion;
									condicion=this.replaceString(condicion,stringToReplace,replacedValue);
								}
								else
								{
									condicion='true';
								}
								if(eval(condicion)){
									action = '<a id="action'+ actions +'" href="#" title="' + this.columns[i].actions[actions].title + '" onclick="javascript:' + this.columns[i].actions[actions].funcion + '"><i class="'+ this.columns[i].actions[actions].iconClass + '"></i></a>';
									divAction = divAction + action;						
								}
							}	
							divAction = divAction + "</div></div>";							
							column = '<td ' + css + '>'+divAction+'</td>';
						}
						else{
							column = '<td ' + css + '></td>';
						}
					}
					else {
						divAction = '<div id="actionToolbar_'+this.id+'_'+ x +'" class="settings-button">';
						divAction = divAction + '<div id="actionToolbar-options_' +this.id+'_'+ x +'" class="toolbar-icons" style="display: none;">';
						css = this.columns[i].css ? 'style="' + this.columns[i].css + '"' :  "";
						for(actions=0;actions<this.columns[i].actions.length;actions++){
							condicion='';
							if(this.columns[i].actions[actions].conditionExp)
							{
								replacedValue = "record[this.columns[i].actions[actions].conditionColumn[";
								stringToReplace =  "{";
								condicion= this.columns[i].actions[actions].conditionExp;
								condicion=this.replaceString(condicion,stringToReplace,replacedValue);			  
	
								replacedValue = "].toUpperCase()]";
								stringToReplace =  "}";
								condicion= condicion;
								condicion=this.replaceString(condicion,stringToReplace,replacedValue);
							}
							else
							{
								condicion='true';
							}
							if(eval(condicion)){
								action = '<a id="action'+ actions +'" href="#" title="' + this.columns[i].actions[actions].title + '" onclick="javascript:' + this.columns[i].actions[actions].funcion + '"><i class="'+ this.columns[i].actions[actions].iconClass + '"></i></a>';
								divAction = divAction + action;						
							}
						}	
						divAction = divAction + "</div></div>";							
						column = '<td ' + css + '>'+divAction+'</td>';
					}
				}else if(this.columns[i].type == 'check'){	
					value = record[this.columns[i].dataIndex.toUpperCase()];
					css = this.columns[i].css ? 'style="' + this.columns[i].css + '"' :  "";						
					disabled = this.columns[i].disabled==false ? disabled="" : disabled="disabled";
					if(value == null || value == 0)
					{
						value = '<input type="checkbox" value="0" '+disabled+'/>'
					}
					else 
					{
						value = '<input type="checkbox" value="1" checked="checked" '+disabled+'/>';
					}
					
					column = '<td ' + css + '>'+value+'</td>';
				}
				else if(this.columns[i].type == 'label'){						
					value = record[this.columns[i].dataIndex.toUpperCase()];
					datos = value.split("-");
					color = datos[1];
					value = datos[0];	
					if(value == null) value = "";			
					column = '<td style="text-align:center"><label class="label l-'+color+'">'+value+'</label></td>';
					
				}
				arrayCols.push(column);		
			}													
		}	
	
		index = 0;
		total =  this.columns.length;	
		column = "";
		for(x=index;x<total;x++){
			column = column + arrayCols[x];
			if(x == total-1){
				$("#" + this.id + " tbody").append("<tr>" + column + "</tr>");
				column = "";
				index = x;
				total = total + this.columns.length;
				if(total > arrayCols.length) break;
			}						
		}
		//CICLO PARA INICIALIZAR LOS ACTIONTOOLBAR : Roberto Medrano ::CORREGIDO 26-febrero-2014
		//recorrer todas las columnas
		for (icolumn = 0; icolumn < this.columns.length; icolumn++) {
			//en busca de tipo 'action'
			if (this.columns[icolumn].type == 'action') {
				//si encuentra una columna tipo action.. recorrer todos los rows para inicializar los toolbar
				for (irow = 0; irow < this.dataSet.length; irow++) {
					$('#actionToolbar_'+this.id+'_'+ irow).toolbar({
						content: '#actionToolbar-options_'+this.id+'_'+irow,
						position: 'top',
						hideOnClick: 'true'
					});
				}
			}
		}//Ya Funca!!
		inicio = this.dataSet[0]["NUM"];
		fin = this.dataSet[this.dataSet.length - 1]["NUM"];				
		$("#" + this.id + " .displayCount").empty();		
		$("#" + this.id + " .displayCount").append("Registro " + inicio + " al " + fin + " de " + this.total);						
		
		scopeGrid = this;
		$("#" + this.id + " tbody tr").each(function(index,value) {
			
			$(value).click(function(e){
				$(value).addClass("highlight").siblings().removeClass("highlight");
				scopeGrid.dispatchEvent("onClick", index);
				scopeGrid.setSelectedItem(index);
			});
			
			$(value).dblclick(function(e){						
				scopeGrid.dispatchEvent("onDblClick", index);
			});			
		});
		 
		/* MOSTRAR U OCULTAR EL PAGINADOR */	
		
		if(this.totalPages > 1){
			if(this.data.page == 1){
				$("#" + this.id + " .startPage.grid_"+ this.id).css("display","none");
				$("#" + this.id + " .backPage.grid_"+ this.id).css("display","none");
				$("#" + this.id + " .nextPage.grid_"+ this.id).css("display","block");
				$("#" + this.id + " .endPage.grid_"+ this.id).css("display","block");
			}else if(this.data.page == this.totalPages){
				$("#" + this.id + " .startPage.grid_"+ this.id).css("display","block");
				$("#" + this.id + " .backPage.grid_"+ this.id).css("display","block");
				$("#" + this.id + " .nextPage.grid_"+ this.id).css("display","none");																		
				$("#" + this.id + " .endPage.grid_"+ this.id).css("display","none");	
			}else{
				$("#" + this.id + " .startPage.grid_"+ this.id).css("display","block");
				$("#" + this.id + " .nextPage.grid_"+ this.id).css("display","block");												
				$("#" + this.id + " .backPage.grid_"+ this.id).css("display","block");
				$("#" + this.id + " .endPage.grid_"+ this.id).css("display","block");
			}					
		}else{						
			$("#" + this.id + " .startPage.grid_"+ this.id).css("display","none");
			$("#" + this.id + " .nextPage.grid_"+ this.id).css("display","none");												
			$("#" + this.id + " .backPage.grid_"+ this.id).css("display","none");
			$("#" + this.id + " .endPage.grid_"+ this.id).css("display","none");	
		}
	}				
	
	this.emptyGrid = function(){
		$("#" + this.id + " .startPage.grid_"+ this.id).css("display","none");
		$("#" + this.id + " .nextPage.grid_"+ this.id).css("display","none");												
		$("#" + this.id + " .backPage.grid_"+ this.id).css("display","none");
		$("#" + this.id + " .endPage.grid_"+ this.id).css("display","none");	
		$("#" + this.id + " tbody").empty();
		$("#" + this.id + " .displayCount.grid_"+ this.id).empty();	
		$("#" + this.id + " .displayCount.grid_"+ this.id).append("No hay registros a mostrar");	
	}		
	
	this.replaceString = function(string,stringToReplace,valueToReplace){
		while(true){
			if(string.indexOf(stringToReplace) == -1) break;
			string = string.replace(stringToReplace,valueToReplace);
		}
		return string;
	}
};


GridComponent.prototype.addEventListener = function (key, func) {
	if (!this.events.hasOwnProperty(key)) {
		this.events[key] = [];
	}
	this.events[key].push(func);
};

GridComponent.prototype.removeEventListener = function (key, func) {
	if (this.events.hasOwnProperty(key)) {
		for (var i in this.events[key]) {
			if (this.events[key][i] === func) {
				this.events[key].splice(i, 1);
			}
		}
	}
};
GridComponent.prototype.dispatchEvent = function (key, dataObj) {
	if (this.events.hasOwnProperty(key)) {		
		//dataObj = dataObj || {};		
		//dataObj.currentTarget = this;
		for (var i in this.events[key]) {
			this.events[key][i](dataObj);
		}
	}
};