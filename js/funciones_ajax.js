// JavaScript Document
// AUTOR: Juan Escobar

// Funcion para cargar una p�gina por ajax, con div dinamico, icono de carga y cacha los errores
function cargar_pagina(pagina,div,icono){
	// S� el parametro div no esta definido, se carga uno por default
	div = div?div:'#div';
	// S� el parametro icono no esta definido, no se carga ninguno, por motivos de que el proceso que hizo la llamada no lo requiera
	icono = icono?icono:'';
	
	if (icono != '')
		$(div).html('<p align="center"><img src="../images/'+icono+'"/></p>');
	
	time = new Date().getTime();
	var pag = pagina + '&time=' + time;
	
	$.ajax({
		type: 'GET',
		url: pag,
		success:
			function(msg){
				$(div).html(msg)
			},
		error:
			function(xhr, ajaxOptions, thrownError){
				if (xhr.status != ""){
					var msg = "<br><br><br>Ha ocurrido un error: ";
					$(div).html('<div class="contenido" style="text-align:center">' + msg + xhr.status + " " + xhr.statusText+ '</div>');
				}
			
			}
	});	
}
