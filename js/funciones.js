// JavaScript Document
/*Codigo para iniciar controles segun su className*/

doc_x = $(document);
doc_x.ready(inicializarControles);
//$(function() {
inicializarControles();

function inicializarControles(){
	$(".contenido").keydown(escanear_enter);
	$(".contenido").keypress(escanear_minusculas);
	$(".contenido_readOnly").keypress(escanear_readOnly);
	$(".contenido_readOnly").keydown(escanear_enter);
	$(".contenido_readOnly").keydown(escanear_retorno_readOnly);
	$(".contenido_libre").keydown(escanear_enter);
	$(".contenido_filtro").keypress(escanear_minusculas);
	$(".contenido_numerico").keypress(escanear_numerico);
	$(".contenido_numerico").keydown(escanear_enter);
	$(".contenido_alfanumerico_con_espacios").keydown(escanear_enter);
	$(".contenido_alfanumerico_con_espacios").keypress(escanear_minusculas);
	$(".contenido_alfanumerico_con_espacios").keypress(escanear_alfanumerico_con_espacios);
	$(".contenido_flotante_negativo").keypress(escanear_flotante_negativo);
	$(".contenido_flotante_negativo").keydown(escanear_enter);
	$(".contenido_moneda").keypress(escanear_flotante);
	$(".contenido_moneda").keydown(escanear_enter);
	$(".contenido_moneda").focus(focus_money);
	$(".contenido_moneda").blur(blur_money);
	$(".contenido_textarea").keypress(escanear_minusculas);
	$(".boton_popup").keydown(escanear_enter);
	$(".renglon_grid").mouseover(mouseOverRenglon);
	$(".renglon_grid").mouseout(mouseOutRenglon);
	$(".contenido_flotante").keypress(escanear_flotante);
	$(".contenido_flotante").keydown(escanear_enter);
	//$("a").mouseover(setEstatusWindowEnBlanco);

	//Obtiene todos los textArea que tenga el atributo maxlength
	$(".contenido_textarea[maxlength]").each(function() {
		var $textarea = $(this);			
		var maxlength = $textarea.attr("maxlength");		

		$textarea.after($('<div style="font-weight:bold">').addClass("charsRemaining"));
		$(this).bind("keyup",function(e){
			var val = $textarea.val().replace(/\r\n|\r|\n/g, "\r\n").slice(0, maxlength);
			$textarea.val(val);
			$textarea.next(".charsRemaining").html(maxlength - val.length + " caracteres restantes");
		});			
		
		$(this).bind("blur",function(e){
			var val = $textarea.val().replace(/\r\n|\r|\n/g, "\r\n").slice(0, maxlength);
			$textarea.val(val);
			$textarea.next(".charsRemaining").html(maxlength - val.length + " caracteres restantes");
		});	
		
		$(this).trigger("blur");	
	});	
};
	
$(function(){
	$(".encabezado").css("border","13px solid red");
})

function mouseOverRenglon(){
	$(this).css("background", '#99CCCC');
	}
function mouseOutRenglon(){
	$(this).css("background", '');
	}
function setEstatusWindowEnBlanco(){
	window.status= '';
	return true;
	}

function focus_money(){

	this.value= unformatNumber(this.value);
	this.select();
	}
	function formatea(numero,decimos){
	

	var formato=''+numero;
	formato=formato.split('.');
	var decimales=formato[1];
	var numero=formato[0].split("").reverse().join("");
	
	if(typeof(decimales)=='undefined')
		var decimales='0';

	
	var str='';
	
	for(var i=0;i<numero.length;i++){
		
			if(i%3==0 && i!=0){
					if(numero[i]=='-'){

					}else{
						str+=',';
					}
					
			}
			str+=numero[i];
	}
	
	str=str.split("").reverse().join("");
	 
	if(decimales!=''){	
		if(decimales.length<decimos){
		 	for(var i=decimales.length;i<decimos;i++){
		 		decimales+='0';
		 	}
		}else{
		 	decimales=decimales.substring(0,decimos);
		}
	}

	return str+'.'+decimales;
}


function blur_money(){
	this.value= formatMoney(this.value, '$');
	}

function formatMoney(numero, prefijo){
	if (numero == "")
		return "";
	numero = Math.round(parseFloat(numero)*Math.pow(10,4))/Math.pow(10,4)
	prefijo = prefijo || '';
	numero += '';
	var splitStr = numero.split('.');
	var splitLeft = splitStr[0];
	var splitRight = splitStr.length > 1 ? '.' + splitStr[1] : '.00';
	//splitRight = splitRight + '00';
	splitRight = splitRight.substr(0,5);
	var regx = /(\d+)(\d{3})/;
	while (regx.test(splitLeft)){
		splitLeft = splitLeft.replace(regx, '$1' + ',' + '$2');
		}
	return prefijo + splitLeft + splitRight;
	}
	
function formatDecimal(numero){
	if (numero == "")
		return "";
	numero = Math.round(parseFloat(numero)*Math.pow(10,2))/Math.pow(10,2)
	//prefijo = prefijo || '';
	numero += '';
	var splitStr = numero.split('.');
	var splitLeft = splitStr[0];
	var splitRight = splitStr.length > 1 ? '.' + splitStr[1] : '.00';
	splitRight = splitRight + '00';
	splitRight = splitRight.substr(0,3);
	var regx = /(\d+)(\d{3})/;
	while (regx.test(splitLeft)){
		splitLeft = splitLeft.replace(regx, '$1' + ',' + '$2');
		}
	return /*prefijo*/ + splitLeft + splitRight;
	}

function unformatNumber(numero){
	if (numero == "")
		return "";
	return numero.replace(/([^0-9\.\-])/g,'')*1;
	} 
	
/*obtiene un elemento por su ID*/
/*
function $x() {
	var elements = new Array();
	for (var i = 0; i < arguments.length; i++) {
		var element = arguments[i];
		if (typeof element == 'string')
			element = document.getElementById(element);
		if (arguments.length == 1)
			return element;
		elements.push(element);
		}
	return elements;
	}
	*/
/*Modificaci�n por compatibilidad con nuevas versiones de navegadores*/
function $x(id_or_name) {
	element = document.getElementById(id_or_name);
	if (!element){
		elements= document.getElementsByName(id_or_name);
		if (elements)
			element= elements[0];
		}
	return element;
	} 
/*	
function escanear_enter(){
	if (window.event.keyCode==13)
		window.event.keyCode= 9;
		alert('Enter!');
		return event.keyCode
	}
*/
function cambiarFoco(Campo){
	var seEncontro = false;
	var seCambio = false;
	var elements = document.getElementsByTagName('*');
	for (var i = 0; i < elements.length; i++) {
		if(!seEncontro){
			if (Campo == elements[i]) seEncontro = true;
		}
		else{
			var element = elements[i];
			if(element.tagName.toUpperCase() == 'INPUT' 
					&& element.type.toUpperCase() != 'HIDDEN' 
					&& element.style.display.toUpperCase() != 'NONE' 
					&& element.style.visibility.toUpperCase() != 'HIDDEN'
					&& element.disabled.toString().toUpperCase() != 'TRUE'){
				element.focus(); 
				seCambio=true; 
				break;
			}
			if(element.tagName.toUpperCase() == 'BUTTON'
					&& element.style.display.toUpperCase() != 'NONE' 
					&& element.style.visibility.toUpperCase() != 'HIDDEN'
					&& element.disabled.toString().toUpperCase() != 'TRUE'){
				element.focus(); 
				seCambio=true; 
				break; 
			}
			if(element.tagName.toUpperCase() == 'SELECT'
					&& element.style.display.toUpperCase() != 'NONE' 
					&& element.style.visibility.toUpperCase() != 'HIDDEN'
					&& element.disabled.toString().toUpperCase() != 'TRUE'){
				element.focus(); 
				seCambio=true; 
				break; 
			}
			if(element.tagName.toUpperCase() == 'TEXTAREA'
					&& element.style.display.toUpperCase() != 'NONE' 
					&& element.style.visibility.toUpperCase() != 'HIDDEN'
					&& element.disabled.toString().toUpperCase() != 'TRUE'){
				element.focus(); 
				seCambio=true; 
				break; 
			}
			if(element.tagName.toUpperCase() == 'A'
					&& element.style.display.toUpperCase() != 'NONE' 
					&& element.style.visibility.toUpperCase() != 'HIDDEN'){
				element.focus(); 
				seCambio=true; 
				break; 
			}
		}	
	}
	if(!seCambio){
		for (var i = 0; i < elements.length; i++) {
			var element = elements[i];
			if(element.tagName.toUpperCase() == 'INPUT' && element.type.toUpperCase() != 'HIDDEN'){
				element.focus(); seCambio=true; break;
			}
			if(element.tagName.toUpperCase() == 'BUTTON'){element.focus(); seCambio=true; break; }
			if(element.tagName.toUpperCase() == 'SELECT'){element.focus(); seCambio=true; break; }
			if(element.tagName.toUpperCase() == 'TEXTAREA'){element.focus(); seCambio=true; break; }
			if(element.tagName.toUpperCase() == 'A'){element.focus(); seCambio=true; break; }
		}
	}
	return !seCambio; 
}

function escanear_enter(e){
	var keyCode = 0;
	
	if(e) keyCode = e.which;
	else keyCode = event.keyCode ? event.keyCode : event.charCode; 
	if (keyCode == 13) { 
		var comp = ($(this).attr('id') != '') ? $(this).attr('id') : $(this).attr('name');
		return cambiarFoco($x(comp)); 
		}
	else return true;
}
	
function enter(){
	if (window.event.keyCode == 13) 
		window.event.keyCode = 9;
	}

function escanear_minusculas(){
	if (window.event.keyCode < 123 && window.event.keyCode > 96)
		window.event.keyCode-= 32;

	}
function escanear_readOnly(){
	window.event.keyCode= 0;	
	}
function escanear_retorno_readOnly(){
	if (window.event.keyCode==8 || window.event.keyCode== 46){
		/*el returnValue= false, es para detener el evento de backspace*/
		window.event.returnValue= false;
		window.event.keyCode= 0;
		}
	}
function escanear_numerico(){
	if (event.keyCode < 48 || event.keyCode > 57) 
		event.returnValue = false;
	};
function escanear_alfanumerico_con_espacios(){
	if ( !(event.keyCode >= 48 && event.keyCode <= 57) && !(event.keyCode >= 65 && event.keyCode <= 90) && !(event.keyCode >= 97 && event.keyCode <= 122) && !(event.keyCode == 32)) 
		event.returnValue = false;
	//else
	//	event.returnValue = true;
	};
function escanear_flotante(input_txt){
	
	var keyCode;
	if(window.event) // IE/Chrome
	{
		keyCode = input_txt.keyCode;
	}
	else if(input_txt.which) // Netscape/Firefox/Opera
	{
		keyCode = input_txt.which;
	}
	
	if (keyCode == 8) //Barra de retroceso (Funcione en firefox)
	{
		return true;
	}
		
	if (keyCode < 48 || keyCode > 57){
		if (keyCode != 46){
			return false;
			}
		else {
			if (arguments[0].type == 'keypress'){
				if ( this.value.indexOf('.') > -1 )
					return false;
				}				
			else {
				if ( arguments[0].value.indexOf('.') > -1 ){
					return false;
				}
			}				
		}
	}
};
	
	
function escanear_flotante_negativo(input_txt){
	var keyCode;
	if(window.event) // IE/Chrome
	{
		keyCode = input_txt.keyCode;
	}
	else if(input_txt.which) // Netscape/Firefox/Opera
	{
		keyCode = input_txt.which;
	}
	
	//Caracteres especiales
	if (keyCode == 8) // 8 = Barra de retroceso (Funcione en firefox)
		return true;
	else 
		if (keyCode == 45) // 45 = signo de menos o guion
		{
			//Ver que no haya un signo de menos ya y que 
			if (arguments[0].type == 'keypress') {
				if (this.value.indexOf('-') > -1) 
					return false;
			}
			else {
				if (arguments[0].value.indexOf('-') > -1) {
					return false;
				}
			}
			//Si es el primer signo de - que presiona se agregara el signo al inicio del campo de texto ya agregado y posteriormente regresara falso
			this.value = '-' + this.value;
			return false;
		}
		
	if (keyCode < 48 || keyCode > 57){
		if (keyCode != 46){
			return false;
			}
		else {
			if (arguments[0].type == 'keypress'){
				if ( this.value.indexOf('.') > -1 )
					return false;
				}				
			else {
				if ( arguments[0].value.indexOf('.') > -1 ){
					return false;
				}
			}				
		}
	}
};

	
/*funciones que quita los espacios en blanco de una cadena*/
String.prototype.trim = function() {
	return this.replace(/^\s+|\s+$/g,"");
}
String.prototype.ltrim = function() {
	return this.replace(/^\s+/,"");
}
String.prototype.rtrim = function() {
	return this.replace(/\s+$/,"");
}

/*Abre una ventana tipo Pop Up tipo modal*/
function AbrirPopUpBusqueda(idControlRecibeValor, strPaginaBusqueda, strParametrosPagina, anchoPagina, altoPagina){
	identificador= new Date();
	ControlRecibeValor= $x(idControlRecibeValor);
	var centerLeft = Number((screen.width/2)-(parseInt(anchoPagina)/2));
    var centerTop = Number((screen.height/2)-(parseInt(altoPagina)/2));
	valor_devuelto = window.open(strPaginaBusqueda + '?identificador=' + identificador.getTime() + '&idControlRecibeValor=' + idControlRecibeValor + '&' + strParametrosPagina,'Ventana',"status=0,toolbar=0, height=" + altoPagina + ",width=" + anchoPagina + ",top=" + centerTop + ",left="+ centerLeft);
	//valor_devuelto =  window.open(strPaginaBusqueda + '?identificador=' + identificador.getTime() + '&' + strParametrosPagina,'','dialogHeight:' + altoPagina + ';dialogWidth:' + anchoPagina + ';');
	cambiarValorConEventoOnChange(idControlRecibeValor, valor_devuelto);
	ControlRecibeValor.focus();
	cambiarFoco(ControlRecibeValor);
	}	

/*Funcion que pone en foco el siguiente elemento de uno dado*/
function PonerFocoSiguienteControl(elemento){
	if (elemento.value.length < elemento.getAttribute('maxlength')) return;
	
	var siguienteElemento = elemento.form.elements[elemento.tabIndex+1];
	if (siguienteElemento && siguienteElemento.focus) siguienteElemento.focus();
}

/*Abre una ventana tipo Pop Up tipo modal*/
function AbrirPopUpBusquedaRegresaArreglo(idControlRecibeValor, strPaginaBusqueda, strParametrosPagina, anchoPagina, altoPagina){
	identificador= new Date();
	//ControlRecibeValor= $x(idControlRecibeValor);
	valor_devuelto=  window.open(strPaginaBusqueda + '?identificador=' + identificador.getTime() + '&' + strParametrosPagina,'','dialogHeight:' + altoPagina + ';dialogWidth:' + anchoPagina + ';');
	$x(idControlRecibeValor).value= valor_devuelto;
	}	

/*funcion que cambia el valor de un input invocando al evento onchange*/
function cambiarValorConEventoOnChange(id_input, valor_nuevo){
	document.getElementById(id_input).value = valor_nuevo;
	if (document.getElementById(id_input).onchange)
		document.getElementById(id_input).onchange();
}

/*agrega funciones al body.onload, parametro: funcion*/
function addLoadEvent(func){
	var oldonload = window.onload;
	if (typeof window.onload != 'function'){
		window.onload = func;
		}
	else {
		window.onload = function(){
			oldonload();
			func();
			}
		}
	}
/*Abre una ventana tipo Pop Up tipo modal para Editar o Agregar*/
function AbrirPopUpAgregarEditar(refrescar, strPaginaPopUp, strParametrosPaginaPopUp, anchoPagina, altoPagina){
	var centerLeft = Number((screen.width/2)-(parseInt(anchoPagina)/2));
    var centerTop = Number((screen.height/2)-(parseInt(altoPagina)/2));	
	var identificador= new Date();
    
    modificado = window.open(strPaginaPopUp + '?identificador=' + identificador.getTime() + '&' + strParametrosPaginaPopUp,'Ventana',"status=0,toolbar=0, height=" + altoPagina + ",width=" + anchoPagina + ",top=" + centerTop + ",left="+ centerLeft);

	//modificado= showModalDialog(strPaginaPopUp + '?identificador=' + identificador.getTime() + '&' + strParametrosPaginaPopUp,'','dialogHeight:' + altoPagina + ';dialogWidth:' + anchoPagina + ';');
	if (refrescar == 'true' && modificado == 'true'){
		window.location.reload();
	 }
}
/*Abre una ventana tipo Pop Up tipo modal para Editar o Agregar*/
function AbrirPopUpAgregarEditarPresupuesto(strPaginaOrigen, strPaginaPopUp, strParametrosPaginaPopUp, anchoPagina, altoPagina){
	identificador= new Date();
	Modificado=  window.open(strPaginaPopUp + '?identificador=' + identificador.getTime() + '&' + strParametrosPaginaPopUp,'','dialogHeight:' + altoPagina + ';dialogWidth:' + anchoPagina + ';');
	if (Modificado == 'true'){
		window.location.href= strPaginaOrigen;
		}
	}
/*Abre una ventana tipo Pop Up tipo modal para Editar o Agregar una tarjeta al presupuesto*/
function AbrirPopUpPresupuestoAgregarEditarEliminarTarjeta(funcionCallBack, strPaginaPopUp, strParametrosPaginaPopUp, anchoPagina, altoPagina){
	identificador= new Date();
	datosModificacionTarjeta=  window.open(strPaginaPopUp + '?identificador=' + identificador.getTime() + '&' + strParametrosPaginaPopUp,'','dialogHeight:' + altoPagina + ';dialogWidth:' + anchoPagina + ';');
	if (datosModificacionTarjeta.Modificado == 'true'){
		funcionCallBack(datosModificacionTarjeta);
		}
	}
	
	
/*Abre una ventana tipo Pop Up tipo modal para Editar o Agregar*/
function AbrirPopUpCopiarTarjeta(strPaginaPopUp, strParametrosPaginaPopUp, anchoPagina, altoPagina){
	identificador= new Date();
	modificado=  window.open(strPaginaPopUp + '?identificador=' + identificador.getTime() + '&' + strParametrosPaginaPopUp,'','dialogHeight:' + altoPagina + ';dialogWidth:' + anchoPagina + ';');
	return modificado;
	}
	
/*Abre una ventana tipo Pop Up tipo modal para Editar o Agregar, devuelve true si se realizo el agregar o el actualizar*/
function AbrirPopUpAgregarEditarDinamico(strPaginaPopUp, strParametrosPaginaPopUp, anchoPagina, altoPagina){
	identificador= new Date();
	modificado=  window.open(strPaginaPopUp + '?identificador=' + identificador.getTime() + '&' + strParametrosPaginaPopUp,'','dialogHeight:' + altoPagina + ';dialogWidth:' + anchoPagina + ';');
	return modificado;
	}
	
function FormToURL(id_forma){
	var forma= $x(id_forma);
	var cadena_url= '?';
	for (var ind = 0; ind < forma.length; ind++){
		cadena_url= cadena_url + forma.elements[ind].name + '=' + forma.elements[ind].value + '&';
		}
	cadena_url= cadena_url.substring(0, cadena_url.length - 1);
	return cadena_url;
	}
/*addEvent*/
/*
function addEvent(elm, evType, fn, useCapture) {
	if (elm.addEventListener) {
		elm.addEventListener(evType, fn, useCapture);
		return true;
	}
	else if (elm.attachEvent) {
		var r = elm.attachEvent('on' + evType, fn);
		return r;
	}
	else {
		elm['on' + evType] = fn;
	}
}
	*/
/*Pon el foco en un control por su id como parametro*/
function ponerFocus(idControl){
	try{
		document.getElementById(idControl).focus();
		}
	catch(err){};
	}
/**/
function AjaxTablaDinamica(strParametros, id_LabelDestino, paginaFuente){
	lblDestino= $("#" + id_LabelDestino);
	identificador= new Date();
	time= identificador.getTime();
	strParametros= strParametros + "&identificador_unico=" + time.toString();
  $.ajax({
		async:true,
		type: "GET",
	 	dataType: "text",
	 	contentType: "application/x-www-form-urlencoded",
		url: paginaFuente,
		data: strParametros,
		complete: function (){
			//$x('busqueda_de_Obra').focus();
			},
	 	beforeSend: function (){
			lblDestino.html('<img src="../images/cargando.gif" title="Descargando Datos"');
			},
	 	success: function (respuestaHtml){
			lblDestino.html(respuestaHtml);
			},
	 	timeout:5000,
	 	error:function (){
			lblDestino.html('<img src="../images/warning_20x18.gif" title="Error en Conexi�n">');
			}
 		}); 
	}


function TraerDescripcionPorAjax(strParametros, id_LabelDestino, id_BotonSubmit, paginaFuente){
	lblDestino= $("#" + id_LabelDestino);
	btnSubmit= $x(id_BotonSubmit);
  $.ajax({
		async:true,
		type: "POST",
	 	dataType: "text",
	 	contentType: "application/x-www-form-urlencoded",
		url: paginaFuente,
		data: strParametros,
	 	beforeSend: function (){
			lblDestino.html('<img src="../images/cargando.gif" title="Descargando Datos">');
			btnSubmit.disabled= "disabled";
			},
	 	success: function (respuestaHtml){
			lblDestino.html(respuestaHtml);
			btnSubmit.disabled= "";
			},
	 	timeout:5000,
	 	error:function (){
			lblDestino.html('<img src="../images/warning_20x18.gif" title="Error en Conexi�n">');
			}
 		}); 
	}
	//variable para mostrar mientras se descarga contenido ajax
	html_imagen_descarga= '<img src="../images/cargando.gif" title="Descargando Datos">';
/**/

function AjaxPaginaDinamica(strParametros, id_LabelDestino, paginaFuente, jsIniciaControles){
	lblDestino= $("#" + id_LabelDestino);
  $.ajax({
		async:true,
		type: "POST",
	 	dataType: "text",
	 	contentType: "application/x-www-form-urlencoded",
		url: paginaFuente,
		data: strParametros,
		complete: function (){
			$x('busqueda_de_Obra').focus();
			},
	 	beforeSend: function (){
			lblDestino.html('<img src="../images/cargando.gif" title="Descargando Datos"');
			},
	 	success: function (respuestaHtml){
			lblDestino.html(respuestaHtml);
			},
	 	timeout:5000,
	 	error:function (){
			lblDestino.html('<img src="../images/warning_20x18.gif" title="Error en Conexi�n">');
			}
 		}); 
	}
function AjaxSubmit(strParametros, id_CampoDestino, paginaFuente){
	lblDestino= $("#" + id_CampoDestino);
  $.ajax({
		async:true,
		type: "POST",
	 	dataType: "text",
	 	contentType: "application/x-www-form-urlencoded",
		url: paginaFuente,
		data: strParametros,
		complete: function (){
			//$x('busqueda_de_Obra').focus();
			},
	 	beforeSend: function (){
			lblDestino.html('<img src="../images/cargando.gif" title="Descargando Datos"');
			},
	 	success: function (respuestaHtml){
			lblDestino.html(respuestaHtml);
			},
	 	timeout:5000,
	 	error:function (){
			lblDestino.html('<img src="../images/warning_20x18.gif" title="Error en Conexi�n">');
			}
 		}); 
	}
	

// Funcion para formatear numeros
function oNumero(numero)
	{
	//Propiedades 
	this.valor = numero || 0
	this.dec = -1;
	//M�todos 
	this.formato = numFormat;
	this.ponValor = ponValor;
	//Definici�n de los m�todos 
	function ponValor(cad)
	{
	if (cad =='-' || cad=='+') return
	if (cad.length ==0) return
	if (cad.indexOf('.') >=0)
		this.valor = parseFloat(cad);
	else 
		this.valor = parseInt(cad);
	} 
	function numFormat(dec, miles)
	{
	var num = this.valor, signo=3, expr;
	var cad = ""+this.valor;
	var ceros = "", pos, pdec, i;
	for (i=0; i < dec; i++)
	ceros += '0';
	pos = cad.indexOf('.')
	if (pos < 0)
		cad = cad+"."+ceros;
	else
		{
		pdec = cad.length - pos -1;
		if (pdec <= dec)
			{
			for (i=0; i< (dec-pdec); i++)
				cad += '0';
			}
		else
			{
			num = num*Math.pow(10, dec);
			num = Math.round(num);
			num = num/Math.pow(10, dec);
			cad = new String(num);
			}
		}
	pos = cad.indexOf('.')
	if (pos < 0) pos = cad.lentgh
	if (cad.substr(0,1)=='-' || cad.substr(0,1) == '+') 
		   signo = 4;
	if (miles && pos > signo)
		do{
			expr = /([+-]?\d)(\d{3}[\.\,]\d*)/
			cad.match(expr)
			cad=cad.replace(expr, RegExp.$1+','+RegExp.$2)
			}
	while (cad.indexOf(',') > signo)
		if (dec<0) cad = cad.replace(/\./,'')
			return cad;
	}
}//Fin del objeto oNumero:

function compare_dates(fecha_final, fecha_inicial)   
  {  
	
	if (fecha_inicial == "" || fecha_final == "")
		{
			return(true);
		}else{
			var xDay=fecha_final.substring(0,2); 
			var xMonth=fecha_final.substring(3,5);   
			var xYear=fecha_final.substring(6,10);   
			var yDay=fecha_inicial.substring(0,2);   
			var yMonth=fecha_inicial.substring(3,5);
			var yYear=fecha_inicial.substring(6,10);   
			if (xYear> yYear)   
			{   
					return(true)   
			}   
			else  
			{   
				if (xYear == yYear)   
				{    
					if (xMonth> yMonth)   
					{   
							return(true)   
					}   
					else  
					{    
						if (xMonth == yMonth)
						
						{   
							if (xDay > yDay){   
								return(true);  
							}
							else if(xDay == yDay){
								return(true);
							}
							else{  
								return(false);   
							}
						}   
						else  
							return(false);   
					}   
				}   
				else  
					return(false);   
			} 
		}
}
function submitFormAjax(id_form,id_div,fnc_before,fnc_success){
	var frm = $('#'+id_form+'');
	var Success = fnc_success;
	 $.ajax({
			url:   frm.attr('action'),
			type:  frm.attr('method'),
			data:  frm.serialize(),
			beforeSend: function () {
					fnc_before;
			},
			error: function(xhr, ajaxOptions, thrownError){
					if (xhr.status != ""){
						var msg = "<br>Ha ocurrido un error: ";
						$('#'+id_div+'').html('<div class="notification-wrap failuer" style="font-size:14px"><span class="icon-failure">Error:</span> '+ 
						xhr.status +' </strong><br><p>' + msg + "<strong>" + xhr.statusText + '</strong></p></div>');
					}
				},
			success:  function (response) {
					
					$('#'+id_div+'').html(response);
					Success;
			}
        }); 
}

function compare_periodos_contables(fecha_final, fecha_inicial){  

	if (fecha_inicial == "" || fecha_final == ""){
		return(true);
	}else{
		//	var xDay=fecha_final.substring(0,2); 
		var xMonth=fecha_final.substring(0,2);   
		var xYear=fecha_final.substring(3,7);   
		//	var yDay=fecha_inicial.substring(0,2);   
		var yMonth=fecha_inicial.substring(0,2);
		var yYear=fecha_inicial.substring(3,7); 
		if(xYear > yYear){
			return true;
		}else if(xYear == yYear && xMonth >= yMonth){
			return true;
		}else {
			alert('El fecha Inicial es mayor al fecha final.');
			return false;
		}
		//if (xYear != yYear){   
//			alert('El a' +String.fromCharCode(241) +'o del Periodo Contable Inicial y Final debe ser el mismo.');
//			return(false);   
//		}else{   
//			if (xMonth>= yMonth){   
//				return(true);   
//			}else{    
//				alert('El periodo Inicial es mayor al periodo Final.');
//				return(false);   
//			}
//		}   
	}
}


// ========================================== INICA SCRIPT DE MSG ========================================== //

var MSGTIMER = 20;
var MSGSPEED = 5;
var MSGOFFSET = 3;
var MSGHIDE = 3;

// build out the divs, set attributes and call the fade function //
function inlineMsg(target,string,autohide) {
  var msg;
  var msgcontent;
  if(!document.getElementById('msg')) {
    msg = document.createElement('div');
    msg.id = 'msg';
    msgcontent = document.createElement('div');
    msgcontent.id = 'msgcontent';
    document.body.appendChild(msg);
    msg.appendChild(msgcontent);
    msg.style.filter = 'alpha(opacity=0)';
    msg.style.opacity = 0;
    msg.alpha = 0;
  } else {
    msg = document.getElementById('msg');
    msgcontent = document.getElementById('msgcontent');
  }
  msgcontent.innerHTML = string;
  msg.style.display = 'block';
  var msgheight = msg.offsetHeight;
  var targetdiv = document.getElementById(target);
  targetdiv.focus();
  var targetheight = targetdiv.offsetHeight;
  var targetwidth = targetdiv.offsetWidth;
  var topposition = topPosition(targetdiv) - ((msgheight - targetheight) / 2);
  var leftposition = leftPosition(targetdiv) + targetwidth + MSGOFFSET;
  msg.style.top = topposition + 'px';
  msg.style.left = leftposition + 'px';
  clearInterval(msg.timer);
  msg.timer = setInterval("fadeMsg(1)", MSGTIMER);
  if(!autohide) {
    autohide = MSGHIDE;  
  }
  window.setTimeout("hideMsg()", (autohide * 1000));
}

// hide the form alert //
function hideMsg(msg) {
  var msg = document.getElementById('msg');
  if(!msg.timer) {
    msg.timer = setInterval("fadeMsg(0)", MSGTIMER);
  }
}

// face the message box //
function fadeMsg(flag) {
  if(flag == null) {
    flag = 1;
  }
  var msg = document.getElementById('msg');
  var value;
  if(flag == 1) {
    value = msg.alpha + MSGSPEED;
  } else {
    value = msg.alpha - MSGSPEED;
  }
  msg.alpha = value;
  msg.style.opacity = (value / 100);
  msg.style.filter = 'alpha(opacity=' + value + ')';
  if(value >= 99) {
    clearInterval(msg.timer);
    msg.timer = null;
  } else if(value <= 1) {
    msg.style.display = "none";
    clearInterval(msg.timer);
  }
}

// calculate the position of the element in relation to the left of the browser //
function leftPosition(target) {
  var left = 0;
  if(target.offsetParent) {
    while(1) {
      left += target.offsetLeft;
      if(!target.offsetParent) {
        break;
      }
      target = target.offsetParent;
    }
  } else if(target.x) {
    left += target.x;
  }
  return left;
}

// calculate the position of the element in relation to the top of the browser window //
function topPosition(target) {
  var top = 0;
  if(target.offsetParent) {
    while(1) {
      top += target.offsetTop;
      if(!target.offsetParent) {
        break;
      }
      target = target.offsetParent;
    }
  } else if(target.y) {
    top += target.y;
  }
  return top;
}

// preload the arrow //
if(document.images) {
  arrow = new Image(7,80); 
  arrow.src = "../images/msg_arrow.gif"; 
}

function validarFecha(strDate) 
{
	if (strDate.length != 10) 
		return false;
	var dateParts = strDate.split("/");
	var date = new Date(dateParts[2], (dateParts[1] - 1), dateParts[0]);
	if (date.getDate() == dateParts[0] && date.getMonth() == (dateParts[1] - 1) && date.getFullYear() == dateParts[2]) {
		return true;
	}
	else return false;
}

/*Abre una ventana tipo Pop Up tipo modal*/
function AbrirPopUpBusquedaChrome(idControlRecibeValor, strPaginaBusqueda, strParametrosPagina, anchoPagina, altoPagina){
	identificador= new Date();
	$x(idControlRecibeValor).value= '';
	ControlRecibeValor= $x(idControlRecibeValor);
	valor_anterior= ControlRecibeValor.value;
	var centerLeft = Number((screen.width/2)-(parseInt(anchoPagina)/2));
    var centerTop = Number((screen.height/2)-(parseInt(altoPagina)/2));	
	//valor_devuelto= showModalDialog(strPaginaBusqueda + '?identificador=' + identificador.getTime() + '&idControlRecibeValor=' + idControlRecibeValor + '&' + strParametrosPagina,'','dialogHeight:' + altoPagina + ';dialogWidth:' + anchoPagina + ';');
    valor_devuelto = window.open(strPaginaBusqueda + '?identificador=' + identificador.getTime() + '&idControlRecibeValor=' + idControlRecibeValor + '&' + strParametrosPagina,'Ventana',"status=0,toolbar=0, height=" + altoPagina + ",width=" + anchoPagina + ",top=" + centerTop + ",left="+ centerLeft);
	//se comprueba que el valor anterior y el valor de ControlRecibeValor sea igual, en ese caso no se asigno directamente por opener.document desde la pagina pop

    //if (valor_anterior == ControlRecibeValor.value)		
		//ControlRecibeValor.value = valor_devuelto;
	ControlRecibeValor.focus();
	cambiarFoco(ControlRecibeValor);
		
}	


/*Funcion de JSON*/
function toJson(obj) 
{
	var json = {};
	$.each(obj,function(ind, el){
		switch(typeof el){
			case 'object':
				if(el.DATA !== undefined && el.COLUMNS !== undefined){
					var recArr = [];
					$.each(el.DATA,function(ind,ele){
						var rec = new Object();
						$.each(el.COLUMNS,function(pos,nm){
							rec[nm.toLowerCase()] = ele[pos];
						});
						recArr.push(rec);
					});
					json[ind] = recArr;
				} else {
					json[ind] = $.serializeCFJSON(el);
				}
				break;
			default:
				json[ind] = el;
				break;
		}
	});
	return json;
}

/*FUNCION PARA ABRIR NUEVA VENTANA CON POST Y NO MOSTRAR LOS DATOS EN URL*/
/*04-MARZO-2014 : ROBERTO MEDRANO*/
function fOpenWindow(url, type, data, target) {
	/*SE CREA UN FORM EN EL PROPIO DOCUMENTO*/
	var form = document.createElement("form");
	/*SE TOMA LA URL PARA EL ACTION*/
	form.action = url;
	/*METODO POST POR DEFECTO PARA NO MOSTRAR DATOS POR URL*/
	form.method = type ? type : 'POST';
	/*TARGET DEFECTO _blank PARA NUEVA VENTANA*/
	form.target = target ? target : '_blank';
	/*OCULTAR EL FORM*/
	form.style.display = 'none';
	/*VERIFICAR SI SE ENVIAN DATOS POR PARAMETROS*/
	if (data) {
		/*RECORRER TODO EL DATA*/
		for (var param in data) {
			/*POR CADA DATA CREAR UN INPUT*/
			var hidden = document.createElement("input");
			/*CREAR EL INPUT DE TIPO HIDDEN*/
			hidden.type = "hidden";
			/*A CADA INPUT HIDDEN ASIGNARLE EL NOMBRE QUE LLEVA EL PARAMETRO*/
			hidden.name = param;
			/*ASIGNARLE EL VALOR AL HIDDEN QUE TRAE EL DATA ACTUAL, SI ES UN OBJETO SE PARSEA*/
			hidden.value = typeof data[param] === "object" ? JSON.stringify(data[param]) : data[param];
			/*SE AGREGA EL INPUT HIDDEN AL FORM*/
			form.appendChild(hidden);
		}
	}
	/*DESPUES DE AGREGAR TODOS LOS INPUT HIDDEN (PARAMETROS DEL DATA), AGREGAR EL FORM AL DOCUMENT*/
	document.body.appendChild(form);
	/*HACERLE SUBMIT AL FORM*/
	form.submit();
}
function ajaxRequest(obj){
	config = obj.config;
	data = obj.data;	
	onBeforeSend = obj.onBeforeSend;
	onSuccess = obj.onSuccess;
	onError = obj.onError;
	scope = obj.scope || {};
	$.ajax({
		cache: config.cache ? config.cache : false,
		type: config.type ? config.type : 'POST',
		url: config.url,
        dataType: config.dataType ? config.dataType : 'json',
		data: data,
		scope: null,
		beforeSend: function(){
			if(onBeforeSend) onBeforeSend();			
		},
		success: function(response){
			if(onSuccess) onSuccess(response);
		},
		error: function(xhr, ajaxOptions, thrownError){
			if(onError) onError(xhr, ajaxOptions, thrownError);
		}
	});
}
var cambiarFocus = function(pop_up) {
    var nom_campo = $($(pop_up).prev())[0].id;
    var valor = $("#" + nom_campo).val();
    if (valor != "") {
        $("#" + nom_campo).focus().blur();
    }
}

var noback = function() { //Bloquea el boton atras del navegador
    window.location.hash = "no-back-button";
    window.location.hash = "Again-No-back-button" //chrome
    window.onhashchange = function() {
        window.location.hash = "no-back-button";
    }
}

function validaFechaNoMayorActual(fecha, id) {
    var myDate = new Date();
    var valida

    if ((myDate.getMonth() + 1).length > 1) {
        var displayDate = (myDate.getDate()) + '/' + (myDate.getMonth() + 1) + '/' + myDate.getFullYear();
    } else {
        var displayDate = (myDate.getDate()) + '/' + '0' + (myDate.getMonth() + 1) + '/' + myDate.getFullYear();
    }

    var fechaActualString = displayDate.split("/");
    var fechaInputString = fecha.split("/");

    if (parseInt(fechaInputString[2]) > parseInt(fechaActualString[2])) {
        valida = 1;
    } else if (parseInt((fechaInputString[1] - 1)) > parseInt((fechaActualString[1] - 1)) && parseInt(fechaInputString[2]) >= parseInt(fechaActualString[2])) {
        valida = 1;
    } else if (parseInt(fechaInputString[0]) > parseInt(fechaActualString[0]) && parseInt((fechaInputString[1] - 1)) >= parseInt((fechaActualString[1] - 1)) &&
        parseInt(fechaInputString[2]) >= parseInt(fechaActualString[2])) {
        valida = 1;
    } else {
        return 0;
    }

    if (valida = 1) {
        inlineMsg(id, 'La Fecha no puede ser mayor a la actual');
        $('#' + id).val(displayDate);
    }
}


function validaFechaNoMenorActual(fecha, id) {
    var myDate = new Date();
    var valida

    if ((myDate.getMonth() + 1).length > 1) {
        var displayDate = (myDate.getDate()) + '/' + (myDate.getMonth() + 1) + '/' + myDate.getFullYear();
    } else {
        var displayDate = (myDate.getDate()) + '/' + '0' + (myDate.getMonth() + 1) + '/' + myDate.getFullYear();
    }

    var fechaActualString = displayDate.split("/");
    var fechaInputString = fecha.split("/");

    if (parseInt(fechaInputString[2]) < parseInt(fechaActualString[2])) {
        valida = 1;
    } else if (parseInt((fechaInputString[1] - 1)) < parseInt((fechaActualString[1] - 1)) && parseInt(fechaInputString[2]) <= parseInt(fechaActualString[2])) {
        valida = 1;
    } else if (parseInt(fechaInputString[0]) < parseInt(fechaActualString[0]) && parseInt((fechaInputString[1] - 1)) <= parseInt((fechaActualString[1] - 1)) &&
        parseInt(fechaInputString[2]) <= parseInt(fechaActualString[2])) {
        valida = 1;
    } else {
        return 0;
    }

    if (valida = 1) {
        inlineMsg(id, 'La Fecha no puede ser menor a la actual');
        $('#' + id).val(displayDate);
    }
}    

function listado_ajax(object){
	var id = object.id;
	var url = object.url;
	var startrow = object.startrow;
	var long = $('#'+id).children().length;
	var dataSet = new Array();
	var atributos = new Array();
	var attr = '';
	var NextPage = '';
	var EndPage = '';
	var BackPage = '';
	var FirstPage = '';
	var group_temp = new Array();
	var varCols = new Array();
	var varRows = new Array();
	try{
		var maxrows = $('#'+id)[0].attributes.maxrows.value;
	}catch(err){
		var maxrows = null;	
	}
/*=================================Envia peticion al componente y convierte el rs en un arreglo=================================================================================*/
	$.ajax({
		type:'get',
		url:url,
		dataType:'json',
		async:false,
		cache:false,
		success:function(response){
			if(response.SUCCESS == true){
				for(var x=0;x<response.RS.DATA.length;x++)
				{					
					var obj = {}
					for(i=0;i<response.RS.DATA[x].length;i++){
						var valor = response.RS.DATA[x][i];
						var propiedad = response.RS.COLUMNS[i];						
						obj[propiedad] = valor;		
					}
					dataSet.push(obj);
				}
			}else{
				console.log(response)	
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {
        	console.log(xhr.success());
			console.log(ajaxOptions);
       	 	console.log(thrownError);
      }
	});
	$('[id*=tb_listado_'+id+']').remove();
	if(dataSet.length == 0)return false;
	if(maxrows == null || maxrows > dataSet.length || maxrows == ''){
		 maxrows = dataSet.length;	
	}
	if(startrow == null)startrow=0;
	maxrows = (parseInt(maxrows) + startrow);
	for(var x=0;x<long;x++){
		var atributos_td = $('#'+id).children()[x].attributes;
		for(var i=0;i<atributos_td.length;i++){
			if(attr != ''){
				attr = attr+' '+atributos_td[i].name+'="'+atributos_td[i].value+'"';
			}else{
				attr = ''+atributos_td[i].name+'="'+atributos_td[i].value+'"';
			} 
			
			atributos[x] = attr;
			if(i == atributos_td.length -1)attr='';
		}
		
	}	
	for( x=startrow;x<maxrows;x++){
		if(x == dataSet.length){
			maxrows = x
			break;
		}
		varCols = new Array();
		record = dataSet[x];
		try{
			var groups = $('#'+id)[0].attributes.groups;
		}catch(err){
			var groups = null;
		}
		try{
			var groupsattr = $('#'+id)[0].attributes.groupsattr;	
		}catch(err){
			var groupsattr = null;
		}
		
		if(groups != null){	
			groups = groups.value.split('||');
			if( groupsattr )
				groupsattr = groupsattr.value.split('||');
			else
				groupsattr = new Array();

			for(var y=0;y<groups.length;y++){
				group = groups[y].split(/[{}]+/);
				if( !groupsattr )
					groupsattr[y] = '';
				for(var i=0;i<group.length;i++){
					group_value = group[i];
					if(!group_value.indexOf(id+'.')){
						group_value = group_value.replace(id+'.','');
						var val = record[group_value.toUpperCase()];
						if(val == null)val='';
						groups[y] = groups[y].replace(group_value,val);
						groups[y] = groups[y].replace('{','');
						groups[y] = groups[y].replace('}','');
						groups[y] = groups[y].replace(id+'.','');
					}
				}
				if(group_temp[y] != groups[y]){
					group_temp[y] = groups[y];
					$('#'+id).before('<tr id="tb_listado_'+id+'"><td colspan='+long+' '+groupsattr[y]+'>'+groups[y]+'</td></tr>');
				}
			}
		}
		for(var j=0;j<long;j++){
			var html = $('#'+id).children()[j].innerHTML;
			var decimal = null;
			if($('#'+id).children()[j].attributes.decimal != null ){
				var decimal = $('#'+id).children()[j].attributes.decimal.value
			}
			var value = html.split(/[{}]+/);
			for(var i=0;i<value.length;i++){
				str = value[i];
				if(!str.indexOf(id+'.')){
					html = html.replace(str, str.toUpperCase());
					var val = record[str.replace(id+'.','').toUpperCase()]
					
					if(val == null)val='';
					if(decimal != null)val=val.toFixed(decimal); 
					html = html.replace(str.toUpperCase(), val)
					html = html.replace('{','');
					html = html.replace('}','');
				}
			}
			varCols.push('<td '+atributos[j]+'>'+html+'</td>');
		}
		$('#'+id).before('<tr id="tb_listado_'+id+'">'+varCols+'</tr>');
	}
	
	if(maxrows < dataSet.length){
		var NextPage = '<button type="button" id="NextPage_'+id+'" style="border:none;background:none;">'+
							'<img style="width:100%;height:100%"  src="../images/siguiente.png" />'+
					   '</button>';
		var EndPage = '<button type="button" id="EndPage_'+id+'" style="border:none;background:none;">'+
							'<img style="width:100%;height:100%"  src="../images/ultimo.png" />'+
					  '</button>';			   
	}
	if(startrow > 0){
		var BackPage = 	'<button type="button" id="BackPage_'+id+'" style="border:none;background:none;">'+
							'<img style="width:100%;height:100%"  src="../images/anterior.png" />'+
						'</button>';
		var FirstPage = '<button type="button" id="FirstPage_'+id+'" style="border:none;background:none;">'+
							'<img style="width:100%;height:100%"  src="../images/primero.png" />'+
						'</button>';				
	}
	$('#'+id).after('<tr id="tb_listado_'+id+'">'+
						'<td colspan="'+j+'" align="center" style="font-weight:bold;">'+
							'<table width="100%" height="100%" style="border:1px solid;">'+
								'<tr >'+
									'<td width="20%" style="border:none;" align="right">'+FirstPage+'</td>'+
									'<td width="10%" style="border:none;" align="right">'+BackPage+'</td>'+
									'<td width="30%" align="center" style="border:none;">'+
										'Registros: '+(startrow + 1)+' al '+maxrows+' de '+dataSet.length+' &nbsp;'+
									'</td>'+	
									'<td width="10%" style="border:none;">'+NextPage+'</td>'+
									'<td width="20%" style="border:none;">'+EndPage+'</td>'+
								'</tr>'+
							'</table>'+	
						'</td>'+
					'</tr>');
	$('#NextPage_'+id+'').click(function(){
		listado_ajax({
					 	id:id,
						url:url,
						startrow:x
					 });
	});
	$('#EndPage_'+id+'').click(function(){
		if(dataSet.length > $('#'+id)[0].attributes.maxrows.value){
			startrow = dataSet.length - $('#'+id)[0].attributes.maxrows.value
		}else if(dataSet.length < $('#'+id)[0].attributes.maxrows.value){
			startrow = $('#'+id)[0].attributes.maxrows.value - dataSet.length
		}
		listado_ajax({
					 	id:id,
						url:url,
						startrow:dataSet.length - $('#'+id)[0].attributes.maxrows.value
					 });
	});
	$('#BackPage_'+id+'').click(function(){
		startrow = 	startrow - parseInt($('#'+id)[0].attributes.maxrows.value);
		if(startrow <= 0){
			startrow = 0	
		}
		listado_ajax({
					 	id:id,
						url:url,
						startrow:startrow 
					 });
	});
	$('#FirstPage_'+id+'').click(function(){
		listado_ajax({
					 	id:id,
						url:url,
						startrow:0
					 });
	});
}

function validar_numero( event, t )
{
	if ((event.which != 46 || $(t).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) 
	{
		event.preventDefault();
	}
}