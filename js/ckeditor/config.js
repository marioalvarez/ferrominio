

CKEDITOR.editorConfig = function( config )
{

///////////////////////Variables de configuracion//////////////////////////////////
    config.language = 'es';
    config.uiColor  = '#8ca9cf';
    config.scayt_autoStartup = false;
	config.scayt_sLang = 'es_ES';

/////////////////Configuracion de los toolbars//////////////////////////////////////
    /*Configuracion del Toolbar del editor de contratos*/
    config.toolbar  = 'EditorContratos';
	config.toolbar_EditorContratos =
	[
        
            { name: 'document', items : [ 'Source','-','NewPage','DocProps','Preview','Print','-','Templates' ] },
	        { name: 'clipboard', items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
	        { name: 'editing', items : [ 'Find','Replace','-','SelectAll','-','SpellChecker', 'Scayt' ] },
            { name: 'tools', items : [ 'Maximize', 'ShowBlocks', ] },
	        '/',
	        { name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ] },
	        { name: 'paragraph', items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote',
	        '-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock' ] },
	        { name: 'insert', items : [ 'Image','Table','HorizontalRule','SpecialChar','PageBreak' ] },
	        '/',
	        { name: 'styles', items : [ 'Format','Font','FontSize' ] },
	        { name: 'colors', items : [ 'TextColor','BGColor' ] }
	];
};
