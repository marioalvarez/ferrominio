/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * Empresa:     RedRabbit                                  *
 * Programador: Porfirio Padilla                           *
 * Fecha:       Enero de 2011                              *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
var contenido = "frm_content";
var titulo    = "span_cu";
var myLayout; // a var is required because this page utilizes: myLayout.allowOverflow() method

$(document).ready(function () {//funcion para crear los compoentes con JQuery que contenga la pagina
       var initClosedEast = false;
       var modulo = gup("modulo");
       var url    = gup("URL");
       var title  = gup("title");
       if(modulo != "" && url != "" &&title != ""){
             initClosedEast = true;
       }
       //Inicializacion del Layout
       myLayout = $('body').layout({ 
               applyDefaultStyles:      true 
             , north__spacing_open:     0 
             , north__minSize:          20
             , east__initClosed:        initClosedEast
             , east__closable:          true
             , east__slideTrigger_open: 'mouseover' //Evento que va a hacer que el panel se abra
             , east__spacing_open:      5
             , east__spacing_closed:    5
             , east__maxSize:           125
             , east__minSize:           125
             , west__initClosed:        false
             , west__slideTrigger_open: 'mouseover'
             , west__maxSize:           300
             , west__minSize:           200
             , west__spacing_open:      5
              , west__spacing_closed:   5
             , south__initClosed:       false //Valor Original: true
             , contentSelector:         '.content' //Para el div conten que sea el unicoque tenga scroll
       }); 
       //myLayout.addPinBtn( "#tbarPinWest", "west" );  //Barra Izquierda
       //Si el elemento existe o esta visible
       if( $('#tbarPinEast').length > 0 ){
            myLayout.addPinBtn( "#tbarPinEast", "east" );  //Barra Derecha
       }
       //Inicializacion del Treeview
       //initTree();
             
       if(initClosedEast){
             master_cargarMenu(unescape(modulo), unescape(url), unescape(title));
             myLayout.hide("east");
       }
       else{
             //Cambiar los links para que funcionen con el modelo actual de iFrame
              master_cambiarLinks();
       }
});
//http://www.anieto2k.com/2006/08/17/coge-los-parametros-de-tu-url-con-javascript/
function gup( name ){
       var regexS = "[\\?&]"+name+"=([^&#]*)";
       var regex = new RegExp ( regexS );
       var tmpURL = window.location.href;
       var results = regex.exec( tmpURL );
       if( results == null ) return"";
       else return results[1];
}

	
function master_cargar(url, cu){
	document.getElementById(contenido).src = url;
	document.getElementById(titulo).innerHTML = cu;
	void(0);
}
	
function master_cambiarLinks(){
	var links = document.getElementsByTagName("a");
	for(var i=0; i<links.length; i++){
		var temp = links[i];
		try{
			if(temp.id*1 > 100)
				temp.href = "javascript:master_cargar('" + temp.href + "', '" + temp.title + "')";
		}
		catch(e){}
	}
}

function master_cargarMenu(id, url, titulo) { //Funcion Principal
     $('#span_cu').html(titulo);

    $( "#frm_content" ).attr('src', url);
/*var fecha = new Date();
	var hash = fecha.getSeconds() + "-" + fecha.getMilliseconds();
	
	//Hebert GB, L�nea para agregar titulo a la secci�n de menu del m�dulo
	//document.getElementById('titulo_menu_modulo').innerHTML= titulo;
	
	//var url = "menu_de_opciones.cfm?modulo=" + id + "&hash="+hash;
	var id_contenedor = "menu_modulo";
    var pagina_requerida = false;
    if (window.XMLHttpRequest) { pagina_requerida = new XMLHttpRequest(); } //Mozilla y Safari
    else if (window.ActiveXObject) { //Diferentes versiones de IE
        try { pagina_requerida = new ActiveXObject("Msxml2.XMLHTTP"); }
        catch (e) {
            try { pagina_requerida = new ActiveXObject("Microsoft.XMLHTTP"); }
            catch (e) { }
        }
    }
    else return false;
    pagina_requerida.onreadystatechange = function() {
        master_cargarpagina(pagina_requerida, id_contenedor, bienvenida);
    }
    pagina_requerida.open("GET", url, true);
    pagina_requerida.send(null);*/
	
}
//Funcion Secundaria
function master_cargarpagina(pagina_requerida, id_contenedor, bienvenida) {
    try {
        if (pagina_requerida.readyState == 4 && (pagina_requerida.status == 200 || window.location.href.indexOf("http") == -1)) {
            var contenido = pagina_requerida.responseText;
            document.getElementById(id_contenedor).innerHTML = contenido;
			initTree();
			master_cambiarLinks();
			master_cargar(bienvenida, "");
        }
    }
    catch (e) { alert(e); }
}
