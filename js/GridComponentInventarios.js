// JavaScript Document
var GridEmpleados = function(object)
{	
	this.id = object.id;			
	this.total = null;
	this.groupSupervisor = object.groupSupervisor;
	this.columns = object.columns;			
	this.url = object.url;
	//this.page = 1;
	//this.pageSize = object.pageSize;
	this.data = object.data;
	this.dataSet = [];
	//this.totalPages = 0;
	this.selectedItem = -1;
	this.loadingMessage = object.loadingMessage ? object.loadingMessage : "Cargando...";
	this.events = {};
	
	this.getRecord = function(index){				
		return this.dataSet[index];
	}
	
	this.getDataSet = function(){
		return this.dataSet;
	}
	
	this.setSelectedItem = function(index){
		this.selectedItem = index;
	}
	
	this.getSelectedItem = function(){
		return this.selectedItem;
	}
	
	
	this.load = function()
	{				
		this.data = arguments[0] ? arguments[0] : this.data;
		scopeGrid = this;
		//console.log(scopeGrid);
		$.ajax({
			cache: false,
			type: 'POST',
			url: scopeGrid.url,
			dataType: 'json',
			data: scopeGrid.data,			
			//beforeSend: function(){
				//$("#" + scopeGrid.id).mask(scopeGrid.loadingMessage);			
			//},
			success: function(response){
				//$("#" + scopeGrid.id).unmask();
				
				if(response.SUCCESS == true){	
					
					scopeGrid.fillDataSet(response);
					scopeGrid.dispatchEvent("onLoad",response);
				}else{
					scopeGrid.dispatchEvent("onError",response);
				}
			},
			error: function(xhr, ajaxOptions, thrownError){
				scopeGrid.dispatchEvent("onError",xhr);
			}
		});
	};
	this.fillDataSet = function(data)
	{
		if(data.RS.DATA.length > 0)
		{
			this.setSelectedItem(-1);
			this.dataSet = new Array();				
			this.total = data.RS.DATA[0][data.RS.COLUMNS.length-1];
			for(x=0;x<data.RS.DATA.length;x++){					
				var obj = {}
				for(i=0;i<data.RS.DATA[x].length;i++){
					var valor = data.RS.DATA[x][i];
					var propiedad = data.RS.COLUMNS[i];						
					obj[propiedad] = valor;	
					
				}
				/* GUARDAR EN UN OBJETO TODOS LOS VALORES DEVUELTOS */
				this.dataSet.push(obj);											
			}
			this.fillGrid();	
								
		}else
		{
			this.dataSet = new Array();
			this.emptyGrid();
		}	
		
	}
	
	this.fillGrid = function()
	{	contador = 0;
		temp = 0;
		$("#" + this.id + " tbody").empty();				
		arrayCols = new Array();
		arrayGroup = new Array();
		for(x=0;x<this.dataSet.length;x++)
		{					
			record = this.dataSet[x];
			for(i=0;i<this.groupSupervisor.length;i++)
			{
				temp = temp;
				//console.log(temp);
				value = record[this.groupSupervisor[i].dataIndex.toUpperCase()];
				Name = record[this.groupSupervisor[i].dataName.toUpperCase()];
				if(value != temp )
				{
				
					//console.log(value);
					if(value == null) value = "";			
					contador = contador +1;
					//console.log(contador);
					group = '<td class="cuadricula" colspan="11" align="left" style="font-weight:bold">'+value+'-'+Name+' </td>';
					temp = value;
					//console.log(temp);
				}
				//else if(value == temp)
				//{
				//console.log(temp);
					for(i=0;i<this.columns.length;i++)
					{
						
						if(this.columns[i].type == 'text')
						{	
							color = 'color:#FF0000';
							value = record[this.columns[i].dataIndex.toUpperCase()];
							Name = record[this.columns[i].dataName.toUpperCase()];
							css = this.columns[i].css ? 'align="' + this.columns[i].css + '"' :  "";						
							if(value == null) value = "";
							if(Name != null)
							{
								title = record.NUM_TELEFONO;
								if(record.PUESTO == 'Entrenamiento')
								{
									column = '<td ' + css + ' class="cuadricula" style="color:#FF0000" title="Telefono de la obra:'+' '+ title+'">'+value+'-'+Name+'</td>';
								}
								else if(record.PUESTO == 'Auxiliar')
								{
									column = '<td ' + css + ' class="cuadricula" style="color:#009900" title="Telefono de la obra:'+' '+ title+'">'+value+'-'+Name+'</td>';
								}else
								{
									column = '<td ' + css + ' class="cuadricula" title="Telefono de la obra:'+' '+ title+'">'+value+'-'+Name+'</td>';
								}
							}
							else
							{ 
								if(record.PUESTO == 'Entrenamiento')
								{
									column = '<td ' + css + ' class="cuadricula" style="color:#FF0000">'+value+'</td>';
								}else if(record.PUESTO == 'Auxiliar') 
								{	
									column = '<td ' + css + ' class="cuadricula" style="color:#009900">'+value+'</td>';
								}else
								{
									column = '<td ' + css + ' class="cuadricula" >'+value+'</td>';
								}
							}
						}else if(this.columns[i].type == 'check')
						{
							css = this.columns[i].css ? 'align="' + this.columns[i].css + '"' :  "";
							onClick = this.columns[i].onClick;
							dataI = record[this.columns[i].dataI.toUpperCase()];
							dataJ = record[this.columns[i].dataJ.toUpperCase()];
							if(record.PUESTO == 'Entrenamiento')
							{
								column = '<td ' + css + ' class="cuadricula" ><input type="checkbox" checked="checked" onclick=javascript:'+onClick+'('+dataJ+','+dataI+')></td>';
							}else
							{
								column = '<td ' + css + ' class="cuadricula" ><input type="checkbox" onclick=javascript:'+onClick+'('+dataJ+','+dataI+')></td>';
							}
						}else if(this.columns[i].type == 'img')
						{
							css = this.columns[i].css ? 'align="' + this.columns[i].css + '"' :  "";
							value = record[this.columns[i].dataIndex.toUpperCase()];
							puesto = record[this.columns[i].dataP.toUpperCase()];
							obra = record[this.columns[i].dataO.toUpperCase()];
							cargo1 = record[this.columns[i].dataC.toUpperCase()];
							cargo = cargo1.toString();
							onClick = this.columns[i].onClick;
							//console.log(cargo);
							column = '<td ' + css + ' class="cuadricula" ><img src="../images/delete.png" alt="Clic para eliminar paises" border="0" title=	"ELIMINAR" onClick="javascript:'+onClick+'('+value+','+puesto+','+obra+','+'cargo'+')"></td>';
						}
						arrayCols.push(column);	
			
				}
				
				arrayGroup.push(group);
			}
			
		}
		index = 0;
		index2 = 0;
		total =  this.columns.length;
		total2 = this.groupSupervisor.length;
		column = "";
		group = "";
		temp = arrayGroup[0];
		color = 'color:#FF0000';
		colorM = '##99CCCC';
		mouse = 'this.style.backgroundColor='+colorM+'';
		onMouseOver="this.style.backgroundColor='#99CCCC'" ; 
		onMouseOut="this.style.backgroundColor=''";
		console.log(total2);
		for(j=index2;j<total2;j++)
		{
			//console.log(group);
			group = arrayGroup[j];	
			if(temp != group)
			{
				j2 = (j) * this.columns.length;
				temp = group;
				group = arrayGroup[j-1];
				$("#" + this.id + " tbody").append("<tr >" + group +  "</tr>");
				
				
				for(r=index;r<total;r++)
				{	
					
					column = column + arrayCols[r];
					if(r == total - 1)
					{
						$("#" + this.id + " tbody").append("<tr onmouseout="+onMouseOut+" onmouseover="+onMouseOver+">" + column +  "</tr>");
						column = "";
						index = r +1;
						total = total + this.columns.length;
						if(total > j2 ){ break;}
					}
					if(r == j2  ) { break;}
				}
			}
			total2 = total2 + this.groupSupervisor.length;
			if(total2 > arrayGroup.length)
			{ 
				if(j+1 == arrayGroup.length)
				{
					temp = arrayGroup[0];
				}
				else
				{
				break;
				}
			}
		}
	}
				
};			


GridEmpleados.prototype.addEventListener = function (key, func) {
	if (!this.events.hasOwnProperty(key)) {
		this.events[key] = [];
	}
	this.events[key].push(func);
};

GridEmpleados.prototype.removeEventListener = function (key, func) {
	if (this.events.hasOwnProperty(key)) {
		for (var i in this.events[key]) {
			if (this.events[key][i] === func) {
				this.events[key].splice(i, 1);
			}
		}
	}
};
GridEmpleados.prototype.dispatchEvent = function (key, dataObj) {
	if (this.events.hasOwnProperty(key)) {		
		//dataObj = dataObj || {};		
		//dataObj.currentTarget = this;
		for (var i in this.events[key]) {
			this.events[key][i](dataObj);
		}
	}
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

var GridEmpleadosNoAsignados = function(object)
{	
	this.id = object.id;			
	this.total = null;
	this.columns = object.columns;			
	this.url = object.url;
	//this.page = 1;
	//this.pageSize = object.pageSize;
	this.data = object.data;
	this.dataSet = [];
	//this.totalPages = 0;
	this.selectedItem = -1;
	this.loadingMessage = object.loadingMessage ? object.loadingMessage : "Cargando...";
	this.events = {};
	
	this.getRecord = function(index){				
		return this.dataSet[index];
	}
	
	this.getDataSet = function(){
		return this.dataSet;
	}
	
	this.setSelectedItem = function(index){
		this.selectedItem = index;
	}
	
	this.getSelectedItem = function(){
		return this.selectedItem;
	}
	
	
	this.load = function()
	{				
		this.data = arguments[0] ? arguments[0] : this.data;
		scopeGrid = this;
		//console.log(scopeGrid);
		$.ajax({
			cache: false,
			type: 'POST',
			url: scopeGrid.url,
			dataType: 'json',
			data: scopeGrid.data,			
			//beforeSend: function(){
				//$("#" + scopeGrid.id).mask(scopeGrid.loadingMessage);			
			//},
			success: function(response){
				//$("#" + scopeGrid.id).unmask();	
				if(response.SUCCESS == true){												
					scopeGrid.fillDataSet(response);
					scopeGrid.dispatchEvent("onLoad",response);
				}else{
					scopeGrid.dispatchEvent("onError",response);
				}
			},
			error: function(xhr, ajaxOptions, thrownError){
				scopeGrid.dispatchEvent("onError",xhr);
			}
		});
	};
	this.fillDataSet = function(data)
	{
		if(data.RS.DATA.length > 0)
		{
			this.setSelectedItem(-1);
			this.dataSet = new Array();				
			this.total = data.RS.DATA[0][data.RS.COLUMNS.length-1];
			for(x=0;x<data.RS.DATA.length;x++){					
				var obj = {}
				for(i=0;i<data.RS.DATA[x].length;i++){
					var valor = data.RS.DATA[x][i];
					var propiedad = data.RS.COLUMNS[i];						
					obj[propiedad] = valor;	
					
				}
				/* GUARDAR EN UN OBJETO TODOS LOS VALORES DEVUELTOS */
				this.dataSet.push(obj);											
			}
			this.fillGrid();	
								
		}else
		{
			this.dataSet = new Array();
			this.emptyGrid();
		}	
		
	}
	
	this.fillGrid = function()
	{	contador = 0;
		temp = 0;
		$("#" + this.id + " tbody").empty();				
		arrayCols = new Array();
		for(x=0;x<this.dataSet.length;x++)
		{					
			record = this.dataSet[x];
			
			for(i=0;i<this.columns.length;i++)
			{
				
				if(this.columns[i].type == 'text')
				{	
					color = 'color:#FF0000';
					value = record[this.columns[i].dataIndex.toUpperCase()];
					Name = record[this.columns[i].dataName.toUpperCase()];
					css = this.columns[i].css ? 'align="' + this.columns[i].css + '"' :  "";						
					if(value == null) value = "";
					column = '<td ' + css + ' class="cuadricula" >'+value+'</td>';
				}else if(this.columns[i].type == 'check')
				{
					css = this.columns[i].css ? 'align="' + this.columns[i].css + '"' :  "";
					onClick = this.columns[i].onClick;
					dataI = record[this.columns[i].dataI.toUpperCase()];
					dataJ = record[this.columns[i].dataJ.toUpperCase()];
					if(record.PUESTO == 'Entrenamiento')
					{
						column = '<td ' + css + ' class="cuadricula" ><input type="checkbox" checked="checked" onclick=javascript:'+onClick+'('+dataJ+','+dataI+')></td>';
					}else
					{
						column = '<td ' + css + ' class="cuadricula" ><input type="checkbox" onclick=javascript:'+onClick+'('+dataJ+','+dataI+')></td>';
					}
				}else if(this.columns[i].type == 'img')
				{
					css = this.columns[i].css ? 'align="' + this.columns[i].css + '"' :  "";
					value = record[this.columns[i].dataIndex.toUpperCase()];
					onClick = this.columns[i].onClick;
					//console.log(cargo);
					column = '<td ' + css + ' class="cuadricula" ><img src="../images/delete.png" alt="Clic para eliminar paises" border="0" title=	"ELIMINAR" onClick="javascript:'+onClick+'('+value+')"></td>';
				}
				arrayCols.push(column);	
						
					//}
			}
				
			
			
		}
		index = 0;
		index2 = 0;
		total =  this.columns.length;
		column = "";
		onMouseOver="this.style.backgroundColor='#99CCCC'" ; 
		onMouseOut="this.style.backgroundColor=''";
		
				for(r=index;r<total;r++)
				{	
					column = column + arrayCols[r];
					if(r == total - 1)
					{
						$("#" + this.id + " tbody").append("<tr onmouseout="+onMouseOut+" onmouseover="+onMouseOver+">" + column +  "</tr>");
						column = "";
						index = r +1;
						total = total + this.columns.length;
						if(total > arrayCols.length ){ break;}
					}
					
				}
			
		
	}
				
};			


GridEmpleadosNoAsignados.prototype.addEventListener = function (key, func) {
	if (!this.events.hasOwnProperty(key)) {
		this.events[key] = [];
	}
	this.events[key].push(func);
};

GridEmpleadosNoAsignados.prototype.removeEventListener = function (key, func) {
	if (this.events.hasOwnProperty(key)) {
		for (var i in this.events[key]) {
			if (this.events[key][i] === func) {
				this.events[key].splice(i, 1);
			}
		}
	}
};
GridEmpleadosNoAsignados.prototype.dispatchEvent = function (key, dataObj) {
	if (this.events.hasOwnProperty(key)) {		
		//dataObj = dataObj || {};		
		//dataObj.currentTarget = this;
		for (var i in this.events[key]) {
			this.events[key][i](dataObj);
		}
	}
};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

var GridInsumos = function(object)
{	
	this.id = object.id;			
	this.total = null;
	this.columns = object.columns;			
	this.url = object.url;
	//this.page = 1;
	//this.pageSize = object.pageSize;
	this.data = object.data;
	this.dataSet = [];
	//this.totalPages = 0;
	this.selectedItem = -1;
	this.loadingMessage = object.loadingMessage ? object.loadingMessage : "Cargando...";
	this.events = {};
	
	this.getRecord = function(index){				
		return this.dataSet[index];
	}
	
	this.getDataSet = function(){
		return this.dataSet;
	}
	
	this.setSelectedItem = function(index){
		this.selectedItem = index;
	}
	
	this.getSelectedItem = function(){
		return this.selectedItem;
	}
	
	
	this.load = function()
	{				
		this.data = arguments[0] ? arguments[0] : this.data;
		scopeGrid = this;
		//console.log(scopeGrid);
		$.ajax({
			cache: false,
			type: 'POST',
			url: scopeGrid.url,
			dataType: 'json',
			data: scopeGrid.data,			
			//beforeSend: function(){
				//$("#" + scopeGrid.id).mask(scopeGrid.loadingMessage);			
			//},
			success: function(response){
				//$("#" + scopeGrid.id).unmask();	
				if(response.SUCCESS == true){												
					scopeGrid.fillDataSet(response);
					scopeGrid.dispatchEvent("onLoad",response);
				}else{
					scopeGrid.dispatchEvent("onError",response);
				}
			},
			error: function(xhr, ajaxOptions, thrownError){
				scopeGrid.dispatchEvent("onError",xhr);
			}
		});
	};
	this.fillDataSet = function(data)
	{
		if(data.RS.DATA.length > 0)
		{
			this.setSelectedItem(-1);
			this.dataSet = new Array();				
			this.total = data.RS.DATA[0][data.RS.COLUMNS.length-1];
			for(x=0;x<data.RS.DATA.length;x++){					
				var obj = {}
				for(i=0;i<data.RS.DATA[x].length;i++){
					var valor = data.RS.DATA[x][i];
					var propiedad = data.RS.COLUMNS[i];						
					obj[propiedad] = valor;	
					
				}
				/* GUARDAR EN UN OBJETO TODOS LOS VALORES DEVUELTOS */
				this.dataSet.push(obj);											
			}
			this.fillGrid();	
								
		}else
		{
			this.dataSet = new Array();
			this.emptyGrid();
			
		}	
		
	}
	
	this.fillGrid = function()
	{	contador = 0;
		temp = 0;
		currentrow =0;
		$("#" + this.id + " tbody").empty();				
		arrayCols = new Array();
		for(x=0;x<this.dataSet.length;x++)
		{					
			record = this.dataSet[x];
			currentrow = currentrow + 1;
			
			for(i=0;i<this.columns.length;i++)
			{
				
				if(this.columns[i].type == 'text')
				{	
					color = 'color:#FF0000';
					value = record[this.columns[i].dataIndex.toUpperCase()];
					Name = record[this.columns[i].dataName.toUpperCase()];
					css = this.columns[i].css ? 'align="' + this.columns[i].css + '"' :  "";						
					style = this.columns[i].style ? 'style="'+this.columns[i].style+'"' : "";
					if(value == null) value = "";
					if(style != null)
					{					
						column = '<td ' + css + ' '+style+' class="cuadricula">'+value+'</td>';
					}
					else
					{ 
						column = '<td ' + css + ' class="cuadricula" >'+value+'</td>';
						
					}
				
				}else if(this.columns[i].type == 'img')
				{
					css = this.columns[i].css ? 'align="' + this.columns[i].css + '"' :  "";
					value = record[this.columns[i].dataIndex.toUpperCase()];
					onClick = this.columns[i].onClick;
					//console.log(cargo);
					column = '<td ' + css + ' class="cuadricula" ><img src="../images/delete.png" alt="Click para eliminar " border="0" title= "ELIMINAR" onClick="javascript:'+onClick+'('+value+')"></td>';
				}else if(this.columns[i].type == 'number')
				{	
					color = 'color:#FF0000';
					dataIndex = record[this.columns[i].dataIndex.toUpperCase()];
					dataCR = record[this.columns[i].dataCR.toUpperCase()];
					if(dataIndex != null && dataCR != null)
					{
						Value = dataIndex - dataCR;
						value = parseFloat(Value).toFixed(6);
						id = currentrow;
					}else if(dataIndex != null && dataCR == null)
					{
						Value = record[this.columns[i].dataIndex.toUpperCase()];
						value = parseFloat(Value).toFixed(6);
						id = "";
					}else if(dataIndex == null && dataCR != null)
					{
						Value = record[this.columns[i].dataCR.toUpperCase()];
						value = parseFloat(Value).toFixed(6);
						id = "";
					}
					css = this.columns[i].css ? 'align="' + this.columns[i].css + '"' :  "";
					if(value == null) value = "";
					if (id != null)
					{
						column = '<td ' + css + ' class="cuadricula" id="'+id+'" >'+value+'</td>';
						//console.log(column);
					}else
					{
						column = '<td ' + css + ' class="cuadricula" >'+value+'</td>';
					}
				}else if(this.columns[i].type == 'operacion')
				{	
					
					color = 'color:#FF0000';
					dataIndex = record[this.columns[i].dataIndex.toUpperCase()];
					dataCR = record[this.columns[i].dataCR.toUpperCase()];					
					Value = dataIndex - dataCR;
					value = parseFloat(Value).toFixed(6);
					css = this.columns[i].css ? 'align="' + this.columns[i].css + '"' :  "";
					if(value == null) value = "";
					if(value > 0)
					{
						column = '<td ' + css + ' class="cuadricula" ><input type="text" size="15" class="contenido_flotante " name="nu_Cantidad_'+currentrow+'" value="'+value+'" maxlength="10" style="text-align:right;" " onClick="this.select();" onBlur="javascript:mensaje('+value+','+currentrow+')"	id="nu_Cantidad_'+currentrow+'"></td>'
						
					}else
					{
						column = '<td ' + css + ' class="cuadricula" ><input type="text" size="15" class="contenido_readOnly" value="0" readonly="yes" align="right" name="nu_Cantidad_'+currentrow+'" range="0,'+value+'" validate="float"	required="yes"	message="Cantidad Invalida para el insumo '+currentrow+'"	></td>';
					}
				}else if(this.columns[i].type == 'check')
				{
					css = this.columns[i].css ? 'align="' + this.columns[i].css + '"' :  "";
					if(value > 0)
					{
						column = '<td ' + css + ' class="cuadricula" ><input type="checkbox" onClick="RevisarCheck();"  name="chk_Insumo_'+currentrow+'" id="chk_Insumo_'+currentrow+'" value="'+record.ID_INSUMO+'" ></td>';
					}
					else
					{
						column = '<td ' + css + ' class="cuadricula" ><input type="checkbox" onClick="return false"  name="chk_Insumo_'+currentrow+'" id="chk_Insumo_'+currentrow+'" value="'+record.ID_INSUMO+'" ></td>';
					}
				}
				arrayCols.push(column);	
						
					//}
			}
				
			
			
		}
		index = 0;
		index2 = 0;
		total =  this.columns.length;
		column = "";
		onMouseOver="this.style.backgroundColor='#99CCCC'" ; 
		onMouseOut="this.style.backgroundColor=''";
		
				for(r=index;r<total;r++)
				{	
					column = column + arrayCols[r];
					if(r == total - 1)
					{
						$("#" + this.id + " tbody").append("<tr onmouseout="+onMouseOut+" onmouseover="+onMouseOver+">" + column +  "</tr>");
						column = "";
						index = r +1;
						total = total + this.columns.length;
						if(total > arrayCols.length ){ break;}
					}
					
				}
			
		
	}
	this.emptyGrid = function()
	{
		
		//return false;
	}
				
};			


GridInsumos.prototype.addEventListener = function (key, func) {
	if (!this.events.hasOwnProperty(key)) {
		this.events[key] = [];
	}
	this.events[key].push(func);
};

GridInsumos.prototype.removeEventListener = function (key, func) {
	if (this.events.hasOwnProperty(key)) {
		for (var i in this.events[key]) {
			if (this.events[key][i] === func) {
				this.events[key].splice(i, 1);
			}
		}
	}
};
GridInsumos.prototype.dispatchEvent = function (key, dataObj) {
	if (this.events.hasOwnProperty(key)) {		
		//dataObj = dataObj || {};		
		//dataObj.currentTarget = this;
		for (var i in this.events[key]) {
			this.events[key][i](dataObj);
		}
	}
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

var GridInsumosTraspasos = function(object)
{	
	this.id = object.id;			
	this.total = null;
	this.groupSupervisor = object.groupSupervisor;
	this.columns = object.columns;			
	this.url = object.url;
	//this.page = 1;
	//this.pageSize = object.pageSize;
	this.data = object.data;
	this.dataSet = [];
	//this.totalPages = 0;
	this.selectedItem = -1;
	this.loadingMessage = object.loadingMessage ? object.loadingMessage : "Cargando...";
	this.events = {};
	
	this.getRecord = function(index){				
		return this.dataSet[index];
	}
	
	this.getDataSet = function(){
		return this.dataSet;
	}
	
	this.setSelectedItem = function(index){
		this.selectedItem = index;
	}
	
	this.getSelectedItem = function(){
		return this.selectedItem;
	}
	
	
	this.load = function()
	{				
		this.data = arguments[0] ? arguments[0] : this.data;
		scopeGrid = this;
		//console.log(scopeGrid);
		$.ajax({
			cache: false,
			type: 'POST',
			url: scopeGrid.url,
			dataType: 'json',
			data: scopeGrid.data,			
			//beforeSend: function(){
				//$("#" + scopeGrid.id).mask(scopeGrid.loadingMessage);			
			//},
			success: function(response){
				//$("#" + scopeGrid.id).unmask();
				
				if(response.SUCCESS == true){	
					
					scopeGrid.fillDataSet(response);
					scopeGrid.dispatchEvent("onLoad",response);
				}else{
					scopeGrid.dispatchEvent("onError",response);
				}
			},
			error: function(xhr, ajaxOptions, thrownError){
				scopeGrid.dispatchEvent("onError",xhr);
			}
		});
	};
	this.fillDataSet = function(data)
	{
		if(data.RS.DATA.length > 0)
		{
			this.setSelectedItem(-1);
			this.dataSet = new Array();				
			this.total = data.RS.DATA[0][data.RS.COLUMNS.length-1];
			for(x=0;x<data.RS.DATA.length;x++){					
				var obj = {}
				for(i=0;i<data.RS.DATA[x].length;i++){
					var valor = data.RS.DATA[x][i];
					var propiedad = data.RS.COLUMNS[i];						
					obj[propiedad] = valor;	
					
				}
				/* GUARDAR EN UN OBJETO TODOS LOS VALORES DEVUELTOS */
				this.dataSet.push(obj);	
				
			}
			this.fillGrid();	
								
		}else
		{
			this.dataSet = new Array();
			this.emptyGrid();
		}	
		
	}
	this.fillGrid = function()
	{	contador = 0;
		temp = 0;
		$("#" + this.id + " tbody").empty();				
		arrayCols = new Array();
		arrayGroup = new Array();
		for(x=0;x<this.dataSet.length;x++)
		{					
			record = this.dataSet[x];
			//console.log(record);
			for(h=0;h<this.groupSupervisor.length;h++)
			{
				//temp = temp;
				//console.log(this.groupSupervisor[h]);
				if(this.groupSupervisor[h].type == 'encabezado')
				{
					//console.log(temp);
					value = record[this.groupSupervisor[h].dataIndex.toUpperCase()];
					Name = record[this.groupSupervisor[h].dataName.toUpperCase()];
					onClick = record[this.groupSupervisor[h].onClick.toUpperCase()];
					if(value == null) value = "";		
					group = '<td class="cuadricula" colspan="7" align="left" style="font-weight:bold">'+Name+'</td><td colspan="0"class="cuadricula" align="center"><input type="checkbox" class="checar" name="chk_'+record.ID_TIPOINSUMO+'"  id="chk_'+record.ID_TIPOINSUMO+'" onClick="javascript:checado(this.checked,'+record.ID_TIPOINSUMO+',0);" title="Checar '+Name+'"></td><td background="../images/orilla_derecha.gif" style="background-repeat:repeat-y" width="3px"></td>';
				}
				else if(this.groupSupervisor[h].type == 'subencabezado')
				{
					value = record[this.groupSupervisor[h].dataIndex.toUpperCase()];
					Name = record[this.groupSupervisor[h].dataName.toUpperCase()];
				
				
					//console.log(value);
					if(value == null) value = "";			
					contador = contador +1;
					group = '<td class="cuadricula" colspan="7" align="left" style="font-weight:bold">'+Name+' </td><td colspan="0"class="cuadricula" align="center"><input type="checkbox" class="checar" name="chk_'+record.ID_TIPOINSUMO+record.ID_GRUPOINSUMO+'"  id="chk_'+record.ID_TIPOINSUMO+record.ID_GRUPOINSUMO+'" onClick="javascript:checado(this.checked,'+record.ID_TIPOINSUMO+','+record.ID_GRUPOINSUMO+');" title="Checar '+Name+'"></td><td background="../images/orilla_derecha.gif" style="background-repeat:repeat-y" width="3px"></td>';
					temp = Name;
					//console.log(group);
					for(i=0;i<this.columns.length;i++)
					{
						if(this.columns[i].type == 'text')
						{	
							color = 'color:#FF0000';
							value = record[this.columns[i].dataIndex.toUpperCase()];
							Name = record[this.columns[i].dataName.toUpperCase()];
							css = this.columns[i].css ? 'align="' + this.columns[i].css + '"' :  "";
							style = this.columns[i].style ? 'style="'+this.columns[i].style+'"' : "";
							if(value == null) value = "";
							if(Name != null)
							{					
								column = '<td ' + css + ' '+style+' class="cuadricula">'+value+'-'+Name+'</td>';
							}
							else
							{ 
								column = '<td ' + css + ' class="cuadricula" >'+value+'</td>';
								
							}
						}else if(this.columns[i].type == 'check')
						{
							css = this.columns[i].css ? 'align="' + this.columns[i].css + '"' :  "";						
							column = '<td ' + css + ' class="cuadricula" ><input type="checkbox"   name="chk_'+record.ID_TIPOINSUMO+record.ID_GRUPOINSUMO+'_'+record.ID_INSUMO+'" id="chk_'+record.ID_TIPOINSUMO+record.ID_GRUPOINSUMO+'_'+record.ID_INSUMO+'" value="'+record.ID_INSUMO+'" ></td>';
							
						}else if(this.columns[i].type == 'input')
						{
							css = this.columns[i].css ? 'align="' + this.columns[i].css + '"' :  "";
							value = record[this.columns[i].dataIndex.toUpperCase()];
							Value = parseFloat(value).toFixed(6);
							column = '<td ' + css + ' class="cuadricula" > <input  name="nuTraspaso_'+record.ID_TIPOINSUMO+record.ID_GRUPOINSUMO+record.ID_INSUMO+'" id="nuTraspaso_'+record.ID_TIPOINSUMO+record.ID_GRUPOINSUMO+record.ID_INSUMO+'" value="'+Value+'" class="contenido_flotante" onClick="this.select();" onBlur="javascript:mensaje('+value+','+record.ID_INSUMO+','+record.ID_TIPOINSUMO+','+record.ID_GRUPOINSUMO+')" size="12" type="text" maxlength="10" ></td>';
						}else if(this.columns[i].type == 'operacion')
						{
							css = this.columns[i].css ? 'align="' + this.columns[i].css + '"' :  "";
							valueCosto = record[this.columns[i].dataCosto.toUpperCase()];
							valueCantidad = record[this.columns[i].dataCantidad.toUpperCase()];
							precio_cantidad = valueCosto * valueCantidad;
							Precio_cantidad = parseFloat(precio_cantidad).toFixed(2);
							column = '<td ' + css + ' class="cuadricula">$'+Precio_cantidad+'</td>';
						}else if(this.columns[i].type == 'numeric')
						{
							css = this.columns[i].css ? 'align="' + this.columns[i].css + '"' :  "";
							valueCosto = record[this.columns[i].dataCosto.toUpperCase()];
							valuecantidad = record[this.columns[i].dataCantidad.toUpperCase()];
							if(valueCosto != null)
							{
								ValueCosto = parseFloat(valueCosto).toFixed(2);
								column = '<td ' + css + ' class="cuadricula">$'+ValueCosto+'</td>';
							}else
							{	
								Valuecantidad = parseFloat(valuecantidad).toFixed(6);
								column = '<td ' + css + ' class="cuadricula">'+Valuecantidad+'</td>';
							}
						}
						//console.log(column);
						arrayCols.push(column);	
					}
						//console.log(group);
					
				}
				arrayGroup.push(group);	//}
				//}
			}
			
		}
		index = 0;
		index2 = 0;
		total =  this.columns.length;
		total2 = this.groupSupervisor.length;
		column = "";
		color = 'color:#FF0000';
		colorM = '##99CCCC';
		colorGroup = "background-color:#999999;"
		colorSubGroup = "background-color:#CCCCCC;"
		mouse = 'this.style.backgroundColor='+colorM+'';
		onMouseOver="this.style.backgroundColor='#99CCCC'" ; 
		onMouseOut="this.style.backgroundColor=''";
		group = arrayGroup[1];
		y = 0
		subgroup = arrayGroup[1]
		for(j=index2;j<total2;j++)
		{
			if(arrayGroup[y+1] != subgroup)
			{
				if(group != arrayGroup[y-2])
				{
					group = arrayGroup[y-2]
					$("#" + this.id + " tbody").append("<tr style="+colorGroup+">" + group +  "</tr>");
				}
				$("#" + this.id + " tbody").append("<tr style="+colorSubGroup+">" + subgroup +  "</tr>");
				subgroup = arrayGroup[y+1];
				j2 = j * this.columns.length;
				for(r=index;r<total;r++)
				{
					column = column + arrayCols[r];
					if(r == total - 1)
					{
						$("#" + this.id + " tbody").append("<tr onmouseout="+onMouseOut+" onmouseover="+onMouseOver+">" + column +  "</tr>");
						column = "";
						index = r +1;
						total = total + this.columns.length;
						if(total > j2 ){ break;}
					}
					if(r == j2  ) { break;}
				}
				
			}
			y =  y +2;
			if(j + 1 == total2)
			{
				total2 = total2 + this.groupSupervisor.length;
			}
			if(total2 > arrayGroup.length)
			{ 
				if(j+1 == arrayGroup.length)
				{
					temp = arrayGroup[0];
				}
				else
				{
				break;
				}
			}
			
		}
	}
	this.emptyGrid = function()
	{
		
		detener()
	}
};			


GridInsumosTraspasos.prototype.addEventListener = function (key, func) {
	if (!this.events.hasOwnProperty(key)) {
		this.events[key] = [];
	}
	this.events[key].push(func);
};

GridInsumosTraspasos.prototype.removeEventListener = function (key, func) {
	if (this.events.hasOwnProperty(key)) {
		for (var i in this.events[key]) {
			if (this.events[key][i] === func) {
				this.events[key].splice(i, 1);
			}
		}
	}
};
GridInsumosTraspasos.prototype.dispatchEvent = function (key, dataObj) {
	if (this.events.hasOwnProperty(key)) {		
		//dataObj = dataObj || {};		
		//dataObj.currentTarget = this;
		for (var i in this.events[key]) {
			this.events[key][i](dataObj);
		}
	}
};