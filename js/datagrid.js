/*===================
DATA TABLE
===================*/			
$('.data-table').dataTable();
  $('.data-grid').dataTable({
	  "sPaginationType": "full_numbers",
	   "bSort": false,
	  });
$('.data-table-theme').dataTable({
"sPaginationType": "full_numbers",
});

$('.data-table-noConfig').dataTable( {
"bPaginate": false,
"bLengthChange": false,
"bFilter": true,
"bSort": false,
"bInfo": false,
"bAutoWidth": false
});