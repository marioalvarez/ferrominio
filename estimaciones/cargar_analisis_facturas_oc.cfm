<!--- Parametros del Filtro --->
<cfparam name="Error" default="">
<cfparam name="id_Obra" default="">
<cfparam name="de_Factura_filtro" default="">
<cfparam name="fh_FacturaInicial_filtro" default="">
<cfparam name="fh_FacturaFinal_filtro" default="">
<cfparam name="id_Proveedor_filtro" default="">
<cfparam name="fh_Recepcion_filtro" default="">
<cfparam name="fh_Pago_filtro" default="">
<cfparam name="fh_Corte_filtro" default="">
<cfparam name="id_EstatusFactura_filtro" default="">
<cfparam name="nu_Semana_filtro" default="">
<cfparam name="id_Obra_filtro" default="">
<cfparam name="fh_Pago_vacio" default="">
<cfparam name="id_OrdenCompra_filtro" default="">
<cfparam name="sn_Mostrar" default="N">
<cfparam name="sn_Guardar" default="N">
<cfset CurrentPage=GetFileFromPath(GetTemplatePath())>
<cfparam name="PageNum_RSFacturas" default="1">

<!---<cfset Session.RSAnalisis = QueryNew('nu_Registro, id_CentroCosto,id_Frente,de_Frente,id_Partida,de_Partida,im_Analisis')>--->

<!--- Sacar los datos del listado --->
<cfif sn_Mostrar EQ 'S'>  
	<cfinvoke component="#Application.componentes#.contrarecibosdetalle" method="ContraRecibosDetalle_ListadoFacturas" returnvariable="RSFacturas"
		id_Obra="#id_Obra#"
		id_Empleado="#Session.id_Empleado#"
		de_Factura="#de_Factura_filtro#"
		fh_FacturaInicial="#fh_FacturaInicial_filtro#"
		fh_FacturaFinal="#fh_FacturaFinal_filtro#"
		id_Proveedor="#id_Proveedor_filtro#"
		fh_Recepcion="#fh_Recepcion_filtro#"
		fh_Corte="#fh_Corte_filtro#"
		fh_Pago="#fh_Pago_filtro#"
		nu_Semana="#nu_Semana_filtro#"
		id_EstatusFactura="#id_EstatusFactura_filtro#"
        fh_Pago_vacio="#fh_Pago_vacio#"
	>
	<cfif #RSFacturas.tipoError# NEQ "">
		<cf_error_manejador tipoError="#RSFacturas.tipoError#" mensajeError="#RSFacturas.mensaje#">
	</cfif>
	<cfset MaxRows_RSFacturas=25>
	<cfset StartRow_RSFacturas=Min((PageNum_RSFacturas-1)*MaxRows_RSFacturas+1,Max(RSFacturas.rs.RecordCount,1))>
	<cfset EndRow_RSFacturas=Min(StartRow_RSFacturas+MaxRows_RSFacturas-1,RSFacturas.rs.RecordCount)>
	<cfset TotalPages_RSFacturas=Ceiling(RSFacturas.rs.RecordCount/MaxRows_RSFacturas)>
	<cfset QueryString_RSFacturas=Iif(CGI.QUERY_STRING NEQ "",DE("&"&XMLFormat(CGI.QUERY_STRING)),DE(""))>
	<cfset tempPos=ListContainsNoCase(QueryString_RSFacturas,"PageNum_RSFacturas=","&")>
	<cfif tempPos NEQ 0>
		<cfset QueryString_RSFacturas=ListDeleteAt(QueryString_RSFacturas,tempPos,"&")>
	</cfif>
	<!---<cfdump var="#RSFacturas.rs#">--->
</cfif>

<!--- Guardar los datos de la factura --->
<cfif sn_Guardar EQ 'S'>
	<cfinvoke component="#Application.componentes#.contrarecibosdetalle" method="ContraRecibosDetalle_ListadoFacturas" returnvariable="RSFacturas"
		id_Obra="#id_Obra#"
		id_Empleado="#Session.id_Empleado#"
		de_Factura="#de_Factura_filtro#"
		fh_FacturaInicial="#fh_FacturaInicial_filtro#"
		fh_FacturaFinal="#fh_FacturaFinal_filtro#"
		id_Proveedor="#id_Proveedor_filtro#"
		fh_Recepcion="#fh_Recepcion_filtro#"
		fh_Corte="#fh_Corte_filtro#"
		fh_Pago="#fh_Pago_filtro#"
		nu_Semana="#nu_Semana_filtro#"
		id_EstatusFactura="#id_EstatusFactura_filtro#"
        fh_Pago_vacio="#fh_Pago_vacio#"
	>
	<cfif #RSFacturas.tipoError# NEQ "">
		<cf_error_manejador tipoError="#RSFacturas.tipoError#" mensajeError="#RSFacturas.mensaje#">
	</cfif>

 
	<!--- Manejar la variable del Check --->
	<cfif IsDefined("sn_CopiaFactura_#CurrentRow#")>
		<cfset sn_CopiaFactura = 1>
	<cfelse>
		<cfset sn_CopiaFactura = 0>
	</cfif>
	<!--- validar que un folio de factura no esté tramitada a pago para un proveedor --->
	<cfquery name="RS" datasource="#session.cnx#">
		SELECT *
		FROM Contrarecibosdetalle
		WHERE
			id_Empresa = #Evaluate('id_Empresa_#CurrentRow#')# AND
			id_Obra = #id_Obra# AND
			id_Proveedor = #id_Proveedor# AND
			de_Factura = '#de_Factura#' AND
			id_EstatusFactura = 519 
	</cfquery> 
    
        <cftransaction>
    		<!--- Actualizar los datos de la factura --->
            <cfif #Evaluate('id_EstatusFactura_#CurrentRow#')# EQ 519>        
                <!--- <cfif isDefined('RS') AND #RS.RecordCount# GT 0 >
                    <cfloop query="RS">                    
                        <cfif #RS.id_ContraRecibo# NEQ #FORM.id_ContraRecibo# AND #FORM.de_Referencia# EQ ''>
                            <cf_error_manejador mensajeError="No es posible tramitar a pago dos veces <br> un folio de factura para un proveedor">                            
                        <cfelseif #RS.id_ContraRecibo# EQ #FORM.id_ContraRecibo# AND #FORM.de_Referencia# EQ ''>
                            <cf_error_manejador mensajeError="Esta factura ya se encuentra tramitada a pago">
                        </cfif>
                    </cfloop>
                </cfif>	 --->		
                <cfinvoke component="#Application.componentes#.contrarecibosdetalle" method="ContraRecibosDetalle_ActualizarFactura" returnvariable="RSFactura"
                    id_EmpresaOriginal="#Evaluate('id_EmpresaOriginal_#CurrentRow#')#"
                    id_EmpresaNueva="#Evaluate('id_Empresa_#CurrentRow#')#"
                    id_ContraRecibo="#id_ContraRecibo#"
                    nd_ContraRecibo="#nd_ContraRecibo#"
                    id_EstatusFactura="#Evaluate('id_EstatusFactura_#CurrentRow#')#"
                    cl_TipoFactura="#Evaluate('cl_TipoFactura_#CurrentRow#')#"
                    fh_Pago="#Evaluate('fh_Pago_#CurrentRow#')#"
                    im_SubTotal="#Evaluate('im_SubTotal_#CurrentRow#')#"
                    de_Observaciones="#Evaluate('de_Observaciones_#CurrentRow#')#"
                    sn_CopiaFactura="#sn_CopiaFactura#"                
                >
            <!--- cuando es 512-CANCELADA solo se guarda el estatus, fh_pago y observaciones --->
            <cfelseif #Evaluate('id_EstatusFactura_#CurrentRow#')# EQ 512>
                
                <cfinvoke component="#Application.componentes#.contrarecibosdetalle" method="ContraRecibosDetalle_ActualizarFactura" returnvariable="RSFactura"
                    id_EmpresaOriginal="#Evaluate('id_EmpresaOriginal_#CurrentRow#')#"
                    id_EmpresaNueva="#Evaluate('id_Empresa_#CurrentRow#')#"
                    id_ContraRecibo="#id_ContraRecibo#"
                    nd_ContraRecibo="#nd_ContraRecibo#"
                    id_EstatusFactura="#Evaluate('id_EstatusFactura_#CurrentRow#')#"
                    cl_TipoFactura="#Evaluate('cl_TipoFactura_#CurrentRow#')#"
                    fh_Pago="#Evaluate('fh_Pago_#CurrentRow#')#"
                    im_SubTotal="#Evaluate('im_SubTotal_#CurrentRow#')#"				
                    de_Observaciones="#Evaluate('de_Observaciones_#CurrentRow#')#"
                    sn_CopiaFactura="#sn_CopiaFactura#"
                >
                <cfif #RSFactura.tipoError# NEQ "">
                    <cf_error_manejador tipoError="#RSFactura.tipoError#" mensajeError="#RSFactura.mensaje#">
                </cfif>
                <!---ACTUALIZAR ORDEN DE COMPRA DE LA FACTURA--->
                <cfinvoke component="#Application.componentes#.contrarecibosdetalle" method="RSEditarContraRecibosDetalle_OrdenCompra" 
                    returnvariable="RSContraRecibosEditar"
                    id_EmpresaOriginal="#Evaluate('id_EmpresaOriginal_#CurrentRow#')#"
                    id_EmpresaNueva="#Evaluate('id_Empresa_#CurrentRow#')#"
                    id_ContraRecibo="#id_ContraRecibo#"
                    nd_ContraRecibo="#nd_ContraRecibo#"
                    id_OrdenCompra="NULL"
                >
                <cfif #RSContraRecibosEditar.tipoError# NEQ "">
                    <cf_error_manejador tipoError="#RSContraRecibosEditar.tipoError#" mensajeError="#RSContraRecibosEditar.mensaje#">
                </cfif>
                <!---BORRAR ANALISIS DE LA FACTURA
                <cfinvoke component="#Application.componentes#.contrarecibosdetalleAnalisis" method="RSBorrarContraRecibosDetalleAnalisis" returnvariable="RSAnalisis"
                    id_Empresa="#Evaluate('id_EmpresaOriginal_#CurrentRow#')#"
                    id_Obra="#id_Obra#"
                    id_ContraRecibo="#id_ContraRecibo#"
                    nd_ContraRecibo="#nd_ContraRecibo#"
                >
                <cfif #RSAnalisis.tipoError# NEQ "">
                    <cf_error_manejador tipoError="#RSAnalisis.tipoError#" mensajeError="#RSAnalisis.mensaje#">
                </cfif>			
                --->
                <cfif #Evaluate('cl_TipoFactura_#CurrentRow#')# EQ 1> 
                <!--- BORRAR ANALISIS INSUMOS DE LA FACTURA --->
                    <cfinvoke component="#Application.componentes#.contrarecibosdetalleanalisisinsumos" method="RSBorrarContraRecibosDetalleAnalisisInsumos" returnvariable="RSAnalisisInsumos"
                        id_Empresa="#Evaluate('id_EmpresaOriginal_#CurrentRow#')#"
                        id_Obra="#id_Obra#"
                        id_ContraRecibo="#id_ContraRecibo#"
                        nd_ContraRecibo="#nd_ContraRecibo#"
                    >
                    <cfif #RSAnalisisInsumos.tipoError# NEQ "">
                        <cf_error_manejador tipoError="#RSAnalisisInsumos.tipoError#" mensajeError="#RSAnalisisInsumos.mensaje#">
                    </cfif>  
                </cfif>            				
                                
                <!---BORRAR FACTURA---> 
                <cfinvoke component="#Application.componentes#.contrarecibosdetalle" method="RSBorrarContraRecibosDetalle" returnvariable="RSAnalisis"
                    id_EmpresaOriginal="#Evaluate('id_EmpresaOriginal_#CurrentRow#')#"
                    id_EmpresaNueva="#Evaluate('id_Empresa_#CurrentRow#')#"
                    id_Obra="#id_Obra#"
                    id_ContraRecibo="#id_ContraRecibo#"
                    nd_ContraRecibo="#nd_ContraRecibo#"
                    id_EstatusFactura="#Evaluate('id_EstatusFactura_#CurrentRow#')#"
                    cl_TipoFactura="#Evaluate('cl_TipoFactura_#CurrentRow#')#"
                >
                <cfif #RSAnalisis.tipoError# NEQ "">
                    <cf_error_manejador tipoError="#RSAnalisis.tipoError#" mensajeError="#RSAnalisis.mensaje#">
                </cfif>         
                        
            <cfelse><!--- cuando no es 519-TRAMITAR A PAGO solo se guarda el estatus y observaciones --->
                <cfinvoke component="#Application.componentes#.contrarecibosdetalle" method="ContraRecibosDetalle_ActualizarFactura" returnvariable="RSFactura"
                    id_EmpresaOriginal="#Evaluate('id_EmpresaOriginal_#CurrentRow#')#"
                    id_EmpresaNueva="#Evaluate('id_Empresa_#CurrentRow#')#"
                    id_ContraRecibo="#id_ContraRecibo#"
                    nd_ContraRecibo="#nd_ContraRecibo#"
                    id_EstatusFactura="#Evaluate('id_EstatusFactura_#CurrentRow#')#"
                    cl_TipoFactura="#Evaluate('cl_TipoFactura_#CurrentRow#')#"
                    im_SubTotal="#Evaluate('im_SubTotal_#CurrentRow#')#"				
                    de_Observaciones="#Evaluate('de_Observaciones_#CurrentRow#')#"
                    sn_CopiaFactura="#sn_CopiaFactura#"
                >
                <!---ACTUALIZAR ORDEN DE COMPRA DE LA FACTURA--->
                <cfinvoke component="#Application.componentes#.contrarecibosdetalle" method="RSEditarContraRecibosDetalle_OrdenCompra" 
                    returnvariable="RSContraRecibosEditar"
                    id_EmpresaOriginal="#Evaluate('id_EmpresaOriginal_#CurrentRow#')#"
                    id_EmpresaNueva="#Evaluate('id_Empresa_#CurrentRow#')#"
                    id_ContraRecibo="#id_ContraRecibo#"
                    nd_ContraRecibo="#nd_ContraRecibo#"
                    id_OrdenCompra="NULL"
                >
                <cfif #RSContraRecibosEditar.tipoError# NEQ "">
                    <cf_error_manejador tipoError="#RSContraRecibosEditar.tipoError#" mensajeError="#RSContraRecibosEditar.mensaje#">
                </cfif>
                
                <!---BORRAR FACTURA---> 
                <cfinvoke component="#Application.componentes#.contrarecibosdetalle" method="RSBorrarContraRecibosDetalle" returnvariable="RSAnalisis"
                    id_EmpresaOriginal="#Evaluate('id_EmpresaOriginal_#CurrentRow#')#"
                    id_EmpresaNueva="#Evaluate('id_Empresa_#CurrentRow#')#"
                    id_Obra="#id_Obra#"
                    id_ContraRecibo="#id_ContraRecibo#"
                    nd_ContraRecibo="#nd_ContraRecibo#"
                    id_EstatusFactura="#Evaluate('id_EstatusFactura_#CurrentRow#')#"
                    cl_TipoFactura="#Evaluate('cl_TipoFactura_#CurrentRow#')#"
                >
                <cfif #RSAnalisis.tipoError# NEQ "">
                    <cf_error_manejador tipoError="#RSAnalisis.tipoError#" mensajeError="#RSAnalisis.mensaje#">
                </cfif>				
            </cfif>
            <cfif #RSFactura.tipoError# NEQ "">
                <cf_error_manejador tipoError="#RSFactura.tipoError#" mensajeError="#RSFactura.mensaje#">
            </cfif>
            
            <!--- Guardar el desglose de los analisis (Estatus 519 - TRAMITAR A PAGO)--->
            <!--- Sí el TipoFactura = 1 es una factura de materiales ó 4(tramites y permisos) y se deben de cargar los analisis --->
            <!--- EIRR 09/23/13 condicion original antes de meter orden de compra en tramitar a pago materiales --->
            <!--- <cfif #Evaluate('id_EstatusFactura_#CurrentRow#')# EQ 519 AND (#Evaluate('cl_TipoFactura_#CurrentRow#')# EQ 1 OR #Evaluate('cl_TipoFactura_#CurrentRow#')# EQ 4)> --->
            <cfif (#Evaluate('id_EstatusFactura_#CurrentRow#')# EQ 519 AND #Evaluate('cl_TipoFactura_#CurrentRow#')# EQ 4)>
                <cfif Session.RSAnalisis.RecordCount EQ 0>
                    <cf_error_manejador tipoError="Validacion" mensajeError="No ha capturado los analisis para la factura de tipo Material">
                </cfif>
                
                <!---BORRAR ANALISIS DE LA FACTURA--->
                <cfinvoke component="#Application.componentes#.contrarecibosdetalleAnalisis" method="RSBorrarContraRecibosDetalleAnalisis" returnvariable="RSAnalisis"
                    id_Empresa="#Evaluate('id_EmpresaOriginal_#CurrentRow#')#"
                    id_Obra="#id_Obra#"
                    id_ContraRecibo="#id_ContraRecibo#"
                    nd_ContraRecibo="#nd_ContraRecibo#"
                >
                <cfif #RSAnalisis.tipoError# NEQ "">
                    <cf_error_manejador tipoError="#RSAnalisis.tipoError#" mensajeError="#RSAnalisis.mensaje#">
                </cfif>
                
                <cfset id_Empresa_O = #Evaluate('id_EmpresaOriginal_#CurrentRow#')#>
                <cfset id_Empresa_N = #Evaluate('id_Empresa_#CurrentRow#')#>
                <!--- Validar que el CC y Frente Existan --->
                <cfset tipo = #Evaluate('cl_TipoFactura_#CurrentRow#')#>
                <cfoutput query="Session.RSAnalisis">
                    <cfinvoke component="#Application.componentes#.Frentes" method="ValidacionFrentes" returnvariable="RSFrentes"
                        id_Empresa="#id_Empresa_N#"
                        id_Obra="#id_Obra#"
                        id_CentroCosto="#id_CentroCosto#"
                        id_Frente="#id_Frente#"
                        id_TipoInsumo="#tipo#"
                    >
                    <cfif #RSFrentes.tipoError# NEQ "">
                        <cf_error_manejador tipoError="#RSFrentes.tipoError#" mensajeError="#RSFrentes.mensaje#">
                    </cfif>
                    
                    <cfif #RSFrentes.rs.RecordCount# EQ 0>
                        <cftransaction action="rollback"/>
                        <cf_error_manejador mensajeError="El Análisis Especificado no Existe<br>Verificar presupuesto">
                    </cfif>
                </cfoutput>
                
                <!---AGREGAR NUEVOS ANALISIS DE LA FACTURA--->
                <cfoutput query="Session.RSAnalisis">
                    <cfinvoke component="#Application.componentes#.contrarecibosdetalleAnalisis" method="RSAgregarContraRecibosDetalleAnalisis" returnvariable="RSAnalisis"
                        id_EmpresaOriginal="#id_Empresa_O#"
                        id_EmpresaNueva="#id_Empresa_N#"
                        id_Obra="#id_Obra#"
                        id_ContraRecibo="#id_ContraRecibo#"
                        nd_ContraRecibo="#nd_ContraRecibo#"
                        id_CentroCosto="#Session.RSAnalisis.id_CentroCosto#"
                        id_Frente="#Session.RSAnalisis.id_Frente#"
                        id_Partida="#Session.RSAnalisis.id_Partida#"
                        im_Analisis="#Session.RSAnalisis.im_Analisis#"
                    >
                    <cfif #RSAnalisis.tipoError# NEQ "">
                        <cf_error_manejador tipoError="#RSAnalisis.tipoError#" mensajeError="#RSAnalisis.mensaje#">
                    </cfif>
                </cfoutput>
                <!--- Resetear los analisis --->
                <cfset StructDelete(Session,"RSAnalisis")>
                    
                <!---ACTUALIZAR ORDEN DE COMPRA DE LA FACTURA--->
                <cfinvoke component="#Application.componentes#.contrarecibosdetalle" method="RSEditarContraRecibosDetalle_OrdenCompra" 
                    returnvariable="RSContraRecibosEditar"
                    id_EmpresaOriginal="#Evaluate('id_EmpresaOriginal_#CurrentRow#')#"
                    id_EmpresaNueva="#Evaluate('id_Empresa_#CurrentRow#')#"
                    id_ContraRecibo="#id_ContraRecibo#"
                    nd_ContraRecibo="#nd_ContraRecibo#"
                    id_OrdenCompra="NULL"
                >
                <cfif #RSContraRecibosEditar.tipoError# NEQ "">
                    <cf_error_manejador tipoError="#RSContraRecibosEditar.tipoError#" mensajeError="#RSContraRecibosEditar.mensaje#">
                </cfif>
    
                <!---BORRAR FACTURA---> 
                <cfinvoke component="#Application.componentes#.contrarecibosdetalle" method="RSBorrarContraRecibosDetalle" returnvariable="RSAnalisis"
                    id_EmpresaOriginal="#Evaluate('id_EmpresaOriginal_#CurrentRow#')#"
                    id_EmpresaNueva="#Evaluate('id_Empresa_#CurrentRow#')#"
                    id_Obra="#id_Obra#"
                    id_ContraRecibo="#id_ContraRecibo#"
                    nd_ContraRecibo="#nd_ContraRecibo#"
                    id_EstatusFactura="#Evaluate('id_EstatusFactura_#CurrentRow#')#"
                    cl_TipoFactura="#Evaluate('cl_TipoFactura_#CurrentRow#')#"
                >
                <cfif #RSAnalisis.tipoError# NEQ "">
                    <cf_error_manejador tipoError="#RSAnalisis.tipoError#" mensajeError="#RSAnalisis.mensaje#">
                </cfif>	
                
             <!---  TRAMITAR A PAGO Y TIPO FACTURA MATERIALES --->
             <cfelseif (#Evaluate('id_EstatusFactura_#CurrentRow#')# EQ 519 AND #Evaluate('cl_TipoFactura_#CurrentRow#')# EQ 1)>
                	      
                <!--- <cfif Session.RSAnalisisOC.RecordCount EQ 0>
                    <cf_error_manejador tipoError="Validacion" mensajeError="No ha capturado los analisis para la factura de tipo Material">
                </cfif> --->			
                <!---BORRAR ANALISIS DE LA FACTURA--->
                <!--- <cfinvoke component="#Application.componentes#.contrarecibosdetalleanalisisinsumos" method="RSBorrarContraRecibosDetalleAnalisisInsumos" returnvariable="RSAnalisisOC"
                    id_Empresa="#Evaluate('id_EmpresaOriginal_#CurrentRow#')#"
                    id_Obra="#id_Obra#"
                    id_ContraRecibo="#id_ContraRecibo#"
                    nd_ContraRecibo="#nd_ContraRecibo#"
                >
                <cfif #RSAnalisisOC.tipoError# NEQ "">
                    <cf_error_manejador tipoError="#RSAnalisisOC.tipoError#" mensajeError="#RSAnalisisOC.mensaje#">
                </cfif>			 --->
                <cfset id_Empresa_O = #Evaluate('id_EmpresaOriginal_#CurrentRow#')#>
                <cfset id_Empresa_N = #Evaluate('id_Empresa_#CurrentRow#')#>
                <!--- Validar que el CC y Frente Existan --->
                <cfset tipo = #Evaluate('cl_TipoFactura_#CurrentRow#')#>
                <!--- <cfoutput query="Session.RSAnalisisOC">
                    <cfinvoke component="#Application.componentes#.Frentes" method="ValidacionFrentes" returnvariable="RSFrentes"
                        id_Empresa="#id_Empresa_N#"
                        id_Obra="#id_Obra#"
                        id_CentroCosto="#id_CentroCosto#"
                        id_Frente="#id_Frente#"
                        id_TipoInsumo="#tipo#"
                    >
                    <cfif #RSFrentes.tipoError# NEQ "">
                        <cf_error_manejador tipoError="#RSFrentes.tipoError#" mensajeError="#RSFrentes.mensaje#">
                    </cfif>
                    
                    <cfif #RSFrentes.rs.RecordCount# EQ 0>
                        <cftransaction action="rollback"/>
                        <cf_error_manejador mensajeError="El Análisis Especificado no Existe<br>Verificar presupuesto">
                    </cfif>
                </cfoutput>        --->
      				
                <!--- Obtener frentes desglosados y agrupados --->
               
                   <cfinvoke component="#Application.componentes#.contrarecibosdetalleanalisisinsumos" method="RSObtenerFrentesDesglosados" returnvariable="RSFrentesDesglosados"
                    id_Empresa = "#Evaluate('id_Empresa_#CurrentRow#')#"
                    id_Obra="#id_Obra#"
                    id_OrdenCompra="#Evaluate('idu_OrdenCompra_#CurrentRow#')#">                
                    <cfif #RSFrentesDesglosados.tipoError# NEQ "">
                        <cf_error_manejador tipoError="#RSFrentesDesglosados.tipoError#" mensajeError="#RSFrentesDesglosados.mensaje#">
                    </cfif>                      
                                        
                    <cfinvoke component="#Application.componentes#.ordenescompra" method="RSOrdenCompraPendientePorPagar" returnvariable="RSOCPendientePorPagar"
                        idu_Empresa = "#Evaluate('id_Empresa_#CurrentRow#')#"
                        idu_Obra = "#id_Obra#"
                        idu_OrdenCompra = "#Evaluate('idu_OrdenCompra_#CurrentRow#')#">   

                                                     
                    <cfif #RSOCPendientePorPagar.tipoError# NEQ "">
                        <cf_error_manejador_informacion tipoError="#RSOCPendientePorPagar.tipoError#" mensajeError="#RSOCPendientePorPagar.mensaje#">
                    </cfif>					
                   
					<!--- INSERTAR ContraRecibosDetalleAnalisisInsumos --->                 	            
                    <cfset impContador = #Evaluate('im_SubTotal_#CurrentRow#')#> 
                   
					<cfif #RSOCPendientePorPagar.rs.RecordCount# GT 0>   
						<cfoutput query="RSOCPendientePorPagar.rs">                       	
                                <cfif #RSOCPendientePorPagar.rs.im_CantidadPrecio# GT 0>
                                    <cfloop query="RSFrentesDesglosados.rs">                                
                                    <cfif #RSFrentesDesglosados.rs.im_CantidadPrecio# GT 0>
                                        <!--- <cfif #RSFrentesDesglosados.rs.id_CentroCosto# EQ #RSOCPendientePorPagar.rs.id_CentroCosto# AND #RSFrentesDesglosados.rs.id_Frente# EQ #RSOCPendientePorPagar.rs.id_Frente#>   --->
                                        <cfif #RSFrentesDesglosados.rs.id_CentroCosto# EQ #RSOCPendientePorPagar.rs.id_CentroCosto# AND #RSFrentesDesglosados.rs.id_Frente# EQ #RSOCPendientePorPagar.rs.id_Frente#>          
                                         <cfif impContador GT 0>                                          	                                                                                                                 
                                            <cfif impContador GTE #RSFrentesDesglosados.rs.im_CantidadPrecio#>                                                        
                                                <cfset impContador = impContador - #RSFrentesDesglosados.rs.im_CantidadPrecio#>
                                                <cfset Unidades = 0>                                    
                                                <cfset Unidades = #RSFrentesDesglosados.rs.im_CantidadPrecio# / #RSFrentesDesglosados.rs.im_Precio#>                                                                                             
                                                <!--- insertar frente desglosado --->                                                
                                                
                                                <cfinvoke component="#Application.componentes#.contrarecibosdetalleanalisisinsumos" method="RSInsertarAnalisisInsumos" returnvariable="RSAnalisisInsumos"
                                                    id_Empresa="#RSFrentesDesglosados.rs.id_Empresa#"
                                                    id_Obra="#id_Obra#"
                                                    id_ContraRecibo="#id_ContraRecibo#"
                                                    nd_ContraRecibo="#nd_ContraRecibo#"
                                                    id_CentroCosto="#RSFrentesDesglosados.rs.id_CentroCosto#"
                                                    id_Frente="#RSFrentesDesglosados.rs.id_Frente#"
                                                    id_Partida="#RSFrentesDesglosados.rs.id_Partida#"
                                                    id_Insumo="#RSFrentesDesglosados.rs.id_Insumo#"
                                                    im_Analisis="#RSFrentesDesglosados.rs.im_CantidadPrecio#"
                                                    nu_Cantidad="#Unidades#"
                                                    im_Precio="#RSFrentesDesglosados.rs.im_Precio#">
                                                <cfif #RSFrentesDesglosados.tipoError# NEQ "">
                                                    <cfset Error = #RSAnalisisInsumos.mensaje#>
                                                    <cftransaction action="rollback"/>
                                                    <cf_error_manejador tipoError="#RSAnalisisInsumos.tipoError#" mensajeError="#RSAnalisisInsumos.mensaje#">
                                                </cfif>
                                            <cfelse>                                                                	
                                                <cfset Unidades = 0>                                    
                                                <cfset Unidades = #impContador# / #RSFrentesDesglosados.rs.im_Precio#>
                                                <!--- insertar frente desglosado con unidades resultantes --->	
                                                <cfinvoke component="#Application.componentes#.contrarecibosdetalleanalisisinsumos" method="RSInsertarAnalisisInsumos" returnvariable="RSAnalisisInsumos"
                                                    id_Empresa="#RSFrentesDesglosados.rs.id_Empresa#"
                                                    id_Obra="#id_Obra#"
                                                    id_ContraRecibo="#id_ContraRecibo#"
                                                    nd_ContraRecibo="#nd_ContraRecibo#"
                                                    id_CentroCosto="#RSFrentesDesglosados.rs.id_CentroCosto#"
                                                    id_Frente="#RSFrentesDesglosados.rs.id_Frente#"
                                                    id_Partida="#RSFrentesDesglosados.rs.id_Partida#"                                       
                                                    id_Insumo="#RSFrentesDesglosados.rs.id_Insumo#"
                                                    im_Analisis="#impContador#"
                                                    nu_Cantidad="#Unidades#"
                                                    im_Precio="#RSFrentesDesglosados.rs.im_Precio#"> 
                                                <cfif #RSFrentesDesglosados.tipoError# NEQ "">
                                                    <cftransaction action="rollback"/>
                                                    <cf_error_manejador tipoError="#RSAnalisisInsumos.tipoError#" mensajeError="#RSAnalisisInsumos.mensaje#">
                                                </cfif>                                       
                                                <cfset impContador = 0>                                    
                                            </cfif>                                   
                                        </cfif> 
                                        </cfif>  
                                        </cfif>                                       
                                    </cfloop>                       
                                </cfif>                                                             
                           </cfoutput>
                       <cfelse>                         	              	 
                       		 <cf_location_msg url="cargar_analisis_facturas_oc.cfm?id_Obra=#id_Obra#&de_Factura_filtro=#de_Factura_filtro#&fh_FacturaInicial_filtro=#fh_FacturaInicial_filtro#&fh_FacturaFinal_filtro=#fh_FacturaFinal_filtro#&id_Proveedor_filtro=#id_Proveedor_filtro#&fh_Recepcion_filtro=#fh_Recepcion_filtro#&fh_Corte_filtro=#fh_Corte_filtro#&fh_Pago_filtro=#fh_Pago_filtro#&nu_Semana_filtro=#nu_Semana_filtro#&id_EstatusFactura_filtro=#id_EstatusFactura_filtro#&sn_Mostrar=S" mensaje="No existen analisis pendientes por pagar">	
                       </cfif>  
                                                           			
          
                <!---AGREGAR NUEVOS ANALISIS DE LA FACTURA--->
                <!--- <cfoutput query="Session.RSAnalisisOC">
                    <cfinvoke component="#Application.componentes#.contrarecibosdetalleanalisisinsumos" method="RSAgregarContraRecibosDetalleAnalisisInsumos" returnvariable="RSAnalisisOC"
                        id_EmpresaOriginal="#id_Empresa_O#"
                        id_EmpresaNueva="#id_Empresa_N#"
                        id_Obra="#id_Obra#"
                        id_ContraRecibo="#id_ContraRecibo#"
                        nd_ContraRecibo="#nd_ContraRecibo#"
                        id_CentroCosto="#Session.RSAnalisisOC.id_CentroCosto#"
                        id_Frente="#Session.RSAnalisisOC.id_Frente#"
                        id_Partida="#Session.RSAnalisisOC.id_Partida#"
                        im_Analisis="#Session.RSAnalisisOC.im_Analisis#"
                    >
                    <cfif #RSAnalisisOC.tipoError# NEQ "">
                        <cf_error_manejador tipoError="#RSAnalisisOC.tipoError#" mensajeError="#RSAnalisisOC.mensaje#">
                    </cfif>
                </cfoutput> --->
                
                <!--- Resetear los analisis --->
                <!--- <cfset StructDelete(Session,"RSAnalisisOC")> --->
                                
                <!---ACTUALIZAR ORDEN DE COMPRA DE LA FACTURA--->
                <cfinvoke component="#Application.componentes#.contrarecibosdetalle" method="RSEditarContraRecibosDetalle_OrdenCompra" 
                    returnvariable="RSContraRecibosEditar"
                    id_EmpresaOriginal="#Evaluate('id_EmpresaOriginal_#CurrentRow#')#"
                    id_EmpresaNueva="#Evaluate('id_Empresa_#CurrentRow#')#"
                    id_ContraRecibo="#id_ContraRecibo#"
                    nd_ContraRecibo="#nd_ContraRecibo#"
                    id_OrdenCompra="#Evaluate('idu_OrdenCompra_#CurrentRow#')#" 
                >
                <cfif #RSContraRecibosEditar.tipoError# NEQ "">
                    <cf_error_manejador tipoError="#RSContraRecibosEditar.tipoError#" mensajeError="#RSContraRecibosEditar.mensaje#">
                </cfif>
    
                <!---BORRAR FACTURA---> 
                <cfinvoke component="#Application.componentes#.contrarecibosdetalle" method="RSBorrarContraRecibosDetalleMateriales" returnvariable="RSAnalisisOC"
                    id_EmpresaOriginal="#Evaluate('id_EmpresaOriginal_#CurrentRow#')#"
                    id_EmpresaNueva="#Evaluate('id_Empresa_#CurrentRow#')#"
                    id_Obra="#id_Obra#"
                    id_ContraRecibo="#id_ContraRecibo#"
                    nd_ContraRecibo="#nd_ContraRecibo#"
                    id_EstatusFactura="#Evaluate('id_EstatusFactura_#CurrentRow#')#"
                    cl_TipoFactura="#Evaluate('cl_TipoFactura_#CurrentRow#')#"
                >
                <cfif #RSAnalisisOC.tipoError# NEQ "">
                    <cf_error_manejador tipoError="#RSAnalisisOC.tipoError#" mensajeError="#RSAnalisisOC.mensaje#">
                </cfif>
           
                
            <!--- Sí el TipoFactura = 2 es una factura de mano de obra o 3(servicios) y se actualiza la oc del contrarecibo --->
            <cfelseif #Evaluate('id_EstatusFactura_#CurrentRow#')# EQ 519 AND (#Evaluate('cl_TipoFactura_#CurrentRow#')# EQ 2 OR #Evaluate('cl_TipoFactura_#CurrentRow#')# EQ 3)>

                <!---BORRAR ANALISIS DE LA FACTURA--->
                <cfinvoke component="#Application.componentes#.contrarecibosdetalleAnalisis" method="RSBorrarContraRecibosDetalleAnalisis" returnvariable="RSAnalisis"
                    id_Empresa="#Evaluate('id_EmpresaOriginal_#CurrentRow#')#"
                    id_Obra="#id_Obra#"
                    id_ContraRecibo="#id_ContraRecibo#"
                    nd_ContraRecibo="#nd_ContraRecibo#"
                >
                <cfif #RSAnalisis.tipoError# NEQ "">
                    <cf_error_manejador tipoError="#RSAnalisis.tipoError#" mensajeError="#RSAnalisis.mensaje#">
                </cfif>
                <!---ACTUALIZAR ORDEN DE COMPRA DE LA FACTURA--->
                <cfinvoke component="#Application.componentes#.contrarecibosdetalle" method="RSEditarContraRecibosDetalle_OrdenCompra" 
                    returnvariable="RSContraRecibosEditar"
                    id_EmpresaOriginal="#Evaluate('id_EmpresaOriginal_#CurrentRow#')#"
                    id_EmpresaNueva="#Evaluate('id_Empresa_#CurrentRow#')#"
                    id_ContraRecibo="#id_ContraRecibo#"
                    nd_ContraRecibo="#nd_ContraRecibo#"
                    id_OrdenCompra="#Evaluate('idu_OrdenCompra_#CurrentRow#')#"
                >
                <cfif #RSContraRecibosEditar.tipoError# NEQ "">
                    <cf_error_manejador tipoError="#RSContraRecibosEditar.tipoError#" mensajeError="#RSContraRecibosEditar.mensaje#">
                </cfif>
                <!---BORRAR FACTURA---> 
                <cfinvoke component="#Application.componentes#.contrarecibosdetalle" method="RSBorrarContraRecibosDetalle" returnvariable="RSAnalisis"
                    id_EmpresaOriginal="#Evaluate('id_EmpresaOriginal_#CurrentRow#')#"
                    id_EmpresaNueva="#Evaluate('id_Empresa_#CurrentRow#')#"
                    id_Obra="#id_Obra#"
                    id_ContraRecibo="#id_ContraRecibo#"
                    nd_ContraRecibo="#nd_ContraRecibo#"
                    id_EstatusFactura="#Evaluate('id_EstatusFactura_#CurrentRow#')#"
                    cl_TipoFactura="#Evaluate('cl_TipoFactura_#CurrentRow#')#"
                >
                <cfif #RSAnalisis.tipoError# NEQ "">
                    <cf_error_manejador tipoError="#RSAnalisis.tipoError#" mensajeError="#RSAnalisis.mensaje#">
                </cfif>					
            </cfif>
        </cftransaction>
		<cfif #Error# NEQ "">
            <cf_error_manejador  mensajeError="#RSEmpresas.mensaje#">
		<cfelse>
        	<cf_location_msg url="cargar_analisis_facturas_oc.cfm?id_Obra=#id_Obra#&de_Factura_filtro=#de_Factura_filtro#&fh_FacturaInicial_filtro=#fh_FacturaInicial_filtro#&fh_FacturaFinal_filtro=#fh_FacturaFinal_filtro#&id_Proveedor_filtro=#id_Proveedor_filtro#&fh_Recepcion_filtro=#fh_Recepcion_filtro#&fh_Corte_filtro=#fh_Corte_filtro#&fh_Pago_filtro=#fh_Pago_filtro#&nu_Semana_filtro=#nu_Semana_filtro#&id_EstatusFactura_filtro=#id_EstatusFactura_filtro#&sn_Mostrar=S" mensaje="Se guardo correctamente">	
        </cfif>    
</cfif>
<!--- Cargar las Empresas Disponibles --->
<cfinvoke component="#Application.componentes#.empresas" method="RSMostrarTodosEmpresas" returnvariable="RSEmpresas">
<cfif #RSEmpresas.tipoError# NEQ "">
	<cf_error_manejador tipoError="#RSEmpresas.tipoError#" mensajeError="#RSEmpresas.mensaje#">
</cfif>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<link rel="stylesheet" href="../css/style.css" type="text/css">
<link rel="stylesheet" type="text/css" href="../css/box.css">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Untitled Document</title>
<script language="javascript" src="../js/jquery.js"></script>
<script language="javascript" src="../js/funciones.js"></script>
<script language="javascript" src="../js/funciones_ajax.js"></script>
<script language="javascript" src="../js/validar_selects.js"></script>
<script type="text/javascript" src="../calendario/calendar.js"></script>
<script type="text/javascript" src="../calendario/calendar-setup.js"></script>
<script type="text/javascript" src="../calendario/lang/calendar-es.js"></script>
<script language="javascript">
function imprimir(valor)
	{
	      if(valor == 0){
		  	form1.action = 'cargar_analisis_facturas_oc.cfm';
		  	form1.action.submit();
		  }
		  else{
		  	form1.action = 'rpt_analisis_facturas_oc_excel.cfm';
		  }
					
	}
	<!---Función que redirige el focus del campo de nombre al campo de la ID, cuyo desenfoque es el que manda a llamar el bindeo. --->
	var cambiarFocus = function(pop_up){				
		var nom_campo = $($(pop_up).prev())[0].id;
		var valor = $("#" + nom_campo).val();
		if(valor != "")
		{
			$("#" + nom_campo).focus().blur();
		}
	};
</script>
<style type="text/css"> @import url("../calendario/calendar-blue2.css");</style>
</head>

<body>

<cfinclude template="menu.cfm">&nbsp;
<cfform name="form1" method="post" action="#CurrentPage#">

<table width="700" align="center">
	<tr class="titulo">
		<td align="left">ENCARGADO DE ESTIMACIONES <br>LISTADO DE FACTURAS</td>
	</tr>
</table>
<table width="700" cellpadding="0" cellspacing="0" align="center">
	<tr><td class="x-box-tl" width="1px"></td><td class="x-box-tc"></td><td class="x-box-tr" width="1px"></td></tr>
	<tr><td class="x-box-ml"></td><td class="x-box-mc" width="99%" align="center">	
		<table width="700" align="center">
		  	<tr>
				<td align="left"><!--- <button type="button" class="boton_menu_superior" onClick="javascript:location='facturas_agregar.cfm?id_Obra_filtro=<cfoutput>#id_Obra#&de_Factura_filtro=#de_Factura_filtro#&fh_FacturaInicial_filtro=#fh_FacturaInicial_filtro#&fh_FacturaFinal_filtro=#fh_FacturaFinal_filtro#&id_Proveedor_filtro=#id_Proveedor_filtro#&fh_Recepcion_filtro=#fh_Recepcion_filtro#&fh_Corte_filtro=#fh_Corte_filtro#&fh_Pago_filtro=#fh_Pago_filtro#&nu_Semana_filtro=#nu_Semana_filtro#&id_EstatusFactura_filtro=#id_EstatusFactura_filtro#&sn_Mostrar=#sn_Mostrar#</cfoutput>'"><img src="../images/nuevo.png" alt="Clic para agregar nueva factura" width="24"></button> ---></td>
				<td align="right">Obra:</td>
				<td align="left">
					<cfinput type="text" class="contenido_numerico" message="Clave de Obra no vAlida" name="id_Obra" id="id_Obra" onBlur="" size="6" value="#id_Obra#">
					<cfinput type="text" bind="cfc:#Application.componentes#.obras.getObraPorID(#Session.id_Empresa#,{id_Obra@blur})" bindonload="yes" onFocus="cambiarFocus($(this).before());" id="de_Obra_lab" name="de_Obra_lab" class="contenido_readOnly" style="overflow:hidden; width:250px"  required="no" message="La Clave de Obra no es valida.">
					&nbsp;<button type="button" id="btnBuscarObra" onClick="javascript:AbrirPopUpBusquedaChrome('id_Obra', 'pop_obras_busqueda.cfm', 'id_Empresa=<cfoutput>#Session.id_Empresa#</cfoutput>', '700px', '600px')" class="boton_popup" value=""><img src="../images/buscar.png" alt="Clic para buscar obra" width="24"></button>
				</td>
				<td align="right">Folio Factura:</td>
				<td align="left">					
					<cfinput type="text" name="de_Factura_filtro" class="contenido" value="#de_Factura_filtro#" size="11" maxlength="50">
				</td>
			</tr>
			<tr>
				<td></td>
				<td align="right">De fecha:</td>
				<td align="left">
					<cfinput type="text" name="fh_FacturaInicial_filtro" value="#fh_FacturaInicial_filtro#" message="Fecha Inicial no válida" size="11" mask="99/99/9999" readonly="true" validate="eurodate" class="contenido">
					<input type="button" id="lanzadorDeFecha" value="..." / class="boton" onKeyDown="javascript:escanear_enter();" >
					A Fecha:
					<cfinput type="text" name="fh_FacturaFinal_filtro" value="#fh_FacturaFinal_filtro#" message="Fecha Final no válida" size="11" mask="99/99/9999" readonly="true" validate="eurodate" class="contenido">
					<input type="button" id="lanzadorAFecha" value="..." / class="boton" onKeyDown="javascript:escanear_enter();">
				</td>
				<td align="right">Semana:</td>
				<td align="left">
					<cfinput type="text" name="nu_Semana_filtro" message="Número de semana no válido" validate="integer" class="contenido_numerico" value="#nu_Semana_filtro#" size="5" maxlength="2">
				</td>
			</tr>
			<tr>
				<td></td>
				<td align="right">Proveedor:</td>
				<td align="left" colspan="3">
					<cfinput type="text" name="id_Proveedor_filtro" id="id_Proveedor_filtro" class="contenido_numerico" size="10" value="#id_Proveedor_filtro#" maxlength="10" message="Proveedor no válido" validate="integer">
					<cfinput bind="cfc:#Application.componentes#.proveedores.getProveedorPorID({id_Proveedor_filtro@blur})" bindonload="yes" onFocus="cambiarFocus($(this).before());" id="nb_Proveedor_lab" name="nb_Proveedor_lab" class="contenido_readOnly" style="overflow:hidden; width:250px"  required="no">
					<button type="button" id="btnBuscarProveedor" onClick="javascript:AbrirPopUpBusquedaChrome('id_Proveedor_filtro', 'pop_proveedores_busqueda.cfm', '', '700px', '600px')" class="boton_popup" value=""><img src="../images/buscar_gris.png" width="24"></button>
				</td>
				
			</tr>
			<tr>
				<td></td>
				<td align="right">Fecha Recepci&oacute;n:</td>
				<td align="left">
					<cfinput type="text" name="fh_Recepcion_filtro" value="#fh_Recepcion_filtro#" message="Fecha Recepción no válida" size="11" mask="99/99/9999" readonly="true" validate="eurodate" class="contenido">
					<input type="button" id="lanzadorFechaRecepcion" value="..." / class="boton" onKeyDown="javascript:escanear_enter();" >
					Fecha Corte:
					<cfinput type="text" name="fh_Corte_filtro" value="#fh_Corte_filtro#" message="Fecha Corte no válida" size="11" mask="99/99/9999" readonly="true" validate="eurodate" class="contenido">
					<input type="button" id="lanzadorFechaCorte" value="..." / class="boton" onKeyDown="javascript:escanear_enter();" >
				</td>
				<td align="right">Fecha Pago:</td>
				<td align="left">
					<cfinput type="text" name="fh_Pago_filtro" value="#fh_Pago_filtro#" message="Fecha Pago no válida" size="11" mask="99/99/9999" readonly="true" validate="eurodate" class="contenido">
					<input type="button" id="lanzadorFechaPago" value="..." / class="boton" onKeyDown="javascript:escanear_enter();" >
				</td>              				
			</tr>
			<tr>
				<td></td>
				<!---<td align="right">Tipo de Factura:</td>
				<td align="left">
					<cfselect name="cl_TipoFactura" style="width:200px"	class="contenido"
						title="Seleccione una categoría" queryPosition="below">
						<option value=""  >SELECCIONE UNA OPCIÓN</option>
						<option value="1" >MATERIALES</option>
						<option value="2" >MANO DE OBRA</option>
						<!---<option value="3" >SERVICIO</option>
						<option value="4" >PERMISO</option>--->
					</cfselect>
				</td>--->
				<td align="right">Status:</td>
				<td align="left" >
					<cfselect name="id_EstatusFactura_filtro" class="contenido" style="width:200px">
						<option value="">SELECCIONE...</option>
						<option value="511" <cfif id_EstatusFactura_filtro EQ 511>selected</cfif>>INGRESADA</option>
						<option value="512" <cfif id_EstatusFactura_filtro EQ 512>selected</cfif>>CANCELADA</option>
						<option value="519" <cfif id_EstatusFactura_filtro EQ 519>selected</cfif>>TRAMITAR A PAGO</option>
                        <option value="524" <cfif id_EstatusFactura_filtro EQ 524>selected</cfif>>PAGADA</option>
                    </cfselect>
                </td>
                <td align="left">Fecha Pago vacio:</td> 
				<td align="left">
                <cfif fh_Pago_vacio EQ 1>
                <cfinput type="checkbox" name="fh_Pago_vacio" id="fh_Pago_vacio" value="1" checked="yes">
                <cfelse>
                <cfinput type="checkbox" name="fh_Pago_vacio" id="fh_Pago_vacio" value="1" checked="no">
				</cfif>
                </td> 
			</tr>

			<tr>	
				<td align="center" colspan="5">
					<button  type="submit" class="boton_imagen" onClick="javascript:imprimir(0);"><img src="../images/filtro.png" title="Clic para buscar facturas"></button>
					<cfif sn_Mostrar EQ 'S'>
						<button  type="submit" class="boton_menu_superior" onClick="javascript:imprimir(1);">
							<img src="../images/excel.png" alt="Enviar a Excel" title="clic para generar excel" width="24" border="0">
						</button>
					<cfelse>
							<img src="../images/excel.png" alt="Enviar a Excel" title="clic para generar excel" width="24" border="0">
					</cfif>
										
				</td>				
			</tr>
			<cfinput type="hidden" name="sn_Mostrar" value="S">			
		</table>
		</td><td class="x-box-mr"></td></tr><tr><td class="x-box-bl" width="1px"></td><td class="x-box-bc"></td><td class="x-box-br" width="1px"></td>
	</tr>
</table>

</cfform>
<br>
<cfif sn_Mostrar EQ 'S'>
 
<!--- <table width="990" align="center">
	<tr class="titulo">
		<td>
			<cfif #RSFacturas.rs.RecordCount# GT 0>
				<cfoutput>#RSFacturas.rs.id_Obra# - #RSFacturas.rs.de_Obra#</cfoutput>
			</cfif>
		</td>
	</tr>
</table> --->
<table cellpadding="0" cellspacing="0" class="fondo_listado" align="center">
	<tr class="encabezado_grande">
		<td width="3px"><img src="../images/esquina_izquierda.gif"></td>
		<td align="center">Obra</td>
		<td align="center">Factura</td>
		<td align="center">Semana</td>
		<td align="center">F. Factura</td>
		<td align="center">F. Recepci&oacute;n</td>
		<td align="center">F. Corte</td>
		<td align="center">Status</td>
		<td align="center">Tipo</td>
		<td align="center">Empresa</td>
		<td align="center">Subtotal</td>
		<td align="center">IVA</td>
        <td align="center">Nota credito</td>
		<td align="center">total</td>
		<td align="center">F. Pago</td>
		<!--- <td align="center">Copia</td> --->
		<td align="center">Observaciones</td>
        <td align="center">&nbsp;</td>
	    <td width="3px"><img src="../images/esquina_derecha.gif"></td>
	</tr>
	<script language="javascript" type="text/javascript">
			
		function ValidarCampos(renglon)
		{				
    			
                if($x('id_EstatusFactura_'+renglon).value == 519)//TRAMITAR A PAGO
    			{								
    				if($x('fh_Pago_'+renglon).value == '')
    				{alert('Fecha de Pago requerida');return false;}
    				if(!($x('MO_'+renglon).checked || $x('MAT_'+renglon).checked ||$x('S_'+renglon).checked || $x('TP_'+renglon).checked))
    				{alert('Tipo de factura requerido');return false;}
    				/*if(!($x('S_'+renglon).checked || $x('TP_'+renglon).checked))
    				{alert('Tipo de factura requerido');return false;}*/								
    				if($x('MAT_'+renglon).checked)//Material
    				{									
    					if($x('idu_OrdenCompra_'+renglon).value == '')
    					{				
    						alert('Orden de compra requerido');
    						return false;						
    					}	
    					var cantidad = $x('im_Restante_'+renglon).value.replace('$','0');
    					var cantidad = cantidad.replace(',','');						
    					var sumafacturas = $x('imp_SumaFacturas_'+renglon).value.replace('$','0');
    					var sumafacturas = sumafacturas.replace(',','');																											
    					/*
						if(parseFloat($x('im_SubTotal_'+renglon).value) <= parseFloat(sumafacturas))
    					{												
    					}
    					else */

					if(parseFloat( $x('im_Factura_'+renglon).value) > parseFloat($x('im_Restante_'+renglon).value) && Math.abs(parseFloat($x('im_Restante_'+renglon).value) - parseFloat( $x('im_Factura_'+renglon).value)) > 1)
    					{
    						alert('EL total de la factura: '+ $x('im_Factura_'+renglon).value +' \n no debe ser mayor al pendiente por pagar: '+ parseFloat(cantidad));
    						return false;											
    					}	
    																
    				}
    				else if($x('TP_'+renglon).checked)//[Tramites y permisos]
    				{
    					if($x('im_TotalAnalisis_'+renglon).value == '')
    					{
    						alert('Importe del total de análisis requerido');
    						return false;
    					}
    					else
    					{						
    						if( Math.floor(parseFloat($x('im_TotalAnalisis_'+renglon).value)) != Math.floor(parseFloat($x('im_SubTotal_'+renglon).value) ) )
    						{
    							alert('Importe del total de análisis incompleto');
    							return false;
    						}
    					}
    				}
    				else if($x('MO_'+renglon).checked || $x('S_'+renglon).checked) //Mano de Obra o Servicios
    				{	   
    					if($x('im_SubTotalOrden_'+renglon).value == '')
    					{
    						alert('Importe de la orden de compra requerido');
    						return false;
    					}
    					else
    					{
    						if(parseFloat($x('im_SubTotalOrden_'+renglon).value) != parseFloat($x('im_SubTotal_'+renglon).value))
    						{
    							alert('Importe de orden de compra es incompleto');
    							return false;
    						}
    					}
    				}
    			}
    			else if($x('id_EstatusFactura_'+renglon).value == 512)
    			{
    				if(!($x('MO_'+renglon).checked || $x('MAT_'+renglon).checked ||$x('S_'+renglon).checked || $x('TP_'+renglon).checked))
    				{alert('Tipo de factura requerido');return false;}
    				/*if(!($x('S_'+renglon).checked || $x('TP_'+renglon).checked))
    				{alert('Tipo de factura requerido');return false;}*/
    				if($x('fh_Pago_'+renglon).value == '')
    				{alert('Fecha de Pago requerida');return false;}
    				if($x('de_Observaciones_'+renglon).value == '')
    				{alert('Observaciones requerida');return false;}							
    				if($x('im_SubTotal_'+renglon).value == '')
    				{alert('Importe Subtotal es requerido');return false;}							
    			}
    			else
    			{
    				if(!($x('MO_'+renglon).checked || $x('MAT_'+renglon).checked ||$x('S_'+renglon).checked || $x('TP_'+renglon).checked))
    				{alert('Tipo de factura requerido');return false;}
    				/*if(!($x('S_'+renglon).checked || $x('TP_'+renglon).checked))
    				{alert('Tipo de factura requerido');return false;}*/
    				if($x('de_Observaciones_'+renglon).value == '')
    				{alert('Observaciones requerida');return false;}
    				if($x('im_SubTotal_'+renglon).value == '')
    				{alert('Importe Subtotal es requerido');return false;}							
    			}
			
		}
		
		function ActivaCampos(renglon,estatus){
			//Hacer todas las validaciones en script
			if(estatus == 519){	
				if($x('TFOriginal_'+renglon).value == 2)
				{	
					$x('MO_'+renglon).disabled = '';
					$x('MO_'+renglon).click();
				}
				else 
				if($x('TFOriginal_'+renglon).value == 1 )
				{
					$x('MAT_'+renglon).disabled = '';
					$x('MAT_'+renglon).click();
					
				}
				$x('fh_Pago_'+renglon).disabled = '';
				$x('lanzador_'+renglon).disabled = '';
				$x('de_Observaciones_'+renglon).disabled = '';
				$x('im_SubTotal_'+renglon).disabled = '';			
				/*$x('MO_'+renglon).disabled = '';
				$x('MAT_'+renglon).disabled = '';
				$x('S_'+renglon).disabled = '';
				$x('TP_'+renglon).disabled = '';
				$x('fh_Pago_'+renglon).disabled = '';
				$x('lanzador_'+renglon).disabled = '';
				//$x('sn_CopiaFactura_'+renglon).disabled = '';
				$x('de_Observaciones_'+renglon).disabled = '';
				$x('im_SubTotal_'+renglon).disabled = '';								
				$x('cl_TipoFactura_'+renglon).click();*/				
			}
			else if(estatus == 512)
			{
				$x('MO_'+renglon).disabled = '';
				$x('MAT_'+renglon).disabled = '';			
				$x('fh_Pago_'+renglon).disabled = '';
				$x('lanzador_'+renglon).disabled = '';
				$x('de_Observaciones_'+renglon).disabled = '';
				$x('im_SubTotal_'+renglon).disabled = '';
				$x('analisis_'+renglon).style.display='none';
				$x('mano_obra_'+renglon).style.display='none';
				$x('orden_compra_'+renglon).style.display='none';				
				//$x('sn_CopiaFactura_'+renglon).disabled = '';			
			}
			else{
				$x('MO_'+renglon).disabled = '';
				$x('MAT_'+renglon).disabled = '';
				$x('de_Observaciones_'+renglon).disabled = '';
				$x('im_SubTotal_'+renglon).disabled = '';
				//$x('sn_CopiaFactura_'+renglon).disabled = '';
				
				$x('fh_Pago_'+renglon).disabled = 'disabled';
				$x('lanzador_'+renglon).disabled = 'disabled';
				//Ocultar el div de los analisis
				$x('analisis_'+renglon).style.display='none';
				$x('mano_obra_'+renglon).style.display='none';
				$x('orden_compra_'+renglon).style.display='none';
				
			}
			//Mostrar el boton de guardar y combo empresa
			$x('id_Empresa_'+renglon).disabled = '';
			$x('guardar_'+renglon).style.display='';
		}
		
	</script>
	<script language="javascript">
		function CapturaAnalisis(renglon,tipo,id_contrarecibo,nd_contrarecibo,obra,importe,proveedor,empresa,estatus,id_contrarecibo,nd_contrarecibo,factura,TipoFactura)
		{		
			if(estatus == 519)
			{				
				//Tipo Materiales = 1
				if(tipo == 1 ){
					<!--- Se deshabilita div de analisis y div mano de obra en caso de que sea tramitar a pago y seleccion tipo materiales para mostrar orden de compra --->
					$x('orden_compra_'+renglon).style.display='';
					$x('analisis_'+renglon).style.display='none';
					$x('mano_obra_'+renglon).style.display='none';	
     				TipoFactura = 'MAT';
					cargar_pagina('cargar_analisis_facturas_agregar_oc.cfm?renglon_actual='+renglon+'&cl_accion=F&id_Empresa='+empresa+'&CurrentRow='+renglon+'&id_Obra='+obra+'&id_Proveedor='+proveedor+'&cl_TipoFactura='+tipo+'&im_Factura='+importe+'&TipoFactura='+TipoFactura+'&id_ContraRecibo='+id_contrarecibo+'&nd_ContraRecibo='+nd_contrarecibo+'&de_Factura='+factura, $x('div_formulario_orden_compra_'+renglon));
					cargar_pagina('cargar_analisis_facturas_agregar_oc.cfm?cl_accion=N&CurrentRow='+renglon+'&id_Empresa='+empresa+'&TipoFactura='+TipoFactura, $x('div_orden_compra_'+renglon));
					//cargar_pagina('cargar_analisis_facturas_agregar_oc.cfm?cl_accion=N&CurrentRow='+renglon+'&id_Empresa='+empresa, $x('div_orden_compra_insumos_'+renglon));								
					<!--- EIRR 09/11/2013 --->
				}
				else if(tipo == 4){			
					$x('orden_compra_'+renglon).style.display='none';	
					$x('mano_obra_'+renglon).style.display='none';				
					$x('analisis_'+renglon).style.display='';							
					$x('id_OrdenCompra_'+renglon).value = '';
					$x('im_SubTotalOrden_'+renglon).value = '';
									
					cargar_pagina('cargar_analisis_facturas_agregar_oc.cfm?id_Empresa='+empresa+'&cl_accion=F&CurrentRow='+renglon+'&id_Obra='+obra+'&im_Factura='+importe+'&id_ContraRecibo='+id_contrarecibo+'&nd_ContraRecibo='+nd_contrarecibo+'&de_Factura='+factura+'&sn_Existe=S&sn_Partida=S&TipoFactura='+TipoFactura, $x('div_formulario_'+renglon));
					
                    cargar_pagina('cargar_analisis_facturas_agregar_oc.cfm?id_Empresa='+empresa+'&cl_accion=N&CurrentRow='+renglon,$x('div_analisis_'+renglon));
				}
				//Tipo Mano de Obra = 2
				else{
					
					$x('orden_compra_'+renglon).style.display='';
					$x('analisis_'+renglon).style.display='none';
					$x('mano_obra_'+renglon).style.display='none';	
     				TipoFactura = 'MO';
					cargar_pagina('cargar_analisis_facturas_agregar_oc.cfm?renglon_actual='+renglon+'&cl_accion=F&id_Empresa='+empresa+'&CurrentRow='+renglon+'&id_Obra='+obra+'&id_Proveedor='+proveedor+'&cl_TipoFactura='+tipo+'&im_Factura='+importe+'&TipoFactura='+TipoFactura+'&id_ContraRecibo='+id_contrarecibo+'&nd_ContraRecibo='+nd_contrarecibo+'&de_Factura='+factura, $x('div_formulario_orden_compra_'+renglon));
					cargar_pagina('cargar_analisis_facturas_agregar_oc.cfm?cl_accion=N&CurrentRow='+renglon+'&id_Empresa='+empresa, $x('div_orden_compra_'+renglon));
				
					//$x('orden_compra_'+renglon).style.display='none';					
					//$x('analisis_'+renglon).style.display='none';
					//$x('mano_obra_'+renglon).style.display='';
										
					
				}
			}
		}
		function valor_empresa(renglon,empresa)
		{			
			if( $('#TP_'+renglon).is(':checked') )
				$('#id_Empresa_Actual_'+renglon).val(empresa);
		}		
	</script>  
    
    <cfajaxproxy cfc="#Application.componentes#.ordenescompra" jsclassname="jsOrdenesCompra">
    <script language="javascript">
		 var cambiarFocus = function(pop_up){				
			var nom_campo = $($(pop_up).prev())[0].id;
			var valor = $("#" + nom_campo).val();
			if(valor != "")
			{
				$("#" + nom_campo).focus().blur();
			}
		}; 	

        function ajax_getTotalOCPorID(id_obra, id_ordencompra, id_elemento_bindear,id){
            $('#' + id_elemento_bindear).html(html_imagen_descarga);
			var id_empresa = $x('id_Empresa_'+id).value;
            //alert(id_empresa +' '+ id_obra +' '+ id_ordencompra +' '+ id_elemento_bindear);
			oc= new jsOrdenesCompra();
            valor_devuelto= oc.getTotalOCPorID(id_empresa, id_obra, id_ordencompra);
			//alert(valor_devuelto);
			$x(id_elemento_bindear).value = valor_devuelto;
            //cambiarValorConEventoOnChange(id_elemento_bindear, valor_devuelto);
            }
    </script> 
      
    <cfoutput query="RSFacturas.rs" group="id_Proveedor">
        <tr>
            <td background="../images/orilla_izquierda.gif" style="background-repeat:repeat-y" width="3px"></td>
            <td colspan="17" class="cuadricula"style="font-size:14px;font-weight:bold">#RSFacturas.rs.nb_Proveedor#/#RSFacturas.rs.de_RFC#</td>
            <td background="../images/orilla_derecha.gif" style="background-repeat:repeat-y" width="3px"></td>
        </tr>       
        <cfoutput>
        <cfform name="form_facturas_#CurrentRow#">
        <tr style="font-size:10px" onMouseOver="this.style.backgroundColor='##99CCCC'" onMouseOut="this.style.backgroundColor=''">
            <td background="../images/orilla_izquierda.gif" style="background-repeat:repeat-y" width="3px"></td>
            <td class="cuadricula">#RSFacturas.rs.id_Obra#</td>
            <td class="cuadricula">#de_Factura#<cfif #de_Referencia# NEQ ''>/#de_Referencia#</cfif></td>
            <td class="cuadricula" align="center">#nu_Semana#</td>
            <td class="cuadricula">#DateFormat(fh_Factura,'dd/mm/yyyy')#</td> <!--- #DateFormat(FECHA,"dd/mm/yyyy")# --->
            <td class="cuadricula">#DateFormat(fh_Recepcion,'dd/mm/yyyy')#</td>
            <td class="cuadricula">#DateFormat(fh_Corte,'dd/mm/yyyy')#</td>
            <td class="cuadricula">
                    
                <cfif RSFacturas.rs.id_EstatusFactura EQ 524>
                    <cfselect name="id_EstatusFactura_#CurrentRow#" class="contenido" title="Seleccione un status" 
                    onChange="ActivaCampos(#CurrentRow#,this.value);" style="width:100px" disabled>               
                        <option value="511" <cfif RSFacturas.rs.id_EstatusFactura EQ 511>selected</cfif>>INGRESADA</option>
                        <option value="512" <cfif RSFacturas.rs.id_EstatusFactura EQ 512>selected</cfif>>CANCELADA</option>
                        <option value="519" <cfif RSFacturas.rs.id_EstatusFactura EQ 519>selected</cfif>>TRAMITAR A PAGO</option>
                        <option value="524" <cfif RSFacturas.rs.id_EstatusFactura EQ 524>selected</cfif>>PAGADA</option>
                    </cfselect>
                <cfelse>
                     <cfselect name="id_EstatusFactura_#CurrentRow#" class="contenido" title="Seleccione un status" 
                    onChange="ActivaCampos(#CurrentRow#,this.value);" style="width:100px" >               
                        <option value="511" <cfif RSFacturas.rs.id_EstatusFactura EQ 511>selected</cfif>>INGRESADA</option>
                        <option value="512" <cfif RSFacturas.rs.id_EstatusFactura EQ 512>selected</cfif>>CANCELADA</option>
                        <option value="519" <cfif RSFacturas.rs.id_EstatusFactura EQ 519>selected</cfif>>TRAMITAR A PAGO</option>
                        <option value="524" <cfif RSFacturas.rs.id_EstatusFactura EQ 524>selected</cfif>>PAGADA</option>
                    </cfselect>
                    
                </cfif>    
            </td>
            
            <td class="cuadricula" align="right" nowrap="nowrap">
                <cfinput type="radio" name="cl_TipoFactura_#CurrentRow#" id="MAT_#CurrentRow#"  disabled="disabled" value="1" checked="#IIF(RSFacturas.rs.cl_TipoFactura EQ 1,DE('yes'),DE('no'))#"  message="Seleccione el tipo de Factura" onClick="CapturaAnalisis(#currentrow#,this.value,#id_contrarecibo#,#nd_contrarecibo#,#id_obra#,#NumberFormat(im_SubTotal,'___.__')#,#RSFacturas.rs.id_Proveedor#,form_facturas_#CurrentRow#.id_Empresa_#CurrentRow#.value,form_facturas_#CurrentRow#.id_EstatusFactura_#CurrentRow#.value,#RSFacturas.rs.id_Contrarecibo#,#RSFacturas.rs.nd_Contrarecibo#,'#RSFacturas.rs.de_Factura#','MAT');">MAT 
                <cfinput type="radio" name="cl_TipoFactura_#CurrentRow#" id="MO_#CurrentRow#"   disabled="disabled" value="2" checked="#IIF(RSFacturas.rs.cl_TipoFactura EQ 2,DE('yes'),DE('no'))#"  message="Seleccione el tipo de Factura" onClick="CapturaAnalisis(#currentrow#,this.value,#id_contrarecibo#,#nd_contrarecibo#,#id_obra#,#NumberFormat(im_SubTotal,'___.__')#,#RSFacturas.rs.id_Proveedor#,form_facturas_#CurrentRow#.id_Empresa_#CurrentRow#.value,form_facturas_#CurrentRow#.id_EstatusFactura_#CurrentRow#.value,#RSFacturas.rs.id_Contrarecibo#,#RSFacturas.rs.nd_Contrarecibo#,'#RSFacturas.rs.de_Factura#','MO');">MO  
                <cfinput type="hidden" name="TFOriginal_#CurrentRow#" id="TFOriginal_#CurrentRow#" value="#RSFacturas.rs.cl_TipoFactura#">
            </td>
            <td class="cuadricula" align="right">
                <cfinput type="hidden" name="id_EmpresaOriginal_#CurrentRow#" id="id_EmpresaOriginal_#CurrentRow#" value="#RSFacturas.rs.id_Empresa#">          
                <cfselect  
                    onChange="valor_empresa(#CurrentRow#,this.value);"
                    name="id_Empresa_#CurrentRow#" id="id_Empresa_#CurrentRow#" query="RSEmpresas.rs" value="id_Empresa" required="yes" display="nb_Empresa" queryPosition="below" selected="#RSFacturas.rs.id_Empresa#" style="width:100px" disabled="disabled" message="Empresa es requerida" title="Empresa es requerida">
                </cfselect>
            </td>
            <td class="cuadricula" align="right">
            <!---#NumberFormat(im_SubTotal,"$,___.00")#--->
                <cfinput type="text" name="im_SubTotal_#CurrentRow#" id="im_SubTotal_#CurrentRow#" value="#NumberFormat(im_SubTotal,'_.__')#" class="contenido_flotante_negativo" validate="float" required="yes" maxlength="20" size="10" style="text-align:right" disabled="disabled">
            </td>
            <td class="cuadricula" align="right">
				<cfset iva = 0>
                <cfif #RSFacturas.rs.pj_Iva# EQ 0>
                    <cfset iva = 0>
                <cfelse>
                    <cfset importe = #RSFacturas.rs.im_Subtotal#>
                    <cfset iva = (importe * #RSFacturas.rs.pj_Iva#) / 100>
                </cfif>
                #NumberFormat(im_Iva,'_.__')#
            </td>
            <td class="cuadricula" align="right">
            	#NumberFormat(im_NotaCredito,'_.__')#
                <cfset im_Nota = iif(#im_NotaCredito# EQ '',0,#im_NotaCredito#)>
            </td>
		  	<td class="cuadricula" align="right">
			<!---#NumberFormat(im_SubTotal,"$,___.00")#--->
				<cfinput type="text" name="im_Factura_#CurrentRow#" id="im_Factura_#CurrentRow#" value="#NumberFormat(im_Factura - im_Nota ,'_.__')#" class="contenido_flotante_negativo" validate="float" required="yes" maxlength="20" size="10" style="text-align:right" disabled="disabled">
			</td>
		  	<td class="cuadricula" nowrap="nowrap">					
				<cfinput type="text" name="fh_Pago_#CurrentRow#" id="fh_Pago_#CurrentRow#" size="10" value="#DateFormat(RSFacturas.rs.fh_Pago,'dd/mm/yyyy')#" validate="eurodate" class="contenido_readOnly" readonly="yes" disabled="" message="La fecha de pago es requerida">
				<input type="button" id="lanzador_#CurrentRow#" value="..." / class="boton" disabled="">
				<script type="text/javascript">
					Calendar.setup({
						inputField     :    "fh_Pago_#CurrentRow#",      // id del campo de texto
						ifFormat       :    "%d/%m/%Y",       // formato de la fecha, cuando se escriba en el campo de texto
						button         :    "lanzador_#CurrentRow#"   // el id del botón que lanzará el calendario
					});
				</script>
			</td>
<!--- 			<td class="cuadricula" align="center">
				<cfinput type="checkbox" name="sn_CopiaFactura_#CurrentRow#" class="contenido" value="1" checked="#IIF(RSFacturas.rs.sn_CopiaFactura EQ 1,DE('yes'),DE('no'))#" disabled="disabled">
			</td> --->
		  	<td class="cuadricula">
				<cfinput type="text" name="de_Observaciones_#CurrentRow#" id="de_Observaciones_#CurrentRow#" value="#RSFacturas.rs.de_observaciones#" class="contenido" maxlength="500" message="Comentarios Requeridos" disabled="disabled">
			</td>
			<!---&&--->
			<td  class="cuadricula" align="center">								
    			
                <cfif RSFacturas.rs.id_EstatusFactura NEQ 524>
                    <a href="facturas_editar.cfm?id_Empresa=#RSFacturas.rs.id_Empresa#&id_Contrarecibo=#RSFacturas.rs.id_ContraRecibo#&nd_ContraRecibo=#RSFacturas.rs.nd_ContraRecibo#&id_Obra=#id_Obra#&de_Factura_filtro=#de_Factura_filtro#&fh_FacturaInicial_filtro=#fh_FacturaInicial_filtro#
                    &fh_FacturaFinal_filtro=#fh_FacturaFinal_filtro#&id_Proveedor_filtro=#id_Proveedor_filtro#&fh_Recepcion_filtro=#fh_Recepcion_filtro#&fh_Corte_filtro=#fh_Corte_filtro#&nu_Semana_filtro=#nu_Semana_filtro#&id_EstatusFactura_filtro=#id_EstatusFactura_filtro#
                   	&sn_Mostrar=#sn_Mostrar#"><img src="../images/edit.png" alt="Clic para editar factura" border="0"></a>
                <cfelse>
                    <a href="##" > <img src="../images/edit_gris.png" alt="" /></a>
                
                </cfif>   
            </td>
                <cfif RSFacturas.rs.cl_TipoFactura EQ 2 and RSFacturas.rs.id_OrdenTrabajo NEQ "">
				<td  class="cuadricula" align="center">
                    <a href="rpt_impresion_ot.cfr?id_Empresa=#RSFacturas.rs.id_Empresa#&id_Obra=#id_Obra#&id_OrdenTrabajo=#RSFacturas.rs.id_OrdenTrabajo#&id_OrdenTrabajoInicial=#RSFacturas.rs.id_OrdenTrabajo#&id_OrdenTrabajoFinal=#RSFacturas.rs.id_OrdenTrabajo#&id_Proveedor=#RSFacturas.rs.id_Proveedor#" target="_blank"><img src="../images/imprimir.png" alt="Clic para editar factura" border="0"></a>
				</td>
                </cfif>
				<cfif RSFacturas.rs.cl_TipoFactura EQ 1 and RSFacturas.rs.id_OrdenCompra NEQ "">
                <td  class="cuadricula" align="center">
                     <a href="rpt_impresion_orden_compra.cfr?nb_Imagen=../images/logotipo_1.png&id_Empresa=#RSFacturas.rs.id_Empresa#&id_Obra=#id_Obra#&id_OrdenCompra=#RSFacturas.rs.id_OrdenCompra#" target="_blank"><img src="../images/imprimir.png" alt="Clic para editar factura" border="0"></a>
				</td>
                </cfif>     
            <td background="../images/orilla_derecha.gif" style="background-repeat:repeat-y" width="3px"></td>
 
		</tr>       
		<!--- Cargar los Ajax de Materiales--->
		<tr id="analisis_#CurrentRow#" style="display:none">
			<td background="../images/orilla_izquierda.gif" style="background-repeat:repeat-y" width="3px"></td>
			<td class="cuadricula" colspan="8">
				<div align="left" id="div_formulario_#CurrentRow#"></div>
			</td>
			<td class="cuadricula" colspan="8" align="left">
				<div align="left" id="div_analisis_#CurrentRow#"></div>
			</td>
			<td background="../images/orilla_derecha.gif" style="background-repeat:repeat-y" width="3px"></td>
		</tr>
		<!--- Cargar Mano de Obra --->
		<tr id="mano_obra_#CurrentRow#" style="display:none">
			<td background="../images/orilla_izquierda.gif" style="background-repeat:repeat-y" width="3px"></td>
			<td class="cuadricula" colspan="16" align="center">
				<div align="left" id="div_mano_obra_#CurrentRow#">             
					<table width="300px" align="center">
						<tr>
							<td align="right">Orden Compra:</td>
							<td align="left" colspan="2">
								<cfinput type="text" class="contenido_readOnly" message="Clave de Orden Compra no valida" name="id_OrdenCompra_#CurrentRow#" id="id_OrdenCompra_#CurrentRow#"  size="15"
                                onBlur="ajax_getTotalOCPorID(#id_Obra#,this.value,'im_SubTotalOrden_'+#CurrentRow#,#CurrentRow#)"
                                >
                                &nbsp;<button onFocus="cambiarFocus($(this).before());" type="button" id="btnBuscarOrdenCompra" onClick="javascript:AbrirPopUpBusquedaChrome('id_OrdenCompra_#CurrentRow#', 'pop_ordenes_busqueda.cfm', 'id_Empresa='+form_facturas_#CurrentRow#.id_Empresa_#CurrentRow#.value+'&id_Obra=#id_Obra#&id_Proveedor=#id_Proveedor#&im_Factura='+form_facturas_#CurrentRow#.im_SubTotal_#CurrentRow#.value, '700px', '600px')" class="boton_popup" value=""><img src="../images/buscar.png" title="Clic para buscar orden de compra" alt="Clic para buscar orden de compra" width="24"></button>
							</td>
						</tr>
						<tr>
							<td align="right">SubTotal:</td>
							<td align="left" colspan="2">
								 <cfinput  id="im_SubTotalOrden_#CurrentRow#" name="im_SubTotalOrden_#CurrentRow#" class="contenido_readOnly" size="15" 
									
								> 
								<!---bind="cfc:#Application.componentes#.ordenescompra.RSObtenerOrdenesCompraFacturasTotal({id_Empresa_#CurrentRow#@blur},#id_Obra#,#id_Proveedor#,{id_OrdenCompra_#CurrentRow#@blur})" bindonload="no"--->
							</td>
						</tr>
					</table>
				</div>
			</td>
			<td background="../images/orilla_derecha.gif" style="background-repeat:repeat-y" width="3px"></td>
		</tr>
        <!--- Cargar los Ajax de Ordenes compra --->
		<tr id="orden_compra_#CurrentRow#" style="display:none">
			<td background="../images/orilla_izquierda.gif" style="background-repeat:repeat-y" width="3px"></td>
			<td class="cuadricula" colspan="4">
				 <div align="left" id="div_formulario_orden_compra_#CurrentRow#"></div> 
			</td>
			<td class="" colspan="12" align="left">
			     <div align="left" id="div_orden_compra_#CurrentRow#"></div>
			</td>
            <!--- <td class="cuadricula" colspan="6" align="left">
			     <div align="left" id="div_orden_compra_insumos_#CurrentRow#"></div>
			</td> --->
			<td background="../images/orilla_derecha.gif" style="background-repeat:repeat-y" width="3px"></td>
		</tr>     
		<!--- Boton de Guardar --->
		<tr id="guardar_#CurrentRow#" style="display:none">
			<td background="../images/orilla_izquierda.gif" style="background-repeat:repeat-y" width="3px"></td>
			<td colspan="16" align="center" class="cuadricula">
				<!--- <cfinput type="hidden" name="im_Factura_#CurrentRow#" value="#NumberFormat(im_Subtotal,'_.__')#"> --->
				<cfinput type="hidden" id="im_TotalAnalisis_#CurrentRow#" name="im_TotalAnalisis_#CurrentRow#" message="Importe incompleto">
                <cfinput type="hidden" name="idu_OrdenCompra_#CurrentRow#" id="idu_OrdenCompra_#CurrentRow#">
                <!--- <cfinput type="hidden" name="im_Restante_#CurrentRow#" id="im_Restante_#CurrentRow#"> --->   
                <cfinput type="hidden" name="imp_SumaFacturas_#CurrentRow#" id="imp_SumaFacturas_#CurrentRow#">                             
				<button type="submit" onClick="return ValidarCampos(#CurrentRow#);" class="boton_imagen"><img src="../images/guardar.png" title="Clic para guardar"></button>
	        </td>
			<td background="../images/orilla_derecha.gif" style="background-repeat:repeat-y" width="3px"></td>
		</tr>
		<cfinput type="hidden" name="sn_Guardar" value="S">
		<cfinput type="hidden" name="id_Contrarecibo" value="#RSFacturas.rs.id_Contrarecibo#">
		<cfinput type="hidden" name="nd_Contrarecibo" value="#RSFacturas.rs.nd_Contrarecibo#">
		<cfinput type="hidden" name="id_Proveedor" value="#RSFacturas.rs.id_Proveedor#">
		<cfinput type="hidden" name="de_Factura" value="#RSFacturas.rs.de_Factura#">
		<cfinput type="hidden" name="CurrentRow" value="#CurrentRow#">
		<cfinput type="hidden" name="de_Referencia" value="#RSFacturas.rs.de_Referencia#">
      	                
	
	<!--- Variables del Filtro--->		
		<cfinput type="hidden" name="id_Obra" value="#id_Obra#">
		<cfinput type="hidden" name="de_Factura_filtro" value="#de_Factura_filtro#">
		<cfinput type="hidden" name="fh_FacturaInicial_filtro" value="#fh_FacturaInicial_filtro#">
		<cfinput type="hidden" name="fh_FacturaFinal_filtro" value="#fh_FacturaFinal_filtro#">
		<cfinput type="hidden" name="id_Proveedor_filtro" value="#id_Proveedor_filtro#">
		<cfinput type="hidden" name="fh_Recepcion_filtro" value="#fh_Recepcion_filtro#">
		<cfinput type="hidden" name="fh_Corte_filtro" value="#fh_Corte_filtro#">
		<cfinput type="hidden" name="fh_Pago" value="#fh_Pago_filtro#">
		<cfinput type="hidden" name="nu_Semana_filtro" value="#nu_Semana_filtro#">
		<cfinput type="hidden" name="id_EstatusFactura_filtro" value="#id_EstatusFactura_filtro#">
		<cfinput type="hidden" name="im_SubTotal" value="#NumberFormat(im_SubTotal,'_.__')#">
        			
		</cfform>
        </cfoutput>
	</cfoutput>
	<cfif #RSFacturas.rs.RecordCount# EQ 0>
	<tr>
		<td class="cuadricula" colspan="16" align="center" width="990">
			No se encontraron facturas
		</td>
	</tr>
	</cfif>
		<tr>
			<td class="tabla_grid_orilla_izquierda"></td>
			<td align="center" colspan="17">
				<cfif #sn_Mostrar# EQ "S">
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<cfoutput>
							<tr>
								<td width="20%" align="center">
									<cfif PageNum_RSFacturas GT 1>
										<a href="cargar_analisis_facturas_oc.cfm?sn_Mostrar=#sn_Mostrar#&id_Obra=#id_Obra#&de_Factura_filtro=#de_Factura_filtro#&fh_FacturaInicial_filtro=#fh_FacturaInicial_filtro#&fh_FacturaFinal_filtro=#fh_FacturaFinal_filtro#&id_Proveedor_filtro=#id_Proveedor_filtro#&fh_Recepcion_filtro=#fh_Recepcion_filtro#&fh_Corte_filtro=#fh_Corte_filtro#&fh_Pago_filtro=#fh_Pago_filtro#&nu_Semana_filtro=#nu_Semana_filtro#&id_EstatusFactura_filtro=#id_EstatusFactura_filtro#&PageNum_RSFacturas=1"><img src="../images/primero.png" border="0"></a>
									</cfif>
								</td>
								<td width="20%" align="center">
									<cfif PageNum_RSFacturas GT 1>
										<a href="cargar_analisis_facturas_oc.cfm?sn_Mostrar=#sn_Mostrar#&id_Obra=#id_Obra#&de_Factura_filtro=#de_Factura_filtro#&fh_FacturaInicial_filtro=#fh_FacturaInicial_filtro#&fh_FacturaFinal_filtro=#fh_FacturaFinal_filtro#&id_Proveedor_filtro=#id_Proveedor_filtro#&fh_Recepcion_filtro=#fh_Recepcion_filtro#&fh_Corte_filtro=#fh_Corte_filtro#&fh_Pago_filtro=#fh_Pago_filtro#&nu_Semana_filtro=#nu_Semana_filtro#&id_EstatusFactura_filtro=#id_EstatusFactura_filtro#&PageNum_RSFacturas=#Max(DecrementValue(PageNum_RSFacturas),1)#"><img src="../images/anterior.png" border="0"></a>
									</cfif>
								</td>
								<td width="20%" nowrap="nowrap">
									<cfoutput>Registro #StartRow_RSFacturas# al #EndRow_RSFacturas# de #RSFacturas.rs.RecordCount#</cfoutput>
								</td>
								<td width="20%" align="center">
									<cfif PageNum_RSFacturas LT TotalPages_RSFacturas>
										<a href="cargar_analisis_facturas_oc.cfm?sn_Mostrar=#sn_Mostrar#&id_Obra=#id_Obra#&de_Factura_filtro=#de_Factura_filtro#&fh_FacturaInicial_filtro=#fh_FacturaInicial_filtro#&fh_FacturaFinal_filtro=#fh_FacturaFinal_filtro#&id_Proveedor_filtro=#id_Proveedor_filtro#&fh_Recepcion_filtro=#fh_Recepcion_filtro#&fh_Corte_filtro=#fh_Corte_filtro#&fh_Pago_filtro=#fh_Pago_filtro#&nu_Semana_filtro=#nu_Semana_filtro#&id_EstatusFactura_filtro=#id_EstatusFactura_filtro#&PageNum_RSFacturas=#Min(IncrementValue(PageNum_RSFacturas),TotalPages_RSFacturas)#"><img src="../images/siguiente.png" border="0"></a>
									</cfif>
								</td>
								<td width="20%" align="center">
									<cfif PageNum_RSFacturas LT TotalPages_RSFacturas>
										<a href="cargar_analisis_facturas_oc.cfm?sn_Mostrar=#sn_Mostrar#&id_Obra=#id_Obra#&de_Factura_filtro=#de_Factura_filtro#&fh_FacturaInicial_filtro=#fh_FacturaInicial_filtro#&fh_FacturaFinal_filtro=#fh_FacturaFinal_filtro#&id_Proveedor_filtro=#id_Proveedor_filtro#&fh_Recepcion_filtro=#fh_Recepcion_filtro#&fh_Corte_filtro=#fh_Corte_filtro#&fh_Pago_filtro=#fh_Pago_filtro#&nu_Semana_filtro=#nu_Semana_filtro#&id_EstatusFactura_filtro=#id_EstatusFactura_filtro#&PageNum_RSFacturas=#TotalPages_RSFacturas#"><img src="../images/ultimo.png" border="0"></a>
									</cfif>
								</td>
							</tr>
						</cfoutput>
					</table>
				</td>
			</cfif>
			<td class="tabla_grid_orilla_derecha"></td>
		</tr>
	
	<tr style="background-image:url(../images/abajo.gif)">
		<td width="3px"><img src="../images/abajo_izquierda.gif"></td>
		<td align="center" colspan="16"></td>
		<td width="3px"><img src="../images/abajo_derecha.gif"></td>	
	</tr>

</table>
<br />
	
</cfif>

<!--Lanzador Fecha-->
<script type="text/javascript">
	Calendar.setup({
		inputField     :    "fh_FacturaInicial_filtro",      // id del campo de texto
		ifFormat       :    "%d/%m/%Y",       // formato de la fecha, cuando se escriba en el campo de texto
		button         :    "lanzadorDeFecha"   // el id del botón que lanzará el calendario
	});
</script>
<script type="text/javascript">
	Calendar.setup({
		inputField     :    "fh_FacturaFinal_filtro",      // id del campo de texto
		ifFormat       :    "%d/%m/%Y",       // formato de la fecha, cuando se escriba en el campo de texto
		button         :    "lanzadorAFecha"   // el id del botón que lanzará el calendario
	});
</script>
<script type="text/javascript">
	Calendar.setup({
		inputField     :    "fh_Recepcion_filtro",      // id del campo de texto
		ifFormat       :    "%d/%m/%Y",       // formato de la fecha, cuando se escriba en el campo de texto
		button         :    "lanzadorFechaRecepcion"   // el id del botón que lanzará el calendario
	});
</script>
<script type="text/javascript">
	Calendar.setup({
		inputField     :    "fh_Pago_filtro",      // id del campo de texto
		ifFormat       :    "%d/%m/%Y",       // formato de la fecha, cuando se escriba en el campo de texto
		button         :    "lanzadorFechaPago"   // el id del botón que lanzará el calendario
	});
</script>
<script type="text/javascript">
	Calendar.setup({
		inputField     :    "fh_Corte_filtro",      // id del campo de texto
		ifFormat       :    "%d/%m/%Y",       // formato de la fecha, cuando se escriba en el campo de texto
		button         :    "lanzadorFechaCorte"   // el id del botón que lanzará el calendario
	});
</script>
</body>
</html>