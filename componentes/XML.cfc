﻿<cfcomponent extends='conexion'>
	<!---Funcion para Subir XML y validarlo (Portal)----------------->
    <!---Desarrollador: Miguel Tiznado ------------------------------>
    <!---Fecha: 06/10/2015------------------------------------------->
	<cffunction name="upload_XML" access="public" returntype="struct">
		<cfargument name="fileField" 		type="string" required="true">
        <cftry>
			<cfset Result.SUCCESS = true>
			<cfset Result.TipoError = ''>
			<cfset Result.MESSAGE = 'XML comprobado satisfactoriamente.'>
			

			<!--- Verificamos si va a tener --->
			<cfquery datasource='#cnx#' name="RSDatos">
	        	SELECT 1 FROM Proveedores_ReducirValidaciones_XML
				WHERE id_Proveedor = #Session.id_Proveedor#
			</cfquery>
			<cfset ReducirValidaciones = IIF(RSDatos.RecordCount GT 0, true, false)>


			<cfset UploadDir = "#ExpandPath('../portal/contrarecibos/')#">                  
			<!--- Crear directorio si no existe--->
			<cfif not DirectoryExists(UploadDir)>
				<cfdirectory action="create" directory="#UploadDir#">
			</cfif>
			
			<!---Subir el archivo XML --->
			<cffile action="upload" fileField="#fileField#" destination="#UploadDir#"  nameConflict="makeunique" result="Upload">
			
			<!---Renombra XML sin caracteres especiales --->
			<cfset NuevoNombre = Upload.SERVERFILE>
			<cfset NuevoNombre = ReplaceList(NuevoNombre, 'á,é,í,ó,ú,ñ,##,&,?,à,è,ì,ò,ù,''', 'a,e,i,o,u,n,,,,a,e,i,o,u,')>
			<cfset NuevoNombre = ReplaceList(NuevoNombre, 'Á,É,Í,Ó,Ú,Ñ,##,&,?,À,È,Ì,Ò,Ù,''', 'A,E,I,O,U,N,,,,A,E,I,O,U,')>
    		<cffile action="rename" source="#UploadDir##Upload.SERVERFILE#" destination="#UploadDir##NuevoNombre#">
			<cfset Result.RutaXML = "#UploadDir##NuevoNombre#">
			
			<!---Leer el archivo XML --->
			<cfset XML = xmlParse(Result.RutaXML)>
			
			<!---
				********************************************************************************
				**********************Valida la estructura del Archivo XML**********************
				********************************************************************************
			--->
			<!---Datos Generales --->
			<cfset Result.TipoError 		&= IIF(NOT isDefined("XML.Comprobante.XmlAttributes.version"), 				DE("<br>El atributo 'version' no ha sido encontrado."), DE(''))>
			<cfset Result.TipoError 		&= IIF(NOT isDefined("XML.Comprobante.XmlAttributes.tipoDeComprobante"), 	DE("<br>El atributo 'tipoDeComprobante' no ha sido encontrado."), DE(''))>
			<cfset Result.TipoError 		&= IIF(NOT isDefined("XML.Comprobante.XmlAttributes.formaDePago"), 			DE("<br>El atributo 'formaDePago' no ha sido encontrado."), DE(''))>
			<cfset Result.TipoError 		&= IIF(NOT isDefined("XML.Comprobante.XmlAttributes.subTotal"), 			DE("<br>El atributo 'subTotal' no ha sido encontrado."), DE(''))>
			<cfset Result.TipoError 		&= IIF(NOT isDefined("XML.Comprobante.XmlAttributes.total"), 				DE("<br>El atributo 'total' no ha sido encontrado."), DE(''))>
			<cfset Result.TipoError 		&= IIF(NOT isDefined("XML.Comprobante.XmlAttributes.metodoDePago"), 		DE("<br>El atributo 'metodoDePago' no ha sido encontrado."), DE(''))>
			<cfset Result.TipoError 		&= IIF(NOT isDefined("XML.Comprobante.XmlAttributes.LugarExpedicion"), 		DE("<br>El atributo 'LugarExpedicion' no ha sido encontrado."), DE(''))>
			<cfset Result.TipoError 		&= IIF(NOT isDefined("XML.Comprobante.XmlAttributes.fecha"), 				DE("<br>El atributo 'fecha' no ha sido encontrado."), DE(''))>
			<cfset Result.TipoError 		&= IIF(NOT isDefined("XML.Comprobante.XmlAttributes.sello"), 				DE("<br>El atributo 'sello' no ha sido encontrado."), DE(''))>
			<cfset Result.TipoError 		&= IIF(NOT isDefined("XML.Comprobante.XmlAttributes.noCertificado"), 		DE("<br>El atributo 'noCertificado' no ha sido encontrado."), DE(''))>
			<cfset Result.TipoError 		&= IIF(NOT isDefined("XML.Comprobante.XmlAttributes.certificado"), 			DE("<br>El atributo 'certificado' no ha sido encontrado."), DE(''))>
			<cfset Result.TipoError 		&= IIF(NOT isDefined("XML.Comprobante.Complemento.TimbreFiscalDigital.XmlAttributes.UUID"), 			DE("<br>El atributo 'Folio Fiscal (UUID)' no ha sido encontrado."), DE(''))>
			
			<cfset Result.version 			= XML.Comprobante.XmlAttributes['version']>
			<cfset Result.tipoDeComprobante = XML.Comprobante.XmlAttributes['tipoDeComprobante']>
			<cfset Result.formaDePago 		= XML.Comprobante.XmlAttributes['formaDePago']>
			<cfset Result.subTotal 			= XML.Comprobante.XmlAttributes['subTotal']>
			<cfset Result.total 			= XML.Comprobante.XmlAttributes['total']>
			<cfset Result.metodoDePago 		= XML.Comprobante.XmlAttributes['metodoDePago']>
			<cfset Result.LugarExpedicion	= XML.Comprobante.XmlAttributes['LugarExpedicion']>
			<cfset Result.Fecha 			= XML.Comprobante.XmlAttributes['fecha']>
			<cfset Result.Sello_ 			= XML.Comprobante.XmlAttributes['sello']>
			<cfset Result.noCertificado 	= XML.Comprobante.XmlAttributes['noCertificado']>
			<cfset Result.certificado 		= XML.Comprobante.XmlAttributes['certificado']>
			
			<!---Si no se encuentra el atributo folio del comprobante, entences el folio sera el folio fiscal (UUID) --->
			<cfif isDefined('XML.Comprobante.XmlAttributes.folio')>
				<cfset Result.folio = XML.Comprobante.XmlAttributes['folio']>
			<cfelse>
				<cfset Result.folio = XML.Comprobante.Complemento.TimbreFiscalDigital.XmlAttributes['UUID']>
			</cfif>
			

			<!---Datos Emisor --->
			<cfset Result.TipoError 		&= IIF(NOT isDefined("XML.Comprobante.Emisor.XmlAttributes.rfc"), 							DE("<br>El atributo 'rfc' del nodo 'Emisor' no ha sido encontrado."), DE(''))>
			<cfset Result.Emisor.RFC 		= XML.Comprobante.Emisor.XmlAttributes['rfc']>
			
			<cfif isDefined("XML.Comprobante.Emisor.DomicilioFiscal") AND NOT ReducirValidaciones>
				<cfset Result.TipoError 		&= IIF(NOT isDefined("XML.Comprobante.Emisor.DomicilioFiscal.XmlAttributes.pais"),DE("<br>El atributo 'pais' del nodo 'DomicilioFiscal' no ha sido encontrado."), DE(''))>
				<cfset Result.TipoError 		&= IIF(NOT isDefined("XML.Comprobante.Emisor.DomicilioFiscal.XmlAttributes.municipio"),DE("<br>El atributo 'municipio' del nodo 'DomicilioFiscal' no ha sido encontrado."), DE(''))>
				<cfset Result.TipoError 		&= IIF(NOT isDefined("XML.Comprobante.Emisor.DomicilioFiscal.XmlAttributes.estado"),DE("<br>El atributo 'estado' del nodo 'DomicilioFiscal' no ha sido encontrado."), DE(''))>
				<cfset Result.TipoError 		&= IIF(NOT isDefined("XML.Comprobante.Emisor.DomicilioFiscal.XmlAttributes.codigoPostal"),DE("<br>El atributo 'codigoPostal' del nodo 'DomicilioFiscal' no ha sido encontrado."), DE(''))>

				<cfset Result.Emisor.pais		= XML.Comprobante.Emisor.DomicilioFiscal.XmlAttributes['pais']>
				<cfset Result.Emisor.municipio	= XML.Comprobante.Emisor.DomicilioFiscal.XmlAttributes['municipio']>
				<cfset Result.Emisor.estado		= XML.Comprobante.Emisor.DomicilioFiscal.XmlAttributes['estado']>
				<cfset Result.Emisor.codigoPostal= XML.Comprobante.Emisor.DomicilioFiscal.XmlAttributes['codigoPostal']>
			</cfif>
			<cfif isDefined('XML.Comprobante.Emisor.ExpedidoEn')>
				<cfset Result.TipoError 				&= IIF(NOT isDefined("XML.Comprobante.Emisor.ExpedidoEn.XmlAttributes.pais"),DE("<br>El atributo 'pais' del nodo 'ExpedidoEn' no ha sido encontrado."), DE(''))>
				<cfset Result.Emisor.ExpedidoEn.pais	= XML.Comprobante.Emisor.ExpedidoEn.XmlAttributes['pais']>
			</cfif>

			<cfif isdefined("XML.Comprobante.Emisor.RegimenFiscal.XmlAttributes.Regimen")>
				<cfset Result.TipoError 				&= IIF(NOT isDefined("XML.Comprobante.Emisor.RegimenFiscal.XmlAttributes.Regimen"),DE("<br>El atributo 'Regimen' del nodo 'RegimenFiscal' no ha sido encontrado."), DE(''))>
				<cfset Result.Emisor.RegimenFiscal.Regimen	= XML.Comprobante.Emisor.RegimenFiscal.XmlAttributes['Regimen']>
			</cfif>
			
			<!---Datos Receptor --->
			<cfset Result.TipoError 		&= IIF(NOT isDefined("XML.Comprobante.Receptor.XmlAttributes.rfc"), 				DE("<br>El atributo 'rfc' del nodo 'Receptor' no ha sido encontrado."), DE(''))>
			<cfset Result.Receptor.RFC 		= XML.Comprobante.Receptor.XmlAttributes['rfc']>
			
			<cfif isDefined("XML.Comprobante.Receptor.Domicilio")>
				<cfset Result.TipoError 		&= IIF(NOT isDefined("XML.Comprobante.Receptor.Domicilio.XmlAttributes.pais"), 		DE("<br>El atributo 'pais' del nodo 'Receptor' no ha sido encontrado."), DE(''))>
				<cfset Result.Receptor.pais 	= XML.Comprobante.Receptor.Domicilio.XmlAttributes['pais']>
			</cfif>
			

			<!---Conceptos --->
			<cfset Result.Conceptos = arrayNew(1)>
			<cfloop array="#XML.Comprobante.Conceptos.XmlChildren#" index="i" >
				<cfset Result.TipoError 		&= IIF(NOT isDefined("i.XmlAttributes.cantidad"), 		DE("<br>El atributo 'cantidad' del nodo 'Conceptos' no ha sido encontrado."), DE(''))>
				<cfset Result.TipoError 		&= IIF(NOT isDefined("i.XmlAttributes.unidad"), 		DE("<br>El atributo 'unidad' del nodo 'Conceptos' no ha sido encontrado."), DE(''))>
				<cfset Result.TipoError 		&= IIF(NOT isDefined("i.XmlAttributes.descripcion"), 	DE("<br>El atributo 'descripcion' del nodo 'Conceptos' no ha sido encontrado."), DE(''))>
				<cfset Result.TipoError 		&= IIF(NOT isDefined("i.XmlAttributes.valorUnitario"), 	DE("<br>El atributo 'valorUnitario' del nodo 'Conceptos' no ha sido encontrado."), DE(''))>
				<cfset Result.TipoError 		&= IIF(NOT isDefined("i.XmlAttributes.importe"), 		DE("<br>El atributo 'importe' del nodo 'Conceptos' no ha sido encontrado."), DE(''))>
				
				<cfset concepto = structNew()>
				<cfset concepto.cantidad		= i.XmlAttributes['cantidad']>
				<cfset concepto.unidad			= i.XmlAttributes['unidad']>
				<cfset concepto.descripcion		= i.XmlAttributes['descripcion']>
				<cfset concepto.valorUnitario	= i.XmlAttributes['valorUnitario']>
				<cfset concepto.importe			= i.XmlAttributes['importe']>
				<cfset ArrayAppend(Result.Conceptos,concepto)>
			</cfloop>
			
			<!---Impuestos Retenciones --->
			<cfset Result.RetencionISR = 0>
			<cfset Result.RetencionIVA = 0>
			<cfif IsDefined('XML.Comprobante.Impuestos.Retenciones')>
				<cfset Result.Retenciones = arrayNew(1)>
				<cfloop array="#XML.Comprobante.Impuestos.Retenciones.XmlChildren#" index="i">
					<cfset Result.TipoError 		&= IIF(NOT isDefined("i.XmlAttributes.impuesto"), 		DE("<br>El atributo 'impuesto' del nodo 'Retenciones' no ha sido encontrado."), DE(''))>
					<cfset Result.TipoError 		&= IIF(NOT isDefined("i.XmlAttributes.importe"), 		DE("<br>El atributo 'importe' del nodo 'Retenciones' no ha sido encontrado."), DE(''))>
				
					<cfset retencion = structNew()>
					<cfset retencion.impuesto	= i.XmlAttributes['impuesto']>
					<cfset retencion.importe	= i.XmlAttributes['importe']>

					<cfset ArrayAppend(Result.Retenciones,retencion)>
					
					<cfif retencion.impuesto EQ 'ISR'>
						<cfset Result.RetencionISR = retencion.importe>
					<cfelseif retencion.impuesto EQ 'IVA'>
						<cfset Result.RetencionIVA += retencion.importe>
					</cfif>
				</cfloop>
			</cfif>
			
			<!---Impuestos Traslados --->
			<cfset Result.TrasladoIva = 0>	
			<cfif IsDefined('XML.Comprobante.Impuestos.Traslados')>
				<cfset Result.Traslados = arrayNew(1)>
				<cfloop array="#XML.Comprobante.Impuestos.Traslados.XmlChildren#" index="i">
					<cfset Result.TipoError 		&= IIF(NOT isDefined("i.XmlAttributes.impuesto"), 		DE("<br>El atributo 'impuesto' del nodo 'Traslados' no ha sido encontrado."), DE(''))>
					<cfset Result.TipoError 		&= IIF(NOT isDefined("i.XmlAttributes.importe"), 		DE("<br>El atributo 'importe' del nodo 'Traslados' no ha sido encontrado."), DE(''))>
					<cfset Result.TipoError 		&= IIF(NOT isDefined("i.XmlAttributes.tasa"), 		DE("<br>El atributo 'tasa' del nodo 'Traslados' no ha sido encontrado."), DE(''))>

					<cfset Traslado = structNew()>
					<cfset Traslado.impuesto	= i.XmlAttributes['impuesto']>
					<cfset Traslado.tasa		= i.XmlAttributes['tasa']>
					<cfset Traslado.importe		= i.XmlAttributes['importe']>
					<cfset ArrayAppend(Result.Traslados,Traslado)>
					
					<cfif Traslado.impuesto EQ 'IVA'>
						<cfset Result.TrasladoIva += Traslado.importe>	
					</cfif>
				</cfloop>
			</cfif>
			
			<cfset Result.UUID = XML.Comprobante.Complemento.TimbreFiscalDigital.XmlAttributes['UUID']>
			<!---
				********************************************************************************
				******************************SERVICIO WEB DEL SAT******************************
				********************************************************************************
				Se hace el invoke del servicio web del SAT para validar el Folio UUID 
				en base al RFC Receptor, RFC Emisor, FOLIO(UUID), y total--->
			<cfinvoke  
				webservice="https://consultaqr.facturaelectronica.sat.gob.mx/ConsultaCFDIService.svc?singleWsdl" 
				method="Consulta"
				expresionImpresa="?re=#Result.Emisor.RFC#&rr=#Result.Receptor.RFC#&tt=#Result.total#&id=#Result.UUID#"
				returnvariable="servicio"> 
			<cfset Result.Estatus = servicio.getCodigoEstatus()>
			<cfset Result.Estado = servicio.getEstado()>
			<cfif  Left(Result.Estatus, 1) NEQ 'S'>
				<cfset Result.SUCCESS = false>
				<cfset Result.MESSAGE = Result.Estatus & 'No se pudo comprobar el estatus de la factura'>
				<cfset Result.TipoError = Result.Estatus>
			<cfelseif Result.Estado NEQ 'Vigente'>
				<cfset Result.SUCCESS = false>
				<cfset Result.MESSAGE = 'El estado de la factura no es vigente'>
				<cfset Result.TipoError = Result.Estado>
			</cfif>
			
			<!---
				********************************************************************************
				************************Validacion del Sello con OPENSSL************************
				********************************************************************************
			--->
			<cftry>
				<cfset directory = #Application.sitepath# >
				<cfif Right(directory, 7) EQ 'portal\'> 
					<cfset directory = Left(directory, Len(directory) - 7)> <!---Quitamos la parte de 'portal\'  --->
				</cfif>
				<cfif   StructKeyExists(XML.Comprobante.XmlAttributes, 'certificado') AND  XML.Comprobante.XmlAttributes.certificado NEQ ''>
					<cfif XML.Comprobante.XmlAttributes.version EQ '3.2'>
						<cffile action="read" file="#directory#portal\cadenaoriginal_3_2.xslt" variable="xmltrans">
					<cfelseif XML.Comprobante.XmlAttributes.version EQ '3' OR XML.Comprobante.XmlAttributes.sello EQ '3.0'>
						<cffile action="read" file="#directory#portal\cadenaoriginal_3_0.xslt'" variable="xmltrans">
					<cfelseif XML.Comprobante.XmlAttributes.version EQ '2.2'>
						<cffile action="read" file="#directory#portal\cadenaoriginal_2_2.xslt" variable="xmltrans">
					<cfelseif XML.Comprobante.XmlAttributes.version EQ '2' OR XML.Comprobante.XmlAttributes.sello EQ '2.0'>
						<cffile action="read" file="#directory#portal\cadenaoriginal_2_0.xslt" variable="xmltrans">
					</cfif>
	
					<!---Se guarda la cadena del certificado y el sello en una variable--->
					<cfset Pem = #XML.Comprobante.XmlAttributes.certificado#>
					<cfset Cert = #XML.Comprobante.XmlAttributes.sello#>
					<cfset fechaXML = #Left(XML.Comprobante.XmlAttributes.fecha, 10)#>
					<cfset countdown = 1>
					<!---Salto de línea--->
					<cfset br = "#chr(13)##chr(10)#">
					<!---Variable de Certificado--->
					<cfset certificado = "-----BEGIN CERTIFICATE-----"& #br#>
					<!---Ciclo para dividir la cadena del certificado en cadenas de 64 caracteres--->
					<cfloop
							index="intChar"
							from="1"
							to="#Len(Pem)#"
							step="1">
						<cfset strChar = #Mid(Pem, countdown, 64)# />
						<cfset countdown = countdown + 64>
						<!---Mientras siga habiendo líneas en el certificado.--->
						<cfif strChar NEQ ''>
							<cfset certificado = #certificado# & #strChar# &#br#>
						</cfif>
					</cfloop>
					<cfset certificado = #certificado# &"-----END CERTIFICATE-----">
					
					<!---Se guarda el certificado formateado en el archivo certificado.PEM--->
					<cffile action ="write" file= "#directory#portal\Contrarecibos\certificado.PEM" output= "#certificado#" addNewLine= "no" charset= "utf-8">
					<!---Variable de Sello--->
					<cfset sello = "">
					<cfset countdown = 1>
					<cfif   StructKeyExists(XML.Comprobante.XmlAttributes, 'sello') 
						AND  XML.Comprobante.XmlAttributes.sello NEQ ''>
						<cfloop index="intChar"
								from="1"
								to="#Len(Pem)#"
								step="1">
							<cfset strChar = #Mid(Cert, countdown, 64)# />
							<cfset countdown = countdown + 64>
							<!---Mientras siga habiendo líneas en el Sello.--->
							<cfif strChar NEQ ''>
							    <cfset Sello = #Sello# & #strChar# &#br#>
							</cfif>
						</cfloop>
					</cfif>
					<!---Se guarda el sello formateado en el archivo sello.txt--->
					<cffile action ="write" file= "#directory#portal\Contrarecibos\sello.txt" output="#sello#" addNewLine="no" charset="utf-8">
					
					<!---Se genera la cadena original utilizando la función de transformación de XMLs. xslt--->
					<cfset cadena_Original = xslt( Result.RutaXML, xmltrans)>
					<!---Se guarda la cadena original en el archivo cadena.txt--->
					<cffile action ="write" file= "#directory#portal\Contrarecibos\cadena.txt" output="#cadena_Original#" addNewLine="no" charset="utf-8">
					
					<!---Se ejecuta un comando en Openssl que guarda la llave pública del certificado--->
					<cfexecute name="#directory#portal\OPENSSL\bin\openssl.exe"
						arguments='x509 -in #directory#portal\Contrarecibos\certificado.PEM -pubkey -noout'
						variable="data"
						timeout="20"></cfexecute>
						
					<!---Se guarda el resultado del comando en el archico publicKey.txt--->
					<cffile action ="write" file= "#directory#portal\Contrarecibos\publicKey.txt" output="#data#" addNewLine="no" charset="utf-8">
		
					<!---Se ejecuta un comando en Openssl que convierte el sello a binario--->
		
					<cfset data2 = toBinary(#sello#)>
					<!---Se guarda el resultado del comando en el archico selloBins.txt--->
					<cffile action ="write" file= "#directory#portal\Contrarecibos\selloBin.txt" output="#data2#" addNewLine="no">
		
					<!---Se ejecuta un comando en Openssl que hace la comparación del certificado con el sello y la cadena original--->
					<cfif DateCompare(fechaXML, '2011-02-01') EQ 1>
						<cfexecute name="#directory#portal\OPENSSL\bin\openssl.exe"
							arguments='dgst -sha1 -verify #directory#portal\contrarecibos\publicKey.txt -signature #directory#portal\contrarecibos\sellobin.txt #directory#portal\contrarecibos\cadena.txt'
							variable="data3"
							timeout="20"></cfexecute>
					<cfelse>
						<cfexecute name="#directory#portal\OPENSSL\bin\openssl.exe"
							arguments='dgst -md5 -verify #directory#portal\contrarecibos\publicKey.txt -signature #directory#portal\contrarecibos\sellobin.txt #directory#redobra\portal\contrarecibos\cadena.txt'
							variable="data3"
							timeout="20"></cfexecute>
					</cfif>
					<cfset Result.sello = '#data3#'>
					<!--- <cfif data3 EQ ''>
						<cfset Result.SUCCESS = false>
						<cfset Result.TipoError = 'Error en la verificacion del sello.'>
						<cfset Result.MESSAGE = 'Sello inexistente.'>
					</cfif> --->
				<cfelse>
					<cfset Result.SUCCESS = false>
					<cfset Result.TipoError = 'Error en la verificacion del sello.'>
					<cfset Result.MESSAGE = 'Sello no comprobado.'>
				</cfif>
				<cfcatch type="any"><!---Entrar aqui significa que la estructura del XML es correcta pero hubo un error al verificar el sello --->
					<cfset Result.SUCCESS = false>
					<cfset Result.TipoError = 'Error en la verificacion del sello.'>
            		<cfset Result.MESSAGE = 'Ocurrio un error al comprobar el sello del documento.'>
				</cfcatch>
			</cftry>
			
			<cfcatch type='any' >
                <cfset Result.SUCCESS = false><!---Entrar aqui puede significar que hubo un error con la lectura del XML o con la validacion del servicio del sat--->
				<cfset Result.TipoError &= '<br>Error al validar el documento XML.'>
                <cfset Result.MESSAGE = 'Error al validar el docomento XML. #cfcatch.Detail#'>
            </cfcatch>
        </cftry>
        <cfreturn Result>
    </cffunction>
    
	<cffunction name="xslt" returntype="string" output="No">
		<cfargument name="xmlSource" type="string" required="yes">
		<cfargument name="xslSource" type="string" required="yes">
		<cfargument name="stParameters" type="struct" default="#StructNew()#" required="No">
		<cfscript>
			var source = ""; var transformer = ""; var aParamKeys = ""; var pKey = "";
			var xmlReader = ""; 
			var xslReader = "";
			var pLen = 0;
			var xmlWriter = ""; 
			var xmlResult = ""; 
			var pCounter = 0;
			var tFactory = createObject("java", "javax.xml.transform.TransformerFactory").newInstance();
			
			//if xml use the StringReader - otherwise, just assume it is a file source.
			if(Find("<", arguments.xslSource) neq 0)
			{
				xslReader = createObject("java", "java.io.StringReader").init(arguments.xslSource);
				source = createObject("java", "javax.xml.transform.stream.StreamSource").init(xslReader);
			}
			else
			{
				source = createObject("java", "javax.xml.transform.stream.StreamSource").init("file:///#arguments.xslSource#");
			}
			
			transformer = tFactory.newTransformer(source);
			
			//if xml use the StringReader - otherwise, just assume it is a file source.
			if(Find("<", arguments.xmlSource) neq 0)
			{
				xmlReader = createObject("java", "java.io.StringReader").init(arguments.xmlSource);
				source = createObject("java", "javax.xml.transform.stream.StreamSource").init(xmlReader);
			}
			else
			{
				source = createObject("java", "javax.xml.transform.stream.StreamSource").init("file:///#arguments.xmlSource#");
			}
			
			//use a StringWriter to allow us to grab the String out after.
			xmlWriter = createObject("java", "java.io.StringWriter").init();
			
			xmlResult = createObject("java", "javax.xml.transform.stream.StreamResult").init(xmlWriter);
			
			if(StructCount(arguments.stParameters) gt 0)
			{
				aParamKeys = structKeyArray(arguments.stParameters);
				pLen = ArrayLen(aParamKeys);
				for(pCounter = 1; pCounter LTE pLen; pCounter = pCounter + 1)
				{
					//set params
					pKey = aParamKeys[pCounter];
					transformer.setParameter(pKey, arguments.stParameters[pKey]);
				}
			}
			
			transformer.transform(source, xmlResult);
			return xmlWriter.toString();
		</cfscript>
	</cffunction>
<!---*******************************FUNCION PARA VALIDAR LAS FACTURAS INGRESADAS EN FACTURAS-ORDENESCOMPRA************--->
	<cffunction name="upload_XMLProveedores" access="public" returntype="struct">
		<cfargument name="fileField" 		type="string" required="true">
        <cftry>
			<cfset Result.SUCCESS = true>
			<cfset Result.TipoError = ''>
			<cfset Result.MESSAGE = 'XML comprobado satisfactoriamente.'>
			
			<cfset ReducirValidaciones = false>


			<cfset UploadDir = "#ExpandPath('../proveedores/Facturas_OrdenesCompra/')#">  
			<!--- Crear directorio si no existe--->
			<cfif not DirectoryExists(UploadDir)>
				<cfdirectory action="create" directory="#UploadDir#">
			</cfif>
			
			<!---Subir el archivo XML --->
			<cffile action="upload" fileField="#fileField#" destination="#UploadDir#"  nameConflict="makeunique" result="Upload">
			
			<!---Renombra XML sin caracteres especiales --->
			<cfset NuevoNombre = Upload.SERVERFILE>
			<cfset NuevoNombre = ReplaceList(NuevoNombre, 'á,é,í,ó,ú,ñ,##,&,?,à,è,ì,ò,ù,''', 'a,e,i,o,u,n,,,,a,e,i,o,u,')>
			<cfset NuevoNombre = ReplaceList(NuevoNombre, 'Á,É,Í,Ó,Ú,Ñ,##,&,?,À,È,Ì,Ò,Ù,''', 'A,E,I,O,U,N,,,,A,E,I,O,U,')>
    		<cffile action="rename" source="#UploadDir##Upload.SERVERFILE#" destination="#UploadDir##NuevoNombre#">
			<cfset Result.RutaXML = "#UploadDir##NuevoNombre#">
			
			<!---Leer el archivo XML --->
			<cfset XML = xmlParse(Result.RutaXML)>
			
			<!---
				********************************************************************************
				**********************Valida la estructura del Archivo XML**********************
				********************************************************************************
			--->
			<!---Datos Generales --->
			<cfset Result.TipoError 		&= IIF(NOT isDefined("XML.Comprobante.XmlAttributes.version"), 				DE("<br>El atributo 'version' no ha sido encontrado."), DE(''))>
			<cfset Result.TipoError 		&= IIF(NOT isDefined("XML.Comprobante.XmlAttributes.tipoDeComprobante"), 	DE("<br>El atributo 'tipoDeComprobante' no ha sido encontrado."), DE(''))>
			<cfset Result.TipoError 		&= IIF(NOT isDefined("XML.Comprobante.XmlAttributes.formaDePago"), 			DE("<br>El atributo 'formaDePago' no ha sido encontrado."), DE(''))>
			<cfset Result.TipoError 		&= IIF(NOT isDefined("XML.Comprobante.XmlAttributes.subTotal"), 			DE("<br>El atributo 'subTotal' no ha sido encontrado."), DE(''))>
			<cfset Result.TipoError 		&= IIF(NOT isDefined("XML.Comprobante.XmlAttributes.total"), 				DE("<br>El atributo 'total' no ha sido encontrado."), DE(''))>
			<cfset Result.TipoError 		&= IIF(NOT isDefined("XML.Comprobante.XmlAttributes.metodoDePago"), 		DE("<br>El atributo 'metodoDePago' no ha sido encontrado."), DE(''))>
			<cfset Result.TipoError 		&= IIF(NOT isDefined("XML.Comprobante.XmlAttributes.LugarExpedicion"), 		DE("<br>El atributo 'LugarExpedicion' no ha sido encontrado."), DE(''))>
			<cfset Result.TipoError 		&= IIF(NOT isDefined("XML.Comprobante.XmlAttributes.fecha"), 				DE("<br>El atributo 'fecha' no ha sido encontrado."), DE(''))>
			<cfset Result.TipoError 		&= IIF(NOT isDefined("XML.Comprobante.XmlAttributes.sello"), 				DE("<br>El atributo 'sello' no ha sido encontrado."), DE(''))>
			<cfset Result.TipoError 		&= IIF(NOT isDefined("XML.Comprobante.XmlAttributes.noCertificado"), 		DE("<br>El atributo 'noCertificado' no ha sido encontrado."), DE(''))>
			<cfset Result.TipoError 		&= IIF(NOT isDefined("XML.Comprobante.XmlAttributes.certificado"), 			DE("<br>El atributo 'certificado' no ha sido encontrado."), DE(''))>
			<cfset Result.TipoError 		&= IIF(NOT isDefined("XML.Comprobante.Complemento.TimbreFiscalDigital.XmlAttributes.UUID"), 			DE("<br>El atributo 'Folio Fiscal (UUID)' no ha sido encontrado."), DE(''))>
			
			<cfset Result.version 			= XML.Comprobante.XmlAttributes['version']>
			<cfset Result.tipoDeComprobante = XML.Comprobante.XmlAttributes['tipoDeComprobante']>
			<cfset Result.formaDePago 		= XML.Comprobante.XmlAttributes['formaDePago']>
			<cfset Result.subTotal 			= XML.Comprobante.XmlAttributes['subTotal']>
			<cfset Result.total 			= XML.Comprobante.XmlAttributes['total']>
			<cfset Result.metodoDePago 		= XML.Comprobante.XmlAttributes['metodoDePago']>
			<cfset Result.LugarExpedicion	= XML.Comprobante.XmlAttributes['LugarExpedicion']>
			<cfset Result.Fecha 			= XML.Comprobante.XmlAttributes['fecha']>
			<cfset Result.Sello_ 			= XML.Comprobante.XmlAttributes['sello']>
			<cfset Result.noCertificado 	= XML.Comprobante.XmlAttributes['noCertificado']>
			<cfset Result.certificado 		= XML.Comprobante.XmlAttributes['certificado']>
			
			<!---Si no se encuentra el atributo folio del comprobante, entences el folio sera el folio fiscal (UUID) --->
			<cfif isDefined('XML.Comprobante.XmlAttributes.folio')>
				<cfset Result.folio = XML.Comprobante.XmlAttributes['folio']>
			<cfelse>
				<cfset Result.folio = XML.Comprobante.Complemento.TimbreFiscalDigital.XmlAttributes['UUID']>
			</cfif>
			

			<!---Datos Emisor --->
			<cfset Result.TipoError 		&= IIF(NOT isDefined("XML.Comprobante.Emisor.XmlAttributes.rfc"), 							DE("<br>El atributo 'rfc' del nodo 'Emisor' no ha sido encontrado."), DE(''))>
			<cfset Result.Emisor.RFC 		= XML.Comprobante.Emisor.XmlAttributes['rfc']>
			
			<cfif isDefined("XML.Comprobante.Emisor.DomicilioFiscal") AND NOT ReducirValidaciones>
				<cfset Result.TipoError 		&= IIF(NOT isDefined("XML.Comprobante.Emisor.DomicilioFiscal.XmlAttributes.pais"),DE("<br>El atributo 'pais' del nodo 'DomicilioFiscal' no ha sido encontrado."), DE(''))>
				<cfset Result.TipoError 		&= IIF(NOT isDefined("XML.Comprobante.Emisor.DomicilioFiscal.XmlAttributes.municipio"),DE("<br>El atributo 'municipio' del nodo 'DomicilioFiscal' no ha sido encontrado."), DE(''))>
				<cfset Result.TipoError 		&= IIF(NOT isDefined("XML.Comprobante.Emisor.DomicilioFiscal.XmlAttributes.estado"),DE("<br>El atributo 'estado' del nodo 'DomicilioFiscal' no ha sido encontrado."), DE(''))>
				<cfset Result.TipoError 		&= IIF(NOT isDefined("XML.Comprobante.Emisor.DomicilioFiscal.XmlAttributes.codigoPostal"),DE("<br>El atributo 'codigoPostal' del nodo 'DomicilioFiscal' no ha sido encontrado."), DE(''))>

				<cfset Result.Emisor.pais		= XML.Comprobante.Emisor.DomicilioFiscal.XmlAttributes['pais']>
				<cfset Result.Emisor.municipio	= XML.Comprobante.Emisor.DomicilioFiscal.XmlAttributes['municipio']>
				<cfset Result.Emisor.estado		= XML.Comprobante.Emisor.DomicilioFiscal.XmlAttributes['estado']>
				<cfset Result.Emisor.codigoPostal= XML.Comprobante.Emisor.DomicilioFiscal.XmlAttributes['codigoPostal']>
			</cfif>
			<cfif isDefined('XML.Comprobante.Emisor.ExpedidoEn')>
				<cfset Result.TipoError 				&= IIF(NOT isDefined("XML.Comprobante.Emisor.ExpedidoEn.XmlAttributes.pais"),DE("<br>El atributo 'pais' del nodo 'ExpedidoEn' no ha sido encontrado."), DE(''))>
				<cfset Result.Emisor.ExpedidoEn.pais	= XML.Comprobante.Emisor.ExpedidoEn.XmlAttributes['pais']>
			</cfif>

			<cfif isdefined("XML.Comprobante.Emisor.RegimenFiscal.XmlAttributes.Regimen")>
				<cfset Result.TipoError 				&= IIF(NOT isDefined("XML.Comprobante.Emisor.RegimenFiscal.XmlAttributes.Regimen"),DE("<br>El atributo 'Regimen' del nodo 'RegimenFiscal' no ha sido encontrado."), DE(''))>
				<cfset Result.Emisor.RegimenFiscal.Regimen	= XML.Comprobante.Emisor.RegimenFiscal.XmlAttributes['Regimen']>
			</cfif>
			
			<!---Datos Receptor --->
			<cfset Result.TipoError 		&= IIF(NOT isDefined("XML.Comprobante.Receptor.XmlAttributes.rfc"), 				DE("<br>El atributo 'rfc' del nodo 'Receptor' no ha sido encontrado."), DE(''))>
			<cfset Result.Receptor.RFC 		= XML.Comprobante.Receptor.XmlAttributes['rfc']>
			
			<cfif isDefined("XML.Comprobante.Receptor.Domicilio")>
				<cfset Result.TipoError 		&= IIF(NOT isDefined("XML.Comprobante.Receptor.Domicilio.XmlAttributes.pais"), 		DE("<br>El atributo 'pais' del nodo 'Receptor' no ha sido encontrado."), DE(''))>
				<cfset Result.Receptor.pais 	= XML.Comprobante.Receptor.Domicilio.XmlAttributes['pais']>
			</cfif>
			

			<!---Conceptos --->
			<cfset Result.Conceptos = arrayNew(1)>
			<cfloop array="#XML.Comprobante.Conceptos.XmlChildren#" index="i" >
				<cfset Result.TipoError 		&= IIF(NOT isDefined("i.XmlAttributes.cantidad"), 		DE("<br>El atributo 'cantidad' del nodo 'Conceptos' no ha sido encontrado."), DE(''))>
				<cfset Result.TipoError 		&= IIF(NOT isDefined("i.XmlAttributes.unidad"), 		DE("<br>El atributo 'unidad' del nodo 'Conceptos' no ha sido encontrado."), DE(''))>
				<cfset Result.TipoError 		&= IIF(NOT isDefined("i.XmlAttributes.descripcion"), 	DE("<br>El atributo 'descripcion' del nodo 'Conceptos' no ha sido encontrado."), DE(''))>
				<cfset Result.TipoError 		&= IIF(NOT isDefined("i.XmlAttributes.valorUnitario"), 	DE("<br>El atributo 'valorUnitario' del nodo 'Conceptos' no ha sido encontrado."), DE(''))>
				<cfset Result.TipoError 		&= IIF(NOT isDefined("i.XmlAttributes.importe"), 		DE("<br>El atributo 'importe' del nodo 'Conceptos' no ha sido encontrado."), DE(''))>
				
				<cfset concepto = structNew()>
				<cfset concepto.cantidad		= i.XmlAttributes['cantidad']>
				<cfset concepto.unidad			= i.XmlAttributes['unidad']>
				<cfset concepto.descripcion		= i.XmlAttributes['descripcion']>
				<cfset concepto.valorUnitario	= i.XmlAttributes['valorUnitario']>
				<cfset concepto.importe			= i.XmlAttributes['importe']>
				<cfset ArrayAppend(Result.Conceptos,concepto)>
			</cfloop>
			
			<!---Impuestos Retenciones --->
			<cfset Result.RetencionISR = 0>
			<cfset Result.RetencionIVA = 0>
			<cfif IsDefined('XML.Comprobante.Impuestos.Retenciones')>
				<cfset Result.Retenciones = arrayNew(1)>
				<cfloop array="#XML.Comprobante.Impuestos.Retenciones.XmlChildren#" index="i">
					<cfset Result.TipoError 		&= IIF(NOT isDefined("i.XmlAttributes.impuesto"), 		DE("<br>El atributo 'impuesto' del nodo 'Retenciones' no ha sido encontrado."), DE(''))>
					<cfset Result.TipoError 		&= IIF(NOT isDefined("i.XmlAttributes.importe"), 		DE("<br>El atributo 'importe' del nodo 'Retenciones' no ha sido encontrado."), DE(''))>
				
					<cfset retencion = structNew()>
					<cfset retencion.impuesto	= i.XmlAttributes['impuesto']>
					<cfset retencion.importe	= i.XmlAttributes['importe']>

					<cfset ArrayAppend(Result.Retenciones,retencion)>
					
					<cfif retencion.impuesto EQ 'ISR'>
						<cfset Result.RetencionISR = retencion.importe>
					<cfelseif retencion.impuesto EQ 'IVA'>
						<cfset Result.RetencionIVA = retencion.importe>
					</cfif>
				</cfloop>
			</cfif>
			
			<!---Impuestos Traslados --->
			<cfset Result.TrasladoIva = 0>	
			<cfif IsDefined('XML.Comprobante.Impuestos.Traslados')>
				<cfset Result.Traslados = arrayNew(1)>
				<cfloop array="#XML.Comprobante.Impuestos.Traslados.XmlChildren#" index="i">
					<cfset Result.TipoError 		&= IIF(NOT isDefined("i.XmlAttributes.impuesto"), 		DE("<br>El atributo 'impuesto' del nodo 'Traslados' no ha sido encontrado."), DE(''))>
					<cfset Result.TipoError 		&= IIF(NOT isDefined("i.XmlAttributes.importe"), 		DE("<br>El atributo 'importe' del nodo 'Traslados' no ha sido encontrado."), DE(''))>
					<cfset Result.TipoError 		&= IIF(NOT isDefined("i.XmlAttributes.tasa"), 		DE("<br>El atributo 'tasa' del nodo 'Traslados' no ha sido encontrado."), DE(''))>

					<cfset Traslado = structNew()>
					<cfset Traslado.impuesto	= i.XmlAttributes['impuesto']>
					<cfset Traslado.tasa		= i.XmlAttributes['tasa']>
					<cfset Traslado.importe		= i.XmlAttributes['importe']>
					<cfset ArrayAppend(Result.Traslados,Traslado)>
					
					<cfif Traslado.impuesto EQ 'IVA'>
						<cfset Result.TrasladoIva = Traslado.importe>	
					</cfif>
				</cfloop>
			</cfif>
			
			<cfset Result.UUID = XML.Comprobante.Complemento.TimbreFiscalDigital.XmlAttributes['UUID']>
			<!---
				********************************************************************************
				******************************SERVICIO WEB DEL SAT******************************
				********************************************************************************
				Se hace el invoke del servicio web del SAT para validar el Folio UUID 
				en base al RFC Receptor, RFC Emisor, FOLIO(UUID), y total--->
			<cfinvoke  
				webservice="https://consultaqr.facturaelectronica.sat.gob.mx/ConsultaCFDIService.svc?singleWsdl" 
				method="Consulta"
				expresionImpresa="?re=#Result.Emisor.RFC#&rr=#Result.Receptor.RFC#&tt=#Result.total#&id=#Result.UUID#"
				returnvariable="servicio"> 
			<cfset Result.Estatus = servicio.getCodigoEstatus()>
			<cfset Result.Estado = servicio.getEstado()>
			<cfif  Left(Result.Estatus, 1) NEQ 'S'>
				<cfset Result.SUCCESS = false>
				<cfset Result.MESSAGE = Result.Estatus & 'No se pudo comprobar el estatus de la factura'>
				<cfset Result.TipoError = Result.Estatus>
			<cfelseif Result.Estado NEQ 'Vigente'>
				<cfset Result.SUCCESS = false>
				<cfset Result.MESSAGE = 'El estado de la factura no es vigente'>
				<cfset Result.TipoError = Result.Estado>
			</cfif>
			
			<!---
				********************************************************************************
				************************Validacion del Sello con OPENSSL************************
				********************************************************************************
			--->
			<cftry>
				<cfset directory = #Application.sitepath# >
				<cfif Right(directory, 12) EQ 'proveedores\'> 
					<cfset directory = Left(directory, Len(directory) - 12)> <!---Quitamos la parte de 'proveedores\'  --->
				</cfif>
				<cfif   StructKeyExists(XML.Comprobante.XmlAttributes, 'certificado') AND  XML.Comprobante.XmlAttributes.certificado NEQ ''>
					
					<cfif XML.Comprobante.XmlAttributes.version EQ '3.2'>
						<cffile action="read" file="#directory#proveedores\cadenaoriginal_3_2.xslt" variable="xmltrans">
					<cfelseif XML.Comprobante.XmlAttributes.version EQ '3' OR XML.Comprobante.XmlAttributes.sello EQ '3.0'>
						<cffile action="read" file="#directory#proveedores\cadenaoriginal_3_0.xslt'" variable="xmltrans">
					<cfelseif XML.Comprobante.XmlAttributes.version EQ '2.2'>
						<cffile action="read" file="#directory#proveedores\cadenaoriginal_2_2.xslt" variable="xmltrans">
					<cfelseif XML.Comprobante.XmlAttributes.version EQ '2' OR XML.Comprobante.XmlAttributes.sello EQ '2.0'>
						<cffile action="read" file="#directory#proveedores\cadenaoriginal_2_0.xslt" variable="xmltrans">
					</cfif>
	
					<!---Se guarda la cadena del certificado y el sello en una variable--->
					<cfset Pem = #XML.Comprobante.XmlAttributes.certificado#>
					<cfset Cert = #XML.Comprobante.XmlAttributes.sello#>
					<cfset fechaXML = #Left(XML.Comprobante.XmlAttributes.fecha, 10)#>
					<cfset countdown = 1>
					<!---Salto de línea--->
					<cfset br = "#chr(13)##chr(10)#">
					<!---Variable de Certificado--->
					<cfset certificado = "-----BEGIN CERTIFICATE-----"& #br#>
					<!---Ciclo para dividir la cadena del certificado en cadenas de 64 caracteres--->
					<cfloop
							index="intChar"
							from="1"
							to="#Len(Pem)#"
							step="1">
						<cfset strChar = #Mid(Pem, countdown, 64)# />
						<cfset countdown = countdown + 64>
						<!---Mientras siga habiendo líneas en el certificado.--->
						<cfif strChar NEQ ''>
							<cfset certificado = #certificado# & #strChar# &#br#>
						</cfif>
					</cfloop>
					<cfset certificado = #certificado# &"-----END CERTIFICATE-----">
					
					<!---Se guarda el certificado formateado en el archivo certificado.PEM--->
					<cffile action ="write" file= "#directory#proveedores\Facturas_OrdenesCompra\certificado.PEM" output= "#certificado#" addNewLine= "no" charset= "utf-8">
					<!---Variable de Sello--->
					<cfset sello = "">
					<cfset countdown = 1>
					<cfif   StructKeyExists(XML.Comprobante.XmlAttributes, 'sello') 
						AND  XML.Comprobante.XmlAttributes.sello NEQ ''>
						<cfloop index="intChar"
								from="1"
								to="#Len(Pem)#"
								step="1">
							<cfset strChar = #Mid(Cert, countdown, 64)# />
							<cfset countdown = countdown + 64>
							<!---Mientras siga habiendo líneas en el Sello.--->
							<cfif strChar NEQ ''>
							    <cfset Sello = #Sello# & #strChar# &#br#>
							</cfif>
						</cfloop>
					</cfif>
					<!---Se guarda el sello formateado en el archivo sello.txt--->
					<cffile action ="write" file= "#directory#proveedores\Facturas_OrdenesCompra\sello.txt" output="#sello#" addNewLine="no" charset="utf-8">
					
					<!---Se genera la cadena original utilizando la función de transformación de XMLs. xslt--->
					<cfset cadena_Original = xslt( Result.RutaXML, xmltrans)>
					<!---Se guarda la cadena original en el archivo cadena.txt--->
					<cffile action ="write" file= "#directory#proveedores\Facturas_OrdenesCompra\cadena.txt" output="#cadena_Original#" addNewLine="no" charset="utf-8">
					
					<!---Se ejecuta un comando en Openssl que guarda la llave pública del certificado--->
					<cfexecute name="#directory#proveedores\OPENSSL\bin\openssl.exe"
						arguments='x509 -in #directory#proveedores\Facturas_OrdenesCompra\certificado.PEM -pubkey -noout'
						variable="data"
						timeout="20"></cfexecute>
					<!---Se guarda el resultado del comando en el archico publicKey.txt--->
					<cffile action ="write" file= "#directory#proveedores\Facturas_OrdenesCompra\publicKey.txt" output="#data#" addNewLine="no" charset="utf-8">
		
					<!---Se ejecuta un comando en Openssl que convierte el sello a binario--->
		
					<cfset data2 = toBinary(#sello#)>
					<!---Se guarda el resultado del comando en el archico selloBins.txt--->
					<cffile action ="write" file= "#directory#proveedores\Facturas_OrdenesCompra\selloBin.txt" output="#data2#" addNewLine="no">
		
					<!---Se ejecuta un comando en Openssl que hace la comparación del certificado con el sello y la cadena original--->
					<cfif DateCompare(fechaXML, '2011-02-01') EQ 1>
						<cfexecute name="#directory#proveedores\OPENSSL\bin\openssl.exe"
							arguments='dgst -sha1 -verify #directory#proveedores\Facturas_OrdenesCompra\publicKey.txt -signature #directory#proveedores\Facturas_OrdenesCompra\sellobin.txt #directory#proveedores\Facturas_OrdenesCompra\cadena.txt'
							variable="data3"
							timeout="20"></cfexecute>
					<cfelse>
						<cfexecute name="#directory#proveedores\OPENSSL\bin\openssl.exe"
							arguments='dgst -md5 -verify #directory#proveedores\Facturas_OrdenesCompra\publicKey.txt -signature #directory#proveedores\Facturas_OrdenesCompra\sellobin.txt #directory#ferrominio\proveedores\Facturas_OrdenesCompra\cadena.txt'
							variable="data3"
							timeout="20"></cfexecute>
					</cfif>
					<cfset Result.sello = '#data3#'>
					<cfif data3 EQ ''>
						<cfset Result.SUCCESS = false>
						<cfset Result.TipoError = 'Error en la verificacion del sello.'>
						<cfset Result.MESSAGE = 'Sello inexistente.'>
					</cfif>
				<cfelse>
					<cfset Result.SUCCESS = false>
					<cfset Result.TipoError = 'Error en la verificacion del sello.'>
					<cfset Result.MESSAGE = 'Sello no comprobado.'>
				</cfif>
				<cfcatch type="any"><!---Entrar aqui significa que la estructura del XML es correcta pero hubo un error al verificar el sello --->
					<cfdump var="#Result#">
					<cfset Result.SUCCESS = false>
					<cfset Result.TipoError = 'Error en la verificacion del sello.'>
            		<cfset Result.MESSAGE = 'Ocurrio un error al comprobar el sello del documento.'>
				</cfcatch>
			</cftry>
			
			<cfcatch type='any' >
                <cfset Result.SUCCESS = false><!---Entrar aqui puede significar que hubo un error con la lectura del XML o con la validacion del servicio del sat--->
				<cfset Result.TipoError &= '<br>Error al validar el documento XML.'>
                <cfset Result.MESSAGE = 'Error al validar el docomento XML. #cfcatch.Detail#'>
            </cfcatch>
        </cftry>
        <cfreturn Result>
    </cffunction>

</cfcomponent>