<cfcomponent extends='conexion'>
	<!---Funcion para Agregar a lista de proveedores a 
		    los que se les reducen las validaciones XML-------------->
    <!---Desarrollador: Miguel Tiznado ------------------------------>
    <!---Fecha: 05/01/2015------------------------------------------->
	<cffunction name="Proveedores_ReducirValidaciones_XML_Agregar" access="public" returntype="struct">
        <cfargument name="id_Proveedor" 		type="string" required="true" >
        <cftry>
			<cfset Result.SQL = "
				IF NOT EXISTS(SELECT 1 FROM Proveedores_ReducirValidaciones_XML
						WHERE id_Proveedor = #id_Proveedor#)
					INSERT INTO Proveedores_ReducirValidaciones_XML (id_Proveedor)
					VALUES (#id_Proveedor#)
				">
            <cfquery name="RSDatos" datasource="#session.cnx#">                
                #PreserveSingleQuotes(Result.SQL)#
            </cfquery>
            <cfset Result.SUCCESS = true>
            <cfset Result.MESSAGE = 'La operaci&oacute;n se realiz&oacute; correctamente.'>
            <cfreturn result>
         	<cfcatch type='database'>
                <cfset Result.SUCCESS = false>
				<cfset Result.TipoError = "Error en la consulta de base de datos.">
                <cfset Result.MESSAGE = "Error al recuperar los datos => #cfcatch.Detail#">    
                <cfreturn Result>
            </cfcatch>
        </cftry>
    </cffunction>

	<!---Funcion para obtener lista de los proveedores a 
		    los que se les reducen las validaciones XML-------------->
    <!---Desarrollador: Miguel Tiznado ------------------------------>
    <!---Fecha: 05/01/2016------------------------------------------->
	<cffunction name="get_Proveedores_ReducirValidaciones_XML" returntype="struct" access="public">
		<cfargument name="id_Proveedor" type="string" required="false" default="">
		<cfset id_Proveedor = IIF(isNumeric(id_Proveedor), id_Proveedor, DE('NULL'))>
        <cftry>
			<cfset Result.SQL = "
				SELECT 
					p.* 
				FROM 
					Proveedores_ReducirValidaciones_XML prv
				INNER JOIN Proveedores p (NOLOCK)
					ON p.id_Proveedor = prv.id_Proveedor
				WHERE 
					p.sn_Activo = 1 AND
					p.id_Proveedor = COALESCE(#id_Proveedor#, p.id_Proveedor)
				">
            <cfquery name="RSDatos" datasource="#cnx#">                
                #PreserveSingleQuotes(Result.SQL)#
            </cfquery>
            <cfset Result.RS = RSDatos>
            <cfset Result.SUCCESS = true>
            <cfset Result.MESSAGE = 'La operaci&oacute;n se realiz&oacute; correctamente.'>
            <cfreturn result>
         	<cfcatch type='database'>
                <cfset Result.SUCCESS = false>
				<cfset Result.TipoError = "Error en la consulta de base de datos.">
                <cfset Result.MESSAGE = "Error al recuperar los datos => #cfcatch.Detail#">    
                <cfreturn Result>
            </cfcatch>
        </cftry>
	</cffunction>

	<!---Funcion para Eliminar un proveedor lista de proveedores a 
		    los que se les reducen las validaciones XML-------------->
    <!---Desarrollador: Miguel Tiznado ------------------------------>
    <!---Fecha: 05/01/2016------------------------------------------->
	<cffunction name="Proveedores_ReducirValidaciones_XML_Eliminar" access="remote" returntype="struct" returnformat="JSON" >
        <cfargument name="id_Proveedor"  type="string" required="true">
        <cftry>
			<cfset Result.SQL = "
				DELETE FROM Proveedores_ReducirValidaciones_XML
				WHERE id_Proveedor = #id_Proveedor#
				">
            <cfquery name="RSDatos" datasource="#session.cnx#">                
                #PreserveSingleQuotes(Result.SQL)#
            </cfquery>
            <cfset Result.SUCCESS = true>
            <cfset Result.MESSAGE = 'La operacion se realizo correctamente.'>
            <cfreturn result>
         	<cfcatch type='database'>
                <cfset Result.SUCCESS = false>
				<cfset Result.TipoError = "Error en la consulta de base de datos.">
                <cfset Result.MESSAGE = "Error al recuperar los datos => #cfcatch.Detail#">    
                <cfreturn Result>
            </cfcatch>
        </cftry>
    </cffunction>

    <!---Funcion para verificar	el folio de una factura-------------->
    <!---Desarrollador: Miguel Tiznado ------------------------------>
    <!---Fecha: 27/10/2015------------------------------------------->
	<cffunction name="Proveedores_FolioFacturaValida" access="remote" returntype="boolean" returnformat="JSON">
        <cfargument name="id_Empresa"	 		type="string" required="true" >
		<cfargument name="id_Obra"		 		type="string" required="true" >
		<cfargument name="id_Proveedor" 		type="string" required="true" >
		<cfargument name="de_Folio" 			type="string" required="true" >
        <cftry>
			<cfset Result.SQL = "
				SELECT 
					de_Factura
				FROM 
					Contrarecibosdetalle
				WHERE
					id_Empresa = #id_Empresa# AND
					id_Obra = #id_Obra# AND
					id_Proveedor = #id_Proveedor# AND
					de_Factura = '#de_Folio#' 
	                and id_EstatusFactura not in (512,523)
				">
            <cfquery name="RSDatos" datasource="#cnx#">                
                #PreserveSingleQuotes(Result.SQL)#
            </cfquery>
            <cfset VALIDO = IIF(RSDatos.RecordCount GT 0, false, true)>
            <cfreturn VALIDO>
         	<cfcatch type='database'> 
                <cfreturn false>
            </cfcatch>
        </cftry>
    </cffunction>
    
    <!---Funcion para Agregar Clabes Interbancarias------------------>
    <!---Desarrollador: Miguel Tiznado ------------------------------>
    <!---Fecha: 21/10/2015------------------------------------------->
<cffunction name="Proveedores_Clabes_CuentasBancarias_Agregar" access="public" returntype="struct">
        <cfargument name="id_Proveedor" 		type="string" required="true" >
		<cfargument name="de_Clabe" 			type="string" required="true" >
		<cfargument name="id_Banorte" 			type="string" required="true" >
		
        <cftry>
			<cfif de_Clabe NEQ ''>
				<cfset Result.SQL = "
				EXECUTE Proveedores_Clabes_CuentasBancarias_Agregar
						@id_Proveedor	= #id_Proveedor#,
						@de_Clabe 		= '#Replace(de_Clabe,' ', '', 'ALL')#'
				">
           		<cfquery name="RSDatos" datasource="#session.cnx#">                
                #PreserveSingleQuotes(Result.SQL)#
            </cfquery>

			</cfif>
			<cfquery name="RSDatos" datasource="#session.cnx#">                
                UPDATE Proveedores SET id_Banorte=#id_Banorte# WHERE id_Proveedor=#id_Proveedor#
            </cfquery>
            <cfset Result.SUCCESS = true>
            <cfset Result.MESSAGE = 'La operaci&oacute;n se realiz&oacute; correctamente.'>
            <cfreturn result>
         	<cfcatch type='database'>
                <cfset Result.SUCCESS = false>
				<cfset Result.TipoError = "Error en la consulta de base de datos.">
                <cfset Result.MESSAGE = "Error al recuperar los datos => #cfcatch.Detail#">    
                <cfreturn Result>
            </cfcatch>
        </cftry>
    </cffunction>
    <cffunction name="get_proveedores_ajax" access="remote" returntype="string" returnformat="JSON" >
        <cfargument name="nb_Proveedor" 		type="string" required="true" >
		
            <cfquery name="RSDatos" datasource="#session.cnx#">                
                 SELECT 
                	id_proveedor,nb_Proveedor
                FROM
                	Proveedores
                WHERE
                	nb_Proveedor like '%'+'#nb_Proveedor#'+'%'				
            </cfquery>
			<cfinvoke component="#Application.componentes#/funciones" method="queryToJson" returnVariable="RS" query="#RSDatos#">
			<cfreturn RS>
    </cffunction>
    
    <!---Funcion para Eliminar Clabes Interbancarias----------------->
    <!---Desarrollador: Miguel Tiznado ------------------------------>
    <!---Fecha: 21/10/2015------------------------------------------->
	<cffunction name="Proveedores_Clabes_CuentasBancarias_Eliminar" access="remote" returntype="struct" returnformat="JSON" >
        <cfargument name="id_Proveedor" 		type="string" required="true" >
		<cfargument name="de_Clabe" 			type="string" required="true" >
        <cftry>
			<cfset Result.SQL = "
				EXECUTE Proveedores_Clabes_CuentasBancarias_Eliminar
						@id_Proveedor	= #id_Proveedor#,
						@de_Clabe 		= '#de_Clabe#'
				">
            <cfquery name="RSDatos" datasource="#session.cnx#">                
                #PreserveSingleQuotes(Result.SQL)#
            </cfquery>
            <cfset Result.SUCCESS = true>
            <cfset Result.MESSAGE = 'La operacion se realizo correctamente.'>
            <cfreturn result>
         	<cfcatch type='database'>
                <cfset Result.SUCCESS = false>
				<cfset Result.TipoError = "Error en la consulta de base de datos.">
                <cfset Result.MESSAGE = "Error al recuperar los datos => #cfcatch.Detail#">    
                <cfreturn Result>
            </cfcatch>
        </cftry>
    </cffunction>



    <cffunction name="RSVerificarProveedor" access="remote" returntype="struct" returnformat="JSON" >
        <cfargument name="de_RFC" 		type="string" required="true" >
		<cfset bandera=true>	
		<cftry>
			
            <cfquery name="RSProveedor" datasource="#session.cnx#">                
                SELECT 
                	de_RFC 
                FROM 
                	Proveedores
                WHERE
                	de_RFC='#de_RFC#'            
             </cfquery>

            <cfif #RSProveedor.RecordCount# EQ 0>
             	<cfset bandera=false>	
			</cfif>

			<cfinvoke component="#Application.componentes#/funciones" method="queryToJson" returnVariable="JSON" query="#RSProveedor#">

            <cfset Result.SUCCESS = bandera>
            <cfset Result.MESSAGE = 'La operacion se realizo correctamente.'>
            <cfreturn result>
         	<cfcatch type='database'>
                <cfset Result.SUCCESS = false>
				<cfset Result.TipoError = "Error en la consulta de base de datos.">
                <cfset Result.MESSAGE = "Error al recuperar los datos => #cfcatch.Detail#">    
                <cfreturn Result>
            </cfcatch>
        </cftry>
    </cffunction>
    
    <!---Listado de las Clabes Interbancarias por proveedor---------->
    <!---Desarrollador: Miguel Tiznado ------------------------------>
    <!---Fecha: 21/10/2015------------------------------------------->
	<cffunction name="get_Proveedores_Clabes_CuentasBancarias" access="public" returntype="struct">
        <cfargument name="id_Proveedor" type="string" required="true"> 
        <cftry>
			<cfset Result.SQL = "
				EXECUTE Proveedores_Clabes_CuentasBancarias_Listado
						@id_Proveedor	= #IIF(id_Proveedor NEQ '',id_Proveedor, DE('NULL')) #
				">
            <cfquery name="RSDatos" datasource="#session.cnx#">                
                #PreserveSingleQuotes(Result.SQL)#
            </cfquery>
            <cfset Result.SUCCESS = true>
            <cfset Result.MESSAGE = 'Datos obtenidos con exito'>
            <cfset Result.RS = RSDatos>
            <cfreturn result>
         	<cfcatch type='database'>
                <cfset Result.SUCCESS = false>
				<cfset Result.TipoError = "Error en la consulta de base de datos.">
                <cfset Result.MESSAGE = "Error al recuperar los datos => #cfcatch.Detail#">    
                <cfreturn Result>
            </cfcatch>
        </cftry>
    </cffunction>

 <!---Autor: Rolando López
    Fecha: 14/11/2014--->
      
<!---FUNCION PARA AGREGAR LA RUTA DEL PDF A LA BASE DE DATOS--->
<cffunction name="RSfacturasAgregarPDF" access="public" returntype="struct">
	<cfargument name="id_Contrarecibo" type="string" required="yes">
	<cfargument name="id_Empresa" type="string" required="yes">
	<cfargument name="de_Factura" type="string" required="yes">
	<cfargument name="de_ruta" type="string" required="no" default="">

	<cftry>
		<cfquery datasource='#cnx#' name="RS">
			UPDATE ContrarecibosDetalle 
			SET de_rutaPDF = '#de_ruta#' 
			WHERE id_Contrarecibo = #id_Contrarecibo# AND
			id_Empresa = #id_Empresa# AND
		de_Factura = '#de_Factura#'
		</cfquery>

		<cfset PDF.agregado= TRUE>
		<cfset PDF.tipoError= ''>
		<cfset PDF.mensaje= 'PDF guardado correctamente'>
		<cfreturn PDF>
	<cfcatch type='database'>
		<cfset PDF.agregado= FALSE>
		<cfset PDF.tipoError= 'database-indefinido'>
		<cfset PDF.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
		<cfabort>
		<cfreturn PDF>
	</cfcatch>
	</cftry>
</cffunction>


<cfparam name="current_modulo" default="5">
<cffunction name='RSMostrarPorIDProveedores' access='public' returntype='struct'>

	<cfargument name='id_Proveedor' type='numeric' required='yes'> 
	<cftry>  

		<cfquery datasource='#cnx#' name='RS_MostrarProveedores' username='#user_sql#' password='#password_sql#'>
			EXECUTE bop_Proveedores_ObtenerPorID #id_Proveedor#
		</cfquery>
		<cfset Proveedores.listado= TRUE>
		<cfset Proveedores.tipoError= ''>
		<cfset Proveedores.rs= RS_MostrarProveedores>
		<cfset Proveedores.mensaje= 'Se obtuvo el recordset de Proveedores correctamente'>
		<cfreturn Proveedores>
		<cfcatch type='database'>
			<cfset Proveedores.listado= FALSE>
			<cfset Proveedores.tipoError= 'database-indefinido'>
			<cfset Proveedores.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn Proveedores>
		</cfcatch>
	</cftry>
</cffunction>


<cffunction name='RSMostrarDinamicoProveedores' access='public' returntype='struct'>

	<cfargument name='id_Proveedor' type='numeric' required='no' default='-1'>
	<cfargument name='nb_Proveedor' type='string' required='no' default=''>
	<cfargument name='de_Direccion' type='string' required='no' default=''>
	<cfargument name='nb_Colonia' type='string' required='no' default=''>
	<cfargument name='nb_ProveedorCorto' type='string' required='no' default=''>
	<cfargument name='nu_CodigoPostal' type='numeric' required='no' default='-1'>
	<cfargument name='id_Ciudad' type='numeric' required='no' default='-1'>
	<cfargument name='de_RFC' type='string' required='no' default=''>
	<cfargument name='nb_Responsable' type='string' required='no' default=''>
	<cfargument name='nu_Telefono1' type='string' required='no' default=''>
	<cfargument name='nu_Telefono2' type='string' required='no' default=''>
	<cfargument name='nu_Fax' type='string' required='no' default=''>
	<cfargument name='de_eMail' type='string' required='no' default=''>
	<cfargument name='nu_LimiteCredito' type='numeric' required='no' default='-1'>
	<cfargument name='id_Modulo' type='numeric' required='no' default='-1'>
	<cfargument name='id_TipoMovimiento' type='numeric' required='no' default='-1'>
	<cfargument name='pj_Descuento' type='numeric' required='no' default='-1'>
	<cfargument name='nu_CondicionesPago' type='numeric' required='no' default='-1'>
	<cfargument name='id_Moneda' type='numeric' required='no' default='-1'>
	<cfargument name='sn_Contratista' type='boolean' required='no' default='-1'>
	<cfargument name='cl_TipoPersona' type='string' required='no' default=''>
	<cfargument name='pj_Iva' type='numeric' required='no' default='-1'>
	<cfargument name='cl_TipoCuentaBancaria' type='string' required='no' default=''>
	<cfargument name='nu_CuentaBancaria' type='string' required='no' default=''>
	<cfargument name='nu_CuentaClabe' type='string' required='no' default=''>
	<cfargument name='de_Giro' type='string' required='no' default=''>
	<cfargument name='de_ActaConstitutiva' type='string' required='no' default=''>
	<cfargument name='de_RegistroIMSS' type='string' required='no' default=''>
	<cfargument name='nu_DigitoIMSS' type='numeric' required='no' default='-1'>
	<cfargument name='de_RegistroPublico' type='string' required='no' default=''>
	<cfargument name='de_NumeroPoder' type='string' required='no' default=''>
    <cfargument name='sn_Activo' type='string' required='no' default=''>
    <cfargument name='ma_obra' type="string" required="no" default=''>

	<cftry>
		<cfset WhereDinamico=''>
		<cfif #id_Proveedor# NEQ '-1'>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE id_Proveedor = #id_Proveedor#">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND id_Proveedor = #id_Proveedor#">
			</cfif>
		</cfif>
		<cfif #nb_Proveedor# NEQ ''>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE nb_Proveedor LIKE '%#nb_Proveedor#%'">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND nb_Proveedor LIKE '%#nb_Proveedor#%'">
			</cfif>
		</cfif>
		<cfif #de_Direccion# NEQ ''>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE de_Direccion LIKE '#de_Direccion#%'">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND de_Direccion LIKE '#de_Direccion#%'">
			</cfif>
		</cfif>
		<cfif #nb_Colonia# NEQ ''>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE nb_Colonia LIKE '#nb_Colonia#%'">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND nb_Colonia LIKE '#nb_Colonia#%'">
			</cfif>
		</cfif>
		<cfif #nb_ProveedorCorto# NEQ ''>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE nb_ProveedorCorto LIKE '%#nb_ProveedorCorto#%'">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND nb_ProveedorCorto LIKE '%#nb_ProveedorCorto#%'">
			</cfif>
		</cfif>
		<cfif #nu_CodigoPostal# NEQ '-1'>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE nu_CodigoPostal = #nu_CodigoPostal#">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND nu_CodigoPostal = #nu_CodigoPostal#">
			</cfif>
		</cfif>
		<cfif #id_Ciudad# NEQ '-1'>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE id_Ciudad = #id_Ciudad#">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND id_Ciudad = #id_Ciudad#">
			</cfif>
		</cfif>
		<cfif #de_RFC# NEQ ''>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE de_RFC LIKE '%#de_RFC#%'">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND de_RFC LIKE '%#de_RFC#%'">
			</cfif>
		</cfif>
		<cfif #nb_Responsable# NEQ ''>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE nb_Responsable LIKE '#nb_Responsable#%'">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND nb_Responsable LIKE '#nb_Responsable#%'">
			</cfif>
		</cfif>
		<cfif #nu_Telefono1# NEQ ''>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE nu_Telefono1 LIKE '#nu_Telefono1#%'">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND nu_Telefono1 LIKE '#nu_Telefono1#%'">
			</cfif>
		</cfif>
		<cfif #nu_Telefono2# NEQ ''>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE nu_Telefono2 LIKE '#nu_Telefono2#%'">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND nu_Telefono2 LIKE '#nu_Telefono2#%'">
			</cfif>
		</cfif>
		<cfif #nu_Fax# NEQ ''>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE nu_Fax LIKE '#nu_Fax#%'">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND nu_Fax LIKE '#nu_Fax#%'">
			</cfif>
		</cfif>
		<cfif #de_eMail# NEQ ''>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE de_eMail LIKE '#de_eMail#%'">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND de_eMail LIKE '#de_eMail#%'">
			</cfif>
		</cfif>
		<cfif #nu_LimiteCredito# NEQ '-1'>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE nu_LimiteCredito = #nu_LimiteCredito#">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND nu_LimiteCredito = #nu_LimiteCredito#">
			</cfif>
		</cfif>
		<cfif #id_Modulo# NEQ '-1'>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE id_Modulo = #id_Modulo#">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND id_Modulo = #id_Modulo#">
			</cfif>
		</cfif>
		<cfif #id_TipoMovimiento# NEQ '-1'>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE id_TipoMovimiento = #id_TipoMovimiento#">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND id_TipoMovimiento = #id_TipoMovimiento#">
			</cfif>
		</cfif>
		<cfif #pj_Descuento# NEQ '-1'>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE pj_Descuento = #pj_Descuento#">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND pj_Descuento = #pj_Descuento#">
			</cfif>
		</cfif>
		<cfif #nu_CondicionesPago# NEQ '-1'>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE nu_CondicionesPago = #nu_CondicionesPago#">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND nu_CondicionesPago = #nu_CondicionesPago#">
			</cfif>
		</cfif>
		<cfif #id_Moneda# NEQ '-1'>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE id_Moneda = #id_Moneda#">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND id_Moneda = #id_Moneda#">
			</cfif>
		</cfif>
		<cfif #sn_Contratista# NEQ '-1'>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE sn_Contratista = #sn_Contratista#">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND sn_Contratista = #sn_Contratista#">
			</cfif>
		</cfif>
		<cfif #cl_TipoPersona# NEQ ''>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE cl_TipoPersona LIKE '#cl_TipoPersona#%'">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND cl_TipoPersona LIKE '#cl_TipoPersona#%'">
			</cfif>
		</cfif>
		<cfif #pj_Iva# NEQ '-1'>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE pj_Iva = #pj_Iva#">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND pj_Iva = #pj_Iva#">
			</cfif>
		</cfif>
		<cfif #cl_TipoCuentaBancaria# NEQ ''>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE cl_TipoCuentaBancaria LIKE '#cl_TipoCuentaBancaria#%'">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND cl_TipoCuentaBancaria LIKE '#cl_TipoCuentaBancaria#%'">
			</cfif>
		</cfif>
		<cfif #nu_CuentaBancaria# NEQ ''>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE nu_CuentaBancaria LIKE '#nu_CuentaBancaria#%'">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND nu_CuentaBancaria LIKE '#nu_CuentaBancaria#%'">
			</cfif>
		</cfif>
		<cfif #nu_CuentaClabe# NEQ ''>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE nu_CuentaClabe LIKE '#nu_CuentaClabe#%'">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND nu_CuentaClabe LIKE '#nu_CuentaClabe#%'">
			</cfif>
		</cfif>
		<cfif #de_Giro# NEQ ''>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE de_Giro LIKE '#de_Giro#%'">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND de_Giro LIKE '#de_Giro#%'">
			</cfif>
		</cfif>
		<cfif #de_ActaConstitutiva# NEQ ''>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE de_ActaConstitutiva LIKE '#de_ActaConstitutiva#%'">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND de_ActaConstitutiva LIKE '#de_ActaConstitutiva#%'">
			</cfif>
		</cfif>
		<cfif #de_RegistroIMSS# NEQ ''>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE de_RegistroIMSS LIKE '#de_RegistroIMSS#%'">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND de_RegistroIMSS LIKE '#de_RegistroIMSS#%'">
			</cfif>
		</cfif>
		<cfif #nu_DigitoIMSS# NEQ '-1'>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE nu_DigitoIMSS = #nu_DigitoIMSS#">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND nu_DigitoIMSS = #nu_DigitoIMSS#">
			</cfif>
		</cfif>
		<cfif #de_RegistroPublico# NEQ ''>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE de_RegistroPublico LIKE '#de_RegistroPublico#%'">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND de_RegistroPublico LIKE '#de_RegistroPublico#%'">
			</cfif>
		</cfif>
		<cfif #de_NumeroPoder# NEQ ''>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE de_NumeroPoder LIKE '#de_NumeroPoder#%'">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND de_NumeroPoder LIKE '#de_NumeroPoder#%'">
			</cfif>
		</cfif>
		<cfif #sn_Activo# NEQ ''>
        	<cfif WhereDinamico EQ ''>
                    <cfset WhereDinamico= "WHERE sn_Activo = #sn_Activo# ">
			<cfelse>
               		<cfset WhereDinamico= WhereDinamico & " AND sn_Activo = #sn_Activo#">
            </cfif>
		</cfif>        
        <cfif #ma_obra# NEQ ''>
        		<cfif WhereDinamico EQ ''>
	               <cfset WhereDinamico= WhereDinamico & " WHERE sn_contratista = 1 ">
                 <cfelse>
                 	<cfset WhereDinamico= WhereDinamico & " AND sn_contratista = 1 ">
                 </cfif>
		</cfif>   
        <cfif WhereDinamico EQ ''>
                    <cfset WhereDinamico= "WHERE id_Proveedor <> 0 ">
			<cfelse>
               		<cfset WhereDinamico= WhereDinamico & " AND id_Proveedor <> 0">
            </cfif>

		<cfquery datasource='#cnx#' name='RS_MostrarProveedores' username='#user_sql#' password='#password_sql#'>
			EXECUTE bop_Proveedores_ObtenerDinamico '#WhereDinamico#'
		</cfquery>
		<cfset Proveedores.listado= TRUE>
		<cfset Proveedores.tipoError= ''>
		<cfset Proveedores.rs= RS_MostrarProveedores>
		<cfset Proveedores.mensaje= 'Se obtuvo el recordset de Proveedores correctamente'>
		<cfreturn Proveedores>
		<cfcatch type='database'>
			<cfset Proveedores.listado= FALSE>
			<cfset Proveedores.tipoError= 'database-indefinido'>
			<cfset Proveedores.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn Proveedores>
		</cfcatch>
	</cftry>
</cffunction>


<cffunction name='RSMostrarTodosProveedores' access='public' returntype='struct'>

	<cftry>
		<cfquery datasource='#cnx#' name='RS_MostrarProveedores' username='#user_sql#' password='#password_sql#'>
			EXECUTE bop_Proveedores_ObtenerTodos 
		</cfquery>
		<cfset Proveedores.listado= TRUE>
		<cfset Proveedores.tipoError= ''>
		<cfset Proveedores.rs= RS_MostrarProveedores>
		<cfset Proveedores.mensaje= 'Se obtuvo el recordset de Proveedores correctamente'>
		<cfreturn Proveedores>
		<cfcatch type='database'>
			<cfset Proveedores.listado= FALSE>
			<cfset Proveedores.tipoError= 'database-indefinido'>
			<cfset Proveedores.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn Proveedores>
		</cfcatch>
	</cftry>
</cffunction>

<cffunction name='RSMostrarTodosProveedoresActivos' access='public' returntype='struct'>

	<cftry>
		<cfquery datasource='#cnx#' name='RS_MostrarProveedores' username='#user_sql#' password='#password_sql#'>
			select * from Proveedores where sn_Activo=1
		</cfquery>
		<cfset Proveedores.listado= TRUE>
		<cfset Proveedores.tipoError= ''>
		<cfset Proveedores.rs= RS_MostrarProveedores>
		<cfset Proveedores.mensaje= 'Se obtuvo el recordset de Proveedores correctamente'>
		<cfreturn Proveedores>
		<cfcatch type='database'>
			<cfset Proveedores.listado= FALSE>
			<cfset Proveedores.tipoError= 'database-indefinido'>
			<cfset Proveedores.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn Proveedores>
		</cfcatch>
	</cftry>
</cffunction>


<cffunction name='RSAgregarProveedores' access='public' returntype='struct'>
	<cfargument name='id_Proveedor' type='numeric' required='yes'>
	<cfargument name='nb_Proveedor' type='string' required='yes'>
	<cfargument name='de_Direccion' type='string' required='no' default=''>
	<cfargument name='nb_Colonia' type='string' required='no' default=''>
	<cfargument name='nb_ProveedorCorto' type='string' required='no' default=''>
	<cfargument name='nu_CodigoPostal' type='numeric' required='no' default='0'>
	<cfargument name='id_Ciudad' type='numeric' required='yes'>
	<cfargument name='de_RFC' type='string' required='no' default=''>
	<cfargument name='nb_Responsable' type='string' required='no' default=''>
	<cfargument name='nu_Telefono1' type='string' required='no' default=''>
	<cfargument name='nu_Telefono2' type='string' required='no' default=''>
	<cfargument name='nu_Fax' type='string' required='no' default=''>
	<cfargument name='de_eMail' type='string' required='no' default=''>
	<cfargument name='nu_LimiteCredito' type='numeric' required='yes' default='0'>
	<cfargument name='id_Modulo' type='numeric' required='yes'>
	<cfargument name='id_TipoMovimiento' type='numeric' required='yes'>
	<cfargument name='pj_Descuento' type='numeric' required='yes' default='0'>
	<cfargument name='nu_CondicionesPago' type='numeric' required='yes' default='0'>
	<cfargument name='id_Moneda' type='numeric' required='yes'>
	<cfargument name='sn_Contratista' type='boolean' required='yes'>
	<cfargument name='cl_TipoPersona' type='string' required='no' default=''>
	<cfargument name='pj_Iva' type='numeric' required='no' default='0'>
	<cfargument name='cl_TipoCuentaBancaria' type='string' required='no' default=''>
	<cfargument name='nu_CuentaBancaria' type='string' required='no' default=''>
	<cfargument name='nu_CuentaClabe' type='string' required='no' default=''>
	<cfargument name='de_Giro' type='string' required='no' default=''>
	<cfargument name='de_ActaConstitutiva' type='string' required='no' default=''>
	<cfargument name='de_RegistroIMSS' type='string' required='no' default=''>
	<cfargument name='nu_DigitoIMSS' type='numeric' required='no' default='0'>
	<cfargument name='de_RegistroPublico' type='string' required='no' default=''>
	<cfargument name='de_NumeroPoder' type='string' required='no' default=''>

    <cfset de_RFC = REPLACE(#de_RFC#,'-','','ALL') >
	<cftry>
		<cfquery datasource='#cnx#' name='RSAgregar' username='#user_sql#' password='#password_sql#'>
			EXECUTE bop_Proveedores_Agregar #id_Proveedor#, '#nb_Proveedor#', '#de_Direccion#', '#nb_Colonia#', '#nb_ProveedorCorto#', #nu_CodigoPostal#, #id_Ciudad#, '#de_RFC#', '#nb_Responsable#', '#nu_Telefono1#', '#nu_Telefono2#', '#nu_Fax#', '#de_eMail#', #nu_LimiteCredito#, #id_Modulo#, #id_TipoMovimiento#, #pj_Descuento#, #nu_CondicionesPago#, #id_Moneda#, #sn_Contratista#, '#cl_TipoPersona#', #pj_Iva#, '#cl_TipoCuentaBancaria#', '#nu_CuentaBancaria#', '#nu_CuentaClabe#', '#de_Giro#', '#de_ActaConstitutiva#', '#de_RegistroIMSS#', #nu_DigitoIMSS#, '#de_RegistroPublico#', '#de_NumeroPoder#'
		</cfquery>
		<!---Log--->
		<cf_log datasource='#cnx#' username='#user_sql#' password='#password_sql#' id_Empresa="#Session.id_Empresa#" id_Obra="0" id_Usuario="#Session.id_Usuario#" id_MenuOpcion="#Session.id_MenuOpcion#">
			<cfoutput>
				EXECUTE bop_Proveedores_Agregar #id_Proveedor#, '#nb_Proveedor#', '#de_Direccion#', '#nb_Colonia#', '#nb_ProveedorCorto#', #nu_CodigoPostal#, #id_Ciudad#, '#de_RFC#', '#nb_Responsable#', '#nu_Telefono1#', '#nu_Telefono2#', '#nu_Fax#', '#de_eMail#', #nu_LimiteCredito#, #id_Modulo#, #id_TipoMovimiento#, #pj_Descuento#, #nu_CondicionesPago#, #id_Moneda#, #sn_Contratista#, '#cl_TipoPersona#', #pj_Iva#, '#cl_TipoCuentaBancaria#', '#nu_CuentaBancaria#', '#nu_CuentaClabe#', '#de_Giro#', '#de_ActaConstitutiva#', '#de_RegistroIMSS#', #nu_DigitoIMSS#, '#de_RegistroPublico#', '#de_NumeroPoder#'
			</cfoutput>
		</cf_log>
		<!---Termina Log --->
		<cfset Proveedores.agregado= TRUE>
		<cfset Proveedores.tipoError= ''>
		<cfset Proveedores.mensaje= 'Proveedor guardado correctamente'>
		<cfreturn Proveedores>
		<cfcatch type='database'>
			<cfset Proveedores.agregado= FALSE>
			<cfif cfcatch.NativeErrorCode EQ '2627'>
				<cfset Proveedores.tipoError= 'database-registro_duplicado'>
				<cfset Proveedores.mensaje= 'El Proveedor no se pudo guardar porque ya existe'>
			<cfelse>
				<cfset Proveedores.tipoError= 'database-indefinido'>
				<cfset Proveedores.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			</cfif>
			<cfreturn Proveedores>
		</cfcatch>
	</cftry>
</cffunction>


<cffunction name='RSEditarProveedores' access='public' returntype='struct'>

	<cfargument name='id_Proveedor' type='numeric' required='yes'>
	<cfargument name='nb_Proveedor' type='string' required='yes'>
	<cfargument name='de_Direccion' type='string' required='no' default=''>
	<cfargument name='nb_ProveedorCorto' type='string' required='no' default=''>
	<cfargument name='nu_CodigoPostal' type='numeric' required='no' default='0'>
	<cfargument name='id_Ciudad' type='numeric' required='yes'>
	<cfargument name='de_RFC' type='string' required='no' default=''>
	<cfargument name='nb_Responsable' type='string' required='no' default=''>
	<cfargument name='nu_Telefono1' type='string' required='no' default=''>
	<cfargument name='nu_Telefono2' type='string' required='no' default=''>
	<cfargument name='nu_Fax' type='string' required='no' default=''>
	<cfargument name='de_eMail' type='string' required='no' default=''>
	<cfargument name='nu_LimiteCredito' type='numeric' required='no' default='0'>
	<cfargument name='id_Modulo' type='numeric' required='yes'>
	<cfargument name='id_TipoMovimiento' type='numeric' required='yes'>
	<cfargument name='pj_Descuento' type='numeric' required='no' default='0'>
	<cfargument name='nu_CondicionesPago' type='numeric' required='no' default='0'>
	<cfargument name='id_Moneda' type='numeric' required='yes'>
	<cfargument name='sn_Contratista' type='boolean' required='yes'>
	<cfargument name='cl_TipoPersona' type='string' required='no' default=''>
	<cfargument name='pj_Iva' type='numeric' required='no' default='0'>
	<cfargument name='cl_TipoCuentaBancaria' type='string' required='no' default=''>
	<cfargument name='nu_CuentaBancaria' type='string' required='no' default=''>
	<cfargument name='nu_CuentaClabe' type='string' required='no' default=''>
	<cfargument name='de_Giro' type='string' required='no' default=''>
	<cfargument name='de_ActaConstitutiva' type='string' required='no' default=''>
	<cfargument name='de_RegistroIMSS' type='string' required='no' default=''>
	<cfargument name='nu_DigitoIMSS' type='numeric' required='no' default='0'>
	<cfargument name='de_RegistroPublico' type='string' required='no' default=''>
	<cfargument name='de_NumeroPoder' type='string' required='no' default=''>

	<cftry>
		<cfquery datasource='#cnx#' name='RSFilasAfectadas' username='#user_sql#' password='#password_sql#'>
			EXECUTE bop_Proveedores_Actualizar #id_Proveedor#, '#nb_Proveedor#', '#de_Direccion#', '#nb_ProveedorCorto#', #nu_CodigoPostal#, #id_Ciudad#, '#de_RFC#', '#nb_Responsable#', '#nu_Telefono1#', '#nu_Telefono2#', '#nu_Fax#', '#de_eMail#', #nu_LimiteCredito#, #id_Modulo#, #id_TipoMovimiento#, #pj_Descuento#, #nu_CondicionesPago#, #id_Moneda#, #sn_Contratista#, '#cl_TipoPersona#', #pj_Iva#, '#cl_TipoCuentaBancaria#', '#nu_CuentaBancaria#', '#nu_CuentaClabe#', '#de_Giro#', '#de_ActaConstitutiva#', '#de_RegistroIMSS#', #nu_DigitoIMSS#, '#de_RegistroPublico#', '#de_NumeroPoder#'
		</cfquery>
		<!---Log--->
		<cf_log datasource='#cnx#' username='#user_sql#' password='#password_sql#' id_Empresa="#Session.id_Empresa#" id_Obra="0" id_Usuario="#Session.id_Usuario#" id_MenuOpcion="#Session.id_MenuOpcion#">
			<cfoutput>
				EXECUTE bop_Proveedores_Actualizar #id_Proveedor#, '#nb_Proveedor#', '#de_Direccion#', '#nb_ProveedorCorto#', #nu_CodigoPostal#, #id_Ciudad#, '#de_RFC#', '#nb_Responsable#', '#nu_Telefono1#', '#nu_Telefono2#', '#nu_Fax#', '#de_eMail#', #nu_LimiteCredito#, #id_Modulo#, #id_TipoMovimiento#, #pj_Descuento#, #nu_CondicionesPago#, #id_Moneda#, #sn_Contratista#, '#cl_TipoPersona#', #pj_Iva#, '#cl_TipoCuentaBancaria#', '#nu_CuentaBancaria#', '#nu_CuentaClabe#', '#de_Giro#', '#de_ActaConstitutiva#', '#de_RegistroIMSS#', #nu_DigitoIMSS#, '#de_RegistroPublico#', '#de_NumeroPoder#'
			</cfoutput>
		</cf_log>
		<!---Termina Log --->
		<cfset Proveedores.actualizado= TRUE>
		<cfset Proveedores.filasAfectadas= RSFilasAfectadas.filasAfectadas>
		<cfset Proveedores.tipoError= ''>
		<cfset Proveedores.mensaje= 'Proveedor actualizado correctamente'>
		<cfreturn Proveedores>
		<cfcatch type='database'>
			<cfset Proveedores.actualizado= FALSE>
			<cfset Proveedores.tipoError= 'database-indefinido'>
			<cfset Proveedores.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn Proveedores>
		</cfcatch>
	</cftry>
</cffunction>


<cffunction name='RSEliminarProveedores' access='public' returntype='struct'>

	<cfargument name='id_Proveedor' type='numeric' required='yes'>
	<cfset fh_Baja = DateFormat(now(),'yyyy-mm-dd')>

	<cfif #id_Proveedor# EQ 0>
    
		<cfset Proveedores.eliminado= FALSE>
		<cfset Proveedores.mensaje= 'El Proveedor no se puede eliminar'>
		<cfset Proveedores.tipoError= 'database-integridad'>
        <cfreturn Proveedores>
    
    <cfelse>

        <cftry>
        
            <cfquery datasource='#cnx#' name='RSFilasAfectadas' username='#user_sql#' password='#password_sql#'>
                EXECUTE bop_Proveedores_Eliminar #id_Proveedor#, '#fh_Baja#'
            </cfquery>
    
            <cfset Proveedores.eliminado= TRUE>
            <cfset Proveedores.filasAfectadas= RSFilasAfectadas.filasAfectadas>
            <cfset Proveedores.tipoError= ''>
            <cfset Proveedores.mensaje= 'Proveedor eliminado correctamente'>
            <cfreturn Proveedores>
            <cfcatch type='database'>
                <cfset Proveedores.eliminado= FALSE>
                <cfif cfcatch.NativeErrorCode EQ '547'>
                    <cfset Proveedores.mensaje= 'El Proveedor no se pudo eliminar ya que tiene registros relacionados'>
                    <cfset Proveedores.tipoError= 'database-integridad'>
                <cfelse>
                    <cfset Proveedores.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
                    <cfset Proveedores.tipoError= 'database-indefinido'>
                </cfif>
                <cfreturn Proveedores>
            </cfcatch>
        </cftry>
    
    </cfif>
</cffunction>

<cffunction name="ProveedoresActivar" access="public" returntype="struct">

	<cfargument name='id_Proveedor' type='numeric' required='yes'>
	<cfset fh_Alta = DateFormat(now(),'yyyy-mm-dd')>
	<cftry>
		<cfquery datasource='#cnx#' name='RSFilasAfectadas' username='#user_sql#' password='#password_sql#'>
			UPDATE proveedores set sn_Activo=1, fh_Alta='#fh_Alta#'
            WHERE id_Proveedor=#id_Proveedor#
            SELECT @@ROWCOUNT AS filasAfectadas
		</cfquery>
        
		<cfset Proveedores.eliminar= TRUE>
		<cfset Proveedores.filasAfectadas= RSFilasAfectadas.filasAfectadas>
		<cfset Proveedores.tipoError= ''>
		<cfset Proveedores.mensaje= 'Proveedor eliminado correctamente'>
		<cfreturn Proveedores>
		<cfcatch type='database'>
			<cfset Proveedores.eliminar= FALSE>
			<cfif cfcatch.NativeErrorCode EQ '547'>
				<cfset Proveedores.mensaje= 'El Proveedor no se pudo eliminar ya que tiene registros relacionados'>
				<cfset Proveedores.tipoError= 'database-integridad'>
			<cfelse>
				<cfset Proveedores.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
				<cfset Proveedores.tipoError= 'database-indefinido'>
			</cfif>
			<cfreturn Proveedores>
		</cfcatch>
	</cftry>
</cffunction>


<!---Funcion que agrega un proveedor con ciertas reglas de negocio --->
<!--- MODIFICADO: 2013-01-16 --->
<!--- DESARROLLADOR: Alfonso Carranza --->
<cffunction name='RSAgregarProveedor' access='public' returntype='struct'>
	<cfargument name='nb_Proveedor' type='string' required='yes'>
	<cfargument name='de_Direccion' type='string' required='no' default=''>
	<cfargument name='nb_Colonia' type='string' required='no' default=''>
	<cfargument name='nb_ProveedorCorto' type='string' required='no' default=''>
	<cfargument name='nu_CodigoPostal' type='numeric' required='no' default='0'>
	<cfargument name='id_Ciudad' type='numeric' required='yes'>
	<cfargument name='de_RFC' type='string' required='no' default=''>
	<cfargument name='Clabe' type='string' required='no' default=''>
	<cfargument name='nb_Responsable' type='string' required='no' default=''>
	<cfargument name='nu_Telefono1' type='string' required='no' default=''>
	<cfargument name='nu_Telefono2' type='string' required='no' default=''>
    
    <!---<cfargument name='nu_Capacidad' type='numeric' required='no' default='0'>--->
	<cfargument name='nu_Fax' type='string' required='no' default=''>
	<cfargument name='de_eMail' type='string' required='no' default=''>
	<cfargument name='nu_LimiteCredito' type='numeric' required='no' default='0'>
	<cfargument name='id_Modulo' type='numeric' required='yes'>
	<cfargument name='id_TipoMovimiento' type='numeric' required='yes'>
	<cfargument name='pj_Descuento' type='numeric' required='no' default='0'>
	<cfargument name='nu_CondicionesPago' type='numeric' required='no' default='0'>
	<cfargument name='id_Moneda' type='numeric' required='yes'>
	<cfargument name='sn_Contratista' type='boolean' required='yes'>
	<cfargument name='cl_TipoPersona' type='string' required='no' default='F'>
	<cfargument name='pj_Iva' type="numeric" required="no" default="0">
	<cfargument name='cl_TipoCuentaBancaria' type='string' required='no' default=''>
	<cfargument name='nu_CuentaBancaria' type='string' required='no' default=''>
	<cfargument name='nu_CuentaClabe' type='string' required='YES' default=''>
	
	<cfargument name='de_Giro' type='string' required='no' default=''>
	<cfargument name='de_ActaConstitutiva' type='string' required='no' default=''>
	<cfargument name='de_RegistroIMSS' type='string' required='no' default=''>
	<cfargument name='nu_DigitoIMSS' type='numeric' required='no' default='0'>
	<cfargument name='de_RegistroPublico' type='string' required='no' default=''>
	<cfargument name='de_NumeroPoder' type='string' required='no' default=''>
    
    <cfargument name="de_Curp" type="string" required="no" default="">
    <cfargument name="de_Plaza" type="string" required="no" default="">
    <cfargument name="nb_Autoriza" type="string" required="no" default="">
    <cfargument name="fh_registro" type="string" required="no" default="">
    <cfargument name="nb_ContactoCoppel" type="string" required="no" default="">
    <cfargument name="nb_Banco" type="string" required="no" default="">
    <cfargument name="nu_CuentaCheques" type="string" required="no" default="">
    <cfargument name="nu_SucursalBancaria" type="string" required="no" default="">
	
	<cfargument name="de_Password" type="string" required="no" default="">
    <cfargument name='de_eMail2' type='string' required='no' default=''>
    <cfargument name='de_eMail3' type='string' required='no' default=''>
    <cfargument name='de_EscrituraPublica' type='string' required='no' default=''>
    
    <cfargument name="fh_escriturapublica" type="string" required="no" default="">
    <cfargument name='nb_licescriturapublica' type='string' required='no' default=''>
    <cfargument name='nu_notarioescriturapublica' type='string' required='no' default=''>
    <cfargument name='nb_ejercicioescriturapublica' type='string' required='no' default=''>
    <cfargument name="nu_foliomercantilescpub" type="string" required="no" default="">
    <!--- Kaleb Ontiveros    --->
    <!--- Solicito: D'Labra  --->
    <!--- 24/07/2014         --->
    <cfargument name="nu_diascredito" type="any" required="no" default="">
    
    <cfset fh_Alta = dateFormat(Now(),'yyyy-mm-dd') >
    <!---<cfset fh_Registro = dateFormat(Now(),'yyyy-mm-dd') >--->
    <cfset de_RFC = REPLACE(#de_RFC#,'-','','ALL') >
  	<cfquery datasource='#cnx#' name='nextID' username='#user_sql#' password='#password_sql#'>
        SELECT MAX(id_Proveedor)+1 as ID FROM Proveedores
    </cfquery>
    
	<cfquery datasource='#cnx#' name='RSExisteRFCyMonedas' username='#user_sql#' password='#password_sql#'>
		EXECUTE Proveedores_ExisteRFCyMoneda '#de_RFC#', #id_Moneda#
	</cfquery>
	<cfif #RSExisteRFCyMonedas.Existe# EQ 1>
		<cfset Proveedores.agregado= FALSE>
		<cfset Proveedores.tipoError= 'validaciones'>
		<cfset Proveedores.mensaje= 'Un Proveedor con el mismo RFC especificado y la moneda ya existen, favor de verificarlos'>
		<cfreturn Proveedores>		
	</cfif>
	<cftry>
	
		<cfquery datasource='#cnx#' name='RSAgregar' username='#user_sql#' password='#password_sql#'>
			bop_Proveedores_Agregar #nextID.id#, '#nb_Proveedor#', '#de_Direccion#', '#nb_Colonia#', '#nb_ProveedorCorto#', 
            								#nu_CodigoPostal#, #id_Ciudad#, '#de_RFC#','#nb_Responsable#', '#nu_Telefono1#', '#nu_Telefono2#', 
                                            <!---<cfif #nu_Capacidad# EQ 0>NULL<cfelse>'#nu_Capacidad#'</cfif>,--->
                                            '#nu_Fax#', '#de_eMail#', #nu_LimiteCredito#, #id_Modulo#, #id_TipoMovimiento#, #pj_Descuento#, 
                                            #nu_CondicionesPago#, #id_Moneda#, #sn_Contratista#, '#cl_TipoPersona#', #pj_Iva#, '#cl_TipoCuentaBancaria#', 
                                            '#nu_CuentaBancaria#', '#nu_CuentaClabe#', '#de_Giro#', '#de_ActaConstitutiva#', '#de_RegistroIMSS#', 
                                            #nu_DigitoIMSS#, '#de_RegistroPublico#', '#de_NumeroPoder#', '#de_Curp#', '#de_Plaza#', '#nb_Autoriza#',
                                            <cfif #fh_registro# EQ ''>NULL<cfelse>'#fh_Registro#'</cfif>, '#nb_ContactoCoppel#', '#nb_Banco#', 
                                            '#nu_CuentaCheques#', '#nu_SucursalBancaria#', 1, '#fh_Alta#', '#de_Password#', '#de_eMail2#', '#de_eMail3#',<cfif #de_EscrituraPublica# EQ ''>NULL<cfelse>'#de_EscrituraPublica#'</cfif>,
                             <cfif #fh_escriturapublica# EQ ''>NULL<cfelse>'#fh_escriturapublica#'</cfif>,
                             <cfif #nb_licescriturapublica# EQ ''>NULL<cfelse>'#nb_licescriturapublica#'</cfif>,
                             <cfif #nu_notarioescriturapublica# EQ ''>NULL<cfelse>'#nu_notarioescriturapublica#'</cfif>,
                             <cfif #nb_ejercicioescriturapublica# EQ ''>NULL<cfelse>'#nb_ejercicioescriturapublica#'</cfif>,
                             <cfif #nu_foliomercantilescpub# EQ ''>NULL<cfelse>'#nu_foliomercantilescpub#'</cfif>               
                             <!--- Kaleb Ontiveros    --->
                             <!--- Solicito: D'Labra  --->
                             <!--- 24/07/2014         --->
                             <cfif IsNumeric('#arguments.nu_diascredito#')>,#nu_diascredito#</cfif> 
		</cfquery>
		<!---Log--->
		<cf_log datasource='#cnx#' username='#user_sql#' password='#password_sql#' id_Empresa="#Session.id_Empresa#" id_Obra="0" id_Usuario="#Session.id_Usuario#" id_MenuOpcion="#Session.id_MenuOpcion#">
			<cfoutput>
				bop_Proveedores_Agregar #nextID.id#, '#nb_Proveedor#', '#de_Direccion#', '#nb_Colonia#', '#nb_ProveedorCorto#', 
                								#nu_CodigoPostal#, #id_Ciudad#, '#de_RFC#', '#nb_Responsable#', '#nu_Telefono1#', '#nu_Telefono2#', 
                                                <!---<cfif #nu_Capacidad# EQ 0>NULL<cfelse>'#nu_Capacidad#'</cfif>,--->
                                                '#nu_Fax#', '#de_eMail#', #nu_LimiteCredito#, #id_Modulo#, #id_TipoMovimiento#, #pj_Descuento#, 
                                                #nu_CondicionesPago#, #id_Moneda#, #sn_Contratista#, '#cl_TipoPersona#', #pj_Iva#, '#cl_TipoCuentaBancaria#', 
                                                '#nu_CuentaBancaria#', '#nu_CuentaClabe#', '#de_Giro#', '#de_ActaConstitutiva#', '#de_RegistroIMSS#', 
                                                #nu_DigitoIMSS#, '#de_RegistroPublico#', '#de_NumeroPoder#', '#de_Curp#', '#de_Plaza#', '#nb_Autoriza#',
                                            	<cfif #fh_registro# EQ ''>NULL<cfelse>'#fh_Registro#'</cfif>, '#nb_ContactoCoppel#', 
                                                '#nb_Banco#', '#nu_CuentaCheques#', '#nu_SucursalBancaria#', 1, '#fh_Alta#', '#de_Password#', '#de_eMail2#', '#de_eMail3#',<cfif #de_EscrituraPublica# EQ ''>NULL<cfelse>'#de_EscrituraPublica#'</cfif>,
                             <cfif #fh_escriturapublica# EQ ''>NULL<cfelse>'#fh_escriturapublica#'</cfif>,
                             <cfif #nb_licescriturapublica# EQ ''>NULL<cfelse>'#nb_licescriturapublica#'</cfif>,
                             <cfif #nu_notarioescriturapublica# EQ ''>NULL<cfelse>'#nu_notarioescriturapublica#'</cfif>,
                             <cfif #nb_ejercicioescriturapublica# EQ ''>NULL<cfelse>'#nb_ejercicioescriturapublica#'</cfif>,
                             <cfif #nu_foliomercantilescpub# EQ ''>NULL<cfelse>'#nu_foliomercantilescpub#'</cfif>              
			</cfoutput>
		</cf_log>
		<!---Termina Log --->
		<cfset Proveedores.agregado= TRUE>
		<cfset Proveedores.tipoError= ''>
		<cfset Proveedores.mensaje= 'Proveedor guardado correctamente'>
		<cfreturn Proveedores>
		<cfcatch type='database'>
			<cfset Proveedores.agregado= FALSE>
			<cfif cfcatch.NativeErrorCode EQ '2627'>
				<cfset Proveedores.tipoError= 'database-registro_duplicado'>
				<cfset Proveedores.mensaje= 'El Proveedor no se pudo guardar porque ya existe'>
			<cfelse>
				<cfset Proveedores.tipoError= 'database-indefinido'>
				<cfset Proveedores.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			</cfif>
			<cfreturn Proveedores>
		</cfcatch>
	</cftry>
</cffunction>

<!---Hebert - 2010-01-11 --->
<!---Funcion que agrega un proveedor en contatos con ciertas reglas de negocio --->
<cffunction name='RSAgregarProveedorDeContratos' access='public' returntype='struct'>
	<cfargument name='id_Proveedor' type='numeric' required='yes'>
	<cfargument name='nb_Proveedor' type='string' required='yes'>
	<cfargument name='de_Direccion' type='string' required='no' default=''>
	<cfargument name='nb_Colonia' type='string' required='no' default=''>
	<cfargument name='nb_ProveedorCorto' type='string' required='no' default=''>
	<cfargument name='nu_CodigoPostal' type='numeric' required='no' default='0'>
	<cfargument name='id_Ciudad' type='numeric' required='yes'>
	<cfargument name='de_RFC' type='string' required='no' default=''>
	<cfargument name='nb_Responsable' type='string' required='no' default=''>
	<cfargument name='nu_Telefono1' type='string' required='no' default=''>
	<cfargument name='nu_Telefono2' type='string' required='no' default=''>
	<cfargument name='nu_Fax' type='string' required='no' default=''>
	<cfargument name='de_eMail' type='string' required='no' default=''>
	<cfargument name='nu_LimiteCredito' type='numeric' required='no' default='0'>
	<cfargument name='id_Modulo' type='numeric' required='yes'>
	<cfargument name='id_TipoMovimiento' type='numeric' required='yes'>
	<cfargument name='pj_Descuento' type='numeric' required='no' default='0'>
	<cfargument name='nu_CondicionesPago' type='numeric' required='no' default='0'>
	<cfargument name='id_Moneda' type='numeric' required='yes'>
	<cfargument name='sn_Contratista' type='boolean' required='yes'>
	<cfargument name='cl_TipoPersona' type='string' required='no' default='F'>
	<cfargument name='pj_Iva' type="numeric" required="no" default="0">
	<cfargument name='cl_TipoCuentaBancaria' type='string' required='no' default=''>
	<cfargument name='nu_CuentaBancaria' type='string' required='no' default=''>
	<cfargument name='nu_CuentaClabe' type='string' required='no' default=''>
	<cfargument name='de_Giro' type='string' required='no' default=''>
	<cfargument name='de_ActaConstitutiva' type='string' required='no' default=''>
	<cfargument name='de_RegistroIMSS' type='string' required='no' default=''>
	<cfargument name='nu_DigitoIMSS' type='numeric' required='no' default='0'>
	<cfargument name='de_RegistroPublico' type='string' required='no' default=''>
	<cfargument name='de_NumeroPoder' type='string' required='no' default=''>
	
	<cfquery datasource='#cnx#' name='RSExisteRFCyMonedas' username='#user_sql#' password='#password_sql#'>
		EXECUTE Proveedores_ExisteRFCyMoneda '#de_RFC#', #id_Moneda#
	</cfquery>
	<cfif #RSExisteRFCyMonedas.Existe# EQ 1>
		<cfset Proveedores.agregado= FALSE>
		<cfset Proveedores.tipoError= 'validaciones'>
		<cfset Proveedores.mensaje= 'Un Proveedor con el mismo RFC especificado y la moneda ya existen, favor de verificarlos'>
		<cfreturn Proveedores>		
	</cfif>
	<cftry>
		<cfquery datasource='#cnx#' name='RSAgregar' username='#user_sql#' password='#password_sql#'>
			EXECUTE bop_Proveedores_Agregar #id_Proveedor#, '#nb_Proveedor#', '#de_Direccion#', '#nb_Colonia#', '#nb_ProveedorCorto#', #nu_CodigoPostal#, #id_Ciudad#, '#de_RFC#', '#nb_Responsable#', '#nu_Telefono1#', '#nu_Telefono2#', '#nu_Fax#', '#de_eMail#', #nu_LimiteCredito#, #id_Modulo#, #id_TipoMovimiento#, #pj_Descuento#, #nu_CondicionesPago#, #id_Moneda#, #sn_Contratista#, '#cl_TipoPersona#', #pj_Iva#, '#cl_TipoCuentaBancaria#', '#nu_CuentaBancaria#', '#nu_CuentaClabe#', '#de_Giro#', '#de_ActaConstitutiva#', '#de_RegistroIMSS#', #nu_DigitoIMSS#, '#de_RegistroPublico#', '#de_NumeroPoder#'
		</cfquery>
		<!---Log--->
		<cf_log datasource='#cnx#' username='#user_sql#' password='#password_sql#' id_Empresa="#Session.id_Empresa#" id_Obra="0" id_Usuario="#Session.id_Usuario#" id_MenuOpcion="#Session.id_MenuOpcion#">
			<cfoutput>
				EXECUTE bop_Proveedores_Agregar #id_Proveedor#, '#nb_Proveedor#', '#de_Direccion#', '#nb_Colonia#', '#nb_ProveedorCorto#', #nu_CodigoPostal#, #id_Ciudad#, '#de_RFC#', '#nb_Responsable#', '#nu_Telefono1#', '#nu_Telefono2#', '#nu_Fax#', '#de_eMail#', #nu_LimiteCredito#, #id_Modulo#, #id_TipoMovimiento#, #pj_Descuento#, #nu_CondicionesPago#, #id_Moneda#, #sn_Contratista#, '#cl_TipoPersona#', #pj_Iva#, '#cl_TipoCuentaBancaria#', '#nu_CuentaBancaria#', '#nu_CuentaClabe#', '#de_Giro#', '#de_ActaConstitutiva#', '#de_RegistroIMSS#', #nu_DigitoIMSS#, '#de_RegistroPublico#', '#de_NumeroPoder#'
			</cfoutput>
		</cf_log>
		<!---Termina Log --->
		<cfset Proveedores.agregado= TRUE>
		<cfset Proveedores.tipoError= ''>
		<cfset Proveedores.mensaje= 'Proveedor guardado correctamente'>
		<cfreturn Proveedores>
		<cfcatch type='database'>
			<cfset Proveedores.agregado= FALSE>
			<cfif cfcatch.NativeErrorCode EQ '2627'>
				<cfset Proveedores.tipoError= 'database-registro_duplicado'>
				<cfset Proveedores.mensaje= 'El Proveedor no se pudo guardar porque ya existe'>
			<cfelse>
				<cfset Proveedores.tipoError= 'database-indefinido'>
				<cfset Proveedores.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			</cfif>
			<cfreturn Proveedores>
		</cfcatch>
	</cftry>
</cffunction>


<!---Funcion que edita un proveedor con ciertas reglas de negocio --->
<!--- MODIFICADO: 2013-01-16 --->
<!--- DESARROLLADOR: Alfonso Carranza --->
<cffunction name='RSEditarProveedor' access='public' returntype='struct'>

	<cfargument name='id_Proveedor' type='numeric' required='yes'>
	<cfargument name='nb_Proveedor' type='string' required='yes'>
	<cfargument name='de_Direccion' type='string' required='no' default=''>
	<cfargument name='nb_Colonia' type='string' required='no' default=''>
	<cfargument name='nb_ProveedorCorto' type='string' required='no' default=''>
	<cfargument name='nu_CodigoPostal' type='numeric' required='no' default='0'>
	<cfargument name='id_Ciudad' type='numeric' required='yes'>
	<cfargument name='de_RFC' type='string' required='no' default=''>
	<cfargument name='nb_Responsable' type='string' required='no' default=''>
	<cfargument name='nu_Telefono1' type='string' required='no' default=''>
	<cfargument name='nu_Telefono2' type='string' required='no' default=''>
    
    <!---<cfargument name='nu_Capacidad' type='numeric' required='no' default='0'>--->
	<cfargument name='nu_Fax' type='string' required='no' default=''>
	<cfargument name='de_eMail' type='string' required='no' default=''>
	<cfargument name='nu_LimiteCredito' type='numeric' required='no' default='0'>
	<cfargument name='id_Modulo' type='numeric' required='yes'>
	<cfargument name='id_TipoMovimiento' type='numeric' required='yes'>
	<cfargument name='pj_Descuento' type='numeric' required='no' default='0'>
	<cfargument name='nu_CondicionesPago' type='numeric' required='no' default='0'>
	<cfargument name='id_Moneda' type='numeric' required='yes'>
	<cfargument name='sn_Contratista' type='boolean' required='yes'>
	<cfargument name='cl_TipoPersona' type='string' required='no' default='F'>
	<cfargument name='pj_Iva' type="numeric" required="no" default="0">
	<cfargument name='cl_TipoCuentaBancaria' type='string' required='no' default=''>
	<cfargument name='nu_CuentaBancaria' type='string' required='no' default=''>
	<cfargument name='nu_CuentaClabe' type='string' required='no' default=''>
	
	<cfargument name='de_Giro' type='string' required='no' default=''>
	<cfargument name='de_ActaConstitutiva' type='string' required='no' default=''>
	<cfargument name='de_RegistroIMSS' type='string' required='no' default=''>
	<cfargument name='nu_DigitoIMSS' type='numeric' required='no' default='0'>
	<cfargument name='de_RegistroPublico' type='string' required='no' default=''>
	<cfargument name='de_NumeroPoder' type='string' required='no' default=''>
    
    <cfargument name="de_Curp" type="string" required="no" default="">
    <cfargument name="de_Plaza" type="string" required="no" default="">
    <cfargument name="nb_Autoriza" type="string" required="no" default="">
    <cfargument name="fh_registro" type="string" required="no" default="">
    <cfargument name="nb_ContactoCoppel" type="string" required="no" default="">
    <cfargument name="nb_Banco" type="string" required="no" default="">
    <cfargument name="nu_CuentaCheques" type="string" required="no" default="">
    <cfargument name="nu_SucursalBancaria" type="string" required="no" default="">

	<cfargument name="de_Password" type="string" required="no" default="">
    <cfargument name='de_eMail2' type='string' required='no' default=''>
    <cfargument name='de_eMail3' type='string' required='no' default=''> 
    <cfargument name='de_EscrituraPublica' type='string' required='no' default=''> 
    
    <cfargument name="fh_escriturapublica" type="string" required="no" default="">
    <cfargument name='nb_licescriturapublica' type='string' required='no' default=''>
    <cfargument name='nu_notarioescriturapublica' type='string' required='no' default=''>
    <cfargument name='nb_ejercicioescriturapublica' type='string' required='no' default=''>
    <cfargument name="nu_foliomercantilescpub" type="string" required="no" default="">
    <cfargument name="nu_diascredito" type="any" required="no" default="">

	<cfif de_Password EQ ''>
		<cfset de_Password = 'NULL'>
	<cfelse>
		<cfset de_Password = DE(de_Password)>
	</cfif>
	
    <cfset de_RFC = REPLACE(#de_RFC#,'-','','ALL') >
    <!--- 
	<cfquery datasource='#cnx#' name='RSExisteRFCyMonedas' username='#user_sql#' password='#password_sql#'>
		EXECUTE Proveedores_ExisteRFCyMoneda '#de_RFC#', #id_Moneda#, #id_Proveedor#
	</cfquery>
	<cfif #RSExisteRFCyMonedas.Existe# EQ 1>
		<cfset Proveedores.actualizado= FALSE>
		<cfset Proveedores.tipoError= 'validaciones'>
		<cfset Proveedores.mensaje= 'Un Proveedor con el mismo RFC especificado y la moneda ya existen, favor de verificarlos'>
		<cfreturn Proveedores>		
	</cfif>
	 --->
	<cftry>
		<cfquery datasource='#cnx#' name='RSFilasAfectadas' username='#user_sql#' password='#password_sql#'>
			EXECUTE bop_Proveedores_Actualizar #id_Proveedor#, '#nb_Proveedor#', '#de_Direccion#', '#nb_Colonia#', 
            									'#nb_ProveedorCorto#', #nu_CodigoPostal#, #id_Ciudad#, '#de_RFC#', 
                                                '#nb_Responsable#', '#nu_Telefono1#', '#nu_Telefono2#', 
                                                <!---'#nu_Capacidad#', --->
                                                '#nu_Fax#', 
                                                '#de_eMail#', #nu_LimiteCredito#, #id_Modulo#, #id_TipoMovimiento#, 
                                                #pj_Descuento#, #nu_CondicionesPago#, #id_Moneda#, #sn_Contratista#, 
                                                '#cl_TipoPersona#', #pj_Iva#, '#cl_TipoCuentaBancaria#', '#nu_CuentaBancaria#', 
                                                '#nu_CuentaClabe#', '#de_Giro#', '#de_ActaConstitutiva#', '#de_RegistroIMSS#', 
                                                #nu_DigitoIMSS#, '#de_RegistroPublico#', '#de_NumeroPoder#', '#de_Curp#', '#de_Plaza#', 
                                                '#nb_Autoriza#', <cfif #fh_registro# EQ ''>NULL<cfelse>'#fh_Registro#'</cfif>,
                                                '#nb_ContactoCoppel#', '#nb_Banco#', '#nu_CuentaCheques#', '#nu_SucursalBancaria#',#de_Password#, '#de_eMail2#', '#de_eMail3#',<cfif #de_EscrituraPublica# EQ ''>NULL<cfelse>'#de_EscrituraPublica#'</cfif>,  
		 <cfif #fh_escriturapublica# EQ ''>NULL<cfelse>'#fh_escriturapublica#'</cfif>,
         <cfif #nb_licescriturapublica# EQ ''>NULL<cfelse>'#nb_licescriturapublica#'</cfif>,
         <cfif #nu_notarioescriturapublica# EQ ''>NULL<cfelse>'#nu_notarioescriturapublica#'</cfif>,
         <cfif #nb_ejercicioescriturapublica# EQ ''>NULL<cfelse>'#nb_ejercicioescriturapublica#'</cfif>,
         <cfif #nu_foliomercantilescpub# EQ ''>NULL<cfelse>'#nu_foliomercantilescpub#'</cfif>
        <!--- Kaleb Ontiveros    --->
	    <!--- Solicito: D'Labra  --->
	    <!--- 24/07/2014         --->
        <cfif IsNumeric('#arguments.nu_diascredito#')>,#nu_diascredito#</cfif>
                                            
		</cfquery>
		<!---Log--->
		<cf_log datasource='#cnx#' username='#user_sql#' password='#password_sql#' id_Empresa="#Session.id_Empresa#" id_Obra="0" id_Usuario="#Session.id_Usuario#" id_MenuOpcion="#Session.id_MenuOpcion#">
			<cfoutput>
                EXECUTE bop_Proveedores_Actualizar #id_Proveedor#, '#nb_Proveedor#', '#de_Direccion#', '#nb_Colonia#', 
                                                    '#nb_ProveedorCorto#', #nu_CodigoPostal#, #id_Ciudad#, '#de_RFC#', 
                                                    '#nb_Responsable#', '#nu_Telefono1#', '#nu_Telefono2#', 
                                                    <!---'#nu_Capacidad#', --->
                                                    '#nu_Fax#', 
                                                    '#de_eMail#', #nu_LimiteCredito#, #id_Modulo#, #id_TipoMovimiento#, 
                                                    #pj_Descuento#, #nu_CondicionesPago#, #id_Moneda#, #sn_Contratista#, 
                                                    '#cl_TipoPersona#', #pj_Iva#, '#cl_TipoCuentaBancaria#', '#nu_CuentaBancaria#', 
                                                    '#nu_CuentaClabe#', '#de_Giro#', '#de_ActaConstitutiva#', '#de_RegistroIMSS#', 
                                                    #nu_DigitoIMSS#, '#de_RegistroPublico#', '#de_NumeroPoder#', '#de_Curp#', '#de_Plaza#', 
                                                    '#nb_Autoriza#', <cfif #fh_registro# EQ ''>NULL<cfelse>'#fh_Registro#'</cfif>,
                                                    '#nb_ContactoCoppel#', '#nb_Banco#', '#nu_CuentaCheques#', '#nu_SucursalBancaria#',#de_Password#, '#de_eMail2#', '#de_eMail3#',<cfif #de_EscrituraPublica# EQ ''>NULL<cfelse>'#de_EscrituraPublica#'</cfif>, 
         <cfif #fh_escriturapublica# EQ ''>NULL<cfelse>'#fh_escriturapublica#'</cfif>,
         <cfif #nb_licescriturapublica# EQ ''>NULL<cfelse>'#nb_licescriturapublica#'</cfif>,
         <cfif #nu_notarioescriturapublica# EQ ''>NULL<cfelse>'#nu_notarioescriturapublica#'</cfif>,
         <cfif #nb_ejercicioescriturapublica# EQ ''>NULL<cfelse>'#nb_ejercicioescriturapublica#'</cfif>,
         <cfif #nu_foliomercantilescpub# EQ ''>NULL<cfelse>'#nu_foliomercantilescpub#'</cfif>
         
			</cfoutput>
		</cf_log>
		<!---Termina Log --->
		<cfset Proveedores.actualizado= TRUE>
		<cfset Proveedores.filasAfectadas= RSFilasAfectadas.filasAfectadas>
		<cfset Proveedores.tipoError= ''>
		<cfset Proveedores.mensaje= 'Proveedor actualizado correctamente'>
		<cfreturn Proveedores>
		<cfcatch type='database'>
			<cfset Proveedores.actualizado= FALSE>
			<cfset Proveedores.tipoError= 'database-indefinido'>
			<cfset Proveedores.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfset Proveedores.consulta= "EXECUTE bop_Proveedores_Actualizar #id_Proveedor#, '#nb_Proveedor#', '#de_Direccion#', '#nb_ProveedorCorto#', #nu_CodigoPostal#, #id_Ciudad#, '#de_RFC#', '#nb_Responsable#', '#nu_Telefono1#', '#nu_Telefono2#', '#nu_Capacidad#', '#nu_Fax#', '#de_eMail#', #nu_LimiteCredito#, #id_Modulo#, #id_TipoMovimiento#, #pj_Descuento#, #nu_CondicionesPago#, #id_Moneda#, #sn_Contratista#, '#cl_TipoPersona#', #pj_Iva#, '#cl_TipoCuentaBancaria#', '#nu_CuentaBancaria#', '#nu_CuentaClabe#', '#de_Giro#', '#de_ActaConstitutiva#', '#de_RegistroIMSS#', #nu_DigitoIMSS#, '#de_RegistroPublico#', '#de_NumeroPoder#'">
			<cfreturn Proveedores>
		</cfcatch>
	</cftry>
</cffunction>

<!---Hebert GB - 2010-01-11 --->
<!---Funcion que edita un proveedor en contratos con ciertas reglas de negocio --->
<cffunction name='RSEditarProveedorDeContratos' access='public' returntype='struct'>

	<cfargument name='id_Proveedor' type='numeric' required='yes'>
	<cfargument name='nb_Proveedor' type='string' required='yes'>
	<cfargument name='de_Direccion' type='string' required='no' default=''>
	<cfargument name='nb_Colonia' type='string' required='no' default=''>
	<cfargument name='nb_ProveedorCorto' type='string' required='no' default=''>
	<cfargument name='nu_CodigoPostal' type='numeric' required='no' default='0'>
	<cfargument name='id_Ciudad' type='numeric' required='yes'>
	<cfargument name='de_RFC' type='string' required='no' default=''>
	<cfargument name='nb_Responsable' type='string' required='no' default=''>
	<cfargument name='nu_Telefono1' type='string' required='no' default=''>
	<cfargument name='nu_Telefono2' type='string' required='no' default=''>
	<cfargument name='nu_Fax' type='string' required='no' default=''>
	<cfargument name='de_eMail' type='string' required='no' default=''>
	<cfargument name='pj_Descuento' type='numeric' required='no' default='0'>
	<cfargument name='id_Moneda' type='numeric' required='yes'>
	<cfargument name='cl_TipoPersona' type='string' required='no' default='F'>
	<cfargument name='pj_Iva' type="numeric" required="no" default="0">
	<cfargument name='de_Giro' type='string' required='no' default=''>
	<cfargument name='de_ActaConstitutiva' type='string' required='no' default=''>
	<cfargument name='de_RegistroIMSS' type='string' required='no' default=''>
	<cfargument name='nu_DigitoIMSS' type='numeric' required='no' default='0'>
	<cfargument name='de_RegistroPublico' type='string' required='no' default=''>
	<cfargument name='de_NumeroPoder' type='string' required='no' default=''>

	<cfquery datasource='#cnx#' name='RSExisteRFCyMonedas' username='#user_sql#' password='#password_sql#'>
		EXECUTE Proveedores_ExisteRFCyMoneda '#de_RFC#', #id_Moneda#, #id_Proveedor#
	</cfquery>
	<cfif #RSExisteRFCyMonedas.Existe# EQ 1>
		<cfset Proveedores.actualizado= FALSE>
		<cfset Proveedores.tipoError= 'validaciones'>
		<cfset Proveedores.mensaje= 'Un Proveedor con el mismo RFC especificado y la moneda ya existen, favor de verificarlos'>
		<cfreturn Proveedores>		
	</cfif>
	<cftry>
		<cfquery datasource='#cnx#' name='RSFilasAfectadas' username='#user_sql#' password='#password_sql#'>
			EXECUTE Proveedores_ActualizarEnContratos #id_Proveedor#, '#nb_Proveedor#', '#de_Direccion#', '#nb_Colonia#', '#nb_ProveedorCorto#', #nu_CodigoPostal#, #id_Ciudad#, '#de_RFC#', '#nb_Responsable#', '#nu_Telefono1#', '#nu_Telefono2#', '#nu_Fax#', '#de_eMail#', #pj_Descuento#, #id_Moneda#, '#cl_TipoPersona#', #pj_Iva#, '#de_Giro#', '#de_ActaConstitutiva#', '#de_RegistroIMSS#', #nu_DigitoIMSS#, '#de_RegistroPublico#', '#de_NumeroPoder#'
		</cfquery>
		<!---Log--->
		<cf_log datasource='#cnx#' username='#user_sql#' password='#password_sql#' id_Empresa="#Session.id_Empresa#" id_Obra="0" id_Usuario="#Session.id_Usuario#" id_MenuOpcion="#Session.id_MenuOpcion#">
			<cfoutput>
				EXECUTE Proveedores_ActualizarEnContratos #id_Proveedor#, '#nb_Proveedor#', '#de_Direccion#', '#nb_Colonia#', '#nb_ProveedorCorto#', #nu_CodigoPostal#, #id_Ciudad#, '#de_RFC#', '#nb_Responsable#', '#nu_Telefono1#', '#nu_Telefono2#', '#nu_Fax#', '#de_eMail#', #pj_Descuento#, #id_Moneda#, '#cl_TipoPersona#', #pj_Iva#, '#de_Giro#', '#de_ActaConstitutiva#', '#de_RegistroIMSS#', #nu_DigitoIMSS#, '#de_RegistroPublico#', '#de_NumeroPoder#'
			</cfoutput>
		</cf_log>
		<!---Termina Log --->
		<cfset Proveedores.actualizado= TRUE>
		<cfset Proveedores.filasAfectadas= RSFilasAfectadas.filasAfectadas>
		<cfset Proveedores.tipoError= ''>
		<cfset Proveedores.mensaje= 'Proveedor actualizado correctamente'>
		<cfreturn Proveedores>
		<cfcatch type='database'>
			<cfset Proveedores.actualizado= FALSE>
			<cfset Proveedores.tipoError= 'database-indefinido'>
			<cfset Proveedores.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn Proveedores>
		</cfcatch>
	</cftry>
</cffunction>

<!---Funcion que devuelve proveedores sugeridos para combo--->
<cffunction name="getProveedoresSugeridastmp" access="remote" returntype="query">
	<cfargument name="str_Proveedor" type="string" required="yes">
    
	<cfif isNumeric(#str_Proveedor#)>
		<cfset id_Proveedor= #str_Proveedor#>
		<cfset nb_ProveedorCorto= "NULL">
	<cfelse>
		<cfset id_Proveedor= "NULL">
		<cfif #str_Proveedor# NEQ "">
			<cfset nb_ProveedorCorto= DE("#str_Proveedor#")>
		<cfelse>
			<cfset nb_ProveedorCorto= "NULL">
		</cfif>
	</cfif>
        
	<cfquery name="RSProveedores" datasource="#cnx#" username="#user_sql#" password="#password_sql#">
		EXECUTE Proveedores_ObtenProveedoresSugeridos #id_Proveedor#, #nb_ProveedorCorto#
	</cfquery>
	<cfif #str_Proveedor# EQ "">
		<cfset RSTodos= QueryNew("id_Proveedor,nb_ProveedorCorto")>
		<cfset QueryAddRow(RSTodos,1)>
		<cfset QuerySetCell(RSTodos,"id_Proveedor","",1)>
		<cfset QuerySetCell(RSTodos,"nb_ProveedorCorto","SELECCIONE UN PROVEEDOR",1)>
		<cfinvoke component="#Application.componentes#.funciones" method="JuntarQuerys" queryUno="#RSTodos#" queryDos="#RSProveedores#">
		<cfreturn RSTodos>
	<cfelse>
		<cfreturn RSProveedores>
	</cfif>
</cffunction>

<!---Funcion que devuelve proveedores sugeridos para combo COMPRADOR--->
<cffunction name="getProveedoresSugeridastmpComprador" access="remote" returntype="query">
	<cfargument name="str_Proveedor" type="string" required="yes">
    <cfargument name="EmpleadoComprador" type="string" required="no" default="">
    
	<cfif isNumeric(#str_Proveedor#)>
		<cfset id_Proveedor= #str_Proveedor#>
		<cfset nb_ProveedorCorto= "NULL">
	<cfelse>
		<cfset id_Proveedor= "NULL">
		<cfif #str_Proveedor# NEQ "">
			<cfset nb_ProveedorCorto= DE("#str_Proveedor#")>
		<cfelse>
			<cfset nb_ProveedorCorto= "NULL">
		</cfif>
	</cfif>
    
    <cfif isDefined(#EmpleadoComprador#)>
		<cfif isNumeric(#EmpleadoComprador#)>
            <cfset empleado = #EmpleadoComprador#>
        <cfelse>
            <cfset empleado = "NULL">
        </cfif>
    <cfelse>
    	<cfset empleado = "NULL">
    </cfif>
        
	<cfquery name="RSProveedores" datasource="#cnx#" username="#user_sql#" password="#password_sql#">
		EXECUTE Proveedores_ObtenProveedoresSugeridosComprador #id_Proveedor#, #nb_ProveedorCorto#, #empleado#
	</cfquery>
	<cfif #str_Proveedor# EQ "">
		<cfset RSTodos= QueryNew("id_Proveedor,nb_ProveedorCorto")>
		<cfset QueryAddRow(RSTodos,1)>
		<cfset QuerySetCell(RSTodos,"id_Proveedor","",1)>
		<cfset QuerySetCell(RSTodos,"nb_ProveedorCorto","SELECCIONE UN PROVEEDOR",1)>
		<cfinvoke component="#Application.componentes#.funciones" method="JuntarQuerys" queryUno="#RSTodos#" queryDos="#RSProveedores#">
		<cfreturn RSTodos>
	<cfelse>
		<cfreturn RSProveedores>
	</cfif>
</cffunction>

<!---Funcion que devuelve proveedores de tipo contratista sugeridos para combo--->
<cffunction name="getContratistasSugeridos" access="remote" returntype="query">
	<cfargument name="str_Proveedor" type="string" required="yes">
	<cfif isNumeric(#str_Proveedor#)>
		<cfset id_Proveedor= #str_Proveedor#>
		<cfset nb_ProveedorCorto= "NULL">
	<cfelse>
		<cfset id_Proveedor= "NULL">
		<cfif #str_Proveedor# NEQ "">
			<cfset nb_ProveedorCorto= DE("#str_Proveedor#")>
		<cfelse>
			<cfset nb_ProveedorCorto= "NULL">
		</cfif>
	</cfif>
	<cfquery name="RSProveedores" datasource="#cnx#" username="#user_sql#" password="#password_sql#">
		EXECUTE Proveedores_ObtenContratistasSugeridos #id_Proveedor#, #nb_ProveedorCorto#
	</cfquery>
	<cfif #str_Proveedor# EQ "">
		<cfset RSTodos= QueryNew("id_Proveedor,nb_ProveedorCorto")>
		<cfset QueryAddRow(RSTodos,1)>
		<cfset QuerySetCell(RSTodos,"id_Proveedor","",1)>
		<cfset QuerySetCell(RSTodos,"nb_ProveedorCorto","SELECCIONE UN CONTRATISTA",1)>
		<cfinvoke component="#Application.componentes#.funciones" method="JuntarQuerys" queryUno="#RSTodos#" queryDos="#RSProveedores#">
		<cfreturn RSTodos>
	<cfelse>
		<cfreturn RSProveedores>
	</cfif>
</cffunction>

<!---Funcion que devuelve proveedores sugeridos para combo de Filtro--->
<cffunction name="getProveedoresSugeridasParaFiltrotmp" access="remote" returntype="query">
	<cfargument name="str_Proveedor" type="string" required="true" >
	<cfif isNumeric(#str_Proveedor#)>
		<cfset id_Proveedor= #str_Proveedor#>
		<cfset nb_ProveedorCorto= "NULL">
	<cfelse>
		<cfset id_Proveedor= "NULL">
		<cfif #str_Proveedor# NEQ "">
			<cfset nb_ProveedorCorto= DE("#str_Proveedor#")>
		<cfelse>
			<cfset nb_ProveedorCorto= "NULL">
		</cfif>
	</cfif>
	<cfquery name="RSProveedores" datasource="#cnx#" username="#user_sql#" password="#password_sql#">
		EXECUTE Proveedores_ObtenProveedoresSugeridos #id_Proveedor#, #nb_ProveedorCorto#
	</cfquery>
	<cfif #str_Proveedor# EQ "">
		<cfset RSTodos= QueryNew("id_Proveedor,nb_ProveedorCorto")>
		<cfset QueryAddRow(RSTodos,1)>
		<cfset QuerySetCell(RSTodos,"id_Proveedor","",1)>
		<cfset QuerySetCell(RSTodos,"nb_ProveedorCorto","SELECCIONE",1)>
		<cfinvoke component="#Application.componentes#.funciones" method="JuntarQuerys" queryUno="#RSTodos#" queryDos="#RSProveedores#">
		<cfreturn RSTodos>
	<cfelse>
		<cfreturn RSProveedores>
	</cfif>
</cffunction>

<!---Funcion que devuelve proveedores sugeridos para combo de Filtro--->
<cffunction name="getContratistasSugeridosParaFiltro" access="remote" returntype="query">
	<cfargument name="str_Proveedor" type="string" required="yes">
	<cfif isNumeric(#str_Proveedor#)>
		<cfset id_Proveedor= #str_Proveedor#>
		<cfset nb_ProveedorCorto= "NULL">
	<cfelse>
		<cfset id_Proveedor= "NULL">
		<cfif #str_Proveedor# NEQ "">
			<cfset nb_ProveedorCorto= DE("#str_Proveedor#")>
		<cfelse>
			<cfset nb_ProveedorCorto= "NULL">
		</cfif>
	</cfif>
	<cfquery name="RSProveedores" datasource="#cnx#" username="#user_sql#" password="#password_sql#">
		EXECUTE Proveedores_ObtenContratistasSugeridos #id_Proveedor#, #nb_ProveedorCorto# 
	</cfquery>
	<cfif #str_Proveedor# EQ "">
		<cfset RSTodos= QueryNew("id_Proveedor,nb_ProveedorCorto")>
		<cfset QueryAddRow(RSTodos,1)>
		<cfset QuerySetCell(RSTodos,"id_Proveedor","",1)>
		<cfset QuerySetCell(RSTodos,"nb_ProveedorCorto","Todos",1)>
		<cfinvoke component="#Application.componentes#.funciones" method="JuntarQuerys" queryUno="#RSTodos#" queryDos="#RSProveedores#">
		<cfreturn RSTodos>
	<cfelse>
		<cfreturn RSProveedores>
	</cfif>
</cffunction>


<!---Funcion que trae el nb_Proveedor por su id para bindeo--->
<cffunction name="getProveedorPorID" access="remote" returntype="string">
	<cfargument name="id_Proveedor" type="string" required="no">
	<cfargument name="comodin1" type="string" required="no">

	<cfif Not IsNumeric(#id_Proveedor#)>
		<cfreturn "">
	<cfelse>
		<cfquery name="RS" datasource='#cnx#' username='#user_sql#' password='#password_sql#'>
			EXECUTE bop_Proveedores_ObtenerPorID #id_Proveedor#
		</cfquery>
		<cfreturn RS.nb_Proveedor>
	</cfif>
</cffunction>

<cffunction name="getProveedorPorIDJSON" access="remote" returntype="string" returnFORMAT="JSON">
	<cfargument name="id_Proveedor" type="string" required="no">
	<cfargument name="comodin1" type="string" required="no">

	<cfif Not IsNumeric(#id_Proveedor#)>
		<cfreturn "">
	<cfelse>
		<cfquery name="RS" datasource='#cnx#' username='#user_sql#' password='#password_sql#'>
			EXECUTE bop_Proveedores_ObtenerPorID #id_Proveedor#
		</cfquery>
		<cfreturn RS.nb_Proveedor>
	</cfif>
</cffunction>


<!--- Funcion para traer el minimo y el maximo proveedor --->
<cffunction name='RSProveedoresMinimoMaximo' access='public' returntype='struct'>

	<cftry>
		<cfquery datasource='#cnx#' name='RS_MostrarProveedores'>
			EXECUTE Proveedores_MinimoMaximo 
		</cfquery>
		<cfset Proveedores.listado= TRUE>
		<cfset Proveedores.tipoError= ''>
		<cfset Proveedores.rs= RS_MostrarProveedores>
		<cfset Proveedores.mensaje= 'Se obtuvo el recordset de Proveedores correctamente'>
		<cfreturn Proveedores>
		<cfcatch type='database'>
			<cfset Proveedores.listado= FALSE>
			<cfset Proveedores.tipoError= 'database-indefinido'>
			<cfset Proveedores.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn Proveedores>
		</cfcatch>
	</cftry>
</cffunction>

<!--- Funcion para traer el minimo y el maximo conrtatista --->
<cffunction name='RSContratistasMinimoMaximo' access='public' returntype='struct'>

	<cftry>
		<cfquery datasource='#cnx#' name='RS_MostrarProveedores' username='#user_sql#' password='#password_sql#'>
			EXECUTE Proveedores_ContratistasMinimoMaximo
		</cfquery>
		<cfset Proveedores.listado= TRUE>
		<cfset Proveedores.tipoError= ''>
		<cfset Proveedores.rs= RS_MostrarProveedores>
		<cfset Proveedores.mensaje= 'Se obtuvo el recordset de Proveedores correctamente'>
		<cfreturn Proveedores>
		<cfcatch type='database'>
			<cfset Proveedores.listado= FALSE>
			<cfset Proveedores.tipoError= 'database-indefinido'>
			<cfset Proveedores.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn Proveedores>
		</cfcatch>
	</cftry>
</cffunction>


<!---Funcion que trae el nb_ProveedorCorto de un conrtatista (proveedor con sn_Conrtatista= 1) por su id para bindeo--->
<cffunction name="getContratistaPorID" access="remote" returntype="string">
	<cfargument name="id_Proveedor" type="string" required="no">
	<cfargument name="comodin1" required="no" type="string">
	
	<cfif Not IsNumeric(#id_Proveedor#)>
		<cfreturn "">
	<cfelse>
		<cfquery name="RS" datasource='#cnx#' username='#user_sql#' password='#password_sql#'>
			EXECUTE Proveedores_ObtenerContratistaPorID #id_Proveedor#
		</cfquery>
		<cfreturn RS.nb_Proveedor>
	</cfif>
</cffunction>

<!---Funcion que devuelve el nu_LimiteCredito de un proveedor --->
<cffunction name="getLimiteCredito" access="remote" returntype="string">
	<cfargument name="id_Proveedor" type="string" required="yes">
	<cfargument name="tipo" type="string" required="no" default="">
	<cfif Not IsNumeric(#id_Proveedor#)>
		<cfreturn "">
	<cfelse>
		<cfquery name="RS" datasource='#cnx#' username='#user_sql#' password='#password_sql#'>
			EXECUTE Proveedores_ObtenerLimiteCredito #id_Proveedor#
		</cfquery>
		<cfset nu_LimiteCredito= RS.nu_LimiteCredito>
		<cfif #tipo# EQ "informacion">
			<cfset nu_LimiteCredito= Numberformat(nu_LimiteCredito, "$,_.____")>
		</cfif>
		<cfreturn #nu_LimiteCredito#>
	</cfif>
</cffunction>

<!---Funcion que devuelve el nu_LimiteCredito de un proveedor --->
<cffunction name="getCondicionesDePago" access="remote" returntype="string">
	<cfargument name="id_Proveedor" type="string" required="yes">
	<cfif Not IsNumeric(#id_Proveedor#)>
		<cfreturn "">
	<cfelse>
		<cfquery name="RS" datasource='#cnx#' username='#user_sql#' password='#password_sql#'>
			EXECUTE Proveedores_ObtenerCondicionesDePago #id_Proveedor#
		</cfquery>
		<cfset nu_CondicionesPago= RS.nu_CondicionesPago>
		<cfreturn #nu_CondicionesPago#>
	</cfif>
</cffunction>

<!---Funcion que trae el de_TipoMovimiento por su id para bindeo--->
<cffunction name="getTipoMovimientoPorID" access="remote" returntype="string">
	<cfargument name="id_Modulo" type="numeric" required="yes">
	<cfargument name="id_TipoMovimiento" type="string" required="no">
	<cfif Not IsNumeric(#id_TipoMovimiento#)>
		<cfreturn "">
	<cfelse>
		<cfquery name="RS" datasource='#cnx#' username='#user_sql#' password='#password_sql#'>
			EXECUTE bop_TiposMovimientos_ObtenerPorID #id_Modulo#, #id_TipoMovimiento#
		</cfquery>
		<cfreturn RS.de_TipoMovimiento>
	</cfif>
</cffunction>

<!--- Funcion para traer el minimo y el maximo tipos de movimientos --->
<cffunction name='RSTiposMovimientosMinimoMaximo' access='public' returntype='struct'>
	<cfargument name="id_Modulo" type="numeric" required="yes">

	<cftry>
		<cfquery datasource='#cnx#' name='RS_MostrarTiposMovimientos' username='#user_sql#' password='#password_sql#'>
			EXECUTE TiposMovimientos_MinimoMaximo #id_Modulo#
		</cfquery>
		<cfset TiposMovimientos.listado= TRUE>
		<cfset TiposMovimientos.tipoError= ''>
		<cfset TiposMovimientos.rs= RS_MostrarTiposMovimientos>
		<cfset TiposMovimientos.mensaje= 'Se obtuvo el recordset de TiposMovimientos correctamente'>
		<cfreturn TiposMovimientos>
		<cfcatch type='database'>
			<cfset TiposMovimientos.listado= FALSE>
			<cfset TiposMovimientos.tipoError= 'database-indefinido'>
			<cfset TiposMovimientos.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn TiposMovimientos>
		</cfcatch>
	</cftry>
</cffunction>

<!--- Funcion para traer el id_Moneda y nb_Moneda de un Proveedor --->
<!--- Juan Escobar 10/11/2009 --->
<cffunction name='RSProveedoresMostrarMoneda' access='public' returntype='struct'>

	<cfargument name='id_Proveedor' type='numeric' required='yes'>

	<cftry>
		<cfquery datasource='#cnx#' name='RS_MostrarProveedores' username='#user_sql#' password='#password_sql#'>
			EXECUTE Proveedores_TraerMoneda #id_Proveedor#
		</cfquery>
		<cfset Proveedores.listado= TRUE>
		<cfset Proveedores.tipoError= ''>
		<cfset Proveedores.rs= RS_MostrarProveedores>
		<cfset Proveedores.mensaje= 'Se obtuvo el recordset de Proveedores correctamente'>
		<cfreturn Proveedores>
		<cfcatch type='database'>
			<cfset Proveedores.listado= FALSE>
			<cfset Proveedores.tipoError= 'database-indefinido'>
			<cfset Proveedores.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn Proveedores>
		</cfcatch>
	</cftry>
</cffunction>

<!--- Funcion que regresa la moneda OBLIGATORIA para un proveedor dependiendo de su ID.. revision del dia 12/11/2009 --->
<!--- Juan Escobar 16/11/2009 --->
<cffunction name="getMonedaPorClaveProveedor" access="remote" returntype="string">
	
	<cfargument name="id_Proveedor" type="string" required="no" default="">
	<cfargument name="tipo" type="string" required="no" default="">
	
	<cfif Not IsNumeric(#id_Proveedor#)>
		<cfreturn "">
	</cfif>
	
	<cfif id_Proveedor LT 9000>
		<cfset id_moneda = 1>
		<cfset nb_moneda = 'MN'>
	<cfelse>
		<cfset id_moneda = 2>
		<cfset nb_moneda = 'USD'>
	</cfif>
	
	<cfif tipo EQ 'id_Moneda'>
		<cfreturn #id_Moneda#>
	<cfelseif tipo EQ 'nb_Moneda'>
		<cfreturn #nb_Moneda#>
	</cfif>
</cffunction>

<!--- Funcion que regresa un proveedor apartir de un RFC dado --->
<!--- Kaleb Ontiveros 30/11/2012 --->
<cffunction name="RSProveedorPorRFC" access="public" returntype="struct">
	<cfargument name="de_RFC" type="string" required="yes">
    <cfset de_RFC = REPLACE(#de_RFC#,'-','','ALL') >
    
	<cftry>
		<cfquery datasource='#cnx#' name='RS_MostrarProveedores' username='#user_sql#' password='#password_sql#' result="aa">
			EXECUTE Proveedores_ObtenerPorRFC '#de_RFC#'
		</cfquery>       

		<cfset Proveedores.listado= TRUE>
		<cfset Proveedores.tipoError= ''>
		<cfset Proveedores.rs = RS_MostrarProveedores>
		<cfset Proveedores.mensaje= 'Se obtuvo el recordset de Proveedores correctamente'>
		<cfreturn Proveedores>
		<cfcatch type='database'>
			<cfset Proveedores.listado= FALSE>
			<cfset Proveedores.tipoError= 'database-indefinido'>
			<cfset Proveedores.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn Proveedores>
		</cfcatch>
	</cftry>
</cffunction>

<!---agrega una factura (portal proveedores)--->
<cffunction name="RSfacturasAgregarPorProveedor"  access="public" returntype="struct">
	<cfargument name="fl_OrdenCompra" type="string" required="no">
	<cfargument name="id_Obra" type="string" required="yes">
	<cfargument name="id_Empresa" type="string" required="yes">
	<cfargument name="id_Proveedor" type="string" required="yes">
	<cfargument name="im_OrdenCompra" type="string" required="yes">
	<cfargument name="id_Moneda" type="string" required="yes">
	<cfargument name="fh_ContraRecibo" type="string" required="yes">
	<cfargument name="de_Factura" type="string" required="yes">
	<cfargument name="im_Subtotal" type="string" required="yes">
	<cfargument name="im_iva" type="string" required="yes">
	<cfargument name="im_Factura" type="string" required="yes">
    <cfargument name="id_Modulo" type="string" required="yes">
    <cfargument name="id_Estatus" type="string" required="yes">
    <cfargument name="id_EstatusFactura" type="string" required="yes">
    <cfargument name="cl_TipoFactura" type="string" required="yes">
    <cfargument name="de_observaciones" type="string" required="no">
    <cfargument name="id_OrdenCompra" type="string" required="no" default="NULL"> 
	<cfargument name="imp_TipoCambio" type="string" required="no" default="NULL">
	<cfargument name="id_OrdenTrabajo" type="string" required="no" default="NULL"> 
	<cfargument name="id_Estimacion" type="string" required="no" default="NULL"> 
    <cfargument name="sn_RetencionIVA" type="numeric" required="yes" default="0">
    <cfargument name="im_ISR" type="string" required="yes" default="0">
    <cfargument name="id_InventarioMov" type="string" required="no" default="">
    <cfargument name="de_ruta" type="string" required="no" default="">
	<cfargument name="im_RetencionIVA" type="string" required="yes">
    
<cfset fh_ContraRecibo = (Right(fh_ContraRecibo,4) & "-" & Mid(fh_ContraRecibo,4,2) & "-" & Left(fh_ContraRecibo,2))>
	<cfset fh_Factura = #fh_ContraRecibo#>
    
	<cfif #id_OrdenCompra# eq ''>
		<cfset id_OrdenCompra = 'null'>
	</cfif>
	
	<cfif #fl_OrdenCompra# eq ''>
		<cfset fl_OrdenCompra = 'null'>
	<cfelse>
		<cfset fl_OrdenCompra = DE(#fl_OrdenCompra#)>
	</cfif>
	
	<cfif #imp_TipoCambio# eq ''>
		<cfset imp_TipoCambio = '1'>	
	</cfif>
	<!---Estimacion--->
	<cfif #id_Estimacion# eq ''>
		<cfset id_Estimacion = 'null'>
	<cfelse>
		<cfset id_Estimacion = #id_Estimacion#>
	</cfif>
	<cfif #id_OrdenTrabajo# eq ''>
		<cfset id_OrdenTrabajo = 'null'>
	<cfelse>
		<cfset id_OrdenTrabajo = #id_OrdenTrabajo#>
	</cfif>
    
    <cfif #id_InventarioMov# eq ''>
		<cfset id_InventarioMov = 'null'>
	</cfif>
    
	
	<cftry>
   
    	<cfquery datasource='#cnx#' name="MaxId">
        	SELECT id_ContraRecibo = ISNULL(MAX(id_ContraRecibo) + 1, 1)
			FROM  ContraRecibos 
			WHERE id_Empresa = #id_Empresa#
		</cfquery>
		
       <!--- <cfoutput>
			EXECUTE ContraRecibosDetalle_Insertar  #MaxId.id_ContraRecibo#, #id_Empresa#, '#fh_ContraRecibo#', 1, #id_Proveedor#, #id_Obra#, #id_OrdenCompra#, '#de_Factura#', '#fh_Factura#', null , #id_Modulo# ,null ,null ,null,null, #im_Factura#, #id_Moneda#, 0 , null,#id_Estatus# ,null ,null ,null , #id_EstatusFactura#, null, null, null,null , #im_SubTotal#, #im_Iva#, '#de_observaciones#', #cl_TipoFactura# ,null , #fl_Ordencompra#, #im_OrdenCompra#, #imp_TipoCambio#,#id_OrdenTrabajo#,#id_Estimacion#
		</cfoutput>
		<cfabort>--->
 		<cfquery datasource='#cnx#' name='RSAgregar'>
			EXECUTE ContraRecibosDetalle_Insertar  #MaxId.id_ContraRecibo#, #id_Empresa#, '#fh_ContraRecibo#', 1, #id_Proveedor#, #id_Obra#, #id_OrdenCompra#, '#de_Factura#', '#fh_Factura#', null , #id_Modulo# ,null ,null ,null,null, #im_Factura#, #id_Moneda#, 0 , null,#id_Estatus# ,null ,null ,null , #id_EstatusFactura#, null, null, null,null , #im_SubTotal#, #im_Iva#, '#de_observaciones#', #cl_TipoFactura# ,null , #fl_Ordencompra#, #im_OrdenCompra#, #imp_TipoCambio#,#id_OrdenTrabajo#,#id_Estimacion#,#sn_RetencionIVA#,#im_ISR#,#id_InventarioMov#,'#de_ruta#', #im_RetencionIVA#
        </cfquery>
		<cf_log datasource='#cnx#' username='#user_sql#' password='#password_sql#' id_Empresa="#id_Empresa#" id_Obra="#id_Obra#" id_Usuario="#id_Proveedor#" id_MenuOpcion="1">
			<cfoutput>

				EXECUTE ContraRecibosDetalle_Insertar  #MaxId.id_ContraRecibo#, #id_Empresa#, '#fh_ContraRecibo#', 1, #id_Proveedor#, #id_Obra#, null, '#de_Factura#', '#fh_Factura#', null , #id_Modulo# ,null ,null ,null,null, #im_Factura#, #id_Moneda#, 0 , null,#id_Estatus# ,null ,null ,null , #id_EstatusFactura#, null, null, null,null , #im_SubTotal#, #im_Iva#, '#de_observaciones#', #cl_TipoFactura# ,null , #fl_Ordencompra#, #im_OrdenCompra#, #imp_TipoCambio#,#id_OrdenTrabajo#,#id_Estimacion#,#sn_RetencionIVA#,#im_ISR#,#id_InventarioMov#, '#de_ruta#', #im_RetencionIVA#
			</cfoutput>
		</cf_log>
		<cfset factura.agregado= TRUE>
		<cfset factura.tipoError= ''>
        <cfset factura.id_ContraRecibo= #Maxid.id_ContraRecibo#>
		<cfset factura.mensaje= 'Factura agregada correctamente'>
		<cfreturn factura>
		<cfcatch type='database'>
			<cfset factura.agregado= FALSE>
			<cfif cfcatch.NativeErrorCode EQ '2627'>
				<cfset factura.tipoError= 'database-registro_duplicado'>
				<cfset factura.mensaje= 'La factura no se pudo guardar porque ya existe'>
			<cfelse>
				<cfset factura.tipoError= 'database-indefinido'>
				<cfset factura.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			</cfif>
			<cfreturn factura>
		</cfcatch>
	</cftry>
</cffunction>
<!---TOTAL DE UNA ESTIMACION--->
<cffunction name="Total_EstimacionOT" access="remote" returntype="string">
	<cfargument name="id_Empresa" required="yes" type="string">
	<cfargument name="id_Obra" required="yes" type="string">
	<cfargument name="id_OrdenTrabajo" required="yes" type="string">
	<cfargument name="id_Estimacion" required="yes" type="string">
	<cfargument name="id_Proveedor" required="yes" type="string">		
	<cfif #id_Estimacion# EQ "">
		<cfreturn "">
	</cfif>
	<cftry>
		<cfquery datasource='#cnx#' name='RSEstimacion'>
			EXECUTE Total_Estimacion #id_Empresa#,#id_Obra#,#id_OrdenTrabajo#,#id_Estimacion#,#id_Proveedor#
		</cfquery>
		<cfreturn RSEstimacion.total>
		<cfcatch type='database'>
			<cfreturn "">
		</cfcatch>
	</cftry>
</cffunction>
<!---TOTAL DE UNA ESTIMACION--->
<cffunction name="Total_Estimacion" access="remote" returntype="any">
	<cfargument name="id_Empresa" required="yes" type="string">
	<cfargument name="id_Obra" required="yes" type="string">
	<cfargument name="id_OrdenTrabajo" required="yes" type="string">
	<cfargument name="id_Estimacion" required="yes" type="string">	
	<cfargument name="id_Proveedor" required="yes" type="string">		
	<cfif #id_Estimacion# EQ "">
		<cfreturn "">
	</cfif>
	<cftry>
		<cfquery datasource='#cnx#' name='RSEstimacion' username='#user_sql#' password='#password_sql#'>
			EXECUTE Total_Estimacion #id_Empresa#,#id_Obra#,#id_OrdenTrabajo#,#id_Estimacion#,#id_Provedor#
		</cfquery>
		<cfreturn RSEstimacion.total>
		<cfcatch type='database'>
			<cfreturn "">
		</cfcatch>
	</cftry>
</cffunction>
<!---La estimacion ya esta ligada a otra factura?--->
<cffunction name="Factura_Estimacion" access="remote" returntype="any">
	<cfargument name="id_Empresa" required="yes" type="string">
	<cfargument name="id_Obra" required="yes" type="string">
	<cfargument name="id_OrdenTrabajo" required="yes" type="string">
	<cfargument name="id_Estimacion" required="yes" type="string">		
	<cfif #id_Estimacion# EQ "">
		<cfreturn "">
	</cfif>
	<cftry>
		<cfquery datasource='#cnx#' name='RSFacturaEstimacion' username='#user_sql#' password='#password_sql#'>
			SELECT de_Factura
			FROM Contrarecibosdetalle
			WHERE
				id_Empresa = #id_Empresa#
				AND id_Obra = #id_Obra#
				AND id_OrdenTrabajo = #id_OrdenTrabajo#
				AND id_Estimacion = #id_Estimacion#
				and id_EstatusFactura not in (512,523)
		</cfquery>
		<cfreturn RSFacturaEstimacion.de_Factura>
		<cfcatch type='database'>
			<cfreturn "">
		</cfcatch>
	</cftry>
</cffunction>
<!---ESTIMACION AUTORIZADA?--->
<cffunction name="Estimacion_Autorizada" access="remote" returntype="any">
	<cfargument name="id_Empresa" required="yes" type="string">
	<cfargument name="id_Obra" required="yes" type="string">
	<cfargument name="id_OrdenTrabajo" required="yes" type="string">
	<cfargument name="id_Estimacion" required="yes" type="string">		
	<cfif #id_Estimacion# NEQ ''>      
		<cfquery datasource='#cnx#' name='RS' username='#user_sql#' password='#password_sql#'>
			SELECT
				id_Estimacion
			FROM
				Estimaciones 
			WHERE
				id_Empresa = #id_Empresa#
				AND id_Obra = #id_Obra#
				AND id_OrdenTrabajo = #id_OrdenTrabajo#
				AND id_Estimacion = #id_Estimacion#
				AND id_Estatus not in (310,311,312,313) 
		</cfquery>
		<cfset Regreso = #RS.id_Estimacion#>	
	<cfelse>
		<cfset Regreso = ''>
	</cfif>
	<cfreturn Regreso>    
</cffunction>
<!---carga todos los estatus--->
<!---11-12-2012--->
<!---Ricardo Avendaño--->
<cffunction name="RSMostrarTodosEstatus" access="public" returntype="query">
		<cfquery datasource='#cnx#' name='RS'>
              SELECT id_Estatus, de_Estatus FROM Estatus WHERE id_Estatus > 509 and id_Modulo = 5  ORDER BY id_Estatus                 
		</cfquery>



		<cfreturn RS>
</cffunction>

<!---Autor: Ricardo Avendaño --->
<!---13-10-2012 --->
<cffunction name="getObraPorCl_Construcion" access="remote" returntype="string">
	<cfargument name="cl_Construccion" required="no" default="" type="string">
	<cfargument name="id_Opcion" required="no" default="" type="string">
	<cfif #cl_Construccion# NEQ ''>      
		<cfquery datasource='#cnx#' name='RS' username='#user_sql#' password='#password_sql#'>
			SELECT
				TOP 1 *
			FROM
				Obras 
			WHERE
				cl_Construccion= '#cl_Construccion#'
		</cfquery>
		<cfset Regreso = #RS.de_Obra#>
		<cfif isDefined('arguments.id_Opcion') AND #arguments.id_Opcion# EQ 1>
			<cfset Regreso =  #RS.id_Obra#>
		</cfif>		
	<cfelse>
		<cfset Regreso = "">
	</cfif>
	<cfreturn Regreso>    
</cffunction>
    
<!---Ricardo Avendaño--->
<!---10-12-2012--->
<!---carga facturas contraRecibosDetalle--->
<cffunction name="RSFacturasCargar" access="public" returntype="struct">
	<cfargument name="id_Obra" type="string" required="no">
    <cfargument name="id_OrdenCompra" type="string" required="no">
	<cfargument name="de_Factura" type="string" required="no">
    <cfargument name="fh_FacturaInicial" type="string" required="no">
    <cfargument name="fh_FacturaFinal" type="string" required="no">
    <cfargument name="id_EstatusFactura" type="string" required="no">
    <cfargument name="id_Proveedor" type="string" required="yes">
    <!---<cfset fh_FacturaInicial = #DateFormat(fh_FacturaInicial,'yyyy-mm-dd')#>
    <cfset fh_FacturaFinal = #DateFormat(fh_FacturaFinal,'yyyy-mm-dd')#>--->    

	<cftry>
	
		
	
		
		<cfif #id_OrdenCompra# EQ ''>
			<cfset opc=0>

		<cfelse>	
			<cfset opc=1>
		</cfif>
		<cfif #fh_FacturaInicial# EQ "">
			<cfset fh_FacturaInicial = 'NULL'>
		<cfelse>
			<cfset fh_FacturaInicial = (Right(fh_FacturaInicial,4) & "-" & Mid(fh_FacturaInicial,4,2) & "-" & Left(fh_FacturaInicial,2))>
			<cfset fh_FacturaInicial = DE(#fh_FacturaInicial#)>
		</cfif>
		
		<cfif #fh_FacturaFinal# EQ "">
			<cfset fh_FacturaFinal = 'NULL'>
		<cfelse>
			<cfset fh_FacturaFinal = (Right(fh_FacturaFinal,4) & "-" & Mid(fh_FacturaFinal,4,2) & "-" & Left(fh_FacturaFinal,2))>
			<cfset fh_FacturaFinal = DE(#fh_FacturaFinal#)>
		</cfif>
		
		
		
		
		<cfquery name='RS'datasource='#cnx#' username='#user_sql#' password='#password_sql#'>
			
			ContraRecibos_ObtenerPorFiltros
			
			<cfif #id_Obra# NEQ "">
				#id_Obra#,		
			<cfelse>
				NULL,
			</cfif>
			<cfif #id_OrdenCompra# NEQ "">
				#id_OrdenCompra#,	
			<cfelse>
		 		NULL,
		 	</cfif>
		 	<cfif #de_Factura# NEQ "">
				 '#de_Factura#',
			<cfelse>
				NULL,
			</cfif>
			#fh_FacturaInicial#,#fh_FacturaFinal#,
			
			<cfif #id_EstatusFactura# NEQ "">
				#id_EstatusFactura#,
			<cfelse>
				NULL,
			</cfif>
			#id_Proveedor#,#opc#	


			
		</cfquery>


		<cfset ContraRecibos.listado= TRUE>
		<cfset ContraRecibos.tipoError= ''>
		<cfset ContraRecibos.rs = RS>
		<cfset ContraRecibos.mensaje= 'Se obtuvo el recordset correctamente'>
		<cfreturn ContraRecibos>
		<cfcatch type='database'>
			<cfset ContraRecibos.listado= FALSE>
			<cfset ContraRecibos.tipoError= 'database-indefinido'>
			<cfset ContraRecibos.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn ContraRecibos>
		</cfcatch>
	</cftry>
	
</cffunction>

<!---Ricardo Avendaño--->
<!---11-12-2012--->
<!---carga facturas contraRecibosDetalle--->
<cffunction name="RSListadoFacturasConsulta" access="public" returntype="struct">
    <cfargument name="de_Factura" 		 type="string"   required="no">
    <cfargument name="id_Proveedor" 	 type="string"   required="no">
    <cfargument name="fh_facturaInicial" type="string"   required="no">
    <cfargument name="fh_facturaFinal" 	 type="string"   required="no">
    <cfargument name="fh_fechaCorte" 	 type="string"   required="no">
    <cfargument name="fh_fechaPago" 	 type="string"   required="no">
    <cfargument name="fh_fechaRecepcion_Inicial" type="string"   required="no">
    <cfargument name="fh_fechaRecepcion_Final" type="string"   required="no">	
    <cfargument name="nu_semana" 		 type="string"   required="no">
    <cfargument name="id_Obra" 			 type="string"   required="no" default=""> <!---AGREGADO POR BRENDA--->
    <cfargument name="id_EncargadoEstimaciones" 		 type="string" required="no" default="">    
    <cfargument name="fh_Pago_SI" 		 type="numeric" required="no" default=0>        
    <cfargument name="cl_TipoFactura" 	 type="string"   required="no">
	
   	<cftry>
            <cfquery datasource='#cnx#' name='RSFacturas' username='#user_sql#' password='#password_sql#'>
    
                    EXECUTE ContraRecibosDetalle_ListadoConsulta
                                            <cfif #id_Proveedor# EQ "">NULL<cfelse>#id_Proveedor#</cfif>, 
                                            <cfif #de_Factura# EQ "">NULL<cfelse>'#de_Factura#'</cfif>,
                                            <cfif #fh_facturaInicial# EQ "">NULL<cfelse>'#fh_facturaInicial#'</cfif>,
                                            <cfif #fh_facturaFinal# EQ "">NULL<cfelse>'#fh_facturaFinal#'</cfif>,
                                            <cfif #fh_fechaRecepcion_Inicial# EQ "">NULL<cfelse>'#fh_fechaRecepcion_Inicial#'</cfif>,
											<cfif #fh_fechaRecepcion_Final# EQ "">NULL<cfelse>'#fh_fechaRecepcion_Final#'</cfif>,
                                            <cfif #fh_fechaCorte# EQ "">NULL<cfelse>'#fh_fechaCorte#'</cfif>,
                                            <cfif #fh_fechaPago# EQ "">NULL<cfelse>'#fh_fechaPago#'</cfif>,
                                            <cfif #nu_semana# EQ "">NULL<cfelse>#nu_semana#</cfif>,
                                            <cfif #id_Obra# EQ "">NULL<cfelse>#id_Obra#</cfif>, <!---AGREGADO POR BRENDA--->
                                            <cfif #id_EncargadoEstimaciones# EQ "">NULL<cfelse>#id_EncargadoEstimaciones#</cfif>,
											<cfif #cl_TipoFactura# EQ "">''<cfelse>#cl_TipoFactura#</cfif>
            </cfquery>
		<cfset ContraRecibos.listado= TRUE>
		<cfset ContraRecibos.tipoError= ''>
		<cfset ContraRecibos.rs= RSFacturas>
		<cfset ContraRecibos.mensaje= 'Se obtuvo el recordset de ContraRecibos correctamente'>
		<cfreturn ContraRecibos>
		<cfcatch type='database'>
			<cfset ContraRecibos.listado= FALSE>
			<cfset ContraRecibos.tipoError= 'database-indefinido'>
			<cfset ContraRecibos.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn ContraRecibos>
		</cfcatch>
	</cftry>
    
</cffunction>


<cffunction name='RSEditarProveedorPortal' access='public' returntype='struct'>
	<cfargument name='id_Proveedor' type='numeric' required='yes'>
	<cfargument name='nb_Proveedor' type='string' required='yes'>
	<cfargument name='de_Direccion' type='string' required='no' default=''>
	<cfargument name='nb_ProveedorCorto' type='string' required='no' default=''>
	<cfargument name='nu_CodigoPostal' type='numeric' required='no' default='0'>
	<cfargument name='id_Ciudad' type='numeric' required='yes'>
	<cfargument name='de_RFC' type='string' required='no' default=''>
	<cfargument name='nb_Responsable' type='string' required='no' default=''>
	<cfargument name='nu_Telefono1' type='string' required='no' default=''>
	<cfargument name='nu_Telefono2' type='string' required='no' default=''>
    
    <!---<cfargument name='nu_Capacidad' type='numeric' required='no' default='0'>--->
	<cfargument name='de_eMail' type='string' required='no' default=''>
	<cfargument name='id_Modulo' type='numeric' required='yes'>
	<cfargument name='id_TipoMovimiento' type='numeric' required='yes'>
	<cfargument name='pj_Descuento' type='numeric' required='no' default='0'>
	<cfargument name='id_Moneda' type='numeric' required='yes'>
	<cfargument name='sn_Contratista' type='boolean' required='yes'>
	<cfargument name='cl_TipoPersona' type='string' required='no' default=''>
	<cfargument name='pj_Iva' type='numeric' required='no' default='0'>
	<cfargument name='de_Password' type='string' required='no' default=''>
    <cfargument name='de_Email2' type='string' required='no' default=''>
    <cfargument name='de_Email3' type='string' required='no' default=''>
     <cfargument name='de_RegistroIMSS' type='string' required='no' default=''>
    
    <cfargument name='de_EscrituraPublica' type='string' required='no' default=''> 
    <cfargument name="fh_escriturapublica" type="string" required="no" default="">
    <cfargument name='nb_licescriturapublica' type='string' required='no' default=''>
    <cfargument name='nu_notarioescriturapublica' type='string' required='no' default=''>
    <cfargument name='nb_ejercicioescriturapublica' type='string' required='no' default=''>
    <cfargument name="nu_foliomercantilescpub" type="string" required="no" default="">
    
	<cftry>
		<cfquery datasource='#cnx#' name='RSFilasAfectadas' username='#user_sql#' password='#password_sql#'>
			EXECUTE ProveedoresPortal_Actualizar #id_Proveedor#, '#nb_Proveedor#', '#de_Direccion#','#nb_Colonia#', '#nb_ProveedorCorto#', #nu_CodigoPostal#, #id_Ciudad#, '#de_RFC#', '#nb_Responsable#', '#nu_Telefono1#', '#nu_Telefono2#', 
            <!---'#nu_Capacidad#',---> 
            '#de_eMail#', #id_Modulo#, #id_TipoMovimiento#, #id_Moneda#, #sn_Contratista#, '#cl_TipoPersona#', #pj_Iva#,'#de_Password#', '#de_eMail2#', '#de_eMail3#', '#de_RegistroIMSS#',
            <cfif #de_EscrituraPublica# EQ ''>NULL<cfelse>'#de_EscrituraPublica#'</cfif>,
			<cfif #fh_escriturapublica# EQ ''>NULL<cfelse>'#fh_escriturapublica#'</cfif>,
            <cfif #nb_licescriturapublica# EQ ''>NULL<cfelse>'#nb_licescriturapublica#'</cfif>,
            <cfif #nu_notarioescriturapublica# EQ ''>NULL<cfelse>'#nu_notarioescriturapublica#'</cfif>,
            <cfif #nb_ejercicioescriturapublica# EQ ''>NULL<cfelse>'#nb_ejercicioescriturapublica#'</cfif>,
            <cfif #nu_foliomercantilescpub# EQ ''>NULL<cfelse>'#nu_foliomercantilescpub#'</cfif>  
		</cfquery>
		<cfset Proveedores.actualizado= TRUE>
		<cfset Proveedores.rs= RSFilasAfectadas>
		<cfset Proveedores.tipoError= ''>
		<cfset Proveedores.mensaje= 'Proveedor actualizado correctamente'>
		<cfreturn Proveedores>
		<cfcatch type='database'>
			<cfset Proveedores.actualizado= FALSE>
			<cfset Proveedores.tipoError= 'database-indefinido'>
			<cfset Proveedores.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn Proveedores>
		</cfcatch>
	</cftry>
</cffunction>

<cffunction name='RSAgregarProveedorPortal' access='public' returntype='struct'>
	<cfargument name='nb_Proveedor' type='string' required='yes'>
	<cfargument name='de_Direccion' type='string' required='no' default=''>
	<cfargument name='nb_ProveedorCorto' type='string' required='no' default=''>
	<cfargument name='nu_CodigoPostal' type='numeric' required='no' default='0'>
	<cfargument name='id_Ciudad' type='numeric' required='no' default="16">
	<cfargument name='de_RFC' type='string' required='no' default=''>
	<cfargument name='nb_Responsable' type='string' required='no' default=''>
	<cfargument name='nu_Telefono1' type='string' required='no' default=''>
	<cfargument name='nu_Telefono2' type='string' required='no' default=''>
	<cfargument name='de_eMail' type='string' required='no' default=''>
	<cfargument name='id_Modulo' type='numeric' required='yes'>
	<cfargument name='id_TipoMovimiento' type='numeric' required='yes'>
	<cfargument name='pj_Descuento' type='numeric' required='no' default='0'>
	<cfargument name='id_Moneda' type='numeric' required='yes'>
	<cfargument name='sn_Contratista' type='boolean' required='yes'>
	<cfargument name='cl_TipoPersona' type='string' required='no' default=''>
	<cfargument name='pj_Iva' type='numeric' required='no' default='0'>
	<cfargument name='de_Password' type='string' required='no' default=''>
    <cfset de_RFC = REPLACE(#de_RFC#,'-','','ALL') >

	<cftry>
		<cfquery datasource='#cnx#' name='RSAgregar' username='#user_sql#' password='#password_sql#'>
			EXECUTE ProveedoresPortal_Agregar '#nb_Proveedor#', '#de_Direccion#','#nb_Colonia#', '#nb_ProveedorCorto#', #nu_CodigoPostal#, #id_Ciudad#, '#de_RFC#', '#nb_Responsable#', '#nu_Telefono1#', '#nu_Telefono2#', '#de_eMail#', #id_Modulo#, #id_TipoMovimiento#, #id_Moneda#, #sn_Contratista#, '#cl_TipoPersona#', #pj_Iva#,'#de_Password#'
		</cfquery>
		<cfset Proveedores.agregado= TRUE>
		<cfset Proveedores.tipoError= ''>
		<cfset Proveedores.mensaje= 'Proveedor guardado correctamente'>
		<cfreturn Proveedores>
		<cfcatch type='database'>
			<cfset Proveedores.agregado= FALSE>
			<cfif cfcatch.NativeErrorCode EQ '2627'>
				<cfset Proveedores.tipoError= 'database-registro_duplicado'>
				<cfset Proveedores.mensaje= 'El Proveedor no se pudo guardar porque ya existe'>
			<cfelse>
				<cfset Proveedores.tipoError= 'database-indefinido'>
				<cfset Proveedores.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			</cfif>
			<cfreturn Proveedores>
		</cfcatch>
	</cftry>
</cffunction>

<cffunction name='RSEliminarFacturas' access='public' returntype='struct'>

	<cfargument name='id_Empresa' type='numeric' required='yes'>
	<cfargument name='id_ContraRecibo' type='numeric' required='yes'>
	<cfargument name='nd_ContraRecibo' type='numeric' required='yes'>
    
    <cftry>
    	<cfquery datasource='#cnx#' name='RSFilasAfectadas' username='#user_sql#' password='#password_sql#'>
			UPDATE 
				Contrarecibosdetalle 
			SET 
				id_EstatusFactura = 512, id_OrdenCompra = NULL 
			WHERE 
				id_Empresa = #id_Empresa# AND 
				id_Contrarecibo = #id_Contrarecibo# AND 
				nd_ContraRecibo = #nd_ContraRecibo#
		</cfquery>
		<!--- <cfquery datasource='#cnx#' name='RSFilasAfectadas' username='#user_sql#' password='#password_sql#'>
			EXECUTE ContrarecibosDetalle_facturasEliminar #id_Empresa#, #id_ContraRecibo#, #nd_ContraRecibo#
		</cfquery>
		
		<cf_log datasource='#cnx#' username='#user_sql#' password='#password_sql#' id_Empresa="#id_Empresa#" id_Obra="0" id_Usuario="#Session.id_Proveedor#" id_MenuOpcion="1">
			<cfoutput>
				EXECUTE ContrarecibosDetalle_facturasEliminar #id_Empresa#, #id_ContraRecibo#, #nd_ContraRecibo#
			</cfoutput>
		</cf_log> --->
		
		<cfset facturas.eliminado= TRUE>
		<cfset facturas.tipoError= ''>
		<cfset facturas.mensaje= 'Factura eliminada correctamente'>
		<cfreturn facturas>
		<cfcatch type='database'>
			<cfset facturas.eliminado= FALSE>
			<cfif cfcatch.NativeErrorCode EQ '547'>
				<cfset facturas.mensaje= 'La Factura no se pudo eliminar ya que tiene registros relacionados'>
				<cfset facturas.tipoError= 'database-integridad'>
			<cfelse>
				<cfset facturas.mensaje= 'Error en el acceso a base de datos'>
				<cfset facturas.tipoError= 'database-indefinido'>
			</cfif>
		<cfreturn facturas>
		</cfcatch>
	</cftry>
</cffunction>


<!---Ricardo Av.--->
<!---26-03-2013--->
<!---agrega una nota de credito (portal proveedores)--->
<cffunction name="RSNotaCreditoPorProveedor" access="public" returntype="struct">
	<cfargument name="fl_OrdenCompra" type="string" required="no" default="NULL">
	<cfargument name="id_Obra" type="string" required="no" default="NULL">
	<cfargument name="id_Empresa" type="string" required="no" default="NULL">
	<cfargument name="id_Proveedor" type="string" required="no" default="NULL">
	<cfargument name="im_OrdenCompra" type="string" required="no" default="NULL">
	<cfargument name="id_Moneda" type="string" required="no" default="NULL">
	<cfargument name="fh_ContraRecibo" type="string" required="no" >
	<cfargument name="de_Factura" type="string" required="no" >
	<cfargument name="im_Subtotal" type="string" required="no" default="NULL">
	<cfargument name="im_iva" type="string" required="no" default="NULL">
	<cfargument name="im_Factura" type="string" required="no" default="NULL">
    <cfargument name="id_Modulo" type="string" required="no" default="NULL">
    <cfargument name="id_Estatus" type="string" required="no" default="NULL">
    <cfargument name="id_EstatusFactura" type="string" required="no" default="NULL">
    <cfargument name="cl_TipoFactura" type="string" required="no" default="NULL">
    <cfargument name="de_observaciones" type="string" required="no" default="NULL">
    <cfargument name="de_referencia" type="string" required="no" default="NULL">
	<cfargument name="imp_TipoCambio" type="string" required="no" default="0"> 
	<cfargument name="id_OrdenTrabajo" type="string" required="no" default="NULL"> 
	<cfargument name="id_Estimacion" type="string" required="no" default="NULL"> 
	<!---YA QUE UTILIZA EL MISMO METODO DE AGREGAR FACTURA--->
	<!---Estimacion--->
	<cfif #id_Estimacion# eq ''>
		<cfset id_Estimacion = 'null'>
	<cfelse>
		<cfset id_Estimacion = #id_Estimacion#>
	</cfif>
	<cfif #id_OrdenTrabajo# eq ''>
		<cfset id_OrdenTrabajo = 'null'>
	<cfelse>
		<cfset id_OrdenTrabajo = #id_OrdenTrabajo#>
	</cfif>
    <cfset fh_Factura = #fh_ContraRecibo#>
  	
    <cftry>
   		<!---validar que la factura exista--->
        <cfquery datasource='#cnx#' name="rs">
        	SELECT de_Factura FROM ContraRecibosDetalle WHERE 
            id_Empresa=#id_Empresa# and 
            id_Obra=#id_Obra# and 
            id_Proveedor=#id_Proveedor# and 
            de_Factura='#de_Factura#'
        </cfquery>
        
		<cfif rs.recordcount EQ 0>
           <cfset factura.agregado= FALSE>
           <cfset factura.tipoError= 'validaciones'>
           <cfset factura.mensaje= 'La nota de crédito no se pudo guardar porque la factura no existe'>
           <cfreturn factura>		
        </cfif>
        
            <cfquery datasource='#cnx#' name="MaxId">
                SELECT id_ContraRecibo = ISNULL(MAX(id_ContraRecibo) + 1, 1)
                FROM  ContraRecibos 
                WHERE id_Empresa = #id_Empresa#
            </cfquery>
            
            <cfquery datasource='#cnx#' name='RSAgregar'>
                EXECUTE ContraRecibosDetalle_Insertar  
						@id_ContraRecibo = #MaxId.id_ContraRecibo#, 
						@id_Empresa = #id_Empresa#, 
						@fh_ContraRecibo = '#fh_ContraRecibo#', 
						@nd_ContraRecibo = 1, 
						@id_Proveedor = #id_Proveedor#, 
						@id_Obra = #id_Obra#, 
						@id_OrdenCompra = null, 
						@de_Factura = '#de_Factura#', 
						@fh_Factura = '#fh_Factura#', 
						@fh_FacturaVencimiento = null , 
						@id_Modulo = #id_Modulo# ,
						@id_TipoMovimiento = null ,
						@id_CentroCosto = null ,
						@de_Concepto = null, 
						@de_Referencia = '#de_referencia#', 
						@im_Factura = #im_Factura#, 
						@id_Moneda = #id_Moneda#, 
						@im_TipoCambio = 0 , 
						@cl_FormaPago = null,
						@id_Estatus = #id_Estatus# ,
						@id_EmpleadoRegistro = null ,
					 	@id_EmpleadoBloqueo = null ,
						@id_EmpleadoAutorizo = null , 
						@id_EstatusFactura = #id_EstatusFactura#, 
						@nu_Semana = null, 
						@fh_Recepcion = null, 
						@fh_Corte = null,
						@fh_Pago = null , 
						@im_SubTotal = #im_SubTotal#, 
						@im_Iva = #im_Iva#, 
						@de_Observaciones = '#de_observaciones#', 
					 	@cl_TipoFactura = #cl_TipoFactura# ,
						@sn_CopiaFactura = NULL , 
						@fl_Ordencompra = NULL, 
						@im_OrdenCompra = #im_OrdenCompra#, 
						@imp_TipoCambio = #imp_TipoCambio#,
						@id_OrdenTrabajo = #id_OrdenTrabajo#,
						@id_Estimacion = #id_Estimacion#,
						@sn_RetencionIVA = 0,
						@im_ISR  = 0,
						@Inventario_Mov = NULL,
						@de_ruta = NULL,
						@im_RetencionIVA = 0
            </cfquery>
			
			<cf_log datasource='#cnx#' username='#user_sql#' password='#password_sql#' id_Empresa="#id_Empresa#" id_Obra="#id_Obra#" id_Usuario="#id_Proveedor#" id_MenuOpcion="1">
				<cfoutput>
					EXECUTE ContraRecibosDetalle_Insertar  
						@id_ContraRecibo = #MaxId.id_ContraRecibo#, 
						@id_Empresa = #id_Empresa#, 
						@fh_ContraRecibo = '#fh_ContraRecibo#', 
						@nd_ContraRecibo = 1, 
						@id_Proveedor = #id_Proveedor#, 
						@id_Obra = #id_Obra#, 
						@id_OrdenCompra = null, 
						@de_Factura = '#de_Factura#', 
						@fh_Factura = '#fh_Factura#', 
						@fh_FacturaVencimiento = null , 
						@id_Modulo = #id_Modulo# ,
						@id_TipoMovimiento = null ,
						@id_CentroCosto = null ,
						@de_Concepto = null, 
						@de_Referencia = '#de_referencia#', 
						@im_Factura = #im_Factura#, 
						@id_Moneda = #id_Moneda#, 
						@im_TipoCambio = 0 , 
						@cl_FormaPago = null,
						@id_Estatus = #id_Estatus# ,
						@id_EmpleadoRegistro = null ,
					 	@id_EmpleadoBloqueo = null ,
						@id_EmpleadoAutorizo = null , 
						@id_EstatusFactura = #id_EstatusFactura#, 
						@nu_Semana = null, 
						@fh_Recepcion = null, 
						@fh_Corte = null,
						@fh_Pago = null , 
						@im_SubTotal = #im_SubTotal#, 
						@im_Iva = #im_Iva#, 
						@de_Observaciones = '#de_observaciones#', 
					 	@cl_TipoFactura = #cl_TipoFactura# ,
						@sn_CopiaFactura = NULL , 
						@fl_Ordencompra = NULL, 
						@im_OrdenCompra = #im_OrdenCompra#, 
						@imp_TipoCambio = #imp_TipoCambio#,
						@id_OrdenTrabajo = #id_OrdenTrabajo#,
						@id_Estimacion = #id_Estimacion#,
						@sn_RetencionIVA = 0,
						@im_ISR  = 0,
						@Inventario_Mov = NULL,
						@de_ruta = NULL,
						@im_RetencionIVA = 0
				</cfoutput>
			</cf_log>
		<cfset factura.agregado= TRUE>
		<cfset factura.tipoError= ''>
        <cfset factura.id_ContraRecibo= #Maxid.id_ContraRecibo#>
		<cfset factura.mensaje= 'Factura agregada correctamente'>
		<cfreturn factura>
		<cfcatch type='database'>
			<cfset factura.agregado= FALSE>
			<cfif cfcatch.NativeErrorCode EQ '2627'>
				<cfset factura.tipoError= 'database-registro_duplicado'>
				<cfset factura.mensaje= 'La Nota de credito no se pudo guardar porque ya existe'>
			<cfelse>
				<cfset factura.tipoError= 'database-indefinido'>
				<cfset factura.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			</cfif>
			<cfreturn factura>
		</cfcatch>
	</cftry>
</cffunction>

 <!---Autor: Rolando López
 	  Fecha: 14/11/2014--->
      
 <!---<!---FUNCION PARA AÑADIR LA RUTA DEL PDF A LA BASE DE DATOS--->
  <cffunction name="RSfacturasAñadirPDF" access="public" returntype="struct">
	<cfargument name="id_Contrarecibo" type="string" required="yes">
    <cfargument name="id_Empresa" type="string" required="yes">
    <cfargument name="de_Factura" type="string" required="yes">
    <cfargument name="de_ruta" type="string" required="no" default="">

    <cftry>
		<cfquery datasource='#cnx#' name="RS">
			UPDATE ContrarecibosDetalle 
            SET de_rutaPDF = '#de_ruta#' 
            WHERE id_Contrarecibo = #id_Contrarecibo# AND
            	  id_Empresa = #id_Empresa# AND
                  de_Factura = '#de_Factura#'
        </cfquery>

     	<cfset PDF.agregado= TRUE>
		<cfset PDF.tipoError= ''>
		<cfset PDF.mensaje= 'PDF guardado correctamente'>
		<cfreturn PDF>
		<cfcatch type='database'>
			<cfset PDF.agregado= FALSE>
			<cfset PDF.tipoError= 'database-indefinido'>
            <cfset PDF.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<!---<cfdump var="#PDF#">--->
            <cfabort>
			<cfreturn PDF>
		</cfcatch>
	 </cftry>
  </cffunction>--->
  

<!--- Funcion para enviar cotizaciones a proveedores.(Etapa compras) --->
<!--- Autor: Kaleb Ontiveros --->
<!--- Fecha: 15/04/2013 --->
<cffunction name='agregarCotizacionProveedor' access='public' returntype='struct'>
    <cfargument name="id_Empresa" 			type="any" required="yes">
    <cfargument name="id_Obra" 				type="any" required="yes">
    <cfargument name="id_Requisicion" 		type="any" required="yes">
    <cfargument name="id_Proveedores" 		type="any" required="yes">
    <cfargument name="fh_Registro" 			type="any" required="yes">
    <cfargument name="id_UsuarioRegistro" 	type="any" required="yes">
    <cfargument name="id_InsumosCotizar" 	type="any" required="yes">
    <cfargument name="nu_Cantidades"	 	type="any" required="yes">
    <cfargument name="id_tipoRequisicion"   type="any" required="yes">
    <cfargument name="id_Marcas"	 		type="any" required="yes">
    <cfargument name="id_UsuarioComprador"  type="any" required="no">
    
	
    
    <cfset insumos		= #ListToArray(id_InsumosCotizar)# >
 	<!---<cfset cantidades= #ListToArray(nu_Cantidades)# >--->
    <!---<cfset marcas = #ListToArray(id_Marcas)# >--->
    <cfset proveedores 	= #ListToArray(id_Proveedores)# >
    
   	<!---<cfset id_InsumosCotizar = #QuotedValueList(listadoInsumoss.id_Insumo)#>--->
    
    <cfquery name="RsInsumos" datasource="#cnx#">
    	SELECT 
            r.id_Empresa,
            rd.id_Obra,
            rd.id_Insumo,
            sum(rd.nu_PorComprar) as nu_cantidad
            ,rd.id_Requisicion
        FROM 
            Requisiciones r 
        INNER JOIN RequisicionesDetalle rd 
            ON r.id_Empresa = rd.id_Empresa 
            AND r.id_Obra = rd.id_Obra 
            AND r.id_Requisicion = rd.id_Requisicion
        INNER JOIN Insumos i 
            ON i.id_Insumo = rd.id_Insumo 
        INNER JOIN TiposInsumo ti 
            ON ti.id_TipoInsumo = i.id_TipoInsumo 
        INNER JOIN GruposInsumos gi 
            ON gi.id_TipoInsumo = i.id_TipoInsumo AND gi.id_GrupoInsumo = i.id_GrupoInsumo
        INNER JOIN UnidadesMedida u 
            ON u.id_UnidadMedida = i.id_UnidadMedida 
        INNER JOIN TipoRequisicion tr 
            ON r.id_TipoRequisicion = tr.id_TipoRequisicion 
        WHERE	rd.nu_PorComprar > 0 
        AND		rd.id_Estatus <> 102 
        AND		(SELECT TOP 1 MAX(ic.sn_Activo) FROM InsumosCompradores ic WHERE ic.id_Insumo = RD.id_Insumo AND ic.id_EmpleadoComprador = #id_UsuarioRegistro# GROUP BY id_Insumo, sn_Activo) = 1 
        AND		rd.id_Requisicion = #id_Requisicion# 
        AND		rd.id_Obra = #id_Obra# 
        AND		rd.id_Empresa = #id_Empresa#
        AND		rd.id_Insumo IN ( SELECT id_Insumo FROM InsumosCompradores WHERE id_EmpleadoComprador = #id_UsuarioComprador# ) 
        AND		sn_ComprarPlaza = 0 AND i.id_GrupoInsumo = i.id_GrupoInsumo 
        GROUP BY 
            r.id_Empresa,
            rd.id_Obra,
            rd.id_Insumo,
            rd.id_Requisicion
    </cfquery>

    <cftransaction>
    	<cftry>
            <cfloop array="#proveedores#" index="ind">
                <cfquery name="RsInsumosProveedor" datasource="#cnx#">
            
                    SELECT 
                        rd.id_Insumo
                    FROM
                        RequisicionesDetalle rd
                    INNER JOIN Insumos i 
                        ON rd.id_Insumo = i.id_Insumo
                    INNER JOIN UnidadesMedida u 
                        ON u.id_UnidadMedida = i.id_UnidadMedida
                    INNER JOIN InsumosProveedores ip 
                        ON rd.id_Insumo = ip.id_Insumo
                    WHERE	rd.id_Empresa = #id_Empresa#
                    AND		rd.id_Obra = #id_Obra#
                    AND		rd.id_Requisicion = #id_Requisicion#
                    AND		rd.id_Insumo IN (#QuotedValueList(Session.RsInsumosCotizar.id_Insumo)#)
                    AND		ip.id_Proveedor = #ind#
					AND 	ip.sn_Activo = 1
                    GROUP BY rd.id_Insumo, i.de_Insumo, u.de_UnidadMedida
                
                </cfquery>                                     
                <cfif RsInsumosProveedor.RecordCount GT 0>
                    <cfquery name="RS" datasource="#cnx#">
                        SELECT 
                            ISNULL(MAX(id_Cotizacion) + 1, 1)  as id_Cotizacion
                        FROM 
                            RequisicionesCotizaciones	
                        WHERE 
                            id_Empresa = #id_Empresa# AND id_Obra = #id_Obra# AND id_Requisicion = #id_Requisicion# AND id_Proveedor = #ind#
                    </cfquery>
                    
                    <cfquery name="rsComentarios" dbtype="query">
                        SELECT de_comentario FROM arguments.de_comentarios WHERE id_Proveedor = #ind#
                    </cfquery>
                     
                    <cfif rsComentarios.de_comentario EQ "">
                        <cfset de_comentario = ""> 
                    <cfelse>
                        <cfset de_comentario = #rsComentarios.de_comentario# > 
                    </cfif>
                    
                    <cfstoredproc procedure="RequisicionesCotizacionesAgregarProveedor" datasource="#cnx#" >
                        <cfprocparam type="IN" cfsqltype="CF_SQL_INTEGER" 	dbvarname="@id_Empresa" 		value="#arguments.id_Empresa#">
                        <cfprocparam type="IN" cfsqltype="CF_SQL_INTEGER" 	dbvarname="@id_Obra" 			value="#arguments.id_Obra#">
                        <cfprocparam type="IN" cfsqltype="CF_SQL_INTEGER" 	dbvarname="@id_Requisicion"		value="#arguments.id_Requisicion#">
                        <cfprocparam type="IN" cfsqltype="CF_SQL_INTEGER" 	dbvarname="@id_Cotizacion" 		value="#RS.id_Cotizacion#">                    
                        <cfprocparam type="IN" cfsqltype="CF_SQL_INTEGER" 	dbvarname="@id_Proveedor" 		value="#ind#">
                        <cfprocparam type="IN" cfsqltype="CF_SQL_TIMESTAMP" dbvarname="@fh_Registro" 		value="#arguments.fh_Registro#">
                        <cfprocparam type="IN" cfsqltype="CF_SQL_INTEGER" 	dbvarname="@id_UsuarioRegistro" value="#arguments.id_UsuarioRegistro#">
                        <cfprocparam type="IN" cfsqltype="CF_SQL_VARCHAR" 	dbvarname="@id_Insumo" 			null="yes">
                        <cfprocparam type="IN" cfsqltype="CF_SQL_FLOAT" 	dbvarname="@nu_Cantidad" 		null="yes">
                        <cfprocparam type="IN" cfsqltype="CF_SQL_INTEGER" 	dbvarname="@im_PrecioProveedor" null="yes">
                        <cfprocparam type="IN" cfsqltype="CF_SQL_INTEGER" 	dbvarname="@id_Marca" 			null="yes">
                        <cfprocparam type="IN" cfsqltype="CF_SQL_TIMESTAMP" dbvarname="@fh_Respuesta" 		null="yes">
                        <cfprocparam type="IN" cfsqltype="CF_SQL_TIMESTAMP" dbvarname="@fh_vigencia" 		null="yes">
                        <cfprocparam type="IN" cfsqltype="CF_SQL_VARCHAR" 	dbvarname="@de_Observaciones" 	value="#de_comentario#">
                        <cfprocparam type="IN" cfsqltype="CF_SQL_BIT" 		dbvarname="@Opcion" 			value="1">
                        <cfprocparam type="IN" cfsqltype="CF_SQL_INTEGER"   dbvarname="@id_TipoRequisicion" value="#arguments.id_tipoRequisicion#">
                        <cfprocparam type="IN" cfsqltype="CF_SQL_BIT" 		dbvarname="@sn_Aceptado" 		value="0">
                        <cfprocparam type="IN" cfsqltype="CF_SQL_TIMESTAMP" dbvarname="@fh_Aceptado"        null="yes">
                    </cfstoredproc>
                                      
                    <cfquery name="RsInsumosCotizar" dbtype="query">
                        SELECT id_Insumo, SUM( nu_Cantidad ) AS nu_Cantidad 
                        FROM RsInsumos WHERE id_Insumo IN (#QuotedValueList(Session.RsInsumosCotizar.id_Insumo)#) GROUP BY id_Insumo
                    </cfquery>
                    

                    <cfloop query="RsInsumosCotizar">
                    
                    	<cfquery name="marcaInsumo" dbtype="query">
                        	select id_marca from id_marcas
                            where id_Insumo = '#RsInsumosCotizar.id_Insumo#'
                        </cfquery>
                        
                        <cfstoredproc procedure="RequisicionesCotizacionesAgregarProveedor" datasource="#cnx#">        
                            <cfprocparam type="IN" cfsqltype="CF_SQL_INTEGER" 	dbvarname="@id_Empresa" 		value="#arguments.id_Empresa#">
                            <cfprocparam type="IN" cfsqltype="CF_SQL_INTEGER" 	dbvarname="@id_Obra" 			value="#arguments.id_Obra#">
                            <cfprocparam type="IN" cfsqltype="CF_SQL_INTEGER" 	dbvarname="@id_Requisicion" 	value="#arguments.id_Requisicion#">
                            <cfprocparam type="IN" cfsqltype="CF_SQL_INTEGER" 	dbvarname="@id_Cotizacion" 		value="#RS.id_Cotizacion#">                        
                            <cfprocparam type="IN" cfsqltype="CF_SQL_INTEGER" 	dbvarname="@id_Proveedor" 		value="#ind#">
                            <cfprocparam type="IN" cfsqltype="CF_SQL_TIMESTAMP" dbvarname="@fh_Registro" 		value="#arguments.fh_Registro#">
                            <cfprocparam type="IN" cfsqltype="CF_SQL_INTEGER" 	dbvarname="@id_UsuarioRegistro" value="#arguments.id_UsuarioRegistro#">
                            <cfprocparam type="IN" cfsqltype="CF_SQL_VARCHAR" 	dbvarname="@id_Insumo" 			value="#RsInsumosCotizar.id_Insumo#">
                            <cfprocparam type="IN" cfsqltype="CF_SQL_FLOAT" 	dbvarname="@nu_Cantidad" 		value="#RsInsumosCotizar.nu_Cantidad#">
                            <cfprocparam type="IN" cfsqltype="CF_SQL_INTEGER" 	dbvarname="@im_PrecioProveedor" null="yes">
                            <cfprocparam type="IN" cfsqltype="CF_SQL_INTEGER" 	dbvarname="@id_Marca" 			value="#marcaInsumo.id_marca#">
                            <cfprocparam type="IN" cfsqltype="CF_SQL_TIMESTAMP" dbvarname="@fh_Respuesta" 		null="yes">
                            <cfprocparam type="IN" cfsqltype="CF_SQL_TIMESTAMP" dbvarname="@fh_vigencia" 		null="yes">
                            <cfprocparam type="IN" cfsqltype="CF_SQL_VARCHAR" 	dbvarname="@de_Observaciones" 	null="yes">
                            <cfprocparam type="IN" cfsqltype="CF_SQL_BIT" 		dbvarname="@Opcion" 			value="0">
                            <cfprocparam type="IN" cfsqltype="CF_SQL_INTEGER"   dbvarname="@id_tipoRequisicion" value="#arguments.id_tipoRequisicion#">
                            <cfprocparam type="IN" cfsqltype="CF_SQL_BIT"       dbvarname="@sn_Aceptado"        value="0">
                            <cfprocparam type="IN" cfsqltype="CF_SQL_TIMESTAMP" dbvarname="@fh_Aceptado"        null="yes">
                        </cfstoredproc>          
					</cfloop>                                                               
                </cfif>                    
            </cfloop>
            <cfset Cotizacion.agregada= TRUE>
            <cfset Cotizacion.tipoError= ''>
            <cfset Cotizacion.mensaje= 'Se envio correctamente'>
            <cfreturn Cotizacion>
            <cfcatch type='database'>
                <cftransaction action="rollback">
                <cfset Cotizacion.agregado= FALSE>
                    <cfset Cotizacion.tipoError= 'database-indefinido'>
                    
                    <cfset Cotizacion.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
                <cfreturn Cotizacion>
            </cfcatch>
    	</cftry>
    </cftransaction>
     
</cffunction>


<!--- Funcion para obtener el email de un proveedor mediante el ID o RFC, usado mayormente en envio de correos (Etapa compras)--->
<!--- Autor: Kaleb Ontiveros --->
<!--- Fecha: 27/04/2013 --->
<cffunction name="getProveedorEmail" access="remote" returntype="struct">
	<cfargument name="id_Proveedor" type="string" required="no" default="">
    <cfargument name="de_RFC" 		type="string" required="no" default="">
    
    <cftransaction>
		<cftry>
            <cfstoredproc procedure="Proveedores_ObtenerEmail" datasource="#cnx#" username='#user_sql#' password='#password_sql#'>
				<cfprocparam type="IN" cfsqltype="CF_SQL_INTEGER" dbvarname="@id_Proveedor" value="#arguments.id_Proveedor#" null="#iif(isNumeric(arguments.id_Proveedor),false,true)#">
				<cfprocparam type="IN" cfsqltype="CF_SQL_VARCHAR" dbvarname="@de_RFC" value="#arguments.de_RFC#" null="#iif(len(arguments.de_RFC),false,true)#">
                <cfprocresult name="RSEmail" resultset="1">
            </cfstoredproc>
            
			<cfset Proveedores.listado = TRUE>
            <cfset Proveedores.rs = #RSEmail#> 
            <cfset Proveedores.tipoError = ''>
            <cfset Proveedores.mensaje= 'Se obtuvo el RecordSet correctamente'>
            <cfreturn Proveedores>
            <cfcatch type='database'>
                <cftransaction action="rollback">
                <cfset Proveedores.listado= FALSE>
                    <cfset Proveedores.tipoError= 'database-indefinido'>
                    <cfset Proveedores.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
                <cfreturn Proveedores>
            </cfcatch>
        </cftry>
    </cftransaction>
</cffunction>

<!--- Funcion para obtener los datos de una obra--->
<!--- Autor: Brenda Machado --->
<!--- Fecha: 29/08/2013 --->
<cffunction name="RSDetallesDatosObra" access="remote" returntype="struct">
    <cfargument name="id_Obra" type="numeric" required="yes">
	
   	<cftry>
    	<cfquery datasource='#cnx#' name='RSDatosObra'>
            	DetallesDatosObra
					<cfif #id_Obra# EQ "">
                    	0
					<cfelse>
                    	#id_Obra#
					</cfif>                                       
	  	</cfquery>

		<cfset DatosObra.listado= TRUE>
		<cfset DatosObra.tipoError= ''>
		<cfset DatosObra.rs= RSDatosObra>
		<cfset DatosObra.mensaje= 'Se obtuvo el recordset de Datos de la Obra correctamente'>
		<cfreturn DatosObra>
		<cfcatch type='database'>
			<cfset DatosObra.listado= FALSE>
			<cfset DatosObra.tipoError= 'database-indefinido'>
			<cfset DatosObra.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn DatosObra>
		</cfcatch>
	</cftry>    
</cffunction>

<!--- Funcion para obtener acumulados de TI--->
<!--- Autor: Brenda Machado --->
<!--- Fecha: 30/08/2013 --->
<cffunction name="ReporteTablaInversion" access="public" returntype="struct">
    <cfargument name="fh_Pago" type="numeric" required="yes">
	
   	<cftry>
    	<cfquery datasource='#cnx#' name='RSListado'>
            	Proveedores_Acumulados_TI '#fh_Pago#'                                     
	  	</cfquery>

		<cfset AcumuladoTI.listado= TRUE>
		<cfset AcumuladoTI.tipoError= ''>
		<cfset AcumuladoTI.rs= RSListado>
		<cfset AcumuladoTI.mensaje= 'Se obtuvo el recordset de Acumulados TI correctamente'>
		<cfreturn AcumuladoTI>
		<cfcatch type='database'>
			<cfset AcumuladoTI.listado= FALSE>
			<cfset AcumuladoTI.tipoError= 'database-indefinido'>
			<cfset AcumuladoTI.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn AcumuladoTI>
		</cfcatch>
	</cftry>    
</cffunction>

<!----------Función para agregar el anexo de los documentos de la desición 14-A------------->
<!----------Desarrollado por: Jaime Bustamante----------->
<!----------Fecha: 30/agosto/2013------------------------>
<cffunction name="RSAgregarAnexoD14A" access="public" returntype="struct">
	<cfargument name="id_proveedor" 			type="numeric"	  required="yes">
    <cfargument name="ar_checkList" 			type="string" required="yes">
    <cfargument name="ar_comercialRazonSocial"  type="string" required="yes">
    <cfargument name="ar_compDomicilioFiscal" 	type="string" required="yes">
    <cfargument name="ar_altaHacienda" 			type="string" required="yes">
    <cfargument name="ar_rfc" 					type="string" required="yes">
    <cfargument name="ar_actaConstitutiva" 		type="string" required="no" >
    <cfargument name="ar_credencialElector" 	type="string" required="yes">
    <cfargument name="ar_poderNotarial" 		type="string" required="no" >
    <cfargument name="ar_declaracionAnual" 		type="string" required="yes">
    <cfargument name="ar_altaIMSSS" 			type="string" required="yes">
    <cfargument name="ar_informacionFinanciera" type="string" required="yes">
    <cfargument name="ar_formatoVisitaProv" 	type="string" required="yes">
    <cfargument name="ar_formatoDC3" 			type="string" required="yes">
    
    <cftry>
    	<cfquery datasource='#cnx#' name='RSAgregar' username='#user_sql#' password='#password_sql#'>
        	Exec Proveedores_ArchivosD14A #Arguments.id_proveedor#, '#Arguments.ar_checkList#', '#Arguments.ar_comercialRazonSocial#', '#Arguments.ar_compDomicilioFiscal#', '#Arguments.ar_altaHacienda#', '#Arguments.ar_rfc#',
            <cfif 'Arguments.ar_actaConstitutiva' NEQ "">
            	'#Arguments.ar_actaConstitutiva#'
            	<cfelse> NULL
            </cfif>,
            '#Arguments.ar_credencialElector#',
            <cfif 'Arguments.ar_poderNotarial' NEQ "">
            	'#Arguments.ar_poderNotarial#'
                <cfelse> NULL
            </cfif>,
            '#Arguments.ar_declaracionAnual#', '#Arguments.ar_altaIMSSS#', '#Arguments.ar_informacionFinanciera#', '#Arguments.ar_formatoVisitaProv#', '#Arguments.ar_formatoDC3#'
		</cfquery>
        <cfset ProveedoresD14A.agregado= TRUE>
		<cfset ProveedoresD14A.tipoError= ''>
		<cfset ProveedoresD14A.mensaje= 'Archivos guardados correctamente'>
		<cfreturn ProveedoresD14A>
		<cfcatch type='database'>
			<cfset ProveedoresD14A.agregado= FALSE>
			<cfif cfcatch.NativeErrorCode EQ '2627'>
				<cfset ProveedoresD14A.tipoError= 'database-registro_duplicado'>
				<cfset ProveedoresD14A.mensaje= 'Los archivos no se pudieron guardar porque ya existe un registro para este art&iacute;culo'>
			<cfelse>
				<cfset ProveedoresD14A.tipoError= 'database-indefinido'>
				<cfset ProveedoresD14A.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			</cfif>
			<cfreturn ProveedoresD14A>
		</cfcatch>
	</cftry>
</cffunction>

<!-------------Función para obtener los archivos adjuntos de un proveedor-------------->
<!-------------Desarrollado por: Jaime Bustamante----------->
<!-------------Fecha: 2/septiembre/2013--------------------->
<cffunction name="RSListadoArchivos" access="public" returntype="struct">
	<cfargument name="id_proveedor" type="numeric" required="yes">
    
    

    
    <cfquery name="nd_Archivo" datasource="#session.cnx#">
    	    SELECT MAX(nd_archivo) as nd_Archivo FROM Proveedores_anexosD14A 
            WHERE id_proveedor = #id_proveedor#
    </cfquery>
	
    <cfif #nd_Archivo.nd_Archivo# EQ ''>
		<CFSET nd_Archivo.nd_Archivo = 1>
	</cfif>
    
    <cftry>
        <cfquery datasource='#cnx#' name='RSListado' username='#user_sql#' password='#password_sql#'>
            Exec Proveedores_ObtenerArchivosD14 #id_proveedor#, #nd_Archivo.nd_Archivo#
        </cfquery>
        

        <cfset archivosD14.consultado= TRUE>
        <cfset archivosD14.tipoError=''>
        <cfset archivosD14.rs=RSListado>
        <cfreturn archivosD14>
        <cfcatch type="database">
        	<cfset archivosD14.consultado= FALSE>
        	<cfset archivosD14.tipoError='database-indefinido'>
            <cfreturn archivosD14>
        </cfcatch>
    </cftry>
</cffunction>

<!----------Función para actualizar el anexo de los documentos de la desición 14-A------------->
<!----------Desarrollado por: Jaime Bustamante----------->
<!----------Fecha: 2/septiembre/2013------------------------>
<cffunction name="RSActualizarAnexoD14A" access="public" returntype="struct">
	<cfargument name="id_proveedor" 			type="numeric"required="yes">
    <cfargument name="ar_checkList" 			type="string" required="yes">
    <cfargument name="ar_comercialRazonSocial"  type="string" required="yes">
    <cfargument name="ar_compDomicilioFiscal" 	type="string" required="yes">
    <cfargument name="ar_altaHacienda" 			type="string" required="yes">
    <cfargument name="ar_rfc" 					type="string" required="yes">
    <cfargument name="ar_actaConstitutiva" 		type="string" required="no" >
    <cfargument name="ar_credencialElector" 	type="string" required="yes">
    <cfargument name="ar_poderNotarial" 		type="string" required="no" >
    <cfargument name="ar_declaracionAnual" 		type="string" required="yes">
    <cfargument name="ar_altaIMSSS" 			type="string" required="yes">
    <cfargument name="ar_informacionFinanciera" type="string" required="yes">
    <cfargument name="ar_formatoVisitaProv" 	type="string" required="yes">
    <cfargument name="ar_formatoDC3" 			type="string" required="yes">
    
    <cftry>
    	<cfquery datasource='#cnx#' name='RSActualizar' username='#user_sql#' password='#password_sql#'>
        	Exec Proveedores_ActualizarAnexosD14A #Arguments.id_proveedor#, '#Arguments.ar_checkList#', '#Arguments.ar_comercialRazonSocial#', '#Arguments.ar_compDomicilioFiscal#', '#Arguments.ar_altaHacienda#', '#Arguments.ar_rfc#',
            <cfif 'Arguments.ar_actaConstitutiva' NEQ "">
            	'#Arguments.ar_actaConstitutiva#'
            	<cfelse> NULL
            </cfif>,
            '#Arguments.ar_credencialElector#',
            <cfif 'Arguments.ar_poderNotarial' NEQ "">
            	'#Arguments.ar_poderNotarial#'
                <cfelse> NULL
            </cfif>,
            '#Arguments.ar_declaracionAnual#', '#Arguments.ar_altaIMSSS#', '#Arguments.ar_informacionFinanciera#', '#Arguments.ar_formatoVisitaProv#', '#Arguments.ar_formatoDC3#'
		</cfquery>
        <cfset ProveedoresD14A.actualizado= TRUE>
		<cfset ProveedoresD14A.tipoError= ''>
		<cfset ProveedoresD14A.mensaje= 'Archivos guardados correctamente'>
		<cfreturn ProveedoresD14A>
		<cfcatch type='database'>
			<cfset ProveedoresD14A.actualizado= FALSE>
			<cfif cfcatch.NativeErrorCode EQ '2627'>
				<cfset ProveedoresD14A.tipoError= 'database-registro_duplicado'>
				<cfset ProveedoresD14A.mensaje= 'Los archivos no se pudieron actualizar'>
			<cfelse>
				<cfset ProveedoresD14A.tipoError= 'database-indefinido'>
				<cfset ProveedoresD14A.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			</cfif>
			<cfreturn ProveedoresD14A>
		</cfcatch>
	</cftry>
</cffunction>

<!--- Funcion para obtener Estatus de la Cuenta--->
<!--- Autor: Brenda Machado --->
<!--- Fecha: 02/09/2013 --->
<cffunction name="TipoCuenta" access="remote" returntype="string">
    <cfargument name="id_Obra" type="string" required="yes">
	
    <cfif #id_Obra# neq ''>
   	<cftry>
    	<cfquery datasource='#cnx#' name='RSListado'>
            	bop_Facturas_TipoCuenta #id_Obra#
	  	</cfquery>

		<cfset TipoCuentas.listado= TRUE>
		<cfset TipoCuentas.tipoError= ''>
		<cfset TipoCuentas.rs= RSListado>
		<cfset TipoCuentas.mensaje= 'Se obtuvo el recordset de Acumulados TI correctamente'>
		<cfreturn #RSListado.de_Cuenta#>
		<cfcatch type='database'>
			<cfset TipoCuentas.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn TipoCuentas.mensaje>
		</cfcatch>
	</cftry>  
    <cfelse>
        <cfset TipoCuentas.mensaje= ''>
        <cfreturn TipoCuentas.mensaje>
    </cfif>  
</cffunction>

<!--- Funcion para obtener Encargado de la Obra--->
<!--- Autor: Brenda Machado --->
<!--- Fecha: 02/09/2013 --->
<cffunction name="EncargadoObra" access="remote" returntype="string">
    <cfargument name="id_Obra" type="string" required="no">
	<cfif #id_Obra# neq ''>
   	<cftry>
    	<cfquery datasource='#cnx#' name='RSListado'>
            	bop_DetallesEncargadoObra #id_Obra#
	  	</cfquery>

		<cfset Encargado.listado= TRUE>
		<cfset Encargado.tipoError= ''>
		<cfset Encargado.rs= RSListado>
		<cfset Encargado.mensaje= 'Se obtuvo el recordset de Acumulados TI correctamente'>
		<cfreturn #RSListado.encargadoObra#>
		<cfcatch type='database'>
			<cfset Encargado.listado= FALSE>
			<cfset Encargado.tipoError= 'database-indefinido'>
			<cfset Encargado.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn Encargado>
		</cfcatch>
	</cftry>   
    <cfelse>
    	<cfset Encargado = ''>
        <cfreturn Encargado>
    </cfif> 
</cffunction>

<!----------Función para obtener nombre del proveedor por su id------------->
<!----------Desarrollado por: Jaime Bustamante----------->
<!----------Fecha: 4/septiembre/2013--------------------->
<cffunction name="obtenerProveedorPorID" access="remote" returntype="string">
	<cfargument name="id_Proveedor" required="yes">
	<cfif #id_Proveedor# NEQ "" AND isNumeric(#id_Proveedor#)>
		<cfquery name="RS" datasource="#cnx#" username='#user_sql#' password='#password_sql#'>
			SELECT
				nb_Proveedor
			FROM
				Proveedores 
			WHERE
				id_Proveedor= #id_Proveedor#
		</cfquery>
		<cfset Prov= RS.nb_Proveedor>
	<cfelse>
		<cfset Prov="">
	</cfif>
 	<cfreturn Prov>
</cffunction>

<!---Funcion que devuelve proveedores de tipo contratista por Especialidad sugeridos para combo--->
<cffunction name="getContratistasEspecialidad" access="remote" returntype="query">
	<cfargument name="str_Proveedor" type="string" required="yes">
    <cfargument name="id_Especialidad" type="string" required="yes">
	
	<cfif isNumeric(#str_Proveedor#)>
    	<cfset id_Proveedor= #str_Proveedor#>
        <cfif #id_Proveedor# EQ 0>
			<cfset id_Proveedor= "NULL">
		</cfif>
		<cfset nb_ProveedorCorto= "NULL">
	<cfelse>
		<cfset id_Proveedor= "NULL">
		<cfif #str_Proveedor# NEQ "">
			<cfset nb_ProveedorCorto= DE("#str_Proveedor#")>
		<cfelse>
			<cfset nb_ProveedorCorto= "NULL">
		</cfif>
	</cfif>
    
    <cfif #id_Especialidad# EQ ''>
    	<cfset id_Especialidad = "NULL"> 
	</cfif>
    
	<cfquery name="RSProveedores" datasource="#cnx#" username="#user_sql#" password="#password_sql#">
		EXECUTE Proveedores_ObtenContratistaSugeridos #id_Proveedor#, #nb_ProveedorCorto#, #id_Especialidad#
	</cfquery>
	<cfif #str_Proveedor# EQ "">
		<cfset RSTodos= QueryNew("id_Proveedor,nb_ProveedorCorto")>
		<cfset QueryAddRow(RSTodos,1)>
		<cfset QuerySetCell(RSTodos,"id_Proveedor","",1)>
		<cfset QuerySetCell(RSTodos,"nb_ProveedorCorto","SELECCIONE UN CONTRATISTA",1)>
		<cfinvoke component="#Application.componentes#.funciones" method="JuntarQuerys" queryUno="#RSTodos#" queryDos="#RSProveedores#">
		<cfreturn RSTodos>
	<cfelse>
		<cfreturn RSProveedores>
	</cfif>
</cffunction>

<cffunction name='RSAgregarProveedorPortalDos' access='public' returntype='struct'>
	<cfargument name='id_Proveedor' type='numeric' required='yes'>
    <cfargument name='nb_Proveedor' type='string' required='yes'>
	<cfargument name='de_Direccion' type='string' required='no' default=''>
    <cfargument name='nb_Colonia' type='string' required='no' default=''>
	<cfargument name='nb_ProveedorCorto' type='string' required='no' default=''>
	<cfargument name='nu_CodigoPostal' type='numeric' required='no' default='0'>
	<cfargument name='id_Ciudad' type='numeric' required='no' default="16">
	<cfargument name='de_RFC' type='string' required='no' default=''>
	<cfargument name='nb_Responsable' type='string' required='no' default=''>
	<cfargument name='nu_Telefono1' type='string' required='no' default=''>
	<cfargument name='nu_Telefono2' type='string' required='no' default=''>
    
    <!---<cfargument name='nu_Capacidad' type='numeric' required='no' default='0'>--->
	<cfargument name='de_eMail' type='string' required='no' default=''>
	<cfargument name='id_Modulo' type='numeric' required='yes'>
	<cfargument name='id_TipoMovimiento' type='numeric' required='yes'>
	<!---<cfargument name='pj_Descuento' type='numeric' required='no' default='0'>--->
	<cfargument name='id_Moneda' type='numeric' required='yes'>
	<cfargument name='sn_Contratista' type='boolean' required='yes'>
	<cfargument name='cl_TipoPersona' type='string' required='no' default=''>
	<cfargument name='pj_Iva' type='numeric' required='no' default='0'>
	<cfargument name='de_Password' type='string' required='no' default=''>
    <cfargument name='de_eMail2' type='string' required='no' default=''>
    <cfargument name='de_eMail3' type='string' required='no' default=''>
    <cfargument name='de_RegistroIMSS' type='string' required='no' default=''>
    
    <cfargument name='de_EscrituraPublica' type='string' required='no' default=''>
    <cfargument name="fh_escriturapublica" type="string" required="no" default="">
    <cfargument name='nb_licescriturapublica' type='string' required='no' default=''>
    <cfargument name='nu_notarioescriturapublica' type='string' required='no' default=''>
    <cfargument name='nb_ejercicioescriturapublica' type='string' required='no' default=''>
    <cfargument name="nu_foliomercantilescpub" type="string" required="no" default="">
    
    <cfset fh_Alta = dateFormat(Now(),'yyyy-mm-dd') >
    <cfset de_RFC = REPLACE(#de_RFC#,'-','','ALL') >

	<cftry>
		<cfquery datasource='#cnx#' name='RSAgregar' username='#user_sql#' password='#password_sql#'>
			EXECUTE ProveedoresPortal_AgregarDos #id_Proveedor#, '#nb_Proveedor#', '#de_Direccion#','#nb_Colonia#', '#nb_ProveedorCorto#', #nu_CodigoPostal#, #id_Ciudad#, '#de_RFC#', '#nb_Responsable#', '#nu_Telefono1#', '#nu_Telefono2#', 
            <!---'#nu_Capacidad#',---> 
            '#de_eMail#', #id_Modulo#, #id_TipoMovimiento#, #id_Moneda#, #sn_Contratista#, '#cl_TipoPersona#', #pj_Iva#,'#de_Password#', '#de_eMail2#','#de_eMail3#', '#de_RegistroIMSS#',1, '#fh_Alta#',
             <cfif #de_EscrituraPublica# EQ ''>NULL<cfelse>'#de_EscrituraPublica#'</cfif>,
			 <cfif #fh_escriturapublica# EQ ''>NULL<cfelse>'#fh_escriturapublica#'</cfif>,
             <cfif #nb_licescriturapublica# EQ ''>NULL<cfelse>'#nb_licescriturapublica#'</cfif>,
             <cfif #nu_notarioescriturapublica# EQ ''>NULL<cfelse>'#nu_notarioescriturapublica#'</cfif>,
             <cfif #nb_ejercicioescriturapublica# EQ ''>NULL<cfelse>'#nb_ejercicioescriturapublica#'</cfif>,
             <cfif #nu_foliomercantilescpub# EQ ''>NULL<cfelse>'#nu_foliomercantilescpub#'</cfif>                
		</cfquery>
		<cfset Proveedores.agregado= TRUE>
		<cfset Proveedores.tipoError= ''>
		<cfset Proveedores.mensaje= 'Proveedor guardado correctamente'>
		<cfreturn Proveedores>
		<cfcatch type='database'>
			<cfset Proveedores.agregado= FALSE>
			<cfif cfcatch.NativeErrorCode EQ '2627'>
				<cfset Proveedores.tipoError= 'database-registro_duplicado'>
				<cfset Proveedores.mensaje= 'El Proveedor no se pudo guardar porque ya existe'>
			<cfelse>
				<cfset Proveedores.tipoError= 'database-indefinido'>
				<cfset Proveedores.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			</cfif>
			<cfreturn Proveedores>
		</cfcatch>
	</cftry>
</cffunction>

<!---Existe la factura que quiere agregar el proveedor--->
<cffunction name="ExisteFactura" access="remote" returntype="any">
	<cfargument name="id_Empresa" required="yes">
	<cfargument name="id_Obra" required="yes">
	<cfargument name="id_Proveedor" required="yes">	
	<cfargument name="folio" type="string">

	<cfif #id_Obra# EQ "" OR #id_Empresa# EQ "" OR #id_Proveedor# EQ "" OR #folio# EQ "">
		<cfreturn "">
	</cfif>
	<cftry>
		<cfquery datasource='#cnx#' name='RSExisteFactura' username='#user_sql#' password='#password_sql#'>
			SELECT de_Factura
			FROM Contrarecibosdetalle
			WHERE
				id_Empresa = #id_Empresa# AND
				id_Obra = #id_Obra# AND
				id_Proveedor = #id_Proveedor# AND
				de_Factura = '#folio#' 
                and id_EstatusFactura not in (512,523)
		</cfquery>
		<cfreturn RSExisteFactura.de_Factura>
		<cfcatch type='database'>
			<cfreturn "">
		</cfcatch>
	</cftry>
</cffunction>

<cffunction name='agregarCotizacionProveedorAdmin' access='public' returntype='struct'>
    <cfargument name="id_Empresa" 			type="any" required="yes">
    <cfargument name="id_Obra" 				type="any" required="yes">
    <cfargument name="id_Requisicion" 		type="any" required="yes">
    <cfargument name="id_Proveedores" 		type="any" required="yes">
    <cfargument name="fh_Registro" 			type="any" required="yes">
    <cfargument name="id_UsuarioRegistro" 	type="any" required="yes">
    <cfargument name="id_InsumosCotizar" 	type="any" required="yes">
    <cfargument name="nu_Cantidades"	 	type="any" required="yes">    
    <cfargument name="id_Marcas"	 		type="any" required="yes">
    <cfargument name="id_UsuarioComprador"  type="any" required="no">
    <cfargument name="id_TipoRequisicion"	type="any" required="yes">
    
    
    <cfset insumos		= #ListToArray(id_InsumosCotizar)# >
    <cfset cantidades	= #ListToArray(nu_Cantidades)# >
    <cfset marcas	 	= #ListToArray(id_Marcas)# >
    <cfset proveedores 	= #ListToArray(id_Proveedores)# >
   
    
    <cfquery name="RsInsumos" datasource="#cnx#">
    	SELECT 
            r.id_Empresa,
            rd.id_Obra,
            rd.id_Insumo,
            sum(rd.nu_PorComprar) as nu_cantidad,
            rd.id_Requisicion
        FROM 
            Requisiciones r 
        INNER JOIN RequisicionesDetalle rd 
            ON r.id_Empresa = rd.id_Empresa 
            AND r.id_Obra = rd.id_Obra 
            AND r.id_Requisicion = rd.id_Requisicion
        INNER JOIN Insumos i 
            ON i.id_Insumo = rd.id_Insumo 
        INNER JOIN TiposInsumo ti 
            ON ti.id_TipoInsumo = i.id_TipoInsumo 
        INNER JOIN GruposInsumos gi 
            ON gi.id_TipoInsumo = i.id_TipoInsumo AND gi.id_GrupoInsumo = i.id_GrupoInsumo
        INNER JOIN UnidadesMedida u 
            ON u.id_UnidadMedida = i.id_UnidadMedida 
        LEFT JOIN AlmacenesExistencias ae 
            ON ae.id_Obra = rd.id_Obra AND ae.id_Insumo = rd.id_Insumo 
        INNER JOIN TipoRequisicion tr 
            ON r.id_TipoRequisicion = tr.id_TipoRequisicion 
        WHERE	rd.nu_PorComprar > 0 
        AND		rd.id_Estatus <> 102 
        --AND		(SELECT TOP 1 MAX(ic.sn_Activo) FROM InsumosCompradores ic WHERE ic.id_Insumo = RD.id_Insumo AND ic.id_EmpleadoComprador = #id_UsuarioRegistro# GROUP BY id_Insumo, sn_Activo) = 1 
        AND		rd.id_Requisicion = #id_Requisicion# 
        AND		rd.id_Obra = #id_Obra#
        AND		r.id_TipoRequisicion = #id_TipoRequisicion#
        --AND		rd.id_Insumo IN ( SELECT id_Insumo FROM InsumosCompradores WHERE id_EmpleadoComprador = #id_UsuarioComprador# ) 
        AND		sn_ComprarPlaza = 1 AND i.id_GrupoInsumo = i.id_GrupoInsumo 
        GROUP BY 
            r.id_Empresa,
            rd.id_Obra,
            rd.id_Insumo,
            rd.id_Requisicion
    </cfquery>
    
    <cftransaction>
    	<cftry>
            <cfloop array="#proveedores#" index="ind">
                <cfoutput>
                    <!---Proveedor: #ind#<br />--->
                    
                    <cfquery name="RsEmpresas" dbtype="query">
                        SELECT DISTINCT id_Empresa FROM RsInsumos
                    </cfquery>
                    
                    <cfloop query="RsEmpresas">
                        <!---Empresa: #RsEmpresas.id_Empresa#<br/>--->
                        <cfquery name="RsInsumosProveedor" datasource="#cnx#">
                    
                            SELECT 
                                rd.id_Insumo
                            FROM
                                RequisicionesDetalle rd
                            INNER JOIN Insumos i 
                                ON rd.id_Insumo = i.id_Insumo
                            INNER JOIN UnidadesMedida u 
                                ON u.id_UnidadMedida = i.id_UnidadMedida
                            INNER JOIN InsumosProveedores ip 
                                ON rd.id_Insumo = ip.id_Insumo
                            WHERE	rd.id_Empresa = #RsEmpresas.id_Empresa#
                            AND		rd.id_Obra = #id_Obra#
                            AND		rd.id_Requisicion = #id_Requisicion#
                            AND 	rd.id_Insumo IN (#QuotedValueList(Session.RsInsumosCotizar.id_Insumo)#)                                 
                            AND		ip.id_Proveedor = #ind#
                            GROUP BY rd.id_Insumo, i.de_Insumo, u.de_UnidadMedida
                        
                        </cfquery>
                                                                
                        <cfif RsInsumosProveedor.RecordCount GT 0>
                            <!---<cfdump var="#RsInsumosProveedor#">--->
                        
                            <cfquery name="RS" datasource="#cnx#">
                                SELECT 
                                    ISNULL(MAX(id_Cotizacion) + 1, 1)  as id_Cotizacion
                                FROM 
                                    RequisicionesCotizaciones	
                                WHERE 
                                    id_Empresa = #RsEmpresas.id_Empresa# AND id_Obra = #id_Obra# AND id_Requisicion = #id_Requisicion# AND id_Proveedor = #ind#
                            </cfquery>
                            
                            <cfquery name="rsComentarios" dbtype="query">
                                SELECT de_comentario FROM arguments.de_comentarios WHERE id_Proveedor = #ind#
                            </cfquery>
                             
                            <cfif rsComentarios.de_comentario EQ "">
                                <cfset de_comentario = ""> 
                            <cfelse>
                                <cfset de_comentario = #rsComentarios.de_comentario# > 
                            </cfif>
                            
                                                          
                           
                            <cfstoredproc procedure="RequisicionesCotizacionesAgregarProveedor" datasource="#cnx#" >
                                <cfprocparam type="IN" cfsqltype="CF_SQL_INTEGER" 	dbvarname="@id_Empresa" 		value="#RsEmpresas.id_Empresa#">
                                <cfprocparam type="IN" cfsqltype="CF_SQL_INTEGER" 	dbvarname="@id_Obra" 			value="#arguments.id_Obra#">
                                <cfprocparam type="IN" cfsqltype="CF_SQL_INTEGER" 	dbvarname="@id_Requisicion"		value="#arguments.id_Requisicion#">
                                <cfprocparam type="IN" cfsqltype="CF_SQL_INTEGER" 	dbvarname="@id_Cotizacion" 		value="#RS.id_Cotizacion#">                    
                                <cfprocparam type="IN" cfsqltype="CF_SQL_INTEGER" 	dbvarname="@id_Proveedor" 		value="#ind#">
                                <cfprocparam type="IN" cfsqltype="CF_SQL_DATE"		dbvarname="@fh_Registro" 		value="#arguments.fh_Registro#">
                                <cfprocparam type="IN" cfsqltype="CF_SQL_INTEGER" 	dbvarname="@id_UsuarioRegistro" value="#session.id_empleado#">
                                <cfprocparam type="IN" cfsqltype="CF_SQL_VARCHAR" 	dbvarname="@id_Insumo" 			null="yes">
                                <cfprocparam type="IN" cfsqltype="CF_SQL_FLOAT" 	dbvarname="@nu_Cantidad" 		null="yes">
                                <cfprocparam type="IN" cfsqltype="CF_SQL_INTEGER" 	dbvarname="@im_PrecioProveedor" null="yes">
                                <cfprocparam type="IN" cfsqltype="CF_SQL_INTEGER" 	dbvarname="@id_Marca" 			null="yes">
                                <cfprocparam type="IN" cfsqltype="CF_SQL_TIMESTAMP" dbvarname="@fh_Respuesta" 		null="yes">
                                <cfprocparam type="IN" cfsqltype="CF_SQL_TIMESTAMP" dbvarname="@fh_vigencia" 		null="yes">
                                <cfprocparam type="IN" cfsqltype="CF_SQL_VARCHAR" 	dbvarname="@de_Observaciones" 	value="#de_comentario#">
                                <cfprocparam type="IN" cfsqltype="CF_SQL_BIT" 		dbvarname="@Opcion" 			value="1">
                                <cfprocparam type="IN" cfsqltype="CF_SQL_INTEGER"   dbvarname="@id_TipoRequisicion" value="#arguments.id_tipoRequisicion#">
                                <cfprocparam type="IN" cfsqltype="CF_SQL_BIT" 		dbvarname="@sn_Aceptado" 		value="0">
                                <cfprocparam type="IN" cfsqltype="CF_SQL_DATE" 		dbvarname="@fh_Aceptado"        null="yes">
                               	<cfprocresult name="rs" resultset="1">                                
                            </cfstoredproc>
                            
                           
                            <cfquery name="RsInsumosCotizar" dbtype="query">
                                SELECT id_Insumo, SUM( nu_Cantidad ) AS nu_Cantidad FROM RsInsumos WHERE id_Insumo IN (#QuotedValueList(RsInsumosProveedor.id_Insumo)#) GROUP BY id_Insumo
                            </cfquery>        
                            
                            
                            
                            <cfset QueryAddColumn( RsInsumosCotizar, 'id_Marca', 'CF_SQL_INTEGER', ListToArray( '' ) )>
                            
                            <cfloop query="RsInsumosCotizar" >
                            
                                <cfset posMarca = 1>
                                <cfloop array="#insumos#" index="insumo">
                                
                                    <cfif insumo EQ RsInsumosCotizar.id_Insumo>
                                        <cfset RsInsumosCotizar.id_Marca = marcas[ posMarca ]>
                                        <cfbreak>
                                    </cfif>
                                    <cfset posMarca += 1>
                                
                                </cfloop>
                            
                            </cfloop>
                            
                            <cfloop query="RsInsumosCotizar">
                                
                                 <cfstoredproc procedure="RequisicionesCotizacionesAgregarProveedor" datasource="#cnx#">        
                                    <cfprocparam type="IN" cfsqltype="CF_SQL_INTEGER" 	dbvarname="@id_Empresa" 		value="#RsEmpresas.id_Empresa#">
                                    <cfprocparam type="IN" cfsqltype="CF_SQL_INTEGER" 	dbvarname="@id_Obra" 			value="#arguments.id_Obra#">
                                    <cfprocparam type="IN" cfsqltype="CF_SQL_INTEGER" 	dbvarname="@id_Requisicion" 	value="#arguments.id_Requisicion#">
                                    <cfprocparam type="IN" cfsqltype="CF_SQL_INTEGER" 	dbvarname="@id_Cotizacion" 		value="#RS.id_Cotizacion#">                        
                                    <cfprocparam type="IN" cfsqltype="CF_SQL_INTEGER" 	dbvarname="@id_Proveedor" 		value="#ind#">
                                    <cfprocparam type="IN" cfsqltype="CF_SQL_DATE"		dbvarname="@fh_Registro" 		value="#arguments.fh_Registro#">
                                    <cfprocparam type="IN" cfsqltype="CF_SQL_INTEGER" 	dbvarname="@id_UsuarioRegistro" value="#session.id_Empleado#">
                                    <cfprocparam type="IN" cfsqltype="CF_SQL_VARCHAR" 	dbvarname="@id_Insumo" 			value="#RsInsumosCotizar.id_Insumo#">
                                    <cfprocparam type="IN" cfsqltype="CF_SQL_FLOAT" 	dbvarname="@nu_Cantidad" 		value="#RsInsumosCotizar.nu_Cantidad#">
                                    <cfprocparam type="IN" cfsqltype="CF_SQL_INTEGER" 	dbvarname="@im_PrecioProveedor" null="yes">
                                    <cfprocparam type="IN" cfsqltype="CF_SQL_INTEGER" 	dbvarname="@id_Marca" 			value="#RsInsumosCotizar.id_Marca#" null="yes">
                                    <cfprocparam type="IN" cfsqltype="CF_SQL_TIMESTAMP" dbvarname="@fh_Respuesta" 		null="yes">
                                    <cfprocparam type="IN" cfsqltype="CF_SQL_TIMESTAMP" dbvarname="@fh_vigencia" 		null="yes">
                                    <cfprocparam type="IN" cfsqltype="CF_SQL_VARCHAR" 	dbvarname="@de_Observaciones" 	null="yes">
                                    <cfprocparam type="IN" cfsqltype="CF_SQL_BIT" 		dbvarname="@Opcion" 			value="0">
                                    <cfprocparam type="IN" cfsqltype="CF_SQL_INTEGER"   dbvarname="@id_TipoRequisicion" value="#arguments.id_tipoRequisicion#">
                                	<cfprocparam type="IN" cfsqltype="CF_SQL_BIT" 		dbvarname="@sn_Aceptado" 		value="0">
                               		<cfprocparam type="IN" cfsqltype="CF_SQL_DATE" 		dbvarname="@fh_Aceptado"        null="yes">
                                </cfstoredproc>
                                
                            </cfloop>
                           
                       </cfif>
                        
                    </cfloop>
                                            
                </cfoutput>
            </cfloop>
         	
            <cfset Cotizacion.agregada= TRUE>
            <cfset Cotizacion.tipoError= ''>
            <cfset Cotizacion.mensaje= 'Se envio correctamente'>
            <cfreturn Cotizacion>
            <cfcatch type='database'>
                <cftransaction action="rollback">
                <cfset Cotizacion.agregado= FALSE>
                    <cfset Cotizacion.tipoError= 'database-indefinido'>
                    
                    <cfset Cotizacion.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
                <cfreturn Cotizacion>
            </cfcatch>
    	</cftry>
    </cftransaction>
     
</cffunction>


			<!---Funcion que sirve para obtener la especialidad de los proveedores--->
            <!---                Miguel Hull      20 Febrero del 2014             --->
 <cffunction name="obtenerEspecialidadComprador" access="remote" returntype="struct" returnFormat="JSON"> 
         <cfargument name="id_Proveedor" type="numeric" required="yes" default="0">
         <cftry>
             <cfquery datasource='#session.cnx#' name='RsEspecialidades' username='#user_sql#' password='#password_sql#'>
                select esp.id_Especialidad,e.de_Especialidad

                from EspecialidadesProveedores esp
                
                inner join Proveedores p on p.id_Proveedor = esp.id_Proveedor
                inner join Especialidades e on e.id_Especialidad = esp.id_Especialidad
                
                 where esp.id_proveedor =#id_Proveedor#
                
                 order by e.de_Especialidad
                
            </cfquery>
            <cfset Result.SUCCESS = true>
            <cfset Result.MESSAGE = 'datos obtenidos con exito'>
             <cfset Result.RS = RsEspecialidades>
            <cfreturn result>
            
         <cfcatch type='database'>
                <cfset Result.SUCCESS = false>
				<cfset Result.MESSAGE = "Error al recuperar los datos => #cfcatch.Detail#">    
                <cfreturn Result>
            </cfcatch>
        </cftry>
    </cffunction>
    
    
    		<!---Funcion que sirve para obtener el nu_Minimo de la tabla especialidades--->
            <!---                Miguel Hull               21 Febrero del 2014             --->
 <cffunction name="get_NuMinimo" access="remote" returntype="struct" > 
         <cfargument name="id_Especialidad" type="any" required="yes" default="">
         <cfargument name="id_Proveedor"	type="any" required="yes" default="">
         
         <cftry>
             <cfquery datasource='#session.cnx#' name='RsNuMinimo' username='#user_sql#' password='#password_sql#'>
					execute ProveedoresEquipos_ObtenerMinimoCapacidad #id_Especialidad#,#id_Proveedor#               
            </cfquery>
            <cfset Result.SUCCESS = true>
            <cfset Result.MESSAGE = 'datos obtenidos con exito'>
             <cfset Result.RS = RsNuMinimo>
            <cfreturn result>
            
         <cfcatch type='database'>
                <cfset Result.SUCCESS = false>
				<cfset Result.MESSAGE = "Error al recuperar los datos => #cfcatch.Detail#">    
                <cfreturn Result>
            </cfcatch>
        </cftry>
    </cffunction>
    
        	<!---Funcion que sirve para obtener el nu_capacidad de la tabla especialidades--->
            <!---                Miguel Hull              21 Febrero del 2014             --->
 <cffunction name="get_NuCapacidad" access="remote" returntype="struct" > 
         <cfargument name="id_Proveedor" type="any" required="yes" default="0">
         <cftry>
             <cfquery datasource='#session.cnx#' name='RsCapacidad' username='#user_sql#' password='#password_sql#'>
                select 
                	COALESCE(nu_Capacidad,0) AS  nu_Capacacidad
                from 
                	Proveedores
                where
                 	id_Proveedor = #id_Proveedor#
               
            </cfquery>
            <cfset Result.SUCCESS = true>
            <cfset Result.MESSAGE = 'datos obtenidos con exito'>
             <cfset Result.RS = RsCapacidad>
            <cfreturn result>
            
         <cfcatch type='database'>
                <cfset Result.SUCCESS = false>
				<cfset Result.MESSAGE = "Error al recuperar los datos => #cfcatch.Detail#">    
                <cfreturn Result>
            </cfcatch>
        </cftry>
    </cffunction>
    
            <!---Funcion que sirve para obtener el numero de Equipos de un proveedor  --->
            <!---                Miguel Hull              21 Febrero del 2014             --->
 <cffunction name="get_NumEquipos" access="remote" returntype="struct" > 
 		 <cfargument name="id_Especialidad" type="any" required="yes" default="">
         <cfargument name="id_Proveedor" type="any" required="yes" default="">
         <cftry>
             <cfquery datasource='#session.cnx#' name='RsEquipos' username='#user_sql#' password='#password_sql#'>
				select count(*) as NumEquipos from ctl_ProveedoresEquipos where idu_Especialidad = #id_Especialidad# and idu_Proveedor = #id_Proveedor#
               
            </cfquery>
            <cfset Result.SUCCESS = true>
            <cfset Result.MESSAGE = 'datos obtenidos con exito'>
             <cfset Result.RS = RsEquipos>
            <cfreturn result>
            
         <cfcatch type='database'>
                <cfset Result.SUCCESS = false>
				<cfset Result.MESSAGE = "Error al recuperar los datos => #cfcatch.Detail#">    
                <cfreturn Result>
            </cfcatch>
        </cftry>
    </cffunction>

	<!---Funcion para verificar si un proveedor tiene OT Asignadas--->
    <!---Desarrollador: Giovanni Manjarrez -------------------------->
    <!---Fecha: 26-03-2014------------------------------------------->
    <cffunction name="getOTAsignadas_Proveedor" access="remote" returntype="struct">
    	<cfargument name="id_Proveedor" type="numeric" required="yes">
        <cftry>
            <cfquery name="ObtenerOT_Proveedor" datasource="#session.cnx#">
                SELECT 
                        COUNT (id_OrdenTrabajo) AS OTAsignadas
                FROM 	
                        OrdenesTrabajo 
                  WHERE
                        id_Proveedor= #id_Proveedor# AND
						id_Estatus = 308
            </cfquery>
            <cfset Result.SUCCESS = true>
            <cfset Result.MESSAGE = 'Datos obtenidos con exito'>
            <cfset Result.RS = ObtenerOT_Proveedor>
            <cfreturn result>
            <cfcatch type='database'>
                <cfset Result.SUCCESS = false>
                <cfset Result.MESSAGE = "Error al recuperar los datos => #cfcatch.Detail#">    
                <cfreturn Result>
            </cfcatch>
        </cftry>
    </cffunction>

    <!---Funcion para obtener el nombre de un Proveedor-------------->
    <!---Desarrollador: Giovanni Manjarrez -------------------------->
    <!---Fecha: 26-03-2014------------------------------------------->
    <cffunction name="getNombre_Proveedor" access="remote" returntype="struct">
        <cfargument name="id_Proveedor" type="numeric" required="yes">
        <cftry>
            <cfquery name="ObtenerNB_Proveedor" datasource="#session.cnx#">
                SELECT 
                        nb_Proveedor
                FROM 	
                        Proveedores 
                WHERE
                        id_Proveedor= #id_Proveedor#
            </cfquery>
            <cfset Result.SUCCESS = true>
            <cfset Result.MESSAGE = 'Datos obtenidos con exito'>
            <cfset Result.RS = ObtenerNB_Proveedor>
            <cfreturn result>
         	<cfcatch type='database'>
                <cfset Result.SUCCESS = false>
                <cfset Result.MESSAGE = "Error al recuperar los datos => #cfcatch.Detail#">    
                <cfreturn Result>
            </cfcatch>
        </cftry>
    </cffunction>  
    
     <cffunction name="get_CapacidadEspecialidad" access="remote" returntype="numeric" > 
         <cfargument name="id_Especialidad" type="any" required="yes" default="0">
         <cfargument name="id_Proveedor" type="any" required="yes" default="0">
         
         <cftry>
             <cfquery datasource='#session.cnx#' name='RsCapacidad' username='#user_sql#' password='#password_sql#'>
               select
                    nu_Capacidad
                
                from 
                    EspecialidadesProveedores esp
                
                where 
                    id_Proveedor = #id_Proveedor# and 
                    esp.id_Especialidad= #id_Especialidad# and
                    sn_Activo = 1
               
            </cfquery>
             <cfset result = #RsCapacidad.nu_Capacidad#>
            
             <cfif result EQ '' OR result EQ 'null'>
			 	<cfset result =0>
			 </cfif>
             
            <cfreturn result>
            
         <cfcatch type='database'>
                <cfset Result.SUCCESS = false>
				<cfset Result.MESSAGE = "Error al recuperar los datos => #cfcatch.Detail#">    
                <cfreturn Result>
            </cfcatch>
        </cftry>
    </cffunction>

<!---Funcion que trae el nb_Proveedor por su RFC--->
<cffunction name="getProveedorPorRFC" access="remote" returntype="struct" returnFormat="JSON">
	<cfargument name="de_RFC" type="string" required="yes">
    	<cfset de_RFC = Replace(de_RFC,'-','','ALL')>
        
		<cfquery name="RS" datasource='#cnx#' username='#user_sql#' password='#password_sql#'>
			Select de_RFC,de_password from proveedores where de_RFC like '#de_RFC#'		
        </cfquery>
        <cfset Result.SUCCESS = true>
        <cfset Result.de_RFC = RS.de_RFC> 
        <cfset Result.registrar = "false">
  		<cfif RS.de_password EQ ''>
	        <cfset Result.registrar = "true">
        </cfif>
        <cfreturn Result>
</cffunction>

<!---Autor: Ricardo Avendaño --->
<!---Fecha: 2014/12/31--->
<!---Descripcion: obtiene los insumos que no se enviaran a cotizar--->
<cffunction name="getInsumosSinProveedor" access="remote" returntype="struct" returnFormat="JSON">
	<cfargument name="id_Empresa" 		required="yes" type="any">
	<cfargument name="id_Obra" 			required="yes" type="any">
	<cfargument name="id_Requisicion" 	required="yes" type="any">
	<cfargument name="insumos" 			required="yes" type="any">
	<cfargument name="proveedores" 		required="yes" type="any">
   
    <cfset ins = insumos.split(",")> 		<cfset ins = ArrayToList(ins)>
	<cfset prov = proveedores.split(",")>   <cfset prov = ArrayToList(prov)>
	
    <cfif prov eq "" >
		<cfset prov = 'NULL' >
	</cfif>
    <cfif ins eq "">
		<cfset ins = 'NULL' >
	</cfif>
  	
	<cftry>
		<cfquery name="RSQuery" datasource="#session.cnx#">
				select  id_Insumo, de_Insumo, de_UnidadMedida, nu_Cantidad   from
				(
					select rd.id_Insumo, i.de_Insumo, u.de_UnidadMedida, sum(rd.nu_Cantidad) as nu_Cantidad 
					from RequisicionesDetalle rd
						INNER JOIN Insumos i ON	rd.id_Insumo = i.id_Insumo
						INNER JOIN UnidadesMedida u ON u.id_UnidadMedida = i.id_UnidadMedida        
					where 
						rd.id_empresa=#id_empresa#
						and rd.id_obra=#id_obra#
						and rd.id_Requisicion=#id_requisicion#
						and rd.id_insumo in (#ListQualify(ins,"'" , "," , "all")#)
					group by rd.id_Insumo, i.de_Insumo, u.de_UnidadMedida
				)t
				where 
				id_insumo not in(select id_insumo
								 from InsumosProveedores
								 where
									id_insumo in (#ListQualify(ins,"'" , "," , "all")#)
                                    and id_proveedor in (#prov#)
									and sn_activo=1
                                 )
		</cfquery>
		
		<cfset Result.SUCCESS = true>
		<cfset Result.MESSAGE = 'Datos obtenidos con exito'>
        <cfset Result.RS = RSQuery>
		<cfreturn Result>
	 <cfcatch type='database'>
		<cfset Result.SUCCESS = false>
		<cfset Result.MESSAGE = "Error al recuperar los datos => #cfcatch.Detail#">    
		<cfreturn Result>
	</cfcatch>
	</cftry>
</cffunction>

</cfcomponent>
