<cfcomponent extends='conexion'>
<cffunction name='RSMostrarNextIDPresupuestos' access='public' returntype='struct'>

	<cfargument name='id_Empresa' type='numeric' required='yes'>
	<cfargument name='id_Obra' type='numeric' required='yes'>
	<cfargument name='id_CentroCosto' type='numeric' required='yes'>
	<cfargument name='id_Frente' type='numeric' required='yes'>
	<cfargument name='id_Partida' type='numeric' required='yes'>

	<cftry>
		<cfquery datasource='#cnx#' name='RS_MostrarNextIDPresupuestos' username='#user_sql#' password='#password_sql#'>
			EXECUTE bop_Presupuestos_MostrarNextID #id_Empresa#, #id_Obra#, #id_CentroCosto#, #id_Frente#, #id_Partida#
		</cfquery>
		<cfset Presupuestos.NextID= TRUE>
		<cfset Presupuestos.tipoError= ''>
		<cfset Presupuestos.rs= RS_MostrarNextIDPresupuestos>
		<cfset Presupuestos.mensaje= 'Se obtuvo el recordset de Presupuestos correctamente'>
		<cfreturn Presupuestos>
		<cfcatch type='database'>
			<cfset Presupuestos.NextID= FALSE>
			<cfset Presupuestos.tipoError= 'database-indefinido'>
			<cfset Presupuestos.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn Presupuestos>
		</cfcatch>
	</cftry>
</cffunction>


<cffunction name='RSMostrarPorIDPresupuestos' access='public' returntype='struct'>

	<cfargument name='id_Empresa' type='numeric' required='yes'>
	<cfargument name='id_Obra' type='numeric' required='yes'>
	<cfargument name='id_CentroCosto' type='numeric' required='yes'>
	<cfargument name='id_Frente' type='numeric' required='yes'>
	<cfargument name='id_Partida' type='numeric' required='yes'>
	<cfargument name='nd_Presupuesto' type='numeric' required='yes'>

	<cftry>
		<cfquery datasource='#cnx#' name='RS_MostrarPresupuestos' username='#user_sql#' password='#password_sql#'>
			EXECUTE bop_Presupuestos_ObtenerPorID #id_Empresa#, #id_Obra#, #id_CentroCosto#, #id_Frente#, #id_Partida#, #nd_Presupuesto#
		</cfquery>
		<cfset Presupuestos.listado= TRUE>
		<cfset Presupuestos.tipoError= ''>
		<cfset Presupuestos.rs= RS_MostrarPresupuestos>
		<cfset Presupuestos.mensaje= 'Se obtuvo el recordset de Presupuestos correctamente'>
		<cfreturn Presupuestos>
		<cfcatch type='database'>
			<cfset Presupuestos.listado= FALSE>
			<cfset Presupuestos.tipoError= 'database-indefinido'>
			<cfset Presupuestos.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn Presupuestos>
		</cfcatch>
	</cftry>
</cffunction>


<cffunction name='RSMostrarDinamicoPresupuestos' access='public' returntype='struct'>

	<cfargument name='id_Empresa' type='numeric' required='no' default='-1'>
	<cfargument name='id_Obra' type='numeric' required='no' default='-1'>
	<cfargument name='id_CentroCosto' type='numeric' required='no' default='-1'>
	<cfargument name='id_Frente' type='numeric' required='no' default='-1'>
	<cfargument name='id_Partida' type='numeric' required='no' default='-1'>
	<cfargument name='nd_Presupuesto' type='numeric' required='no' default='-1'>
	<cfargument name='id_Tarjeta' type='string' required='no' default='-1'>
	<cfargument name='id_NaturalezaTarjeta' type='numeric' required='no' default='-1'>
	<cfargument name='id_TipoEjecucionTarjeta' type='numeric' required='no' default='-1'>
	<cfargument name='nu_Cantidad' type='numeric' required='no' default=''>
	<cfargument name='im_TarjetaPrecio' type='numeric' required='no' default='-1'>
	<cfargument name='im_SubTotal' type='numeric' required='no' default='-1'>
	<cfargument name='sn_Explosion' type='boolean' required='no' default='-1'>
	<cfargument name='sn_Autorizado' type='boolean' required='no' default='-1'>

	<cftry>
		<cfset WhereDinamico=''>
		<cfif #id_Empresa# NEQ '-1'>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE id_Empresa = #id_Empresa#">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND id_Empresa = #id_Empresa#">
			</cfif>
		</cfif>
		<cfif #id_Obra# NEQ '-1'>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE id_Obra = #id_Obra#">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND id_Obra = #id_Obra#">
			</cfif>
		</cfif>
		<cfif #id_CentroCosto# NEQ '-1'>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE id_CentroCosto = #id_CentroCosto#">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND id_CentroCosto = #id_CentroCosto#">
			</cfif>
		</cfif>
		<cfif #id_Frente# NEQ '-1'>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE id_Frente = #id_Frente#">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND id_Frente = #id_Frente#">
			</cfif>
		</cfif>
		<cfif #id_Partida# NEQ '-1'>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE id_Partida = #id_Partida#">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND id_Partida = #id_Partida#">
			</cfif>
		</cfif>
		<cfif #nd_Presupuesto# NEQ '-1'>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE nd_Presupuesto = #nd_Presupuesto#">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND nd_Presupuesto = #nd_Presupuesto#">
			</cfif>
		</cfif>
		<cfif #id_Tarjeta# NEQ '-1'>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE id_Tarjeta = '#id_tarjeta#'">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND id_Tarjeta = '#id_Tarjeta#'">
			</cfif>
		</cfif>
		<cfif #id_NaturalezaTarjeta# NEQ '-1'>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE id_NaturalezaTarjeta = #id_NaturalezaTarjeta#">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND id_NaturalezaTarjeta = #id_NaturalezaTarjeta#">
			</cfif>
		</cfif>
		<cfif #id_TipoEjecucionTarjeta# NEQ '-1'>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE id_TipoEjecucionTarjeta = #id_TipoEjecucionTarjeta#">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND id_TipoEjecucionTarjeta = #id_TipoEjecucionTarjeta#">
			</cfif>
		</cfif>
		<cfif #nu_Cantidad# NEQ ''>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE nu_Cantidad = #nu_Cantidad#">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND nu_Cantidad = #nu_Cantidad#">
			</cfif>
		</cfif>
		<cfif #im_TarjetaPrecio# NEQ '-1'>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE im_TarjetaPrecio = #im_TarjetaPrecio#">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND im_TarjetaPrecio = #im_TarjetaPrecio#">
			</cfif>
		</cfif>
		<cfif #im_SubTotal# NEQ '-1'>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE im_SubTotal = #im_SubTotal#">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND im_SubTotal = #im_SubTotal#">
			</cfif>
		</cfif>
		<cfif #sn_Explosion# NEQ '-1'>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE sn_Explosion = #sn_Explosion#">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND sn_Explosion = #sn_Explosion#">
			</cfif>
		</cfif>
		<cfif #sn_Autorizado# NEQ '-1'>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE sn_Autorizado = #sn_Autorizado#">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND sn_Autorizado = #sn_Autorizado#">
			</cfif>
		</cfif>

		<cfquery datasource='#cnx#' name='RS_MostrarPresupuestos' username='#user_sql#' password='#password_sql#'>
			EXECUTE bop_Presupuestos_ObtenerDinamico '#WhereDinamico#'
		</cfquery>
		<cfset Presupuestos.listado= TRUE>
		<cfset Presupuestos.tipoError= ''>
		<cfset Presupuestos.rs= RS_MostrarPresupuestos>
		<cfset Presupuestos.mensaje= 'Se obtuvo el recordset de Presupuestos correctamente'>
		<cfreturn Presupuestos>
		<cfcatch type='database'>
			<cfset Presupuestos.listado= FALSE>
			<cfset Presupuestos.tipoError= 'database-indefinido'>
			<cfset Presupuestos.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn Presupuestos>
		</cfcatch>
	</cftry>
</cffunction>


<cffunction name='RSMostrarTodosPresupuestos' access='public' returntype='struct'>

	<cftry>
		<cfquery datasource='#cnx#' name='RS_MostrarPresupuestos' username='#user_sql#' password='#password_sql#'>
			EXECUTE bop_Presupuestos_ObtenerTodos 
		</cfquery>
		<cfset Presupuestos.listado= TRUE>
		<cfset Presupuestos.tipoError= ''>
		<cfset Presupuestos.rs= RS_MostrarPresupuestos>
		<cfset Presupuestos.mensaje= 'Se obtuvo el recordset de Presupuestos correctamente'>
		<cfreturn Presupuestos>
		<cfcatch type='database'>
			<cfset Presupuestos.listado= FALSE>
			<cfset Presupuestos.tipoError= 'database-indefinido'>
			<cfset Presupuestos.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn Presupuestos>
		</cfcatch>
	</cftry>
</cffunction>


<cffunction name='RSAgregarConNextIDPresupuestos' access='public' returntype='struct'>

	<cfargument name='id_Empresa' type='numeric' required='yes'>
	<cfargument name='id_Obra' type='numeric' required='yes'>
	<cfargument name='id_CentroCosto' type='numeric' required='yes'>
	<cfargument name='id_Frente' type='numeric' required='yes'>
	<cfargument name='id_Partida' type='numeric' required='yes'>
	<cfargument name='id_Tarjeta' type='string' required='yes' default='0'>
	<cfargument name='id_NaturalezaTarjeta' type='numeric' required='yes' default='0'>
	<cfargument name='id_TipoEjecucionTarjeta' type='numeric' required='yes' default='0'>
	<cfargument name='nu_Cantidad' type='numeric' required='yes' default='0'>
	<cfargument name='im_TarjetaPrecio' type='numeric' required='yes' default='0'>
	<cfargument name='im_SubTotal' type='numeric' required='yes' default='0'>
	<cfargument name='sn_Explosion' type='boolean' required='yes' default='0'>
	<cfargument name='sn_Autorizado' type='boolean' required='yes' default='0'>

	<cftry>
		<cftransaction>
			<cfset NextIDtmp= this.RSMostrarNextIDCentrosCostos(arguments.cnx, arguments.user_sql, arguments.password_sql, attributes.id_Empresa, attributes.id_Obra, attributes.id_CentroCosto, attributes.id_Frente, attributes.id_Partida).rs.NextID>
			<cfset Presupuestos= this.RSAgregarCentrosCostos(arguments.cnx, arguments.user_sql, arguments.password_sql, arguments.id_Empresa, arguments.id_Obra, arguments.id_CentroCosto, arguments.id_Frente, arguments.id_Partida, NextIDtmp, arguments.id_Tarjeta, arguments.id_NaturalezaTarjeta, arguments.id_TipoEjecucionTarjeta, arguments.nu_Cantidad, arguments.im_TarjetaPrecio, arguments.im_SubTotal, arguments.sn_Explosion, arguments.sn_Autorizado)>
		</cftransaction>
		<cfset Presupuestos.NextID= NextIDtmp>
		<cfreturn Presupuestos>
		<cfcatch type='database'>
			<cftransaction action='rollback'/>
			<cfset Presupuestos.agregado= FALSE>
			<cfif cfcatch.NativeErrorCode EQ '2627'>
				<cfset Presupuestos.tipoError= 'database-registro_duplicado'>
				<cfset Presupuestos.mensaje= 'El Presupuesto no se pudo guardar porque ya existe'>
			<cfelse>
				<cfset Presupuestos.tipoError= 'database-indefinido'>
				<cfset Presupuestos.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			</cfif>
			<cfreturn Presupuestos>
		</cfcatch>
	</cftry>
</cffunction>


<cffunction name='RSAgregarPresupuestos' access='public' returntype='struct'>

	<cfargument name='id_Empresa' type='numeric' required='yes' default='0'>
	<cfargument name='id_Obra' type='numeric' required='yes' default='0'>
	<cfargument name='id_CentroCosto' type='numeric' required='yes' default='0'>
	<cfargument name='id_Frente' type='numeric' required='yes' default='0'>
	<cfargument name='id_Partida' type='numeric' required='yes' default='0'>
	<cfargument name='nd_Presupuesto' type='numeric' required='yes' default='0'>
	<cfargument name='id_Tarjeta' type='string' required='yes' default=''>
	<cfargument name='id_NaturalezaTarjeta' type='numeric' required='yes' default='0'>
	<cfargument name='id_TipoEjecucionTarjeta' type='numeric' required='yes' default='0'>
	<cfargument name='nu_Cantidad' type='numeric' required='yes' default='0'>
	<cfargument name='im_TarjetaPrecio' type='numeric' required='yes' default='0'>
	<cfargument name='im_SubTotal' type='numeric' required='yes' default='0'>
	<cfargument name='sn_Explosion' type='boolean' required='yes' default='0'>
	<cfargument name='sn_Autorizado' type='boolean' required='yes' default='0'>

	<cftry>
		<cfquery datasource='#cnx#' name='RSAgregar' username='#user_sql#' password='#password_sql#'>
			EXECUTE bop_Presupuestos_Agregar #id_Empresa#, #id_Obra#, #id_CentroCosto#, #id_Frente#, #id_Partida#, #nd_Presupuesto#, '#id_Tarjeta#', #id_NaturalezaTarjeta#, #id_TipoEjecucionTarjeta#, #nu_Cantidad#, #im_TarjetaPrecio#, #im_SubTotal#, #sn_Explosion#, #sn_Autorizado#
		</cfquery>
		<!---Log--->
		<cf_log datasource='#cnx#' username='#user_sql#' password='#password_sql#' id_Empresa="#Session.id_Empresa#" id_Obra="#Session.id_Obra#" id_Usuario="#Session.id_Usuario#" id_MenuOpcion="#Session.id_MenuOpcion#">
			<cfoutput>
				EXECUTE bop_Presupuestos_Agregar #id_Empresa#, #id_Obra#, #id_CentroCosto#, #id_Frente#, #id_Partida#, #nd_Presupuesto#, '#id_Tarjeta#', #id_NaturalezaTarjeta#, #id_TipoEjecucionTarjeta#, #nu_Cantidad#, #im_TarjetaPrecio#, #im_SubTotal#, #sn_Explosion#, #sn_Autorizado#
			</cfoutput>
		</cf_log>
		<!---Termina Log --->
		<cfset Presupuestos.agregado= TRUE>
		<cfset Presupuestos.tipoError= ''>
		<cfset Presupuestos.mensaje= 'Presupuesto guardado correctamente'>
		<cfreturn Presupuestos>
		<cfcatch type='database'>
			<cfset Presupuestos.agregado= FALSE>
			<cfif cfcatch.NativeErrorCode EQ '2627'>
				<cfset Presupuestos.tipoError= 'database-registro_duplicado'>
				<cfset Presupuestos.mensaje= 'El Presupuesto no se pudo guardar porque ya existe'>
			<cfelse>
				<cfset Presupuestos.tipoError= 'database-indefinido'>
				<cfset Presupuestos.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			</cfif>
			<cfreturn Presupuestos>
		</cfcatch>
	</cftry>
</cffunction>


<cffunction name='RSEditarPresupuestos' access='public' returntype='struct'>

	<cfargument name='id_Empresa' type='numeric' required='yes'>
	<cfargument name='id_Obra' type='numeric' required='yes'>
	<cfargument name='id_CentroCosto' type='numeric' required='yes'>
	<cfargument name='id_Frente' type='numeric' required='yes'>
	<cfargument name='id_Partida' type='numeric' required='yes'>
	<cfargument name='nd_Presupuesto' type='numeric' required='yes'>
	<cfargument name='id_Tarjeta' type='string' required='yes' default='0'>
	<cfargument name='id_NaturalezaTarjeta' type='numeric' required='yes' default='0'>
	<cfargument name='id_TipoEjecucionTarjeta' type='numeric' required='yes' default='0'>
	<cfargument name='nu_Cantidad' type='numeric' required='yes' default='0'>
	<cfargument name='im_TarjetaPrecio' type='numeric' required='yes' default='0'>
	<cfargument name='im_SubTotal' type='numeric' required='yes' default='0'>
	<cfargument name='sn_Explosion' type='boolean' required='yes' default='0'>
	<cfargument name='sn_Autorizado' type='boolean' required='yes' default='0'>

	<cftry>
		<cfquery datasource='#cnx#' name='RSFilasAfectadas' username='#user_sql#' password='#password_sql#'>
			EXECUTE bop_Presupuestos_Actualizar #id_Empresa#, #id_Obra#, #id_CentroCosto#, #id_Frente#, #id_Partida#, #nd_Presupuesto#, '#id_Tarjeta#', #id_NaturalezaTarjeta#, #id_TipoEjecucionTarjeta#, #nu_Cantidad#, #im_TarjetaPrecio#, #im_SubTotal#, #sn_Explosion#, #sn_Autorizado#
		</cfquery>
		<!---Log--->
		<cf_log datasource='#cnx#' username='#user_sql#' password='#password_sql#' id_Empresa="#Session.id_Empresa#" id_Obra="#Session.id_Obra#" id_Usuario="#Session.id_Usuario#" id_MenuOpcion="#Session.id_MenuOpcion#">
			<cfoutput>
				EXECUTE bop_Presupuestos_Actualizar #id_Empresa#, #id_Obra#, #id_CentroCosto#, #id_Frente#, #id_Partida#, #nd_Presupuesto#, '#id_Tarjeta#', #id_NaturalezaTarjeta#, #id_TipoEjecucionTarjeta#, #nu_Cantidad#, #im_TarjetaPrecio#, #im_SubTotal#, #sn_Explosion#, #sn_Autorizado#
			</cfoutput>
		</cf_log>
		<!---Termina Log --->
		<cfset Presupuestos.actualizado= TRUE>
		<cfset Presupuestos.filasAfectadas= RSFilasAfectadas.filasAfectadas>
		<cfset Presupuestos.tipoError= ''>
		<cfset Presupuestos.mensaje= 'Presupuesto actualizado correctamente'>
		<cfreturn Presupuestos>
		<cfcatch type='database'>
			<cfset Presupuestos.actualizado= FALSE>
			<cfset Presupuestos.tipoError= 'database-indefinido'>
			<cfset Presupuestos.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn Presupuestos>
		</cfcatch>
	</cftry>
</cffunction>


<cffunction name='RSEliminarPresupuestos' access='public' returntype='struct'>

	<cfargument name='id_Empresa' type='numeric' required='yes'>
	<cfargument name='id_Obra' type='numeric' required='yes'>
	<cfargument name='id_CentroCosto' type='numeric' required='yes'>
	<cfargument name='id_Frente' type='numeric' required='yes'>
	<cfargument name='id_Partida' type='numeric' required='yes'>
	<cfargument name='nd_Presupuesto' type='numeric' required='yes'>

	<cftry>
		<cfquery datasource='#cnx#' name='RSFilasAfectadas' username='#user_sql#' password='#password_sql#'>
			EXECUTE bop_Presupuestos_Eliminar #id_Empresa#, #id_Obra#, #id_CentroCosto#, #id_Frente#, #id_Partida#, #nd_Presupuesto#
		</cfquery>
		<!---Log--->
		<cf_log datasource='#cnx#' username='#user_sql#' password='#password_sql#' id_Empresa="#Session.id_Empresa#" id_Obra="#Session.id_Obra#" id_Usuario="#Session.id_Usuario#" id_MenuOpcion="#Session.id_MenuOpcion#">
			<cfoutput>
				EXECUTE bop_Presupuestos_Eliminar #id_Empresa#, #id_Obra#, #id_CentroCosto#, #id_Frente#, #id_Partida#, #nd_Presupuesto#
			</cfoutput>
		</cf_log>
		<!---Termina Log --->
		<cfset Presupuestos.eliminado= TRUE>
		<cfset Presupuestos.filasAfectadas= RSFilasAfectadas.filasAfectadas>
		<cfset Presupuestos.tipoError= ''>
		<cfset Presupuestos.mensaje= 'Presupuesto eliminado correctamente'>
		<cfreturn Presupuestos>
		<cfcatch type='database'>
			<cfset Presupuestos.eliminado= FALSE>
			<cfif cfcatch.NativeErrorCode EQ '547'>
				<cfset Presupuestos.mensaje= 'El Presupuesto no se pudo eliminar ya que tiene registros relacionados'>
				<cfset Presupuestos.tipoError= 'database-integridad'>
			<cfelse>
				<cfset Presupuestos.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
				<cfset Presupuestos.tipoError= 'database-indefinido'>
			</cfif>
			<cfreturn Presupuestos>
		</cfcatch>
	</cftry>
</cffunction>

<!---Funcion que trae los frentes de un presupuesto con su total--->
<cffunction name="RSMostrarFrentesConTotal" access="public" returntype="struct">
	<cfargument name="id_Empresa" type="string" required="yes">
	<cfargument name="id_Obra" type="numeric" required="yes">
	<cfargument name='id_CentroCosto' type='numeric' required='yes'>
	<cftry>
		<cfquery datasource='#cnx#' name='RS_MostrarFrentesPresupuesto' username='#user_sql#' password='#password_sql#'>
			EXECUTE Presupuestos_ObtenerFrentesConTotal <cfif #id_Empresa# NEQ ''>#id_Empresa#<cfelse>NULL</cfif>,#id_Obra#,#id_CentroCosto#
		</cfquery>
		<cfset Frentes.listado= TRUE>
		<cfset Frentes.tipoError= ''>
		<cfset Frentes.rs= RS_MostrarFrentesPresupuesto>
		<cfset Frentes.mensaje= 'Se obtuvo el recordset de frentes para presupuesto correctamente'>
		<cfreturn Frentes>
		<cfcatch type='database'>
			<cfset Frentes.listado= FALSE>
			<cfset Frentes.tipoError= 'database-indefinido'>
			<cfset Frentes.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn Frentes>
		</cfcatch>
	</cftry>
</cffunction>
	
<!---Funcion que trae las partidas de un presupuesto con su total--->
<cffunction name="RSMostrarPartidasConTotal" access="public" returntype="struct">
	<cfargument name="id_Empresa" type="string" required="yes">
	<cfargument name="id_Obra" type="numeric" required="yes">
	<cfargument name='id_CentroCosto' type='numeric' required='yes'>
	<cfargument name="id_Frente" type="numeric" required="yes">
	<cftry>
		<cfquery datasource='#cnx#' name='RS_MostrarPartidasPresupuesto' username='#user_sql#' password='#password_sql#'>
			EXECUTE Presupuestos_ObtenerPartidasConTotal <cfif #id_Empresa# NEQ ''>#id_Empresa#<cfelse>NULL</cfif>,#id_Obra#,#id_CentroCosto#,#id_Frente#
		</cfquery>
		<cfset Partidas.listado= TRUE>
		<cfset Partidas.tipoError= ''>
		<cfset Partidas.rs= RS_MostrarPartidasPresupuesto>
		<cfset Partidas.mensaje= 'Se obtuvo el recordset de partidas para presupuesto correctamente'>
		<cfreturn Partidas>
		<cfcatch type='database'>
			<cfset Partidas.listado= FALSE>
			<cfset Partidas.tipoError= 'database-indefinido'>
			<cfset Partidas.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn Partidas>
		</cfcatch>
	</cftry>
</cffunction>

<!---Funcion que trae los detalles de un presupuesto --->
<cffunction name="RSMostrarDetallePresupuesto" access="public" returntype="struct" output="no">
	<cfargument name="id_Empresa" type="string" required="no">
	<cfargument name="id_Obra" type="numeric" required="yes">
	<cfargument name='id_CentroCosto' type='numeric' required='no'>
	<cfargument name="id_Frente" type="numeric" required="no">
	<cfargument name="id_Partida" type="numeric" required="no">
	<cfif #id_Empresa# EQ ''>
		<cfset id_Empresa= "NULL">
	</cfif>
	<cfif isDefined("arguments.id_CentroCosto")>
		<cfset id_CentroCosto= #id_CentroCosto#>
	<cfelse>
		<cfset id_CentroCosto= "NULL">
	</cfif>
	<cfif isDefined("arguments.id_Frente")>
		<cfset id_FrenteParamSP= #id_Frente#>
	<cfelse>
		<cfset id_FrenteParamSP= "NULL">
	</cfif>
	<cfif isDefined("arguments.id_Partida")>
		<cfset id_PartidaParamSP= #id_Partida#>
	<cfelse>
		<cfset id_PartidaParamSP= "NULL">
	</cfif>
	
	<cftry>
		<cfquery datasource='#cnx#' name='RS_MostrarDetallesPresupuesto' username='#user_sql#' password='#password_sql#'>
			EXECUTE Presupuestos_ObtenerDetalles #id_Empresa#, #id_Obra#,#id_CentroCosto#,#id_FrenteParamSP#, #id_PartidaParamSP#
		</cfquery>
		<cfset Detalles.listado= TRUE>
		<cfset Detalles.tipoError= ''>
		<cfset Detalles.rs= RS_MostrarDetallesPresupuesto>
		<cfset Detalles.mensaje= 'Se obtuvo el recordset de detalles del presupuesto correctamente'>
		<cfreturn Detalles>
		<cfcatch type='database'>
			<cfset Detalles.listado= FALSE>
			<cfset Detalles.tipoError= 'database-indefinido'>
			<cfset Detalles.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn Detalles>
		</cfcatch>
	</cftry>
</cffunction>

<!---Funcion que trae los detalles de un presupuesto para la explosion de insumos--->
<cffunction name="RSMostrarDetallePresupuestoParaExplosion" access="public" returntype="struct" output="no">
	<cfargument name="id_Empresa" type="string" required="no">
	<cfargument name="id_Obra" type="numeric" required="yes">
	<cfargument name="id_CentroCosto" type="numeric" required="yes">
	<cfargument name="id_Frente" type="numeric" required="no">
	<cfargument name="id_Partida" type="numeric" required="no">
	
	<cfif #id_Empresa# EQ ''>
		<cfset id_Empresa= "NULL">
	</cfif>
	<cfif isDefined("arguments.id_Frente")>
		<cfset id_FrenteParamSP= #id_Frente#>
	<cfelse>
		<cfset id_FrenteParamSP= "NULL">
	</cfif>
	<cfif isDefined("arguments.id_Partida")>
		<cfset id_PartidaParamSP= #id_Partida#>
	<cfelse>
		<cfset id_PartidaParamSP= "NULL">
	</cfif>
	
	<cftry>
		<cfquery datasource='#cnx#' name='RS_MostrarDetallesPresupuesto' username='#user_sql#' password='#password_sql#'>
			EXECUTE Presupuestos_ObtenerDetallesParaExplosion #id_Empresa#, #id_Obra#, #id_CentroCosto#, #id_FrenteParamSP#, #id_PartidaParamSP#
		</cfquery>
		<cfset Detalles.listado= TRUE>
		<cfset Detalles.tipoError= ''>
		<cfset Detalles.rs= RS_MostrarDetallesPresupuesto>
		<cfset Detalles.mensaje= 'Se obtuvo el recordset de detalles del presupuesto correctamente'>
		<cfreturn Detalles>
		<cfcatch type='database'>
			<cfset Detalles.listado= FALSE>
			<cfset Detalles.tipoError= 'database-indefinido'>
			<cfset Detalles.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn Detalles>
		</cfcatch>
	</cftry>
</cffunction>

<!---Funcion que trae las partidas de un presupuesto para explosion--->
<cffunction name="RSMostrarPartidasParaExplosion" access="public" returntype="struct">
	<cfargument name="id_Empresa" type="numeric" required="yes">
	<cfargument name="id_Obra" type="numeric" required="yes">
	<cfargument name="id_CentroCosto" type="numeric" required="yes">
	<cfargument name="id_Frente" type="numeric" required="yes">
	<cftry>
		<cfquery datasource='#cnx#' name='RS_MostrarPartidasPresupuesto' username='#user_sql#' password='#password_sql#'>
			EXECUTE Presupuestos_ObtenerPartidasParaExplosion #id_Empresa#, #id_Obra#, #id_CentroCosto#, #id_Frente#
		</cfquery>
		<cfset Partidas.listado= TRUE>
		<cfset Partidas.tipoError= ''>
		<cfset Partidas.rs= RS_MostrarPartidasPresupuesto>
		<cfset Partidas.mensaje= 'Se obtuvo el recordset de partidas para explosi�n correctamente'>
		<cfreturn Partidas>
		<cfcatch type='database'>
			<cfset Partidas.listado= FALSE>
			<cfset Partidas.tipoError= 'database-indefinido'>
			<cfset Partidas.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn Partidas>
		</cfcatch>
	</cftry>
</cffunction>

<!---Funcion que trae los frebtes de un presupuesto para explosion--->
<cffunction name="RSMostrarFrentesParaExplosion" access="public" returntype="struct">
	<cfargument name="id_Empresa" type="numeric" required="yes">
	<cfargument name="id_Obra" type="numeric" required="yes">
	<cfargument name="id_CentroCosto" type="numeric" required="yes">
	<cftry>
		<cfquery datasource='#cnx#' name='RS_MostrarFrentesPresupuesto' username='#user_sql#' password='#password_sql#'>
			EXECUTE Presupuestos_ObtenerFrentesParaExplosion #id_Empresa#, #id_Obra#, #id_CentroCosto#
		</cfquery>
		<cfset Frentes.listado= TRUE>
		<cfset Frentes.tipoError= ''>
		<cfset Frentes.rs= RS_MostrarFrentesPresupuesto>
		<cfset Frentes.mensaje= 'Se obtuvo el recordset de frentes para explosi�n correctamente'>
		<cfreturn Frentes>
		<cfcatch type='database'>
			<cfset Frentes.listado= FALSE>
			<cfset Frentes.tipoError= 'database-indefinido'>
			<cfset Frentes.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn Frentes>
		</cfcatch>
	</cftry>
</cffunction>

<!---Funcion que trae los detalles de un presupuesto para la fusion de insumos--->
<cffunction name="RSMostrarDetallePresupuestoParaFusion" access="public" returntype="struct" output="no">
	<cfargument name="id_Empresa" type="string" required="yes">
	<cfargument name="id_Obra" type="numeric" required="yes">
	<cfargument name="id_CentroCosto" type="numeric" required="yes">
	<cfargument name="id_Frente" type="numeric" required="no">
	<cfargument name="id_Partida" type="numeric" required="no">
	<cfif #id_Empresa# EQ ''>
		<cfset id_Empresa = "NULL">
	</cfif>
	<cfif isDefined("arguments.id_Frente")>
		<cfset id_FrenteParamSP= #id_Frente#>
	<cfelse>
		<cfset id_FrenteParamSP= "NULL">
	</cfif>
	<cfif isDefined("arguments.id_Partida")>
		<cfset id_PartidaParamSP= #id_Partida#>
	<cfelse>
		<cfset id_PartidaParamSP= "NULL">
	</cfif>
	
	<cftry>
		<cfquery datasource='#cnx#' name='RS_MostrarDetallesPresupuesto' username='#user_sql#' password='#password_sql#'>
			EXECUTE Presupuestos_ObtenerDetallesParaFusion #id_Empresa#, #id_Obra#, #id_CentroCosto#, #id_FrenteParamSP#, #id_PartidaParamSP#
		</cfquery>
		<cfset Detalles.listado= TRUE>
		<cfset Detalles.tipoError= ''>
		<cfset Detalles.rs= RS_MostrarDetallesPresupuesto>
		<cfset Detalles.mensaje= 'Se obtuvo el recordset de detalles del presupuesto correctamente'>
		<cfreturn Detalles>
		<cfcatch type='database'>
			<cfset Detalles.listado= FALSE>
			<cfset Detalles.tipoError= 'database-indefinido'>
			<cfset Detalles.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn Detalles>
		</cfcatch>
	</cftry>
</cffunction>

<!---Funcion que agrega una tarjeta a un presupuesto --->
<cffunction name='RSAgregarTarjetaAPresupuesto' access='public' returntype='struct' output="no">

	<cfargument name='id_Empresa' type='numeric' required='yes'>
	<cfargument name='id_Obra' type='numeric' required='yes'>
	<cfargument name='id_CentroCosto' type='numeric' required='yes'>
	<cfargument name='id_Frente' type='numeric' required='yes'>
	<cfargument name='id_Partida' type='numeric' required='yes'>
	<cfargument name='id_Tarjeta' type='string' required='yes'>
	<cfargument name='id_NaturalezaTarjeta' type='numeric' required='yes'>
	<cfargument name='id_TipoEjecucionTarjeta' type='numeric' required='yes'>
	<cfargument name='nu_Cantidad' type='numeric' required='yes'>
	<cfargument name='sn_Explosion' type='boolean' required='yes'>
	<cfargument name='sn_Autorizado' type='boolean' required='yes'>
	
	<cftry>
		<!---Validaciones: ---->
		
		<!---Modificacion: 2009-07-31 --->
		<!--- 1.- Aqui se valida que el centro de costo, frente, partida no este cerrado (aun pendiente por definir exactamente)--->
		<cfquery datasource="#cnx#" name="RSCentroCostoCerrado" username='#user_sql#' password='#password_sql#'>
			EXECUTE Presupuestos_ObtenEstatusCentroCosto #id_Empresa#, #id_Obra#, #id_CentroCosto#
		</cfquery>
		<!--- 	Modificacion: 2009-07-31, ahora se pueden seguir agregando detalles al presupeusto aunque este autorizado, y ahora se verificara que el centro de costos no este en sn_Abierto= 0
		<cfif #RSCentroCostoCerrado.sn_AutorizadoBase# EQ 1>
			<cfset Presupuestos.actualizado= FALSE>
			<cfset Presupuestos.mensaje= 'El presupuesto del centro de costos se encuentra cerrado y no se puede modificar'>
			<cfset Presupuestos.tipoError= 'validaciones'>
			<cfreturn Presupuestos>
		</cfif> --->
		<cfif #RSCentroCostoCerrado.sn_Abierto# NEQ 1>
			<cfset Presupuestos.actualizado= FALSE>
			<cfset Presupuestos.mensaje= 'El presupuesto del centro de costos se encuentra cerrado y no se puede modificar'>
			<cfset Presupuestos.tipoError= 'validaciones'>
			<cfreturn Presupuestos>
		</cfif>
		
		<!---1.- EL importe de la tarjeta no debe ser cero o menor a cero --->
		<cfquery datasource="#cnx#" name='RSValidaImporteTarjeta' username='#user_sql#' password='#password_sql#'>
			EXECUTE Tarjetas_ObtenerImporte #id_Empresa#, #id_Obra#, #id_CentroCosto#, '#id_Tarjeta#'
		</cfquery>
		<cfif #RSValidaImporteTarjeta.im_TarjetaPrecio# LTE 0>
			<cfset Presupuestos.agregado= FALSE>
			<cfset Presupuestos.tipoError= 'validaciones'>
			<cfset Presupuestos.mensaje= 'Una tarjeta con importe igual o menor a cero no es v�lida.'>
			<cfreturn Presupuestos>						
		</cfif>
		
		<cfquery datasource='#cnx#' name='RSAgregar' username='#user_sql#' password='#password_sql#'>
			EXECUTE Presupuestos_AgregarTarjeta #id_Empresa#, #id_Obra#, #id_CentroCosto#, #id_Frente#, #id_Partida#, '#id_Tarjeta#', #id_NaturalezaTarjeta#, #id_TipoEjecucionTarjeta#, #nu_Cantidad#, #sn_Explosion#, #sn_Autorizado#
		</cfquery>
		<!---Log--->
		<cf_log datasource='#cnx#' username='#user_sql#' password='#password_sql#' id_Empresa="#Session.id_Empresa#" id_Obra="#Session.id_Obra#" id_Usuario="#Session.id_Usuario#" id_MenuOpcion="#Session.id_MenuOpcion#">
			<cfoutput>
				EXECUTE Presupuestos_AgregarTarjeta #id_Empresa#, #id_Obra#, #id_CentroCosto#, #id_Frente#, #id_Partida#, '#id_Tarjeta#', #id_NaturalezaTarjeta#, #id_TipoEjecucionTarjeta#, #nu_Cantidad#, #sn_Explosion#, #sn_Autorizado#
			</cfoutput>
		</cf_log>
		<!---Termina Log --->
		<cfset Presupuestos.agregado= TRUE>
		<cfset Presupuestos.tipoError= ''>
		<cfset Presupuestos.mensaje= 'Tarjeta agregada al presupuesto correctamente'>
		<cfreturn Presupuestos>
		<cfcatch type='database'>
			<cfset Presupuestos.agregado= FALSE>
			<cfif cfcatch.NativeErrorCode EQ '2627'>
				<cfset Presupuestos.tipoError= 'database-registro_duplicado'>
				<cfset Presupuestos.mensaje= 'La tarjeta no se puedo agregar al debido a que ya se encuentra repetido el detalle del presupuesto'>
			<cfelse>
				<cfset Presupuestos.tipoError= 'database-indefinido'>
				<cfset Presupuestos.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			</cfif>
			<cfreturn Presupuestos>
		</cfcatch>
	</cftry>
</cffunction>

<!---Funcion que edita una tarjeta (nd_Presupuesto)a un presupuesto --->
<cffunction name='RSEditarTarjetaAPresupuesto' access='public' returntype='struct' output="no">

	<cfargument name='id_Empresa' type='numeric' required='yes'>
	<cfargument name='id_Obra' type='numeric' required='yes'>
	<cfargument name='id_CentroCosto' type='numeric' required='yes'>
	<cfargument name='id_Frente' type='numeric' required='yes'>
	<cfargument name='id_Partida' type='numeric' required='yes'>
	<cfargument name="nd_Presupuesto" type="numeric" required="yes">
	<cfargument name='id_Tarjeta' type='string' required='yes'>
	<cfargument name='id_NaturalezaTarjeta' type='numeric' required='yes'>
	<cfargument name='id_TipoEjecucionTarjeta' type='numeric' required='yes'>
	<cfargument name='nu_Cantidad' type='numeric' required='yes'>
	<cfargument name='sn_Explosion' type='boolean' required='yes'>
	<cfargument name='sn_Autorizado' type='boolean' required='yes'>
	
	<!---Validaciones: ---->
	<!---1.- Aqui se valida que el centro de costo, frente, partida no este cerrado (aun pendiente por definir exactamente)--->
	<cfquery datasource="#cnx#" name="RSCentroCostoCerrado" username='#user_sql#' password='#password_sql#'>
		EXECUTE Presupuestos_ObtenEstatusCentroCosto #id_Empresa#, #id_Obra#, #id_CentroCosto#
	</cfquery>
	<!--- 	Modificacion: 2009-07-31, ahora se pueden eguier agregando detalles al presupeusto aunque este autorizado, y ahora se verificara que el centro de costos no este en sn_Abierto= 0
	<cfif #RSCentroCostoCerrado.sn_AutorizadoBase# EQ 1>
		<cfset Presupuestos.actualizado= FALSE>
		<cfset Presupuestos.mensaje= 'El presupuesto del centro de costos se encuentra cerrado y no se puede modificar'>
		<cfset Presupuestos.tipoError= 'validaciones'>
		<cfreturn Presupuestos>
	</cfif> --->
	<cfif #RSCentroCostoCerrado.sn_Abierto# NEQ 1>
		<cfset Presupuestos.actualizado= FALSE>
		<cfset Presupuestos.mensaje= 'El presupuesto del centro de costos se encuentra cerrado y no se puede modificar'>
		<cfset Presupuestos.tipoError= 'validaciones'>
		<cfreturn Presupuestos>
	</cfif>
	
	<!---3.- Tambien se validara que no se modifique un detalle que ya fue explosionado --->
	<cfquery datasource="#cnx#" name="RSEstatusDetalle" username='#user_sql#' password='#password_sql#'>
		EXECUTE Presupuestos_ObtenEstatusDetalle #id_Empresa#, #id_Obra#, #id_CentroCosto#, #id_Frente#, #id_Partida#, #nd_Presupuesto#
	</cfquery>
	<cfif #RSEstatusDetalle.sn_Explosion# EQ 1>
		<cfset Presupuestos.actualizado= FALSE>
		<cfset Presupuestos.mensaje= 'El presupuesto se encuentra explosionado y no se puede modificar.'>
		<cfset Presupuestos.tipoError= 'validaciones'>
		<cfreturn Presupuestos>
	</cfif>
	
	<cftry>
		<cfquery datasource='#cnx#' name='RSEditar' username='#user_sql#' password='#password_sql#'>
			EXECUTE Presupuestos_EditarTarjeta #id_Empresa#, #id_Obra#, #id_CentroCosto#, #id_Frente#, #id_Partida#, #nd_Presupuesto#, #id_NaturalezaTarjeta#, #id_TipoEjecucionTarjeta#, #nu_Cantidad#, #sn_Explosion#, #sn_Autorizado#
		</cfquery>
		<!---Log--->
		<cf_log datasource='#cnx#' username='#user_sql#' password='#password_sql#' id_Empresa="#Session.id_Empresa#" id_Obra="#Session.id_Obra#" id_Usuario="#Session.id_Usuario#" id_MenuOpcion="#Session.id_MenuOpcion#">
			<cfoutput>
				EXECUTE Presupuestos_EditarTarjeta #id_Empresa#, #id_Obra#, #id_CentroCosto#, #id_Frente#, #id_Partida#, #nd_Presupuesto#, #id_NaturalezaTarjeta#, #id_TipoEjecucionTarjeta#, #nu_Cantidad#, #sn_Explosion#, #sn_Autorizado#
			</cfoutput>
		</cf_log>
		<!---Termina Log --->
		<cfset Presupuestos.actualizado= TRUE>
		<cfset Presupuestos.tipoError= ''>
		<cfset Presupuestos.mensaje= 'Tarjeta editada al presupuesto correctamente'>
		<cfreturn Presupuestos>
		<cfcatch type='database'>
			<cfset Presupuestos.agregado= FALSE>
			<cfif cfcatch.NativeErrorCode EQ '2627'>
				<cfset Presupuestos.tipoError= 'database-registro_duplicado'>
				<cfset Presupuestos.mensaje= 'La tarjeta no se pudo editar al debido a que ya se encuentra repetido el detalle del presupuesto'>
			<cfelse>
				<cfset Presupuestos.tipoError= 'database-indefinido'>
				<cfset Presupuestos.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			</cfif>
			<cfreturn Presupuestos>
		</cfcatch>
	</cftry>
</cffunction>

<!---Funcion que trae el importe de un presupuesto segun los parametros enviados (desde centros de costos hasta la partida)--->
<cffunction name='RSObtenImporteTotalParametrizado' access='public' returntype='struct'>

	<cfargument name='id_Empresa' type='string' required='yes'>
	<cfargument name='id_Obra' type='numeric' required='yes'>
	<cfargument name='id_CentroCosto' type='numeric' required='no'>
	<cfargument name='id_Frente' type='numeric' required='no'>
	<cfargument name='id_Partida' type='numeric' required='no'>
	<cfif #arguments.id_Empresa# EQ ''>
		<cfset id_Empresa= "NULL">
	</cfif>
	<cfif isDefined("arguments.id_Obra")>
		<cfset id_Obra= #id_Obra#>
	<cfelse>
		<cfset id_Obra= "NULL">
	</cfif>
	<cfif isDefined("arguments.id_CentroCosto")>
		<cfset id_CentroCosto= #id_CentroCosto#>
	<cfelse>
		<cfset id_CentroCosto= "NULL">
	</cfif>
	<cfif isDefined("arguments.id_Frente")>
		<cfset id_FrenteParamSP= #id_Frente#>
	<cfelse>
		<cfset id_FrenteParamSP= "NULL">
	</cfif>
	<cfif isDefined("arguments.id_Partida")>
		<cfset id_PartidaParamSP= #id_Partida#>
	<cfelse>
		<cfset id_PartidaParamSP= "NULL">
	</cfif>

	<cftry>
		<cfquery datasource='#cnx#' name='RS_MostrarTotalParametizado' username='#user_sql#' password='#password_sql#'>
			EXECUTE Presupuestos_ObtenerImporteTotalParametrizado #id_Empresa#, #id_Obra#, #id_CentroCosto#, #id_FrenteParamSP#, #id_PartidaParamSP#
		</cfquery>
		<cfset Presupuestos.listado= TRUE>
		<cfset Presupuestos.tipoError= ''>
		<cfset Presupuestos.rs= RS_MostrarTotalParametizado>
		<cfset Presupuestos.mensaje= 'Se obtuvo el recordset de total de Presupuestos correctamente'>
		<cfreturn Presupuestos>
		<cfcatch type='database'>
			<cfset Presupuestos.listado= FALSE>
			<cfset Presupuestos.tipoError= 'database-indefinido'>
			<cfset Presupuestos.mensaje= 'Error en el acceso a base de datos TP(#cfcatch.Detail#)'>
			<cfreturn Presupuestos>
		</cfcatch>
	</cftry>
</cffunction>

<!---Funcion que elimina un detalle al presupuesto, antes validando el estatus del presupuesto(centro de costos) --->
<cffunction name='RSEliminarDetalleAPresupuesto' access='public' returntype='struct'>

	<cfargument name='id_Empresa' type='numeric' required='yes'>
	<cfargument name='id_Obra' type='numeric' required='yes'>
	<cfargument name='id_CentroCosto' type='numeric' required='yes'>
	<cfargument name='id_Frente' type='numeric' required='yes'>
	<cfargument name='id_Partida' type='numeric' required='yes'>
	<cfargument name='nd_Presupuesto' type='numeric' required='yes'>

	<!---Validaciones: ---->
	<!---Modificacion: 2009-07-31 --->
	<!--- 1.- Aqui se valida que el centro de costo, frente, partida no este cerrado (aun pendiente por definir exactamente)--->
	<cfquery datasource="#cnx#" name="RSCentroCostoCerrado" username='#user_sql#' password='#password_sql#'>
		EXECUTE Presupuestos_ObtenEstatusCentroCosto #id_Empresa#, #id_Obra#, #id_CentroCosto#
	</cfquery>
	<!--- 	Modificacion: 2009-07-31, ahora se pueden seguir eliminando detalles al presupeusto aunque este autorizado, y ahora se verificara que el centro de costos no este en sn_Abierto= 0
	<cfif #RSCentroCostoCerrado.sn_AutorizadoBase# EQ 1>
		<cfset Presupuestos.actualizado= FALSE>
		<cfset Presupuestos.mensaje= 'El presupuesto del centro de costos se encuentra cerrado y no se puede modificar'>
		<cfset Presupuestos.tipoError= 'validaciones'>
		<cfreturn Presupuestos>
	</cfif> --->
	<cfif #RSCentroCostoCerrado.sn_Abierto# NEQ 1>
		<cfset Presupuestos.eliminado= FALSE>
		<cfset Presupuestos.mensaje= 'El presupuesto del centro de costos se encuentra cerrado y no se puede modificar'>
		<cfset Presupuestos.tipoError= 'validaciones'>
		<cfreturn Presupuestos>
	</cfif>
	
	<!---2.- Tambien se validara que no se elimine un detalle que ya fue explosionado --->
	<cfquery datasource="#cnx#" name="RSEstatusDetalle" username='#user_sql#' password='#password_sql#'>
		EXECUTE Presupuestos_ObtenEstatusDetalle #id_Empresa#, #id_Obra#, #id_CentroCosto#, #id_Frente#, #id_Partida#, #nd_Presupuesto#
	</cfquery>
	<cfif #RSEstatusDetalle.sn_Explosion# EQ 1>
		<cfset Presupuestos.eliminado= FALSE>
		<cfset Presupuestos.mensaje= 'El presupuesto se encuentra explosionado y no se puede eliminar.'>
		<cfset Presupuestos.tipoError= 'validaciones'>
		<cfreturn Presupuestos>
	</cfif>
		
	<cftry>
		<cfquery datasource='#cnx#' name='RSEliminar' username='#user_sql#' password='#password_sql#'>
			EXECUTE Presupuestos_EliminarDetalle #id_Empresa#, #id_Obra#, #id_CentroCosto#, #id_Frente#, #id_Partida#, #nd_Presupuesto#
		</cfquery>
		<!---Log--->
		<cf_log datasource='#cnx#' username='#user_sql#' password='#password_sql#' id_Empresa="#Session.id_Empresa#" id_Obra="#Session.id_Obra#" id_Usuario="#Session.id_Usuario#" id_MenuOpcion="#Session.id_MenuOpcion#">
			<cfoutput>
				EXECUTE Presupuestos_EliminarDetalle #id_Empresa#, #id_Obra#, #id_CentroCosto#, #id_Frente#, #id_Partida#, #nd_Presupuesto#
			</cfoutput>
		</cf_log>
		<!---Termina Log --->
		<cfset Presupuestos.eliminado= TRUE>
		<cfset Presupuestos.tipoError= ''>
		<cfset Presupuestos.mensaje= 'Detalle de Presupuesto eliminado correctamente'>
		<cfreturn Presupuestos>
		<cfcatch type='database'>
			<cfset Presupuestos.eliminado= FALSE>
			<cfif cfcatch.NativeErrorCode EQ '547'>
				<cfset Presupuestos.mensaje= 'El Detalle del Presupuesto no se pudo eliminar ya que tiene registros relacionados'>
				<cfset Presupuestos.tipoError= 'database-integridad'>
			<cfelse>
				<cfset Presupuestos.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
				<cfset Presupuestos.tipoError= 'database-indefinido'>
			</cfif>
			<cfreturn Presupuestos>
		</cfcatch>
	</cftry>
</cffunction>

<!---Funcion que trae el estatus de un presupuesto (centro de costos) --->
<cffunction name="ObtenEstatusPresupuestoDeCentroCosto" returntype="struct" access="public">
	<cfargument name='id_Empresa' type='numeric' required='yes'>
	<cfargument name='id_Obra' type='numeric' required='yes'>
	<cfargument name='id_CentroCosto' type='numeric' required='no'>
	<cftry>
		<cfquery datasource='#cnx#' name='RSEstatus' username='#user_sql#' password='#password_sql#'>
			EXECUTE Presupuestos_ObtenEstatusCentroCosto #id_Empresa#, #id_Obra#, <cfif #id_CentroCosto# NEQ ''>#id_CentroCosto#<cfelse>null</cfif>
		</cfquery>
		<cfset Presupuestos.listado= TRUE>
		<cfset Presupuestos.rs= #RSEstatus#>
		<cfset Presupuestos.estatus= #RSEstatus.sn_AutorizadoBase#>
		<cfset Presupuestos.tipoError= ''>
		<cfset Presupuestos.mensaje= "El estatus del presupuesto se obtuvo correctamente">
		<cfreturn Presupuestos>
		<cfcatch type="any">
			<cfset Presupuestos.listado= FALSE>
			<cfset Presupuestos.tipoError= 'database-indefinido'>
			<cfset Presupuestos.mensaje= "Error en el acceso a base de datos EP(#cfcatch.Detail#)">
			<cfreturn Presupuestos>
		</cfcatch>
	</cftry>
</cffunction>

<!---Funcion que copia un presupuesto con todo y tarjetas, o solo las tarjetas de un centro de costos a otro --->
<cffunction name="CopiarEntreCentrosDeCostos" returntype="struct" access="public">
	<cfargument name="id_EmpresaDestino" type="numeric" required="yes">
	<cfargument name="id_ObraDestino" type="numeric" required="yes">
	<cfargument name="id_CentroCostoDestino" type="numeric" required="yes">
	<cfargument name="id_EmpresaOrigen" type="numeric" required="yes">
	<cfargument name="id_ObraOrigen" type="numeric" required="yes">
	<cfargument name="id_CentroCostoOrigen" type="numeric" required="yes">
	<cfargument name="nivel_copiado" type="string" required="yes">
	
	<cftry>
		<cfquery name="RSCopiar" datasource="#cnx#" username="#user_sql#" password="#password_sql#">
			EXECUTE Presupuestos_CopiarEntreCentrosDeCostos '#nivel_copiado#', #id_EmpresaDestino#, #id_ObraDestino#, #id_CentroCostoDestino#, #id_EmpresaOrigen#, #id_ObraOrigen#, #id_CentroCostoOrigen#
		</cfquery>
		<!---Log--->
		<cf_log datasource='#cnx#' username='#user_sql#' password='#password_sql#' id_Empresa="#Session.id_Empresa#" id_Obra="#Session.id_Obra#" id_Usuario="#Session.id_Usuario#" id_MenuOpcion="#Session.id_MenuOpcion#">
			<cfoutput>
				EXECUTE Presupuestos_CopiarEntreCentrosDeCostos '#nivel_copiado#', #id_EmpresaDestino#, #id_ObraDestino#, #id_CentroCostoDestino#, #id_EmpresaOrigen#, #id_ObraOrigen#, #id_CentroCostoOrigen#
			</cfoutput>
		</cf_log>
		<!---Termina Log --->
		<cfset Presupuestos.copiado= TRUE>
		<cfset Presupuestos.tipoError= "">
		<cfset Presupuestos.mensaje= "El presupuesto se ha copiado correctamente">
		<cfcatch type="database">
			<cfset Presupuestos.copiado= FALSE>
			<cfset Presupuestos.tipoError= "database-indefinido">
			<cfset Presupuestos.mensaje= "Ha ocurrido un error en el acceso a base de datos">
		</cfcatch>
	</cftry>
	<cfreturn Presupuestos>
</cffunction>

<!---No se usa de momento --->
<cffunction name="GeneraExplosionInsumos" returntype="struct" access="public">
	<cfargument name="id_Empresa" type="numeric" required="yes">
	<cfargument name="id_Obra" type="numeric" required="yes">
	<cfargument name="id_CentroCosto" type="numeric" required="yes">
	<cfargument name="id_Frente" type="numeric" required="no">
	<cfargument name="id_Partida" type="numeric" required="no">
	<cfargument name="nd_Presupuesto" type="numeric" required="no">
	<cfargument name="sn_MultiplicaFactorFrente" type="numeric" required="no">
	
	<cfif isDefined("arguments.id_Frente")>
		<cfset id_FrenteParamSP= #id_Frente#>
	<cfelse>
		<cfset id_FrenteParamSP= "NULL">
	</cfif>
	<cfif isDefined("arguments.id_Partida")>
		<cfset id_PartidaParamSP= #id_Partida#>
	<cfelse>
		<cfset id_PartidaParamSP= "NULL">
	</cfif>
	<cfif isDefined("arguments.nd_Presupuesto")>
		<cfset nd_PresupuestoParamSP= #nd_Presupuesto#>
	<cfelse>
		<cfset nd_PresupuestoParamSP= "NULL">
	</cfif>
	<cfif isDefined("arguments.sn_MultiplicaFactorFrente")>
		<cfset sn_MultiplicaFactorFrenteParamSP= #sn_MultiplicaFactorFrente#>
	<cfelse>
		<cfset sn_MultiplicaFactorFrenteParamSP= "NULL">
	</cfif>
	
	<cftry>
		<cfquery name="RS_Explosion" datasource="#cnx#" username="#user_sql#" password="#password_sql#">
			EXECUTE Presupuestos_GeneraExplosionInsumos #id_Empresa#, #id_Obra#, #id_CentroCosto#, #id_FrenteParamSP#, #id_PartidaParamSP#, #nd_PresupuestoParamSP#, #sn_MultiplicaFactorFrenteParamSP#
		</cfquery>
		<!---Log--->
		<cf_log datasource='#cnx#' username='#user_sql#' password='#password_sql#' id_Empresa="#Session.id_Empresa#" id_Obra="#Session.id_Obra#" id_Usuario="#Session.id_Usuario#" id_MenuOpcion="#Session.id_MenuOpcion#">
			<cfoutput>
				EXECUTE Presupuestos_GeneraExplosionInsumos #id_Empresa#, #id_Obra#, #id_CentroCosto#, #id_FrenteParamSP#, #id_PartidaParamSP#, #nd_PresupuestoParamSP#, #sn_MultiplicaFactorFrenteParamSP#
			</cfoutput>
		</cf_log>
		<!---Termina Log --->
		<cfset Presupuestos.explosion= TRUE>
		<cfset Presupuestos.tipoError= "">
		<cfset Presupuestos.mensaje= "Se ha generado la explosi�n de insumos correctamente">
		<cfcatch type="database">
			<cfset Presupuestos.explosion= FALSE>
			<cfset Presupuestos.tipoError= "database-indefinido">
			<cfset Presupuestos.mensaje= "Ha ocurrido un error en el acceso a base de dato<br/>(#cfcatch.Detail#)">
		</cfcatch>
	</cftry>
	<cfreturn Presupuestos>
</cffunction>

<!---Funcion que genera la explosion de insumos segun un arreglo de detalles de presupuesto--->
<cffunction name="GeneraExplosionInsumosDeListaDetalles" returntype="struct" access="public">
	<cfargument name="id_Empresa" type="numeric" required="no">
	<cfargument name="id_Obra" type="numeric" required="yes">
	<cfargument name="id_CentroCosto" type="numeric" required="yes">
	<cfargument name="id_Frente" type="numeric" required="yes">
	<cfargument name="id_Partida" type="numeric" required="yes">
	<cfargument name="nd_Presupuestos" type="array" required="yes">
	<cfargument name="sn_MultiplicaFactorFrente" type="numeric" required="no">
	
	<cfif #id_Empresa# EQ ''>
		<cfset id_Empresa = "NULL">
	</cfif>
	<cfif isDefined("arguments.sn_MultiplicaFactorFrente")>
		<cfset sn_MultiplicaFactorFrenteParamSP= #sn_MultiplicaFactorFrente#>
	<cfelse>
		<cfset sn_MultiplicaFactorFrenteParamSP= "NULL">
	</cfif>	
	
	<cftransaction>
		<cftry>
			<!---Se recorren los presupuestos para validaciones --->
			<cfloop array="#nd_Presupuestos#" index="nd_Presupuesto">
				<!--- Se obtiene la info del presupuesto--->
				<cfquery name="RS_ObtenerPresupuesto" datasource="#cnx#" username="#user_sql#" password="#password_sql#">
					EXECUTE bop_Presupuestos_ObtenerPorID #id_Empresa#, #id_Obra#, #id_CentroCosto#, #id_Frente#, #id_Partida#, #nd_Presupuesto#
				</cfquery>o
				<cfquery name="RS_EstaExplosionado" datasource="#cnx#" username="#user_sql#" password="#password_sql#">
					EXECUTE Presupuestos_ObtenSnExplosionado #id_Empresa#, #id_Obra#, #id_CentroCosto#, #id_Frente#, #id_Partida#, #nd_Presupuesto#
				</cfquery>
				<!---Se valida que no este explosionado ya --->
				<cfif #RS_EstaExplosionado.sn_Explosion# EQ 1>
					<cfthrow type="presupuesto_ya_explosionado" message="El detalle de presupuesto #nd_Presupuesto# de la tarjeta #RS_ObtenerPresupuesto.id_Tarjeta# ya se encuentra explosionado, favor de verificar">
				</cfif>
			</cfloop>
			
			<!---Se recorren los presupuestos para generarles la explosion --->
			<cfloop array="#nd_Presupuestos#" index="nd_Presupuesto">
            	<cfset detalle = #nd_Presupuesto#>
				<cfquery name="RS_Explosion" datasource="#cnx#" username="#user_sql#" password="#password_sql#">
					EXECUTE Presupuestos_GeneraExplosionInsumos #id_Empresa#, #id_Obra#, #id_CentroCosto#, #id_Frente#, #id_Partida#, #nd_Presupuesto#, #sn_MultiplicaFactorFrenteParamSP#
				</cfquery>
				<!---Log--->
				<cf_log datasource='#cnx#' username='#user_sql#' password='#password_sql#' id_Empresa="#Session.id_Empresa#" id_Obra="#Session.id_Obra#" id_Usuario="#Session.id_Usuario#" id_MenuOpcion="#Session.id_MenuOpcion#">
					<cfoutput>
						EXECUTE Presupuestos_GeneraExplosionInsumos #id_Empresa#, #id_Obra#, #id_CentroCosto#, #id_Frente#, #id_Partida#, #nd_Presupuesto#, #sn_MultiplicaFactorFrenteParamSP#
					</cfoutput>
				</cf_log>
				<!---Termina Log --->
			</cfloop>
			<cftransaction action="commit"/>
			<cfset Presupuestos.explosion= TRUE>
			<cfset Presupuestos.tipoError= "">
			<cfset Presupuestos.mensaje= "Se ha generado la explosi�n de insumos correctamente">
			<cfcatch type="presupuesto_ya_explosionado">
				<cftransaction action="rollback"/>
				<cfset Presupuestos.explosion= FALSE>
				<cfset Presupuestos.tipoError= "validaciones">
				<cfset Presupuestos.mensaje= #cfcatch.Message#>
			</cfcatch>
			<cfcatch type="database">
				<cftransaction action="rollback"/>
				<cfset Presupuestos.explosion= FALSE>
				<cfset Presupuestos.tipoError= "database-indefinido">
				<cfset Presupuestos.mensaje= "Ha ocurrido un error en el acceso a base de datos (#cfcatch.Detail#)  - #detalle#">
			</cfcatch>
		</cftry>
	</cftransaction>
	<cfreturn Presupuestos>
</cffunction>

<!---Funcion que genera la explosion de insumos segun un arreglo de partidas de presupuesto--->
<cffunction name="GeneraExplosionInsumosDeListaPartidas" returntype="struct" access="public">
	<cfargument name="id_Empresa" type="numeric" required="yes">
	<cfargument name="id_Obra" type="numeric" required="yes">
	<cfargument name="id_CentroCosto" type="numeric" required="yes">
	<cfargument name="id_Frente" type="numeric" required="yes">
	<cfargument name="id_Partidas" type="array" required="yes">
	<cfargument name="sn_MultiplicaFactorFrente" type="numeric" required="no">
	
	<cfif isDefined("arguments.sn_MultiplicaFactorFrente")>
		<cfset sn_MultiplicaFactorFrenteParamSP= #sn_MultiplicaFactorFrente#>
	<cfelse>
		<cfset sn_MultiplicaFactorFrenteParamSP= "NULL">
	</cfif>
	
	<cftransaction>
		<cftry>
			<cfloop array="#id_Partidas#" index="id_Partida">
            	<cfset partida = #id_Partida#>
				<cfquery name="RS_Explosion" datasource="#cnx#" username="#user_sql#" password="#password_sql#">
					EXECUTE Presupuestos_GeneraExplosionInsumos #id_Empresa#, #id_Obra#, #id_CentroCosto#, #id_Frente#, #id_Partida#, NULL, #sn_MultiplicaFactorFrenteParamSP#
				</cfquery>
				<!---Log--->
				<cf_log datasource='#cnx#' username='#user_sql#' password='#password_sql#' id_Empresa="#Session.id_Empresa#" id_Obra="#Session.id_Obra#" id_Usuario="#Session.id_Usuario#" id_MenuOpcion="#Session.id_MenuOpcion#">
					<cfoutput>
						EXECUTE Presupuestos_GeneraExplosionInsumos #id_Empresa#, #id_Obra#, #id_CentroCosto#, #id_Frente#, #id_Partida#, NULL, #sn_MultiplicaFactorFrenteParamSP#
					</cfoutput>
				</cf_log>
				<!---Termina Log --->
			</cfloop>
			<cftransaction action="commit"/>
			<cfset Presupuestos.explosion= TRUE>
			<cfset Presupuestos.tipoError= "">
			<cfset Presupuestos.mensaje= "Se ha generado la explosi�n de insumos correctamente">
			<cfcatch type="error_custom">
				<cftransaction action="rollback"/>
				<cfset Presupuestos.explosion= FALSE>
				<cfset Presupuestos.tipoError= "validaciones">
				<cfset Presupuestos.mensaje= #cfcatch.Message#>
			</cfcatch>
			<cfcatch type="database">
				<cftransaction action="rollback"/>
				<cfset Presupuestos.explosion= FALSE>
				<cfset Presupuestos.tipoError= "database-indefinido">
				<cfset Presupuestos.mensaje= "Ha ocurrido un error en el acceso a base de datos (#cfcatch.Detail#)  - #partida#">
			</cfcatch>
		</cftry>
	</cftransaction>
	<cfreturn Presupuestos>
</cffunction>

<!---Funcion que genera la explosion de insumos segun un arreglo de partidas de presupuesto--->
<cffunction name="GeneraExplosionInsumosDeListaFrentes" returntype="struct" access="public">
	<cfargument name="id_Empresa" type="numeric" required="yes">
	<cfargument name="id_Obra" type="numeric" required="yes">
	<cfargument name="id_CentroCosto" type="numeric" required="yes">
	<cfargument name="id_Frentes" type="array" required="yes">
	<cfargument name="sn_MultiplicaFactorFrente" type="numeric" required="no">
	
	<cfif isDefined("arguments.sn_MultiplicaFactorFrente")>
		<cfset sn_MultiplicaFactorFrenteParamSP= #sn_MultiplicaFactorFrente#>
	<cfelse>
		<cfset sn_MultiplicaFactorFrenteParamSP= "NULL">
	</cfif>

	<cfsetting requesttimeout="999999999">   
	
	<cftransaction>
		<cftry>
			<cfloop array="#id_Frentes#" index="id_Frente">
            	<cfset frente = #id_Frente#>

                <cfquery name="explo" datasource="#session.cnx#">
                    Explosion #id_Empresa#,#id_Obra#,#id_CentroCosto#,#id_Frente#,NULL,NULL
                </cfquery>
                
                <cfoutput query="explo">
                    <cfquery name="exec" datasource="#session.cnx#">
                        #explo.str_sql#
                    </cfquery>		
                </cfoutput>
                
				<!---<cfquery name="RS_Explosion" datasource="#cnx#" username="#user_sql#" password="#password_sql#">
					EXECUTE Presupuestos_GeneraExplosionInsumos #id_Empresa#, #id_Obra#, #id_CentroCosto#, #id_Frente#, NULL, NULL, #sn_MultiplicaFactorFrenteParamSP#
				</cfquery>
				Log
				<cf_log datasource='#cnx#' username='#user_sql#' password='#password_sql#' id_Empresa="#Session.id_Empresa#" id_Obra="#Session.id_Obra#" id_Usuario="#Session.id_Usuario#" id_MenuOpcion="#Session.id_MenuOpcion#">
					<cfoutput>
						EXECUTE Presupuestos_GeneraExplosionInsumos #id_Empresa#, #id_Obra#, #id_CentroCosto#, #id_Frente#, NULL, NULL, #sn_MultiplicaFactorFrenteParamSP#
					</cfoutput>
				</cf_log>--->
				<!---Termina Log --->
                
			</cfloop>
			<cftransaction action="commit"/>
			<cfset Presupuestos.explosion= TRUE>
			<cfset Presupuestos.tipoError= "">
			<cfset Presupuestos.mensaje= "Se ha generado la explosi�n de insumos correctamente">
			<cfcatch type="error_custom">
				<cftransaction action="rollback"/>
				<cfset Presupuestos.explosion= FALSE>
				<cfset Presupuestos.tipoError= "validaciones">
				<cfset Presupuestos.mensaje= #cfcatch.Message#>
			</cfcatch>
			<cfcatch type="database">
				<cftransaction action="rollback"/>
				<cfset Presupuestos.explosion= FALSE>
				<cfset Presupuestos.tipoError= "database-indefinido">
				<cfset Presupuestos.mensaje= "Ha ocurrido un error en el acceso a base de datos(#cfcatch.Detail#)  - #frente#">
			</cfcatch>
		</cftry>
	</cftransaction>
	<cfreturn Presupuestos>
</cffunction>

<!---Funcion que guarda lso tipos de ejecucion de tarjetas segun un arreglo de detalles de presupuesto--->
<cffunction name="GuardaTiposEjecucionDeListaDetalles" returntype="struct" access="public">
	<cfargument name="id_Empresa" type="string" required="yes">
	<cfargument name="id_Obra" type="numeric" required="yes">
	<cfargument name="id_CentroCosto" type="numeric" required="yes">
	<cfargument name="id_Frente" type="numeric" required="yes">
	<cfargument name="id_Partida" type="numeric" required="yes">
	<cfargument name="nd_Presupuestos" type="array" required="yes">
	<cfargument name="id_TiposEjecucionTarjetas" type="array" required="yes">
	
	<cftransaction>
		<cftry>
			<cfset ind= 0>
			<cfloop array="#nd_Presupuestos#" index="nd_Presupuesto">
				<cfset ind= ind + 1>
				<cfquery name="RS_Editar" datasource="#cnx#" username="#user_sql#" password="#password_sql#">
					EXECUTE Presupuestos_GuardaTipoEjecucion #id_Empresa#, #id_Obra#, #id_CentroCosto#, #id_Frente#, #id_Partida#, #nd_Presupuesto#, #id_TiposEjecucionTarjetas[ind]#
				</cfquery>
				<!---Log--->
				<cf_log datasource='#cnx#' username='#user_sql#' password='#password_sql#' id_Empresa="#Session.id_Empresa#" id_Obra="#Session.id_Obra#" id_Usuario="#Session.id_Usuario#" id_MenuOpcion="#Session.id_MenuOpcion#">
					<cfoutput>
						EXECUTE Presupuestos_GuardaTipoEjecucion #id_Empresa#, #id_Obra#, #id_CentroCosto#, #id_Frente#, #id_Partida#, #nd_Presupuesto#, #id_TiposEjecucionTarjetas[ind]#
					</cfoutput>
				</cf_log>
				<!---Termina Log --->
			</cfloop>
			<cftransaction action="commit"/>
			<cfset Presupuestos.editar= TRUE>
			<cfset Presupuestos.tipoError= "">
			<cfset Presupuestos.mensaje= "Se han guardado los tipos de ejecuci�n de tarjeta correctamente">
			<cfcatch type="database">
				<cftransaction action="rollback"/>
				<cfset Presupuestos.editar= FALSE>
				<cfset Presupuestos.tipoError= "database-indefinido">
				<cfset Presupuestos.mensaje= "Ha ocurrido un error en el acceso a base de datos">
			</cfcatch>
		</cftry>
	</cftransaction>
	<cfreturn Presupuestos>
</cffunction>

<!---Hebert GB - 2011-04-05 --->
<!---Funcion que guarda el tipo de ejecucion de tarjetas de un presupuesto--->
<cffunction name="GuardaTipoEjecucion" returntype="struct" access="public">
	<cfargument name="id_Empresa" type="numeric" required="yes">
	<cfargument name="id_Obra" type="numeric" required="yes">
	<cfargument name="id_CentroCosto" type="numeric" required="yes">
	<cfargument name="id_Frente" type="numeric" required="yes">
	<cfargument name="id_Partida" type="numeric" required="yes">
	<cfargument name="nd_Presupuesto" type="numeric" required="yes">
	<cfargument name="id_TipoEjecucionTarjeta" type="numeric" required="yes">
	
	<cfset Presupuestos= StructNew()>
	<cfset ValidacionesNegocio= "">
	
	<!---Se obtienen los datos del presupuesto --->
	<cfquery name="RS_Presupuesto" datasource="#cnx#" username="#user_sql#" password="#password_sql#">
		EXECUTE bop_Presupuestos_ObtenerPorID #id_Empresa#, #id_Obra#, #id_CentroCosto#, #id_Frente#, #id_Partida#, #nd_Presupuesto#
	</cfquery>
	
	<!---Reglas Negocio --->
	<!---Regla 1: No se podr� editar un tipo de ejecuci�n a un presupuesto si este ya fu� explosionado --->
	<cfif RS_Presupuesto.sn_Explosion EQ 1>
		<cfset ValidacionesNegocio= ValidacionesNegocio & "<br>No se puede editar el tipo de ejecuci�n del presupuesto porque ya est� explosionado">
	</cfif>

	<cfif ValidacionesNegocio NEQ "">
		<cfset Presupuestos.editar= FALSE>
		<cfset Presupuestos.tipoError= "validaciones">
		<cfset Presupuestos.mensaje= ValidacionesNegocio>
		<cfreturn Presupuestos>
	</cfif>	
	<cftry>
		<cfquery name="RS_Editar" datasource="#cnx#" username="#user_sql#" password="#password_sql#">
			EXECUTE Presupuestos_GuardaTipoEjecucion #id_Empresa#, #id_Obra#, #id_CentroCosto#, #id_Frente#, #id_Partida#, #nd_Presupuesto#, #id_TipoEjecucionTarjeta#
		</cfquery>
		<!---Log--->
		<cf_log datasource='#cnx#' username='#user_sql#' password='#password_sql#' id_Empresa="#Session.id_Empresa#" id_Obra="#Session.id_Obra#" id_Usuario="#Session.id_Usuario#" id_MenuOpcion="#Session.id_MenuOpcion#">
			<cfoutput>
				EXECUTE Presupuestos_GuardaTipoEjecucion #id_Empresa#, #id_Obra#, #id_CentroCosto#, #id_Frente#, #id_Partida#, #nd_Presupuesto#, #id_TipoEjecucionTarjeta#
			</cfoutput>
		</cf_log>
		<!---Termina Log --->
		<cfset Presupuestos.editar= TRUE>
		<cfset Presupuestos.tipoError= "">
		<cfset Presupuestos.mensaje= "Se ha guardado el tipo de ejecuci�n de tarjeta correctamente">
		<cfcatch type="database">
			<cfset Presupuestos.editar= FALSE>
			<cfset Presupuestos.tipoError= "database-indefinido">
			<cfset Presupuestos.mensaje= "Ha ocurrido un error en el acceso a base de datos">
		</cfcatch>
	</cftry>
	<cfreturn Presupuestos>
</cffunction>

<!---Hebert GB - 2011-04-05 --->
<!---Funcion que elimina los presupuestosinsumos de un presupuesto--->
<cffunction name="EliminaPresupuestosInsumos" returntype="struct" access="public">
	<cfargument name="id_Empresa" type="numeric" required="yes">
	<cfargument name="id_Obra" type="numeric" required="yes">
	<cfargument name="id_CentroCosto" type="numeric" required="yes">
	<cfargument name="id_Frente" type="numeric" required="yes">
	<cfargument name="id_Partida" type="numeric" required="yes">
	<cfargument name="nd_Presupuesto" type="numeric" required="yes">
	
	<cfset Presupuestos= StructNew()>
	<cfset ValidacionesNegocio= "">
	
	<!---Se obtienen los datos del presupuesto --->
	<cfquery name="RS_Presupuesto" datasource="#cnx#" username="#user_sql#" password="#password_sql#">
		EXECUTE bop_Presupuestos_ObtenerPorID #id_Empresa#, #id_Obra#, #id_CentroCosto#, #id_Frente#, #id_Partida#, #nd_Presupuesto#
	</cfquery>
	
	<!---Reglas Negocio --->
	<!---Regla 1: No se podr� eliminar los presupuestosinsumos a un presupuesto si este ya fu� explosionado --->
	<cfif RS_Presupuesto.sn_Explosion EQ 1>
		<cfset ValidacionesNegocio= ValidacionesNegocio & "<br>No se pueden eliminar los insumos de contrataci�n del presupuesto porque ya est� explosionado">
	</cfif>

	<cfif ValidacionesNegocio NEQ "">
		<cfset Presupuestos.editar= FALSE>
		<cfset Presupuestos.tipoError= "validaciones">
		<cfset Presupuestos.mensaje= ValidacionesNegocio>
		<cfreturn Presupuestos>
	</cfif>	
	<cftry>
		<cfquery name="RS_Editar" datasource="#cnx#" username="#user_sql#" password="#password_sql#">
			EXECUTE Presupuestos_EliminaInsumosContratacion #id_Empresa#, #id_Obra#, #id_CentroCosto#, #id_Frente#, #id_Partida#, #nd_Presupuesto#
		</cfquery>
		<!---Log--->
		<cf_log datasource='#cnx#' username='#user_sql#' password='#password_sql#' id_Empresa="#Session.id_Empresa#" id_Obra="#Session.id_Obra#" id_Usuario="#Session.id_Usuario#" id_MenuOpcion="#Session.id_MenuOpcion#">
			<cfoutput>
				EXECUTE Presupuestos_EliminaInsumosContratacion #id_Empresa#, #id_Obra#, #id_CentroCosto#, #id_Frente#, #id_Partida#, #nd_Presupuesto#
			</cfoutput>
		</cf_log>
		<!---Termina Log --->
		<cfset Presupuestos.editar= TRUE>
		<cfset Presupuestos.tipoError= "">
		<cfset Presupuestos.mensaje= "Se ha guardado el tipo de ejecuci�n de tarjeta correctamente">
		<cfcatch type="database">
			<cfset Presupuestos.editar= FALSE>
			<cfset Presupuestos.tipoError= "database-indefinido">
			<cfset Presupuestos.mensaje= "Ha ocurrido un error en el acceso a base de datos">
		</cfcatch>
	</cftry>
	<cfreturn Presupuestos>
</cffunction>

<!---Funcion para saber si un insumo existe en un presupuesto --->
<cffunction name="VerificaExisteInsumoEnPresupuesto" access="public" returntype="struct">
	<cfargument name="id_Empresa" type="numeric" required="yes">
	<cfargument name="id_Obra" type="numeric" required="yes">
	<cfargument name="id_CentroCosto" type="numeric" required="yes">
	<cfargument name="id_Insumo" type="string" required="yes">
	
	<cftry>
		<cfquery name="RSVerificar" datasource="#cnx#" username="#user_sql#" password="#password_sql#">
			EXECUTE ExplosionInsumos_ExistePresupuesto #id_Empresa#, #id_Obra#, #id_CentroCosto#, '#id_Insumo#'
		</cfquery>
		<cfset Presupuesto.listado= TRUE>
		<cfset Presupuesto.tipoError= ''>
		<cfset Presupuesto.rs= RSVerificar>
		<cfset Presupuesto.mensaje= 'Se obtuvo el recordset correctamente'>
		<cfreturn Presupuesto>
		<cfcatch type='database'>
			<cfset Presupuesto.listado= FALSE>
			<cfset Presupuesto.tipoError= 'database-indefinido'>
			<cfset Presupuesto.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn Presupuesto>
		</cfcatch>
	</cftry>
	<cfreturn Presupuesto>
</cffunction>

<!---Funcion para saber si un insumo existe en un presupuesto o es universal (Requisiciones) --->
<cffunction name="VerificaExisteInsumoEnPresupuestoOUniversal" access="public" returntype="struct">
	<cfargument name="id_Empresa" type="numeric" required="yes">
	<cfargument name="id_Obra" type="numeric" required="yes">
	<cfargument name="id_CentroCosto" type="numeric" required="yes">
	<cfargument name="id_Insumo" type="string" required="yes">
	
	<cftry>
		<cfquery name="RSVerificar" datasource="#cnx#" username="#user_sql#" password="#password_sql#">
			EXECUTE Insumos_EnPresupuestoOUniversal #id_Empresa#, #id_Obra#, #id_CentroCosto#, '#id_Insumo#'
		</cfquery>
		<cfset Presupuesto.listado= TRUE>
		<cfset Presupuesto.tipoError= ''>
		<cfset Presupuesto.rs= RSVerificar>
		<cfset Presupuesto.mensaje= 'Se obtuvo el recordset correctamente'>
		<cfreturn Presupuesto>
		<cfcatch type='database'>
			<cfset Presupuesto.listado= FALSE>
			<cfset Presupuesto.tipoError= 'database-indefinido'>
			<cfset Presupuesto.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn Presupuesto>
		</cfcatch>
	</cftry>
	<cfreturn Presupuesto>
</cffunction>

<cffunction name="AutorizaPresupuestoDeCentroDeCostos" returntype="struct" access="public">
	<cfargument name="id_Empresa" type="numeric" required="yes">
	<cfargument name="id_Obra" type="numeric" required="yes">
	<cfargument name="id_CentroCosto" type="numeric" required="yes">
	
	<cftry>
		<cfquery name="RS_Autorizar" datasource="#cnx#" username="#user_sql#" password="#password_sql#">
			EXECUTE Presupuestos_Autoriza #id_Empresa#, #id_Obra#, #id_CentroCosto#
		</cfquery>
		<!---Log--->
		<cf_log datasource='#cnx#' username='#user_sql#' password='#password_sql#' id_Empresa="#Session.id_Empresa#" id_Obra="#Session.id_Obra#" id_Usuario="#Session.id_Usuario#" id_MenuOpcion="#Session.id_MenuOpcion#">
			<cfoutput>
				EXECUTE Presupuestos_Autoriza #id_Empresa#, #id_Obra#, #id_CentroCosto#
			</cfoutput>
		</cf_log>
		<!---Termina Log --->
		<cfset Presupuestos.autorizado= TRUE>
		<cfset Presupuestos.tipoError= "">
		<cfset Presupuestos.mensaje= "Se ha autorizado el presupuesto correctamente">
	<cfcatch type="database">
		<cfset Presupuestos.autorizado= FALSE>
		<cfset Presupuestos.tipoError= "database-indefinido">
		<cfset Presupuestos.mensaje= "Ha ocurrido un error en el acceso a base de datos">
	</cfcatch>
	</cftry>
	<cfreturn Presupuestos>
</cffunction>


<cffunction name="DesGeneraExplosionInsumos" returntype="struct" access="public">
	<cfargument name="id_Empresa" type="numeric" required="yes">
	<cfargument name="id_Obra" type="numeric" required="yes">
	<cfargument name="id_CentroCosto" type="numeric" required="yes">
	<cfargument name="id_Frente" type="numeric" required="yes">
	<cfargument name="id_Partida" type="numeric" required="yes">
	<cfargument name="nd_Presupuesto" type="numeric" required="yes">
	<cfargument name="sn_MultiplicaFactorFrente" type="numeric" required="no">
	
	<cfif isDefined("arguments.sn_MultiplicaFactorFrente")>
		<cfset sn_MultiplicaFactorFrenteParamSP= #sn_MultiplicaFactorFrente#>
	<cfelse>
		<cfset sn_MultiplicaFactorFrenteParamSP= "NULL">
	</cfif>
	
	<cftry>
		<cfquery name="RS_Explosion" datasource="#cnx#" username="#user_sql#" password="#password_sql#">
			EXECUTE Presupuestos_GeneraDesExplosionInsumos #id_Empresa#, #id_Obra#, #id_CentroCosto#, #id_Frente#, #id_Partida#, #nd_Presupuesto#, #sn_MultiplicaFactorFrenteParamSP#
		</cfquery>
		<!---Log--->
		<cf_log datasource='#cnx#' username='#user_sql#' password='#password_sql#' id_Empresa="#Session.id_Empresa#" id_Obra="#Session.id_Obra#" id_Usuario="#Session.id_Usuario#" id_MenuOpcion="#Session.id_MenuOpcion#">
			<cfoutput>
			EXECUTE Presupuestos_GeneraDesExplosionInsumos #id_Empresa#, #id_Obra#, #id_CentroCosto#, #id_Frente#, #id_Partida#, #nd_Presupuesto#, #sn_MultiplicaFactorFrenteParamSP#
			</cfoutput>
		</cf_log>
		<!---Termina Log --->
		<cfset Presupuestos.desexplosion= TRUE>
		<cfset Presupuestos.tipoError= "">
		<cfset Presupuestos.mensaje= "Se ha generado la desexplosi�n de insumos correctamente">
		<cfcatch type="database">
			<cfset Presupuestos.desexplosion= FALSE>
			<cfset Presupuestos.tipoError= "database-indefinido">
			<cfset Presupuestos.mensaje= "Ha ocurrido un error en el acceso a base de datos(#cfcatch.Detail#)">
		</cfcatch>
	</cftry>
	<cfreturn Presupuestos>
</cffunction>


<!---Funcion que trae el recordset de una explosion de un presupuesto--->
<cffunction name="RSMostrarExplosionDePresupuestoConRestantes" access="remote" returntype="struct">
	<cfargument name="id_Empresa" type="string" required="yes">
	<cfargument name="id_Obra" type="string" required="yes">
	<cfargument name="id_CentroCosto" type="string" required="yes">
	<cfargument name="id_Frente" type="string" required="yes">
	<cfargument name="id_Partida" type="string" required="yes">
	<cfargument name="nd_Presupuesto" type="string" required="yes">
	
	<cfif Not IsNumeric(#id_Empresa#) OR Not IsNumeric(#id_Obra#) OR Not IsNumeric(#id_CentroCosto#) OR Not IsNumeric(#id_Frente#) OR Not IsNumeric(#id_Partida#) OR Not IsNumeric(#nd_Presupuesto#)>
		<cfset Presupuestos.listado= FALSE>
		<cfset Presupuestos.tipoError= 'validaciones'>
		<cfset Presupuestos.mensaje= 'Necesita definir un presupuesto'>
		<cfreturn Presupuestos>	
	</cfif>
	<cftry>
		<cfquery datasource='#cnx#' name='RS_MostrarExplosion' username='#user_sql#' password='#password_sql#'>
			EXECUTE Presupuestos_ObtenerExplosionInsumosConRestantes #id_Empresa#, #id_Obra#, #id_CentroCosto#, #id_Frente#, #id_Partida#, #nd_Presupuesto#, 1
		</cfquery>
		<cfset Presupuestos.listado= TRUE>
		<cfset Presupuestos.tipoError= ''>
		<cfset Presupuestos.rs= RS_MostrarExplosion>
		<cfset Presupuestos.mensaje= 'Se obtuvo el recordset de explosi�n correctamente'>
		<cfreturn Presupuestos>
		<cfcatch type='database'>
			<cfset Presupuestos.listado= FALSE>
			<cfset Presupuestos.tipoError= 'database-indefinido'>
			<cfset Presupuestos.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn Presupuestos>
		</cfcatch>
	</cftry>
</cffunction>

<!---Funcion que trae el recordset de una explosion de un presupuesto para estimaciones (filtrados por tipo de ejecucion= 2 y 4: destajos y subcontratos)--->
<cffunction name="RSMostrarExplosionDePresupuestoConRestantesParaEstimar" access="remote" returntype="struct">
	<cfargument name="id_Empresa" type="string" required="yes">
	<cfargument name="id_Obra" type="string" required="yes">
	<cfargument name="id_CentroCosto" type="string" required="yes">
	<cfargument name="id_Frente" type="string" required="yes">
	<cfargument name="id_Partida" type="string" required="yes">
		
	<cfif Not IsNumeric(#id_Empresa#) OR Not IsNumeric(#id_Obra#) OR Not IsNumeric(#id_CentroCosto#) OR Not IsNumeric(#id_Frente#)  OR Not IsNumeric(#id_Partida#) <!---OR Not IsNumeric(#nd_Presupuesto#) --->>
		<cfset Presupuestos.listado= FALSE>
		<cfset Presupuestos.tipoError= 'validaciones'>
		<cfset Presupuestos.mensaje= 'Necesita definir un presupuesto'>
		<cfreturn Presupuestos>	
	</cfif>
	<cftry>
		<!---
			YA NO ES NECESARIA LA EXPLOSION, AHORA SE TRAERAN LOS DATOS DE LA EXPLOSION DE INSUMOS
		<cfquery datasource='#cnx#' name='RS_MostrarExplosion' username='#user_sql#' password='#password_sql#'>
			EXECUTE Presupuestos_ObtenerExplosionInsumosConRestantesParaEstimar #id_Empresa#, #id_Obra#, #id_CentroCosto#, #id_Frente#, #id_Partida#, null, 1
		</cfquery>
		--->
		<cfquery datasource='#cnx#' name='RS_MostrarExplosion' username='#user_sql#' password='#password_sql#'>
			EXECUTE Presupuestos_ObtenerExplosionInsumosConRestantesParaEstimarCoppel #id_Empresa#, #id_Obra#, #id_CentroCosto#, #id_Frente#, <cfif #id_Partida# EQ "">NULL<cfelse>#id_Partida#</cfif>
		</cfquery>
		<cfset Presupuestos.listado= TRUE>
		<cfset Presupuestos.tipoError= ''>
		<cfset Presupuestos.rs= RS_MostrarExplosion>
		<cfset Presupuestos.mensaje= 'Se obtuvo el recordset de explosi�n correctamente'>
		<cfreturn Presupuestos>
		<cfcatch type='database'>
			<cfset Presupuestos.listado= FALSE>
			<cfset Presupuestos.tipoError= 'database-indefinido'>
			<cfset Presupuestos.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn Presupuestos>
		</cfcatch>
	</cftry>
</cffunction>

<!---Funcion que trae la descripcion de un presupuesto (de_Tarjeta) por su id para bindeo--->
<cffunction name="getPresupuestoPorID" access="remote" returntype="string">
	<cfargument name="id_Empresa" type="numeric" required="yes">
	<cfargument name="id_Obra" type="numeric" required="yes">
	<cfargument name="id_CentroCosto" type="string" required="yes">
	<cfargument name="id_Frente" type="string" required="yes">
	<cfargument name="id_Partida" type="string" required="yes">
	<cfargument name="nd_Presupuesto" type="string" required="yes">
	<cfif Not IsNumeric(#id_CentroCosto#) OR Not IsNumeric(#id_Frente#) OR Not IsNumeric(#id_Partida#) OR Not IsNumeric(#nd_Presupuesto#)>
		<cfreturn "">
	<cfelse>
		<cfquery name="RS" datasource='#cnx#' username='#user_sql#' password='#password_sql#'>
			EXECUTE Presupuestos_ObtenDescripcionPorID #id_Empresa#, #id_Obra#, #id_CentroCosto#, #id_Frente#, #id_Partida#, #nd_Presupuesto#
		</cfquery>
		<cfreturn RS.de_Tarjeta>
	</cfif>
</cffunction>


<!---Funcion que devuelve un recordset de presupuestos para pops de busqueda --->
<cffunction name='RSMostrarPresupuestosParaPopBusquedaEstimaciones' access='public' returntype='struct'>
	<cfargument name="id_Empresa" required="yes">
	<cfargument name="id_Obra" required="yes">
	<cfargument name="id_CentroCosto" required="yes">
	<cfargument name="id_Frente" required="yes">
	<cfargument name="id_Partida" required="yes">
	
	<cfargument name="id_Tarjeta" required="no" default="">
	<cfargument name="de_Tarjeta" required="no" default="">

	<cfif #id_Tarjeta# EQ ''>
		<cfset id_TarjetaParamSP= "NULL">
	<cfelse>
		<cfset id_TarjetaParamSP= #id_Tarjeta#>
	</cfif>

	<cftry>
		<cfquery datasource='#cnx#' name='RS_MostrarPresupuestos' username='#user_sql#' password='#password_sql#'>
			EXECUTE Presupuestos_ObtenerParaPopBusquedaDestajoYSubContrato #id_Empresa#, #id_Obra#, #id_CentroCosto#, #id_Frente#, #id_Partida#, '#id_TarjetaParamSP#', <cfif #de_Tarjeta# EQ "">NULL<cfelse>'#de_Tarjeta#'</cfif>
		</cfquery>
		<cfset Presupuestos.listado= TRUE>
		<cfset Presupuestos.tipoError= ''>
		<cfset Presupuestos.rs= RS_MostrarPresupuestos>
		<cfset Presupuestos.mensaje= 'Se obtuvo el recordset de Presupuestos correctamente'>
		<cfreturn Presupuestos>
		<cfcatch type='database'>
			<cfset Presupuestos.listado= FALSE>
			<cfset Presupuestos.tipoError= 'database-indefinido'>
			<cfset Presupuestos.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn Presupuestos>
		</cfcatch>
	</cftry>
</cffunction>

<!---Hebert GB - 2011-02-14 --->
<!---Funcion que agrega una tarjeta de una fusi�n a un presupuesto en base a un arreglo de presupuestos--->
<cffunction name='RSAgregarTarjetaFusionAPresupuesto' access='public' returntype='struct' output="no">
	<cfargument name='id_Empresa' type='numeric' required='yes'>
	<cfargument name='id_Obra' type='numeric' required='yes'>
	<cfargument name='id_CentroCosto' type='numeric' required='yes'>
	<cfargument name='id_Frente' type='numeric' required='yes'>
	<cfargument name='id_Partida' type='numeric' required='yes'>
	<cfargument name='id_Tarjeta' type='string' required='yes'>
	<cfargument name='de_Tarjeta' type='string' required='yes'>
	<cfargument name='id_UnidadMedida' type='numeric' required='yes'>
	<cfargument name='id_TipoEjecucionTarjeta' type='numeric' required='yes'>
	<cfargument name='PresupuestosParaFusion' type='array' required='yes'>
	
	<!---Se inician valores por default --->
	<cfset id_NaturalezaTarjeta= 1><!---Base --->
	<cfset nu_Cantidad= 1>
	<cfset sn_Explosion= 0>
	<cfset sn_Autorizado= 1>
	<cfset sn_Fusionado= 0>
	<cfset sn_Fusion= 1> 
	<cfset im_TarjetaPrecio= 0>
	
	<cfset Presupuestos= StructNew()>
	<cftransaction>
		<cftry>
			<!---Validaciones: --->
			
			<!--- 1.- Aqui se valida que el centro de costo, frente, partida no este cerrado (aun pendiente por definir exactamente)--->
			<cfquery datasource="#cnx#" name="RSCentroCostoCerrado" username='#user_sql#' password='#password_sql#'>
				EXECUTE Presupuestos_ObtenEstatusCentroCosto #id_Empresa#, #id_Obra#, #id_CentroCosto#
			</cfquery>
			<cfif RSCentroCostoCerrado.sn_Abierto NEQ 1>
				<cfthrow detail="El presupuesto del centro de costos se encuentra cerrado y no se puede modificar" message="El presupuesto del centro de costos se encuentra cerrado y no se puede modificar" type="validaciones">
			</cfif>
			
			<!--- Si pasa las validaciones explicitas, entonces se empiezan a agregar las tarjetas con sus validaciones implicitas--->
			
			<!--- Se agrega la tarjeta nueva--->
			<cfinvoke component="construnet.componentes.tarjetas" method="RSAgregarTarjetas" returnvariable="RSAgregarTarjeta" id_Empresa="#id_Empresa#" id_Obra="#id_Obra#" id_CentroCosto="#id_CentroCosto#" id_Tarjeta="#id_Tarjeta#" de_Tarjeta="#de_Tarjeta#" id_UnidadMedida="#id_UnidadMedida#" im_TarjetaPrecio="#im_TarjetaPrecio#">
			<cfif RSAgregarTarjeta.tipoError NEQ "">
				<cfthrow detail="#RSAgregarTarjeta.mensaje#" message="#RSAgregarTarjeta.mensaje#" type="validaciones">				
			</cfif>
			
			<!---Ya no se anidaran las tarjetas, sino que se tomar�n todas las tarjetas anidadas e insumos anidados y se anidar�n a la nueva tarjeta ya agrupados --->
			<!---Se agregan sus tarjetas anidadas --->
			<!--- 
			<cfloop array="#PresupuestosParaFusion#" index="PresupuestoIndice">
				<!--- Se valida la tarjeta por anidar--->
				<cfinvoke component="construnet.componentes.tarjetasanidadas" method="ValidarTarjetaAnidadaPorAgregar" returnvariable="RSValidarTarjetaAnidada" id_Empresa="#id_Empresa#" id_Obra="#id_Obra#" id_CentroCosto="#id_CentroCosto#" id_Tarjeta="#arguments.id_Tarjeta#" id_TarjetaAnidada="#PresupuestoIndice.id_Tarjeta#">
				<cfif RSValidarTarjetaAnidada.EsValido NEQ TRUE>
					<cfthrow detail="#RSValidarTarjetaAnidada.Mensaje#" message="#RSValidarTarjetaAnidada.Mensaje#" type="validaciones">
				</cfif>
				<cfinvoke component="construnet.componentes.tarjetasanidadas" method="AgregarTarjetaAnidadaATarjeta" returnvariable="RSAgregarTarjetaAnidada" id_Empresa="#id_Empresa#" id_Obra="#id_Obra#" id_CentroCosto="#id_CentroCosto#" id_Tarjeta="#arguments.id_Tarjeta#" id_TarjetaAnidada="#PresupuestoIndice.id_Tarjeta#" nu_Cantidad="#PresupuestoIndice.nu_Cantidad#">
				<cfif #RSAgregarTarjetaAnidada.tipoError# NEQ "">
					<cfthrow detail="#RSAgregarTarjetaAnidada.mensaje#" message="#RSAgregarTarjetaAnidada.mensaje#" type="validaciones">
				</cfif>
			</cfloop>
			--->
			
			<!---Se acumulan las tarjetas anidadas e insumos anidados de cada tarjeta de cada presupuesto --->
			<cfloop array="#PresupuestosParaFusion#" index="PresupuestoIndice">
				<cfquery datasource='#cnx#' username='#user_sql#' password='#password_sql#'>
					EXECUTE Tarjetas_AcumulaTarjetaDePresupuesto #id_Empresa#, #id_Obra#, #id_CentroCosto#, #arguments.id_Tarjeta#, #PresupuestoIndice.id_Tarjeta#, #PresupuestoIndice.nu_Cantidad#
				</cfquery>
			</cfloop>
			
			<!---Se trae el siguiente nd_Presupuesto por agregar en el frente, partida--->
			<cfquery datasource='#cnx#' name='RSUltimoPresupuesto' username='#user_sql#' password='#password_sql#'>
				EXECUTE bop_Presupuestos_MostrarNextID #id_Empresa#, #id_Obra#, #id_CentroCosto#, #id_Frente#, #id_Partida#
			</cfquery>
			
			<!---Se agrega el detalle de presupuesto --->
			<cfquery datasource='#cnx#' name='RSAgregar' username='#user_sql#' password='#password_sql#'>
				EXECUTE Presupuestos_AgregarTarjeta #id_Empresa#, #id_Obra#, #id_CentroCosto#, #id_Frente#, #id_Partida#, '#id_Tarjeta#', #id_NaturalezaTarjeta#, #id_TipoEjecucionTarjeta#, #nu_Cantidad#, #sn_Explosion#, #sn_Autorizado#, #sn_Fusionado#, #sn_Fusion#
			</cfquery>
			<!---Log--->
			<cf_log datasource='#cnx#' username='#user_sql#' password='#password_sql#' id_Empresa="#Session.id_Empresa#" id_Obra="#Session.id_Obra#" id_Usuario="#Session.id_Usuario#" id_MenuOpcion="#Session.id_MenuOpcion#">
				<cfoutput>
					EXECUTE Presupuestos_AgregarTarjeta #id_Empresa#, #id_Obra#, #id_CentroCosto#, #id_Frente#, #id_Partida#, '#id_Tarjeta#', #id_NaturalezaTarjeta#, #id_TipoEjecucionTarjeta#, #nu_Cantidad#, #sn_Explosion#, #sn_Autorizado#, #sn_Fusionado#, #sn_Fusion#
				</cfoutput>
			</cf_log>
			<!---Termina Log --->
	
			<!--- Se registra la fusion en presupuestosfusiones y adem�s se marcan los presupuestos como fusionados--->
			<cfloop array="#PresupuestosParaFusion#" index="PresupuestoIndice">
				<cfquery datasource='#cnx#' name='RSAgregar' username='#user_sql#' password='#password_sql#'>
					EXECUTE PresupuestosFusiones_Agregar #id_Empresa#, #id_Obra#, #id_CentroCosto#, #arguments.id_Frente#, #arguments.id_Partida#, #RSUltimoPresupuesto.NextID#, #PresupuestoIndice.id_Frente#, #PresupuestoIndice.id_Partida#, #PresupuestoIndice.nd_Presupuesto#
				</cfquery>
			</cfloop>
			
			<!--- Se registran los basicos de las tarjetas que se anidaron en la nueva tarjeta del nuevo presupuesto--->
			<!---Ya no se necesita generar los basicos
			<cfquery datasource='#cnx#' name='RSAgregar' username='#user_sql#' password='#password_sql#'>
				EXECUTE Presupuestos_GeneraBasicosDeFusion #id_Empresa#, #id_Obra#, #id_CentroCosto#, #id_Frente#, #id_Partida#, #RSUltimoPresupuesto.NextID#
			</cfquery>
			--->
			
			<cfset Presupuestos.agregado= TRUE>
			<cfset Presupuestos.tipoError= ''>
			<cfset Presupuestos.mensaje= 'Tarjeta agregada al presupuesto correctamente'>
			<cfcatch type="validaciones">
				<cftransaction action="rollback"/>
				<cfset Presupuestos.agregado= FALSE>
				<cfset Presupuestos.tipoError= 'validaciones'>
				<cfset Presupuestos.mensaje= '#cfcatch.message#'>
			</cfcatch>
			<cfcatch type='database'>
				<cfset Presupuestos.agregado= FALSE>
				<cftransaction action="rollback"/>
				<cfif cfcatch.NativeErrorCode EQ '2627'>
					<cfset Presupuestos.tipoError= 'database-registro_duplicado'>
					<cfset Presupuestos.mensaje= 'La tarjeta no se puedo agregar al debido a que ya se encuentra repetido el detalle del presupuesto'>
				<cfelse>
					<cfset Presupuestos.tipoError= 'database-indefinido'>
					<cfset Presupuestos.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
				</cfif>
			</cfcatch>
			<cfcatch type="any">
				<cftransaction action="rollback"/>
				<cfset Presupuestos.agregado= FALSE>
				<cfset Presupuestos.tipoError= 'indefinido'>
				<cfset Presupuestos.mensaje= 'Error al agregar presupuesto(#cfcatch.Message#)'>
			</cfcatch>
		</cftry>
	</cftransaction>
	<cfreturn Presupuestos>
</cffunction>

<cffunction name='RSPresupuesto_ObtenerPresupuesto_SinManoObra' access='public' returntype='struct'>

	<cfargument name='id_Empresa' type='numeric' required='yes'>
	<cfargument name='id_Obra' type='numeric' required='yes'>
	<cfargument name='id_CentroCosto' type='numeric' required='yes'>
	<cfargument name='id_Frente' type='numeric' required='yes'>
	<cfargument name='id_Partida' type='numeric' required='yes'>
	<cfargument name='nd_Presupuesto' type='numeric' required='yes'>
	<cfargument name='id_TipoEjecucion' type='string' required='no' default="">
    <cfif #id_TipoEjecucion# EQ ''>
    	<cfset id_TipoEjecucion = "null">
	</cfif>
	<cftry>
		<cfquery datasource='#cnx#' name='RS_MostrarPresupuestos' username='#user_sql#' password='#password_sql#'>
			EXECUTE Presupuestos_ObtenerExplosionInsumosTarjeta #id_Empresa#, #id_Obra#, #id_CentroCosto#, #id_Frente#, #id_Partida#, #nd_Presupuesto#, #id_TipoEjecucion#
		</cfquery>
		<cfset Presupuestos.listado= TRUE>
		<cfset Presupuestos.tipoError= ''>
		<cfset Presupuestos.rs= RS_MostrarPresupuestos>
		<cfset Presupuestos.mensaje= 'Se obtuvo el recordset de Presupuestos correctamente'>
		<cfreturn Presupuestos>
		<cfcatch type='database'>
			<cfset Presupuestos.listado= FALSE>
			<cfset Presupuestos.tipoError= 'database-indefinido'>
			<cfset Presupuestos.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn Presupuestos>
		</cfcatch>
	</cftry>
</cffunction>


<!---Hebert GB - 2011-04-01 ---->

<!---Funcion que devuelve el importe de la explosion de insumos para tipos de ejecucion Subcontrato y Destajo () --->
<cffunction name='RSPresupuesto_ObtenerPresupuesto_ParaSubContrato' access='public' returntype='struct'>
	<cfargument name='id_Empresa' type='numeric' required='yes'>
	<cfargument name='id_Obra' type='numeric' required='yes'>
	<cfargument name='id_CentroCosto' type='numeric' required='yes'>
	<cfargument name='id_Frente' type='numeric' required='yes'>
	<cfargument name='id_Partida' type='numeric' required='yes'>
	<cfargument name='nd_Presupuesto' type='numeric' required='yes'>
	<cfargument name='id_TipoEjecucionSeleccionado' type='numeric' required='yes'>
	<cftry>
		<cfquery datasource='#cnx#' name='RS_MostrarPresupuestos' username='#user_sql#' password='#password_sql#'>
			EXECUTE Presupuestos_ObtenerExplosionInsumosParaSubContrato #id_Empresa#, #id_Obra#, #id_CentroCosto#, #id_Frente#, #id_Partida#, #nd_Presupuesto#, #id_TipoEjecucionSeleccionado#
		</cfquery>
		<cfset Presupuestos.listado= TRUE>
		<cfset Presupuestos.tipoError= ''>
		<cfset Presupuestos.rs= RS_MostrarPresupuestos>
		<cfset Presupuestos.mensaje= 'Se obtuvo el recordset de Presupuestos correctamente'>
		<cfreturn Presupuestos>
		<cfcatch type='database'>
			<cfset Presupuestos.listado= FALSE>
			<cfset Presupuestos.tipoError= 'database-indefinido'>
			<cfset Presupuestos.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn Presupuestos>
		</cfcatch>
	</cftry>
</cffunction>

<cffunction name="RS_ObtenerPresupuestoAsignado" access="public" returntype="struct">
	<cfargument name="id_Empresa" type="numeric" required="yes">
	<cfargument name="id_Obra" type="numeric" required="yes">
	<cfargument name="id_CentroCosto" type="numeric" required="yes">
	<cfargument name="id_Frente" type="numeric" required="yes">
	<cfargument name="id_Partida" type="numeric" required="yes">
	<cfargument name="nd_Presupuesto" type="numeric" required="yes">
	
	<cftry>
		<cfquery datasource='#cnx#' name='RS_MostrarPresupuestoRestante' username='#user_sql#' password='#password_sql#'>
			EXECUTE PresupuestosInsumos_ObtenerAsignado #id_Empresa#, #id_Obra#, #id_CentroCosto#, #id_Frente#, #id_Partida#, #nd_Presupuesto#
		</cfquery>
		<cfset PresupuestosInsumos.tipoError= ''>
		<cfset PresupuestosInsumos.rs= RS_MostrarPresupuestoRestante>
		<cfset PresupuestosInsumos.mensaje= 'Se obtuvo el recordset de Insumos de Presupuestos correctamente'>
		<cfcatch type='database'>
			<cfset PresupuestosInsumos.tipoError= 'database-indefinido'>
			<cfset PresupuestosInsumos.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
		</cfcatch>
	</cftry>
	<cfreturn PresupuestosInsumos>
</cffunction>

<!---Funcion que trae los Centros de costo de un presupuesto con su total--->
<cffunction name="RSMostrarCentroCostoConTotal" access="public" returntype="struct">
	<cfargument name="id_Empresa" type="string" required="yes">
	<cfargument name="id_Obra" type="numeric" required="yes">
	<cftry>
		<cfif id_Empresa EQ ''>
			<cfset id_Empresa = 'NULL'>
		</cfif>
		<cfquery datasource='#cnx#' name='RS_MostrarCentroCostoPresupuesto' username='#user_sql#' password='#password_sql#'>
			EXECUTE Presupuestos_ObtenerCentroCostoConTotal #id_Empresa#, #id_Obra#
		</cfquery>
		<cfset Frentes.listado= TRUE>
		<cfset Frentes.tipoError= ''>
		<cfset Frentes.rs= RS_MostrarCentroCostoPresupuesto>
		<cfset Frentes.mensaje= 'Se obtuvo el recordset de centro costo para presupuesto correctamente'>
		<cfreturn Frentes>
		<cfcatch type='database'>
			<cfset Frentes.listado= FALSE>
			<cfset Frentes.tipoError= 'database-indefinido'>
			<cfset Frentes.mensaje= 'Error en el acceso a base de datos CCT(#cfcatch.Detail#)'>
			<cfreturn Frentes>
		</cfcatch>
	</cftry>
</cffunction>
<!---Funcion que trae el estatus de un presupuesto (obra) --->
<cffunction name="ObtenEstatusPresupuestoDeObra" returntype="struct" access="public">
	<cfargument name='id_Empresa' type='numeric' required='yes'>
	<cfargument name='id_Obra' type='numeric' required='yes'>
	<cftry>
		<cfquery datasource='#cnx#' name='RSEstatus' username='#user_sql#' password='#password_sql#'>
			EXECUTE Presupuestos_ObtenEstatusObra #id_Empresa#, #id_Obra#
		</cfquery>
		<cfset Presupuestos.listado= TRUE>
		<cfset Presupuestos.rs= #RSEstatus#>
		<cfset Presupuestos.estatus= #RSEstatus.sn_AutorizadoBase#>
		<cfset Presupuestos.tipoError= ''>
		<cfset Presupuestos.mensaje= "El estatus del presupuesto se obtuvo correctamente">
		<cfreturn Presupuestos>
		<cfcatch type="any">
			<cfset Presupuestos.listado= FALSE>
			<cfset Presupuestos.tipoError= 'database-indefinido'>
			<cfset Presupuestos.mensaje= "Error en el acceso a base de datos EP(#cfcatch.Detail#)">
			<cfreturn Presupuestos>
		</cfcatch>
	</cftry>
</cffunction>

<!---Funcion para autorizar presupuesto de Obra. --->
<cffunction name="AutorizaPresupuestoDeObra" returntype="struct" access="public">
	<cfargument name="id_Obra" type="numeric" required="yes">
	<cftry>
		<cfquery name="RS_Autorizar" datasource="#cnx#" username="#user_sql#" password="#password_sql#">
			EXECUTE Presupuestos_AutorizaObra #id_Obra#
		</cfquery>
		<cfset Presupuestos.autorizado= TRUE>
		<cfset Presupuestos.tipoError= "">
		<cfset Presupuestos.mensaje= "Se ha autorizado el presupuesto correctamente">
	<cfcatch type="database">
		<cfset Presupuestos.autorizado= FALSE>
		<cfset Presupuestos.tipoError= "database-indefinido">
		<cfset Presupuestos.mensaje= "Ha ocurrido un error en el acceso a base de datos">
	</cfcatch>
	</cftry>
	<cfreturn Presupuestos>
</cffunction>
<!---Funcion que trae los centros de un presupuesto para explosion--->
<cffunction name="RSMostrarCentrosParaExplosion" access="public" returntype="struct">
	<cfargument name="id_Empresa" type="string" required="yes">
	<cfargument name="id_Obra" type="numeric" required="yes">
	<cfif #id_Empresa# EQ ''>
		<cfset id_Empresa = 'NULL'>
	</cfif>
	<cftry>
		<cfquery datasource='#cnx#' name='RS_MostrarCentrosPresupuesto' username='#user_sql#' password='#password_sql#'>
			EXECUTE Presupuestos_ObtenerCentrosParaExplosion #id_Empresa#, #id_Obra#
		</cfquery>
		<cfset Frentes.listado= TRUE>
		<cfset Frentes.tipoError= ''>
		<cfset Frentes.rs= RS_MostrarCentrosPresupuesto>
		<cfset Frentes.mensaje= 'Se obtuvo el recordset de Centros para explosi�n correctamente'>
		<cfreturn Frentes>
		<cfcatch type='database'>
			<cfset Frentes.listado= FALSE>
			<cfset Frentes.tipoError= 'database-indefinido'>
			<cfset Frentes.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn Frentes>
		</cfcatch>
	</cftry>
</cffunction>
<!---Funcion que genera la explosion de insumos segun un arreglo de partidas de presupuesto--->
<cffunction name="GeneraExplosionInsumosDeListaCentros" returntype="struct" access="public">
	<cfargument name="id_Empresas" type="array" required="yes">
	<cfargument name="id_Obra" type="numeric" required="yes">
	<cfargument name="sn_MultiplicaFactorFrente" type="numeric" required="no">
	<cfargument name="id_CentroCostos" type="array">
	
	<cfif isDefined("arguments.sn_MultiplicaFactorFrente")>
		<cfset sn_MultiplicaFactorFrenteParamSP= #sn_MultiplicaFactorFrente#>
	<cfelse>
		<cfset sn_MultiplicaFactorFrenteParamSP= "NULL">
	</cfif>
    
	<cfsetting requesttimeout="999999999">
       
	<cftransaction>
		<cftry>
			<cfset ind = 0>
            <cfset centrocosto = 0>
			<cfloop array="#id_CentroCostos#" index="id_CentroCosto">
				<cfset ind= ind + 1>
                <cfset centrocosto = #id_CentroCosto#>

                <cfquery name="explo" datasource="#session.cnx#">
                    Explosion #id_Empresas[ind]#,#id_Obra#,#id_CentroCosto#,NULL,NULL,NULL
                </cfquery>
                
                <cfoutput query="explo">
                    <cfquery name="exec" datasource="#session.cnx#">
                        #explo.str_sql#
                    </cfquery>		
                </cfoutput>
				
                <!--- <cfquery name="RS_Explosion" datasource="#cnx#" username="#user_sql#" password="#password_sql#">
					EXECUTE Presupuestos_GeneraExplosionInsumos #id_Empresas[ind]#, #id_Obra#, #id_CentroCosto#, NULL, NULL, NULL, #sn_MultiplicaFactorFrenteParamSP#
				</cfquery>
				Log
				<cf_log datasource='#cnx#' username='#user_sql#' password='#password_sql#' id_Empresa="#Session.id_Empresa#" id_Obra="#Session.id_Obra#" id_Usuario="#Session.id_Usuario#" id_MenuOpcion="#Session.id_MenuOpcion#">
					<cfoutput>
						EXECUTE Presupuestos_GeneraExplosionInsumos #id_Empresas[ind]#, #id_Obra#, #id_CentroCosto#, NULL, NULL, NULL, #sn_MultiplicaFactorFrenteParamSP#
					</cfoutput>
				</cf_log>
				Termina Log --->
			</cfloop>
			<cftransaction action="commit"/>
			<cfset Presupuestos.explosion= TRUE>
			<cfset Presupuestos.tipoError= "">
			<cfset Presupuestos.mensaje= "Se ha generado la explosi�n de insumos correctamente">
			<cfcatch type="error_custom">
				<cftransaction action="rollback"/>
				<cfset Presupuestos.explosion= FALSE>
				<cfset Presupuestos.tipoError= "validaciones">
				<cfset Presupuestos.mensaje= #cfcatch.Message#>
			</cfcatch>
			<cfcatch type="database">
				<cftransaction action="rollback"/>
				<cfset Presupuestos.explosion= FALSE>
				<cfset Presupuestos.tipoError= "database-indefinido">
				<cfset Presupuestos.mensaje= "Ha ocurrido un error en el acceso a base de datos (#cfcatch.Detail#)  - #centrocosto#">
			</cfcatch>
		</cftry>
	</cftransaction>
	<cfreturn Presupuestos>
</cffunction>

<cffunction name="RSTotalesExplosionado" returntype="struct" access="public">
	<cfargument name='id_Empresa' type='string' required='yes'>
	<cfargument name='id_Obra' type='numeric' required='yes'>
    <cfif id_Empresa EQ ''>
		<cfset id_Empresa = 'NULL'>
	</cfif>
	<cftry>
		<cfif id_Obra EQ 2>
            <cfquery datasource='#cnx#' name='RSTotal' username='#user_sql#' password='#password_sql#'>
                SELECT ISNULL(SUM(im_SubTotal),0)  as [Total] FROM explosionInsumos_Presupuestos where id_Obra = #id_Obra# AND id_Empresa = ISNULL(#id_Empresa#,id_Empresa)
            </cfquery>
		<cfelse>
            <cfquery datasource='#cnx#' name='RSTotal' username='#user_sql#' password='#password_sql#'>
                SELECT ISNULL(SUM(im_SubTotal),0)  as [Total] FROM explosionInsumos where id_Obra = #id_Obra# AND id_Empresa = ISNULL(#id_Empresa#,id_Empresa)
            </cfquery>
		</cfif>    	
		<cfset Presupuestos.total= TRUE>
		<cfset Presupuestos.rs= #RSTotal#>
		<cfset Presupuestos.tipoError= ''>
		<cfset Presupuestos.mensaje= "Se obtuvo el total correctamente"> 
		<cfreturn Presupuestos>
		<cfcatch type="any">
			<cfset Presupuestos.listado= FALSE>
			<cfset Presupuestos.tipoError= 'database-indefinido'>
			<cfset Presupuestos.mensaje= "Error en el acceso a base de datos EP(#cfcatch.Detail#)">
			<cfreturn Presupuestos>
		</cfcatch> 
	</cftry>
</cffunction>

<!---Funcion que genera la explosion de insumos segun un arreglo de partidas de presupuesto--->
<cffunction name="GeneraDesExplosionInsumosDeListaCentros" returntype="struct" access="public">
	<cfargument name="id_Empresas" type="array" required="yes">
	<cfargument name="id_Obra" type="numeric" required="yes">
	<cfargument name="sn_MultiplicaFactorFrente" type="numeric" required="no">
	<cfargument name="id_CentroCostos" type="array">	
	<cfif isDefined("arguments.sn_MultiplicaFactorFrente")>
		<cfset sn_MultiplicaFactorFrenteParamSP= #sn_MultiplicaFactorFrente#>
	<cfelse>
		<cfset sn_MultiplicaFactorFrenteParamSP= "NULL">
	</cfif>	
	<cftransaction>
		<cftry>
			<cfset ind = 0>
			<cfloop array="#id_CentroCostos#" index="id_CentroCosto">
				<cfset ind= ind + 1>
				<cfquery name="RS_Explosion" datasource="#cnx#" username="#user_sql#" password="#password_sql#">
					EXECUTE Presupuestos_GeneraDesExplosionInsumos #id_Empresas[ind]#, #id_Obra#, #id_CentroCosto#, NULL, NULL, NULL, #sn_MultiplicaFactorFrenteParamSP#
				</cfquery>
				<!---Log
				<cf_log datasource='#cnx#' username='#user_sql#' password='#password_sql#' id_Empresa="#Session.id_Empresa#" id_Obra="#Session.id_Obra#" id_Usuario="#Session.id_Usuario#" id_MenuOpcion="#Session.id_MenuOpcion#">
					<cfoutput>
						EXECUTE Presupuestos_GeneraExplosionInsumos #id_Empresas[ind]#, #id_Obra#, #id_CentroCosto#, NULL, NULL, NULL, #sn_MultiplicaFactorFrenteParamSP#
					</cfoutput>
				</cf_log>--->
				<!---Termina Log --->
			</cfloop>
			<cftransaction action="commit"/>
			<cfset Presupuestos.explosion= TRUE>
			<cfset Presupuestos.tipoError= "">
			<cfset Presupuestos.mensaje= "Se ha generado la desexplosi�n de insumos correctamente">
			<cfcatch type="error_custom">
				<cftransaction action="rollback"/>
				<cfset Presupuestos.explosion= FALSE>
				<cfset Presupuestos.tipoError= "validaciones">
				<cfset Presupuestos.mensaje= #cfcatch.Message#>
			</cfcatch>
			<cfcatch type="database">
				<cftransaction action="rollback"/>
				<cfset Presupuestos.explosion= FALSE>
				<cfset Presupuestos.tipoError= "database-indefinido">
				<cfset Presupuestos.mensaje= "Ha ocurrido un error en el acceso a base de datos(#cfcatch.Detail#)">
			</cfcatch>
		</cftry>
	</cftransaction>
	<cfreturn Presupuestos>
</cffunction>

<cffunction name="GeneraDesExplosionInsumosDeListaFrentes" returntype="struct" access="public">
	<cfargument name="id_Empresa" type="numeric" required="yes">
	<cfargument name="id_Obra" type="numeric" required="yes">
	<cfargument name="id_CentroCosto" type="numeric" required="yes">
	<cfargument name="id_Frentes" type="array" required="yes">
	<cfargument name="sn_MultiplicaFactorFrente" type="numeric" required="no">
	
	<cfif isDefined("arguments.sn_MultiplicaFactorFrente")>
		<cfset sn_MultiplicaFactorFrenteParamSP= #sn_MultiplicaFactorFrente#>
	<cfelse>
		<cfset sn_MultiplicaFactorFrenteParamSP= "NULL">
	</cfif>
	
	<cftransaction>
		<cftry>
			<cfloop array="#id_Frentes#" index="id_Frente">
				<cfquery name="RS_Explosion" datasource="#cnx#" username="#user_sql#" password="#password_sql#">
					EXECUTE Presupuestos_GeneraDesExplosionInsumos #id_Empresa#, #id_Obra#, #id_CentroCosto#, #id_Frente#, NULL, NULL, #sn_MultiplicaFactorFrenteParamSP#
				</cfquery>
				<!---Log--->
				<cf_log datasource='#cnx#' username='#user_sql#' password='#password_sql#' id_Empresa="#Session.id_Empresa#" id_Obra="#Session.id_Obra#" id_Usuario="#Session.id_Usuario#" id_MenuOpcion="#Session.id_MenuOpcion#">
					<cfoutput>
						EXECUTE Presupuestos_GeneraDesExplosionInsumos #id_Empresa#, #id_Obra#, #id_CentroCosto#, #id_Frente#, NULL, NULL, #sn_MultiplicaFactorFrenteParamSP#
					</cfoutput>
				</cf_log>
				<!---Termina Log --->
			</cfloop>
			<cftransaction action="commit"/>
			<cfset Presupuestos.explosion= TRUE>
			<cfset Presupuestos.tipoError= "">
			<cfset Presupuestos.mensaje= "Se ha generado la desexplosi�n de insumos correctamente">
			<cfcatch type="error_custom">
				<cftransaction action="rollback"/>
				<cfset Presupuestos.explosion= FALSE>
				<cfset Presupuestos.tipoError= "validaciones">
				<cfset Presupuestos.mensaje= #cfcatch.Message#>
			</cfcatch>
			<cfcatch type="database">
				<cftransaction action="rollback"/>
				<cfset Presupuestos.explosion= FALSE>
				<cfset Presupuestos.tipoError= "database-indefinido">
				<cfset Presupuestos.mensaje= "Ha ocurrido un error en el acceso a base de datos(#cfcatch.Detail#)">
			</cfcatch>
		</cftry>
	</cftransaction>
	<cfreturn Presupuestos>
</cffunction>

<cffunction name="GeneraDesExplosionInsumosDeListaPartidas" returntype="struct" access="public">
	<cfargument name="id_Empresa" type="numeric" required="yes">
	<cfargument name="id_Obra" type="numeric" required="yes">
	<cfargument name="id_CentroCosto" type="numeric" required="yes">
	<cfargument name="id_Frente" type="numeric" required="yes">
	<cfargument name="id_Partidas" type="array" required="yes">
	<cfargument name="sn_MultiplicaFactorFrente" type="numeric" required="no">
	
	<cfif isDefined("arguments.sn_MultiplicaFactorFrente")>
		<cfset sn_MultiplicaFactorFrenteParamSP= #sn_MultiplicaFactorFrente#>
	<cfelse>
		<cfset sn_MultiplicaFactorFrenteParamSP= "NULL">
	</cfif>
	
	<cftransaction>
		<cftry>
			<cfloop array="#id_Partidas#" index="id_Partida">
				<cfquery name="RS_Explosion" datasource="#cnx#" username="#user_sql#" password="#password_sql#">
					EXECUTE Presupuestos_GeneraDesExplosionInsumos #id_Empresa#, #id_Obra#, #id_CentroCosto#, #id_Frente#, #id_Partida#, NULL, #sn_MultiplicaFactorFrenteParamSP#
				</cfquery>
				<!---Log--->
				<cf_log datasource='#cnx#' username='#user_sql#' password='#password_sql#' id_Empresa="#Session.id_Empresa#" id_Obra="#Session.id_Obra#" id_Usuario="#Session.id_Usuario#" id_MenuOpcion="#Session.id_MenuOpcion#">
					<cfoutput>
						EXECUTE Presupuestos_GeneraDesExplosionInsumos #id_Empresa#, #id_Obra#, #id_CentroCosto#, #id_Frente#, #id_Partida#, NULL, #sn_MultiplicaFactorFrenteParamSP#
					</cfoutput>
				</cf_log>
				<!---Termina Log --->
			</cfloop>
			<cftransaction action="commit"/>
			<cfset Presupuestos.explosion= TRUE>
			<cfset Presupuestos.tipoError= "">
			<cfset Presupuestos.mensaje= "Se ha generado la desexplosi�n de insumos correctamente">
			<cfcatch type="error_custom">
				<cftransaction action="rollback"/>
				<cfset Presupuestos.explosion= FALSE>
				<cfset Presupuestos.tipoError= "validaciones">
				<cfset Presupuestos.mensaje= #cfcatch.Message#>
			</cfcatch>
			<cfcatch type="database">
				<cftransaction action="rollback"/>
				<cfset Presupuestos.explosion= FALSE>
				<cfset Presupuestos.tipoError= "database-indefinido">
				<cfset Presupuestos.mensaje= "Ha ocurrido un error en el acceso a base de datos(#cfcatch.Detail#)">
			</cfcatch>
		</cftry>
	</cftransaction>
	<cfreturn Presupuestos>
</cffunction>

<!--- Funcion para obtener presupuestos parametricos--->
<!--- Autor: Brenda Machado --->
<!--- Fecha: 26/09/2013 --->
<cffunction name="RSPresupuestosParametricos" access="public" returntype="struct">
	<cfargument name='id_CentroCosto' type="string" required='no'>
	<cfargument name="id_Frente" type="string" required="no">
    <cfargument name="id_Partida" type="string" required="no">
    <cfargument name="id_Tarjeta" type="string" required="no">
    <cfargument name="idu_TipoObra" type="string" default='NULL'>
    <cfargument name="sn_Sotano" type="string" default='NULL'>
    <cfargument name="cl_TipoEstacionamiento" type="string" default='NULL'>
    <cfargument name="cl_Interviene" type="string" default='NULL'>
    <cfargument name="idu_Region" type="string" default='NULL'>
    <cfargument name="nu_Niveles" type="string" default='NULL'>
    <cfargument name="idu_SistemaConstruccion" type="string" default='NULL'>
    
    <cfif #id_CentroCosto# EQ '' AND NOT isDefined(#id_CentroCosto#)>
		<cfset #id_CentroCosto# = 'NULL'>
    </cfif>   
    <cfif #id_Frente# EQ '' AND NOT isDefined(#id_Frente#)>
		<cfset #id_Frente# = 'NULL'>
    </cfif>   
    <cfif #id_Partida# EQ '' AND NOT isDefined(#id_Partida#)>
		<cfset #id_Partida# = 'NULL'>
    </cfif>   
    <cfif #id_Tarjeta# EQ '' AND NOT isDefined(#id_Tarjeta#)>
		<cfset #id_Tarjeta# = 'NULL'>
    </cfif>   
    
    <cfif #idu_TipoObra# EQ ''>
		<cfset #idu_TipoObra# = 'NULL'>
    </cfif>       
    <cfif #sn_Sotano# EQ ''>
        <cfset #sn_Sotano# = 'NULL'>
    </cfif>
    <cfif #cl_TipoEstacionamiento# EQ ''>
        <cfset #cl_TipoEstacionamiento# = 'NULL'>
    </cfif>
    <cfif #cl_Interviene# EQ ''>
        <cfset #cl_Interviene# = 'NULL'>
    </cfif>
    <cfif #idu_Region# EQ ''>
        <cfset #idu_Region# = 'NULL'>
    </cfif>
    <cfif #nu_Niveles# EQ ''>
        <cfset #nu_Niveles# = 'NULL'>
    </cfif>
    <cfif #idu_SistemaConstruccion# EQ ''>
        <cfset #idu_SistemaConstruccion# = 'NULL'>
    </cfif>
    
	<cftry>
		<cfquery datasource='#cnx#' name='RS_MostrarPresupuesto' username='#user_sql#' password='#password_sql#'>
			EXECUTE PresupuestosParametricos_DatosGlobales #id_CentroCosto#, #id_Frente#, #id_Partida#, '#id_Tarjeta#',
           		#idu_TipoObra#, #sn_Sotano#, #cl_TipoEstacionamiento#, #cl_Interviene#, #idu_Region#, #nu_Niveles#, 
                #idu_SistemaConstruccion#
		</cfquery>
		<cfset PptosParametrico.listado= TRUE>
		<cfset PptosParametrico.tipoError= ''>
		<cfset PptosParametrico.rs= RS_MostrarPresupuesto>
		<cfset PptosParametrico.mensaje= 'Se obtuvo el recordset para presupuesto correctamente'>
		<cfreturn PptosParametrico>
		<cfcatch type='database'>
			<cfset PptosParametrico.listado= FALSE>
			<cfset PptosParametrico.tipoError= 'database-indefinido'>
			<cfset PptosParametrico.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn PptosParametrico>
		</cfcatch>
	</cftry>
</cffunction>
<!---Editar la descripcion del CentroCosto--->
<cffunction name='RSEditarDescripcionCentroCosto' access='public' returntype='struct'>
	<cfargument name='id_Empresa' type='string' required='yes'>
	<cfargument name='id_Obra' type='string' required='yes'>
	<cfargument name='id_CentroCosto' type='string' required='yes'>
	<cfargument name='de_CentroCosto' type='string' required='yes'>    

	<cftry>
		<cfquery datasource='#cnx#' name='RSFilasAfectadas' username='#user_sql#' password='#password_sql#'>
			EXECUTE Presupuestos_EditarDescripcionCentroCosto #id_Empresa#, #id_Obra#, #id_CentroCosto#, '#de_CentroCosto#'
		</cfquery>
		<cfset Presupuestos.actualizado= TRUE>
		<cfset Presupuestos.tipoError= ''>
		<cfset Presupuestos.mensaje= 'Centro Costo actualizado correctamente'>
		<cfreturn Presupuestos>
		<cfcatch type='database'>
			<cfset Presupuestos.actualizado= FALSE>
			<cfset Presupuestos.tipoError= 'database-indefinido'>
			<cfset Presupuestos.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn Presupuestos>
		</cfcatch>
	</cftry>
</cffunction>

<!---Editar la descripcion del Frente--->
<cffunction name='RSEditarDescripcionFrentes' access='public' returntype='struct'>
	<cfargument name='id_Empresa' type='string' required='yes'>
	<cfargument name='id_Obra' type='string' required='yes'>
	<cfargument name='id_CentroCosto' type='string' required='yes'>
    <cfargument name='id_Frente' type='string' required='yes'>
	<cfargument name='de_Frente' type='string' required='yes'>    

	<cftry>
		<cfquery datasource='#cnx#' name='RSFilasAfectadas' username='#user_sql#' password='#password_sql#'>
			EXECUTE Presupuestos_EditarDescripcionFrente #id_Empresa#, #id_Obra#, #id_CentroCosto#, #id_Frente#, '#de_Frente#'
		</cfquery>
		<cfset Presupuestos.actualizado= TRUE>
		<cfset Presupuestos.tipoError= ''>
		<cfset Presupuestos.mensaje= 'Frente actualizado correctamente'>
		<cfreturn Presupuestos>
		<cfcatch type='database'>
			<cfset Presupuestos.actualizado= FALSE>
			<cfset Presupuestos.tipoError= 'database-indefinido'>
			<cfset Presupuestos.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn Presupuestos>
		</cfcatch>
	</cftry>
</cffunction>
<!---Editar la descripcion de la Partida--->
<cffunction name='RSEditarDescripcionPartidas' access='public' returntype='struct'>
	<cfargument name='id_Empresa' type='string' required='yes'>
	<cfargument name='id_Obra' type='string' required='yes'>
	<cfargument name='id_CentroCosto' type='string' required='yes'>
    <cfargument name='id_Frente' type='string' required='yes'>
    <cfargument name='id_Partida' type='string' required='yes'>
	<cfargument name='de_Partida' type='string' required='yes'>    

	<cftry>
		<cfquery datasource='#cnx#' name='RSFilasAfectadas' username='#user_sql#' password='#password_sql#'>
			EXECUTE Presupuestos_EditarDescripcionPartida #id_Empresa#, #id_Obra#, #id_CentroCosto#, #id_Frente#, #id_Partida#, '#de_Partida#'
		</cfquery>
		<cfset Presupuestos.actualizado= TRUE>
		<cfset Presupuestos.tipoError= ''>
		<cfset Presupuestos.mensaje= 'Partida actualizado correctamente'>
		<cfreturn Presupuestos>
		<cfcatch type='database'>
			<cfset Presupuestos.actualizado= FALSE>
			<cfset Presupuestos.tipoError= 'database-indefinido'>
			<cfset Presupuestos.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn Presupuestos>
		</cfcatch>
	</cftry>
</cffunction>

<!---Funcion que trae la descripcion del Centro de Costo--->
<cffunction name="GetDe_CentroCosto" access="remote" returntype="string">
	<cfargument name="id_Empresa" type="string" required="yes">
	<cfargument name="id_Obra" type="string" required="yes">
	<cfargument name="id_CentroCosto" type="string" required="yes">
		<cfquery name="RS" datasource='#cnx#' username='#user_sql#' password='#password_sql#'>
			EXECUTE Obtener_de_Centro_Costo #id_Empresa#, #id_Obra#, #id_CentroCosto#
		</cfquery>
		<cfreturn RS.de_CentroCosto>
</cffunction>
<!---Funcion que trae la descripcion del Frente--->
<cffunction name="GetDe_Frente" access="remote" returntype="string">
	<cfargument name="id_Empresa" type="string" required="yes">
	<cfargument name="id_Obra" type="string" required="yes">
	<cfargument name="id_CentroCosto" type="string" required="yes">
    <cfargument name="id_Frente" type="string" required="yes">
		<cfquery name="RS" datasource='#cnx#' username='#user_sql#' password='#password_sql#'>
			EXECUTE Obtener_de_Frente #id_Empresa#, #id_Obra#, #id_CentroCosto#, #id_Frente#
		</cfquery>
		<cfreturn RS.de_Frente>
</cffunction>
<!---Funcion que trae la descripcion del Frente--->
<cffunction name="GetDe_Partida" access="remote" returntype="string">
	<cfargument name="id_Empresa" type="string" required="yes">
	<cfargument name="id_Obra" type="string" required="yes">
	<cfargument name="id_CentroCosto" type="string" required="yes">
    <cfargument name="id_Frente" type="string" required="yes">
    <cfargument name="id_Partida" type="string" required="yes">
		<cfquery name="RS" datasource='#cnx#' username='#user_sql#' password='#password_sql#'>
			EXECUTE Obtener_de_Partida #id_Empresa#, #id_Obra#, #id_CentroCosto#, #id_Frente#, #id_Partida#
		</cfquery>
		<cfreturn RS.de_Partida>
</cffunction>



<!---Funcion que trae el recordset de una explosion de un presupuesto para estimaciones (filtrados por tipo de ejecucion= 2 y 4: destajos y subcontratos)--->
<cffunction name="RSMostrarExplosionDePresupuestoConRestantesParaDestajo" access="remote" returntype="struct">
	<cfargument name="id_Empresa" type="string" required="yes">
	<cfargument name="id_Obra" type="string" required="yes">
	<cfargument name="id_CentroCosto" type="string" required="yes">
	<cfargument name="id_Frente" type="string" required="yes">
	<cfargument name="id_Partida" type="string" required="yes">
		
	<cfif Not IsNumeric(#id_Empresa#) OR Not IsNumeric(#id_Obra#) OR Not IsNumeric(#id_CentroCosto#) OR Not IsNumeric(#id_Frente#)  OR Not IsNumeric(#id_Partida#) <!---OR Not IsNumeric(#nd_Presupuesto#) --->>
		<cfset Presupuestos.listado= FALSE>
		<cfset Presupuestos.tipoError= 'validaciones'>
		<cfset Presupuestos.mensaje= 'Necesita definir un presupuesto'>
		<cfreturn Presupuestos>	
	</cfif>
	<cftry>
		<!--- <cfquery datasource='#cnx#' name='RS_MostrarExplosion' username='#user_sql#' password='#password_sql#'>
			EXECUTE [ObtenerExplosionInsumosConRestantesParaDestajo] #id_Empresa#, #id_Obra#, #id_CentroCosto#, #id_Frente#, <cfif #id_Partida# EQ "">NULL<cfelse>#id_Partida#</cfif>
		</cfquery> --->
        
        <cfstoredproc procedure="ObtenerExplosionInsumosConRestantesParaDestajo" datasource="#cnx#">
            <cfprocparam type="IN" cfsqltype="CF_SQL_INTEGER" dbvarname="@id_Empresa" value="#arguments.id_Empresa#" null="#iif(isNumeric(arguments.id_Empresa),false,true)#">
            <cfprocparam type="IN" cfsqltype="CF_SQL_INTEGER" dbvarname="@id_Obra" value="#arguments.id_Obra#" null="#iif(isNumeric(arguments.id_Obra),false,true)#">
            <cfprocparam type="IN" cfsqltype="CF_SQL_INTEGER" dbvarname="@id_CentroCosto" value="#arguments.id_CentroCosto#" null="#iif(isNumeric(arguments.id_CentroCosto),false,true)#">
            <cfprocparam type="IN" cfsqltype="CF_SQL_INTEGER" dbvarname="@id_Frente" value="#arguments.id_Frente#" null="#iif(isNumeric(arguments.id_Frente),false,true)#">
            <cfprocparam type="IN" cfsqltype="CF_SQL_INTEGER" dbvarname="@id_Partida" value="#arguments.id_Partida#" null="#iif(isNumeric(arguments.id_Partida),false,true)#">
            <cfprocresult name="RS_MostrarExplosion" resultset="1">
        </cfstoredproc>

		<cfset Presupuestos.listado= TRUE>
		<cfset Presupuestos.tipoError= ''>
		<cfset Presupuestos.rs= RS_MostrarExplosion>
		<cfset Presupuestos.mensaje= 'Se obtuvo el recordset de explosi�n correctamente'>
		<cfreturn Presupuestos>
		<cfcatch type='database'>
			<cfset Presupuestos.listado= FALSE>
			<cfset Presupuestos.tipoError= 'database-indefinido'>
			<cfset Presupuestos.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn Presupuestos>
		</cfcatch>
	</cftry>
</cffunction>

<cffunction name="GuardaOperacionExplosionInsumosTarjetas" returntype="struct" access="public">
	<cfargument name="id_Empresas" type="array" required="no">
	<cfargument name="id_Empresa" type="string" required="yes">
	<cfargument name="id_Obra" type="numeric" required="yes">
	<cfargument name="id_CentroCosto" type="numeric" required="yes">
	<cfargument name="id_Frente" type="numeric" required="yes">
	<cfargument name="id_Partida" type="numeric" required="yes">
	<cfargument name="nd_PresupuestosEjecucion" type="array" required="yes">
	<cfargument name="id_TiposEjecucionTarjetas" type="array" required="yes">
	<cfargument name="nd_PresupuestosExplosion" type="array" required="yes">
	<cfargument name="sn_MultiplicaFactorFrente" type="numeric" required="yes">
	<cfargument name="cl_Operacion" type="string" required="yes">
    
    <cfsetting requesttimeout="99999999">
	<cftransaction>
		<cftry>
            <cfif #cl_Operacion# EQ 'guardar_ejecucion'>
            	<cfset ind= 0>
                <cfloop array="#nd_PresupuestosEjecucion#" index="nd_Presupuesto">
                    <cfset ind= ind + 1>
                	<cfloop array="#id_Empresas#" index="id_Empresa"> 
                        <cfquery name="RS_Editar" datasource="#cnx#" username="#user_sql#" password="#password_sql#">
                            EXECUTE Presupuestos_GuardaTipoEjecucion #id_Empresa#, #id_Obra#, #id_CentroCosto#, #id_Frente#, #id_Partida#, #nd_Presupuesto#, #id_TiposEjecucionTarjetas[ind]#
                        </cfquery>
                    </cfloop> 
                </cfloop>
            </cfif>
            <cfif #cl_Operacion# EQ 'explosion'>
            	<cfset ind= 0>
                <cfloop array="#nd_PresupuestosEjecucion#" index="nd_Presupuesto">
                    <cfset ind= ind + 1>
                    <cfloop array="#id_Empresas#" index="id_Empresa">
                        <cfquery name="RS_Editar" datasource="#cnx#" username="#user_sql#" password="#password_sql#">
                            EXECUTE Presupuestos_GuardaTipoEjecucion #id_Empresa#, #id_Obra#, #id_CentroCosto#, #id_Frente#, #id_Partida#, #nd_Presupuesto#, #id_TiposEjecucionTarjetas[ind]#
                        </cfquery>
                    </cfloop>
                </cfloop>
                <cfloop array="#nd_PresupuestosExplosion#" index="nd_Presupuesto">
                	<cfloop array="#id_Empresas#" index="id_Empresa">
                        <cfquery name="RS_Explosion" datasource="#cnx#" username="#user_sql#" password="#password_sql#">
                            EXECUTE Presupuestos_GeneraExplosionInsumos 	#id_Empresa#, #id_Obra#, #id_CentroCosto#, #id_Frente#, #id_Partida#, #nd_Presupuesto#, #sn_MultiplicaFactorFrente#
                        </cfquery>
                    </cfloop>
                </cfloop>
            </cfif>
            <cfif #cl_Operacion# EQ 'desexplosion'>
                <cfloop array="#nd_PresupuestosExplosion#" index="nd_Presupuesto">
                	<cfloop array="#id_Empresas#" index="id_Empresa">
                        <cfquery name="RS_Explosion" datasource="#cnx#" username="#user_sql#" password="#password_sql#">
                            EXECUTE Presupuestos_GeneraDesExplosionInsumos #id_Empresa#, #id_Obra#, #id_CentroCosto#, #id_Frente#, #id_Partida#, #nd_Presupuesto#, #sn_MultiplicaFactorFrente#
                        </cfquery>
                    </cfloop>
                </cfloop>
            </cfif>
             
            <cfif #cl_Operacion# EQ 'cambiar_empresa'>
                <cfloop array="#nd_PresupuestosExplosion#" index="nd_Presupuesto">
                
                    <cfquery name="RS_Explosion" datasource="#cnx#" username="#user_sql#" password="#password_sql#">
                        EXECUTE ExplosionInsumos_CambiarEmpresa #id_Empresa#, #id_Obra#, #id_CentroCosto#, #id_Frente#, #id_Partida#, #nd_Presupuesto#
                    </cfquery>
                </cfloop>
            </cfif>
			<cftransaction action="commit"/>
			<cfset Presupuestos.editar= TRUE>
			<cfset Presupuestos.tipoError= "">
			<cfset Presupuestos.mensaje= "Se ha realizado correctamente">
			<cfcatch type="database">
				<cftransaction action="rollback"/>
				<cfset Presupuestos.editar= FALSE>
				<cfset Presupuestos.tipoError= "database-indefinido">
				<cfset Presupuestos.mensaje= "Ha ocurrido un error en el acceso a base de datos(#cfcatch.Detail#) - (#cfcatch.Message#) - #cl_Operacion#">
			</cfcatch>
		</cftry>
	</cftransaction>
	<cfreturn Presupuestos>
</cffunction>

<cffunction name="GuardaOperacionExplosionInsumosPartidas" returntype="struct" access="public">
	<cfargument name="id_Empresas" type="array" required="no">
	<cfargument name="id_Empresa" type="string" required="yes">
	<cfargument name="id_Obra" type="numeric" required="yes">
	<cfargument name="id_CentroCosto" type="numeric" required="yes">
	<cfargument name="id_Frente" type="numeric" required="yes">
	<cfargument name="id_PartidasEjecucion" type="array" required="yes">
	<cfargument name="id_TiposEjecucionPartidas" type="array" required="yes">
	<cfargument name="id_PartidasExplosion" type="array" required="yes">
	<cfargument name="sn_MultiplicaFactorFrente" type="numeric" required="yes">
	<cfargument name="cl_Operacion" type="string" required="yes">
    
    <cfsetting requesttimeout="99999999">
	<cftransaction>
		<cftry>
            <cfif #cl_Operacion# EQ 'guardar_ejecucion'>
            	<cfset ind= 0>
                <cfloop array="#id_PartidasEjecucion#" index="id_Partida">
                    <cfset ind= ind + 1>
                    <cfloop array="#id_Empresas#" index="id_Empresa">
                        <cfquery name="RS_Editar" datasource="#cnx#" username="#user_sql#" password="#password_sql#">
                            EXECUTE Presupuestos_GuardaTipoEjecucion #id_Empresa#, #id_Obra#, #id_CentroCosto#, #id_Frente#, #id_Partida#, NULL, #id_TiposEjecucionPartidas[ind]#
                        </cfquery>
                    </cfloop>
                </cfloop>
            </cfif>
            <cfif #cl_Operacion# EQ 'explosion'>
            	<cfset ind= 0>
                <cfloop array="#id_PartidasEjecucion#" index="id_Partida">
                    <cfset ind= ind + 1>
                    <cfloop array="#id_Empresas#" index="id_Empresa">
						<cfquery name="RS_Editar" datasource="#cnx#" username="#user_sql#" password="#password_sql#">
                            EXECUTE Presupuestos_GuardaTipoEjecucion #id_Empresa#, #id_Obra#, #id_CentroCosto#, #id_Frente#, #id_Partida#, NULL, #id_TiposEjecucionPartidas[ind]#
                        </cfquery>
                    </cfloop>
                </cfloop>
                <cfloop array="#id_PartidasExplosion#" index="id_Partida">
                	<cfloop array="#id_Empresas#" index="id_Empresa">
                        <cfquery name="RS_Explosion" datasource="#cnx#" username="#user_sql#" password="#password_sql#">
                            EXECUTE Presupuestos_GeneraExplosionInsumos 	#id_Empresa#, #id_Obra#, #id_CentroCosto#, #id_Frente#, #id_Partida#, NULL, #sn_MultiplicaFactorFrente#
                        </cfquery>
                    </cfloop>
                </cfloop>
            </cfif>
            <cfif #cl_Operacion# EQ 'desexplosion'>
                <cfloop array="#id_PartidasExplosion#" index="id_Partida">
                	<cfloop array="#id_Empresas#" index="id_Empresa">
                        <cfquery name="RS_Explosion" datasource="#cnx#" username="#user_sql#" password="#password_sql#">
                            EXECUTE Presupuestos_GeneraDesExplosionInsumos #id_Empresa#, #id_Obra#, #id_CentroCosto#, #id_Frente#, #id_Partida#, NULL, #sn_MultiplicaFactorFrente#
                        </cfquery>
					</cfloop>
                </cfloop>
            </cfif>
            <cfif #cl_Operacion# EQ 'cambiar_empresa'>
                <cfloop array="#id_PartidasExplosion#" index="id_Partida">
                    <cfquery name="RS_Explosion" datasource="#cnx#" username="#user_sql#" password="#password_sql#">
                        EXECUTE ExplosionInsumos_CambiarEmpresa #id_Empresa#, #id_Obra#, #id_CentroCosto#, #id_Frente#, #id_Partida#, NULL
                    </cfquery>
                </cfloop>
            </cfif>
			<cftransaction action="commit"/>
			<cfset Presupuestos.editar= TRUE>
			<cfset Presupuestos.tipoError= "">
			<cfset Presupuestos.mensaje= "Se ha realizado correctamente">
			<cfcatch type="database">
				<cftransaction action="rollback"/>
				<cfset Presupuestos.editar= FALSE>
				<cfset Presupuestos.tipoError= "database-indefinido">
				<cfset Presupuestos.mensaje= "Ha ocurrido un error en el acceso a base de datos(#cfcatch.Detail#) - (#cfcatch.Message#) - #cl_Operacion#">
			</cfcatch>
		</cftry>
	</cftransaction>
	<cfreturn Presupuestos>
</cffunction>

<cffunction name="GuardaOperacionExplosionInsumosFrentes" returntype="struct" access="public">
	<cfargument name="id_Empresas" type="array" required="no">
	<cfargument name="id_Empresa" type="string" required="yes">
	<cfargument name="id_Obra" type="numeric" required="yes">
	<cfargument name="id_CentroCosto" type="numeric" required="yes">
	<cfargument name="id_FrentesEjecucion" type="array" required="yes">
	<cfargument name="id_TiposEjecucionFrentes" type="array" required="yes">
	<cfargument name="id_FrentesExplosion" type="array" required="yes">
	<cfargument name="sn_MultiplicaFactorFrente" type="numeric" required="yes">
	<cfargument name="cl_Operacion" type="string" required="yes">
    
    <cfsetting requesttimeout="99999999">
	<cftransaction>
		<cftry>
            <cfif #cl_Operacion# EQ 'guardar_ejecucion'>
            	<cfset ind= 0>
                <cfloop array="#id_FrentesEjecucion#" index="id_Frente">
                    <cfset ind= ind + 1>
                    <cfloop array="#id_Empresas#" index="id_Empresa">
                        <cfquery name="RS_Editar" datasource="#cnx#" username="#user_sql#" password="#password_sql#">
                            EXECUTE Presupuestos_GuardaTipoEjecucion #id_Empresa#, #id_Obra#, #id_CentroCosto#, #id_Frente#, NULL, NULL, #id_TiposEjecucionFrentes[ind]#
                        </cfquery>
                    </cfloop>
                </cfloop>
            </cfif>
            <cfif #cl_Operacion# EQ 'explosion'>
            	<cfset ind= 0>
                <cfloop array="#id_FrentesEjecucion#" index="id_Frente">
                    <cfset ind= ind + 1>
                    <cfloop array="#id_Empresas#" index="id_Empresa">
                        <cfquery name="RS_Editar" datasource="#cnx#" username="#user_sql#" password="#password_sql#">
                            EXECUTE Presupuestos_GuardaTipoEjecucion #id_Empresa#, #id_Obra#, #id_CentroCosto#, #id_Frente#, NULL, NULL, #id_TiposEjecucionFrentes[ind]#
                        </cfquery>
                    </cfloop>
                </cfloop>
                <cfloop array="#id_FrentesExplosion#" index="id_Frente">
					<cfloop array="#id_Empresas#" index="id_Empresa">
						<!---<cfquery name="explo" datasource="#cnx#" username="#user_sql#" password="#password_sql#">
                            Explosion #id_Empresa#,#id_Obra#,#id_CentroCosto#,#id_Frente#,NULL,NULL
                        </cfquery>
                        <cfoutput query="explo">
                            <cfquery name="exec" datasource="#cnx#" username="#user_sql#" password="#password_sql#">
                                #explo.str_sql#
                            </cfquery>		
                        </cfoutput>
                        --->
                         <cfquery name="RS_Explosion" datasource="#cnx#" username="#user_sql#" password="#password_sql#">
                            EXECUTE Presupuestos_GeneraExplosionInsumos 	#id_Empresa#, #id_Obra#, #id_CentroCosto#, #id_Frente#, NULL, NULL, #sn_MultiplicaFactorFrente#
                        </cfquery>
                    </cfloop>
                </cfloop>
            </cfif>
            <cfif #cl_Operacion# EQ 'desexplosion'>
                <cfloop array="#id_FrentesExplosion#" index="id_Frente">
                	<cfloop array="#id_Empresas#" index="id_Empresa">
                        <cfquery name="RS_Explosion" datasource="#cnx#" username="#user_sql#" password="#password_sql#">
                            EXECUTE Presupuestos_GeneraDesExplosionInsumos #id_Empresa#, #id_Obra#, #id_CentroCosto#, #id_Frente#, NULL, NULL, #sn_MultiplicaFactorFrente#
                        </cfquery>
                    </cfloop>
                </cfloop>
            </cfif>
            <cfif #cl_Operacion# EQ 'cambiar_empresa'>
                <cfloop array="#id_FrentesExplosion#" index="id_Frente">
                    <cfquery name="RS_Explosion" datasource="#cnx#" username="#user_sql#" password="#password_sql#">
                        EXECUTE ExplosionInsumos_CambiarEmpresa #id_Empresa#, #id_Obra#, #id_CentroCosto#, #id_Frente#, NULL, NULL
                    </cfquery>
                </cfloop>
            </cfif>
			<cftransaction action="commit"/>
			<cfset Presupuestos.editar= TRUE>
			<cfset Presupuestos.tipoError= "">
			<cfset Presupuestos.mensaje= "Se ha realizado correctamente">
			<cfcatch type="database">
				<cftransaction action="rollback"/>
				<cfset Presupuestos.editar= FALSE>
				<cfset Presupuestos.tipoError= "database-indefinido">
				<cfset Presupuestos.mensaje= "Ha ocurrido un error en el acceso a base de datos(#cfcatch.Detail#) - (#cfcatch.Message#) - #cl_Operacion#">
			</cfcatch>
		</cftry>
	</cftransaction>
	<cfreturn Presupuestos>
</cffunction>

<cffunction name="GuardaOperacionExplosionInsumosCentros" returntype="struct" access="public">
	<cfargument name="id_Empresas" type="array" required="no">
	<cfargument name="id_Empresa" type="string" required="yes">
	<cfargument name="id_Obra" type="numeric" required="yes">
	<cfargument name="id_CentrosEjecucion" type="array" required="yes">
	<cfargument name="id_TiposEjecucionCentros" type="array" required="yes">
	<cfargument name="id_CentrosExplosion" type="array" required="yes">
	<cfargument name="sn_MultiplicaFactorFrente" type="numeric" required="yes">
	<cfargument name="cl_Operacion" type="string" required="yes">

	<cfsetting requesttimeout="99999999"> 
    <cftransaction>
		<cftry>
            <cfif #cl_Operacion# EQ 'guardar_ejecucion'>
            	<cfset ind= 0>
                <cfloop array="#id_CentrosEjecucion#" index="id_CentroCosto">
                    <cfset ind= ind + 1>
                    <cfloop array="#id_Empresas#" index="id_Empresa">
                        <cfquery name="RS_Editar" datasource="#cnx#" username="#user_sql#" password="#password_sql#">
                            EXECUTE Presupuestos_GuardaTipoEjecucion #id_Empresa#, #id_Obra#, #id_CentroCosto#, NULL, NULL, NULL, #id_TiposEjecucionCentros[ind]#
                        </cfquery>
                    </cfloop>
                </cfloop>
            </cfif>
			<cfif #cl_Operacion# EQ 'explosion'>
            	<cfset ind= 0>
                <cfloop array="#id_CentrosEjecucion#" index="id_CentroCosto">
                	<cfset ind= ind + 1>
                    <cfloop array="#id_Empresas#" index="id_Empresa">
                        <cfquery name="RS_Editar" datasource="#cnx#" username="#user_sql#" password="#password_sql#">
                            EXECUTE Presupuestos_GuardaTipoEjecucion #id_Empresa#, #id_Obra#, #id_CentroCosto#, NULL, NULL, NULL, #id_TiposEjecucionCentros[ind]#
                        </cfquery>
                    </cfloop>
                </cfloop>

                <cfloop array="#id_CentrosExplosion#" index="id_CentroCosto">
                    <cfloop array="#id_Empresas#" index="id_Empresa">                        
						 <cfquery name="RS_Explosion" datasource="#cnx#" username="#user_sql#" password="#password_sql#">
                            EXECUTE Presupuestos_GeneraExplosionInsumos 	#id_Empresa#, #id_Obra#, #id_CentroCosto#, NULL, NULL, NULL, #sn_MultiplicaFactorFrente#
                        </cfquery>  
					</cfloop>
                </cfloop>
            </cfif>
            <cfif #cl_Operacion# EQ 'desexplosion'>
                <cfloop array="#id_CentrosExplosion#" index="id_CentroCosto">
                    <cfloop array="#id_Empresas#" index="id_Empresa">
                        <cfquery name="RS_Explosion" datasource="#cnx#" username="#user_sql#" password="#password_sql#">
                            EXECUTE Presupuestos_GeneraDesExplosionInsumos #id_Empresa#, #id_Obra#, #id_CentroCosto#, NULL, NULL, NULL, #sn_MultiplicaFactorFrente#
                        </cfquery>
                    </cfloop>
                </cfloop>
            </cfif>
            <cfif #cl_Operacion# EQ 'cambiar_empresa'>
                <cfloop array="#id_CentrosExplosion#" index="id_CentroCosto">
                    <cfquery name="RS_Explosion" datasource="#cnx#" username="#user_sql#" password="#password_sql#">
                        EXECUTE ExplosionInsumos_CambiarEmpresa #arguments.id_Empresa#, #id_Obra#, #id_CentroCosto#, NULL, NULL, NULL
                    </cfquery>
                </cfloop>
            </cfif>
			<cftransaction action="commit"/>
			<cfset Presupuestos.editar= TRUE>
			<cfset Presupuestos.tipoError= "">
			<cfset Presupuestos.mensaje= "Se ha realizado correctamente">
			<cfcatch type="database">
				<cftransaction action="rollback"/>
				<cfset Presupuestos.editar= FALSE>
				<cfset Presupuestos.tipoError= "database-indefinido">
				<cfset Presupuestos.mensaje= "Ha ocurrido un error en el acceso a base de datos(#cfcatch.Detail# - #cfcatch.message#) - #cl_Operacion#">
			</cfcatch>
		</cftry>
	</cftransaction>
	<cfreturn Presupuestos>
</cffunction>

<cffunction name="getSiCodigoEsValido" returntype="string" access="remote">
	<cfargument name="cl_Firma" type="string" required="yes">
	<!---Habria que modificar que e parametro sea requerido cuando se corrija la funcion --->
	<cfargument name="id_ProcesoEsencial" required="no" default="">
	<cfif cl_Firma EQ "FerroJrgh">
		<cfreturn "True">
	<cfelse>
		<cfreturn "">
	</cfif>
</cffunction>

</cfcomponent>