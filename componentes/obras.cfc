<cfcomponent extends='conexion'>
<cffunction name='RSMostrarNextIDObras' access='public' returntype='struct'>

	<cfargument name='id_Empresa' type='numeric' required='yes'>

	<cftry>
		<cfquery datasource='#cnx#' name='RS_MostrarNextIDObras' username='#user_sql#' password='#password_sql#'>
			EXECUTE bop_Obras_MostrarNextID #id_Empresa#
		</cfquery>
		<cfset Obras.NextID= TRUE>
		<cfset Obras.tipoError= ''>
		<cfset Obras.rs= RS_MostrarNextIDObras>
		<cfset Obras.mensaje= 'Se obtuvo el recordset de Obras correctamente'>
		<cfreturn Obras>
		<cfcatch type='database'>
			<cfset Obras.NextID= FALSE>
			<cfset Obras.tipoError= 'database-indefinido'>
			<cfset Obras.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn Obras>
		</cfcatch>
	</cftry>
</cffunction>

   <cffunction name="get_obras_ajax" access="remote" returntype="string" returnformat="JSON" >
        <cfargument name="de_Obra"	type="string" required="true" >
        <cfargument name="id_Empresa"	type="string" required="true" >

        
            <cfquery name="RSDatos" datasource="#session.cnx#">                
                 SELECT 
                    P.id_Empresa,P.id_Obra,O.de_Obra
               	 FROM
                    Obras O 
                    INNER JOIN PRESUPUESTOS P ON
                    O.id_Empresa=P.id_Empresa
                    AND O.id_Obra=p.id_Obra
                 WHERE
             		P.id_Empresa=#id_Empresa#  
                   <cfif de_Obra NEQ ''>
                   		AND O.de_Obra like '%'+'#de_Obra#'+'%'
                   </cfif>
                  GROUP BY P.id_Empresa,P.id_obra,O.de_obra       
                  --HAVING SUM(p.im_SubTotal) > 0     
            </cfquery>
            
            <cfinvoke component="#Application.componentes#/funciones" method="queryToJson" returnVariable="RS" query="#RSDatos#">
            <cfreturn RS>
    </cffunction>
    
<cffunction name='RSMostrarPorIDObras' access='public' returntype='struct'>

	<cfargument name='id_Empresa' type='numeric' required='yes'>
	<cfargument name='id_Obra' type='numeric' required='yes'>

	<cftry>
		<cfquery datasource='#cnx#' name='RS_MostrarObras' username='#user_sql#' password='#password_sql#'>
			EXECUTE bop_Obras_ObtenerPorID #id_Empresa#, #id_Obra#
		</cfquery>
		<cfset Obras.listado= TRUE>
		<cfset Obras.tipoError= ''>
		<cfset Obras.rs= RS_MostrarObras>
		<cfset Obras.mensaje= 'Se obtuvo el recordset de Obras correctamente'>
		<cfreturn Obras>
		<cfcatch type='database'>
			<cfset Obras.listado= FALSE>
			<cfset Obras.tipoError= 'database-indefinido'>
			<cfset Obras.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn Obras>
		</cfcatch>
	</cftry>
</cffunction>


<cffunction name='RSMostrarDinamicoObras' access='public' returntype='struct'>

	<cfargument name='id_Empresa' type='numeric' required='no' default='-1'>
	<cfargument name='id_Obra' type='numeric' required='no' default='-1'>
	<cfargument name='de_Obra' type='string' required='no' default=''>
	<cfargument name='fh_Inicio' type='date' required='no' default=''>
	<cfargument name='de_Contrato' type='string' required='no' default=''>
	<cfargument name='de_Ubicacion' type='string' required='no' default=''>
	<cfargument name='de_Direccion' type='string' required='no' default=''>
	<cfargument name='id_TipoObra' type='numeric' required='no' default='-1'>
	<cfargument name='id_TipoAvance' type='numeric' required='no' default='-1'>
	<cfargument name='id_Responsable' type='numeric' required='no' default='-1'>
	<cfargument name='nu_FactorSalarioReal' type='numeric' required='no' default='-1'>

	<cftry>
		<cfset WhereDinamico=''>
		<cfif #id_Empresa# NEQ '-1'>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE id_Empresa = #id_Empresa#">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND id_Empresa = #id_Empresa#">
			</cfif>
		</cfif>
		<cfif #id_Obra# NEQ '-1'>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE id_Obra = #id_Obra#">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND id_Obra = #id_Obra#">
			</cfif>
		</cfif>
		<cfif #de_Obra# NEQ ''>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE de_Obra LIKE '%#de_Obra#'">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND de_Obra LIKE '%#de_Obra#'">
			</cfif>
		</cfif>
		<cfif #fh_Inicio# NEQ ''>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE fh_Inicio = '#fh_Inicio#'">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND fh_Inicio = '#fh_Inicio#'">
			</cfif>
		</cfif>
		<cfif #de_Contrato# NEQ ''>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE de_Contrato LIKE '%#de_Contrato#'">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND de_Contrato LIKE '%#de_Contrato#'">
			</cfif>
		</cfif>
		<cfif #de_Ubicacion# NEQ ''>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE de_Ubicacion LIKE '%#de_Ubicacion#'">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND de_Ubicacion LIKE '%#de_Ubicacion#'">
			</cfif>
		</cfif>
		<cfif #de_Direccion# NEQ ''>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE de_Direccion LIKE '%#de_Direccion#'">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND de_Direccion LIKE '%#de_Direccion#'">
			</cfif>
		</cfif>
		<cfif #id_TipoObra# NEQ '-1'>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE id_TipoObra = #id_TipoObra#">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND id_TipoObra = #id_TipoObra#">
			</cfif>
		</cfif>
		<cfif #id_TipoAvance# NEQ '-1'>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE id_TipoAvance = #id_TipoAvance#">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND id_TipoAvance = #id_TipoAvance#">
			</cfif>
		</cfif>
		<cfif #id_Responsable# NEQ '-1'>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE id_Responsable = #id_Responsable#">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND id_Responsable = #id_Responsable#">
			</cfif>
		</cfif>
		<cfif #nu_FactorSalarioReal# NEQ '-1'>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE nu_FactorSalarioReal = #nu_FactorSalarioReal#">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND nu_FactorSalarioReal = #nu_FactorSalarioReal#">
			</cfif>
		</cfif>

		<cfquery datasource='#cnx#' name='RS_MostrarObras' username='#user_sql#' password='#password_sql#'>
			EXECUTE bop_Obras_ObtenerDinamico '#WhereDinamico#'
		</cfquery>
		<cfset Obras.listado= TRUE>
		<cfset Obras.tipoError= ''>
		<cfset Obras.rs= RS_MostrarObras>
		<cfset Obras.mensaje= 'Se obtuvo el recordset de Obras correctamente'>
		<cfreturn Obras>
		<cfcatch type='database'>
			<cfset Obras.listado= FALSE>
			<cfset Obras.tipoError= 'database-indefinido'>
			<cfset Obras.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn Obras>
		</cfcatch>
	</cftry>
</cffunction>


<cffunction name='RSMostrarTodosObras' access='public' returntype='struct'>

	<cftry>
		<cfquery datasource='#cnx#' name='RS_MostrarObras' username='#user_sql#' password='#password_sql#'>
			EXECUTE bop_Obras_ObtenerTodos 
		</cfquery>
		<cfset Obras.listado= TRUE>
		<cfset Obras.tipoError= ''>
		<cfset Obras.rs= RS_MostrarObras>
		<cfset Obras.mensaje= 'Se obtuvo el recordset de Obras correctamente'>
		<cfreturn Obras>
		<cfcatch type='database'>
			<cfset Obras.listado= FALSE>
			<cfset Obras.tipoError= 'database-indefinido'>
			<cfset Obras.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn Obras>
		</cfcatch>
	</cftry>
</cffunction>


<cffunction name='RSAgregarConNextIDObras' access='public' returntype='struct'>

	<cfargument name='id_Empresa' type='numeric' required='yes'>
	<cfargument name='de_Obra' type='string' required='yes' default=''>
	<cfargument name='fh_Inicio' type='date' required='yes' default=''>
	<cfargument name='de_Contrato' type='string' required='no' default=''>
	<cfargument name='de_Ubicacion' type='string' required='no' default=''>
	<cfargument name='de_Direccion' type='string' required='no' default=''>
	<cfargument name='id_TipoObra' type='numeric' required='yes' default='0'>
	<cfargument name='id_TipoAvance' type='numeric' required='yes' default='0'>
	<cfargument name='id_Responsable' type='numeric' required='no' default='0'>
	<cfargument name='nu_FactorSalarioReal' type='numeric' required='no' default='0'>

	<cftry>
		<cftransaction>
			<cfset NextIDtmp= this.RSMostrarNextIDCentrosCostos(arguments.cnx, arguments.user_sql, arguments.password_sql, attributes.id_Empresa).rs.NextID>
			<cfset Obras= this.RSAgregarCentrosCostos(arguments.cnx, arguments.user_sql, arguments.password_sql, arguments.id_Empresa, NextIDtmp, arguments.de_Obra, arguments.fh_Inicio, arguments.de_Contrato, arguments.de_Ubicacion, arguments.de_Direccion, arguments.id_TipoObra, arguments.id_TipoAvance, arguments.id_Responsable, arguments.nu_FactorSalarioReal)>
		</cftransaction>
		<cfset Obras.NextID= NextIDtmp>
		<cfreturn Obras>
		<cfcatch type='database'>
			<cftransaction action='rollback'/>
			<cfset Obras.agregado= FALSE>
			<cfif cfcatch.NativeErrorCode EQ '2627'>
				<cfset Obras.tipoError= 'database-registro_duplicado'>
				<cfset Obras.mensaje= 'La Obra no se pudo guardar porque ya existe'>
			<cfelse>
				<cfset Obras.tipoError= 'database-indefinido'>
				<cfset Obras.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			</cfif>
			<cfreturn Obras>
		</cfcatch>
	</cftry>
</cffunction>


<cffunction name='RSAgregarObras' access='public' returntype='struct'>

	<cfargument name='id_Empresa' type='numeric' required='yes' default='0'>
	<cfargument name='id_Obra' type='numeric' required='yes' default='0'>
	<cfargument name='de_Obra' type='string' required='yes' default=''>
	<cfargument name='fh_Inicio' type='string' required='yes' default='null'>
	<cfargument name='de_Contrato' type='string' required='no' default=''>
	<cfargument name='de_Ubicacion' type='string' required='no' default=''>
	<cfargument name='de_Direccion' type='string' required='no' default=''>
	<cfargument name='id_TipoObra' type='numeric' required='yes' default='0'>
	<cfargument name='id_TipoAvance' type='numeric' required='yes' default='0'>
	<cfargument name='id_Responsable' type='numeric' required='no' default='0'>
	<cfargument name='nu_FactorSalarioReal' type='numeric' required='no' default='0'>
	<cfargument name='nu_AreaConstruccion' type='numeric' required='no' default='0'>
    <cfargument name='nu_Niveles' type='numeric' required='no' default='0'>
    <cfargument name='idu_SistemaConstruccion' type='numeric' required='no' default='0'>
    <cfargument name='sn_Sotano' type='numeric' required='no' default='0'>
    <cfargument name='cl_Estacionamiento' type='numeric' required='no' default='0'>
    <cfargument name='cl_Interviene' type='numeric' required='no' default='0'>
    <cfargument name='cl_Construccion' type='string' required='no' default='0'>
	<cfargument name="id_Ciudad" type="string" required="no" default='null'>
    <cfargument name="id_Contador" type="string" required="no" default='null'>
    <cfargument name="id_EncargadoEstimaciones" type="string" required="no" default='null'>
	<cfargument name="idu_Region" type="string" required="no"  default='null'>
	
    <cfif #id_Contador# eq ''>
    	<cfset id_Contador = 'null'>
    </cfif>
    <cfif #id_EncargadoEstimaciones# eq ''>
    	<cfset id_EncargadoEstimaciones = 'null'>
    </cfif>
	<cfif #idu_Region# eq ''>
    	<cfset idu_Region = 'null'>
    </cfif>
	<cfif #fh_Inicio# eq ''>
    	<cfset fh_Inicio = ''>
    </cfif>
    

	<cftry>	
		<cfquery datasource='#cnx#' name='RSAgregar' username='#user_sql#' password='#password_sql#'>
			EXECUTE bop_Obras_Agregar #id_Empresa#, #id_Obra#, '#de_Obra#', '#fh_Inicio#', '#de_Contrato#', '#de_Ubicacion#', '#de_Direccion#', #id_TipoObra#, #id_TipoAvance#, #id_Responsable#, #nu_FactorSalarioReal#,#id_EncargadoEstimaciones#,#id_GrupoObra#,'#cl_Construccion#',#id_Contador#, #nu_AreaConstruccion#, #id_Ciudad#, #idu_Region#
		</cfquery>

		<!---Log--->
		<cf_log datasource='#cnx#' username='#user_sql#' password='#password_sql#' id_Empresa="#Session.id_Empresa#" id_Obra="#Session.id_Obra#" id_Usuario="#Session.id_Usuario#" id_MenuOpcion="#Session.id_MenuOpcion#">
			<cfoutput>
				EXECUTE bop_Obras_Agregar #id_Empresa#, #id_Obra#, '#de_Obra#', '#fh_Inicio#', '#de_Contrato#', '#de_Ubicacion#', '#de_Direccion#', #id_TipoObra#, #id_TipoAvance#, #id_Responsable#, #nu_FactorSalarioReal#,#id_EncargadoEstimaciones#,#id_GrupoObra#,'#cl_Construccion#',#id_Contador#, #nu_AreaConstruccion#, #id_Ciudad#, #idu_Region#
			</cfoutput>
		</cf_log>
		<!---Termina Log --->
		<cfset Obras.agregado= TRUE>
		<cfset Obras.tipoError= ''>
		<cfset Obras.mensaje= 'Obra guardada correctamente'>
		<cfreturn Obras>
		<cfcatch type='database'>
			<cfset Obras.agregado= FALSE>
			<!---<cfif cfcatch.NativeErrorCode EQ '2627'>
				<cfset Obras.tipoError= 'database-registro_duplicado'>
				<cfset Obras.mensaje= 'La Obra no se pudo guardar porque ya existe'>
			<cfelse>--->
				<cfset Obras.tipoError= 'database-indefinido'>
				<cfset Obras.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<!---</cfif>--->
			<cfreturn Obras>
		</cfcatch>
	</cftry>
</cffunction>


<cffunction name='RSEditarObras' access='public' returntype='struct'>
	
	<cfargument name='id_Empresa' type='string' required='yes' default='0'>
	<cfargument name='id_Obra' type='string' required='yes' default='0'>
	<cfargument name='de_Obra' type='string' required='yes' default=''>
	<cfargument name='fh_Inicio' type='date' required='yes' default=''>
	<cfargument name='de_Contrato' type='string' required='no' default=''>
	<cfargument name='de_Ubicacion' type='string' required='no' default=''>
	<cfargument name='de_Direccion' type='string' required='no' default=''>
	<cfargument name='id_TipoObra' type='string' required='yes' default='0'>
	<cfargument name='id_TipoAvance' type='string' required='yes' default='0'>
	<cfargument name='id_Responsable' type='string' required='no' default='0'>
	<cfargument name='nu_FactorSalarioReal' type='string' required='no' default='0'>
	<cfargument name='nu_AreaConstruccion'  type='string' required='no' default='0'>
    <cfargument name='nu_Niveles' type='string' required='no' default='0'>
    <cfargument name='idu_SistemaConstruccion' type='string' required='no' default='0'>
    <cfargument name='sn_Sotano' type='string' required='no' default='0'>
    <cfargument name='cl_Estacionamiento' type='string' required='no' default='0'>
    <cfargument name='cl_Interviene' type='string' required='no' default='0'>
    <cfargument name='id_Contador' type='string' required="no" default="">
    <cfargument name='id_EncargadoEstimaciones' type='string' required="no" default="">
    <cfargument name='id_Ciudad' type='string' required="no" default="">
	<cfargument name="idu_Region" type="string" required="no"  default='null'>
	<cfif #id_Contador# eq ''>
    	<cfset id_Contador = 'null'>
    </cfif>
    <cfif #id_EncargadoEstimaciones# eq ''>
    	<cfset id_EncargadoEstimaciones = 'null'>
    </cfif>
	<cfif #id_Responsable# eq ''>
    	<cfset id_Responsable = 'null'>
    </cfif>
    <cfif #id_Ciudad# EQ ''>
    	<cfset id_Ciudad = 'NULL'>
    </cfif>
	<cfif #nu_AreaConstruccion# EQ ''>
    	<cfset nu_AreaConstruccion = 0>
    </cfif>
	<cfif #idu_Region# eq ''>
    	<cfset idu_Region = 'null'>
    </cfif>
	
	
	<cftry>    	
		<cfquery datasource='#cnx#' name='RSFilasAfectadas' username='#user_sql#' password='#password_sql#'>
			<!--- EXECUTE bop_Obras_Actualizar #id_Empresa#, #id_Obra#, '#de_Obra#', '#fh_Inicio#', '#de_Contrato#', '#de_Ubicacion#', '#de_Direccion#', #id_TipoObra#, #id_TipoAvance#, #id_Responsable#, #nu_FactorSalarioReal#,#id_EncargadoEstimaciones#,#id_GrupoObra#,'#cl_Construccion#',#id_Contador#, #nu_AreaConstruccion#, #nu_Niveles#, #idu_SistemaConstruccion#, #sn_Sotano#, #cl_Estacionamiento#, #cl_Interviene#, #id_Ciudad# --->
            EXECUTE bop_Obras_Actualizar #id_Empresa#, #id_Obra#, '#de_Obra#', '#fh_Inicio#', '#de_Contrato#', '#de_Ubicacion#', '#de_Direccion#', #id_TipoObra#, #id_TipoAvance#, #id_Responsable#, #nu_FactorSalarioReal#,#id_EncargadoEstimaciones#,#id_GrupoObra#,'#cl_Construccion#',#id_Contador#, #nu_AreaConstruccion#, #id_Ciudad#, #idu_Region#
		</cfquery>
		<!---Log--->
		<cf_log datasource='#cnx#' username='#user_sql#' password='#password_sql#' id_Empresa="#Session.id_Empresa#" id_Obra="#Session.id_Obra#" id_Usuario="#Session.id_Usuario#" id_MenuOpcion="#Session.id_MenuOpcion#">
			<cfoutput>
<!--- 				EXECUTE bop_Obras_Actualizar #id_Empresa#, #id_Obra#, '#de_Obra#', '#fh_Inicio#', '#de_Contrato#', '#de_Ubicacion#', '#de_Direccion#', #id_TipoObra#, #id_TipoAvance#, #id_Responsable#, #nu_FactorSalarioReal#,#id_EncargadoEstimaciones#,#id_GrupoObra#,'#cl_Construccion#',#id_Contador#, #nu_AreaConstruccion#, #nu_Niveles#, #idu_SistemaConstruccion#, #sn_Sotano#, #cl_Estacionamiento#, #cl_Interviene#, #id_Ciudad# --->
                EXECUTE bop_Obras_Actualizar #id_Empresa#, #id_Obra#, '#de_Obra#', '#fh_Inicio#', '#de_Contrato#', '#de_Ubicacion#', '#de_Direccion#', #id_TipoObra#, #id_TipoAvance#, #id_Responsable#, #nu_FactorSalarioReal#,#id_EncargadoEstimaciones#,#id_GrupoObra#,'#cl_Construccion#',#id_Contador#, #nu_AreaConstruccion#, #id_Ciudad#, #idu_Region#
			</cfoutput>
		</cf_log>
		<!---Termina Log --->
		<cfset Obras.actualizado= TRUE>
		<cfset Obras.filasAfectadas= RSFilasAfectadas.filasAfectadas>
		<cfset Obras.tipoError= ''>
		<cfset Obras.mensaje= 'Obra actualizada correctamente'>
		<cfreturn Obras>
		<cfcatch type='database'>
			<cfset Obras.actualizado= FALSE>
			<cfset Obras.tipoError= 'database-indefinido'>
			<cfset Obras.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn Obras>
		</cfcatch>
	</cftry>
</cffunction>

<!--- Funcion para editar los datos de ObrasEstimaciones usados en la tabla de inversion. --->
<!--- Autor: Kaleb Ontiveros --->
<!--- Fecha: 15/02/2013 --->
<cffunction name='RSEditarObrasDet' access='public' returntype='struct'>	
	<cfargument name='id_Empresa' 				type='numeric' required='yes' default='0'>
	<cfargument name='id_Obra' 					type='numeric' required='yes' default='0'>
	<cfargument name='de_Predio' 				type='string' required='yes' default=''>
	<cfargument name='cl_Contabilidad' 			type='string' required='yes' default=''>
	<cfargument name='fh_Bendicion' 			type='string' required='no' default=''>
	<cfargument name='pj_Planeado' 				type='string' required='no' default=''>
	<cfargument name='pj_Real' 					type='string' required='no' default=''>
	<cfargument name='de_GerenteConstruccion' 	type='string' required='yes' default=''>
	<cfargument name='de_Proyectista' 			type='string' required='yes' default=''>
	<cfargument name='de_Residente' 			type='string' required='yes' default=''>

	<cfif de_Predio EQ ''>
        <cfset de_Predio = "NULL">
    </cfif>
	<cfif cl_Contabilidad EQ ''>
    	<cfset cl_Contabilidad = "NULL">
    </cfif>
	<cfif fh_Bendicion EQ '' OR fh_Bendicion EQ "//">
    	<cfset fh_Bendicion = "NULL">
    </cfif>
	<cfif pj_Planeado EQ ''>
    	<cfset pj_Planeado = "NULL">
    </cfif>
	<cfif pj_Real EQ ''>
    	<cfset pj_Real = "NULL">
    </cfif>
	<cfif de_GerenteConstruccion EQ ''>
    	<cfset de_GerenteConstruccion = "NULL">
    </cfif>
	<cfif de_Proyectista EQ ''>
    	<cfset de_Proyectista = "NULL">
    </cfif>
	<cfif de_Residente EQ ''>
    	<cfset de_Residente = "NULL">
    </cfif>                           
	<cftry>
		<cfquery datasource='#cnx#' name='RSFilasAfectadas' username='#user_sql#' password='#password_sql#'>
			EXECUTE ObrasDatosEstimaciones_Editar #id_Empresa#, #id_Obra#,
																<cfif de_Predio EQ "NULL">
                                                                	#de_Predio#
                                                                <cfelse>
                                                                	'#de_Predio#'
                                                                </cfif>,
																<cfif cl_Contabilidad EQ "NULL">
                                                                    #cl_Contabilidad#
                                                                <cfelse>
                                                                	'#cl_Contabilidad#'
                                                                </cfif>,
																<cfif fh_Bendicion EQ "NULL">
                                                                    #fh_Bendicion#
                                                                <cfelse>
                                                                	'#fh_Bendicion#'
                                                                </cfif>,
																<cfif pj_Planeado EQ "NULL">
                                                                    #pj_Planeado#
                                                                <cfelse>
                                                                	'#pj_Planeado#'
                                                                </cfif>,
																<cfif pj_Real EQ "NULL">
                                                                    #pj_Real#
                                                                <cfelse>
                                                                	'#pj_Real#'
                                                                </cfif>,                                                                                                                                                                                                                                                            
																<cfif de_GerenteConstruccion EQ "NULL">
                                                                    #de_GerenteConstruccion#
                                                                <cfelse>
                                                                	'#de_GerenteConstruccion#'
                                                                </cfif>,
																<cfif de_Proyectista EQ "NULL">
                                                                    #de_Proyectista#
                                                                <cfelse>
                                                                	'#de_Proyectista#'
                                                                </cfif>,
																<cfif de_Residente EQ "NULL">
                                                                    #de_Residente#
                                                                <cfelse>
                                                                	'#de_Residente#'
                                                                </cfif>
		</cfquery>
		<cfset Obras.actualizado= TRUE>
		<cfset Obras.tipoError= ''>
		<cfset Obras.mensaje= 'Obra actualizada correctamente'>
		<cfreturn Obras>
		<cfcatch type='database'>
			<cfset Obras.actualizado= FALSE>
			<cfset Obras.tipoError= 'database-indefinido'>
			<cfset Obras.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn Obras>
		</cfcatch>
	</cftry>
</cffunction>


<!--- Funcion para obtener los datos de ObrasEstimaciones usados en la tabla de inversion. --->
<!--- Autor: Kaleb Ontiveros --->
<!--- Fecha: 18/02/2013 --->
<cffunction name='getObrasEstimaciones' access='public' returntype='struct'>	
	<cfargument name='id_Obra' type='numeric' required='yes' default='0'>
    
	<cftry>
		<cfquery datasource='#cnx#' name='RS_DatosObra' username='#user_sql#' password='#password_sql#'>
			ObrasObtenerEnzabezadoTabla #id_Obra#
		</cfquery>
		<cfset Obras.obtenidos= TRUE>
		<cfset Obras.rs= #RS_DatosObra#>        
		<cfset Obras.tipoError= ''>
		<cfset Obras.mensaje= 'Datos Obtenidos Correctamente'>
		<cfreturn Obras>
		<cfcatch type='database'>
			<cfset Obras.obtenidos= FALSE>
			<cfset Obras.tipoError= 'database-indefinido'>
			<cfset Obras.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn Obras>
		</cfcatch>
	</cftry>
</cffunction>


<cffunction name='RSAutorizaBorrarObra' access='public' returntype='struct'>

	<cfargument name='id_Empresa' type='numeric' required='yes'>
	<cfargument name='id_Obra' type='numeric' required='yes'>
	<cftry>
		<cfquery datasource='#cnx#' name='RSFilasAfectadas' username='#user_sql#' password='#password_sql#'>
			EXECUTE Obras_AutorizaBorrar #id_Empresa#, #id_Obra#
		</cfquery>
		<!---Log--->
		<cf_log datasource='#cnx#' username='#user_sql#' password='#password_sql#' id_Empresa="#Session.id_Empresa#" id_Obra="#Session.id_Obra#" id_Usuario="#Session.id_Usuario#" id_MenuOpcion="#Session.id_MenuOpcion#">
			<cfoutput>
				EXECUTE Obras_AutorizaBorrar #id_Empresa#, #id_Obra#
			</cfoutput>
		</cf_log>
		<!---Termina Log --->
		<cfset AutorizaObra.eliminado= TRUE>
      <!---  <cfset AutorizaObra.filasAfectadas= RSFilasAfectadas.filasAfectadas>--->
	    <cfset AutorizaObra.tipoError= ''>
		<cfset AutorizaObra.mensaje= 'Obra eliminada correctamente'>
		<cfreturn AutorizaObra>
		<cfcatch type='database'>
			<cfset AutorizaObra.eliminado= FALSE>
			<cfif cfcatch.NativeErrorCode EQ '547'>
				<cfset AutorizaObra.mensaje= 'La Obra no se pudo eliminar ya que tiene registros relacionados'>
				<cfset AutorizaObra.tipoError= 'database-integridad'>
			<cfelse>
				<cfset AutorizaObra.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
				<cfset AutorizaObra.tipoError= 'database-indefinido'>
			</cfif>
			<cfreturn AutorizaObra>
		</cfcatch>
	</cftry>
</cffunction>


<cffunction name='RSEliminarObras' access='public' returntype='struct'>

	<cfargument name='id_Empresa' type='numeric' required='yes'>
	<cfargument name='id_Obra' type='numeric' required='yes'>

	<cftry>
		<cfquery datasource='#cnx#' name='RSFilasAfectadas' username='#user_sql#' password='#password_sql#'>
			EXECUTE bop_Obras_Eliminar #id_Empresa#, #id_Obra#
		</cfquery>
		<!---Log--->
		<cf_log datasource='#cnx#' username='#user_sql#' password='#password_sql#' id_Empresa="#Session.id_Empresa#" id_Obra="#Session.id_Obra#" id_Usuario="#Session.id_Usuario#" id_MenuOpcion="#Session.id_MenuOpcion#">
			<cfoutput>
				EXECUTE bop_Obras_Eliminar #id_Empresa#, #id_Obra#
			</cfoutput>
		</cf_log>
		<!---Termina Log --->
		<cfset Obras.eliminado= TRUE>
		<cfset Obras.filasAfectadas= RSFilasAfectadas.filasAfectadas>
		<cfset Obras.tipoError= ''>
		<cfset Obras.mensaje= 'Obra eliminada correctamente'>
		<cfreturn Obras>
		<cfcatch type='database'>
			<cfset Obras.eliminado= FALSE>
			<cfif cfcatch.NativeErrorCode EQ '547'>
				<cfset Obras.mensaje= 'La Obra no se pudo eliminar ya que tiene registros relacionados'>
				<cfset Obras.tipoError= 'database-integridad'>
			<cfelse>
				<cfset Obras.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
				<cfset Obras.tipoError= 'database-indefinido'>
			</cfif>
			<cfreturn Obras>
		</cfcatch>
	</cftry>
</cffunction>

<!--- Definicion de funciones extensivas del componente Obras--->
<!---Autor: Hebert G.B --->
<!---Fecha: 2009/04/02 4:45 PM --->
<!---Descripcion: Funcion que trae el campo de_Obra en base a el id_Empresa y el id_Obra --->
	<cffunction name="getObraPorID" access="remote" returntype="string">
 		<cfargument name="id_Empresa" type="string" required="yes">
		<cfargument name="id_Obra" required="yes">
		<cfargument name="comodin1"  required="no" type="string">
		
	
        <cfif #id_Empresa# EQ 'NULL'>
			<cfset #id_Empresa# = 1>
		</cfif>
		<cfif #id_Empresa# NEQ "" AND #id_Obra# NEQ "" AND isNumeric(#id_Obra#)>
		<cfquery name="RS" datasource="#cnx#" username='#user_sql#' password='#password_sql#'>
			SELECT
				de_Obra
			FROM
				Obras 
			WHERE
				id_Empresa= #id_Empresa# AND
				id_Obra= #id_Obra#
		</cfquery>
			<cfset Regreso= RS.de_Obra>
		<cfelse>
			<cfset Regreso="">
		</cfif>
 		<cfreturn Regreso>
 	</cffunction>

<!---Autor: Fernando Bayona --->
<!---Fecha: 2013-11-13 --->
<!---Descripcion: Obtiene la descripcion de la regi�n de la obra --->
    <cffunction name="getRegionObraPorID" access="remote" returntype="string">
 		<cfargument name="id_Empresa" required="yes">
		<cfargument name="id_Obra" required="yes">
        <cfif #id_Empresa# EQ 'NULL'>
			<cfset #id_Empresa# = 1>
		</cfif>
		<cfif #id_Empresa# NEQ "" AND #id_Obra# NEQ "" AND isNumeric(#id_Obra#)>
		<cfquery name="RS" datasource="#cnx#" username='#user_sql#' password='#password_sql#'>
			SELECT 
                r.nom_region 
            FROM 
                Obras o
            INNER JOIN Regiones r 
                ON r.idu_region = o.idu_Region
            WHERE 
                o.id_Obra = #id_Obra#
            AND
                o.id_Empresa = #id_Empresa#
				
		</cfquery>
			<cfset Regreso= RS.nom_region>
		<cfelse>
			<cfset Regreso="">
		</cfif>
 		<cfreturn Regreso>
 	</cffunction>
    
<!---Autor: Fernando Bayona --->
<!---Fecha: 2013-11-13 --->
<!---Descripcion: Obtiene el ID del almac�n de la obra y si no existe lo crea --->
    <cffunction name="getAlmacenPorID" access="remote" returntype="string" returnFormat="json">
 		<cfargument name="id_Empresa" required="no">
		<cfargument name="id_Obra" required="no">
        <cfargument name="id_Almacen" required="no">
        
        <cfif #id_Empresa# EQ 'NULL'>
			<cfset #id_Empresa# = 1>
		</cfif>
        
        <cfset Regreso="0">
        
		<cfif #id_Empresa# NEQ "" AND #id_Obra# NEQ "" AND isNumeric(#id_Obra#) AND #id_Almacen# NEQ "" AND isNumeric(#id_Almacen#)>
                            
        	<cfquery name="RsExiste" datasource="#cnx#" username='#user_sql#' password='#password_sql#'>
            	SELECT idu_centro  FROM cat_mrcentros WHERE idu_centro =#id_Almacen#
            </cfquery>
            
            <cfif RsExiste.idu_centro NEQ ''>
           		
                <cfquery name="RS" datasource="#cnx#" username='#user_sql#' password='#password_sql#'>
                    EXECUTE Proc_AgregarAlmacenMMTO #id_Empresa#, #id_Obra#, '#id_Almacen#'
                </cfquery>
                <cfset Regreso="1">
                
			</cfif>
            
        </cfif>
        
 		<cfreturn Regreso>
        
 	</cffunction>
	
	<!--- Funcion para saber s� una obra ya tiene ppto autorizado en una empresa --->
	<!--- Autor: Kaleb Ontiveros --->
    <!--- Fecha: 06/11/2012 --->
	<cffunction name="obraAutorizada" access="public" returntype="string">
		<cfargument name="id_Obra" required="yes">
			<cftry>
				<cfquery name="RS" datasource="#cnx#" username='#user_sql#' password='#password_sql#'>
					EXECUTE Obras_VerificaPresupuestoAutorizado #id_Obra#
				</cfquery>
				<cfset Obras.rs= #RS.sn_AutorizadoBase#>
				<cfset Obras.tipoError= ''>
				<cfset Obras.mensaje= 'Obra correctamente'>
				<cfreturn #RS.sn_AutorizadoBase#>
				<cfcatch type='database'>
					<cfset Obras.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
					<cfset Obras.tipoError= 'database-indefinido'>
					<cfreturn ''>
				</cfcatch>
			</cftry>
 	</cffunction>


	<!--- Edita el registro de la obra asignandole un Administrador y un Residente--->
    <!---Autor: Alfonso Carranza --->
    <!---Fecha: 2013/01/14 --->
    <cffunction name='AsignacionObras' access='public' returntype='struct'>
        
        <cfargument name='id_Empresa' 			type='numeric' required='yes' default='0'>
        <cfargument name='id_Obra' 				type='numeric' required='yes' default='0'>
        <cfargument name="id_supervisorObra"	type="numeric" required="yes" default="0">
        <cfargument name="id_contador" 			type="numeric" required="yes" default="0">
        <!---<cfargument name="id_residente" 		type="numeric" required="yes" default="0">--->
    
        <cftry>
            <cfquery datasource='#cnx#' name='RSFilasAfectadas' username='#user_sql#' password='#password_sql#'>
                EXECUTE Obras_Asignacion #id_Empresa#, #id_Obra#, #id_supervisorObra#, #id_contador#
            </cfquery>
            <!---Log--->
            <cf_log datasource='#cnx#' username='#user_sql#' password='#password_sql#' id_Empresa="#Session.id_Empresa#" id_Obra="#Session.id_Obra#" id_Usuario="#Session.id_Usuario#" id_MenuOpcion="#Session.id_MenuOpcion#">
                <cfoutput>
                    EXECUTE Obras_Asignacion #id_Empresa#, #id_Obra#, #id_supervisorObra#, #id_contador#
                </cfoutput>
            </cf_log>
            <!---Termina Log --->
            <cfset Obras.actualizado= TRUE>
            <cfset Obras.filasAfectadas= RSFilasAfectadas.filasAfectadas>
            <cfset Obras.tipoError= ''>
            <cfset Obras.mensaje= 'Obra actualizada correctamente'>
            <cfreturn Obras>
            <cfcatch type='database'>
                <cfset Obras.actualizado= FALSE>
                <cfset Obras.tipoError= 'database-indefinido'>
                <cfset Obras.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
                <cfreturn Obras>
            </cfcatch>
        </cftry>
    </cffunction>
    
    <!--- Edita el registro de la obra asignandole un Residente--->
    <!---Autor: Alfonso Carranza --->
    <!---Fecha: 2013/01/14 --->
    
     <cffunction name='AsignacionObrasResidentes' access='public' returntype='struct'>
        
        <cfargument name='idu_Empresa' 			type='numeric' required='yes' default='0'>
        <cfargument name='idu_Obra' 				type='numeric' required='yes' default='0'>
        <cfargument name="idu_residente" 		type="numeric" required="yes" default="0">
    
        <cftry>
            <cfquery datasource='#cnx#' name='RSFilasAfectadas' username='#user_sql#' password='#password_sql#'>
                EXECUTE Obras_AsignacionResidente #idu_Empresa#, #idu_Obra#, #idu_residente#
            </cfquery>
            <!---Log--->
            <cf_log datasource='#cnx#' username='#user_sql#' password='#password_sql#' id_Empresa="#Session.id_Empresa#" id_Obra="#Session.id_Obra#" id_Usuario="#Session.id_Usuario#" id_MenuOpcion="#Session.id_MenuOpcion#">
                <cfoutput>
                    EXECUTE Obras_AsignacionResidente #idu_Empresa#, #idu_Obra#,#idu_residente#
                </cfoutput>
            </cf_log>
            <!---Termina Log --->
            <cfset Obras.actualizado= TRUE>
            <cfset Obras.filasAfectadas= RSFilasAfectadas.filasAfectadas>
            <cfset Obras.tipoError= ''>
            <cfset Obras.mensaje= 'Obra actualizada correctamente'>
            <cfreturn Obras>
            <cfcatch type='database'>
                <cfset Obras.actualizado= FALSE>
                <cfset Obras.tipoError= 'database-indefinido'>
                <cfset Obras.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
                <cfreturn Obras>
            </cfcatch>
        </cftry>
    </cffunction>
	
	<!---Autor: Alfonso Caranza --->
    <!---Fecha: 2013/01/14 --->
    <!---Descripcion: Funcion que trae el campo de_Obra en base a el id_Empresa, el id_Obra y el "id_Residente" --->
	<cffunction name="getObraPorID_Empleado" access="remote" returntype="string">
 		<cfargument name="id_Empresa" required="yes">
		<cfargument name="id_Obra" required="yes">
		<cfargument name="id_Empleado" required="yes">
		<cfif #id_Empresa# NEQ "" AND #id_Obra# NEQ "" AND isNumeric(#id_Obra#)>
		<cfquery name="RS" datasource="#cnx#" username='#user_sql#' password='#password_sql#'>
			SELECT
				de_Obra
			FROM
				Obras 
			WHERE
				id_Empresa= #id_Empresa# 	AND
				id_Obra=#id_Obra#			AND
                (id_Contador=#id_Empleado#	OR
                id_Residente=#id_Empleado#)
		</cfquery>
			<cfset Regreso= RS.de_Obra>
		<cfelse>
			<cfset Regreso="">
		</cfif>
 		<cfreturn Regreso>
 	</cffunction>
    
<!---Autor: Ricardo A. --->
<!---Fecha: 21-01-2013--->
<!---Descripcion: Funcion que trae el campo de_Obra en base a el id_Empresa y el id_Obra y id_Supervisor--->
	<cffunction name="getObraPorID_Supervisor" access="remote" returntype="string">
 		<cfargument name="id_Empresa" required="yes">
        <cfargument name="id_Empleado" required="yes">
		<cfargument name="id_Obra" required="yes">
		<cfif #id_Empresa# NEQ "" AND #id_Obra# NEQ "" AND isNumeric(#id_Obra#)>
		<cfquery name="RS" datasource="#cnx#" username='#user_sql#' password='#password_sql#'>
			SELECT
				de_Obra
			FROM
				Obras 
			WHERE
				--id_Empresa= #id_Empresa# AND
				id_Obra=#id_Obra# 
               <cfif Session.id_Puesto EQ 99>
                and id_SupervisorObra = #Session.id_Empleado#
               </cfif>
		</cfquery>
			<cfset Regreso= RS.de_Obra>
		<cfelse>
			<cfset Regreso="">
		</cfif>
 		<cfreturn Regreso>
 	</cffunction>
    
	<cffunction name="ObtenerObraPorID" access="public" returntype="struct">
		<cfargument name="id_Obra" required="yes">
		<cfif #id_Obra# NEQ "" AND isNumeric(#id_Obra#)>
			<cfquery name="RS" datasource="#cnx#" username='#user_sql#' password='#password_sql#'>
				SELECT
					TOP 1 *
				FROM
					Obras 
				WHERE
					id_Obra= #id_Obra#
			</cfquery>
			<cfset Regreso= RS>
		<cfelse>
			<cfset Regreso="">
		</cfif>
 		<cfreturn Regreso>
 	</cffunction>

	<cffunction name="ObtenerObraPorID_Residente" access="public" returntype="struct">
		<cfargument name="id_Empleado" required="yes">
		<cftry>
			<cfquery name="RS" datasource="#cnx#" username='#user_sql#' password='#password_sql#'>
				<!---SELECT
					TOP 1 *
				FROM
					Obras 
				WHERE
					id_Residente=#id_Empleado#--->
                SELECT TOP 1 *
                FROM Obras AS O
                INNER JOIN ObrasEncargados AS OE ON OE.idu_Empresa = O.id_Empresa
                                 AND OE.idu_Obra = O.id_Obra													  
                WHERE O.id_Residente = #id_Empleado#
                OR OE.idu_Residente = #id_Empleado# 
			</cfquery>
			<cfset Obras.listado= TRUE>
			<cfset Obras.tipoError= ''>
			<cfset Obras.rs= RS>
			<cfset Obras.mensaje= 'Se obtuvo el recordset de Obras correctamente'>
			<cfreturn Obras>
			<cfcatch type='database'>
				<cfset Obras.listado= FALSE>
				<cfset Obras.tipoError= 'database-indefinido'>
				<cfset Obras.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
				<cfreturn Obras>
			</cfcatch>
		</cftry>
 	</cffunction>

<cffunction name='RSTodosObras' access='public' returntype='struct'>
	<cftry>
		<cfquery datasource='#cnx#' name='RS_MostrarObras' username='#user_sql#' password='#password_sql#'>
			EXECUTE Obras_ObtenerTodos 
		</cfquery>
		<cfset Obras.listado= TRUE>
		<cfset Obras.tipoError= ''>
		<cfset Obras.rs= RS_MostrarObras>
		<cfset Obras.mensaje= 'Se obtuvo el recordset de Obras correctamente'>
		<cfreturn Obras>
		<cfcatch type='database'>
			<cfset Obras.listado= FALSE>
			<cfset Obras.tipoError= 'database-indefinido'>
			<cfset Obras.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn Obras>
		</cfcatch>
	</cftry>
</cffunction>

<cffunction name='getEstadoCuenta' access='remote' returntype='string'>	
	<cfargument name='id_Obra' type='numeric' required='yes' default='0'>
    
		<cfif #id_Obra# NEQ "" AND isNumeric(#id_Obra#)>
		<cfquery name="RS_Fecha" datasource="#cnx#" username='#user_sql#' password='#password_sql#'>
			SELECT 
            	TOP 1 [fecha bendici�n] as fh_Bendicion 
            FROM 
            	datosobras DO INNER JOIN Obras O 
                	ON DO.Tienda = O.cl_Construccion 
			WHERE 
            	O.id_Obra = #id_Obra#
		</cfquery>
			<cfif #RS_Fecha.fh_Bendicion# EQ '' >
                <cfset Regreso = "Abierta" >
            <cfelse>
                <cfif #DateFormat(Now(),'DD/MM/YYYY')# GT DateFormat(DateAdd('d',45,RS_Fecha.fh_Bendicion),'DD/MM/YYYY' )>
                    <cfset Regreso = "Cerrada" >                   
                <cfelse>
                    <cfset Regreso = "Abierta" >                   
                </cfif>
            </cfif>
		<cfelse>
			<cfset Regreso="">
		</cfif>
 		<cfreturn Regreso>              
</cffunction>

<!--- Obtiene el encargado de estimaci�n--->
<cffunction name="getEncargadoEst" access="remote" returntype="string">
    <cfargument name="id_Obra" required="yes">
    
    <cfif #id_Obra# NEQ "">
        <cfquery name="RS" datasource="#cnx#" username='#user_sql#' password='#password_sql#'>
            DECLARE @ID_ENCARGADO INT 
			SELECT @ID_ENCARGADO = ID_ENCARGADOESTIMACIONES FROM OBRAS WHERE id_Obra = #id_Obra# 	
			IF @ID_ENCARGADO IS NOT NULL
				BEGIN
					SELECT
						MAX(E.nb_Nombre+' '+E.nb_ApellidoPaterno+' '+E.nb_ApellidoMaterno) as nb_EncargadoEstimaciones
					FROM 
						Obras O INNER JOIN Empleados E 
							ON E.id_Empleado = O.id_EncargadoEstimaciones 
					WHERE
						id_Obra= #id_Obra# 
				END
			ELSE 
				BEGIN 
					SELECT 'Sin Responsable de Estimacion' AS nb_EncargadoEstimaciones
				END
        </cfquery>
        <cfset Regreso= RS.nb_EncargadoEstimaciones>
    <cfelse>
        <cfset Regreso="">
    </cfif>
    <cfreturn Regreso>
</cffunction>        
        
<!--- Funcion para limpiar una obra siempre y cuando no tenga Facturas o Estimaciones ligadas --->
<cffunction name='LimpiarObra' access='public' returntype='struct'>
    <cfargument name="id_Obra" required="yes">
    
	<cftry>
		<cfquery datasource='#cnx#' name='RS_Limpiar' username='#user_sql#' password='#password_sql#'>
			EXECUTE ObrasEliminarPresupuesto #id_Obra# 
		</cfquery>
		<!---Log--->
		<cf_log datasource='#cnx#' username='#user_sql#' password='#password_sql#' id_Empresa="#Session.id_Empresa#" id_Obra="#Session.id_Obra#" id_Usuario="#Session.id_Usuario#" id_MenuOpcion="#Session.id_MenuOpcion#">
			<cfoutput>
				EXECUTE ObrasEliminarPresupuesto #id_Obra#
			</cfoutput>
		</cf_log>
		<!---Termina Log --->        
		<cfset Obras.listado= TRUE>
		<cfset Obras.tipoError= ''>
		<!--- <cfset Obras.rs= RS_Limpiar> --->
		<cfset Obras.mensaje= 'Se realizo la operacion correctamente'>
		<cfreturn Obras>
		<cfcatch type='database'>
			<cfset Obras.listado= FALSE>
			<cfset Obras.tipoError= 'database-indefinido'>
			<cfset Obras.mensaje = Replace(cfcatch.Detail, "[Macromedia][SQLServer JDBC Driver][SQLServer]", "")>
			<cfreturn Obras>
		</cfcatch>
	</cftry>
</cffunction>


<!--- Obtiene el encargado de estimaci�n--->
<cffunction name="getObraPorCl_Construccion" access="remote" returntype="query">
    <cfargument name="id_empresa" type="string" required="yes">
    <cfargument name="cl_Construccion" type="string" required="yes">
    <cfargument name="tipoPermisoMayor" type="string" required="yes">
	
    <cfif #cl_Construccion# NEQ "">
        <cfquery name="RS" datasource="#cnx#" username='#user_sql#' password='#password_sql#'>
            SELECT
				O.id_Obra, O.de_Obra
            FROM 
                Obras O INNER JOIN ObrasEncargados OE ON OE.idu_Obra = O.id_Obra AND OE.idu_Empresa = O.id_Empresa
            WHERE    
			<cfswitch expression='#UCase(Session.nb_Puesto)#'>
				<cfcase  value ='SUPERVISOR DE OBRA'>OE.idu_SupervisorObra = #Session.id_Empleado# AND</cfcase>
				<cfcase  value ='PRESUPUESTOS'>OE.idu_Presupuestos = #Session.id_Empleado# AND</cfcase>
				<cfcase  value ='ENCARGADO ESTIMACIONES'>OE.idu_EncargadoEstimaciones = #Session.id_Empleado# AND</cfcase>
				<cfcase  value ='ADMINISTRADOR DE OBRA'>OE.idu_AdministradorObra = #Session.id_Empleado# AND</cfcase>
				<cfcase  value ='RESIDENTE'>OE.idu_Residente = #Session.id_Empleado#</cfcase>
			</cfswitch>
            	O.id_Empresa = #id_Empresa# AND
                O.cl_Construccion LIKE '#arguments.cl_Construccion#'            
			<cfif tipoPermisoMayor EQ 0>
				AND O.idu_region = #Session.id_region#
			</cfif>
        </cfquery>
        <cfset Regreso= RS>
         <cfreturn Regreso>
    <cfelse>
        <cfset Regreso = QueryNew('id_Obra,de_Obra','Integer, VarChar')>
        <cfreturn Regreso>
    </cfif>
     Regreso>
</cffunction>   

	<cffunction name="RSPresupuestosParametrico" access="public" returntype="struct">
    	<cfargument name="idu_Empresa" type="string" default='NULL'>
        <cfargument name="idu_Obra" type="string" default='NULL'>
        <cfargument name="idu_TipoObra" type="string" default='NULL'>
        <cfargument name="sn_Sotano" type="string" default='NULL'>
        <cfargument name="cl_TipoEstacionamiento" type="string" default='NULL'>
        <cfargument name="cl_Interviene" type="string" default='NULL'>
        <cfif #idu_Empresa# EQ ''>
			<cfset #idu_Empresa# = 'NULL'>
		</cfif>
        <cfif #idu_Obra# EQ ''>
			<cfset #idu_Obra# = 'NULL'>
		</cfif>
        <cfif #idu_TipoObra# EQ ''>
			<cfset #idu_TipoObra# = 'NULL'>
		</cfif>       
        <cfif #sn_Sotano# EQ ''>
			<cfset #sn_Sotano# = 'NULL'>
		</cfif>
        <cfif #cl_TipoEstacionamiento# EQ ''>
			<cfset #cl_TipoEstacionamiento# = 'NULL'>
		</cfif>
        <cfif #cl_Interviene# EQ ''>
			<cfset #cl_Interviene# = 'NULL'>
		</cfif>
    	<cftry>        
			<cfquery datasource="#cnx#" name="RSPresupuestosParametrico" username="#user_sql#" password="#password_sql#">
        		EXECUTE PresupuestosParametrico #idu_Empresa#, #idu_Obra#, #idu_TipoObra#, #sn_Sotano#, #cl_TipoEstacionamiento#, #cl_Interviene#
        	</cfquery>
            <cfset PptoParam.listado= TRUE>
			<cfset PptoParam.tipoError= ''>
            <cfset PptoParam.rs= RSPresupuestosParametrico>
            <cfset PptoParam.mensaje= 'Se obtuvo el recordset de Presupuestos Parametrico correctamente'>
            <cfreturn PptoParam>           
        <cfcatch type="database">
            <cfset PptoParam.listado= FALSE>
			<cfset PptoParam.tipoError= ''>
            <cfset PptoParam.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
            <cfreturn PptoParam>
        </cfcatch>
        </cftry>
	</cffunction>
    
    <cffunction name="RSPresupuestosParametricoAnalisis" access="public" returntype="struct">
    	<cfargument name="idu_Empresa" type="string" default='NULL'>
        <cfargument name="idu_Obra" type="string" default='NULL'>       
        <cfif #idu_Empresa# EQ ''>
			<cfset #idu_Empresa# = 'NULL'>
		</cfif>
        <cfif #idu_Obra# EQ ''>
			<cfset #idu_Obra# = 'NULL'>
		</cfif>
       	<cftry>        
			<cfquery datasource="#cnx#" name="RSPresupuestosParametricoAnalisis" username="#user_sql#" password="#password_sql#">
        		EXECUTE PresupuestosParametricoAnalisis #idu_Empresa#, #idu_Obra#
        	</cfquery>
            <cfset PptoParamAn.listado= TRUE>
			<cfset PptoParamAn.tipoError= ''>
            <cfset PptoParamAn.rs= RSPresupuestosParametricoAnalisis>
            <cfset PptoParamAn.mensaje= 'Se obtuvo el recordset de Presupuestos Parametrico Analisis correctamente'>
            <cfreturn PptoParamAn>           
        <cfcatch type="database">
            <cfset PptoParamAn.listado= FALSE>
			<cfset PptoParamAn.tipoError= ''>
            <cfset PptoParamAn.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
            <cfreturn PptoParamAn>
        </cfcatch>
        </cftry>
	</cffunction>
    
    <cffunction name="RSPresupuestosParametricoSubAnalisis" access="public" returntype="struct">
    	<cfargument name="idu_Empresa" type="string" default='NULL'>
        <cfargument name="idu_Obra" type="string" default='NULL'>
        <cfargument name="idu_CentroCosto" type="string" default='NULL'>        
        <cfif #idu_Empresa# EQ ''>
			<cfset #idu_Empresa# = 'NULL'>
		</cfif>
        <cfif #idu_Obra# EQ ''>
			<cfset #idu_Obra# = 'NULL'>
		</cfif>
         <cfif #idu_CentroCosto# EQ ''>
			<cfset #idu_CentroCosto# = 'NULL'>
		</cfif>
       	<cftry>        
			<cfquery datasource="#cnx#" name="RSPresupuestosParametricoSubAnalisis" username="#user_sql#" password="#password_sql#">
        		EXECUTE PresupuestosParametricoSubAnalisis #idu_Empresa#, #idu_Obra#, #idu_CentroCosto#
        	</cfquery>
            <cfset PptoParamSubAn.listado= TRUE>
			<cfset PptoParamSubAn.tipoError= ''>
            <cfset PptoParamSubAn.rs= RSPresupuestosParametricoSubAnalisis>
            <cfset PptoParamSubAn.mensaje= 'Se obtuvo el recordset de Presupuestos Parametrico SubAnalisis correctamente'>
            <cfreturn PptoParamSubAn>           
        <cfcatch type="database">
            <cfset PptoParamSubAn.listado= FALSE>
			<cfset PptoParamSubAn.tipoError= ''>
            <cfset PptoParamSubAn.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
            <cfreturn PptoParamSubAn>
        </cfcatch>
        </cftry>
	</cffunction>
    
    <cffunction name="RSPptosParametrico" access="public" returntype="struct">
    	<cfargument name="idu_TipoObra" type="string" default='NULL'>
        <cfargument name="sn_Sotano" type="string" default='NULL'>
        <cfargument name="cl_TipoEstacionamiento" type="string" default='NULL'>
        <cfargument name="cl_Interviene" type="string" default='NULL'>
        <cfargument name="idu_Region" type="string" default='NULL'>
        <cfargument name="nu_Niveles" type="string" default='NULL'>
        <cfargument name="idu_SistemaConstruccion" type="string" default='NULL'>
        
        <cfif #idu_TipoObra# EQ ''>
			<cfset #idu_TipoObra# = 'NULL'>
		</cfif>       
        <cfif #sn_Sotano# EQ ''>
			<cfset #sn_Sotano# = 'NULL'>
		</cfif>
        <cfif #cl_TipoEstacionamiento# EQ ''>
			<cfset #cl_TipoEstacionamiento# = 'NULL'>
		</cfif>
        <cfif #cl_Interviene# EQ ''>
			<cfset #cl_Interviene# = 'NULL'>
		</cfif>
        <cfif #idu_Region# EQ ''>
			<cfset #idu_Region# = 'NULL'>
		</cfif>
        <cfif #nu_Niveles# EQ ''>
			<cfset #nu_Niveles# = 'NULL'>
		</cfif>
        <cfif #idu_SistemaConstruccion# EQ ''>
			<cfset #idu_SistemaConstruccion# = 'NULL'>
        </cfif>
            
    	<cftry>        
			<cfquery datasource="#cnx#" name="RSPresupuestosParametrico" username="#user_sql#" password="#password_sql#">
        		EXECUTE PresupuestosParametrico2 #idu_TipoObra#, #sn_Sotano#, #cl_TipoEstacionamiento#, #cl_Interviene#,
                	 #idu_Region#, #nu_Niveles#, #idu_SistemaConstruccion#
        	</cfquery>
            
            <cfdump var="#RSPresupuestosParametrico#">
            <cfabort>
            
            <cfset PptoParam.listado= TRUE>
			<cfset PptoParam.tipoError= ''>
            <cfset PptoParam.rs= RSPresupuestosParametrico>
            <cfset PptoParam.mensaje= 'Se obtuvo el recordset de Presupuestos Parametrico correctamente'>
            <cfreturn PptoParam>           
        <cfcatch type="database">
            <cfset PptoParam.listado= FALSE>
			<cfset PptoParam.tipoError= ''>
            <cfset PptoParam.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
            <cfreturn PptoParam>
        </cfcatch>
        </cftry>
	</cffunction>
    
<cffunction name="obtenerClaveObra" access="remote" returntype="string">
    <cfargument name="id_TipoObra" type="string" default='NULL'>
    <cfargument name="id_Obra" type="string" default='NULL'>
    <cfif #id_Obra# eq '' || #id_TipoObra# eq ''>
        <cfset Obras = ''>
        <cfreturn Obras>
    <cfelse>
            <cftry>    
                <cfquery datasource="#cnx#" name="RS" username="#user_sql#" password="#password_sql#">
                    execute NextID_Obra_Remodelacion_Mantenimiento #id_Obra#, #id_TipoObra#
                </cfquery>
                <cfset Obras= RS.id_Obra>
                <cfreturn Obras>
                <cfcatch type="database">
                    <cfset Obras= 'Error'>    
                    <cfreturn Obras>
                </cfcatch>    
            </cftry>
     </cfif>    
</cffunction>

<cffunction name='RSTodosObras_Construnet' access='public' returntype='struct'>
	<cftry>
		<cfquery datasource='#cnx#' name='RS_MostrarObras' username='#user_sql#' password='#password_sql#'>
		 	SELECT distinct id_Obra, cast (id_Obra as varchar(20))+' - '+de_Obra as de_Obra FROM Obras
			WHERE idu_sector is null
		</cfquery>
		<cfset Obras.listado= TRUE>
		<cfset Obras.tipoError= ''>
		<cfset Obras.rs= RS_MostrarObras>
		<cfset Obras.mensaje= 'Se obtuvo el recordset de Obras correctamente'>
		<cfreturn Obras>
		<cfcatch type='database'>
			<cfset Obras.listado= FALSE>
			<cfset Obras.tipoError= 'database-indefinido'>
			<cfset Obras.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn Obras>
		</cfcatch>
	</cftry>
</cffunction>


<!--- Edita el registro de la obras encargados asignandole un Residente--->
    <!---Autor: Eulises Casta�os --->
    <!---Fecha: 2014/01/13 --->
    
     <cffunction name='AsignacionObrasEncargadosResidente' access='public' returntype='struct'>
        
        <cfargument name='idu_Empresa' 			type='numeric' required='yes' default='0'>
        <cfargument name='idu_Obra' 				type='numeric' required='yes' default='0'>
        <cfargument name="idu_residente" 		type="numeric" required="yes" default="0">
    
        <cftry>
            <cfquery datasource='#cnx#' name='RSFilasAfectadas' username='#user_sql#' password='#password_sql#'>
                EXECUTE obrasEncargados_AsignarResidente #idu_Empresa#, #idu_Obra#, #idu_residente#
            </cfquery>
            <!---Log--->
            <cf_log datasource='#cnx#' username='#user_sql#' password='#password_sql#' id_Empresa="#Session.id_Empresa#" id_Obra="#Session.id_Obra#" id_Usuario="#Session.id_Usuario#" id_MenuOpcion="#Session.id_MenuOpcion#">
                <cfoutput>
                    EXECUTE obrasEncargados_AsignarResidente #idu_Empresa#, #idu_Obra#,#idu_residente#
                </cfoutput>
            </cf_log>
            <!---Termina Log --->
            <cfset Obras.actualizado= TRUE>
            <cfset Obras.filasAfectadas= RSFilasAfectadas.RecordCount>
            <cfset Obras.tipoError= ''>
            <cfset Obras.mensaje= 'Obra actualizada correctamente'>
            <cfreturn Obras>
            <cfcatch type='database'>
                <cfset Obras.actualizado= FALSE>
                <cfset Obras.tipoError= 'database-indefinido'>
                <cfset Obras.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
                <cfreturn Obras>
            </cfcatch>
        </cftry>
    </cffunction>
    
    <cffunction name='AsignacionObrasEncargadosPresupuesto' access='public' returntype='struct'>
        <cfargument name='idu_Empresa' 			type='numeric' required='yes' default='0'>
        <cfargument name='idu_Obra' 			type='numeric' required='yes' default='0'>
        <cfargument name="idu_presupuesto" 		type="numeric" required="yes" default="0">
    
        <cftry>
            <cfquery datasource='#cnx#' name='RSFilasAfectadas' username='#user_sql#' password='#password_sql#'>
                EXECUTE ObrasEncargados_Asignar_Presupuesto #idu_Empresa#, #idu_Obra#, #idu_presupuesto#
            </cfquery>
            <!---Log--->
            <cf_log datasource='#cnx#' username='#user_sql#' password='#password_sql#' id_Empresa="#Session.id_Empresa#" id_Obra="#Session.id_Obra#" id_Usuario="#Session.id_Usuario#" id_MenuOpcion="#Session.id_MenuOpcion#">
                <cfoutput>
                    EXECUTE ObrasEncargados_Asignar_Presupuesto #idu_Empresa#, #idu_Obra#, #idu_presupuesto#
                </cfoutput>
            </cf_log>
            <!---Termina Log --->
            <cfset Obras.actualizado= TRUE>
            <cfset Obras.filasAfectadas= 1>
            <cfset Obras.tipoError= ''>
            <cfset Obras.mensaje= 'Obra actualizada correctamente'>
            <cfreturn Obras>
            <cfcatch type='database'>
                <cfset Obras.actualizado= FALSE>
                <cfset Obras.tipoError= 'database-indefinido'>
                <cfset Obras.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
                <cfreturn Obras>
            </cfcatch>
        </cftry>
    </cffunction>
    
    	<!--- Edita el registro de la obra asignandole un Administrador y un Residente--->
    <!---Autor:Eulises Casta�os--->
    <!---Fecha: 2014/01/14 --->
    <cffunction name='AsignacionObras1' access='public' returntype='struct'>
        
        <cfargument name='idu_Empresa' 			type='numeric' required='yes' default='0'>
        <cfargument name='idu_Obra' 				type='numeric' required='yes' default='0'>
        <cfargument name="idu_supervisorObra"	type="string" required="no" default="">
        <cfargument name="idu_AdministradorObra" 			type="string" required="no" default="">
        <!---<cfargument name="id_residente" 		type="numeric" required="yes" default="0">--->
    <cfif #idu_supervisorObra# eq ''>
    	<cfset idu_supervisorObra = 'null'>
    </cfif>
    <cfif #idu_AdministradorObra# eq ''>
    	<cfset idu_AdministradorObra = 'null'>
    </cfif>
        <cftry>
            <cfquery datasource='#cnx#' name='RSFilasAfectadas' username='#user_sql#' password='#password_sql#'>
                EXECUTE obras_Asignacion #idu_Empresa#, #idu_Obra#, #idu_supervisorObra#, #idu_AdministradorObra#
            </cfquery>
            
            <!---Log--->
            <cf_log datasource='#cnx#' username='#user_sql#' password='#password_sql#' id_Empresa="#Session.id_Empresa#" id_Obra="#Session.id_Obra#" id_Usuario="#Session.id_Usuario#" id_MenuOpcion="#Session.id_MenuOpcion#">
                <cfoutput>
                    EXECUTE obras_Asignacion #idu_Empresa#, #idu_Obra#, #idu_supervisorObra#, #idu_AdministradorObra#
                </cfoutput>
            </cf_log>
            
            <!---Termina Log --->
            <cfset Obras.actualizado= TRUE>
            <cfset Obras.filasAfectadas= RSFilasAfectadas.filasAfectadas>
            <cfset Obras.tipoError= ''>
            <cfset Obras.mensaje= 'Obra actualizada correctamente'>
            <cfreturn Obras>
            <cfcatch type='database'>
                <cfset Obras.actualizado= FALSE>
                <cfset Obras.tipoError= 'database-indefinido'>
                <cfset Obras.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
                <cfreturn Obras>
            </cfcatch>
        </cftry>
    </cffunction>
    <!--- Edita el registro de la obra asignandole un Administrador y un Residente--->
    <!---Autor: Eulises Casta�os--->
    <!---Fecha: 2014/01/14 --->
    <cffunction name='AsignacionObrasEncargados' access='public' returntype='struct'>
        
        <cfargument name='idu_Empresa' 			type='numeric' required='yes' default='0'>
        <cfargument name='idu_Obra' 				type='numeric' required='yes' default='0'>
        <cfargument name="idu_supervisorObra"	type="string" required="no" default="">
        <cfargument name="idu_AdministradorObra" 			type="string" required="no" default="">
        <!---<cfargument name="id_residente" 		type="numeric" required="yes" default="0">--->
    	<cfif #idu_supervisorObra# eq ''>
    	<cfset idu_supervisorObra = 'null'>
    </cfif>
    <cfif #idu_AdministradorObra# eq ''>
    	<cfset idu_AdministradorObra = 'null'>
    </cfif>
        <cftry>
            <cfquery datasource='#cnx#' name='RSFilasAfectadas' username='#user_sql#' password='#password_sql#'>
                EXECUTE obrasEncargados_Asignar #idu_Empresa#, #idu_Obra#, #idu_supervisorObra#, #idu_AdministradorObra#
            </cfquery>
            
            <!---Log--->
            <cf_log datasource='#cnx#' username='#user_sql#' password='#password_sql#' id_Empresa="#Session.id_Empresa#" id_Obra="#Session.id_Obra#" id_Usuario="#Session.id_Usuario#" id_MenuOpcion="#Session.id_MenuOpcion#">
                <cfoutput>
                    EXECUTE obrasEncargados_Asignar #idu_Empresa#, #idu_Obra#, #idu_supervisorObra#, #idu_AdministradorObra#
                </cfoutput>
            </cf_log>
            
            <!---Termina Log --->
            <cfset Obras.actualizado= TRUE>
            <!---<cfset Obras.filasAfectadas= RSFilasAfectadas.filasAfectadas>--->
            <cfset Obras.tipoError= ''>
            <cfset Obras.mensaje= 'Obra actualizada correctamente'>
            <cfreturn Obras>
            <cfcatch type='database'>
            
                <cfset Obras.actualizado= FALSE>
                <cfset Obras.tipoError= 'database-indefinido'>
                <cfset Obras.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
                <cfreturn Obras>
            </cfcatch>
        </cftry>
    </cffunction>
	
<!---Funcion para asignar encargo de la obra--->
<cffunction name="AsignarEncargadoObra" access="public" returntype="struct">
    <cfargument name="id_Obra" type="string" required="yes">
	<cfargument name="id_EncargadoEstimaciones" type="string" required="yes">
    <cfif #id_Obra# eq ''>
        <cfset Obras.mensaje = ''>
		<cfset Obras.Error = 'Datos incompletos'>
        <cfreturn Obras>
    <cfelse>
            <cftry>    
                <cfquery datasource="#cnx#" name="RS" username="#user_sql#" password="#password_sql#">
		 
				update obras set id_EncargadoEstimaciones =  #id_EncargadoEstimaciones#
				where id_obra = #id_Obra#
		
                </cfquery>
                <cfset Obras.mensaje = ''>
				<cfset Obras.Error = 'Se guardo correctamente'>
                <cfreturn Obras>
                <cfcatch type="database">
                    <cfset Obras.mensaje = 1>
					<cfset Obras.Error = 'Error en el acceso a la base de datos'>  
                    <cfreturn Obras>
                </cfcatch>    
            </cftry>
     </cfif>    
</cffunction>

<!---Funcion para asignar el responsable de la obra--->
<cffunction name="AsignarResponsableObra" access="public" returntype="struct">
    <cfargument name="id_Obra" type="string" required="yes">
	<cfargument name="id_Responsable" type="string" required="yes">
    <cfif #id_Obra# eq ''>
        <cfset Obras.mensaje = ''>
		<cfset Obras.Error = 'Datos incompletos'>
        <cfreturn Obras>
    <cfelse>
            <cftry>    
                <cfquery datasource="#cnx#" name="RS" username="#user_sql#" password="#password_sql#">
				   IF NOT EXISTS (SELECT * FROM OBRASENCARGADOS WHERE IDU_OBRA = #id_Obra#)
					BEGIN 
						INSERT INTO OBRASENCARGADOS (IDU_EMPRESA,IDU_OBRA,IDU_PRESUPUESTOS)
						VALUES (1,#id_Obra#,#id_Responsable#)
					END
				   ELSE
					BEGIN
						 update obrasencargados set idu_Presupuestos = #id_Responsable#
						 where idu_obra = #id_Obra#
						   
						 update obras set id_Responsable =  #id_Responsable#
						 where id_obra = #id_Obra#
					END
                </cfquery>
                <cfset Obras.mensaje = ''>
				<cfset Obras.Error = 'Se guardo correctamente'>
                <cfreturn Obras>
                <cfcatch type="database">
                    <cfset Obras.mensaje = 1>
					<cfset Obras.Error = 'Error en el acceso a la base de datos'>  
                    <cfreturn Obras>
                </cfcatch>    
            </cftry>
     </cfif>    
</cffunction>	

<!---Funcion que devuelve obras sugeridas para usuario--->
<cffunction name="getObrasSugeridasEmpleados" access="remote" returntype="query">
	<cfargument name="str_obra" type="string" required="yes">
	<cfargument name="id_Usuario" type="string" required="yes">
	<cfargument name="id_Puesto" type="string" required="yes">
    
	<cfif isNumeric(#str_obra#)>
		<cfset id_Obra = #str_obra#>
		<cfset de_Obra = "NULL">
	<cfelse>
		<cfset id_Obra= "NULL">
		<cfif #str_obra# NEQ "">
			<cfset de_Obra = DE(#str_obra#)>
		<cfelse>
			<cfset de_Obra= "NULL">
		</cfif>
	</cfif>
        
	<cfquery name="RSObras" datasource="#cnx#" username="#user_sql#" password="#password_sql#">
		EXECUTE Obras_ObtenObrasCombo #id_Obra#, #de_Obra#, #id_Usuario#, #id_Puesto#
	</cfquery>
	<cfif #str_obra# EQ "">
		<cfset RSTodos= QueryNew("id_Obra,de_Obra")>
		<cfset QueryAddRow(RSTodos,1)>
		<cfset QuerySetCell(RSTodos,"id_Obra","",1)>
		<cfset QuerySetCell(RSTodos,"de_Obra","SELECCIONE UNA OBRA",1)>
		<cfinvoke component="#Application.componentes#.funciones" method="JuntarQuerys" queryUno="#RSTodos#" queryDos="#RSObras#">
		<cfreturn RSTodos>
	<cfelse>
		<cfreturn RSObras>
	</cfif>
</cffunction>

<cffunction name='getEncabezadoTablaInversion' access='public' returntype='struct'>	
	<cfargument name='id_Obra' type='numeric' required='yes' default='0'>
    
	<cftry>
		<cfquery datasource='#cnx#' name='RS_DatosObra' username='#user_sql#' password='#password_sql#'>
			TablaInversion_Encabezado #id_Obra#
		</cfquery>
		<cfset Obras.obtenidos= TRUE>
		<cfset Obras.rs= #RS_DatosObra#>        
		<cfset Obras.tipoError= ''>
		<cfset Obras.mensaje= 'Datos Obtenidos Correctamente'>
		<cfreturn Obras>
		<cfcatch type='database'>
			<cfset Obras.obtenidos= FALSE>
			<cfset Obras.tipoError= 'database-indefinido'>
			<cfset Obras.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn Obras>
		</cfcatch>
	</cftry>
</cffunction>

<!---REGIONES--->
<cffunction name="Ciudades_regiones" returntype="query" access="remote" output="yes">
	<cfargument name="id_Ciudad"      required="no" type="string">

	<cfif #id_Ciudad# neq ''>
		<cfquery datasource='#cnx#' name='rsCiudades' username='#user_sql#' password='#password_sql#'>
            EXECUTE obras_Regiones_Ciudades #id_Ciudad#
		</cfquery>
		<cfset rs= QueryNew('idu_Region, nom_region')>
		<cfset QueryAddRow(rs)>
		<cfset QuerySetCell(rs,"idu_Region","")>
		<cfset QuerySetCell(rs,"nom_region","SELECCIONE UNA REGION")>
        <cfinvoke component="#Application.componentes#.funciones" method="JuntarQuerys" queryUno="#rs#" queryDos="#rsCiudades#">
    <cfelse>
		<cfset rs= QueryNew('idu_Region, nom_region')>
		<cfset QueryAddRow(rs,1)>
		<cfset QuerySetCell(rs,"idu_Region","",1)>
		<cfset QuerySetCell(rs,"nom_region","SELECCIONE UNA REGION",1)>
    </cfif>
	<cfreturn rs>
</cffunction>

<cffunction name='AsignarFechas' access='public' returntype='struct'>
        
        <cfargument name='id_Obra' type='string' required='yes'>
        <cfargument name='fh_LimitePedidosIniciales' type='string' required='yes'>
        <cfargument name='fh_LiberacionPedidos' type='string' required='yes'>
        <cfargument name='EmpleadoRegistro' type='string' required='yes'>
       
       <cfif fh_LimitePedidosIniciales EQ '--' >
       
      			 <cfset  fh_LimitePedidosIniciales = ''>
                 
	   </cfif>
       
       <cfif fh_LiberacionPedidos EQ '--' >
       
       			<cfset  fh_LiberacionPedidos = ''>
       
	   </cfif>
       
        <cftry>
          
            <cfquery datasource="#cnx#" name="RS_MostrarNextIDObras"  username="#user_sql#" password="#password_sql#">            
                EXECUTE obras_agregar_fecha #id_Obra#,'#fh_LimitePedidosIniciales#','#fh_LiberacionPedidos#',#EmpleadoRegistro#
            </cfquery>
            <cfset Obras.obtenidos= TRUE>                  
            <cfset Obras.tipoError= ''>
            <cfset Obras.mensaje= 'Datos Obtenidos Correctamente'>
            <cfreturn Obras>
       <cfcatch type='database'>
                <cfset Obras.obtenidos= FALSE>
                <cfset Obras.tipoError= 'database-indefinido'>
                <cfset Obras.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
                <cfreturn Obras>
            </cfcatch>
        </cftry>
</cffunction>

<cffunction name='ObtenerFechas' access='public' returntype='struct'>
        
        <cfargument name='id_Obra' type='string' required='yes'> 
        <cftry>
             
          
            <cfquery datasource="#cnx#" name="RS_MostrarNextIDObras"  username="#user_sql#" password="#password_sql#">            
                EXECUTE obras_obtener_fecha #id_Obra#
            </cfquery>
            <cfset Obras.RS= RS_MostrarNextIDObras> 
            <cfset Obras.obtenidos= TRUE>                  
            <cfset Obras.tipoError= ''>
            <cfset Obras.mensaje= 'Datos Obtenidos Correctamente'>
            <cfreturn Obras>
            <cfcatch type='database'>
                <cfset Obras.obtenidos= FALSE>
                <cfset Obras.tipoError= 'database-indefinido'>
                <cfset Obras.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
                <cfreturn Obras>
            </cfcatch>
        </cftry>
</cffunction>


<!--- - - - - - - - - - - - - - KALEB ONTIVEROS - - - - - - - - -- - - - - - - --->
<!--- - - - - - - - - - - - - -FUNCIONES REDOBRA - - - - - - - - - - - - - - - --->
<!--- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  --->

    <cffunction name='getObras' access='remote' returntype='struct' returnformat="JSON">
        <cfargument name='id_empresa' type='any' required='no' default=""> 
        <cfargument name='id_obra'    type='any' required='no' default=""> 
        
        <cftry>
            <cfquery datasource="#cnx#" name="RS_OBRA">
                SELECT O.id_Obra, CAST(O.id_Obra as VARCHAR ) +' - '+O.de_Obra AS de_Obra
                FROM Obras o
                WHERE o.id_Empresa = 1
            </cfquery>
        	<!--- <cfstoredproc procedure="bop_Obras_ObtenerTodos" datasource="#cnx#">
                <!--- <cfprocparam type="IN" cfsqltype="CF_SQL_INTEGER" dbvarname="@id_empresa"   value="#arguments.id_empresa#"  null="#iif(isNumeric(arguments.id_empresa),false,true)#">
                <cfprocparam type="IN" cfsqltype="CF_SQL_INTEGER" dbvarname="@id_obra"      value="#arguments.id_obra#"     null="#iif(isNumeric(arguments.id_obra),false,true)#"> --->
                <cfprocresult name="RS_OBRA" resultset="1">
            </cfstoredproc> --->
            
            <cfset Result.SUCCESS = true />
            <cfset Result.MESSAGE = 'Datos obtenidos con exito.' />
            <cfset Result.RS      = RS_OBRA />
            <cfreturn Result>
            <cfcatch type="any">
            	<cfset Result.SUCCESS = false />
				<cfset Result.MESSAGE = 'Error en el acceso a la base de datos.' />
                <cfset Result.ERROR   = '#cfcatch.Message#' />
                <cfreturn Result>
            </cfcatch>
        </cftry>
    </cffunction>

	<!---
	Autor: Rolando López
	Fecha: 30/09/2014
	 --->
     
     <!---FUNCIÓN PARA ELIMINAR OBRA--->
    <cffunction name='EliminarObra' access='public' returntype='struct'>
    	<cfargument name="id_Obra" required="yes">
	<cftry>
		<cfquery datasource='#cnx#' name='RS_Limpiar' username='#user_sql#' password='#password_sql#'>
			EXECUTE Obras_Eliminar #id_Obra# 
		</cfquery>    
		<cfset Obras.listado= TRUE>
		<cfset Obras.tipoError= ''>
		<!--- <cfset Obras.rs= RS_Limpiar> --->
		<cfset Obras.mensaje= 'La obra se ha eliminado correctamente'>
		<cfreturn Obras>
		<cfcatch type='database'>
			<cfset Obras.listado= FALSE>
			<cfset Obras.tipoError= 'database-indefinido'>
			<cfset Obras.mensaje = Replace(cfcatch.Detail, "[Macromedia][SQLServer JDBC Driver][SQLServer]", "")>
			<cfreturn Obras>
		</cfcatch>
	</cftry>
</cffunction>

<cffunction name='Obraborrartodo' returntype="struct" access="remote" returnformat="JSON" >
    <cfargument name="id_Obra" required="no">
	<cftry>
		<cfquery datasource='#cnx#' name='Limpiar' username='#user_sql#' password='#password_sql#'>
			EXECUTE Obras_BorrarTodo
		</cfquery>
        <cfset Obras.SUCCESS= TRUE>    
		<cfset Obras.tipoError= ''>
		<cfset Obras.MESSAGE= 'Los datos se ha eliminado correctamente'>
		<cfreturn Obras>
		<cfcatch type='database'>
        	<cfset Obras.SUCCESS= FALSE>
			<cfset Obras.tipoError= 'database-indefinido'>
			<cfset Obras.MESSAGE = Replace(cfcatch.Detail, "[Macromedia][SQLServer JDBC Driver][SQLServer]", "")>
			<cfreturn Obras>
		</cfcatch>
	</cftry>
</cffunction>

<!---Autor: Ricardo A. --->
<!---Fecha: 21-01-2013--->
<!---Descripcion: Funcion que trae el campo de_Obra en base a el id_Empresa y el id_Obra y id_Supervisor--->
<cffunction name="getObrafiltroSelect" access="remote" returntype="query">
	<cfargument name="id_Empresa" required="yes">
	<cfargument name="id_Obra" required="yes">
	
	<cfif id_Empresa EQ ''>
		<cfset id_Empresa = 'NULL'>
	</cfif>

	<cfif id_obra EQ ''>
		<cfset id_obra = 'NULL'>
	</cfif>

	<cftry>
		
		<cfquery name="RS" datasource="#session.cnx#">
			exec bop_getObrafiltroSelect #id_Empresa#, #id_Obra#
		</cfquery>

		<cfreturn #RS#>
		<cfcatch type="any">
			<cfreturn ''>
		</cfcatch>

	</cftry>

</cffunction>

<cffunction name="getObrasFiltradoPorId" access="remote" returntype="query">
	<cfargument name="id_Empresa" type="string" required="no" default="">
	<cfargument name="id_obra" 	  type="string" required="no" default="">
	
	<cfif id_Empresa EQ ''>
		<cfset id_Empresa = 'NULL'>
	</cfif>
	<cfif id_obra EQ ''>
		<cfset id_obra = 'NULL'>
	</cfif>
    
	<cfquery name="RSObras" datasource="#session.cnx#" >
		exec bop_getObrafiltroSelect #id_Empresa#, #id_Obra#
	</cfquery>

	<cfif #id_obra# EQ "NULL">
		<cfset RSTodos= QueryNew("id_obra,de_obra")>
		<cfset QueryAddRow(RSTodos,1)>
		<cfset QuerySetCell(RSTodos,"id_obra","",1)>
		<cfset QuerySetCell(RSTodos,"de_obra","SELECCIONE UNA OBRA",1)>
		<cfinvoke component="#Application.componentes#.funciones" method="JuntarQuerys" queryUno="#RSTodos#" queryDos="#RSObras#">
		<cfreturn RSTodos>
	<cfelse>
		<cfreturn RSObras>
	</cfif>
</cffunction>

<cffunction name='getObras_ajax' access='remote' returntype='struct' returnformat="JSON">
        <cfargument name='id_empresa' type='any' required='no' default=""> 
        <cfargument name='id_obra'    type='any' required='no' default=""> 
        
        <cftry>
            <cfquery datasource="#cnx#" name="RS_OBRA">
            	<cfif #id_Empresa# NEQ ''>
            	
	                SELECT O.id_Obra,  de_Obra
	                FROM Obras o
	                WHERE o.id_Empresa = #id_empresa# AND id_Obra = #id_Obra#
	            <cfelse>
	            	SELECT top 1 O.id_Obra,  de_Obra
	                FROM Obras o
	                WHERE id_Obra = #id_Obra#    
                </cfif>
            </cfquery>
            
            <cfset Result.SUCCESS = true />
            <cfset Result.MESSAGE = 'Datos obtenidos con exito.' />
            <cfset Result.RS      = RS_OBRA />
            <cfreturn Result>
            <cfcatch type="any">
            	<cfset Result.SUCCESS = false />
				<cfset Result.MESSAGE = 'Error en el acceso a la base de datos.' />
                <cfset Result.ERROR   = '#cfcatch.Message#' />
                <cfreturn Result>
            </cfcatch>
        </cftry>
    </cffunction>

</cfcomponent>