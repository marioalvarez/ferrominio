<cfcomponent extends='conexion'>
	<!---Funcion para pantalla Tramitar a pago facturas-------------->
	<!---Desarrollador: Miguel Tiznado ------------------------------>
	<!---Fecha: 30/09/2015------------------------------------------->
	<cffunction name="ContraRecibosDetalle_TramitarAPago" access="remote" returntype="struct" returnformat="json">
		<cfargument name="registros" 			type="array" required="true">
		<cftransaction action="begin">
			<cftry>
				<cfset Result.SQL = "">
				<cfloop array="#registros#" index="registro" >
					<cfset Result.SQL &= "
					EXECUTE ContraRecibosDetalle_TramitarAPago  
						@id_Empresa				= #registro.id_Empresa#,
						@id_ContraRecibo		= #registro.id_ContraRecibo#,  
						@nd_ContraRecibo		= #registro.nd_ContraRecibo#,  
						@id_EstatusFactura		= #registro.id_EstatusFactura#,
						@fh_Pago				= '#registro.fh_Pago#',
						@de_Observaciones		= '#registro.de_Observaciones#'
					">
					</cfloop>
				<cfquery name="RSDatos" datasource="#session.cnx#">                
					#PreserveSingleQuotes(Result.SQL)#
				</cfquery>
				<cfset Result.SUCCESS = true>
				<cfset Result.MESSAGE = 'La operacion se realizo correctamente.'>
				<cftransaction action="commit"> 
				<cfreturn result>
				<cfcatch type='database'>
					<cfset Result.SUCCESS = false>
					<cfset Result.TIPOERROR = "Error en la consulta de base de datos.">
					<cfset Result.MESSAGE = "Error al recuperar los datos => #cfcatch.Detail#">
					<cftransaction action="rollback">    
					<cfreturn Result>
				</cfcatch>
			</cftry>
		</cftransaction>
	</cffunction>
	
<cffunction name='RSMostrarNextIDContraRecibosDetalle' access='public' returntype='struct'>

	<cfargument name='id_Empresa' type='numeric' required='yes'>
	<cfargument name='id_ContraRecibo' type='numeric' required='yes'> 

	<cftry>
		<cfquery datasource='#cnx#' name='RS_MostrarNextIDContraRecibosDetalle' username='#user_sql#' password='#password_sql#'>
			EXECUTE bop_ContraRecibosDetalle_MostrarNextID #id_Empresa#, #id_ContraRecibo#
		</cfquery>
		<cfset ContraRecibosDetalle.NextID= TRUE>
		<cfset ContraRecibosDetalle.tipoError= ''>
		<cfset ContraRecibosDetalle.rs= RS_MostrarNextIDContraRecibosDetalle>
		<cfset ContraRecibosDetalle.mensaje= 'Se obtuvo el recordset de Detalles de Contrarecibo correctamente'>
		<cfreturn ContraRecibosDetalle>
		<cfcatch type='database'>
			<cfset ContraRecibosDetalle.NextID= FALSE>
			<cfset ContraRecibosDetalle.tipoError= 'database-indefinido'>
			<cfset ContraRecibosDetalle.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn ContraRecibosDetalle>
		</cfcatch>
	</cftry>
</cffunction>


<cffunction name='RSMostrarPorIDContraRecibosDetalle' access='public' returntype='struct'>

	<cfargument name='id_Empresa' type='numeric' required='yes'>
	<cfargument name='id_ContraRecibo' type='numeric' required='yes'>
	<cfargument name='nd_ContraRecibo' type='numeric' required='yes'>

	<cftry>
		<cfquery datasource='#cnx#' name='RS_MostrarContraRecibosDetalle' username='#user_sql#' password='#password_sql#'>
			EXECUTE bop_ContraRecibosDetalle_ObtenerPorID #id_Empresa#, #id_ContraRecibo#, #nd_ContraRecibo#
		</cfquery>
		<cfset ContraRecibosDetalle.listado= TRUE>
		<cfset ContraRecibosDetalle.tipoError= ''>
		<cfset ContraRecibosDetalle.rs= RS_MostrarContraRecibosDetalle>
		<cfset ContraRecibosDetalle.mensaje= 'Se obtuvo el recordset de Detalles de Contrarecibo correctamente'>
		<cfreturn ContraRecibosDetalle>
		<cfcatch type='database'>
			<cfset ContraRecibosDetalle.listado= FALSE>
			<cfset ContraRecibosDetalle.tipoError= 'database-indefinido'>
			<cfset ContraRecibosDetalle.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn ContraRecibosDetalle>
		</cfcatch>
	</cftry>
</cffunction>


<cffunction name='RSMostrarDinamicoContraRecibosDetalle' access='public' returntype='struct'>

	<cfargument name='id_Empresa' type='numeric' required='no' default='-1'>
	<cfargument name='id_ContraRecibo' type='numeric' required='no' default='-1'>
	<cfargument name='nd_ContraRecibo' type='numeric' required='no' default='-1'>
	<cfargument name='id_Proveedor' type='numeric' required='no' default='-1'>
	<cfargument name='id_Obra' type='numeric' required='no' default='-1'>
	<cfargument name='id_OrdenCompra' type='numeric' required='no' default='-1'>
	<cfargument name='de_Factura' type='string' required='no' default=''>
	<cfargument name='fh_Factura' type='string' required='no' default=''>
	<cfargument name='fh_FacturaVencimiento' type='string' required='no' default=''>
	<cfargument name='id_Modulo' type='numeric' required='no' default='-1'>
	<cfargument name='id_TipoMovimiento' type='numeric' required='no' default='-1'>
	<cfargument name='id_CentroCosto' type='numeric' required='no' default='-1'>
	<cfargument name='de_Concepto' type='string' required='no' default=''>
	<cfargument name='de_Referencia' type='string' required='no' default=''>
	<cfargument name='im_Factura' type='numeric' required='no' default='-1'>
	<cfargument name='id_Moneda' type='numeric' required='no' default='-1'>
	<cfargument name='im_TipoCambio' type='numeric' required='no' default='-1'>
	<cfargument name='cl_FormaPago' type='string' required='no' default=''>
	<cfargument name='id_Estatus' type='numeric' required='no' default='-1'>
	<cfargument name='id_EmpleadoRegistro' type='numeric' required='no' default='-1'>
	<cfargument name='id_EmpleadoBloqueo' type='numeric' required='no' default='-1'>
	<cfargument name='id_EmpleadoAutorizo' type='numeric' required='no' default='-1'>

	<cftry>
		<cfset WhereDinamico=''>
		<cfif #id_Empresa# NEQ '-1'>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE id_Empresa = #id_Empresa#">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND id_Empresa = #id_Empresa#">
			</cfif>
		</cfif>
		<cfif #id_ContraRecibo# NEQ '-1'>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE id_ContraRecibo = #id_ContraRecibo#">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND id_ContraRecibo = #id_ContraRecibo#">
			</cfif>
		</cfif>
		<cfif #nd_ContraRecibo# NEQ '-1'>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE nd_ContraRecibo = #nd_ContraRecibo#">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND nd_ContraRecibo = #nd_ContraRecibo#">
			</cfif>
		</cfif>
		<cfif #id_Proveedor# NEQ '-1'>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE id_Proveedor = #id_Proveedor#">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND id_Proveedor = #id_Proveedor#">
			</cfif>
		</cfif>
		<cfif #id_Obra# NEQ '-1'>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE id_Obra = #id_Obra#">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND id_Obra = #id_Obra#">
			</cfif>
		</cfif>
		<cfif #id_OrdenCompra# NEQ '-1'>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE id_OrdenCompra = #id_OrdenCompra#">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND id_OrdenCompra = #id_OrdenCompra#">
			</cfif>
		</cfif>
		<cfif #de_Factura# NEQ ''>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE de_Factura LIKE '#de_Factura#%'">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND de_Factura LIKE '#de_Factura#%'">
			</cfif>
		</cfif>
		<cfif #fh_Factura# NEQ ''>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE fh_Factura = '#fh_Factura#'">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND fh_Factura = '#fh_Factura#'">
			</cfif>
		</cfif>
		<cfif #fh_FacturaVencimiento# NEQ ''>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE fh_FacturaVencimiento = '#fh_FacturaVencimiento#'">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND fh_FacturaVencimiento = '#fh_FacturaVencimiento#'">
			</cfif>
		</cfif>
		<cfif #id_Modulo# NEQ '-1'>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE id_Modulo = #id_Modulo#">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND id_Modulo = #id_Modulo#">
			</cfif>
		</cfif>
		<cfif #id_TipoMovimiento# NEQ '-1'>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE id_TipoMovimiento = #id_TipoMovimiento#">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND id_TipoMovimiento = #id_TipoMovimiento#">
			</cfif>
		</cfif>
		<cfif #id_CentroCosto# NEQ '-1'>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE id_CentroCosto = #id_CentroCosto#">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND id_CentroCosto = #id_CentroCosto#">
			</cfif>
		</cfif>
		<cfif #de_Concepto# NEQ ''>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE de_Concepto LIKE '#de_Concepto#%'">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND de_Concepto LIKE '#de_Concepto#%'">
			</cfif>
		</cfif>
		<cfif #de_Referencia# NEQ ''>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE de_Referencia LIKE '#de_Referencia#%'">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND de_Referencia LIKE '#de_Referencia#%'">
			</cfif>
		</cfif>
		<cfif #im_Factura# NEQ '-1'>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE im_Factura = #im_Factura#">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND im_Factura = #im_Factura#">
			</cfif>
		</cfif>
		<cfif #id_Moneda# NEQ '-1'>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE id_Moneda = #id_Moneda#">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND id_Moneda = #id_Moneda#">
			</cfif>
		</cfif>
		<cfif #im_TipoCambio# NEQ '-1'>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE im_TipoCambio = #im_TipoCambio#">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND im_TipoCambio = #im_TipoCambio#">
			</cfif>
		</cfif>
		<cfif #cl_FormaPago# NEQ ''>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE cl_FormaPago LIKE '%#cl_FormaPago#'">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND cl_FormaPago LIKE '%#cl_FormaPago#'">
			</cfif>
		</cfif>
		<cfif #id_Estatus# NEQ '-1'>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE id_Estatus = #id_Estatus#">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND id_Estatus = #id_Estatus#">
			</cfif>
		</cfif>
		<cfif #id_EmpleadoRegistro# NEQ '-1'>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE id_EmpleadoRegistro = #id_EmpleadoRegistro#">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND id_EmpleadoRegistro = #id_EmpleadoRegistro#">
			</cfif>
		</cfif>
		<cfif #id_EmpleadoBloqueo# NEQ '-1'>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE id_EmpleadoBloqueo = #id_EmpleadoBloqueo#">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND id_EmpleadoBloqueo = #id_EmpleadoBloqueo#">
			</cfif>
		</cfif>
		<cfif #id_EmpleadoAutorizo# NEQ '-1'>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE id_EmpleadoAutorizo = #id_EmpleadoAutorizo#">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND id_EmpleadoAutorizo = #id_EmpleadoAutorizo#">
			</cfif>
		</cfif>

		<cfquery datasource='#cnx#' name='RS_MostrarContraRecibosDetalle' username='#user_sql#' password='#password_sql#'>
			EXECUTE bop_ContraRecibosDetalle_ObtenerDinamico '#WhereDinamico#'
		</cfquery>
		<cfset ContraRecibosDetalle.listado= TRUE>
		<cfset ContraRecibosDetalle.tipoError= ''>
		<cfset ContraRecibosDetalle.rs= RS_MostrarContraRecibosDetalle>
		<cfset ContraRecibosDetalle.mensaje= 'Se obtuvo el recordset de Detalles de Contrarecibo correctamente'>
		<cfreturn ContraRecibosDetalle>
		<cfcatch type='database'>
			<cfset ContraRecibosDetalle.listado= FALSE>
			<cfset ContraRecibosDetalle.tipoError= 'database-indefinido'>
			<cfset ContraRecibosDetalle.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn ContraRecibosDetalle>
		</cfcatch>
	</cftry>
</cffunction>


<cffunction name='RSMostrarTodosContraRecibosDetalle' access='public' returntype='struct'>

	<cftry>
		<cfquery datasource='#cnx#' name='RS_MostrarContraRecibosDetalle' username='#user_sql#' password='#password_sql#'>
			EXECUTE bop_ContraRecibosDetalle_ObtenerTodos 
		</cfquery>
		<cfset ContraRecibosDetalle.listado= TRUE>
		<cfset ContraRecibosDetalle.tipoError= ''>
		<cfset ContraRecibosDetalle.rs= RS_MostrarContraRecibosDetalle>
		<cfset ContraRecibosDetalle.mensaje= 'Se obtuvo el recordset de Detalles de Contrarecibo correctamente'>
		<cfreturn ContraRecibosDetalle>
		<cfcatch type='database'>
			<cfset ContraRecibosDetalle.listado= FALSE>
			<cfset ContraRecibosDetalle.tipoError= 'database-indefinido'>
			<cfset ContraRecibosDetalle.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn ContraRecibosDetalle>
		</cfcatch>
	</cftry>
</cffunction>


<cffunction name='RSAgregarConNextIDContraRecibosDetalle' access='public' returntype='struct'>

	<cfargument name='id_Empresa' type='numeric' required='yes'>
	<cfargument name='id_ContraRecibo' type='numeric' required='yes'>
	<cfargument name='id_Proveedor' type='numeric' required='yes' default='0'>
	<cfargument name='id_Obra' type='numeric' required='yes' default='0'>
	<cfargument name='id_OrdenCompra' type='numeric' required='yes' default='0'>
	<cfargument name='de_Factura' type='string' required='yes' default=''>
	<cfargument name='fh_Factura' type='date' required='yes' default=''>
	<cfargument name='fh_FacturaVencimiento' type='date' required='no' default=''>
	<cfargument name='id_Modulo' type='numeric' required='yes' default='0'>
	<cfargument name='id_TipoMovimiento' type='numeric' required='yes' default='0'>
	<cfargument name='id_CentroCosto' type='numeric' required='yes' default='0'>
	<cfargument name='de_Concepto' type='string' required='no' default=''>
	<cfargument name='de_Referencia' type='string' required='no' default=''>
	<cfargument name='im_Factura' type='numeric' required='yes' default='0'>
	<cfargument name='id_Moneda' type='numeric' required='yes' default='0'>
	<cfargument name='im_TipoCambio' type='numeric' required='yes' default='0'>
	<cfargument name='cl_FormaPago' type='string' required='yes' default=''>
	<cfargument name='id_Estatus' type='numeric' required='yes' default='0'>
	<cfargument name='id_EmpleadoRegistro' type='numeric' required='yes' default='0'>
	<cfargument name='id_EmpleadoBloqueo' type='numeric' required='no' default='0'>
	<cfargument name='id_EmpleadoAutorizo' type='numeric' required='no' default='0'>

	<cftry>
		<cftransaction>
			<cfset NextIDtmp= this.RSMostrarNextIDCentrosCostos(arguments.cnx, arguments.user_sql, arguments.password_sql, attributes.id_Empresa, attributes.id_ContraRecibo).rs.NextID>
			<cfset ContraRecibosDetalle= this.RSAgregarCentrosCostos(arguments.cnx, arguments.user_sql, arguments.password_sql, arguments.id_Empresa, arguments.id_ContraRecibo, NextIDtmp, arguments.id_Proveedor, arguments.id_Obra, arguments.id_OrdenCompra, arguments.de_Factura, arguments.fh_Factura, arguments.fh_FacturaVencimiento, arguments.id_Modulo, arguments.id_TipoMovimiento, arguments.id_CentroCosto, arguments.de_Concepto, arguments.de_Referencia, arguments.im_Factura, arguments.id_Moneda, arguments.im_TipoCambio, arguments.cl_FormaPago, arguments.id_Estatus, arguments.id_EmpleadoRegistro, arguments.id_EmpleadoBloqueo, arguments.id_EmpleadoAutorizo)>
		</cftransaction>
		<cfset ContraRecibosDetalle.NextID= NextIDtmp>
		<cfreturn ContraRecibosDetalle>
		<cfcatch type='database'>
			<cftransaction action='rollback'/>
			<cfset ContraRecibosDetalle.agregado= FALSE>
			<cfif cfcatch.NativeErrorCode EQ '2627'>
				<cfset ContraRecibosDetalle.tipoError= 'database-registro_duplicado'>
				<cfset ContraRecibosDetalle.mensaje= 'El Detalle de Contrarecibo no se pudo guardar porque ya existe'>
			<cfelse>
				<cfset ContraRecibosDetalle.tipoError= 'database-indefinido'>
				<cfset ContraRecibosDetalle.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			</cfif>
			<cfreturn ContraRecibosDetalle>
		</cfcatch>
	</cftry>
</cffunction>


<cffunction name='RSAgregarContraRecibosDetalle' access='public' returntype='struct'>

	<cfargument name='id_Empresa' type='numeric' required='yes' default='0'>
	<cfargument name='id_ContraRecibo' type='numeric' required='yes' default='0'>
	<cfargument name='nd_ContraRecibo' type='numeric' required='yes' default='0'>
	<cfargument name='id_Proveedor' type='numeric' required='yes' default='0'>
	<cfargument name='id_Obra' type='numeric' required='yes' default='0'>
	<cfargument name='id_OrdenCompra' type='numeric' required='yes' default='0'>
	<cfargument name='de_Factura' type='string' required='yes' default=''>
	<cfargument name='fh_Factura' type='date' required='yes' default=''>
	<cfargument name='fh_FacturaVencimiento' type='date' required='no' default=''>
	<cfargument name='id_Modulo' type='numeric' required='yes' default='0'>
	<cfargument name='id_TipoMovimiento' type='numeric' required='yes' default='0'>
	<cfargument name='id_CentroCosto' type='numeric' required='yes' default='0'>
	<cfargument name='de_Concepto' type='string' required='no' default=''>
	<cfargument name='de_Referencia' type='string' required='no' default=''>
	<cfargument name='im_Factura' type='numeric' required='yes' default='0'>
	<cfargument name='id_Moneda' type='numeric' required='yes' default='0'>
	<cfargument name='im_TipoCambio' type='numeric' required='yes' default='0'>
	<cfargument name='cl_FormaPago' type='string' required='no' default=''>
	<cfargument name='id_Estatus' type='numeric' required='yes' default='0'>
	<cfargument name='id_EmpleadoRegistro' type='numeric' required='yes' default='0'>
	<cfargument name='id_EmpleadoBloqueo' type='string' required='no' default=''>
	<cfargument name='id_EmpleadoAutorizo' type='string' required='no' default=''>
	<cfargument name='id_EstatusFactura' type='string' required='no' default=''>
	<cfif cl_FormaPago NEQ "">
		<cfset cl_FormaPago = DE(#cl_FormaPago#)>
	<cfelse>
		<cfset cl_FormaPago = 'null'>
	</cfif>
	<cfif id_EmpleadoBloqueo EQ "">
		<cfset id_EmpleadoBloqueo = 'null'>
	</cfif>
	<cfif id_EmpleadoAutorizo EQ "">
		<cfset id_EmpleadoAutorizo = 'null'>
	</cfif>
	<cfif id_EstatusFactura EQ "">
		<cfset id_EstatusFactura = 'null'>
	</cfif>
	<cftry>
		<cfquery datasource='#cnx#' name='RSAgregar' username='#user_sql#' password='#password_sql#'>
			EXECUTE bop_ContraRecibosDetalle_Agregar #id_Empresa#, #id_ContraRecibo#, #nd_ContraRecibo#, #id_Proveedor#, #id_Obra#, #id_OrdenCompra#, '#de_Factura#', '#fh_Factura#', '#fh_FacturaVencimiento#', #id_Modulo#, #id_TipoMovimiento#, #id_CentroCosto#, '#de_Concepto#', '#de_Referencia#', #im_Factura#, #id_Moneda#, #im_TipoCambio#, #cl_FormaPago#, #id_Estatus#, #id_EmpleadoRegistro#, #id_EmpleadoBloqueo#, #id_EmpleadoAutorizo#,#id_EstatusFactura#
		</cfquery>
		<cfset ContraRecibosDetalle.agregado= TRUE>
		<cfset ContraRecibosDetalle.tipoError= ''>
		<cfset ContraRecibosDetalle.mensaje= 'Detalle de Contrarecibo guardado correctamente'>
		<cfreturn ContraRecibosDetalle>
		<cfcatch type='database'>
			<cfset ContraRecibosDetalle.agregado= FALSE>
			<cfif cfcatch.NativeErrorCode EQ '2627'>
				<cfset ContraRecibosDetalle.tipoError= 'database-registro_duplicado'>
				<cfset ContraRecibosDetalle.mensaje= 'El Detalle de Contrarecibo no se pudo guardar porque ya existe'>
			<cfelse>
				<cfset ContraRecibosDetalle.tipoError= 'database-indefinido'>
				<cfset ContraRecibosDetalle.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			</cfif>
			<cfreturn ContraRecibosDetalle>
		</cfcatch>
	</cftry>
</cffunction>


<cffunction name='RSEditarContraRecibosDetalle' access='public' returntype='struct'>

	<cfargument name='id_Empresa' type='numeric' required='yes'>
	<cfargument name='id_ContraRecibo' type='numeric' required='yes'>
	<cfargument name='nd_ContraRecibo' type='numeric' required='yes'>
	<cfargument name='id_Proveedor' type='numeric' required='yes' default='0'>
	<cfargument name='id_Obra' type='numeric' required='yes' default='0'>
	<cfargument name='id_OrdenCompra' type='numeric' required='yes' default='0'>
	<cfargument name='de_Factura' type='string' required='yes' default=''>
	<cfargument name='fh_Factura' type='date' required='yes' default=''>
	<cfargument name='fh_FacturaVencimiento' type='date' required='no' default=''>
	<cfargument name='id_Modulo' type='numeric' required='yes' default='0'>
	<cfargument name='id_TipoMovimiento' type='numeric' required='yes' default='0'>
	<cfargument name='id_CentroCosto' type='numeric' required='yes' default='0'>
	<cfargument name='de_Concepto' type='string' required='no' default=''>
	<cfargument name='de_Referencia' type='string' required='no' default=''>
	<cfargument name='im_Factura' type='numeric' required='yes' default='0'>
	<cfargument name='id_Moneda' type='numeric' required='yes' default='0'>
	<cfargument name='im_TipoCambio' type='numeric' required='yes' default='0'>
	<cfargument name='cl_FormaPago' type='string' required='yes' default=''>
	<cfargument name='id_Estatus' type='numeric' required='yes' default='0'>
	<cfargument name='id_EmpleadoRegistro' type='numeric' required='yes' default='0'>
	<cfargument name='id_EmpleadoBloqueo' type='numeric' required='no' default='0'>
	<cfargument name='id_EmpleadoAutorizo' type='numeric' required='no' default='0'>

	<cftry>
		<cfquery datasource='#cnx#' name='RSFilasAfectadas' username='#user_sql#' password='#password_sql#'>
			EXECUTE bop_ContraRecibosDetalle_Actualizar #id_Empresa#, #id_ContraRecibo#, #nd_ContraRecibo#, #id_Proveedor#, #id_Obra#, #id_OrdenCompra#, '#de_Factura#', '#fh_Factura#', '#fh_FacturaVencimiento#', #id_Modulo#, #id_TipoMovimiento#, #id_CentroCosto#, '#de_Concepto#', '#de_Referencia#', #im_Factura#, #id_Moneda#, #im_TipoCambio#, '#cl_FormaPago#', #id_Estatus#, #id_EmpleadoRegistro#, #id_EmpleadoBloqueo#, #id_EmpleadoAutorizo#
		</cfquery>
		<cfset ContraRecibosDetalle.actualizado= TRUE>
		<cfset ContraRecibosDetalle.filasAfectadas= RSFilasAfectadas.filasAfectadas>
		<cfset ContraRecibosDetalle.tipoError= ''>
		<cfset ContraRecibosDetalle.mensaje= 'Detalle de Contrarecibo actualizado correctamente'>
		<cfreturn ContraRecibosDetalle>
		<cfcatch type='database'>
			<cfset ContraRecibosDetalle.actualizado= FALSE>
			<cfset ContraRecibosDetalle.tipoError= 'database-indefinido'>
			<cfset ContraRecibosDetalle.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn ContraRecibosDetalle>
		</cfcatch>
	</cftry>
</cffunction>


<cffunction name='RSEliminarContraRecibosDetalle' access='public' returntype='struct'>

	<cfargument name='id_Empresa' type='numeric' required='yes'>
	<cfargument name='id_ContraRecibo' type='numeric' required='yes'>
	<cfargument name='nd_ContraRecibo' type='numeric' required='yes'>

	<cftry>
		<cfquery datasource='#cnx#' name='RSFilasAfectadas' username='#user_sql#' password='#password_sql#'>
			EXECUTE bop_ContraRecibosDetalle_Eliminar #id_Empresa#, #id_ContraRecibo#, #nd_ContraRecibo#
		</cfquery>
		<cfset ContraRecibosDetalle.eliminado= TRUE>
		<cfset ContraRecibosDetalle.filasAfectadas= RSFilasAfectadas.filasAfectadas>
		<cfset ContraRecibosDetalle.tipoError= ''>
		<cfset ContraRecibosDetalle.mensaje= 'Detalle de Contrarecibo eliminado correctamente'>
		<cfreturn ContraRecibosDetalle>
		<cfcatch type='database'>
			<cfset ContraRecibosDetalle.eliminado= FALSE>
			<cfif cfcatch.NativeErrorCode EQ '547'>
				<cfset ContraRecibosDetalle.mensaje= 'El Detalle de Contrarecibo no se pudo eliminar ya que tiene registros relacionados'>
				<cfset ContraRecibosDetalle.tipoError= 'database-integridad'>
			<cfelse>
				<cfset ContraRecibosDetalle.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
				<cfset ContraRecibosDetalle.tipoError= 'database-indefinido'>
			</cfif>
			<cfreturn ContraRecibosDetalle>
		</cfcatch>
	</cftry>
</cffunction>

<!--- Funcion para traer los detalles de un contrarecibo --->
<cffunction name='RSContrarecibosDetalleListado' access='public' returntype='struct'>

	<cfargument name='id_Empresa' type='numeric' required='yes'>
	<cfargument name='id_ContraRecibo' type='numeric' required='yes'>

	<cftry>
		<cfquery datasource='#cnx#' name='RS_MostrarContraRecibosDetalle' username='#user_sql#' password='#password_sql#'>
			EXECUTE ContrarecibosDetalle_Listado #id_Empresa#, #id_ContraRecibo#
		</cfquery>
		<cfset ContraRecibosDetalle.listado= TRUE>
		<cfset ContraRecibosDetalle.tipoError= ''>
		<cfset ContraRecibosDetalle.rs= RS_MostrarContraRecibosDetalle>
		<cfset ContraRecibosDetalle.mensaje= 'Se obtuvo el recordset de Detalles de ContraRecibo correctamente'>
		<cfreturn ContraRecibosDetalle>
		<cfcatch type='database'>
			<cfset ContraRecibosDetalle.listado= FALSE>
			<cfset ContraRecibosDetalle.tipoError= 'database-indefinido'>
			<cfset ContraRecibosDetalle.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn ContraRecibosDetalle>
		</cfcatch>
	</cftry>
</cffunction>


<!---Funcion que devuelve el nu_LimiteCredito de un proveedor --->
<!---Hebert GB - 2009-11-04 --->
<cffunction name="getSaldoProveedor" access="remote" returntype="string">
	<cfargument name="id_Proveedor" type="string" required="yes">
	<cfargument name="tipo" type="string" required="no" default="">
	<cfif Not IsNumeric(#id_Proveedor#)>
		<cfreturn "">
	<cfelse>
		<cfquery name="RS" datasource='#cnx#' username='#user_sql#' password='#password_sql#'>
			EXECUTE ContraRecibosDetAmortizaciones_ObtenerSaldo #id_Proveedor#
		</cfquery>
		<cfset im_Saldo= RS.im_Saldo>
		<cfif #tipo# EQ "informacion">
			<cfset im_Saldo= Numberformat(im_Saldo, "$,_.____")>
		</cfif>
		<cfreturn #im_Saldo#>
	</cfif>
</cffunction>

<!--- Funcion para traer las facturas de un proveedor parametrizadas (Facturas de Carga de Movimientos)--->
<cffunction name='RSFacturasCargaMovimientos' access='public' returntype='struct'>
	<cfargument name='id_Empresa' type='numeric' required='yes'>
	<cfargument name="id_Proveedor" type="numeric" required="yes">
	<cfargument name="fh_FacturaInicial" type="string" required="yes">
	<cfargument name="fh_FacturaFinal" type="string" required="yes">
	<cfargument name="sn_ConSaldo" type="string" required="yes">
	
	<cfif #arguments.fh_FacturaInicial# NEQ "">
		<cfset fh_FacturaInicial_ParamSP= DE(#arguments.fh_FacturaInicial#)>
	<cfelse>
		<cfset fh_FacturaInicial_ParamSP= "NULL">
	</cfif>
	<cfif #arguments.fh_FacturaFinal# NEQ "">
		<cfset fh_FacturaFinal_ParamSP= DE(#arguments.fh_FacturaFinal#)>
	<cfelse>
		<cfset fh_FacturaFinal_ParamSP= "NULL">
	</cfif>

	<cftry>
		<cfquery datasource='#cnx#' name='RS_MostrarContraRecibosDetalle' username='#user_sql#' password='#password_sql#'>
			EXECUTE ContraRecibosDetalle_ObtenerFacturasDeProveedor #id_Empresa#, #id_Proveedor#, #fh_FacturaInicial_ParamSP#, #fh_FacturaFinal_ParamSP#, '#sn_ConSaldo#'
		</cfquery>
		<cfset ContraRecibosDetalle.listado= TRUE>
		<cfset ContraRecibosDetalle.tipoError= ''>
		<cfset ContraRecibosDetalle.rs= RS_MostrarContraRecibosDetalle>
		<cfset ContraRecibosDetalle.mensaje= 'Se obtuvo el recordset de Detalles de ContraRecibo correctamente'>
		<cfreturn ContraRecibosDetalle>
		<cfcatch type='database'>
			<cfset ContraRecibosDetalle.listado= FALSE>
			<cfset ContraRecibosDetalle.tipoError= 'database-indefinido'>
			<cfset ContraRecibosDetalle.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn ContraRecibosDetalle>
		</cfcatch>
	</cftry>
</cffunction>

<!--- Funcion para traer las amortizaciones de una factura--->
<cffunction name='RSAmortizaciones' access='public' returntype='struct'>
	<cfargument name='id_Empresa' type='numeric' required='yes'>
	<cfargument name="id_ContraRecibo" type="numeric" required="yes">
	<cfargument name="nd_ContraRecibo" type="string" required="yes">

	<cftry>
		<cfquery datasource='#cnx#' name='RS_MostrarAmortizaciones' username='#user_sql#' password='#password_sql#'>
			EXECUTE ContraRecibosDetalle_ObtenerAmortizaciones #id_Empresa#, #id_ContraRecibo#, #nd_ContraRecibo#
		</cfquery>
		<cfset ContraRecibosDetalle.listado= TRUE>
		<cfset ContraRecibosDetalle.tipoError= ''>
		<cfset ContraRecibosDetalle.rs= RS_MostrarAmortizaciones>
		<cfset ContraRecibosDetalle.mensaje= 'Se obtuvo el recordset de Amortizaciones de ContraRecibo Detalle correctamente'>
		<cfreturn ContraRecibosDetalle>
		<cfcatch type='database'>
			<cfset ContraRecibosDetalle.listado= FALSE>
			<cfset ContraRecibosDetalle.tipoError= 'database-indefinido'>
			<cfset ContraRecibosDetalle.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn ContraRecibosDetalle>
		</cfcatch>
	</cftry>
</cffunction>


<!--- Funcion para sacar las facturas de un contrarecibo --->
<!--- Juan Escobar 06/11/2009 --->
<cffunction name='ContrarecibosDetalle_Facturas' access='public' returntype='struct'>

	<cfargument name='id_Empresa' type='numeric' required='yes'>
	<cfargument name='id_Obra' type='string' required='no' default="">
	<cfargument name='id_CentroCostoInicial' type='string' required='no' default="">
	<cfargument name='id_CentroCostoFinal' type='string' required='no' default="">
	<cfargument name='id_ProveedorInicial' type='string' required='no' default="">
	<cfargument name='id_ProveedorFinal' type='string' required='no' default="">
	<cfargument name='id_Estatus' type='string' required='no' default="">
	<cfargument name='fh_FacturaInicial' type='string' required='no' default="">
	<cfargument name='fh_FacturaFinal' type='string' required='no' default="">
	
	<cfif id_Obra EQ "">
		<cfset id_Obra = 'null'>
	</cfif>
	<cfif id_CentroCostoInicial EQ "">
		<cfset id_CentroCostoInicial = 'null'>
	</cfif>
	<cfif id_CentroCostoFinal EQ "">
		<cfset id_CentroCostoFinal = 'null'>
	</cfif>
	<cfif id_ProveedorInicial EQ "">
		<cfset id_ProveedorInicial = 'null'>
	</cfif>
	<cfif id_ProveedorFinal EQ "">
		<cfset id_ProveedorFinal = 'null'>
	</cfif>
	<cfif id_Estatus EQ "">
		<cfset id_Estatus = 'null'>
	</cfif>
	<cfif fh_FacturaInicial EQ "">
		<cfset fh_FacturaInicial = 'null'>
	<cfelse>
		<cfset fh_FacturaInicial = DE(#fh_FacturaInicial#)>
	</cfif>
	<cfif fh_FacturaFinal EQ "">
		<cfset fh_FacturaFinal = 'null'>
	<cfelse>
		<cfset fh_FacturaFinal = DE(#fh_FacturaFinal#)>
	</cfif>
	
	<cftry>
		<cfquery datasource='#cnx#' name='RS_MostrarContraRecibos' username='#user_sql#' password='#password_sql#'>
			EXECUTE ContrarecibosDetalle_Facturas #id_Empresa#, #id_Obra#, #id_CentroCostoInicial#, 
					#id_CentroCostoFinal#, #id_ProveedorInicial#, #id_ProveedorFinal#, #id_Estatus#,
					#fh_FacturaInicial#, #fh_FacturaFinal#
		</cfquery>
		<cfset ContraRecibos.listado= TRUE>
		<cfset ContraRecibos.tipoError= ''>
		<cfset ContraRecibos.rs= RS_MostrarContraRecibos>
		<cfset ContraRecibos.mensaje= 'Se obtuvo el recordset de ContraRecibos correctamente'>
		<cfreturn ContraRecibos>
		<cfcatch type='database'>
			<cfset ContraRecibos.listado= FALSE>
			<cfset ContraRecibos.tipoError= 'database-indefinido'>
			<cfset ContraRecibos.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn ContraRecibos>
		</cfcatch>
	</cftry>
</cffunction>

<!--- Funcion para Autorizar o bloquear una factura --->
<!--- Juan Escobar 07/11/2009 --->
<cffunction name='ContraRecibosDetalle_AutorizarBloquearFactura' access='public' returntype='struct'>

	<cfargument name='id_Empresa' type='numeric' required='yes'>
	<cfargument name='id_ContraRecibo' type='numeric' required='yes'>
	<cfargument name='nd_ContraRecibo' type='numeric' required='yes'>
	<cfargument name='cl_FormaPago' type='string' required='no' default=''>
	<cfargument name='id_Estatus' type='string' required='no' default=''>
	<cfargument name='id_EmpleadoBloqueo' type='string' required='no' default=''>
	<cfargument name='id_EmpleadoAutorizo' type='string' required='no' default=''>
	
	<cfif #cl_FormaPago# NEQ ''>
		<cfset cl_FormaPago = DE(#cl_FormaPago#)>
	<cfelse>
		<cfset cl_FormaPago = 'null'>
	</cfif>
	<cfif #id_Estatus# EQ ''>
		<cfset id_Estatus = 'null'>
	</cfif>
	<cfif #id_EmpleadoBloqueo# EQ ''>
		<cfset id_EmpleadoBloqueo = 'null'>
	</cfif>
	<cfif #id_EmpleadoAutorizo# EQ ''>
		<cfset id_EmpleadoAutorizo = 'null'>
	</cfif>

	<cftry>
		<cfquery datasource='#cnx#' name='RSFilasAfectadas' username='#user_sql#' password='#password_sql#'>
			EXECUTE ContrarecibosDetalle_AutorizarBloquearFactura #id_Empresa#, #id_ContraRecibo#, #nd_ContraRecibo#, #cl_FormaPago#, #id_Estatus#, #id_EmpleadoBloqueo#, #id_EmpleadoAutorizo#
		</cfquery>
		<cfset ContraRecibosDetalle.actualizado= TRUE>
		<cfset ContraRecibosDetalle.tipoError= ''>
		<cfset ContraRecibosDetalle.mensaje= 'Detalle de ContraRecibo actualizado correctamente'>
		<cfreturn ContraRecibosDetalle>
		<cfcatch type='database'>
			<cfset ContraRecibosDetalle.actualizado= FALSE>
			<cfset ContraRecibosDetalle.tipoError= 'database-indefinido'>
			<cfset ContraRecibosDetalle.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn ContraRecibosDetalle>
		</cfcatch>
	</cftry>
</cffunction>

<!--- Funcion para sacar las facturas de un contrarecibo para la generacion de cheques --->
<!--- Hebert GB 11/11/2009 --->
<cffunction name='RS_FacturasParaGenerarCheques' access='public' returntype='struct'>
	<cfargument name='id_Empresa' type='numeric' required='yes'>
	<cfargument name='id_ProveedorInicial' type='string' required='no' default="">
	<cfargument name='id_ProveedorFinal' type='string' required='no' default="">
	<cfargument name='id_Obra' type='string' required='no' default="">
	<cfargument name='id_CentroCostoInicial' type='string' required='no' default="">
	<cfargument name='id_CentroCostoFinal' type='string' required='no' default="">
	<cfargument name='fh_FacturaInicial' type='string' required='no' default="">
	<cfargument name='fh_FacturaFinal' type='string' required='no' default="">
	<cfargument name='solo_programadas' type="string" required="no" default="no">
	
	<cfif id_ProveedorInicial EQ "">
		<cfset id_ProveedorInicial = 'NULL'>
	</cfif>
	<cfif id_ProveedorFinal EQ "">
		<cfset id_ProveedorFinal = 'NULL'>
	</cfif>
	<cfif id_Obra EQ "">
		<cfset id_Obra = 'NULL'>
	</cfif>
	<cfif id_CentroCostoInicial EQ "">
		<cfset id_CentroCostoInicial = 'NULL'>
	</cfif>
	<cfif id_CentroCostoFinal EQ "">
		<cfset id_CentroCostoFinal = 'NULL'>
	</cfif>
	<cfif fh_FacturaInicial EQ "">
		<cfset fh_FacturaInicial = 'NULL'>
	<cfelse>
		<cfset fh_FacturaInicial = DE(#fh_FacturaInicial#)>
	</cfif>
	<cfif fh_FacturaFinal EQ "">
		<cfset fh_FacturaFinal = 'NULL'>
	<cfelse>
		<cfset fh_FacturaFinal = DE(#fh_FacturaFinal#)>
	</cfif>
	
	<cftry>
		<cfquery datasource='#cnx#' name='RS_MostrarContraRecibos' username='#user_sql#' password='#password_sql#'>
			EXECUTE ContrarecibosDetalle_FacturasParaGenerarCheques #id_Empresa#, #id_ProveedorInicial#, #id_ProveedorFinal#, #id_Obra#, #id_CentroCostoInicial#, #id_CentroCostoFinal#, #fh_FacturaInicial#, #fh_FacturaFinal#, '#solo_programadas#'
		</cfquery>
		<cfset ContraRecibos.listado= TRUE>
		<cfset ContraRecibos.tipoError= ''>
		<cfset ContraRecibos.rs= RS_MostrarContraRecibos>
		<cfset ContraRecibos.mensaje= 'Se obtuvo el recordset de ContraRecibos correctamente'>
		<cfreturn ContraRecibos>
		<cfcatch type='database'>
			<cfset ContraRecibos.listado= FALSE>
			<cfset ContraRecibos.tipoError= 'database-indefinido'>
			<cfset ContraRecibos.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn ContraRecibos>
		</cfcatch>
	</cftry>
</cffunction>

<!--- Funcion para sacar las facturas de un contrarecibo con saldo--->
<!--- Florencia AA 28/10/2010 --->
<cffunction name='RS_ChequeParaGenerarFactura' access='public' returntype='struct'>
	<cfargument name='id_Empresa' type='numeric' required='yes'>
	<cfargument name='id_CuentaBancaria' type='numeric' required='yes'>
	<cfargument name='id_Cheque' type='numeric' required='yes'>
	<cfargument name='fh_FacturaInicial' type='string' required='no' default="">
	<cfargument name='fh_FacturaFinal' type='string' required='no' default="">
	
	
	<cfif fh_FacturaInicial EQ "">
		<cfset fh_FacturaInicial = 'NULL'>
	<cfelse>
		<cfset fh_FacturaInicial = DE(#fh_FacturaInicial#)>
	</cfif>
	<cfif fh_FacturaFinal EQ "">
		<cfset fh_FacturaFinal = 'NULL'>
	<cfelse>
		<cfset fh_FacturaFinal = DE(#fh_FacturaFinal#)>
	</cfif>
	
	<cftry>
		<cfquery datasource='#cnx#' name='RS_MostrarContraRecibos' username='#user_sql#' password='#password_sql#'>
			EXECUTE Cheques_TraerFacturasConSaldo #id_Empresa#, #id_CuentaBancaria#, #id_Cheque#, #fh_FacturaInicial#, #fh_FacturaFinal#
		</cfquery>
		<cfset ContraRecibos.listado= TRUE>
		<cfset ContraRecibos.tipoError= ''>
		<cfset ContraRecibos.rs= RS_MostrarContraRecibos>
		<cfset ContraRecibos.mensaje= 'Se obtuvo el recordset de ContraRecibos correctamente'>
		<cfreturn ContraRecibos>
		<cfcatch type='database'>
			<cfset ContraRecibos.listado= FALSE>
			<cfset ContraRecibos.tipoError= 'database-indefinido'>
			<cfset ContraRecibos.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn ContraRecibos>
		</cfcatch>
	</cftry>
</cffunction>


<!--- Funcion para sacar las facturas de un contrarecibo para la pantalla de Programación de Pagos --->
<!--- Juan Escobar 11/11/2009 --->
<cffunction name='ContrarecibosDetalle_FacturasProgramacionPagos' access='public' returntype='struct'>

	<cfargument name='id_Empresa' type='numeric' required='yes'>
	<cfargument name='id_Obra' type='string' required='no' default="">
	<cfargument name='id_CentroCostoInicial' type='string' required='no' default="">
	<cfargument name='id_CentroCostoFinal' type='string' required='no' default="">
	<cfargument name='id_ProveedorInicial' type='string' required='no' default="">
	<cfargument name='id_ProveedorFinal' type='string' required='no' default="">
	<cfargument name='fh_FacturaInicial' type='string' required='no' default="">
	<cfargument name='fh_FacturaFinal' type='string' required='no' default="">
	
	<cfif id_Obra EQ "">
		<cfset id_Obra = 'null'>
	</cfif>
	<cfif id_CentroCostoInicial EQ "">
		<cfset id_CentroCostoInicial = 'null'>
	</cfif>
	<cfif id_CentroCostoFinal EQ "">
		<cfset id_CentroCostoFinal = 'null'>
	</cfif>
	<cfif id_ProveedorInicial EQ "">
		<cfset id_ProveedorInicial = 'null'>
	</cfif>
	<cfif id_ProveedorFinal EQ "">
		<cfset id_ProveedorFinal = 'null'>
	</cfif>
	<cfif fh_FacturaInicial EQ "">
		<cfset fh_FacturaInicial = 'null'>
	<cfelse>
		<cfset fh_FacturaInicial = DE(#fh_FacturaInicial#)>
	</cfif>
	<cfif fh_FacturaFinal EQ "">
		<cfset fh_FacturaFinal = 'null'>
	<cfelse>
		<cfset fh_FacturaFinal = DE(#fh_FacturaFinal#)>
	</cfif>
	
	<cftry>
		<cfquery datasource='#cnx#' name='RS_MostrarContraRecibos' username='#user_sql#' password='#password_sql#'>
			EXECUTE ContrarecibosDetalle_FacturasProgramacionPagos #id_Empresa#, #id_ProveedorInicial#, #id_ProveedorFinal#, 
					#id_Obra#, #id_CentroCostoInicial#, #id_CentroCostoFinal#, #fh_FacturaInicial#, #fh_FacturaFinal#
		</cfquery>
		<cfset ContraRecibos.listado= TRUE>
		<cfset ContraRecibos.tipoError= ''>
		<cfset ContraRecibos.rs= RS_MostrarContraRecibos>
		<cfset ContraRecibos.mensaje= 'Se obtuvo el recordset de ContraRecibos correctamente'>
		<cfreturn ContraRecibos>
		<cfcatch type='database'>
			<cfset ContraRecibos.listado= FALSE>
			<cfset ContraRecibos.tipoError= 'database-indefinido'>
			<cfset ContraRecibos.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn ContraRecibos>
		</cfcatch>
	</cftry>
</cffunction>


<!--- Funcion para sacar las facturas para busquedas de pop --->
<!--- Hebert GB 13/11/2009 --->
<cffunction name='RS_MostrarFacturasParaBusqueda' access='public' returntype='struct'>
	<cfargument name='id_Empresa' type='numeric' required='yes'>
	<cfargument name='id_Proveedor' type='numeric' required='yes'>
	<cfargument name='de_Factura' type='string' required='no' default="">
	<cfargument name='fh_Factura' type='string' required='no' default="">
	<cfargument name='fh_FacturaVencimiento' type='string' required='no' default="">
	<cfargument name='id_Estatus' type="string" required="no" default="">
	
	<cfif de_Factura EQ "">
		<cfset de_Factura = "NULL">
	<cfelse>
		<cfset de_Factura = DE(#de_Factura#)>
	</cfif>
	<cfif fh_Factura EQ "">
		<cfset fh_Factura = "NULL">
	<cfelse>
		<cfset fh_Factura = DE(#fh_Factura#)>
	</cfif>
	<cfif fh_FacturaVencimiento EQ "">
		<cfset fh_FacturaVencimiento = "NULL">
	<cfelse>
		<cfset fh_FacturaVencimiento = DE(#fh_FacturaVencimiento#)>
	</cfif>
	<cfif id_Estatus EQ "">
		<cfset id_Estatus= "NULL">
	</cfif>
	
	<cftry>
		<cfquery datasource='#cnx#' name='RS_MostrarContraRecibos' username='#user_sql#' password='#password_sql#'>
			EXECUTE ContraRecibosDetalle_FacturasParaBusquedas #id_Empresa#, #id_Proveedor#, #de_Factura#, #fh_Factura#, #fh_FacturaVencimiento#, #id_Estatus#
		</cfquery>
		<cfset ContraRecibos.listado= TRUE>
		<cfset ContraRecibos.tipoError= ''>
		<cfset ContraRecibos.rs= RS_MostrarContraRecibos>
		<cfset ContraRecibos.mensaje= 'Se obtuvo el recordset de facturas correctamente'>
		<cfreturn ContraRecibos>
		<cfcatch type='database'>
			<cfset ContraRecibos.listado= FALSE>
			<cfset ContraRecibos.tipoError= 'database-indefinido'>
			<cfset ContraRecibos.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn ContraRecibos>
		</cfcatch>
	</cftry>
</cffunction>

<cffunction name='MostrarDinamicoContraRecibosDetalle' access='public' returntype='struct'>

	<cfargument name='id_Empresa' type='numeric' required='no' default='-1'>
	<cfargument name='id_Proveedor' type='numeric' required='no' default='-1'>
	<cfargument name='de_Factura' type='string' required='no' default=''>


	<cftry>
		<cfset WhereDinamico=''>
		<cfif #id_Empresa# NEQ '-1'>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE id_Empresa = #id_Empresa#">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND id_Empresa = #id_Empresa#">
			</cfif>
		</cfif>
		
		<cfif #id_Proveedor# NEQ '-1'>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE id_Proveedor = #id_Proveedor#">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND id_Proveedor = #id_Proveedor#">
			</cfif>
		</cfif>
		
		
		<cfif #de_Factura# NEQ ''>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE de_Factura LIKE '#de_Factura#%'">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND de_Factura LIKE '#de_Factura#'">
			</cfif>
		</cfif>
		
	
		<cfquery datasource='#cnx#' name='RS_MostrarContraRecibosDetalle' username='#user_sql#' password='#password_sql#'>
			EXECUTE bop_ContraRecibosDetalle_ObtenerDinamico '#WhereDinamico#'
		</cfquery>
		<cfset ContraRecibosDetalle.listado= TRUE>
		<cfset ContraRecibosDetalle.tipoError= ''>
		<cfset ContraRecibosDetalle.rs= RS_MostrarContraRecibosDetalle>
		<cfset ContraRecibosDetalle.mensaje= 'Se obtuvo el recordset de Detalles de Contrarecibo correctamente'>
		<cfreturn ContraRecibosDetalle>
		<cfcatch type='database'>
			<cfset ContraRecibosDetalle.listado= FALSE>
			<cfset ContraRecibosDetalle.tipoError= 'database-indefinido'>
			<cfset ContraRecibosDetalle.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn ContraRecibosDetalle>
		</cfcatch>
	</cftry>
</cffunction>

<!--- Funcion para traer el listado de Notas de Credito --->
<!--- Florencia Aragon 30/08/2010 --->
<cffunction name='ContrarecibosDetalle_ListadoNotasCredito' access='public' returntype='struct'>

	<cfargument name='id_Empresa' type='numeric' required='yes'>
	<cfargument name='id_Proveedor' type='string' required='no' default="">
	<cfargument name='de_Factura' type='string' required='no' default="">
	<cfif id_Proveedor EQ ''>
		<CFSET id_Proveedor = 'null'>
	</CFIF>
	<cfif de_Factura EQ ''>
		<CFSET de_Factura = 'null'>
	</CFIF>
	<cftry>
		<cfquery datasource='#cnx#' name='RS_MostrarContraRecibos' username='#user_sql#' password='#password_sql#'>
			EXECUTE NotasCreditoProveedores_Listado #id_Empresa#, #de_Factura#, #id_Proveedor#
		</cfquery>
		<cfset ContraRecibos.listado= TRUE>
		<cfset ContraRecibos.tipoError= ''>
		<cfset ContraRecibos.rs= RS_MostrarContraRecibos>
		<cfset ContraRecibos.mensaje= 'Se obtuvo el recordset de ContraRecibos correctamente'>
		<cfreturn ContraRecibos>
		<cfcatch type='database'>
			<cfset ContraRecibos.listado= FALSE>
			<cfset ContraRecibos.tipoError= 'database-indefinido'>
			<cfset ContraRecibos.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn ContraRecibos>
		</cfcatch>
	</cftry>
</cffunction>

<!--- Funcion para sacar las facturas de un contrarecibo para la pantalla de Notas de Crédito --->
<!--- Florencia Aragon 30/08/2010 --->
<cffunction name='ContrarecibosDetalle_ListadoFacturas_NotaCredito' access='public' returntype='struct'>

	<cfargument name='id_Empresa' type='numeric' required='yes'>
	<cfargument name='id_ContraRecibo' type='string' required='yes'>


	<cftry>
		<cfquery datasource='#cnx#' name='RS_MostrarContraRecibos' username='#user_sql#' password='#password_sql#'>
			EXECUTE ContraRecibosDetalle_ListadoFacturas_NotasCredito #id_Empresa#, #id_ContraRecibo#
		</cfquery>
		<cfset ContraRecibos.listado= TRUE>
		<cfset ContraRecibos.tipoError= ''>
		<cfset ContraRecibos.rs= RS_MostrarContraRecibos>
		<cfset ContraRecibos.mensaje= 'Se obtuvo el recordset de ContraRecibos correctamente'>
		<cfreturn ContraRecibos>
		<cfcatch type='database'>
			<cfset ContraRecibos.listado= FALSE>
			<cfset ContraRecibos.tipoError= 'database-indefinido'>
			<cfset ContraRecibos.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn ContraRecibos>
		</cfcatch>
	</cftry>
</cffunction>

<!--- Florencia Aragón 05 feb 2010--->
<!--- Funcion para traer informacion de contrarecibos para bind--->
<cffunction name="getConsultaContraReciboDetBindeo" access="remote" returntype="string">
	<cfargument name="id_Empresa" required="yes">
	<cfargument name="id_Proveedor" required="yes">
	<cfargument name="de_Factura" required="yes">
	<cfargument name="tipo" type="string">
		<cfif id_Proveedor EQ ''>
			<cfreturn "">
		</cfif>
		<cfif de_Factura EQ ''>
			<cfreturn "">
		<cfelse>
			<cfset de_Factura = DE(#de_Factura#)>
		</cfif>
	<cftry>
		<cfquery datasource='#cnx#' name='RS_Mostrar' username='#user_sql#' password='#password_sql#'>
			EXECUTE ContraRecibosDetalle_TraerDatos #id_Empresa#, #id_Proveedor#,#de_Factura#
		</cfquery>
		<cfif #RS_Mostrar.recordcount# GT 0>			
			<cfif tipo EQ 'fh_Factura'>
				<cfreturn (MID(RS_Mostrar.fh_Factura,9,2) & '/' & MID(RS_Mostrar.fh_Factura,6,2) & '/' & Left(RS_Mostrar.fh_Factura,4))>
			<cfelseif tipo EQ 'im_Factura'>
				<cfreturn NumberFormat(RS_Mostrar.im_Factura,"$,_.____")>
			<cfelseif tipo EQ 'id_Moneda'>
				<cfreturn RS_Mostrar.id_Moneda>
			<cfelseif tipo EQ 'im_TipoCambio'>
				<cfreturn RS_Mostrar.im_TipoCambio>
			<cfelseif tipo EQ 'im_Saldo'>
				<cfreturn NumberFormat(RS_Mostrar.im_Saldo,"$,_.____")>
			</cfif>
		<cfelse>
			<cfreturn "">
		</cfif>
		<cfcatch type='database'>
			<cfreturn "">
		</cfcatch>
	</cftry>
</cffunction>

<!--- Funcion para sacar las facturas de un contrarecibo para la pantalla de Notas de Crédito --->
<!--- Florencia Aragon 30/08/2010 --->
<cffunction name='ContrarecibosDetalle_CancelarContraReciboDetalle' access='public' returntype='struct'>

	<cfargument name='id_Empresa' type='numeric' required='yes'>
	<cfargument name='id_ContraRecibo' type='numeric' required='yes'>
	<cfargument name='nd_ContraRecibo' type='numeric' required='yes'>
	<cfargument name='fh_Poliza' type='string' required='yes'>

	<cfset fh_Poliza = (Right(fh_Poliza,4) & '/' & Mid(fh_Poliza,4,2) & '/' & Left(fh_Poliza,2))>
	<cfset fh_Poliza = DE(#fh_Poliza#)>
	<cftry>
		
		<cfset ErrorValidacionesNegocio= "">
		<!---Requerimiento 1: Validar que el contrarecibo no tenga cheques autorizados asociados --->
		<cfquery name="RS_ChequesContraRecibos" datasource="#cnx#" username="#user_sql#" password="#password_sql#">
			EXECUTE ContraRecibosDetalles_ChequesAutorizados #id_Empresa#, #id_ContraRecibo#, #nd_ContraRecibo#
		</cfquery>
		
		
		<cfif #RS_ChequesContraRecibos.RecordCount# GT 0>
			<cfset ErrorValidacionesNegocio= ErrorValidacionesNegocio & "Existen cheques con póliza utilizados para el pago de esta factura.">
		</cfif>
		
		<cfif #ErrorValidacionesNegocio# NEQ "">
			<cfset ContraRecibosDetalle.agregado= FALSE>
			<cfset ContraRecibosDetalle.tipoError= "validaciones">
			<cfset ContraRecibosDetalle.mensaje= #ErrorValidacionesNegocio#>
			<cfreturn ContraRecibosDetalle>
		</cfif>	
		
		<cftransaction action="begin"/>
			<!---Cancelar ContraRecibos y Amortizaciones --->
			<cfquery name="RS_CancelarFactura" datasource="#cnx#" username="#user_sql#" password="#password_sql#">
				EXECUTE ContraRecibosDetalles_CancelarContraRecibo #id_Empresa#, #id_ContraRecibo#, #nd_ContraRecibo#
			</cfquery>
			<!---Realizar ContraPolizas --->
			<cfquery datasource='#cnx#' name='RS_MostrarContraRecibos' username='#user_sql#' password='#password_sql#'>
				EXECUTE Contabilidad_GenerarPolizaCancelacionContraRecibosDet #id_Empresa#, #id_ContraRecibo#, #nd_ContraRecibo#, #fh_Poliza#
			</cfquery>
		<cftransaction action="commit"/>
		
		<cfset ContraRecibosDetalle.agregado= TRUE>
		<cfset ContraRecibosDetalle.tipoError= ''>
		<cfset ContraRecibosDetalle.mensaje= 'Factura cancelada exitosamente'>
		<cfreturn ContraRecibosDetalle>
		<cfcatch type='database'>
			<cftransaction action="rollback"/>
			<cfset ContraRecibosDetalle.agregado= FALSE>
			<cfset ContraRecibosDetalle.tipoError= 'database-indefinido'>
			<cfset ContraRecibosDetalle.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)EXECUTE Contabilidad_GenerarPolizaCancelacionContraRecibosDet #id_Empresa#, #id_ContraRecibo#, #nd_ContraRecibo#, #fh_Poliza#'>
			<cfreturn ContraRecibosDetalle>
		</cfcatch>
	</cftry>
</cffunction>


<!--- Autor: Kaleb Ontiveros --->
<!--- Fecha: 19/JUNIO/2014   --->
<!--- Descripción: Rediseño de la pantalla de tramitar facturas --->
<cffunction name='ContraRecibosDetalleTramitar_ListadoFacturas' access='public' returntype='struct'>
	<cfargument name='id_Empresa'       type='any' required='no' default="">
	<cfargument name='id_Obra'          type='any' required='no' default="">
	<cfargument name='id_Empleado'      type='any' required='no' default="">
	<cfargument name='de_Factura'       type='any' required='no' default="">
	<cfargument name='fh_FacturaInicial'type='any' required='no' default="">
	<cfargument name='fh_FacturaFinal'  type='any' required='no' default="">
	<cfargument name='id_Proveedor'     type='any' required='no' default="">
	<cfargument name='fh_Recepcion'     type='any' required='no' default="">
	<cfargument name='fh_Corte'         type='any' required='no' default="">
	<cfargument name='id_EstatusFactura'type='any' required='no' default="">
	<cfargument name='fh_Pago'          type='any' required='no' default="">
	<cfargument name='nu_Semana'        type='any' required='no' default="">
    <cfargument name='fh_Pago_vacio'    type='any' required='no' default="">
	<cfargument name='estatus'          type='any' required='no' default="">
    <cfargument name='page'             type='numeric' required='no' default="1">
	<cfargument name='pageSize'         type='numeric' required='no' default="20">

    <cftry>
        <cfstoredproc procedure="ContraRecibosDetalleTramitar_ListadoFacturas" datasource="#cnx#">
            <cfprocparam type="IN" cfsqltype="CF_SQL_INTEGER" dbvarname="@id_Empresa" value="#arguments.id_Empresa#" null="#iif(isNumeric(arguments.id_Empresa),false,true)#">
            <cfprocparam type="IN" cfsqltype="CF_SQL_INTEGER" dbvarname="@id_Obra" value="#arguments.id_Obra#">
            <cfprocparam type="IN" cfsqltype="CF_SQL_INTEGER" dbvarname="@id_Empleado" value="#arguments.id_Empleado#" null="#iif(isNumeric(arguments.id_Empleado),false,true)#">
            <cfprocparam type="IN" cfsqltype="CF_SQL_VARCHAR" dbvarname="@de_Factura" value="#arguments.de_Factura#" null="#iif(len(arguments.de_Factura),false,true)#">
            <cfprocparam type="IN" cfsqltype="CF_SQL_TIMESTAMP" dbvarname="@fh_FacturaInicial" value="#arguments.fh_FacturaInicial#" null="#iif(isDate(arguments.fh_FacturaInicial),false,true)#">
            <cfprocparam type="IN" cfsqltype="CF_SQL_TIMESTAMP" dbvarname="@fh_FacturaFinal" value="#arguments.fh_FacturaFinal#" null="#iif(isDate(arguments.fh_FacturaFinal),false,true)#">
            <cfprocparam type="IN" cfsqltype="CF_SQL_INTEGER" dbvarname="@id_Proveedor" value="#arguments.id_Proveedor#" null="#iif(isNumeric(arguments.id_Proveedor),false,true)#">
            <cfprocparam type="IN" cfsqltype="CF_SQL_TIMESTAMP" dbvarname="@fh_Recepcion" value="#arguments.fh_Recepcion#" null="#iif(isDate(arguments.fh_Recepcion),false,true)#">
            <cfprocparam type="IN" cfsqltype="CF_SQL_TIMESTAMP" dbvarname="@fh_Corte" value="#arguments.fh_Corte#" null="#iif(isDate(arguments.fh_Corte),false,true)#">
            <cfprocparam type="IN" cfsqltype="CF_SQL_TIMESTAMP" dbvarname="@fh_Pago" value="#arguments.fh_Pago#" null="#iif(isDate(arguments.fh_Pago),false,true)#">
            <cfprocparam type="IN" cfsqltype="CF_SQL_INTEGER" dbvarname="@id_EstatusFactura" value="#arguments.id_EstatusFactura#" null="#iif(isNumeric(arguments.id_EstatusFactura),false,true)#">
            <cfprocparam type="IN" cfsqltype="CF_SQL_INTEGER" dbvarname="@nu_Semana" value="#arguments.nu_Semana#" null="#iif(isNumeric(arguments.nu_Semana),false,true)#">
            <cfprocparam type="IN" cfsqltype="CF_SQL_INTEGER" dbvarname="@fh_pago_vacio" value="#arguments.fh_pago_vacio#" null="#iif(isNumeric(arguments.fh_pago_vacio),false,true)#">
            <cfprocparam type="IN" cfsqltype="CF_SQL_INTEGER" dbvarname="@page" value="#arguments.page#" null="#iif(isNumeric(arguments.page),false,true)#">
            <cfprocparam type="IN" cfsqltype="CF_SQL_INTEGER" dbvarname="@pageSize" value="#arguments.pageSize#" null="#iif(isNumeric(arguments.pageSize),false,true)#">
            <cfprocresult name="RS_MostrarContraRecibos" resultset="1">
        </cfstoredproc>

        <cfset ContraRecibos.listado= TRUE>
        <cfset ContraRecibos.tipoError= ''>
        <cfset ContraRecibos.rs= RS_MostrarContraRecibos>
        <cfset ContraRecibos.mensaje= 'Se obtuvo el recordset de ContraRecibos correctamente'>
        <cfreturn ContraRecibos>
        
        <cfcatch type='any'>
            <cfset ContraRecibos.listado= FALSE>
            <cfset ContraRecibos.tipoError= 'database-indefinido'>
            <cfset ContraRecibos.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
            <cfreturn ContraRecibos>
        </cfcatch>
    </cftry>
    
</cffunction>


<!--- Funcion para sacar el Listado de Facturas por Encargado de Estimaciones --->
<!--- Juan Escobar 15/10/2012 --->
<cffunction name='ContraRecibosDetalle_ListadoFacturas' access='public' returntype='struct'>
	<cfargument name='id_Empresa' type='string' required='no' default="NULL">
	<cfargument name='id_Obra' type='string' required='no' default="">
	<cfargument name='id_Empleado' type='string' required='no' default="">
	<cfargument name='de_Factura' type='string' required='no' default="">
	<cfargument name='fh_FacturaInicial' type='string' required='no' default="">
	<cfargument name='fh_FacturaFinal' type='string' required='no' default="">
	<cfargument name='id_Proveedor' type='string' required='no' default="">
	<cfargument name='fh_Recepcion' type='string' required='no' default="">
	<cfargument name='fh_Corte' type='string' required='no' default="">
	<cfargument name='id_EstatusFactura' type='string' required='no' default="">
	<cfargument name='fh_Pago' type='string' required='no' default="">
	<cfargument name='nu_Semana' type='string' required='no' default="">
    <cfargument name='fh_Pago_vacio' type='string' required='no' default="">
	<cfargument name='estatus' type='string' required='no' default="">
	
	<cfif id_Empresa EQ "">
		<cfset id_Empresa = 'null'>
	</cfif>
	
	<cfif id_Empleado EQ "">
		<cfset id_Empleado = 'null'>
	</cfif>
	
	<cfif id_Obra EQ "">
		<cfset id_Obra = 'null'>
	</cfif>
	
	<cfif nu_Semana EQ "">
		<cfset nu_Semana = 'null'>
	</cfif>
	
	<cfif id_Proveedor EQ "">
		<cfset id_Proveedor = 'null'>
	</cfif>
	
	<cfif id_EstatusFactura EQ "">
		<cfset id_EstatusFactura = 'null'>
	</cfif>
	
	<cfif de_Factura EQ "">
		<cfset de_Factura = 'null'>
	<cfelse>
		<cfset de_Factura = DE(#trim(de_Factura)#)>
	</cfif>
	
	<cfif fh_FacturaInicial EQ "">
		<cfset fh_FacturaInicial = 'null'>
	<cfelse>
		<cfset fh_FacturaInicial = (Right(fh_FacturaInicial,4) & '/' & Mid(fh_FacturaInicial,4,2) & '/' & Left(fh_FacturaInicial,2))>
		<cfset fh_FacturaInicial = DE(#fh_FacturaInicial#)>
	</cfif>
	
	<cfif fh_FacturaFinal EQ "">
		<cfset fh_FacturaFinal = 'null'>
	<cfelse>
		<cfset fh_FacturaFinal = (Right(fh_FacturaFinal,4) & '/' & Mid(fh_FacturaFinal,4,2) & '/' & Left(fh_FacturaFinal,2))>
		<cfset fh_FacturaFinal = DE(#fh_FacturaFinal#)>
	</cfif>
	
	<cfif fh_Recepcion EQ "">
		<cfset fh_Recepcion = 'null'>
	<cfelse>
		<cfset fh_Recepcion = (Right(fh_Recepcion,4) & '/' & Mid(fh_Recepcion,4,2) & '/' & Left(fh_Recepcion,2))>
		<cfset fh_Recepcion = DE(#fh_Recepcion#)>
	</cfif>
	
	<cfif fh_Corte EQ "">
		<cfset fh_Corte = 'null'>
	<cfelse>
		<cfset fh_Corte = (Right(fh_Corte,4) & '/' & Mid(fh_Corte,4,2) & '/' & Left(fh_Corte,2))>
		<cfset fh_Corte = DE(#fh_Corte#)>
	</cfif>
	
	<cfif fh_Pago NEQ "" AND isDate(fh_Pago)>
		<cfset fh_Pago = (Right(fh_Pago,4) & '/' & Mid(fh_Pago,4,2) & '/' & Left(fh_Pago,2))>
		<cfset fh_Pago = DE(#fh_Pago#)>
	<cfelse>		
		<cfset fh_Pago = 'null'>
	</cfif>	
    
    <cfif fh_Pago_vacio EQ ''>
		<cfset fh_Pago_vacio ='null'>
	</cfif>
<!---    <cfdump var="#fh_Pago_vacio#">
    <cfabort>--->
	<cftry>
		<cfquery datasource='#cnx#' name='RS_MostrarContraRecibos' username='#user_sql#' password='#password_sql#'>
			EXECUTE ContraRecibosDetalle_ListadoFacturas #id_Empresa#, #id_Obra#, #id_Empleado#, 
														#de_Factura#, #fh_FacturaInicial#, #fh_FacturaFinal#, 
														#id_Proveedor#, #fh_Recepcion#, #fh_Corte#, 
														#fh_Pago#, #id_EstatusFactura#, #nu_Semana#, #fh_Pago_vacio#
		</cfquery>
		
		<cfif estatus NEQ ''>
			<cfset where = 'where id_EstatusFactura in (' & #estatus# &')'>
            <cfquery name="RS_MostrarContraRecibos" dbtype="query">
                select * from RS_MostrarContraRecibos
                #where#
            </cfquery> 
        </cfif>
		
		<!--- <cf_log datasource='#cnx#' username='#user_sql#' password='#password_sql#' id_Empresa="#Session.id_Empresa#" id_Obra="#id_Obra#" id_Usuario="#Session.id_Usuario#" id_MenuOpcion="#Session.id_MenuOpcion#">
			<cfoutput>
                EXECUTE ContraRecibosDetalle_ListadoFacturas #id_Empresa#, #id_Obra#, #id_Empleado#, 
                                                            #de_Factura#, #fh_FacturaInicial#, #fh_FacturaFinal#, 
                                                            #id_Proveedor#, #fh_Recepcion#, #fh_Corte#, 
                                                            #fh_Pago#, #id_EstatusFactura#, #nu_Semana#, #fh_Pago_vacio#
			</cfoutput>
		</cf_log> --->        
		<cfset ContraRecibos.listado= TRUE>
		<cfset ContraRecibos.tipoError= ''>
		<cfset ContraRecibos.rs= RS_MostrarContraRecibos>
		<cfset ContraRecibos.mensaje= 'Se obtuvo el recordset de ContraRecibos correctamente'>
		<cfreturn ContraRecibos>
		<cfcatch type='database'>
			<cfset ContraRecibos.listado= FALSE>
			<cfset ContraRecibos.tipoError= 'database-indefinido'>
			<cfset ContraRecibos.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn ContraRecibos>
		</cfcatch>
	</cftry>
</cffunction>

<!--- Funcion para sacar el Listado de Facturas por Encargado de Estimaciones --->
<!--- Juan Escobar 15/10/2012 --->
<cffunction name='ContraRecibosDetalle_ActualizarFactura' access='public' returntype='struct'>

	<cfargument name='id_EmpresaOriginal' type='numeric' required='yes'>
	<cfargument name='id_EmpresaNueva' type='numeric' required='yes'>
	<cfargument name='id_ContraRecibo' type='numeric' required='no' default="">
	<cfargument name='nd_ContraRecibo' type='numeric' required='no' default="">
	<cfargument name='id_EstatusFactura' type='string' required='no' default="">
	<cfargument name='de_Observaciones' type='string' required='no' default="">
	<cfargument name='fh_Pago' type='string' required='no' default="null">
	<cfargument name='cl_TipoFactura' type='string' required='no' default="">
	<cfargument name='sn_CopiaFactura' type='string' required='no' default="null">
	<cfargument name='im_SubTotal' type='numeric' required='no' default="null">    
	
	<cfif fh_Pago EQ "" OR fh_Pago EQ 'null'>
		<cfset fh_Pago = 'null'>
	<cfelse>
		<cfset fh_Pago = (Right(fh_Pago,4) & '/' & Mid(fh_Pago,4,2) & '/' & Left(fh_Pago,2))>
		<cfset fh_Pago = DE(#fh_Pago#)>
	</cfif>
	
	<cftry>  
		<cfquery name="RsFactura" datasource="#cnx#" username='#user_sql#' password='#password_sql#'>
			SELECT id_Empresa,id_Obra,id_OrdenCompra FROM Contrarecibosdetalle WHERE id_Empresa = #id_EmpresaOriginal# AND id_Contrarecibo = #id_Contrarecibo#
		</cfquery>
		<cfif #RsFactura.id_OrdenCompra# NEQ '' and id_EstatusFactura EQ '512'>
			
			<cfquery name="RsEntradasOC" datasource="#cnx#" username='#user_sql#' password='#password_sql#'>
	            EXECUTE Movimientos #RsFactura.id_Empresa#, #RsFactura.id_Obra#, #RsFactura.id_OrdenCompra#, #id_Contrarecibo#
	        </cfquery>

	        <cfquery name="RsEntradaSalida" datasource="#cnx#" username='#user_sql#' password='#password_sql#'>
	            SELECT 1 as id FROM InventariosMovimientos WHERE id_Empresa = #RsFactura.id_Empresa# AND id_OrdenCompra = #RsFactura.id_OrdenCompra# AND id_Obra = #RsFactura.id_Obra# AND id_TipoMovimiento = 7 AND id_Estatus <> 402
	        </cfquery>
	 
	        <cfif #RsEntradasOC.Numero_Movimientos# NEQ 0 OR #RsEntradaSalida.id# EQ 1>
	        	<cfset ContraRecibos.listado= FALSE>
				<cfset ContraRecibos.tipoError= 'database-indefinido'>
				<cfset ContraRecibos.mensaje= 'No se puede cancelar la factura porque tiene una entrada a inventario.'>
				<cfreturn ContraRecibos>
	        </cfif>

		</cfif>

		<cfquery datasource='#cnx#' name='RS_MostrarContraRecibos' username='#user_sql#' password='#password_sql#'>
			EXECUTE ContraRecibosDetalle_ActualizarFactura #id_EmpresaOriginal#,#id_EmpresaNueva#, #id_ContraRecibo#, #nd_ContraRecibo#, #id_EstatusFactura#,
					#fh_Pago#, '#de_Observaciones#', '#cl_TipoFactura#', #sn_CopiaFactura#, #im_SubTotal#
		</cfquery>
		<!---Log
		<cf_log datasource='#cnx#' username='#user_sql#' password='#password_sql#' id_Empresa="#Session.id_Empresa#" id_Obra="#id_Obra#" id_Usuario="#Session.id_Usuario#" id_MenuOpcion="#Session.id_MenuOpcion#">
			<cfoutput>
				EXECUTE ContraRecibosDetalle_ActualizarFactura #id_EmpresaOriginal#,#id_EmpresaNueva#, #id_ContraRecibo#, #nd_ContraRecibo#, #id_EstatusFactura#,#fh_Pago#, '#de_Observaciones#', '#cl_TipoFactura#', #sn_CopiaFactura#, #im_SubTotal#
			</cfoutput>
		</cf_log>
		<!---Termina Log --->--->
		<cfset ContraRecibos.listado= TRUE>
		<cfset ContraRecibos.tipoError= ''>
		<cfset ContraRecibos.mensaje= 'Se obtuvo el recordset de ContraRecibos correctamente'>
		<cfreturn ContraRecibos>
		<cfcatch type='database'>
			<cfset ContraRecibos.listado= FALSE>
			<cfset ContraRecibos.tipoError= 'database-indefinido'>
			<cfset ContraRecibos.mensaje= 'Error en el acceso a base de datos al Actualizar la Factura(#cfcatch.Detail#)'>
			<cfreturn ContraRecibos>
		</cfcatch>
	</cftry>
</cffunction>

<!--- actualizar factura para tramitar a pago facturas tipo materiales --->
<cffunction name='ContraRecibosDetalle_ActualizarFacturaTP' access='public' returntype='struct'>

	<cfargument name='id_EmpresaOriginal' type='numeric' required='yes'>
	<cfargument name='id_EmpresaNueva' type='numeric' required='yes'>
	<cfargument name='id_ContraRecibo' type='numeric' required='no' default="">
	<cfargument name='nd_ContraRecibo' type='numeric' required='no' default="">
	<cfargument name='id_EstatusFactura' type='string' required='no' default="">
	<cfargument name='de_Observaciones' type='string' required='no' default="">
	<cfargument name='fh_Pago' type='string' required='no' default="null">
	<cfargument name='cl_TipoFactura' type='string' required='no' default="">
	<cfargument name='sn_CopiaFactura' type='string' required='no' default="null">
	<cfargument name='im_SubTotal' type='numeric' required='no' default="null">
    <cfargument name='id_Moneda' type='string' required='no' default="null">
    <cfargument name="imp_SubTotalFactura" type="string" required="no" default="null">    
    <cfargument name="idu_MonedaFactura" type="string" required="no" default="null">    
	
	<cfif fh_Pago EQ "" OR fh_Pago EQ 'null'>
		<cfset fh_Pago = 'null'>
	<cfelse>
		<cfset fh_Pago = (Right(fh_Pago,4) & '/' & Mid(fh_Pago,4,2) & '/' & Left(fh_Pago,2))>
		<cfset fh_Pago = DE(#fh_Pago#)>
	</cfif>
	
	<cftry>    	
		<cfquery datasource='#cnx#' name='RS_MostrarContraRecibos' username='#user_sql#' password='#password_sql#'>
			EXECUTE ContraRecibosDetalle_ActualizarFacturaTP #id_EmpresaOriginal#,#id_EmpresaNueva#, #id_ContraRecibo#, #nd_ContraRecibo#, #id_EstatusFactura#,
					#fh_Pago#, '#de_Observaciones#', '#cl_TipoFactura#', #sn_CopiaFactura#, #im_SubTotal#, #id_Moneda#, #imp_SubTotalFactura#, #idu_MonedaFactura#
		</cfquery>		
		<cfset ContraRecibos.listado= TRUE>
		<cfset ContraRecibos.tipoError= ''>
		<cfset ContraRecibos.mensaje= 'Se obtuvo el recordset de ContraRecibos correctamente'>
		<cfreturn ContraRecibos>
		<cfcatch type='database'>
			<cfset ContraRecibos.listado= FALSE>
			<cfset ContraRecibos.tipoError= 'database-indefinido'>
			<cfset ContraRecibos.mensaje= 'Error en el acceso a base de datos al Actualizar la Factura(#cfcatch.Detail#)'>
			<cfreturn ContraRecibos>
		</cfcatch>
	</cftry>
</cffunction>


<cffunction name='RSEditarContraRecibosDetalle_OrdenCompra' access='public' returntype='struct'>

	<cfargument name='id_EmpresaOriginal' type='numeric' required='yes'>
	<cfargument name='id_EmpresaNueva' type='numeric' required='yes'>
	<cfargument name='id_ContraRecibo' type='numeric' required='yes'>
	<cfargument name='nd_ContraRecibo' type='numeric' required='yes'>
	<cfargument name='id_OrdenCompra' type='string' required='yes' >
	<cftry>
		<cfquery datasource='#cnx#' name='RSFilasAfectadas' username='#user_sql#' password='#password_sql#'>
			EXECUTE ContraRecibosDetalle_ActualizarOrdenCompra #id_EmpresaOriginal#, #id_EmpresaNueva#, #id_ContraRecibo#, #nd_ContraRecibo#, #id_OrdenCompra#
		</cfquery>
		<cfset ContraRecibosDetalle.actualizado= TRUE>
		<cfset ContraRecibosDetalle.tipoError= ''>
		<cfset ContraRecibosDetalle.mensaje= 'Detalle de Contrarecibo actualizado correctamente'>
		<cfreturn ContraRecibosDetalle>
		<cfcatch type='database'>
			<cfset ContraRecibosDetalle.actualizado= FALSE>
			<cfset ContraRecibosDetalle.tipoError= 'database-indefinido'>
			<cfset ContraRecibosDetalle.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn ContraRecibosDetalle>
		</cfcatch>
	</cftry>
</cffunction>

<cffunction name='RSBorrarContraRecibosDetalle' access='public' returntype='struct'>

	<cfargument name='id_EmpresaOriginal' type='numeric' required='yes'>
	<cfargument name='id_EmpresaNueva' type='numeric' required='yes'>
	<cfargument name='id_Obra' type='numeric' required='yes'>
	<cfargument name='id_ContraRecibo' type='numeric' required='yes'>
	<cfargument name='nd_ContraRecibo' type='numeric' required='yes'>
	<cfargument name='id_EstatusFactura' type='numeric' required='yes'>
	<cfargument name='cl_TipoFactura' type='numeric' required='yes'>
	
	<cftry>
		<cfquery datasource='#cnx#' name='RSFilasAfectadas' username='#user_sql#' password='#password_sql#'>
			EXECUTE ContraRecibosDetalle_BorrarFactura #id_EmpresaOriginal#,#id_EmpresaNueva#,#id_Obra#, #id_ContraRecibo#, #nd_ContraRecibo#, #id_EstatusFactura#, #cl_TipoFactura#
		</cfquery>
		<cfset ContraRecibosDetalle.actualizado= TRUE>
		<cfset ContraRecibosDetalle.tipoError= ''>
		<cfset ContraRecibosDetalle.mensaje= 'Detalle de Contrarecibo actualizado correctamente'>
		<cfreturn ContraRecibosDetalle>
		<cfcatch type='database'>
			<cfset ContraRecibosDetalle.actualizado= FALSE>
			<cfset ContraRecibosDetalle.tipoError= 'database-indefinido'>
			<cfset ContraRecibosDetalle.mensaje= 'Error en el acceso a base de datos al Eliminar(#cfcatch.Detail#)'>
			<cfreturn ContraRecibosDetalle>
		</cfcatch>
	</cftry>
</cffunction>

<!--- Funcion para generar un reporte de facturas con diversos filtros--->
<!--- Kaleb Ontiveros 04/12/2012 --->  
<cffunction name='RSMostrarContrarecibosReporte' access='public' returntype='struct'>
	<cfargument name='id_Empresa' type='string' required='no' default="">
	<cfargument name='id_Obra' type='string' required='no' default="">
	<cfargument name='id_Proveedor' type='string' required='no' default="">
	<cfargument name='id_OrdenCompra' type='string' required='no' default="">
	<cfargument name='cl_TipoFactura' type='string' required='no' default="">
	<cfargument name='de_Factura' type='string' required='no' default="">
	<cfargument name='id_Tipo' type='string' required='no' default="1">
	<cfargument name='id_Estatus' type='string' required='no' default="511,519,521">
	
	<cfif id_Empresa EQ ''>
		<cfset id_Empresa = 'NULL'>
	</cfif>	
	<cfif id_Obra EQ ''>
		<cfset id_Obra = 'NULL'>
	</cfif>	
	<cfif id_Proveedor EQ ''>
		<cfset id_Proveedor = 'NULL'>
	</cfif>	
	<cfif id_OrdenCompra EQ ''>
		<cfset id_OrdenCompra = 'NULL'>
	</cfif>	
	<cfif cl_TipoFactura EQ ''>
		<cfset cl_TipoFactura = 'NULL'>
	</cfif>	
	<cfif de_Factura EQ ''>
		<cfset de_Factura = 'NULL'>
	<cfelse>
    	<cfset de_Factura = DE(#de_Factura#)>
	</cfif>	
	
	<cftry>
    <cfif id_Tipo EQ 1>
		<cfquery datasource='#cnx#' name='RS_MostrarContraRecibosDetalle' username='#user_sql#' password='#password_sql#'>
			EXECUTE Contrarecibosdetalle_Reporte_PorPagar #id_Empresa#, #id_Obra#, #id_Proveedor#, #id_OrdenCompra#, #cl_TipoFactura#, #de_Factura#, '#id_Estatus#'
		</cfquery>
	<cfelse>
		<cfquery datasource='#cnx#' name='RS_MostrarContraRecibosDetalle' username='#user_sql#' password='#password_sql#'>
			EXECUTE Contrarecibosdetalle_Reporte_PorPagar_Proveedor #id_Empresa#, #id_Obra#, #id_Proveedor#, #id_OrdenCompra#, #cl_TipoFactura#, #de_Factura#, '#id_Estatus#'
		</cfquery>
	</cfif>
		<cfset ContraRecibosDetalle.listado= TRUE>
		<cfset ContraRecibosDetalle.tipoError= ''>
		<cfset ContraRecibosDetalle.rs= RS_MostrarContraRecibosDetalle>
		<cfset ContraRecibosDetalle.mensaje= 'Se obtuvo el recordset de Reporte Contrarecibos Correctament'>
		<cfreturn ContraRecibosDetalle>
		<cfcatch type='database'>
			<cfset ContraRecibosDetalle.listado= FALSE>
			<cfset ContraRecibosDetalle.tipoError= 'database-indefinido'>
			<cfset ContraRecibosDetalle.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn ContraRecibosDetalle>
		</cfcatch>
	</cftry>
</cffunction>

<!--- Funcion para generar un reporte de facturas con diversos filtros--->
<!--- Kaleb Ontiveros 04/12/2012 --->
<cffunction name='RSMostrarContrarecibosReporteGeneral' access='public' returntype='struct'>
	<cfargument name='id_Obra' type='string' required='no'>
	<cfargument name='nu_Semana' type='string' required='no'>
	<cfargument name='fh_Pago' type='string' required='no'>
	<cfargument name='de_Factura' type='string' required='no'>
	<cfargument name='id_Encargado' type='string' required='no'>

	<cfif id_Obra EQ ''>
		<cfset id_Obra = 'NULL'>
	</cfif>	
	<cfif nu_Semana EQ ''>
		<cfset nu_Semana = 'NULL'>
	</cfif>	
	<cfif de_Factura EQ ''>
		<cfset de_Factura = 'NULL'>
        <cfelse>
        <cfset de_Factura = '#de_Factura#' >	
	</cfif>        
    <cfif id_Encargado EQ ''>
		<cfset id_Encargado = 'NULL'>
	</cfif>    
	<cfif fh_Pago EQ "">
		<cfset fh_Pago = 'NULL'>
	<cfelse>
		<cfset fh_Pago = RIGHT(fh_Pago,4) & '/' & MID(fh_Pago,4,2) & '/' & LEFT(fh_Pago,2)>
		<cfset fh_Pago = DE(fh_Pago)>
	</cfif>
    	
	<cftry>
        <cfquery datasource='#cnx#' name='RS_MostrarContraRecibosDetalle' username='#user_sql#' password='#password_sql#'>
        	EXECUTE ContraRecibosDetalle_Reporte_General #id_Obra#,#nu_Semana#,#fh_Pago#,#de_Factura#,#id_Encargado#
    	</cfquery>

		<cfset ContraRecibosDetalle.listado= TRUE>
		<cfset ContraRecibosDetalle.tipoError= ''>
		<cfset ContraRecibosDetalle.rs= RS_MostrarContraRecibosDetalle>
		<cfset ContraRecibosDetalle.mensaje= 'Se obtuvo el recordset de Reporte Contrarecibos Correctamente'>
		<cfreturn ContraRecibosDetalle>
		<cfcatch type='database'>
			<cfset ContraRecibosDetalle.listado= FALSE>
			<cfset ContraRecibosDetalle.tipoError= 'database-indefinido'>
			<cfset ContraRecibosDetalle.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn ContraRecibosDetalle>
		</cfcatch>
	</cftry>
</cffunction>

<!--- Funcion para agregar una nueva factura --->
<!--- Kaleb Ontiveros 12/11/2012 --->
<cffunction name='RSAgregarFacturas' access='public' returntype="struct">
	<cfargument name='id_Empresa' type='numeric' required='yes'>
    <cfargument name='id_Obra' type='numeric' required='yes'>
    <cfargument name='id_Proveedor' type='numeric' required='yes'>
    <cfargument name='nu_Semana' type='numeric' required='yes'>
    <cfargument name='de_Factura' type='string' required='yes'>
    <cfargument name='de_NotaCredito' type='string' required='no'>
    <cfargument name='fh_Factura' type='string' required='yes'>
    <cfargument name='fh_Recepcion' type='string' required='yes'>
    <cfargument name='fh_Corte' type='string' required='yes'>
    <cfargument name='id_Modulo' type='numeric' required='yes'>
    <cfargument name='id_Moneda' type='string' required='yes'>
    <cfargument name='im_Subtotal' type='numeric' required='yes'>
    <cfargument name='im_iva' type='numeric' required='yes'>
    <cfargument name='im_Factura' type='numeric' required='yes'>
    <cfargument name='im_TipoCambio' type='numeric' required='yes'>    
    <cfargument name='de_Observaciones' type='string' required='no'>
    <cfargument name='fl_OrdenCompra' type='string' required='no' default="">
    <cfargument name='im_OrdenCompra' type='string' required='no' default="0">
    <cfargument name='cl_TipoRecepcion' type='string' required='no' default="NULL">
    <cfif im_OrdenCompra EQ ''>
		<cfset im_OrdenCompra = 0.00>
	</cfif>
    
	<cftry>
    	
		<cfquery datasource='#cnx#' name='RS_MostrarContraRecibos' username='#user_sql#' password='#password_sql#'>
			EXECUTE ContraRecibosDetalle_AgregarFactura #id_Empresa#,
                                                         #id_Obra#,
                                                         #id_Proveedor#,
                                                         #nu_Semana#,
                                                         '#de_Factura#',
                                                         '#de_NotaCredito#',
                                                         '#fh_Factura#',
                                                         '#fh_Recepcion#',
                                                         '#fh_Corte#',
                                                         #im_Subtotal#,
                                                         #im_iva#,
                                                         #im_Factura#,
                                                         #id_Modulo#,
                                                         #id_Moneda#,
                                                         #im_TipoCambio#,
                                                         '#de_Observaciones#',
                                                         '#fl_OrdenCompra#',
                                                         #im_OrdenCompra#,
                                                         '#cl_TipoRecepcion#'
                                                 
		</cfquery>
		<cfset ContraRecibos.agregado= TRUE>
		<cfset ContraRecibos.tipoError= ''>
		<cfset ContraRecibos.mensaje= 'Se agrego el ContraRecibo correctamente'>
		<cfreturn ContraRecibos>
		<cfcatch type='database'>
			<cfset ContraRecibos.agregado= FALSE>
			<cfset ContraRecibos.tipoError= 'database-indefinido'>
			<cfset ContraRecibos.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn ContraRecibos>
		</cfcatch>
	</cftry>
</cffunction>

<!--- Funcion para sacar el Listado de Facturas por Encargado de Estimaciones--->
<!--- Ricardo Avendaño 15/10/2012 --->
<cffunction name='Cargar_ContraRecibosDetalle' access='public' returntype='struct'>

	<cfargument name='id_Empresa' type='string' required='yes' default="">
	<cfargument name='id_Proveedor' type='string' required='yes' default="">
	<cfargument name='nd_ContraRecibo' type='string' required='yes' default="">
	
	<cftry>
		<cfquery datasource='#cnx#' name='RS_MostrarContraRecibos' username='#user_sql#' password='#password_sql#'>
			EXECUTE ContraRecibosDetalle_ListadoFacturasEditar #id_Empresa#, #id_ContraRecibo#, #nd_ContraRecibo# 
		</cfquery>
		<cfset ContraRecibos.listado= TRUE>
		<cfset ContraRecibos.tipoError= ''>
		<cfset ContraRecibos.rs= RS_MostrarContraRecibos>
		<cfset ContraRecibos.mensaje= 'Se obtuvo el recordset de ContraRecibos correctamente'>
		<cfreturn ContraRecibos>
		<cfcatch type='database'>
			<cfset ContraRecibos.listado= FALSE>
			<cfset ContraRecibos.tipoError= 'database-indefinido'>
			<cfset ContraRecibos.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn ContraRecibos>
		</cfcatch>
	</cftry>
</cffunction>

<!---funcion que valida que no se agregue la misma factura--->
<cffunction name="getFacrura" access="public" returntype="struct">
    <cfargument name='id_Empresa' type='string' required='yes' default="">
    <cfargument name='id_Obra' type='string' required='yes' default="">
	<cfargument name='id_Proveedor' type='string' required='yes' default="">
	<cfargument name='de_Factura' type='string' required='yes' default="">
	<cfargument name='nu_Semana' type='string' required='yes' default="">
    <cftry>
        <cfquery datasource='#cnx#' name="rs_Factura">
            SELECT id_ContraRecibo  FROM ContraRecibosDetalle
            WHERE 
            	id_Empresa=#id_Empresa# and id_Obra=#id_Obra# and 
                id_Proveedor=#id_Proveedor# and de_Factura='#de_Factura#' and 
                nu_Semana=#nu_Semana#
				and id_estatusfactura not in (512,523)
        </cfquery>
    	<cfset Facturas.listado=TRUE>
      	<cfset Facturas.tipoError=''>
		<cfset Facturas.rs=rs_Factura>     
        <cfset Facturas.mensaje= 'Se obtuvo el recordset de ContraRecibos correctamente'>   
        <cfreturn Facturas>
        <cfcatch type="database">
			<cfset Facturas.listado=FALSE>
            <cfset Facturas.tipoError='database-indefinido'> 
            <cfset Facturas.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>  
            <cfreturn Facturas>
        </cfcatch>
    </cftry>
</cffunction>

<!--- Funcion para editar una factura --->
<!--- Ricardo Avendaño 03/12/2012 --->
<cffunction name='RSEditarFacturas' access='public' returntype="struct">

	<cfargument name='id_Empresa' type='string' required='yes'>
   	<cfargument name='id_ContraRecibo' type='string' required='yes'>
    <cfargument name='nd_ContraRecibo' type='string' required='yes'>
    <cfargument name='de_Factura' type='string' required='yes'>
    <cfargument name='fh_Factura' type='string' required='yes'>
    <cfargument name='fh_Recepcion' type='string' required='yes'>
    <cfargument name='fh_Corte' type='string' required='yes'>

	<cftry>
		<cfquery datasource='#cnx#' name='RS_EditarContraRecibos' username='#user_sql#' password='#password_sql#'>
			EXECUTE ContraRecibosDetalle_EditarFactura
                         #id_Empresa#,
                         #id_ContraRecibo#,
                         #nd_ContraRecibo#,
                         '#de_Factura#',
                         '#fh_Factura#',
                         '#fh_Recepcion#',
                         '#fh_Corte#'
                               
		</cfquery>
		<cfset ContraRecibos.listado= TRUE>
		<cfset ContraRecibos.tipoError= ''>
		<cfset ContraRecibos.mensaje= 'Se actualizo el ContraRecibos correctamente'>
		<cfreturn ContraRecibos>
		<cfcatch type='database'>
			<cfset ContraRecibos.listado= FALSE>
			<cfset ContraRecibos.tipoError= 'database-indefinido'>
			<cfset ContraRecibos.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn ContraRecibos>
		</cfcatch>
	</cftry>
</cffunction>

<!---Ricardo Avendaño--->
<!---11-12-2012--->
<!---carga facturas contraRecibosDetalle--->
   <cffunction name="RSListadoFacturas" access="public" returntype="struct">
	<cfargument name="id_Obra" type="string" required="no" default="">
    <cfargument name="id_Proveedor" type="string" required="no">
	<cfargument name="de_Factura" type="string" required="no">
    <cfargument name="cl_TipoFactura" type="string" required="no">		
	<cfargument name="id_EstatusFactura" type="string" required="no">
	
	<cfif #id_Obra# EQ "">
		<cfset id_Obra = 'NULL'>
	</cfif>
	<cfif #id_Proveedor# EQ "">
		<cfset id_Proveedor = 'NULL'>
	</cfif>
	<cfif #de_Factura# EQ "">
		<cfset de_Factura = 'NULL'>
	<cfelse>
		<cfset de_Factura = DE(de_Factura)>
	</cfif>
	<cfif #cl_TipoFactura# EQ "">
		<cfset cl_TipoFactura = 'NULL'>
	</cfif>
    <cfif #id_EstatusFactura# EQ "">
		<cfset id_EstatusFactura = 0>
	</cfif>  
    
    
	<cftry>
		<cfquery datasource='#session.cnx#' name='RS_MostrarFacturas' >
			EXECUTE ContraRecibosDetalle_ObtenerListado #id_Obra#, #id_Proveedor#, #de_Factura#, #cl_TipoFactura#, #id_EstatusFactura#
		</cfquery>
		<cfset ContraRecibos.listado= TRUE>
		<cfset ContraRecibos.tipoError= ''>
		<cfset ContraRecibos.rs= RS_MostrarFacturas>
		<cfset ContraRecibos.mensaje= 'Se obtuvo el recordset de ContraRecibos correctamente'>
		<cfreturn ContraRecibos>
		<cfcatch type='database'>
			<cfset ContraRecibos.listado= FALSE>
			<cfset ContraRecibos.tipoError= 'database-indefinido'>
			<cfset ContraRecibos.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn ContraRecibos>
		</cfcatch>
	</cftry>  
</cffunction>


	<!---Ricardo Avendaño--->
    <!---12/12/2014--->
    <!---carga facturas contraRecibosDetalle--->
   <cffunction name="RSListadoFacturasParaPago" access="public" returntype="struct">
	<cfargument name="id_Obra" 				type="string" required="no" default="">
    <cfargument name="id_Proveedores" 		type="string" required="no">
    <cfargument name="id_Empresa" 	 		type="string" required="no">
	<cfargument name="de_Factura" 	  		type="string" required="no">
    <cfargument name="cl_TipoFactura" 		type="string" required="no">		
	<cfargument name="id_EstatusFactura"	type="string" required="no">
	
	<cfif #id_Empresa# EQ "">
		<cfset id_Empresa = 'NULL'>
	</cfif>
	<cfif #id_Obra# EQ "">
		<cfset id_Obra = 'NULL'>
	</cfif>
	<cfif #id_Proveedor# EQ "">
		<cfset id_Proveedor = 'NULL'>
	</cfif>
	<cfif #de_Factura# EQ "">
		<cfset de_Factura = 'NULL'>
	<cfelse>
		<cfset de_Factura = DE(de_Factura)>
	</cfif>
	<cfif #cl_TipoFactura# EQ "">
		<cfset cl_TipoFactura = 'NULL'>
	</cfif>
    <cfif #id_EstatusFactura# EQ "">
		<cfset id_EstatusFactura = 0>
	</cfif>  
    
    
	<cftry>
	
		<cfquery datasource='#session.cnx#' name='RS_MostrarFacturas' >
			EXECUTE ContraRecibosDetalle_ObtenerListadoParaPago #id_Obra# ,null, #id_Empresa# , #de_Factura#, #cl_TipoFactura#, #id_EstatusFactura#
		</cfquery>
		
		<cfquery name="RSDatos" dbtype="query">                
            SELECT 
				* 
			FROM
				RS_MostrarFacturas
			<cfif id_Proveedores NEQ ''>
				WHERE 
					id_Proveedor IN (#id_Proveedores#)
			</cfif>
        </cfquery>
		<cfset ContraRecibos.listado= TRUE>
		<cfset ContraRecibos.tipoError= ''>
		<cfset ContraRecibos.rs= RSDatos>
		<cfset ContraRecibos.mensaje= 'Se obtuvo el recordset de ContraRecibos correctamente'>
		<cfreturn ContraRecibos>
		<cfcatch type='database'>
			<cfset ContraRecibos.listado= FALSE>
			<cfset ContraRecibos.tipoError= 'database-indefinido'>
			<cfset ContraRecibos.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn ContraRecibos>
		</cfcatch>
	</cftry>  
</cffunction>

<cffunction name='ContraRecibosDetalle_Actualizar' access='public' returntype='struct'>
	<cfargument name='id_Empresa' type='numeric' required='yes'>
    <cfargument name='id_Obra' type='numeric' required='yes'>
	<cfargument name='id_ContraRecibo' type='numeric' required='yes' default="">
	<cfargument name='nd_ContraRecibo' type='numeric' required='yes' default="">
	<cfargument name='id_EstatusFactura' type='numeric' required='yes' default="">	
	<cfargument name='fh_Recepcion' type='string' required='no' default="null">
	<cfargument name='fh_Corte' type='string' required='no' default="null">
	<cfargument name='nu_Semana' type='string' required='yes'>
	<cfargument name='cl_TipoRecepcion' type='string' required='no' default="NULL">
	
	<cfif cl_TipoRecepcion NEQ 'null'>
		<cfset cl_TipoRecepcion = DE(#cl_TipoRecepcion#)>
	</cfif>
	<cfif fh_Recepcion NEQ 'null'>
		<cfset fh_Recepcion = (Right(fh_Recepcion,4) & '/' & Mid(fh_Recepcion,4,2) & '/' & Left(fh_Recepcion,2))>
		<cfset fh_Recepcion = DE(#fh_Recepcion#)>
	</cfif>
	<cfif fh_Recepcion NEQ 'null'>
		<cfset fh_Corte = (Right(fh_Corte,4) & '/' & Mid(fh_Corte,4,2) & '/' & Left(fh_Corte,2))>
		<cfset fh_Corte = DE(#fh_Corte#)>
	</cfif>

	<cftry>
		<cfquery datasource='#cnx#' name='RS_MostrarContraRecibos' username='#user_sql#' password='#password_sql#'>
			EXECUTE ContraRecibosDetalle_MarcarFactura 
								#id_Empresa#, #id_ContraRecibo#, #nd_ContraRecibo#, 
								#id_EstatusFactura#, #fh_Recepcion#, #fh_Corte#, #nu_Semana#,#cl_TipoRecepcion#
		</cfquery>
		<!---Log--->
		<cf_log datasource='#cnx#' username='#user_sql#' password='#password_sql#' id_Empresa="#id_Empresa#" id_Obra="#id_Obra#" id_Usuario="#Session.id_Usuario#" id_MenuOpcion="#Session.id_MenuOpcion#">
			<cfoutput>
				EXECUTE ContraRecibosDetalle_MarcarFactura 
									#id_Empresa#, #id_ContraRecibo#, #nd_ContraRecibo#, 
									#id_EstatusFactura#, #fh_Recepcion#, #fh_Corte#, #nu_Semana#,#cl_TipoRecepcion#
			</cfoutput>
		</cf_log>
		<!---Termina Log --->
		<cfset ContraRecibosDetalle.listado= TRUE>
		<cfset ContraRecibosDetalle.tipoError= ''>
		<cfset ContraRecibosDetalle.mensaje= 'Se obtuvo el recordset de ContraRecibos correctamente'>
		<cfreturn ContraRecibosDetalle>
		<cfcatch type='database'>
			<cfset ContraRecibosDetalle.listado= FALSE>
			<cfset ContraRecibosDetalle.tipoError= 'database-indefinido'>
			<cfset ContraRecibosDetalle.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn ContraRecibosDetalle>
		</cfcatch>
	</cftry>
</cffunction>
<cffunction name='ContraRecibosDetalle_ObrasProveedor' access='public' returntype='struct'>
	<cfargument name='id_Proveedor' type='string' required='yes' default="">	
	<cftry>
		<cfquery datasource='#cnx#' name='RS_MostrarContraRecibos' username='#user_sql#' password='#password_sql#'>
			EXECUTE ContraRecibosDetalle_ObrasProveedor #id_Proveedor#
		</cfquery>
		<cfset ContraRecibos.listado= TRUE>
		<cfset ContraRecibos.tipoError= ''>
		<cfset ContraRecibos.rs= RS_MostrarContraRecibos>
		<cfset ContraRecibos.mensaje= 'Se obtuvo el recordset de ContraRecibos correctamente'>
		<cfreturn ContraRecibos>
		<cfcatch type='database'>
			<cfset ContraRecibos.listado= FALSE>
			<cfset ContraRecibos.tipoError= 'database-indefinido'>
			<cfset ContraRecibos.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn ContraRecibos>
		</cfcatch>
	</cftry>
</cffunction>
<cffunction name='RSEditarFacturasComentario' access='public' returntype="struct">

	<cfargument name='id_Empresa' type='string' required='yes'>
   	<cfargument name='id_ContraRecibo' type='string' required='yes'>
    <cfargument name='nd_ContraRecibo' type='string' required='yes'>
    <cfargument name='de_Concepto' type='string' required='no'>
	<cfargument name='de_Observaciones' type='string' required='no'>
	<cfargument name='id_EstatusFactura' type='string' required='no'>	
	<cftry>
		<cfquery datasource='#cnx#' name='RS_EditarContraRecibos' username='#user_sql#' password='#password_sql#'>
			EXECUTE ContraRecibosDetalle_EditarFacturaComentario #id_Empresa#,#id_ContraRecibo#,#nd_ContraRecibo#,'#de_Concepto#','#de_Observaciones#',#id_EstatusFactura#
		</cfquery>
		<cfset ContraRecibos.listado= TRUE>
		<cfset ContraRecibos.tipoError= ''>
		<cfset ContraRecibos.mensaje= 'Se actualizo el ContraRecibos correctamente'>
		<cfreturn ContraRecibos>
		<cfcatch type='database'>
			<cfset ContraRecibos.listado= FALSE>
			<cfset ContraRecibos.tipoError= 'database-indefinido'>
			<cfset ContraRecibos.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn ContraRecibos>
		</cfcatch>
	</cftry>
</cffunction>

<!--- Funcion obtener ordenes compra --->
<!--- Autor: EIRR --->
<cffunction name='RSOrdenesCompraObtener' access='public' returntype="struct">
	<cfargument name='id_Empresa' type='string' required='yes'>
    <cfargument name='id_Obra' type='string' required='yes'>
    <cfargument name='id_Proveedor' type='string' required='yes'>
    <cfargument name='cl_TipoOrdenCompra' type='string' required='yes'>   
	<cftry>
		<cfquery datasource='#cnx#' name='RSOrdenesCompraObtener' username='#user_sql#' password='#password_sql#'>
			EXECUTE OrdenesCompraObtener #id_Empresa#, #id_Obra#, #id_Proveedor#, #cl_TipoOrdenCompra#
		</cfquery>
		<cfset OrdenesCompra.listado= TRUE>
		<cfset OrdenesCompra.tipoError= ''>
        <cfset OrdenesCompra.rs = RSOrdenesCompraObtener>
		<cfset OrdenesCompra.mensaje= 'Se obtuvo el recorset de Ordenes Compra correctamente'>
		<cfreturn OrdenesCompra>
		<cfcatch type='database'>
			<cfset OrdenesCompra.listado= FALSE>
			<cfset OrdenesCompra.tipoError= 'database-indefinido'>
			<cfset OrdenesCompra.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn OrdenesCompra>
		</cfcatch>
	</cftry>
</cffunction>

<cffunction name='RSBorrarContraRecibosDetalleMateriales' access='public' returntype='struct'>
	<cfargument name='id_EmpresaOriginal' type='numeric' required='yes'>
	<cfargument name='id_EmpresaNueva' type='numeric' required='yes'>
	<cfargument name='id_Obra' type='numeric' required='yes'>
	<cfargument name='id_ContraRecibo' type='numeric' required='yes'>
	<cfargument name='nd_ContraRecibo' type='numeric' required='yes'>
	<cfargument name='id_EstatusFactura' type='numeric' required='yes'>
	<cfargument name='cl_TipoFactura' type='numeric' required='yes'>
	
	<cftry>
		<cfquery datasource='#cnx#' name='RSFilasAfectadas' username='#user_sql#' password='#password_sql#'>
			EXECUTE ContraRecibosDetalle_BorrarFacturaMateriales #id_EmpresaOriginal#,#id_EmpresaNueva#,#id_Obra#, #id_ContraRecibo#, #nd_ContraRecibo#, #id_EstatusFactura#, #cl_TipoFactura#
		</cfquery>
		<cfset ContraRecibosDetalle.actualizado= TRUE>
		<cfset ContraRecibosDetalle.tipoError= ''>
		<cfset ContraRecibosDetalle.mensaje= 'Detalle de Contrarecibo actualizado correctamente'>
		<cfreturn ContraRecibosDetalle>
		<cfcatch type='database'>
			<cfset ContraRecibosDetalle.actualizado= FALSE>
			<cfset ContraRecibosDetalle.tipoError= 'database-indefinido'>
			<cfset ContraRecibosDetalle.mensaje= 'Error en el acceso a base de datos al Eliminar(#cfcatch.Detail#)'>
			<cfreturn ContraRecibosDetalle>
		</cfcatch>
	</cftry>
</cffunction>

<!--- Funcion para borrar facturas en recepción de facturas--->
<!--- Autor: Brenda Machado --->
<!--- Fecha: 09/09/2013 --->
<cffunction name='RSEliminarFacturasRecepcion' access='public' returntype="struct">
	<cfargument name='id_Empresa' type='string' required='yes'>
   	<cfargument name='id_ContraRecibo' type='string' required='yes'>
    <cfargument name='nd_ContraRecibo' type='string' required='yes'>
    
	<cftry>
		<!--- <cfquery datasource='#cnx#' name='RS_BorrarContraRecibos' username='#user_sql#' password='#password_sql#'>
			EXECUTE ContraRecibosDetalle_BorrarFacturaRecepcion #id_Empresa#,#id_ContraRecibo#,#nd_ContraRecibo#
		</cfquery> --->
		<cfquery datasource='#cnx#' name='RS_BorrarContraRecibos' username='#user_sql#' password='#password_sql#'>
			UPDATE 
				Contrarecibosdetalle 
			SET 
				id_EstatusFactura = 512, id_OrdenCompra = NULL
			WHERE 
				id_Empresa = #id_Empresa# AND 
				id_Contrarecibo = #id_Contrarecibo# AND 
				nd_ContraRecibo = #nd_ContraRecibo#
		</cfquery>
		<cfset ContraRecibos.listado= TRUE>
		<cfset ContraRecibos.tipoError= ''>
		<cfset ContraRecibos.mensaje= 'Se actualizo la Factura correctamente'>
		<cfreturn ContraRecibos>
		<cfcatch type='database'>
			<cfset ContraRecibos.listado= FALSE>
			<cfset ContraRecibos.tipoError= 'database-indefinido'>
			<cfset ContraRecibos.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn ContraRecibos>
		</cfcatch>
	</cftry>
</cffunction>

<!--- Funcion para editar facturas en recepción de facturas--->
<!--- Autor: Brenda Machado --->
<!--- Fecha: 26/08/2013 --->
<cffunction name='RSEditarFacturasRecepcion' access='public' returntype="struct">
	<cfargument name='id_Empresa' type='string' required='yes'>
   	<cfargument name='id_ContraRecibo' type='string' required='yes'>
    <cfargument name='nd_ContraRecibo' type='string' required='yes'>
    <cfargument name='de_Factura' type='string' required='yes'>

	<cftry>
		<cfquery datasource='#cnx#' name='RS_EditarContraRecibos' username='#user_sql#' password='#password_sql#'>
			EXECUTE ContraRecibosDetalle_EditarFacturaRecepcion #id_Empresa#,#id_ContraRecibo#,#nd_ContraRecibo#,'#de_Factura#'
		</cfquery>
		<cfset ContraRecibos.listado= TRUE>
		<cfset ContraRecibos.tipoError= ''>
		<cfset ContraRecibos.mensaje= 'Se actualizo la Factura correctamente'>
		<cfreturn ContraRecibos>
		<cfcatch type='database'>
			<cfset ContraRecibos.listado= FALSE>
			<cfset ContraRecibos.tipoError= 'database-indefinido'>
			<cfset ContraRecibos.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn ContraRecibos>
		</cfcatch>
	</cftry>
</cffunction>

	<!--- 
	Autor: Rolando López
	Fecha: 2014/05/23
	---> 
    
    <!---MUESTRA EL LISTADO DE FACTURAS PENDIENTES POR PAGAR--->
	<cffunction name='RSMostrarContrarecibosPorPagarReporte' access='public' returntype='struct'>
        <cfargument name='id_Obra' type='string' required='no'>
        <cfargument name='nu_Semana' type='string' required='no'>
        <cfargument name='fh_Pago' type='string' required='no'>
        
        <cfif id_Obra EQ ''>
            <cfset id_Obra = 'NULL'>
        </cfif>	
        <cfif nu_Semana EQ ''>
            <cfset nu_Semana = 'NULL'>
        </cfif>	
        <cfif fh_Pago NEQ "">
            <cfset fh_Pago = RIGHT(fh_Pago,4) & '/' & MID(fh_Pago,4,2) & '/' & LEFT(fh_Pago,2)>
        </cfif>
        
        <cftry>
            <cfquery datasource='#cnx#' name='RS_MostrarContraRecibosDetalle' username='#user_sql#' password='#password_sql#'>
                EXECUTE Contrarecibos_Mostrar_Pendientes #id_Obra#
            </cfquery>
            <cfset ContraRecibosDetalle.listado= TRUE>
            <cfset ContraRecibosDetalle.tipoError= ''>
            <cfset ContraRecibosDetalle.rs= RS_MostrarContraRecibosDetalle>
            <cfset ContraRecibosDetalle.mensaje= 'Se obtuvo el recordset de Reporte Contrarecibos Correctamente'>
            <cfreturn ContraRecibosDetalle>
            <cfcatch type='database'>
                <cfset ContraRecibosDetalle.listado= FALSE>
                <cfset ContraRecibosDetalle.tipoError= 'database-indefinido'>
                <cfset ContraRecibosDetalle.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
                <cfreturn ContraRecibosDetalle>
            </cfcatch>
        </cftry>
    </cffunction>
    
<cffunction name='ContraRecibosDetalle_Actualizar_Pagar' access='public' returntype='struct'>
	<cfargument name='id_Empresa' type='numeric' required='yes'>
    <cfargument name='id_Obra' type='numeric' required='yes'>
	<cfargument name='id_ContraRecibo' type='numeric' required='yes' default="">
	<cfargument name='nd_ContraRecibo' type='numeric' required='yes' default="">
	<cfargument name='id_EstatusFactura' type='numeric' required='yes' default="">	
	<cfargument name='fh_Pago' type='string' required='no' default="null">
    
	<cfif fh_Pago NEQ 'null'>
    	<cfset fh_Pago = (Right(fh_Pago,4) & '-' & Mid(fh_Pago,4,2) & '-' & Left(fh_Pago,2))>
	
	</cfif>

	<cftry>

		<cfquery datasource='#cnx#' name='RS_MostrarContraRecibos'>
			EXECUTE ContraRecibosDetalle_MarcarFactura_Pagar #id_Empresa#, #id_ContraRecibo#, #nd_ContraRecibo#,#id_EstatusFactura#, '#fh_Pago#'
		</cfquery>
		<!---Log--->
		<!--- <cf_log datasource='#cnx#' username='#user_sql#' password='#password_sql#' id_Empresa="#id_Empresa#" id_Obra="#id_Obra#" id_Usuario="#Session.id_Usuario#" id_MenuOpcion="#Session.id_MenuOpcion#">
			<cfoutput>
                EXECUTE ContraRecibosDetalle_MarcarFactura_Pagar 
                        #id_Empresa#, #id_ContraRecibo#, #nd_ContraRecibo#,#id_EstatusFactura#, #fh_Pago#
			</cfoutput>
		</cf_log> --->
		<!---Termina Log --->
		<cfset ContraRecibosDetalle.listado= TRUE>
		<cfset ContraRecibosDetalle.tipoError= ''>
		<cfset ContraRecibosDetalle.mensaje= 'Se obtuvo el recordset de ContraRecibos correctamente'>
		<cfreturn ContraRecibosDetalle>
		<cfcatch type='database'>
			<cfset ContraRecibosDetalle.listado= FALSE>
			<cfset ContraRecibosDetalle.tipoError= 'database-indefinido'>
			<cfset ContraRecibosDetalle.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn ContraRecibosDetalle>
		</cfcatch>
	</cftry>
</cffunction>

   <cffunction name="RSListadoFacturasParaPago_Temporal" access="public" returntype="struct">
	<cfargument name="id_Obra" 				type="string" required="no" default="">
    <cfargument name="id_Proveedor" 		type="string" required="no">
    <cfargument name="id_Empresa" 	 		type="string" required="no" default="NULL">
	<cfargument name="de_Factura" 	  		type="string" required="no">
    <cfargument name="cl_TipoFactura" 		type="string" required="no">		
	<cfargument name="id_EstatusFactura"	type="string" required="no">
	
	<cfif #id_Empresa# EQ "">
		<cfset id_Empresa = 'NULL'>
	</cfif>
	<cfif #id_Obra# EQ "">
		<cfset id_Obra = 'NULL'>
	</cfif>
	<cfif #id_Proveedor# EQ "">
		<cfset id_Proveedor = 'NULL'>
	</cfif>
	<cfif #de_Factura# EQ "">
		<cfset de_Factura = 'NULL'>
	<cfelse>
		<cfset de_Factura = DE(de_Factura)>
	</cfif>
	<cfif #cl_TipoFactura# EQ "">
		<cfset cl_TipoFactura = 'NULL'>
	</cfif>
    <cfif #id_EstatusFactura# EQ "">
		<cfset id_EstatusFactura = 0>
	</cfif>  
    
    
	<cftry>
	
		<cfquery datasource='#session.cnx#' name='RS_MostrarFacturas' >
			EXECUTE ContraRecibosDetalle_ObtenerListadoParaPago_Temporal #id_Obra# ,#id_Proveedor# , #de_Factura#, #cl_TipoFactura#, #id_EstatusFactura#
		</cfquery>
		<cfset ContraRecibos.listado= TRUE>
		<cfset ContraRecibos.tipoError= ''>
		<cfset ContraRecibos.rs= RS_MostrarFacturas>
		<cfset ContraRecibos.mensaje= 'Se obtuvo el recordset de ContraRecibos correctamente'>
		<cfreturn ContraRecibos>
		<cfcatch type='database'>
			<cfset ContraRecibos.listado= FALSE>
			<cfset ContraRecibos.tipoError= 'database-indefinido'>
			<cfset ContraRecibos.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn ContraRecibos>
		</cfcatch>
	</cftry>  
</cffunction>
<!---*******************************************************FATURA "LIGADA" A LAS ORDENES DE COMPRA********************************--->
<!------>
<cffunction name="importes_OrdenesCompra" returntype="query" access="remote" output="yes">
	<cfargument name="id_Empresa"      required="no" type="string">
	<cfargument name="id_Obra"         required="no" type="string">    
    <cfargument name="id_Proveedor"    required="no" type="string">

	<cfif #id_Obra# neq '' and #id_Empresa# neq '' and #id_Proveedor# neq ''>
		<cfquery datasource='#cnx#' name='rsPago' username='#user_sql#' password='#password_sql#'>
            EXECUTE Factura_OrdenesCompra #id_empresa# , #id_Obra#, #id_Proveedor#
		</cfquery>
		<cfset rs= QueryNew('id_OrdenCompra, de_OrdenCompra')>
		<cfset QueryAddRow(rs)>
		<cfset QuerySetCell(rs,"id_OrdenCompra","")>
		<cfset QuerySetCell(rs,"de_OrdenCompra","Seleccione una orden de compra")>
        <cfinvoke component="#Application.componentes#.funciones" method="JuntarQuerys" queryUno="#rs#" queryDos="#rsPago#">
    <cfelse>
		<cfset rs= QueryNew('id_OrdenCompra, de_OrdenCompra')>
		<cfset QueryAddRow(rs,1)>
		<cfset QuerySetCell(rs,"id_OrdenCompra","",1)>
		<cfset QuerySetCell(rs,"de_OrdenCompra","Seleccione una orden de compra",1)>
    </cfif>
	<cfreturn rs>
</cffunction>

<cffunction name="ObtenerDatosOC" returntype="query" access="public" output="yes">
	<cfargument name="id_Empresa"      required="no" type="string">
	<cfargument name="id_Obra"         required="no" type="string">    
    <cfargument name="id_OrdenCompra"    required="no" type="string">
    <cftry>
    	
    
		<cfquery datasource='#cnx#' name='rsOC' username='#user_sql#' password='#password_sql#'>
            EXECUTE ObtenerDatosOC #id_empresa# , #id_Obra#, #id_OrdenCompra#
		</cfquery>
		<cfset rsOC.listado= TRUE>
		<cfset rsOC.tipoError= ''>
		<cfset rsOC.mensaje= 'Se obtuvo el recordset de ContraRecibos correctamente'>
		<cfreturn rsOC>
		<cfcatch type='database'>
			<cfset rsOC.listado= FALSE>
			<cfset rsOC.tipoError= 'database-indefinido'>
			<cfset rsOC.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn rsOC>
		</cfcatch>
	</cftry>
	<cfreturn rsOC>
</cffunction>


<cffunction name='RSAgregarFacturaOrdenCompraDetalle' access='public' returntype='struct'>

	<cfargument name='id_Empresa' type='numeric' required='yes' default='0'>
	<cfargument name='id_Proveedor' type='numeric' required='yes' default='0'>
	<cfargument name='id_Obra' type='numeric' required='yes' default='0'>
	<cfargument name='id_OrdenCompra' type='numeric' required='yes' default='0'>
	<cfargument name='im_Factura' type='numeric' required='yes' default='0'>
	<cfargument name='id_EmpleadoRegistro' type='numeric' required='yes' default='0'>
	<cfargument name='id_ContrareciboFact' type='numeric' required='yes' default='0'>
	<cfargument name='im_OrdenCOmpra' type='numeric' required='yes' default='0'>
	<cfargument name='de_Factura' type='string' required='yes' default='0'>
	<cfargument name='fh_Factura' type='date' required='yes' default='0'>
	

  	<cfif fh_Factura NEQ "">
       <cfset fh_Factura = #DateFormat(fh_Factura,'yyyy-mm-dd')#>
  	</cfif>

	<!--- <cfoutput>EXECUTE Factura_OrdenesCompraAgregarDetalle #id_Empresa#, #id_Proveedor#, #id_Obra#, #id_OrdenCompra#, '#de_Factura#', '#fh_Factura#',#session.id_Empleado#,#id_ContrareciboFact#,#im_OrdenCOmpra#</cfoutput> --->
	<cftry>
		<cfquery datasource='#cnx#' name='RSAgregar' username='#user_sql#' password='#password_sql#'>
			EXECUTE Factura_OrdenesCompraAgregarDetalle #id_Empresa#, #id_Proveedor#, #id_Obra#, #id_OrdenCompra#, '#de_Factura#', '#fh_Factura#',#session.id_Empleado#,#id_ContrareciboFact#,#im_OrdenCOmpra#
		</cfquery>
		<cfset Factura.agregado= TRUE>
		<cfset Factura.tipoError= ''>
		<cfset Factura.mensaje= 'Detalle de Contrarecibo guardado correctamente'>
		<cfreturn Factura>
		<cfcatch type='database'>
			<cfset Factura.agregado= FALSE>
			<!--- <cfif cfcatch.NativeErrorCode EQ '2627'>
				<cfset Factura.tipoError= 'database-registro_duplicado'>
				<cfset Factura.mensaje= 'El Detalle de Contrarecibo no se pudo guardar porque ya existe'>
			<cfelse> --->
				<cfset Factura.tipoError= 'database-indefinido'>
				<cfset Factura.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<!--- </cfif> --->
			<cfreturn Factura>
		</cfcatch>
	</cftry>

</cffunction>


<cffunction name='RSMostrarFacturasOrdenesCompra' access='public' returntype='struct'>

	<cfargument name='id_Empresa' type='string' required='no'>
	<cfargument name='id_Obra' type='string' required='no'>
	<cfargument name='folio' type='string' required='no' default="">
	<cfargument name='id_Proveedor' type='string' required='no' default="">
	<cfargument name='fh_ContraReciboInicial' type='string' required='no' default="">
	<cfargument name='fh_ContraReciboFinal' type='string' required='no' default="">

	
	<cfif id_Empresa EQ "">
		<cfset id_Empresa = "NULL">
	</cfif>

	<cfif id_Obra EQ "">
		<cfset id_Obra = "NULL">
	</cfif>

	<cfif folio EQ "">
		<cfset folio = "NULL">
	<cfelse>
		<cfset folio = de("#folio#")>
	</cfif>
	<cfif id_Proveedor EQ "">
		<cfset id_Proveedor = "NULL">
	</cfif>
	
	<cfif fh_ContrareciboInicial NEQ "">
		<cfset fh_ContrareciboInicial = RIGHT(fh_ContrareciboInicial,4) & '/' & MID(fh_ContrareciboInicial,4,2) & '/' & LEFT(fh_ContrareciboInicial,2)>
		<cfset fh_ContrareciboInicial = DE(#fh_ContrareciboInicial#)>
	<cfelse>
		<cfset fh_ContrareciboInicial = 'null'>
	</cfif>
	<cfif fh_ContraReciboFinal NEQ "">
		<cfset fh_ContraReciboFinal = RIGHT(fh_ContraReciboFinal,4) & '/' & MID(fh_ContraReciboFinal,4,2) & '/' & LEFT(fh_ContraReciboFinal,2)>
		<cfset fh_ContraReciboFinal = DE(#fh_ContraReciboFinal#)>
	<cfelse>
		<cfset fh_ContraReciboFinal = 'null'>
	</cfif>

	


	<cftry>
		<cfquery datasource='#cnx#' name='RS_MostrarFacturasBD' username='#user_sql#' password='#password_sql#'>
			EXECUTE Facturas_OrdenesCompra_Listado #id_Empresa#, #id_Obra#, #id_Proveedor#, #folio#, #fh_ContrareciboInicial#, #fh_ContrareciboFinal#
		</cfquery>
		<cfset RS_MostrarFacturas.listado= TRUE>
		<cfset RS_MostrarFacturas.tipoError= ''>
		<cfset RS_MostrarFacturas.rs= RS_MostrarFacturasBD>
		<cfset RS_MostrarFacturas.mensaje= 'Se obtuvo el recordset de Facturas correctamente'>
		<cfreturn RS_MostrarFacturas>
		<cfcatch type='database'>
			<cfset RS_MostrarFacturas.listado= FALSE>
			<cfset RS_MostrarFacturas.tipoError= 'database-indefinido'>
			<cfset RS_MostrarFacturas.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn RS_MostrarFacturas>
		</cfcatch>
	</cftry>
</cffunction>

<cffunction name='RSAgregarFacturaOrdenCompra' access='public' returntype='struct'>

	<cfargument name='id_Empresa' type='numeric' required='yes' default='0'>
	<cfargument name='id_Proveedor' type='numeric' required='yes' default='0'>
	<cfargument name='de_Factura' type='string' required='yes' default=''>
	<cfargument name='fh_Factura' type='date' required='yes' default=''>
	<cfargument name='im_Factura' type='numeric' required='yes' default='0'>
	<cfargument name='rutaXML' type='string' required='yes' default=''>

  	<cfif fh_Factura NEQ "">
       <cfset fh_Factura = RIGHT(fh_Factura,4) & '/' & MID(fh_Factura,4,2) & '/' & LEFT(fh_Factura,2)>
  	</cfif>
	<cftry>
		<cfquery datasource='#cnx#' name='RSAgregar' username='#user_sql#' password='#password_sql#'>
			EXECUTE Factura_OrdenesCompraAgregar #id_Empresa#, #id_Proveedor#, '#de_Factura#', '#fh_Factura#', #im_Factura#,  #session.id_Empleado#, '#rutaXML#'
		</cfquery>
		<cfset Factura.agregado= TRUE>
		<cfset Factura.tipoError= ''>
		<cfset Factura.mensaje= 'Detalle de Contrarecibo guardado correctamente'>
		<cfset Factura.id_ContrareciboFact= RSAgregar.id_ContrareciboFact>
		<cfreturn Factura>
		<cfcatch type='database'>
			<cfset Factura.agregado= FALSE>
			<!--- <cfif cfcatch.NativeErrorCode EQ '2627'>
				<cfset Factura.tipoError= 'database-registro_duplicado'>
				<cfset Factura.mensaje= 'El Detalle de Contrarecibo no se pudo guardar porque ya existe'>
			<cfelse> --->
				<cfset Factura.tipoError= 'database-indefinido'>
				<cfset Factura.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<!--- </cfif> --->
			<cfreturn Factura>
		</cfcatch>
	</cftry>

</cffunction>


<cffunction name='RSMostrarDatosFacturaOC' access='public' returntype='struct'>

	<cfargument name='id_Empresa' type='string' required='no'>
	<cfargument name='id_ContraReciboFact' type='string' required='no' default="">
	
	<cftry>
		<cfquery datasource='#cnx#' name='RS_MostrarFacturasBD' username='#user_sql#' password='#password_sql#'>
			EXECUTE ObtenerDatos_Facturas_OrdenesCompra #id_Empresa#, #id_ContrareciboFact#
		</cfquery>
		<cfset RS_MostrarFacturas.listado= TRUE>
		<cfset RS_MostrarFacturas.tipoError= ''>
		<cfset RS_MostrarFacturas.rs= RS_MostrarFacturasBD>
		<cfset RS_MostrarFacturas.mensaje= 'Se obtuvo el recordset de Facturas correctamente'>
		<cfreturn RS_MostrarFacturas>
		<cfcatch type='database'>
			<cfset RS_MostrarFacturas.listado= FALSE>
			<cfset RS_MostrarFacturas.tipoError= 'database-indefinido'>
			<cfset RS_MostrarFacturas.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn RS_MostrarFacturas>
		</cfcatch>
	</cftry>
</cffunction>

<cffunction name='RSMostrarDatosFacturaOCDetalle' access='public' returntype='struct'>

	<cfargument name='id_Empresa' type='string' required='no'>
	<cfargument name='id_ContraReciboFact' type='string' required='no' default="">
	
	<cftry>
		<cfquery datasource='#cnx#' name='RS_MostrarFacturasBD' username='#user_sql#' password='#password_sql#'>
			EXECUTE ObtenerDatos_Facturas_OrdenesCompraDetalle #id_Empresa#, #id_ContrareciboFact#
		</cfquery>
		<cfset RS_MostrarFacturas.listado= TRUE>
		<cfset RS_MostrarFacturas.tipoError= ''>
		<cfset RS_MostrarFacturas.rs= RS_MostrarFacturasBD>
		<cfset RS_MostrarFacturas.mensaje= 'Se obtuvo el recordset de Facturas correctamente'>
		<cfreturn RS_MostrarFacturas>
		<cfcatch type='database'>
			<cfset RS_MostrarFacturas.listado= FALSE>
			<cfset RS_MostrarFacturas.tipoError= 'database-indefinido'>
			<cfset RS_MostrarFacturas.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn RS_MostrarFacturas>
		</cfcatch>
	</cftry>
</cffunction>


<cffunction name='RSEditarFacturaOrdenCompraDetalle' access='public' returntype='struct'>

	<cfargument name='id_Empresa' type='numeric' required='yes' default='0'>
	<cfargument name='id_Proveedor' type='numeric' required='yes' default='0'>
	<cfargument name='id_Obra' type='numeric' required='yes' default='0'>
	<cfargument name='id_OrdenCompra' type='numeric' required='yes' default='0'>
	<cfargument name='im_Factura' type='numeric' required='yes' default='0'>
	<cfargument name='id_EmpleadoRegistro' type='numeric' required='yes' default='0'>
	<cfargument name='id_ContrareciboFact' type='numeric' required='yes' default='0'>
	<cfargument name='im_OrdenCOmpra' type='numeric' required='yes' default='0'>
	<cfargument name='de_Factura' type='string' required='yes' default='0'>
	<cfargument name='fh_Factura' type='date' required='yes' default='0'>

  	<cfif fh_Factura NEQ "">
       <cfset fh_Factura = #DateFormat(fh_Factura,'yyyy-mm-dd')#>
  	</cfif>


	<cftry>
		<cfquery datasource='#cnx#' name='RSAgregar' username='#user_sql#' password='#password_sql#'>
			EXECUTE Factura_OrdenesCompraAgregarDetalle #id_Empresa#, #id_Proveedor#, #id_Obra#, #id_OrdenCompra#, '#de_Factura#', '#fh_Factura#',#session.id_Empleado#,#id_ContrareciboFact#,#im_OrdenCOmpra#
		</cfquery>
		<cfset Factura.agregado= TRUE>
		<cfset Factura.tipoError= ''>
		<cfset Factura.mensaje= 'Detalle de Contrarecibo guardado correctamente'>
		<cfreturn Factura>
		<cfcatch type='database'>
			<cfset Factura.agregado= FALSE>
			<!--- <cfif cfcatch.NativeErrorCode EQ '2627'>
				<cfset Factura.tipoError= 'database-registro_duplicado'>
				<cfset Factura.mensaje= 'El Detalle de Contrarecibo no se pudo guardar porque ya existe'>
			<cfelse> --->
				<cfset Factura.tipoError= 'database-indefinido'>
				<cfset Factura.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<!--- </cfif> --->
			<cfreturn Factura>
		</cfcatch>
	</cftry>

</cffunction>

<cffunction name='RSEliminarFacturaOrdenCompra' access='public' returntype='struct'>

	<cfargument name='id_Empresa' type='numeric' required='yes'>
	<cfargument name='id_ContrareciboFact' type='numeric' required='yes'>

	<cftry>
		<cfquery datasource='#cnx#' name='RSFilasAfectadas' username='#user_sql#' password='#password_sql#'>
			EXECUTE Factura_OrdenesCompra_Eliminar #id_Empresa#, #id_ContrareciboFact#
		</cfquery>
		<cfset FacturaOrdenCompra.eliminado= TRUE>
		<cfset FacturaOrdenCompra.tipoError= ''>
		<cfset FacturaOrdenCompra.mensaje= 'Eliminado correctamente'>
		<cfreturn FacturaOrdenCompra>
		<cfcatch type='database'>
			<cfset FacturaOrdenCompra.eliminado= FALSE>
			<cfif cfcatch.NativeErrorCode EQ '547'>
				<cfset FacturaOrdenCompra.mensaje= 'No se pudo eliminar ya que tiene registros relacionados'>
				<cfset FacturaOrdenCompra.tipoError= 'database-integridad'>
			<cfelse>
				<cfset FacturaOrdenCompra.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
				<cfset FacturaOrdenCompra.tipoError= 'database-indefinido'>
			</cfif>
			<cfreturn FacturaOrdenCompra>
		</cfcatch>
	</cftry>
</cffunction>
<!---**********************************GUARDAR XML Y PDF DE FACTURA***********************************************************--->
<!---FUNCION PARA AGREGAR LA RUTA DEL PDF A LA BASE DE DATOS--->
<cffunction name="RSfacturasAgregarPDF" access="public" returntype="struct">
	<cfargument name="id_Contrarecibo" type="string" required="yes">
	<cfargument name="id_Empresa" type="string" required="yes">
	<cfargument name="de_ruta" type="string" required="no" default="">

	<cftry>
			<cfquery datasource='#cnx#' name="RS">
				EXECUTE Facturas_OrdenesCompra_GuardarRutaPDF #id_Empresa#, #id_Contrarecibo#, '#de_ruta#'
			</cfquery>

			<cfset PDF.agregado= TRUE>
			<cfset PDF.tipoError= ''>
			<cfset PDF.mensaje= 'PDF guardado correctamente'>
			<cfreturn PDF>
		<cfcatch type='database'>
			<cfset PDF.agregado= FALSE>
			<cfset PDF.tipoError= 'database-indefinido'>
			<cfset PDF.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfabort>
			<cfreturn PDF>
		</cfcatch>
	</cftry>
</cffunction>

<cffunction name="getRFCProveedor" access="remote" returntype="string">
	<cfargument name="id_Proveedor" required="yes" type="string">	
	<cfif #id_Proveedor# EQ "">
		<cfreturn "">
	</cfif>
	<cftry>
		<cfquery datasource='#cnx#' name='RSProveedor'>
			SELECT
				de_RFC
			FROM
				Proveedores
			WHERE
				id_Proveedor = #id_Proveedor#
		</cfquery>
		<cfreturn RSProveedor.de_RFC>
		<cfcatch type='database'>
			<cfreturn "">
		</cfcatch>
	</cftry>
</cffunction>

<cffunction name="getRFCEmpresa" access="remote" returntype="string">
	<cfargument name="id_empresa" required="yes" type="string">	
	<cfif #id_empresa# EQ "">
		<cfreturn "">
	</cfif>
	<cftry>
		<cfquery datasource='#cnx#' name='RSEmpresa'>
			SELECT
				de_RFC
			FROM
				Empresas
			WHERE
				id_Empresa = #id_Empresa#
		</cfquery>
		<cfreturn RSEmpresa.de_RFC>
		<cfcatch type='database'>
			<cfreturn "">
		</cfcatch>
	</cftry>
</cffunction>
<!---*************************************************************************************************************************--->
<cffunction name='ContraRecibosDetalle_Actualizar_Pagar_Temporal' access='public' returntype='struct'>
	<cfargument name='id_Empresa' type='numeric' required='yes'>
    <cfargument name='id_Obra' type='numeric' required='yes'>
	<cfargument name='id_ContraRecibo' type='numeric' required='yes' default="">
	<cfargument name='nd_ContraRecibo' type='numeric' required='yes' default="">
	<cfargument name='id_EstatusFactura' type='numeric' required='yes' default="">	
	<cfargument name='fh_Pago' type='string' required='no' default="null">
    
	<cfif fh_Pago NEQ 'null'>
    	<cfset fh_Pago = (Right(fh_Pago,4) & '-' & Mid(fh_Pago,4,2) & '-' & Left(fh_Pago,2))>
	
	</cfif>

	<cftry>

		<cfquery datasource='#cnx#' name='RS_MostrarContraRecibos'>
			EXECUTE ContraRecibosDetalle_MarcarFactura_Pagar_Temporal #id_Empresa#, #id_ContraRecibo#, #nd_ContraRecibo#,#id_EstatusFactura#, '#fh_Pago#'
		</cfquery>
		<!---Log--->
		<!--- <cf_log datasource='#cnx#' username='#user_sql#' password='#password_sql#' id_Empresa="#id_Empresa#" id_Obra="#id_Obra#" id_Usuario="#Session.id_Usuario#" id_MenuOpcion="#Session.id_MenuOpcion#">
			<cfoutput>
                EXECUTE ContraRecibosDetalle_MarcarFactura_Pagar 
                        #id_Empresa#, #id_ContraRecibo#, #nd_ContraRecibo#,#id_EstatusFactura#, #fh_Pago#
			</cfoutput>
		</cf_log> --->
		<!---Termina Log --->
		<cfset ContraRecibosDetalle.listado= TRUE>
		<cfset ContraRecibosDetalle.tipoError= ''>
		<cfset ContraRecibosDetalle.mensaje= 'Se obtuvo el recordset de ContraRecibos correctamente'>
		<cfreturn ContraRecibosDetalle>
		<cfcatch type='database'>
			<cfset ContraRecibosDetalle.listado= FALSE>
			<cfset ContraRecibosDetalle.tipoError= 'database-indefinido'>
			<cfset ContraRecibosDetalle.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn ContraRecibosDetalle>
		</cfcatch>
	</cftry>
</cffunction>
   <cffunction name="RSListadoFacturas_CancelarPagar" access="public" returntype="struct">
    <cfargument name="id_Empresa" 	 		type="string" required="no" default="">
	<cfargument name="id_Obra" 				type="string" required="no" default="">
    <cfargument name="id_Proveedor" 		type="string" required="no">
	<cfargument name="de_Factura" 	  		type="string" required="no">
   	
	<cfif #id_Empresa# EQ "">
		<cfset id_Empresa = 'NULL'>
	</cfif>
	<cfif #id_Obra# EQ "">
		<cfset id_Obra = 'NULL'>
	</cfif>
	<cfif #id_Proveedor# EQ "">
		<cfset id_Proveedor = 'NULL'>
	</cfif>
	<cfif #de_Factura# EQ "">
		<cfset de_Factura = 'NULL'>
	<cfelse>
		<cfset de_Factura = DE(de_Factura)>
	</cfif>       
	<cftry>
	
		<cfquery datasource='#session.cnx#' name='RS_MostrarFacturas' >
			EXECUTE ContraRecibosDetalle_ObtenerListado_CancelarPagar #id_Empresa#, #id_Obra# ,#id_Proveedor# , #de_Factura#
		</cfquery>
		<cfset ContraRecibos.listado= TRUE>
		<cfset ContraRecibos.tipoError= ''>
		<cfset ContraRecibos.rs= RS_MostrarFacturas>
		<cfset ContraRecibos.mensaje= 'Se obtuvo el recordset de ContraRecibos correctamente'>
		<cfreturn ContraRecibos>
		<cfcatch type='database'>
			<cfset ContraRecibos.listado= FALSE>
			<cfset ContraRecibos.tipoError= 'database-indefinido'>
			<cfset ContraRecibos.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn ContraRecibos>
		</cfcatch>
	</cftry>  
</cffunction>

</cfcomponent>