<cfcomponent extends='conexion'>
  <cffunction name="reporte_desglosado_saldos" access="public" returntype="struct">
        <cfargument name="id_Empresa"    type="string" default="">
        <cfargument name="id_Obra"       type="string" default="">
        <cfargument name="id_Agrupador"  type="string" default="">

       <cftry>
        
            <cfquery name="RS" datasource="#cnx#"  username='#user_sql#' password='#password_sql#'>
              
                    EXEC reporte_desglosado_saldos #id_Empresa#,<cfif #id_Obra# NEQ ''>
           															#id_Obra#
           														<cfelse>
           															NULL   
        														</cfif>, 
        														<cfif #id_Agrupador# NEQ ''>
           															'#id_Agrupador#'
           														<cfelse>
           															NULL   
        														</cfif>
        	</cfquery>

            <cfset myResult.SUCCESS = true> 
            <cfset myResult.RS = Rs>
            <cfcatch type="database">
                <cfset myResult.SUCCESS = false>    
                <cfset myResult.MESSAGE = 'Error en acceso a base de datos, #cfcatch.Detail#'>
            </cfcatch>  
        </cftry>
        <cfreturn myResult>
</cffunction>
<cffunction name="Ingresos_Obra" access="public" returntype="struct">
    <cfargument name="id_Empresa"    type="string" default="">
    <cfargument name="id_Obra"       type="string" default="">


   <cftry>
    
        <cfquery name="RS" datasource="#cnx#"  username='#user_sql#' password='#password_sql#'>
          
                EXEC INGRESOS_OBRA #id_Empresa#,<cfif #id_Obra# NEQ ''>
													#id_Obra#
												<cfelse>
													NULL 
												</cfif>	  
    														
    	</cfquery>

        <cfset myResult.SUCCESS = true> 
        <cfset myResult.RS = Rs>
        <cfcatch type="database">
            <cfset myResult.SUCCESS = false>    
            <cfset myResult.MESSAGE = 'Error en acceso a base de datos, #cfcatch.Detail#'>
        </cfcatch>  
    </cftry>
    <cfreturn myResult>
</cffunction>
<cffunction name="Ingresos_Diversos" access="public" returntype="struct">
    <cfargument name="id_Empresa"    type="string" default="">
    <cfargument name="id_Obra"       type="string" default="">


   <cftry>
    
        <cfquery name="RS" datasource="#cnx#"  username='#user_sql#' password='#password_sql#'>
          
                EXEC INGRESOS_DIVERSO #id_Empresa#,
       								   <cfif #id_Obra# NEQ ''>
											#id_Obra#
										<cfelse>
											NULL 
										</cfif>	  
    														
    	</cfquery>

        <cfset myResult.SUCCESS = true> 
        <cfset myResult.RS = Rs>
        <cfcatch type="database">
            <cfset myResult.SUCCESS = false>    
            <cfset myResult.MESSAGE = 'Error en acceso a base de datos, #cfcatch.Detail#'>
        </cfcatch>  
    </cftry>
    <cfreturn myResult>
</cffunction>


<cffunction name="reporte_libro_bancos" access="public" returntype="struct">
	<cfargument name="id_Obra"    		type="string" default="">
	<cfargument name="fh_inicio"    		type="string" default="">
	<cfargument name="fh_fin"       		type="string" default="">
	<cfargument name="id_banco"				type="string" default="">
	<cfargument name="id_CuentaBancaria"  	type="string" default="">

	<cfif #dateformat(fh_inicio,'dd')# GT 12>
		<cfset fh_inicio = #dateformat(fh_inicio,'yyyy-mm-dd')#>
	<cfelse>
		<cfset fh_inicio = #dateformat(fh_inicio,'yyyy-dd-mm')#>
	</cfif>
	<!---Fecha Fin--->
	<cfif #dateformat(fh_fin,'dd')# GT 12>
		<cfset fh_fin = #dateformat(fh_fin,'yyyy-mm-dd')#>
	<cfelse>
		<cfset fh_fin = #dateformat(fh_fin,'yyyy-dd-mm')#>
	</cfif>
	<cfif id_Banco EQ ''><cfset id_Banco = 'NULL'></cfif>
	<cfif id_CuentaBancaria EQ ''><cfset id_CuentaBancaria = 'NULL'></cfif>
	<cftry>
		<cfquery name="RS" datasource="#cnx#"  username='#user_sql#' password='#password_sql#'>
			EXECUTE	reporte_libro_bancos
			<cfif #id_Obra# EQ ''>
				NULL,
			<cfelse>
				#id_Obra#,    
			</cfif> 
			
			<cfif #fh_inicio# EQ ''>
				NULL,
			<cfelse>
				'#fh_inicio#',    
			</cfif>
			
			<cfif #fh_fin# EQ ''>
				NULL,
			<cfelse>
				'#fh_fin#',
			</cfif>
			#id_Banco#,
			#id_CuentaBancaria#
		</cfquery>
	<cfset myResult.SUCCESS = true> 
	<cfset myResult.RS = Rs>
	<cfcatch type="database">
		<cfset myResult.SUCCESS = false>    
		<cfset myResult.MESSAGE = 'Error en acceso a base de datos, #cfcatch.Detail#'>
	</cfcatch>  
	</cftry>
	<cfreturn myResult>
</cffunction>

<cffunction name='RSMostrarNextIDBancos' access='public' returntype='struct'>
	<!--- <cfargument name='id_Empresa' type='numeric' required='yes'> --->
	<cftry>
		<cfquery datasource='#cnx#' name='RS_MostrarNextIDBancos' username='#user_sql#' password='#password_sql#'>
			EXECUTE bop_Bancos_MostrarNextID 
		</cfquery>
		<cfset Bancos.NextID= TRUE>
		<cfset Bancos.tipoError= ''>
		<cfset Bancos.rs= RS_MostrarNextIDBancos>
		<cfset Bancos.mensaje= 'Se obtuvo el recordset de Bancos correctamente'>
		<cfreturn Bancos>
		<cfcatch type='database'>
			<cfset Bancos.NextID= FALSE>
			<cfset Bancos.tipoError= 'database-indefinido'>
			<cfset Bancos.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn Bancos>
		</cfcatch>
	</cftry>
</cffunction>


<cffunction name='RSMostrarPorIDBancos' access='public' returntype='struct'>
	<!--- <cfargument name='id_Empresa' type='numeric' required='yes'> --->
	<cfargument name='id_Banco' type='numeric' required='yes'>

	<cftry>
		<cfquery datasource='#cnx#' name='RS_MostrarBancos' username='#user_sql#' password='#password_sql#'>
			EXECUTE bop_Bancos_ObtenerPorID #id_Banco#
		</cfquery>
		<cfset Bancos.listado= TRUE>
		<cfset Bancos.tipoError= ''>
		<cfset Bancos.rs= RS_MostrarBancos>
		<cfset Bancos.mensaje= 'Se obtuvo el recordset de Bancos correctamente'>
		<cfreturn Bancos>
		<cfcatch type='database'>
			<cfset Bancos.listado= FALSE>
			<cfset Bancos.tipoError= 'database-indefinido'>
			<cfset Bancos.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn Bancos>
		</cfcatch>
	</cftry>
</cffunction>

<cffunction name='cuentas_Bancarias' access="remote" returntype="string" returnformat="JSON">
  <cfargument name="id_banco" type="string" default="" required="yes">
  <cftry>
        <cfquery datasource='#cnx#' name='RSCuentas' username='#user_sql#' password='#password_sql#'>
          	SELECT id_cuentaBancaria,nu_cuentaBancaria as de_bancaria,nu_clabe,de_cuentaBancaria,nu_cuentaBancaria 
			FROM CuentasBancarias 
			WHERE id_banco=#id_banco#
        </cfquery>
        
  
		<cfinvoke component="#Application.componentes#/funciones" method="queryToJson" returnVariable="RS" query="#RSCuentas#">
      
      	<cfreturn RS>
        <cfcatch type='database'>
            <cfset error='error en el acceso a base de datos'>
            <cfreturn error>
        </cfcatch>
    </cftry>
</cffunction>

<cffunction name='getSaldos' access='remote' returntype='string' returnformat="JSON">
  <cfargument name="id_banco" type="string" default="" required="yes">
  <cfargument name="id_Cuenta" type="string" default="" required="yes">

  <cftry>

        <cfquery datasource='#cnx#' name='RSCuentas' username='#user_sql#' password='#password_sql#'>
          	 bop_cuentasBancariasMovimientos_calcularSaldo @id_banco=#id_banco#, @id_cuentaBancaria=#id_Cuenta#
        </cfquery>
		
		
		<cfinvoke component="#Application.componentes#/funciones" method="queryToJson" returnVariable="RS" query="#RSCuentas#">
      	
      	<cfreturn RS>
        <cfcatch type='database'>
            
          	<cfset error='error en el acceso a base de datos'>
            <cfreturn error>
        </cfcatch>
    </cftry>
</cffunction>
<cffunction name='MostrarOC_movimiento' access='remote' returntype='struct' returnformat="JSON">
  <cfargument name="id_banco" type="string" default="" required="yes">
  <cfargument name="id_CuentaBancaria" type="string" default="" required="yes">
  <cfargument name="id_Movimiento" type="string" default="" required="yes">
  <cfargument name="id_TipoMovimiento" type="string" default="" required="yes">


  <cftry>

        <cfquery datasource='#cnx#' name='RSOC' username='#user_sql#' password='#password_sql#'>
          	 Reporte_OrdenCompra_porCuenta
			      	  @id_banco=#id_banco#, 
			      	  @id_CuentaBancaria=#id_CuentaBancaria#, 
			      	  @id_Movimiento=#id_Movimiento#, 
			      	  @id_TipoMovimiento= #id_TipoMovimiento#
        </cfquery>
		
		
		<cfinvoke component="#Application.componentes#/funciones" method="queryToJson" returnVariable="RSOC" query="#RSOC#">
		<cfset RS.QUERY=RSOC>
      	<cfset RS.FLAG=true>
      	<cfreturn RS>
        <cfcatch type='database'>
            
          	<cfset error='error en el acceso a base de datos'>
            <cfreturn error>
        </cfcatch>
    </cftry>
</cffunction>
<cffunction name='MostrarFacturas_movimiento' access='remote' returntype='struct' returnformat="JSON">
  <cfargument name="id_Empresa" type="string" default="" required="yes">
  <cfargument name="id_Obra" type="string" default="" required="yes">
  <cfargument name="id_OrdenCompra" type="string" default="" required="yes">
 
  <cftry>
  	
        <cfquery datasource='#cnx#' name='RSOC' username='#user_sql#' password='#password_sql#'>
          	 Reporte_Factura_porCuenta
			      	  @id_Empresa=#id_Empresa#, 
			      	  @id_Obra=#id_Obra#, 
			      	  @id_OrdenCompra=#id_OrdenCompra#
			      	 
        </cfquery>
		
		
		<cfinvoke component="#Application.componentes#/funciones" method="queryToJson" returnVariable="RSOC" query="#RSOC#">
		<cfset RS.QUERY=RSOC>
      	<cfset RS.FLAG=true>
      	<cfreturn RS>
        <cfcatch type='database'>
            
          	<cfset error='error en el acceso a base de datos'>
            <cfreturn error>
        </cfcatch>
    </cftry>
</cffunction>
<cffunction name='MostrarInsumos_Factura' access='remote' returntype='struct' returnformat="JSON">
  <cfargument name="id_Empresa" type="string" default="" required="yes">
  <cfargument name="id_contraRecibo" type="string" default="" required="yes">
 
  <cftry>
  			
        <cfquery datasource='#cnx#' name='RSOC' username='#user_sql#' password='#password_sql#'>
          	  Reporte_Insumos_Factura
			      	  @ID_EMPRESA=#id_Empresa#, 
			      	  @ID_CONTRARECIBO=#id_contraRecibo#
			      	 
        </cfquery>
	
		
		<cfinvoke component="#Application.componentes#/funciones" method="queryToJson" returnVariable="RSOC" query="#RSOC#">
		<cfset RS.QUERY=RSOC>
      	<cfset RS.FLAG=true>
      	<cfreturn RS>
        <cfcatch type='database'>
            
          	<cfset error='error en el acceso a base de datos'>
            <cfreturn error>
        </cfcatch>
    </cftry>
</cffunction>
<cffunction name='getSaldosquery' access='public' returntype='query'>
  <cfargument name="id_banco" type="string" default="" required="yes">
  <cfargument name="id_Cuenta" type="string" default="" required="yes">

  <cftry>

        <cfquery datasource='#cnx#' name='RSCuentas' username='#user_sql#' password='#password_sql#'>
          	 bop_cuentasBancariasMovimientos_calcularSaldo @id_banco=#id_banco#, @id_cuentaBancaria=#id_Cuenta#
        </cfquery>
		
		<cfreturn RSCuentas>
        <cfcatch type='database'>
            
          	<cfset error='error en el acceso a base de datos'>
            <cfreturn error>
        </cfcatch>
    </cftry>
</cffunction>



<cffunction name='getBancos' access='public' returntype='query'>
	
  <cftry>
        <cfquery datasource='#cnx#' name='RSBancos' username='#user_sql#' password='#password_sql#'>
          SELECT 
			id_Banco,de_Banco
		  FROM 
			bancos
		  WHERE sn_activo=1	
        </cfquery>
        
  
        
        <cfreturn RSBancos>
        <cfcatch type='database'>
            <cfset error='error en el acceso a base de datos'>
            <cfreturn error>
        </cfcatch>
    </cftry>
</cffunction>



<cffunction name='RSMostrarDinamicoBancos' access='public' returntype='struct'>

	<cfargument name='id_Empresa' type='numeric' required='no' default='-1'>
	<cfargument name='id_Banco' type='numeric' required='no' default='-1'>
	<cfargument name='de_Banco' type='string' required='no' default=''>
	<cfargument name='de_BancoSucursal' type='string' required='no' default=''>
	<cfargument name='de_Direccion' type='string' required='no' default=''>
	<cfargument name='id_Ciudad' type='numeric' required='no' default='-1'>
	<cfargument name='nu_Telefono1' type='string' required='no' default=''>
	<cfargument name='nu_Telefono2' type='string' required='no' default=''>
	<cfargument name='nb_Responsable' type='string' required='no' default=''>

	<cftry>
		<cfset WhereDinamico=''>
		<cfif #id_Empresa# NEQ '-1'>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE id_Empresa = #id_Empresa#">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND id_Empresa = #id_Empresa#">
			</cfif>
		</cfif>
		<cfif #id_Banco# NEQ '-1'>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE id_Banco = #id_Banco#">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND id_Banco = #id_Banco#">
			</cfif>
		</cfif>
		<cfif #de_Banco# NEQ ''>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE de_Banco LIKE '#de_Banco#%'">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND de_Banco LIKE '#de_Banco#%'">
			</cfif>
		</cfif>
		<cfif #de_BancoSucursal# NEQ ''>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE de_BancoSucursal LIKE '#de_BancoSucursal#%'">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND de_BancoSucursal LIKE '#de_BancoSucursal#%'">
			</cfif>
		</cfif>
		<cfif #de_Direccion# NEQ ''>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE de_Direccion LIKE '#de_Direccion#%'">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND de_Direccion LIKE '#de_Direccion#%'">
			</cfif>
		</cfif>
		<cfif #id_Ciudad# NEQ '-1'>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE id_Ciudad = #id_Ciudad#">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND id_Ciudad = #id_Ciudad#">
			</cfif>
		</cfif>
		<cfif #nu_Telefono1# NEQ ''>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE nu_Telefono1 LIKE '#nu_Telefono1#%'">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND nu_Telefono1 LIKE '#nu_Telefono1#%'">
			</cfif>
		</cfif>
		<cfif #nu_Telefono2# NEQ ''>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE nu_Telefono2 LIKE '#nu_Telefono2#%'">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND nu_Telefono2 LIKE '#nu_Telefono2#%'">
			</cfif>
		</cfif>
		<cfif #nb_Responsable# NEQ ''>
			<cfif WhereDinamico EQ ''>
				<cfset WhereDinamico= "WHERE nb_Responsable LIKE '#nb_Responsable#%'">
			<cfelse>
				<cfset WhereDinamico= WhereDinamico & " AND nb_Responsable LIKE '#nb_Responsable#%'">
			</cfif>
		</cfif>

		<cfquery datasource='#cnx#' name='RS_MostrarBancos' username='#user_sql#' password='#password_sql#'>
			EXECUTE bop_Bancos_ObtenerDinamico '#WhereDinamico#'
		</cfquery>
		<cfset Bancos.listado= TRUE>
		<cfset Bancos.tipoError= ''>
		<cfset Bancos.rs= RS_MostrarBancos>
		<cfset Bancos.mensaje= 'Se obtuvo el recordset de Bancos correctamente'>
		<cfreturn Bancos>
		<cfcatch type='database'>
			<cfset Bancos.listado= FALSE>
			<cfset Bancos.tipoError= 'database-indefinido'>
			<cfset Bancos.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn Bancos>
		</cfcatch>
	</cftry>
</cffunction>


<cffunction name='RSMostrarTodosBancos' access='public' returntype='struct'>

	<cftry>
		<cfquery datasource='#cnx#' name='RS_MostrarBancos' username='#user_sql#' password='#password_sql#'>
			EXECUTE bop_Bancos_ObtenerTodos 
		</cfquery>
		<cfset Bancos.listado= TRUE>
		<cfset Bancos.tipoError= ''>
		<cfset Bancos.rs= RS_MostrarBancos>
		<cfset Bancos.mensaje= 'Se obtuvo el recordset de Bancos correctamente'>
		<cfreturn Bancos>
		<cfcatch type='database'>
			<cfset Bancos.listado= FALSE>
			<cfset Bancos.tipoError= 'database-indefinido'>
			<cfset Bancos.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn Bancos>
		</cfcatch>
	</cftry>
</cffunction>

<cffunction name='RSMostrarTodosBancosCuentasBancarias' access='public' returntype='struct'>

	<cftry>
		<cfquery datasource='#session.cnx#' name='RS'>
			EXECUTE bop_BancosCentasBancarias_ObtenerTodos 
		</cfquery>
		<cfset Bancos.listado= TRUE>
		<cfset Bancos.tipoError= ''>
		<cfset Bancos.rs= RS>
		<cfset Bancos.mensaje= 'Se obtuvo el recordset de Bancos correctamente'>
		<cfreturn Bancos>
		<cfcatch type='database'>
			<cfset Bancos.listado= FALSE>
			<cfset Bancos.tipoError= 'database-indefinido'>
			<cfset Bancos.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn Bancos>
		</cfcatch>
	</cftry>
</cffunction>

<cffunction name='Obeter_Banco_CuentaBancaria' access='public' returntype='struct'>
	<cfargument name="formulario" type="any" required="yes" default="">
	<cftry>
		<cfquery datasource='#session.cnx#' name='RS'>
			EXECUTE bop_BancosCentasBancarias_ObtenerTodos 
		</cfquery>
		<cfoutput query="RS">
			<cfif formulario.id_cuentaBancariaOrigen EQ ID_CUENTABANCARIARANK>
				<cfset datos.id_bancoOrigen = RS.id_banco>
				<cfset datos.id_cuentaBancariaOrigen = RS.id_cuentaBancaria>
			</cfif>
			<cfif formulario.id_cuentaBancariaDestino EQ ID_CUENTABANCARIARANK>
				<cfset datos.id_bancoDestino = RS.id_banco>
				<cfset datos.id_cuentaBancariaDestino = RS.id_cuentaBancaria>
			</cfif>			
		</cfoutput>

		<cfset datos.listado= TRUE>
		<cfset datos.tipoError= ''>
		<cfset datos.mensaje= 'Se obtuvo el recordset correctamente'>
		<cfreturn datos>
		<cfcatch type='database'>
			<cfset datos.listado= FALSE>
			<cfset datos.tipoError= 'database-indefinido'>
			<cfset datos.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn datos>
		</cfcatch>
	</cftry>
</cffunction>
<cffunction name="RSInserta_ordenCompra_diversos" access="public" returntype="struct">
	<cfargument name='id_Empresa' 		  type='numeric' required='yes' default='0'>
	<cfargument name="id_Obra"        	  type="string" required="true">
	<cfargument name="id_empleado"    	  type="string" required="true">
	<cfargument name="id_Almacen"     	  type="string" required="true">
	<cfargument name="id_Requisicion"     type="string" required="true">
	<cfif id_Requisicion EQ ''>
		<cfset id_Requisicion='NULL'>	

	</cfif>
	<cftransaction>
			
			<cfquery name="inserta_OC" datasource='#session.cnx#'>
				DECLARE @id_OrdenCompra INT,@id_EmpleadoSolicito INT
				SET @id_EmpleadoSolicito = (select top 1 id_Responsable from obras where id_Obra = #id_Obra#)
				SET @id_OrdenCompra = (select isnull(max(id_OrdenCompra)+1,1) from OrdenesCompra where id_Empresa = #id_Empresa# and id_Obra = #id_Obra#)
				INSERT INTO OrdenesCompra([id_Empresa],[id_Obra],[id_OrdenCompra],[id_Requisicion],[fh_OrdenCompra],[id_Proveedor],[id_EmpleadoComprador]
					,[id_EmpleadoSolicito],[id_EmpleadoAutorizo],[id_EmpleadoVoBo],[id_Almacen],[id_TipoOrdenCompra],[id_Moneda]
					,[im_TipoCambio],[im_SubTotal],[pj_IVA],[im_Total],[im_Ajustado],[id_Estatus],
					[nb_Recibe],
					[nu_Telefono],
					[de_Domicilio],
					[de_Comentarios],
					[id_MedioEnvio],
					[im_GastosManiobra],
					[pj_Anticipo],
					[im_Anticipo],
					[id_proveedor_contratista])
				VALUES(#id_Empresa#,#id_Obra#,@id_OrdenCompra,#id_Requisicion#,getDate(),0,
					#id_empleado#,@id_EmpleadoSolicito,#id_empleado#,#id_empleado#,#id_Almacen#,3,1,1.00,
					0.00,0.00,0.00,NULL,103,'', '','','',6,0.00,0.00,0.00,NULL)
				
				SELECT @id_OrdenCompra as id_OrdenCompra
			</cfquery>



	</cftransaction>
	<cfset RS.OK=TRUE>
	<cfset RS.id_OrdenCompra=inserta_OC.id_OrdenCompra>
	<cfreturn RS>
</cffunction>
<cffunction name="RSInserta_ordenCompradetalle_diversos" access="public" returntype="struct">
	<cfargument name='id_Empresa' 			 type='numeric' required='yes' default='0'>
	<cfargument name="id_OrdenCompra"        type="string" required="true">
	<cfargument name="id_Obra"       		 type="string" required="true">
	<cfargument name="id_empleado"   		 type="string" required="true">
	<cfargument name="id_insumo"     		 type="string" required="true">
	<cfargument name="im_Precio"     		 type="string" required="true">
	<cfargument name="cantidad"     		 type="string" required="true">

	<cftransaction>
			
		<cfquery name="inserta_OCD" datasource='#session.cnx#'>
			DECLARE @nd_OrdenCompra INT
			SET @nd_OrdenCompra = (select isnull(max(nd_OrdenCompra)+1,1) from OrdenesCompraDetalle
			where id_Empresa = #id_Empresa# and id_Obra = #id_Obra# and id_OrdenCompra = #id_OrdenCompra#)	
			
			INSERT INTO OrdenesCompraDetalle
				([id_Empresa],[id_Obra],[id_OrdenCompra],[nd_OrdenCompra],[id_CentroCosto],[id_Insumo],[fh_Entrega],
				[nu_Cantidad],[nu_CantidadAjustada],[im_Precio],[im_PrecioPresupuesto],[im_CantidadPrecio],[id_Marca], [id_Frente], [id_Partida], de_Detalle)
			VALUES
				(#id_Empresa#,#id_Obra#,#id_OrdenCompra#,@nd_OrdenCompra,900,'#id_Insumo#',getdate(),
				(#cantidad#),0,#im_Precio#,NULL, (#cantidad#) * #im_Precio#,NULL, 901 , 1, '')	
				
			SELECT * FROM OrdenesCompraDetalle WHERE id_Empresa=#id_Empresa# AND id_Obra=#id_Obra# AND id_OrdenCompra=#id_OrdenCompra# 		
		</cfquery>
		<cfquery name="requisicion" datasource='#session.cnx#'>
			SELECT id_requisicion
			FROM OrdenesCompra
			WHERE id_Empresa=#id_Empresa# AND id_Obra=#id_Obra# AND id_OrdenCompra=#id_OrdenCompra# 		
				
		</cfquery>
		<cfquery name="nd_detalle" datasource='#session.cnx#'>
							SELECT 
								ISNULL(MAX(nd_requisicion)+1,1) as nd_requisicion 
							FROM 
								Requisiciones_OrdenesCompra
							WHERE id_Empresa=#id_Empresa# AND id_Obra=#id_Obra# AND id_OrdenCompra=#id_OrdenCompra#
		</cfquery>	

		<cfquery name="inserta_nu_por_comprar" datasource='#session.cnx#'>
								INSERT INTO Requisiciones_OrdenesCompra
									(
									[id_Empresa],
									[id_Obra],
									[id_OrdenCompra],
									[id_requisicion],
									[nd_requisicion],
									[id_insumo],
									[nu_comprado],
									[id_centroCosto],
									[id_Frente],
									[id_Partida],
									[im_Precio],
									[id_Estatus]
									)
								VALUES(	
									#id_Empresa#,
									#id_Obra#,
									#id_OrdenCompra#,
									#requisicion.id_requisicion#,
									#nd_detalle.nd_requisicion#,
									'#id_insumo#',
									#cantidad#,
									900,
									901,
									1,
									#im_Precio#,
									103
									)
			</cfquery>
			
			

	</cftransaction>
	<cfset RS.OK=TRUE>
	<cfset RS.QUERY=inserta_OCD>
	<cfreturn RS>





</cffunction>
<cffunction name="RS_actualizaTotales_OC" access="public" returntype="struct">
	<cfargument name='id_Empresa' type='numeric' required='yes' default='0'>
	<cfargument name="id_OrdenCompra"        type="string" required="true">
	<cfargument name="id_Obra"        type="string" required="true">
	
	<cftransaction>
			
		<cfquery name="inserta_OCD"  datasource='#session.cnx#'>
			DECLARE @im_SubTotal FLOAT=(select sum(im_CantidadPrecio) from OrdenesCompraDetalle where id_OrdenCompra=#id_OrdenCompra# and id_Obra=#id_Obra# AND id_Empresa=#id_Empresa#)

			UPDATE 
				OrdenesCompra
			SET
				im_SubTotal=@im_SubTotal,im_Total=@im_SubTotal
			WHERE
				id_Obra=#id_Obra# AND id_Empresa=#id_Empresa# AND id_OrdenCompra=#id_OrdenCompra#	

			
			SELECT * from OrdenesCompraDetalle 	WHERE
				id_Obra=#id_Obra# AND id_Empresa=#id_Empresa# AND id_OrdenCompra=#id_OrdenCompra#	
				
		</cfquery>
			

	</cftransaction>
	<cfset RS.OK=TRUE>
	<cfset RS.QUERY=inserta_OCD>
	<cfreturn RS>
</cffunction>
<cffunction name='RSMostrarNextIDContraRecibos' access='public' returntype='struct'>

	<cfargument name='id_Empresa' type='numeric' required='yes'>

	<cftry>
		<cfquery name='RS_MostrarNextIDContraRecibos' datasource='#session.cnx#'>
			EXECUTE bop_ContraRecibos_MostrarNextID #id_Empresa#
		</cfquery>
		<cfset ContraRecibos.NextID= TRUE>
		<cfset ContraRecibos.tipoError= ''>
		<cfset ContraRecibos.rs= RS_MostrarNextIDContraRecibos>
		<cfset ContraRecibos.mensaje= 'Se obtuvo el recordset de ContraRecibos correctamente'>
		<cfreturn ContraRecibos>
		<cfcatch type='database'>
			<cfset ContraRecibos.NextID= FALSE>
			<cfset ContraRecibos.tipoError= 'database-indefinido'>
			<cfset ContraRecibos.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn ContraRecibos>
		</cfcatch>
	</cftry>
</cffunction>
<cffunction name='RSAgregarContraRecibos' access='public' returntype='struct'>

	<cfargument name='id_Empresa' type='numeric' required='yes' default='0'>
	<cfargument name='id_ContraRecibo' type='numeric' required='yes' default='0'>
	<cfargument name='fh_ContraRecibo' type='date' required='yes' default=''>
	<cfargument name='id_Proveedor' type='numeric' required='yes' default='0'>
	<cfargument name='id_Estatus' type='numeric' required='no' default='0'>

	<cftry>
		<cfquery name='RSAgregar' datasource='#session.cnx#'>
			EXECUTE bop_ContraRecibos_Agregar #id_Empresa#, #id_ContraRecibo#, '#fh_ContraRecibo#', #id_Proveedor#, #id_Estatus#
		</cfquery>
		<cfset ContraRecibos.agregado= TRUE>
		<cfset ContraRecibos.tipoError= ''>
		<cfset ContraRecibos.mensaje= 'ContraRecibo guardado correctamente'>
		<cfreturn ContraRecibos>
		<cfcatch type='database'>
			<cfset ContraRecibos.agregado= FALSE>
			<cfif cfcatch.NativeErrorCode EQ '2627'>
				<cfset ContraRecibos.tipoError= 'database-registro_duplicado'>
				<cfset ContraRecibos.mensaje= 'El ContraRecibo no se pudo guardar porque ya existe'>
			<cfelse>
				<cfset ContraRecibos.tipoError= 'database-indefinido'>
				<cfset ContraRecibos.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			</cfif>
			<cfreturn ContraRecibos>
		</cfcatch>
	</cftry>
</cffunction>
<cffunction name='RSAgregarContraRecibosDetalle' access='public' returntype='struct'>

	<cfargument name='id_Empresa' type='numeric' required='yes' default='0'>
	<cfargument name='id_ContraRecibo' type='numeric' required='yes' default='0'>
	<cfargument name='nd_ContraRecibo' type='numeric' required='yes' default='0'>
	<cfargument name='id_Proveedor' type='numeric' required='yes' default='0'>
	<cfargument name='id_Obra' type='numeric' required='yes' default='0'>
	<cfargument name='id_OrdenCompra' type='numeric' required='yes' default='0'>
	<cfargument name='de_Factura' type='string' required='yes' default=''>
	<cfargument name='fh_Factura' type='date' required='yes' default=''>
	<cfargument name='fh_FacturaVencimiento' type='string' required='no' default=''>
	<cfargument name='id_Modulo' type='numeric' required='yes' default='0'>
	<cfargument name='id_TipoMovimiento' type='string' default='0'>
	<cfargument name='id_CentroCosto' type='string' default='0'>
	<cfargument name='de_Concepto' type='string' required='no' default=''>
	<cfargument name='de_Referencia' type='string' required='no' default=''>
	<cfargument name='im_Factura' type='numeric' required='yes' default='0'>
	<cfargument name='id_Moneda' type='numeric' required='yes' default='0'>
	<cfargument name='im_TipoCambio' type='numeric' required='yes' default='0'>
	<cfargument name='cl_FormaPago' type='string' required='no' default=''>
	<cfargument name='id_Estatus' type='numeric' required='yes' default='0'>
	<cfargument name='id_EmpleadoRegistro' type='numeric' required='yes' default='0'>
	<cfargument name='id_EmpleadoBloqueo' type='string' required='no' default=''>
	<cfargument name='id_EmpleadoAutorizo' type='string' required='no' default=''>
	<cfargument name='id_EstatusFactura' type='string' required='no' default=''>
	<cfif cl_FormaPago NEQ "">
		<cfset cl_FormaPago = DE(#cl_FormaPago#)>
	<cfelse>
		<cfset cl_FormaPago = 'null'>
	</cfif>
	<cfif id_EmpleadoBloqueo EQ "">
		<cfset id_EmpleadoBloqueo = 'null'>
	</cfif>
	<cfif id_EmpleadoAutorizo EQ "">
		<cfset id_EmpleadoAutorizo = 'null'>
	</cfif>
	<cfif id_EstatusFactura EQ "">
		<cfset id_EstatusFactura = 'null'>
	</cfif>
	<cftry>
		<cfquery name='RSAgregar' datasource='#session.cnx#'>
			EXECUTE bop_ContraRecibosDetalle_Agregar #id_Empresa#, #id_ContraRecibo#, #nd_ContraRecibo#, #id_Proveedor#, #id_Obra#, #id_OrdenCompra#, '#de_Factura#', '#fh_Factura#', #fh_FacturaVencimiento#, #id_Modulo#, #id_TipoMovimiento#, #id_CentroCosto#, '#de_Concepto#', '#de_Referencia#', #im_Factura#, #id_Moneda#, #im_TipoCambio#, #cl_FormaPago#, #id_Estatus#, #id_EmpleadoRegistro#, #id_EmpleadoBloqueo#, #id_EmpleadoAutorizo#,#id_EstatusFactura#
		</cfquery>
		<cfset ContraRecibosDetalle.agregado= TRUE>
		<cfset ContraRecibosDetalle.tipoError= ''>
		<cfset ContraRecibosDetalle.mensaje= 'Detalle de Contrarecibo guardado correctamente'>
		<cfreturn ContraRecibosDetalle>
		<cfcatch type='database'>
			<cfset ContraRecibosDetalle.agregado= FALSE>
			<cfif cfcatch.NativeErrorCode EQ '2627'>
				<cfset ContraRecibosDetalle.tipoError= 'database-registro_duplicado'>
				<cfset ContraRecibosDetalle.mensaje= 'El Detalle de Contrarecibo no se pudo guardar porque ya existe'>
			<cfelse>
				<cfset ContraRecibosDetalle.tipoError= 'database-indefinido'>
				<cfset ContraRecibosDetalle.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			</cfif>
			<cfreturn ContraRecibosDetalle>
		</cfcatch>
	</cftry>
</cffunction>
<cffunction name="RSInsertarAnalisisInsumos" access="public" returntype="struct">
    <cfargument name='id_Empresa' type='string' required='yes'> 
    <cfargument name='id_ContraRecibo' type='string' required='yes'> 
    <cfargument name='nd_ContraRecibo' type='string' required='yes'> 
    <cfargument name='id_Obra' type='string' required='yes'> 
    <cfargument name='id_CentroCosto' type='string' required='yes'> 
    <cfargument name='id_Frente' type='string' required='yes'> 
    <cfargument name='id_Partida' type='string' required='yes'>    
    <cfargument name='id_Insumo' type='string' required='yes'> 
    <cfargument name='im_Analisis' type='string' required='yes'> 
    <cfargument name='nu_Cantidad' type='string' required='yes'> 
    <cfargument name='im_Precio' type='string' required='yes'> 
    <cftry>
		<cfif #id_Partida# EQ ''>
            <cfset id_Partida = 0>
        </cfif>  
       	<cfquery name="RSInsertarAnalisisInsumos" datasource='#session.cnx#'>
        	EXECUTE ContraRecibosDetalleAnalisisInsumosInsertar #id_Empresa#, #id_ContraRecibo#, #nd_ContraRecibo#, #id_Obra#, #id_CentroCosto#, #id_Frente#, #id_Partida#, '#id_Insumo#', #im_Analisis#, #nu_Cantidad#, #im_Precio#
        </cfquery>
    	<cfset IsertarAnalisis.Insertar= TRUE>
		<cfset IsertarAnalisis.tipoError= ''>        
		<cfset IsertarAnalisis.mensaje= 'Se inserto analisis insumo correctamente'>
		<cfreturn IsertarAnalisis>
    <cfcatch type="database">
    	<cfset IsertarAnalisis.Insertar= FALSE>
        <cfset IsertarAnalisis.tipoError= 'database-indefinido'>
		<cfset IsertarAnalisis.mensaje= 'Error al insertar analisis insumo (#cfcatch.Detail#)'>
        <cfreturn IsertarAnalisis>
    </cfcatch>
    </cftry>
</cffunction>

<cffunction name='BancoCuentaBancaria_Rank' access='public' returntype='struct'>
	<cfargument name="id_cuentaBancariaRank" type="any" required="yes" default="">

	<cftry>
		<cfquery datasource='#session.cnx#' name='RS'>
			EXECUTE bop_BancosCentasBancarias_ObtenerTodos 
		</cfquery>


		<cfoutput query="RS">
			<cfif id_cuentaBancariaRank EQ RS.id_cuentaBancariaRank>
				<cfset datos.id_banco = RS.id_banco>
				<cfset datos.id_cuentaBancaria = RS.id_cuentaBancaria>				
			</cfif>		
		</cfoutput>

		<cfset datos.listado= TRUE>
		<cfset datos.tipoError= ''>
		<cfset datos.mensaje= 'Se obtuvo el recordset correctamente'>
		<cfreturn datos>
		<cfcatch type='database'>
			<cfset datos.listado= FALSE>
			<cfset datos.tipoError= 'database-indefinido'>
			<cfset datos.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn datos>
		</cfcatch>
	</cftry>
</cffunction>


<cffunction name='RSAgregarConNextIDBancos' access='public' returntype='struct'>

	<cfargument name='id_Empresa' type='numeric' required='yes'>
	<cfargument name='de_Banco' type='string' required='yes' default=''>
	<cfargument name='de_BancoSucursal' type='string' required='yes' default=''>
	<cfargument name='de_Direccion' type='string' required='yes' default=''>
	<cfargument name='id_Ciudad' type='numeric' required='yes' default='0'>
	<cfargument name='nu_Telefono1' type='string' required='no' default=''>
	<cfargument name='nu_Telefono2' type='string' required='no' default=''>
	<cfargument name='nb_Responsable' type='string' required='no' default=''>

	<cftry>
		<cftransaction>
			<cfset NextIDtmp= this.RSMostrarNextIDCentrosCostos(arguments.cnx, arguments.user_sql, arguments.password_sql, attributes.id_Empresa).rs.NextID>
			<cfset Bancos= this.RSAgregarCentrosCostos(arguments.cnx, arguments.user_sql, arguments.password_sql, arguments.id_Empresa, NextIDtmp, arguments.de_Banco, arguments.de_BancoSucursal, arguments.de_Direccion, arguments.id_Ciudad, arguments.nu_Telefono1, arguments.nu_Telefono2, arguments.nb_Responsable)>
		</cftransaction>
		<cfset Bancos.NextID= NextIDtmp>
		<cfreturn Bancos>
		<cfcatch type='database'>
			<cftransaction action='rollback'/>
			<cfset Bancos.agregado= FALSE>
			<cfif cfcatch.NativeErrorCode EQ '2627'>
				<cfset Bancos.tipoError= 'database-registro_duplicado'>
				<cfset Bancos.mensaje= 'El Banco no se pudo guardar porque ya existe'>
			<cfelse>
				<cfset Bancos.tipoError= 'database-indefinido'>
				<cfset Bancos.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			</cfif>
			<cfreturn Bancos>
		</cfcatch>
	</cftry>
</cffunction>


<cffunction name='RSAgregarBancos' access='public' returntype='struct'>

	<!--- <cfargument name='id_Empresa' type='numeric' required='yes' default='0'> --->
	<cfargument name='id_Banco' type='numeric' required='yes' default='0'>
	<cfargument name='de_Banco' type='string' required='yes' default=''>
	<cfargument name='de_BancoSucursal' type='string' required='yes' default=''>
	<cfargument name='de_Direccion' type='string' required='yes' default=''>
	<cfargument name='id_Ciudad' type='numeric' required='yes' default='0'>
	<cfargument name='nu_Telefono1' type='string' required='no' default=''>
	<cfargument name='nu_Telefono2' type='string' required='no' default=''>
	<cfargument name='nb_Responsable' type='string' required='no' default=''>

	<cfset fh_registro = #dateFormat(now(),'yyyy-mm-dd')# >
	<cfset id_usuarioRegistro = #session.id_usuario# >

	<cftry>
		<cfquery datasource='#cnx#' name='RSAgregar' username='#user_sql#' password='#password_sql#'>
			EXECUTE bop_Bancos_Agregar #id_Banco#, '#de_Banco#', '#de_BancoSucursal#', '#de_Direccion#', #id_Ciudad#, '#nu_Telefono1#', '#nu_Telefono2#', '#nb_Responsable#', '#fh_registro#', #id_usuarioRegistro#
		</cfquery>
		<cfset Bancos.agregado= TRUE>
		<cfset Bancos.tipoError= ''>
		<cfset Bancos.mensaje= 'Banco guardado correctamente'>
		<cfreturn Bancos>
		<cfcatch type='database'>
			<cfset Bancos.agregado= FALSE>
			<cfif cfcatch.NativeErrorCode EQ '2627'>
				<cfset Bancos.tipoError= 'database-registro_duplicado'>
				<cfset Bancos.mensaje= 'El Banco no se pudo guardar porque ya existe'>
			<cfelse>
				<cfset Bancos.tipoError= 'database-indefinido'>
				<cfset Bancos.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			</cfif>
			<cfreturn Bancos>
		</cfcatch>
	</cftry>
</cffunction>


<cffunction name='RSEditarBancos' access='public' returntype='struct'>

	<!--- <cfargument name='id_Empresa' type='numeric' required='yes'> --->
	<cfargument name='id_Banco' type='numeric' required='yes'>
	<cfargument name='de_Banco' type='string' required='yes' default=''>
	<cfargument name='de_BancoSucursal' type='string' required='yes' default=''>
	<cfargument name='de_Direccion' type='string' required='yes' default=''>
	<cfargument name='id_Ciudad' type='numeric' required='yes' default='0'>
	<cfargument name='nu_Telefono1' type='string' required='no' default=''>
	<cfargument name='nu_Telefono2' type='string' required='no' default=''>
	<cfargument name='nb_Responsable' type='string' required='no' default=''>

	<cftry>
		<cfquery datasource='#cnx#' name='RSFilasAfectadas' username='#user_sql#' password='#password_sql#'>
			EXECUTE bop_Bancos_Actualizar #id_Banco#, '#de_Banco#', '#de_BancoSucursal#', '#de_Direccion#', #id_Ciudad#, '#nu_Telefono1#', '#nu_Telefono2#', '#nb_Responsable#'
		</cfquery>
		<cfset Bancos.actualizado= TRUE>
		<cfset Bancos.filasAfectadas= RSFilasAfectadas.filasAfectadas>
		<cfset Bancos.tipoError= ''>
		<cfset Bancos.mensaje= 'Banco actualizado correctamente'>
		<cfreturn Bancos>
		<cfcatch type='database'>
			<cfset Bancos.actualizado= FALSE>
			<cfset Bancos.tipoError= 'database-indefinido'>
			<cfset Bancos.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn Bancos>
		</cfcatch>
	</cftry>
</cffunction>


<cffunction name='RSEliminarBancos' access='public' returntype='struct'>
	<!--- <cfargument name='id_Empresa' type='numeric' required='yes'> --->
	<cfargument name='id_Banco' type='numeric' required='yes'>

	<cftry>
		<cfquery datasource='#cnx#' name='RSFilasAfectadas' username='#user_sql#' password='#password_sql#'>
			EXECUTE bop_Bancos_Eliminar #id_Banco#
		</cfquery>
		<cfset Bancos.eliminado= TRUE>
		<cfset Bancos.filasAfectadas= RSFilasAfectadas.filasAfectadas>
		<cfset Bancos.tipoError= ''>
		<cfset Bancos.mensaje= 'Banco eliminado correctamente'>
		<cfreturn Bancos>
		<cfcatch type='database'>
			<cfset Bancos.eliminado= FALSE>
			<cfif cfcatch.NativeErrorCode EQ '547'>
				<cfset Bancos.mensaje= 'El Banco no se pudo eliminar ya que tiene registros relacionados'>
				<cfset Bancos.tipoError= 'database-integridad'>
			<cfelse>
				<cfset Bancos.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
				<cfset Bancos.tipoError= 'database-indefinido'>
			</cfif>
			<cfreturn Bancos>
		</cfcatch>
	</cftry>
</cffunction>

<!---Funciones extensivas del componente --->

<!---Hecho por: Florencia Aragon 26/11/09 --->
<!---DinamicoBancos pero con descripción de Ciudad --->
<cffunction name='RSMostrarDinamicoBancosListado' access='public' returntype='struct'>
	<!--- <cfargument name='id_Empresa' type='string' required='no' default='1'> --->
	<cfargument name='id_Banco' type='string' required='no' default='null'>
	<cfargument name='de_Banco' type='string' required='no' default=''>
	
	<cftry>
			<cfif id_Banco EQ ''>
				<cfset id_Banco = "NULL">
			</cfif>
		<cfquery datasource='#cnx#' name='RS_MostrarBancos' username='#user_sql#' password='#password_sql#'>
			EXECUTE Bancos_ObtenerDinamicoConNbCiudad #id_Banco#,'#de_Banco#'
		</cfquery>
		<cfset Bancos.listado= TRUE>
		<cfset Bancos.tipoError= ''>
		<cfset Bancos.rs= RS_MostrarBancos>
		<cfset Bancos.mensaje= 'Se obtuvo el recordset de Bancos correctamente'>
		<cfreturn Bancos>
		<cfcatch type='database'>
			<cfset Bancos.listado= FALSE>
			<cfset Bancos.tipoError= 'database-indefinido'>
			<cfset Bancos.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn Bancos>
		</cfcatch>
	</cftry>
</cffunction>

<!---Funcion que trae el de_banco por su id para bindeo--->
<cffunction name="getBancoPorID" access="remote" returntype="string">
	<!--- <cfargument name="id_Empresa" type="string" required="no"> --->
	<cfargument name="id_Banco" type="string" required="no">

	<cfif Not IsNumeric(#id_Banco#)>
		<cfreturn "">
	<cfelse>
		<cfquery name="RS" datasource='#cnx#' username='#user_sql#' password='#password_sql#'>
			EXECUTE bop_Bancos_ObtenerPorID #id_Banco#
		</cfquery>
		<cfreturn RS.de_Banco>
	</cfif>
</cffunction>


<!---Funcion para traer listado de movimientos estado de cuenta chequera--->
<!--- Florencia Aragon 25/02/2010 --->
<cffunction name='RSMostrarListadoMovimientosEdoCtaChequera' access='public' returntype='struct'>
	<cfargument name="id_Empresa" type="string" required="no">
	<cfargument name="id_CuentaBancaria" type="string" required="no">
	<cfargument name="id_CuentaBancariaConciliacion" type="string" required="no">
		
	<cfif id_CuentaBancaria EQ "">
		<cfset id_CuentaBancaria = "NULL">
	</cfif>
	<cfif id_CuentaBancariaConciliacion EQ "">
		<cfset id_CuentaBancariaConciliacion = "NULL">
	</cfif>
	
	<cftry>
		<cfquery datasource='#cnx#' name='RS_MostrarListado' username='#user_sql#' password='#password_sql#'>
			EXECUTE Bancos_ListadoMovimientosEdoCtaChequera #id_Empresa#,#id_CuentaBancaria#,#id_CuentaBancariaConciliacion#
		</cfquery>
		<cfset Reporte.listado= TRUE>
		<cfset Reporte.tipoError= ''>
		<cfset Reporte.rs= RS_MostrarListado>
		<cfset Reporte.mensaje= 'Se obtuvo el recordset de movimientos correctamente'>
		<cfreturn Reporte>
		<cfcatch type='database'>
			<cfset Reporte.listado= FALSE>
			<cfset Reporte.tipoError= 'database-indefinido'>
			<cfset Reporte.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn Reporte>
		</cfcatch>
	</cftry>
</cffunction>

<!---Funcion que trae el de_banco por su id para bindeo--->
<cffunction name="getBancoPorIDCuentaBancaria" access="remote" returntype="string">
	<cfargument name="id_Empresa" type="string" required="no">
	<cfargument name="id_CuentaBancaria" type="string" required="no">

	<cfif Not IsNumeric(#id_CuentaBancaria#)>
		<cfreturn "">
	<cfelse>
		<cfquery name="RS" datasource='#cnx#' username='#user_sql#' password='#password_sql#'>
			EXECUTE Bancos_ObtenerDescripcionBanco #id_Empresa#,#id_CuentaBancaria#
		</cfquery>
		<cfreturn #RS.de_Banco#>
	</cfif>
</cffunction>


<!---Funcion para traer listado de conciliaciones bancarias--->
<!--- Florencia Aragon 03/03/2010 --->
<cffunction name='RSMostrarListadoConciliacionesBancarias' access='public' returntype='struct'>
	<cfargument name="id_Empresa" type="string" required="no">
	<cfargument name="id_CuentaBancaria" type="string" required="no">
		
	<cfif id_CuentaBancaria EQ "">
		<cfset id_CuentaBancaria = "NULL">
	</cfif>
	
	<cftry>
		<cfquery datasource='#cnx#' name='RS_MostrarListado' username='#user_sql#' password='#password_sql#'>
			EXECUTE Bancos_ListadoConciliacionesBancarias #id_Empresa#,#id_CuentaBancaria#
		</cfquery>
		<cfset Reporte.listado= TRUE>
		<cfset Reporte.tipoError= ''>
		<cfset Reporte.rs= RS_MostrarListado>
		<cfset Reporte.mensaje= 'Se obtuvo el recordset de conciliaciones correctamente'>
		<cfreturn Reporte>
		<cfcatch type='database'>
			<cfset Reporte.listado= FALSE>
			<cfset Reporte.tipoError= 'database-indefinido'>
			<cfset Reporte.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn Reporte>
		</cfcatch>
	</cftry>
</cffunction>


<!---Funcion para traer listado de movimientos estado de cuenta bancos--->
<!--- Florencia Aragon 03/03/2010 --->
<cffunction name='RSMostrarListadoMovimientosEdoCtaBancos' access='public' returntype='struct'>
	<cfargument name="id_Empresa" type="string" required="no">
	<cfargument name="id_CuentaBancaria" type="string" required="no">
	<cfargument name="id_CuentaBancariaConciliacion" type="string" required="no">
		
	<cfif id_CuentaBancaria EQ "">
		<cfset id_CuentaBancaria = "NULL">
	</cfif>
	<cfif id_CuentaBancariaConciliacion EQ "">
		<cfset id_CuentaBancariaConciliacion = "NULL">
	</cfif>
	
	<cftry>
		<cfquery datasource='#cnx#' name='RS_MostrarListado' username='#user_sql#' password='#password_sql#'>
			EXECUTE Bancos_ListadoMovimientosEdoCtaBancos #id_Empresa#,#id_CuentaBancaria#,#id_CuentaBancariaConciliacion#
		</cfquery>
		<cfset Reporte.listado= TRUE>
		<cfset Reporte.tipoError= ''>
		<cfset Reporte.rs= RS_MostrarListado>
		<cfset Reporte.mensaje= 'Se obtuvo el recordset de movimientos correctamente'>
		<cfreturn Reporte>
		<cfcatch type='database'>
			<cfset Reporte.listado= FALSE>
			<cfset Reporte.tipoError= 'database-indefinido'>
			<cfset Reporte.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn Reporte>
		</cfcatch>
	</cftry>
</cffunction>

<cffunction name='MostrarMovimientosDiversos' access='public' returntype='struct'>
	<cfargument name="id_banco" 		 type="string" required="no" default="">
	<cfargument name="id_cuentaBancaria" type="string" required="no" default="">
	<cfargument name="sn_entradaSalida"  type="string" required="no" default="">	
	<cfargument name="nu_cuentaBancaria" type="string" required="no" default="">	

	<cfif id_banco EQ ''>
		<cfset id_banco = 'null'>
	</cfif>
	<cfif id_cuentaBancaria EQ ''>
		<cfset id_cuentaBancaria = 'null'>
	</cfif>
	<cfif nu_cuentaBancaria EQ ''>
		<cfset nu_cuentaBancaria = 'null'>
	</cfif>	
	<cftry>
		<cfquery datasource='#session.cnx#' name='RS'>
			EXECUTE bop_movimientosDiversos_mostrar #id_banco#, #id_cuentaBancaria#, #nu_cuentaBancaria#
		</cfquery>
		<cfset datos.listado= TRUE>
		<cfset datos.tipoError= ''>
		<cfset datos.rs= RS>
		<cfset datos.mensaje= 'Se obtuvo el recordset de Bancos correctamente'>
		<cfreturn datos>
		<cfcatch type='database'>
			<cfset datos.listado= FALSE>
			<cfset datos.tipoError= 'database-indefinido'>
			<cfset datos.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn datos>
		</cfcatch>
	</cftry>
</cffunction>

<cffunction name='movimientosDiversosAgregar' access='public' returntype='struct'>
	<cfargument name='id_banco' 		  type='string'  required='yes'>
	<cfargument name='id_cuentaBancaria'  type='string'  required='yes'>
	<cfargument name='de_concepto' 		  type='string'  required='yes'>
	<cfargument name='sn_entradaSalida'   type='string'  required='yes'>
	<cfargument name='fh_movimiento' 	  type='string'  required='yes'>
	<cfargument name='im_movimiento' 	  type='string'  required='yes'>
	<cfargument name='id_estatus' 		  type='string'  required='yes'>
	<cfargument name='id_obra'	 		  type='string'  required='no' default="">
	<cfargument name='id_Empresa'	 	  type='string'  required='no' default="">
	<cfargument name='id_agrupador'		  type='string'  required='no' default="">
	<cfargument name='id_contraRecibo'	  type='string'  required='no' default="">

	<cftry>
		<cfinvoke component="#Application.componentes#.funciones" method="formateaFecha" returnvariable="fh_InicioOperacion" formato_original="d/m/y" formato_nuevo="y/m/d" fecha="#fh_movimiento#" separador="/">
		<cfset fh_movimiento = dateFormat(fh_movimiento,'yyyy-mm-dd')  >
		<cfset id_usuarioRegistro = #session.id_usuario#>
		<cfset fh_registro = dateFormat(now(),'yyyy-mm-dd')>

		<cfif id_Empresa EQ ''>
			<cfset id_Empresa = 'null'>
		</cfif>
		<cfif id_obra EQ ''>
			<cfset id_obra = 'null'>
		</cfif>
		<cfif id_agrupador EQ ''>
			<cfset id_agrupador = 'null'>
		<cfelse>
			<cfset id_agrupador = id_agrupador >			
		</cfif>	
		<cfif id_contraRecibo EQ ''>
			<cfset id_contraRecibo = 'null'>
		<cfelse>
			<cfset id_contraRecibo = id_contraRecibo>			
		</cfif>		
		<cfquery datasource='#session.cnx#' name='rsNextId'>
			EXECUTE bop_BancosMovDiversos_MostrarNextID #id_banco#, #id_cuentaBancaria#
		</cfquery>
		
		<cfquery datasource='#session.cnx#' name='RS'>
			EXECUTE bop_MovDiversos_Agregar #id_banco#, #id_cuentaBancaria# ,#rsNextId.nextId#, '#de_concepto#', '#sn_entradaSalida#', '#fh_movimiento#', #im_movimiento#, #id_estatus#, #id_usuarioRegistro#, '#fh_registro#', #id_obra#,#id_Empresa#, '#id_agrupador#',#id_contraRecibo#
		</cfquery>
		
		<cfset datos.agregado= TRUE>
		<cfset datos.id_movimientoDiverso= #rsNextId.nextId#>
		<cfset datos.tipoError= ''>
		<cfset datos.mensaje= 'Movimiento guardado correctamente'>
		<cfreturn datos>
		<cfcatch type='database'>
			<cfset datos.agregado= FALSE>
			<cfset datos.tipoError= 'Error en el acceso a base de datos'>
			<cfset datos.mensaje= '#cfcatch.detail#'>
			<cfreturn datos>
		</cfcatch>
	</cftry>
</cffunction>

<cffunction name='obtenerMovimientoDiverso' access='public' returntype='struct'>
	<cfargument name='id_banco' 	  		type='numeric' required='yes'>
	<cfargument name='id_cuentaBancaria' 	type='numeric' required='yes'>
	<cfargument name='id_movimientoDiverso' type='numeric' required='yes'>

	<cftry>

		<cfquery datasource='#session.cnx#' name='RS'>
			EXECUTE bop_obtenerMovimientoDiversoPorId #id_banco#, #id_cuentaBancaria#, #id_movimientoDiverso#
		</cfquery>

		<cfset datos.listado= TRUE>
		<cfset datos.rs= RS>
		<cfset datos.tipoError= ''>
		<cfset datos.mensaje= 'su obtuvo correctamente'>
		<cfreturn datos>
		<cfcatch type='database'>
			<cfset datos.agregado= FALSE>
			<cfset datos.tipoError= 'Error en el acceso a base de datos'>
			<cfset datos.mensaje= '#cfcatch.detail#'>
			<cfreturn datos>
		</cfcatch>
	</cftry>
</cffunction>

<cffunction name='movimientosDiversosEditar' access='public' returntype='struct'>
	<cfargument name='id_movimientoDiverso' type='numeric' required='yes'>
	<cfargument name='id_banco' 		  type='string'  required='yes'>
	<cfargument name='id_cuentaBancaria'  type='string'  required='yes'>
	<cfargument name='de_concepto' 		  type='string'  required='yes'>
	<cfargument name='sn_entradaSalida'   type='string'  required='yes'>
	<cfargument name='fh_movimiento' 	  type='string'  required='yes'>
	<cfargument name='im_movimiento' 	  type='string'  required='yes'>
	<cfargument name='id_obra' 	 		  type='string'  required='no' default="">
	<cfargument name='id_agrupador' 	  type='string'  required='no' default="">
		
	<cfif id_obra EQ ''>
		<cfset id_obra = 'null'>
	</cfif>
	<cfif id_agrupador EQ ''>
		<cfset id_agrupador = 'null'>
	<cfelse>
		<cfset id_agrupador = de(id_agrupador) >			
	</cfif>	

	<cftry>
		<cfinvoke component="#Application.componentes#.funciones" method="formateaFecha" returnvariable="fh_InicioOperacion" formato_original="d/m/y" formato_nuevo="y/m/d" fecha="#fh_movimiento#" separador="/">
		<cfset fh_movimiento = de( dateFormat(fh_movimiento,'yyyy-mm-dd') ) >

		<cfquery datasource='#session.cnx#' name='RS'>
			EXECUTE bop_MovDiversos_Editar #id_movimientoDiverso#, #id_banco#, #id_cuentaBancaria#, '#de_concepto#', '#sn_entradaSalida#', #fh_movimiento#, #im_movimiento#, #id_obra#, #id_agrupador#
		</cfquery>

		<cfset datos.agregado= TRUE>
		<cfset datos.tipoError= ''>
		<cfset datos.mensaje= 'Movimiento guardado correctamente'>
		<cfreturn datos>
		<cfcatch type='database'>
			<cfset datos.agregado= FALSE>
			<cfset datos.tipoError= 'Error en el acceso a base de datos'>
			<cfset datos.mensaje= '#cfcatch.detail#'>
			<cfreturn datos>
		</cfcatch>
	</cftry>
</cffunction>

<cffunction name='movimientosDiversosEliminar' access='public' returntype='struct'>
	<cfargument name='id_banco' 	  		type='numeric' required='yes'>
	<cfargument name='id_cuentaBancaria' 	type='numeric' required='yes'>
	<cfargument name='id_movimientoDiverso' type='numeric' required='yes'>

	<cftry>

		<!--- elimina el registro en la tabla de cuentasBancariasMovimientos --->
		<cfquery datasource='#session.cnx#' name='RSMovimientosEliminar'>
			EXECUTE bop_cuentasBancariasmovDiversos_eliminar #id_banco#, #id_cuentaBancaria#, #id_movimientoDiverso#
		</cfquery>
		<!---  --->		

		<!--- elimina el registro de la tabla bancoMovDiversos --->
		<cfquery datasource='#session.cnx#' name='RS'>
			EXECUTE bop_movDiversos_eliminar #id_banco#, #id_cuentaBancaria#, #id_movimientoDiverso#
		</cfquery>
		<!---  --->

		<cfset datos.eliminado= TRUE>
		<cfset datos.tipoError= ''>
		<cfset datos.mensaje= 'Movimiento eliminado correctamente'>
		<cfreturn datos>
		<cfcatch type='database'>
			<cfset datos.eliminado= FALSE>
			<cfset datos.tipoError= 'Error en el acceso a base de datos'>
			<cfset datos.mensaje= '#cfcatch.detail#'>
			<cfreturn datos>
		</cfcatch>
	</cftry>
</cffunction>

<cffunction name='cuentasBancariasMovimientos_agregar' access='public' returntype='struct'>
	<cfargument name='id_banco' 	  		type='numeric' required='yes'>
	<cfargument name='id_cuentaBancaria' 	type='numeric' required='yes'>
	<cfargument name='sn_entradaSalida' 	type='string'  required='no' default="">
	<cfargument name='id_tipoMovimiento' 	type='string'  required='no' default="">
	<cfargument name='im_movimiento' 		type='string'  required='yes' default="">
	<cfargument name='fh_movimiento' 		type='string'  required='yes' default="">
	<cfargument name='id_estatus' 			type='string'  required='yes' default="">
	<cfargument name='id_usuarioRegistro' 	type='numeric' required='yes' default="">
	<cfargument name='fh_registro' 			type='string'  required='yes' default="">
	<cfargument name='id_traspaso' 			type='string'  required='no'  default="">
	<cfargument name='id_movimientoDiverso' type='string'  required='no'  default="">

	<cftry>
		<cfinvoke component="#Application.componentes#.funciones" method="formateaFecha" returnvariable="fh_movimiento" formato_original="d/m/y" formato_nuevo="y/m/d" fecha="#fh_movimiento#" separador="/">
		<cfset fh_movimiento=  de( dateformat(fh_movimiento,'yyyy-mm-dd') )>
		<cfset fh_registro=  de(fh_registro)>

		<cfquery name="rsNextId" datasource="#session.cnx#">
			bop_cuentasBancariasMovimientos_nextId #id_banco#, #id_cuentaBancaria#
		</cfquery>

		<cfquery datasource='#session.cnx#' name='RS'>
			EXECUTE bop_cuentasBancariasMovmientos_insertar 
			#id_banco#, #id_cuentaBancaria#, #rsNextId.nextId#
			,'#sn_entradaSalida#', #id_tipoMovimiento#, #im_movimiento#
			,#fh_movimiento#, #id_estatus#, #id_usuarioRegistro#
			,#fh_registro#, #id_traspaso#, #id_movimientoDiverso#
		</cfquery>

		<cfset datos.agregado= TRUE>
		<cfset datos.tipoError= ''>
		<cfset datos.mensaje= 'Movimiento guardado correctamente'>
		<cfreturn datos>
		<cfcatch type='database'>
			<cfset datos.agregado= FALSE>
			<cfset datos.tipoError= 'Error en el acceso a base de datos'>
			<cfset datos.mensaje= '#cfcatch.detail#'>
			<cfreturn datos>
		</cfcatch>
	</cftry>
</cffunction>

<cffunction name='cuentasBancariasMovimientos_editar' access='public' returntype='struct'>
	<cfargument name='id_banco' 	  		type='numeric' required='yes'>
	<cfargument name='id_cuentaBancaria' 	type='numeric' required='yes'>
	<cfargument name='im_movimiento' 		type='string'  required='yes' default="">
	<cfargument name='fh_movimiento' 		type='string'  required='yes' default="">
	<cfargument name='id_traspaso' 			type='string'  required='yes' default="">

	<cftry>
		<cfinvoke component="#Application.componentes#.funciones" method="formateaFecha" returnvariable="fh_movimiento" formato_original="d/m/y" formato_nuevo="y/m/d" fecha="#fh_movimiento#" separador="/">
		<cfset fh_movimiento=  de( dateformat(fh_movimiento,'yyyy-mm-dd') )>

		<cfquery name="rsMovimiento" datasource="#session.cnx#">
			select id_movimiento from cuentasBancariasMovimientos
			where id_banco= #id_banco# and id_cuentaBancaria=#id_cuentaBancaria# and id_traspaso=#id_traspaso#
		</cfquery>

		<cfquery datasource='#session.cnx#' name='RS'>
			EXECUTE bop_cuentasBancariasMovmientos_editar 
			#id_banco# , #id_cuentaBancaria# , #rsMovimiento.id_movimiento# , #im_movimiento# , #fh_movimiento#
		</cfquery>

		<cfset datos.agregado= TRUE>
		<cfset datos.tipoError= ''>
		<cfset datos.mensaje= 'Movimiento editado correctamente'>
		<cfreturn datos>
		<cfcatch type='database'>
			<cfset datos.agregado= FALSE>
			<cfset datos.tipoError= 'Error en el acceso a base de datos'>
			<cfset datos.mensaje= '#cfcatch.detail#'>
			<cfreturn datos>
		</cfcatch>
	</cftry>
</cffunction>

<cffunction name='cuentasBancariasMovimientosDiversos_editar' access='public' returntype='struct'>
	<cfargument name='id_banco' 	  		type='numeric' required='yes'>
	<cfargument name='id_cuentaBancaria' 	type='numeric' required='yes'>
	<cfargument name='id_movimientoDiverso'	type='string'  required='yes' default="">
	<cfargument name='sn_entradaSalida'		type='string'  required='yes' default="">
	<cfargument name='im_movimiento' 		type='string'  required='yes' default="">
	<cfargument name='fh_movimiento' 		type='string'  required='yes' default="">
	
	<cftry>
		<cfinvoke component="#Application.componentes#.funciones" method="formateaFecha" returnvariable="fh_movimiento" formato_original="d/m/y" formato_nuevo="y/m/d" fecha="#fh_movimiento#" separador="/">
		<cfset fh_movimiento=  de( dateformat(fh_movimiento,'yyyy-mm-dd') )>

		<cfquery datasource='#session.cnx#' name='RS'>
			EXECUTE bop_cuentasBancariasMovmientosDiversos_editar 
			#id_banco# , #id_cuentaBancaria# , #id_movimientoDiverso# , '#sn_entradaSalida#', #im_movimiento# , #fh_movimiento#
		</cfquery>

		<cfset datos.agregado= TRUE>
		<cfset datos.tipoError= ''>
		<cfset datos.mensaje= 'Movimiento editado correctamente'>
		<cfreturn datos>
		<cfcatch type='database'>
			<cfset datos.agregado= FALSE>
			<cfset datos.tipoError= 'Error en el acceso a base de datos'>
			<cfset datos.mensaje= '#cfcatch.detail#'>
			<cfreturn datos>
		</cfcatch>
	</cftry>
</cffunction>

<cffunction name='RS_GeneraPagoElectronico' access='public' returntype='struct'>
	<cfargument name='id_Empresa' 	  		type='string' required='yes'>
	<cfargument name='id_Proveedor' 	    type='string' required='yes'>
	<cfargument name='fh_pago'				type='string'  required='yes' default="">
	<cfargument name='operacion'		    type='string'  required='yes' default="">
	<cfargument name='cuenta_origen' 		type='string'  required='yes' default="">
	<cfargument name='cuenta_clabeDestino'  type='string'  required='yes' default="">
	<cfargument name='im_Total'             type='string'  required='yes' default="">
	<cfargument name='referencia'           type='string'  required='yes' default="">
	<cfargument name='descripcion'          type='string'  required='yes' default="">
	<cfargument name='rfc_ordenante'        type='string'  required='yes' default="">
	<cfargument name='iva'      		    type='string'  required='yes' default="">
	<cfargument name='email_beneficiario'   type='string'  required='yes' default="">
	<cfargument name='fecha_Aplicacion'     type='string'  required='yes' default="">
	<cfargument name='instruccion_Pago'     type='string'  required='yes' default="">
	
	<cfset total=replace(#im_Total#, ",", "", 'ALL')>
	<cfset total=replace(#total#, "$", "", 'ALL')>

	<cfquery name="getMoneda" datasource="#session.cnx#">
	select *
		FROM cuentasBancarias
		where nu_cuentaBancaria='#cuenta_origen#'

	</cfquery>
	
	<cfset mn_origen=#getMoneda.id_Moneda#>
	<cfset mn_destino=#getMoneda.id_Moneda#>


	<cfif #dateformat(fh_pago,'dd')# GT 12>
		<cfset fh_pago = #dateformat(fh_pago,'yyyy-mm-dd hh:mm:ss')#>
		 <cfelse>
		<cfset fh_pago = #dateformat(fh_pago,'yyyy-dd-mm hh:mm:ss')#>
	</cfif>
    <!---Fecha Fin--->
    <cfif #dateformat(fecha_Aplicacion,'dd')# GT 12>
		<cfset fecha_Aplicacion = #dateformat(fecha_Aplicacion,'yyyy-mm-dd hh:mm:ss')#>
    <cfelse>
        <cfset fecha_Aplicacion = #dateformat(fecha_Aplicacion,'yyyy-dd-mm hh:mm:ss')#>
    </cfif>

	<cftry>

		<cfquery name="RS_inserta_Pago" datasource='#session.cnx#'>
			AGREGA_PAGO_PROVEEDOR 
				@id_Proveedor = #id_Proveedor#,
				@fh_pago = '#fh_pago#',
				@operacion = '#operacion#',
				@cuenta_origen = '#cuenta_origen#',
				@cuenta_clabeDestino = '#cuenta_clabeDestino#',
				@im_total = #total#,
				@referencia = '#referencia#',
				@descripcion = '#descripcion#',
				@mn_origen = #mn_origen#,
				@mn_destino = #mn_destino#,
				@rfc_ordenante ='#rfc_ordenante#',
				@iva =#iva#,
				@email_beneficiario ='#email_beneficiario#',
				@fecha_aplicacion  = '#fecha_Aplicacion#',
				@instraccion_pago  ='#instruccion_Pago#'
		</cfquery>

		<cfset datos.agregado= TRUE>
		<cfset datos.tipoError= ''>
		<cfset datos.mensaje= 'Pago Agregado correctamente'>
		<cfset datos.pago= RS_inserta_Pago.id_Pago>
		<cfreturn datos>
		<cfcatch type='database'>
			<cfset datos.agregado= FALSE>
			<cfset datos.tipoError= 'Error en el acceso a base de datos'>
			<cfset datos.mensaje= '#cfcatch.detail#'>
			<cfreturn datos>
		</cfcatch>
	</cftry>
</cffunction>

<cffunction name='RS_GeneraDetallePagoElectronico' access='public' returntype='struct'>
	<cfargument name='id_Empresa' 	  		    type='string' required='yes'>
	<cfargument name='id_Pago' 	                type='string' required='yes'>
	<cfargument name='id_ContraRecibo'			type='string' required='yes' default="">
	<cfargument name='im_Subtotal'		    	type='string'  required='yes' default="">
	<cfargument name='fh_pago' 					type='string'  required='yes' default="">
	
	
	<cfset subtotal=replace(#im_Subtotal#, ",", "", 'ALL')>
	<cfset subtotal=replace(#subtotal#, "$", "", 'ALL')>

	<cfif #dateformat(fh_pago,'dd')# GT 12>
		<cfset fh_pago = #dateformat(fh_pago,'yyyy-mm-dd hh:mm:ss')#>
		 <cfelse>
		<cfset fh_pago = #dateformat(fh_pago,'yyyy-dd-mm hh:mm:ss')#>
	</cfif>
   	<cfquery name="obra" datasource="#session.cnx#">
   		SELECT id_Obra
   		FROM ContraRecibosDetalle
   		WHERE id_Empresa=#id_Empresa# AND id_ContraRecibo=#id_ContraRecibo#
   	</cfquery>
   

	<cftry>
		<cfquery name="RS_inserta_Pago" datasource='#session.cnx#'>
			AGREGA_DETALLE_PAGO_PROVEEDOR 
				@id_Empresa = #id_Empresa#,
				@id_Pago = #id_Pago#,
				@id_ContraRecibo = #id_ContraRecibo#,
				@im_Subtotal = #subtotal#,
				@fh_Pago = '#fh_pago#',
				@id_obra=#obra.id_Obra#
				
		</cfquery>



		<cfset datos.agregado= TRUE>
		<cfset datos.tipoError= ''>
		<cfset datos.mensaje= 'Pago Agregado correctamente'>
		<cfset datos.pago= RS_inserta_Pago.nd_Pago>
		<cfreturn datos>
		<cfcatch type='database'>
			<cfset datos.agregado= FALSE>
			<cfset datos.tipoError= 'Error en el acceso a base de datos'>
			<cfset datos.mensaje= '#cfcatch.detail#'>
			<cfreturn datos>
		</cfcatch>
	</cftry>
</cffunction>
<cffunction name='Actualiza_Tipo_Cambio' access='public' returntype='struct'>
	<cfargument name='id_Empresa' 	  		    	type='string' required='yes'>
	<cfargument name='id_Obra' 	                	type='string' required='yes'>
	<cfargument name='id_ContraRecibo'				type='string' required='yes' default="">
	<cfargument name='nd_ContraRecibo'				type='string' required='yes' default="">
	<cfargument name='id_OrdenCompra'		    	type='string'  required='yes' default="">
	<cfargument name='id_Proveedor' 				type='string'  required='yes' default="">
	<cfargument name='tipo_Cambio' 					type='string'  required='yes' default="">
	
	


	<cftry>
		
		<cfquery name="RS_Actualiza" datasource='#session.cnx#'>
				Actualiza_Tipo_Cambio
				@id_Empresa = #id_Empresa#,
				@id_Obra = #id_Obra#,
				@id_ContraRecibo = #id_ContraRecibo#,
				@nd_ContraRecibo =#nd_ContraRecibo#,
				@id_OrdenCompra = #id_OrdenCompra#,
				@id_Proveedor = #id_Proveedor#,
				@tipo_Cambio = #tipo_Cambio#
				
		</cfquery>



		<cfset datos.agregado= TRUE>
		<cfset datos.tipoError= ''>
		<cfset datos.mensaje= 'Se ha Actualizado el tipo de cambio'>
		
		<cfreturn datos>
		<cfcatch type='database'>
			<cfset datos.agregado= FALSE>
			<cfset datos.tipoError= 'Error en el acceso a base de datos'>
			<cfset datos.mensaje= '#cfcatch.detail#'>
			<cfreturn datos>
		</cfcatch>
	</cftry>
</cffunction>

<cffunction name='AGREGA_MOVIMIENTO_BANCARIO' access='public' returntype='struct'>
	<cfargument name='id_Banco' 	  		    type='string' required='yes'>
	<cfargument name='nu_cuentaBancaria' 	    type='string' required='yes'>
	<cfargument name='im_Pago'					type='string' required='yes' default="">
	<cfargument name='id_EmpleadoRegistro'		type='string'  required='yes' default="">
	<cfargument name='id_Proveedor' 			type='string'  required='yes' default="">
	<cfargument name='id_Pago' 			        type='string'  required='yes' default="">

	<cfset subtotal=replace(#im_Pago#, ",", "", 'ALL')>
	<cfset subtotal=replace(#subtotal#, "$", "", 'ALL')>
	<cfset nu_cuentaBancaria=TRIM(#nu_cuentaBancaria#)>
	<cfquery name="RSCuenta" datasource='#session.cnx#'>
		SELECT	id_cuentaBancaria
		FROM cuentasBancarias
		WHERE nu_cuentaBancaria='#nu_cuentaBancaria#' and id_Banco=#id_Banco#
	</cfquery>

	<cftry>
		<cfquery name="RS_inserta_Pago" datasource='#session.cnx#'>
			AGREGA_MOVIMIENTO_BANCARIO 
				@id_Banco = #id_Banco#,
				@id_CuentaBancaria = #RSCuenta.id_cuentaBancaria#,
				@im_Pago = #subtotal#,
				@id_EmpleadoRegistro = #id_EmpleadoRegistro#,
				@id_Proveedor = #id_Proveedor#,
				@id_Pago=#id_Pago#
				
		</cfquery>
	

		<cfset datos.id_Movimiento=#RS_inserta_Pago.id_movimiento#>
		<cfset datos.agregado= TRUE>
		<cfset datos.tipoError= ''>
		<cfset datos.mensaje= 'Pago Agregado correctamente'>
		<cfreturn datos>
		<cfcatch type='database'>
			<cfset datos.agregado= FALSE>
			<cfset datos.tipoError= 'Error en el acceso a base de datos'>
			<cfset datos.mensaje= '#cfcatch.detail#'>
			<cfreturn datos>
		</cfcatch>
	</cftry>
</cffunction>

<!--- <cffunction name='RS_GeneraPagoElectronico_txt' access='public' returntype='struct'>
  	<cfargument name='pagos' type="query" 	  required='yes'>
  	<cfargument name='ruta'  type='string'    required='yes'>
  

  <cfset bo_DocumentosClientes = StructNew()>
  <cfset cadena=''>
   <cftry>
   
    <cfloop query="pagos">

     
      <cfset bo_DocumentosClientes.tipoError= ''>  

   
        <cfset cadena = cadena>
            

      <cfset cadena= cadena & pagos.operacion & '|'>
      <!--- Clave --->
      
      <cfif Pagos.clave NEQ 0>
		<cfset tamano=13-Len(Pagos.clave)>
     	<cfset cadena= cadena & Pagos.clave>
		
		<cfloop index ="i" from="1" to="#tamano#" step="1">
     		<cfset cadena= cadena  & Chr(160) >
     	</cfloop>
      		
      	<cfset cadena= cadena  & '|'>
      <cfelse>
      	<cfset cadena=cadena & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & '|'>
      
      </cfif>
      
      <!--- Cuenta Origen--->
      <cfif LEN(Pagos.cuenta_origen) LT 21>
      	<cfset tamano=20-LEN(Pagos.cuenta_origen)>
      	<cfloop index ="i" from="1" to="#tamano#" step="1">
     		<cfset cadena= cadena  & '0'>
     	</cfloop>
     	<cfset cadena= cadena & Pagos.cuenta_origen & '|'>

      	
      </cfif>
      <!--- Cuenta Destino---->
   	 <cfif LEN(Pagos.cuenta_clabeDestino) LT 21>
      	<cfset tamano=20-LEN(Pagos.cuenta_clabeDestino)>
      	<cfloop index ="i" from="1" to="#tamano#" step="1">
     		<cfset cadena= cadena  & '0'>
     	</cfloop>
     	<cfset cadena= cadena & Pagos.cuenta_clabeDestino & '|'>

     </cfif>
     <!--- iMPORTE--->
     <cfset importe=replace(#Pagos.im_Total#, "$", "", 'ALL')>
     <cfset importe=replace(#importe#, ",", "", 'ALL')>
     <cfset importe=numberFormat(importe, '.__')>
	 <cfset importe=listToArray("#importe#", '.')>
  	
     <cfif importe[2] eq '00'>
     	<cfset importe=importe[1]&'00'>
     <cfelse>
     	<cfset importe=	importe[1]&''&importe[2]>
     </cfif>
    
     <cfset tamano=14-Len(importe)>
     
	 <cfloop index ="i" from="1" to="#tamano#" step="1">
     	<cfset cadena= cadena  & '0'>
     </cfloop>
     <cfset cadena= cadena & importe & '|'>
     
     <!--- REFERENCIA--->
     <cfif Len(pagos.referencia) NEQ  0>
     	<cfset tamano=10-Len(pagos.referencia)>
     	<cfloop index ="i" from="1" to="#tamano#" step="1">
     		<cfset cadena= cadena  & '0'>
     	</cfloop>
      		<cfset cadena= cadena & Pagos.referencia & '|'>

     <cfelse>	
     	<cfset cadena=cadena & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & '|'>
	 </cfif>

	 <!--- DESCRIPCION --->

	 <cfif Len(pagos.descripcion) NEQ 0>
	 	<cfset tamano=30-Len(pagos.descripcion)>
     	<cfset cadena= cadena & Pagos.descripcion>	
     	<cfloop index ="i" from="1" to="#tamano#" step="1">
     		<cfset cadena= cadena  & chr(160)>
     	</cfloop>
     	<cfset cadena= cadena & '|'>	
     <cfelse>
     	<cfset cadena=cadena & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & '|'>		
	 </cfif>	
	 <!--- RFC ORDENANTE --->
	 <cfif Len(pagos.rfc_ordenante) NEQ 0>
	 	<cfset tamano=13-Len(pagos.rfc_ordenante)>
     	<cfset cadena= cadena & Pagos.rfc_ordenante>	

     	<cfloop index ="i" from="1" to="#tamano#" step="1">
     		<cfset cadena= cadena  & chr(160)>
     	</cfloop>
     	<cfset cadena= cadena & '|'>	
     <cfelse>
     	<cfset cadena=cadena & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & '|'>		
	 </cfif>
	 
	 <!--- IVA --->
	<cfif Len(Pagos.iva) NEQ 0>

		 <cfset iva2=replace(#Pagos.iva#, "$", "", 'ALL')>
	     <cfset iva2=replace(#iva2#, ",", "", 'ALL')>
	 	 <cfset iva2=numberFormat(#iva2#, '.__')>
		 <cfset iva2=listToArray("#iva2#", '.')>
		 

		 <cfif iva2[2] eq '00'>
	     	<cfset iva2=iva2[1]&'00'>
	     <cfelse>
	     	<cfset iva2=iva2[1]&'00'&iva2[2]>
	     </cfif>
	    
	     <cfset tamano=14-Len(iva2)>
	     
		 <cfloop index ="i" from="1" to="#tamano#" step="1">
	     	<cfset cadena= cadena  & '0'>
	     </cfloop>
	     <cfset cadena= cadena & iva2 & '|'>
	 <cfelse>
     	<cfset cadena=cadena & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & '|'>		
	     
	 </cfif>
	 <!--- EMAIL BENEFICIARIO --->
	 <cfif Len(pagos.email_beneficiario) NEQ 0>
	 	<cfset tamano=39-Len(pagos.email_beneficiario)>
     	<cfset cadena= cadena & Pagos.email_beneficiario>	
     	<cfloop index ="i" from="1" to="#tamano#" step="1">
     		<cfset cadena= cadena  & chr(160)>
     	</cfloop>
     	<cfset cadena= cadena & '|'>	
     <cfelse>
     	<cfset cadena=cadena & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160)& chr(160)& chr(160)& chr(160)& chr(160)& chr(160)& chr(160)& chr(160)& chr(160)&'|'>		
	 </cfif>	


     <cfif #dateformat(fecha_Aplicacion,'dd')# GT 12>
		<cfset fecha = #dateformat(fecha_Aplicacion,'ddmmyyyy')#>
     <cfelse>
        <cfset fecha = #dateformat(fecha_Aplicacion,'mmddyyyy')#>
     </cfif>

   	 <cfset cadena= cadena & fecha & '|'>

   	 <cfif Len(pagos.instruccion_Pago) NEQ  0>
	 	<cfset tamano=70-Len(pagos.instruccion_Pago)>
     	<cfset cadena= cadena & Pagos.instruccion_Pago>	

     	<cfloop index ="i" from="1" to="#tamano#" step="1">
     		<cfset cadena= cadena  & chr(160)>
     	</cfloop>
     	<cfset cadena= cadena >	
     <cfelse>
     	<cfset cadena=cadena & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) &  chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) &  chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) &  chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) &  chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) &  chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) &  chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) &  chr(160) >		
	 </cfif>

	  <cfset cadena= cadena &chr(10)>

   
   </cfloop>



    <cfset ruta2= expandPath("#ruta#")>


    <cffile action="write"  file="#ruta2#" output="#cadena#" fixnewline="true">

    <cfcatch type=''>
                 <cfset bo_DocumentosClientes.tipoError= 'Error'>
                 <cfset bo_DocumentosClientes.mensaje = '#cfcatch.Detail#'>                                                              
    </cfcatch>
  </cftry>
  <cfreturn bo_DocumentosClientes>

</cffunction>  --->
<cffunction name='RS_GeneraPagoElectronico_txt' access='public' returntype='struct'>
  	<cfargument name='pagos' type="query" 	  required='yes'>
  	<cfargument name='ruta'  type='string'    required='yes'>
  

  <cfset bo_DocumentosClientes = StructNew()>
  <cfset cadena=''>
   <cftry>
   
    <cfloop query="pagos">
    	
    <cfquery name="Banorte" datasource="#session.cnx#">
    	SELECT id_Banorte
    	FROM proveedores
    	WHERE ID_proveedor=#ID_PROVEEDOR#	


    </cfquery>
     
      <cfset bo_DocumentosClientes.tipoError= ''>  

   
        <cfset cadena = cadena>
            
      <!---Operacion 2= pagos a roveedores o terceros, 4 = SPEI--->   
      <cfset cadena= cadena & pagos.operacion & '	'>
      <!--- Clave --->
      <cfset cadena= cadena & Banorte.id_Banorte & '	'> 
     <!---  <cfif Pagos.clave NEQ 0>
		<cfset tamano=13-Len(Pagos.clave)>
     	<cfset cadena= cadena & Pagos.clave>
		
		<cfloop index ="i" from="1" to="#tamano#" step="1">
     		<cfset cadena= cadena  & Chr(160) >
     	</cfloop>
      		
      	<cfset cadena= cadena  & Chr(9)>
      <cfelse>
      	<cfset cadena=cadena & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & '|'>
      
      </cfif>
       --->
      <!--- Cuenta Origen--->
<!---       <cfif LEN(Pagos.cuenta_origen) LT 11>
      	<cfset tamano=10-LEN(Pagos.cuenta_origen)>
      	<cfloop index ="i" from="1" to="#tamano#" step="1">
     		<cfset cadena= cadena  & '0'>
     	</cfloop>
     	<cfset cadena= cadena & Pagos.cuenta_origen & Chr(9)>
     </cfif> --->
     <cfset cadena= cadena & Pagos.cuenta_origen &  '	'>
     <!--- Cuenta Destino---->
     <cfset cadena= cadena & Pagos.cuenta_clabeDestino & '	'>
	 <!--- <cfif LEN(Pagos.cuenta_clabeDestino) LT 19>
      	<cfset tamano=18-LEN(Pagos.cuenta_clabeDestino)>
      	<cfloop index ="i" from="1" to="#tamano#" step="1">
     		<cfset cadena= cadena  & '0'>
     	</cfloop>
     	<cfset cadena= cadena & Pagos.cuenta_clabeDestino & Chr(9)>

     </cfif>
      ---><!--- iMPORTE--->
     <cfset importe=replace(#Pagos.im_Total#, "$", "", 'ALL')>
     <cfset importe=replace(#importe#, ",", "", 'ALL')>
     <cfset importe=numberFormat(importe, '.__')>
  	<!--- 
     <cfif importe[2] eq '00'>
     	<cfset importe=importe[1]&'00'>
     <cfelse>
     	<cfset importe=	importe[1]&''&importe[2]>
     </cfif>	 --->
    
    <!---  <cfset tamano=14-Len(importe)> --->
     <cfset cadena= cadena & importe & '	'>
	<!---  <cfloop index ="i" from="1" to="#tamano#" step="1">
     	<cfset cadena= cadena  & '0'>
     </cfloop>
     <cfset cadena= cadena & importe & Chr(9)> --->
     
     <!--- REFERENCIA--->
     <cfset cadena= cadena & Pagos.referencia & '	'>

     <!--- <cfif Len(pagos.referencia) NEQ  0>
     	<cfset tamano=10-Len(pagos.referencia)>
     	<cfloop index ="i" from="1" to="#tamano#" step="1">
     		<cfset cadena= cadena  & '0'>
     	</cfloop>
      		<cfset cadena= cadena & Pagos.referencia & Chr(9)>

     <cfelse>	
     	<cfset cadena=cadena & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & Chr(9)>
	 </cfif>	
 --->
	 <!--- DESCRIPCION --->
	 <cfset cadena= cadena & Pagos.descripcion & '	'>	
	<!---  <cfif Len(pagos.descripcion) NEQ 0>
	 	<cfset tamano=30-Len(pagos.descripcion)>
     	<cfset cadena= cadena & Pagos.descripcion>	
     	<cfloop index ="i" from="1" to="#tamano#" step="1">
     		<cfset cadena= cadena  & chr(160)>
     	</cfloop>
     	<cfset cadena= cadena & Chr(9)>	
     <cfelse>
     	<cfset cadena=cadena & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & Chr(9)>		
	 </cfif> --->	
	 <!--- RFC ORDENANTE --->
     <cfset cadena= cadena & Pagos.rfc_ordenante & '	'>	
	
	<!---  <cfif Len(pagos.rfc_ordenante) NEQ 0>
	 	<cfset tamano=13-Len(pagos.rfc_ordenante)>

     	<cfloop index ="i" from="1" to="#tamano#" step="1">
     		<cfset cadena= cadena  & chr(160)>
     	</cfloop>
     	<cfset cadena= cadena & '	'>	
     <cfelse>
     	<cfset cadena=cadena & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & '	'>		
	 </cfif> --->
	 
	 <!--- IVA --->
	 <cfset iva2=replace(#Pagos.iva#, "$", "", 'ALL')>
	 <cfset iva2=replace(#iva2#, ",", "", 'ALL')>
	 <cfset iva2=numberFormat(#iva2#, '.__')>
	 <cfset cadena= cadena & iva2 & '	'>
	<!--- <cfif Len(Pagos.iva) NEQ 0>

		 <cfset iva2=replace(#Pagos.iva#, "$", "", 'ALL')>
	     <cfset iva2=replace(#iva2#, ",", "", 'ALL')>
	 	 <cfset iva2=numberFormat(#iva2#, '.__')>
<!--- 	<cfset iva2=listToArray("#iva2#", '.')>
 --->	 

	<!--- 	 <cfif iva2[2] eq '00'>
	     	<cfset iva2=iva2[1]&'00'>
	     <cfelse>
	     	<cfset iva2=iva2[1]&'00'&iva2[2]>
	     </cfif>
     --->
	     <cfset tamano=14-Len(iva2)>
	     
		 <cfloop index ="i" from="1" to="#tamano#" step="1">
	     	<cfset cadena= cadena  & '0'>
	     </cfloop>
	     <cfset cadena= cadena & iva2 & Chr(9)>
	 <cfelse>
     	<cfset cadena=cadena & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & Chr(9)>		
	     
	 </cfif> --->
	 <!--- EMAIL BENEFICIARIO --->
<!--- 	 <cfif Len(pagos.email_beneficiario) NEQ 0>
	 	<cfset tamano=39-Len(pagos.email_beneficiario)>
     	<cfset cadena= cadena & Pagos.email_beneficiario>	
     	<cfloop index ="i" from="1" to="#tamano#" step="1">
     		<cfset cadena= cadena  & chr(160)>
     	</cfloop>
     	<cfset cadena= cadena & Chr(9)>	
     <cfelse>
     	<cfset cadena=cadena & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160)& chr(160)& chr(160)& chr(160)& chr(160)& chr(160)& chr(160)& chr(160)& chr(160)&'|'>		
	 </cfif>	
 --->

     <cfif #dateformat(fecha_Aplicacion,'dd')# GT 12>
		<cfset fecha = #dateformat(fecha_Aplicacion,'ddmmyyyy')#>
     <cfelse>
        <cfset fecha = #dateformat(fecha_Aplicacion,'ddmmyyyy')#>
     </cfif>

   	 <cfset cadena= cadena & fecha & '	'>
<!--- 
   	 <cfif Len(pagos.instruccion_Pago) NEQ  0>
	 	<cfset tamano=70-Len(pagos.instruccion_Pago)>
     	<cfset cadena= cadena & Pagos.instruccion_Pago>	

     	<cfloop index ="i" from="1" to="#tamano#" step="1">
     		<cfset cadena= cadena  & chr(160)>
     	</cfloop>
     	<cfset cadena= cadena >	
     <cfelse>
     	<cfset cadena=cadena & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) &  chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) &  chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) &  chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) &  chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) &  chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) &  chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) & chr(160) &  chr(160) >		
	 </cfif>
 --->
 	  <cfif pagos.operacion  EQ 1 OR pagos.operacion  EQ 2>
 	  		<cfset cadena= cadena & 'X' & Chr(10)>	
 	  <cfelse>
 	  		<cfif len(pagos.instruccion_Pago) EQ 0>
 	  			<cfset cadena= cadena & 'X' & Chr(10)>	
			<cfelse>
 	  			<cfset cadena= cadena & Pagos.instruccion_Pago & Chr(10)>	
			</cfif>
 	  				
 	  </cfif>	
	

   
   </cfloop>



    <cfset ruta2= expandPath("#ruta#")>


    <cffile action="write"  file="#ruta2#" output="#cadena#" fixnewline="true">

    <cfcatch type=''>
                 <cfset bo_DocumentosClientes.tipoError= 'Error'>
                 <cfset bo_DocumentosClientes.mensaje = '#cfcatch.Detail#'>                                                              
    </cfcatch>
  </cftry>
  <cfreturn bo_DocumentosClientes>

</cffunction> 
<cffunction name='conciliacionesBancos_listado' access='public' returntype='struct'>
	<cfargument name="id_banco" type="string" required="no" default="">
	<cfargument name="id_CuentaBancaria" type="string" required="no" default="">
		
	<cfif id_CuentaBancaria EQ "">
		<cfset id_banco = "NULL">
	</cfif>
	<cfif id_CuentaBancaria EQ "">
		<cfset id_CuentaBancaria = "NULL">
	</cfif>
	
	<cftry>
		<cfquery name='RS' datasource='#session.cnx#'>
			EXECUTE conciliacionesBancos_listado #id_banco#, #id_CuentaBancaria#
		</cfquery>

		<cfset Reporte.listado= TRUE>
		<cfset Reporte.tipoError= ''>
		<cfset Reporte.rs= #RS#>
		<cfset Reporte.mensaje= 'Se obtuvo el recordset de conciliaciones correctamente'>
		<cfreturn Reporte>
		<cfcatch type='database'>
			<cfset Reporte.listado= FALSE>
			<cfset Reporte.tipoError= 'database-indefinido'>
			<cfset Reporte.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn Reporte>
		</cfcatch>
	</cftry>
</cffunction>
<cffunction name='ContraRecibosDetalle_Pago_parcial' access='public' returntype='struct'>
	<cfargument name='id_Empresa' 	  		    type='string' required='yes'>
	<cfargument name='id_Obra' 	  			    type='string' required='yes'>
	<cfargument name='id_ContraRecibo'			type='string' required='yes' default="">
	<cfargument name='im_Pago'					type='string'  required='yes' default="">
	<cfargument name='fh_Pago' 					type='string'  required='yes' default="">
	<cfargument name='id_Estatus' 			    type='string'  required='yes' default="">
	<cfargument name='id_Banco' 			    type='string'  required='yes' default="">
	<cfargument name='nu_cuentaBancaria' 		type='string'  required='yes' default="">
	<cfargument name='id_Movimiento' 			type='string'  required='yes' default="">

	<cfset subtotal=replace(#im_Pago#, ",", "", 'ALL')>
	<cfset subtotal=replace(#subtotal#, "$", "", 'ALL')>
	<cfset nu_cuentaBancaria=TRIM(#nu_cuentaBancaria#)>
	<cfquery name="RSCuenta" datasource='#session.cnx#'>
		SELECT	id_cuentaBancaria
		FROM cuentasBancarias
		WHERE nu_cuentaBancaria='#nu_cuentaBancaria#' and id_Banco=#id_Banco#
	</cfquery>
	<cftry>
	
		<cfquery name="RS_inserta_Pago" datasource='#session.cnx#'>
			 	ContraRecibosDetalle_Pago_parcial
					@id_Empresa = #id_Empresa#,
					@id_Obra = #id_Obra#,
					@id_ContraRecibo = #id_ContraRecibo#,
					@im_Pago = #subtotal#,
					@fh_Pago = '#fh_Pago#',
					@id_Estatus=#id_Estatus#,
					@id_Banco = #id_Banco#,
					@id_CuentaBancaria = #RSCuenta.id_CuentaBancaria#,
					@id_Movimiento =  #id_Movimiento#
				
		</cfquery>
		
		<cfset datos.agregado= TRUE>
		<cfset datos.tipoError= ''>
		<cfset datos.mensaje= 'Pago Agregado correctamente'>
		<cfreturn datos>
		<cfcatch type='database'>
			<cfset datos.agregado= FALSE>
			<cfset datos.tipoError= 'Error en el acceso a base de datos'>
			<cfset datos.mensaje= '#cfcatch.detail#'>
			<cfreturn datos>
		</cfcatch>
	</cftry>
</cffunction>



<cffunction name='bancosImportacion_nextId' access='public' returntype='struct'>
	<cfargument name="id_CuentaBancaria_imp" type="string" required="no" default="">
	
	<cftry>
		<cfquery name='RS' datasource='#session.cnx#'>
			select isnull(max(id_importacion_imp)  + 1 ,1) as nextId
			from bancosImportacion
			where id_cuentaBancaria_imp=#id_CuentaBancaria_imp# 
		</cfquery>

		<cfset datos.listado= TRUE>
		<cfset datos.tipoError= ''>
		<cfset datos.rs= #RS#>
		<cfset datos.mensaje= 'Se obtuvo el recordset correctamente'>
		<cfreturn datos>
		<cfcatch type='database'>
			<cfset datos.listado= FALSE>
			<cfset datos.tipoError= 'database-indefinido'>
			<cfset datos.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn datos>
		</cfcatch>
	</cftry>
</cffunction>

<cffunction name='bancosImportacion_agregar' access='public' returntype='struct'>
	<cfargument name="id_CuentaBancaria_imp" type="string" required="no" default="">
	<cfargument name="id_importacion_imp" 	 type="string" required="no" default="">
	<cfargument name="nu_anio" 				 type="string" required="no" default="">
	<cfargument name="nu_mes" 				 type="string" required="no" default="">
	<cfargument name="id_usuarioRegistro" 	 type="string" required="no" default="">
	<cfargument name="fh_registro" 			 type="string" required="no" default="">
	<cfargument name="id_estatus" 			 type="string" required="no" default="">
	
	<cftry>
		
		<cfinvoke component="#Application.componentes#.funciones" method="formateaFecha" returnvariable="fh_registro" formato_original="d/m/y" formato_nuevo="y/m/d" fecha="#fh_registro#" separador="/">
		<cfset fh_registro = de(fh_registro)>

		<cfquery name='RS' datasource='#session.cnx#'>
			bop_bancosImportacion_agregar #id_CuentaBancaria_imp#, #id_importacion_imp#, #nu_anio#, #nu_mes#, #id_usuarioRegistro#, #fh_registro#, #id_estatus#, 1
		</cfquery>

		<cfset datos.agregar= TRUE>
		<cfset datos.tipoError= ''>
		<cfset datos.mensaje= 'Se obtuvo el recordset correctamente'>
		<cfreturn datos>
		<cfcatch type='database'>
			<cfset datos.agregar= FALSE>
			<cfset datos.tipoError= 'database-indefinido'>
			<cfset datos.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn datos>
		</cfcatch>
	</cftry>
</cffunction>

<cffunction name='bancosImportacionMovientos_agregar' access='public' returntype='struct'>
	<cfargument name="id_CuentaBancaria_imp" type="string" required="no" default="">
	<cfargument name="id_importacion_imp" 	 type="string" required="no" default="">
	<cfargument name="QryMovimientos"		 type="any" required="no" default="">
	<cfset CqueryString ="">
	<cftry>
		<cfset lista = "" >
		<cfoutput query="QryMovimientos">

			<!-- validar que no los movimientos no existan en la base de datos -->
			<cfquery name="validarExistencia" datasource="#session.cnx#">
				select nu_movimiento from bancosImportacionMovimientos
				where id_cuentaBancaria_imp = #id_CuentaBancaria_imp# and nu_movimiento=#QryMovimientos.nu_movimiento#
			</cfquery>
			<!--  -->

			<cfif validarExistencia.recordcount EQ 0>

				<cfif im_deposito GT 0 and im_retiro EQ 0>
					<cfset local.im_movimiento = QryMovimientos.im_deposito >
					<cfset local.sn_ingresoEgreso= 'E'>
				</cfif>
				<cfif im_deposito EQ 0 and im_retiro GT 0>
					<cfset local.im_movimiento = QryMovimientos.im_retiro >
					<cfset local.sn_ingresoEgreso= 'S'>
				</cfif>		
				<cfinvoke component="#Application.componentes#.funciones" method="formateaFecha" returnvariable="fh_operacion" formato_original="d/m/y" formato_nuevo="y/m/d" fecha="#QryMovimientos.fh_operacion#" separador="/">
				<cfset local.fh_operacion = dateformat(fh_operacion,'yyyy-mm-dd')>
				<cfinvoke component="#Application.componentes#.funciones" method="formateaFecha" returnvariable="fh_fecha" formato_original="d/m/y" formato_nuevo="y/m/d" fecha="#QryMovimientos.fh_fecha#" separador="/">
				<cfset local.fh_fecha = dateformat(fh_fecha,'yyyy-mm-dd')>

				<cfset CqueryString = CqueryString & "
	                    INSERT INTO bancosImportacionMovimientos (
							id_cuentaBancaria_Imp
				           ,id_importacion_Imp
				           ,id_movimiento_Imp
				           ,nu_cuentaBancaria
				           ,fh_operacion
				           ,fh_fecha
				           ,de_referencia
				           ,de_descripcion
				           ,nu_codigoTransaccion
				           ,de_sucursal
				           ,im_movimiento
				           ,im_saldo
				           ,nu_movimiento
				           ,de_descripcionDetallada
				           ,sn_ingresoEgreso
				           ,fh_registro
				           ,id_usuarioRegistro
				           ,id_estatus
				           ,sn_conciliado)            	
	                    values(
	                    	#id_CuentaBancaria_imp#
	                    	,#id_importacion_imp#  
	                    	,#currentrow# 
	                    	,#QryMovimientos.nu_cuentaBancaria#
	                    	,'#local.fh_operacion#'
							,'#local.fh_fecha#'
							,'#QryMovimientos.de_referencia#'
							,'#QryMovimientos.de_descripcion#'
							,#QryMovimientos.nu_codigoTrasaccion#
							,'#QryMovimientos.de_sucursal#'
							,#local.im_movimiento#
							,#QryMovimientos.im_saldo#
							,#QryMovimientos.nu_movimiento#
							,'#QryMovimientos.de_descripcionDetallada#'
							,'#local.sn_ingresoEgreso#'
							,'#dateformat(now(),'yyyy-mm-dd')#'
							,#session.id_usuario#
							,#0#
							,#0#
	                         )
	                     "/>
	        <cfelse>
	        	<cfset  lista = lista & '#QryMovimientos.nu_movimiento#,' >
			</cfif>                    

		</cfoutput>
		<cfif lista NEQ ''>
			<cfset  lista = listToArray(lista, ",", false, true)>
			<cfset ArraySort(lista, "textnocase", "asc")>	
			<cfset mensaje = "">
			<cfloop array="#lista#" index="i"><cfset mensaje = mensaje & '#i#,' ></cfloop>
			<cfset datos.agregar= FALSE>
			<cfset datos.tipoError= 'registro duplicados'>
			<cfset datos.mensaje= 'Los movimientos: #mensaje#' & ' estan duplicados, favor de revisar'>
			<cfreturn datos>	
			<cfabort>
		</cfif>

        <cfquery datasource="#cnx#" name="movAgregar"> 
            #PreserveSingleQuotes(CqueryString)#
        </cfquery>

		<cfset datos.agregar= TRUE>
		<cfset datos.tipoError= ''>
		<cfset datos.mensaje= 'Se guardo correctamente'>
		<cfreturn datos>
		<cfcatch type='database'>
			<cfset datos.agregar= FALSE>
			<cfset datos.tipoError= 'database-indefinido'>
			<cfset datos.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn datos>
		</cfcatch>
	</cftry>
</cffunction>
  <cffunction name="reporte_desglosado_saldos_detalle" access="public" returntype="struct">
        <cfargument name="id_Empresa"    type="string" default="">
        <cfargument name="id_Obra"       type="string" default="">
        <cfargument name="id_Agrupador"  type="string" default="">

       <cftry>
        
            <cfquery name="RS" datasource="#cnx#"  username='#user_sql#' password='#password_sql#'>
              
                    EXEC reporte_desglosado_saldos_detalle #id_Empresa#,<cfif #id_Obra# NEQ ''>
           															#id_Obra#
           														<cfelse>
           															NULL   
        														</cfif>, 
        														<cfif #id_Agrupador# NEQ ''>
           															'#id_Agrupador#'
           														<cfelse>
           															NULL   
        														</cfif>
        	</cfquery>

            <cfset myResult.SUCCESS = true> 
            <cfset myResult.RS = Rs>
            <cfcatch type="database">
                <cfset myResult.SUCCESS = false>    
                <cfset myResult.MESSAGE = 'Error en acceso a base de datos, #cfcatch.Detail#'>
            </cfcatch>  
        </cftry>
        <cfreturn myResult>
</cffunction>

</cfcomponent>
