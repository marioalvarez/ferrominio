function tabular(e,obj) { 
  tecla=(document.all) ? e.keyCode : e.which; 
  if(tecla!=13) return; 
  frm=obj.form; 
  for(i=0;i<frm.elements.length;i++)  
    if(frm.elements[i]==obj) {  
      if (i==frm.elements.length-1) i=-1; 
      break } 
  frm.elements[i+1].focus(); 
  return false; 
} 
// JavaScript Document
/*Codigo para iniciar controles segun su className*/
doc_x=$(document);
doc_x.ready(inicializarControles);
function inicializarControles(){
	$(".contenido").keydown(escanear_enter);
	$(".contenido").keypress(escanear_minusculas);
	$(".contenido_readOnly").keypress(escanear_readOnly);
	$(".contenido_readOnly").keydown(escanear_enter);
	$(".contenido_readOnly").keydown(escanear_retorno_readOnly);
	$(".contenido_libre").keydown(escanear_enter);
	$(".contenido_filtro").keypress(escanear_minusculas);
	$(".contenido_numerico").keypress(escanear_numerico);
	$(".contenido_numerico").keydown(escanear_enter);
	$(".contenido_alfanumerico_con_espacios").keydown(escanear_enter);
	$(".contenido_alfanumerico_con_espacios").keypress(escanear_minusculas);
	$(".contenido_alfanumerico_con_espacios").keypress(escanear_alfanumerico_con_espacios);
	$(".contenido_flotante").keypress(escanear_flotante);
	$(".contenido_flotante").keydown(escanear_enter);
	$(".contenido_flotante_negativo").keypress(escanear_flotante_negativo);
	$(".contenido_flotante_negativo").keydown(escanear_enter);
	$(".contenido_moneda").keypress(escanear_flotante);
	$(".contenido_moneda").keydown(escanear_enter);
	$(".contenido_moneda").focus(focus_money);
	$(".contenido_moneda").blur(blur_money);
	$(".contenido_textarea").keypress(escanear_minusculas);
	$(".boton_popup").keydown(escanear_enter);
	$(".renglon_grid").mouseover(mouseOverRenglon);
	$(".renglon_grid").mouseout(mouseOutRenglon);
	//$("a").mouseover(setEstatusWindowEnBlanco);
	}

function mouseOverRenglon(){
	$(this).css("background", '#99CCCC');
	}
function mouseOutRenglon(){
	$(this).css("background", '');
	}
function setEstatusWindowEnBlanco(){
	window.status= '';
	return true;
	}

function focus_money(){
	this.value= unformatNumber(this.value);
	this.select();
	}

function blur_money(){
	this.value= formatMoney(this.value, '$');
	}

function formatMoney(numero, prefijo){
	if (numero == "")
		return "";
	numero = Math.round(parseFloat(numero)*Math.pow(10,4))/Math.pow(10,4)
	prefijo = prefijo || '';
	numero += '';
	var splitStr = numero.split('.');
	var splitLeft = splitStr[0];
	var splitRight = splitStr.length > 1 ? '.' + splitStr[1] : '.0000';
	splitRight = splitRight + '0000';
	splitRight = splitRight.substr(0,5);
	var regx = /(\d+)(\d{3})/;
	while (regx.test(splitLeft)){
		splitLeft = splitLeft.replace(regx, '$1' + ',' + '$2');
		}
	return prefijo + splitLeft + splitRight;
	}
	
function formatDecimal(numero){
	if (numero == "")
		return "";
	numero = Math.round(parseFloat(numero)*Math.pow(10,2))/Math.pow(10,2)
	//prefijo = prefijo || '';
	numero += '';
	var splitStr = numero.split('.');
	var splitLeft = splitStr[0];
	var splitRight = splitStr.length > 1 ? '.' + splitStr[1] : '.00';
	splitRight = splitRight + '00';
	splitRight = splitRight.substr(0,3);
	var regx = /(\d+)(\d{3})/;
	while (regx.test(splitLeft)){
		splitLeft = splitLeft.replace(regx, '$1' + ',' + '$2');
		}
	return /*prefijo*/ + splitLeft + splitRight;
	}

function unformatNumber(numero){
	if (numero == "")
		return "";
	return numero.replace(/([^0-9\.\-])/g,'')*1;
	} 
	
/*obtiene un elemento por su ID*/
function $x() {
	var elements = new Array();
	for (var i = 0; i < arguments.length; i++) {
		var comp = null;
		var element = arguments[i];
		if (typeof element == 'string')
			comp = document.getElementById(element);
			if(comp == null) comp = document.getElementsByName(element)[0];
		if (arguments.length == 1)
			return comp;
		elements.push(comp);
		}
	return elements;
	}
function escanear_enter(){
	tecla=(document.all) ? e.keyCode : e.which;
  	if(tecla==13) {
		if(document.all){
			if (window.event.keyCode==13) window.event.keyCode= 9;
		}
		else return false;

	}
	}

function escanear_minusculas(){
	if (window.event.keyCode < 123 && window.event.keyCode > 96)
		window.event.keyCode-= 32;
	}
function escanear_readOnly(){
	window.event.keyCode= 0;	
	}
function escanear_retorno_readOnly(){
	if (window.event.keyCode==8 || window.event.keyCode== 46){
		/*el returnValue= false, es para detener el evento de backspace*/
		window.event.returnValue= false;
		window.event.keyCode= 0;
		}
	}
function escanear_numerico(){
	if (event.keyCode < 48 || event.keyCode > 57) 
		event.returnValue = false;
	}
function escanear_alfanumerico_con_espacios(){
	if ( !(event.keyCode >= 48 && event.keyCode <= 57) && !(event.keyCode >= 65 && event.keyCode <= 90) && !(event.keyCode >= 97 && event.keyCode <= 122) && !(event.keyCode == 32)) 
		event.returnValue = false;
	//else
	//	event.returnValue = true;
	}
function escanear_flotante(input_txt){
	if (event.keyCode < 48 || event.keyCode > 57){
		if (event.keyCode != 46){
			event.returnValue = false;
			}
		else {
			if (arguments[0].type == 'keypress'){
				if ( this.value.indexOf('.') > -1 )
					event.returnValue = false;
				}				
			else {
				if ( arguments[0].value.indexOf('.') > -1 ){
					event.returnValue = false;
					}
				}				
			}
		}
	}
	
function escanear_flotante_negativo(input_txt){
	if (event.keyCode < 48 || event.keyCode > 57){
		if (event.keyCode != 46 && event.keyCode != 45){
			event.returnValue = false;
			}
		//si es 46 o 45
		else {
			//si es el punto decimal
			if (event.keyCode == 46){
				if (arguments[0].type == 'keypress'){
					if ( this.value.indexOf('.') > -1 )
						event.returnValue = false;
					}				
				else {
					if ( arguments[0].value.indexOf('.') > -1 ){
						event.returnValue = false;
						}
					}				
				}
			//si no es entonces es el signo negativo
			else{			
				if (arguments[0].type == 'keypress'){
					if ( this.value != '' )
						event.returnValue = false;
					}				
				else {
					if ( arguments[0].value != '' ){
						event.returnValue = false;
						}
					}
				}
			//aqui termina el else de si es 46 o 45
			}
		//aqui termina el primer if de si es menor a 48 y mayor a 57
		}
	//aqui termina la funcion
	}
	
/*funciones que quita los espacios en blanco de una cadena*/
String.prototype.trim = function() {
	return this.replace(/^\s+|\s+$/g,"");
}
String.prototype.ltrim = function() {
	return this.replace(/^\s+/,"");
}
String.prototype.rtrim = function() {
	return this.replace(/\s+$/,"");
}

/*Abre una ventana tipo Pop Up tipo modal*/
function AbrirPopUpBusqueda(idControlRecibeValor, strPaginaBusqueda, strParametrosPagina, anchoPagina, altoPagina){
	identificador= new Date();
	ControlRecibeValor= $x(idControlRecibeValor);
	valor_devuelto= showModalDialog(strPaginaBusqueda + '?identificador=' + identificador.getTime() + '&' + strParametrosPagina,'','dialogHeight:' + altoPagina + ';dialogWidth:' + anchoPagina + ';');
	cambiarValorConEventoOnChange(idControlRecibeValor, valor_devuelto);
	ControlRecibeValor.select();	
	}	
	
/*Abre una ventana tipo Pop Up tipo modal*/
function AbrirPopUpBusquedaRegresaArreglo(idControlRecibeValor, strPaginaBusqueda, strParametrosPagina, anchoPagina, altoPagina){
	identificador= new Date();
	//ControlRecibeValor= $x(idControlRecibeValor);
	valor_devuelto= showModalDialog(strPaginaBusqueda + '?identificador=' + identificador.getTime() + '&' + strParametrosPagina,'','dialogHeight:' + altoPagina + ';dialogWidth:' + anchoPagina + ';');
	$x(idControlRecibeValor).value= valor_devuelto;
	}	

/*funcion que cambia el valor de un input invocando al evento onchange*/
function cambiarValorConEventoOnChange(id_input, valor_nuevo){
	document.getElementById(id_input).value = valor_nuevo;
	if (document.getElementById(id_input).onchange)
		document.getElementById(id_input).onchange();
}

/*agrega funciones al body.onload, parametro: funcion*/
function addLoadEvent(func){
	var oldonload = window.onload;
	if (typeof window.onload != 'function'){
		window.onload = func;
		}
	else {
		window.onload = function(){
			oldonload();
			func();
			}
		}
	}
/*Abre una ventana tipo Pop Up tipo modal para Editar o Agregar*/
function AbrirPopUpAgregarEditar(refrescar, strPaginaPopUp, strParametrosPaginaPopUp, anchoPagina, altoPagina){
	identificador= new Date();
	modificado= showModalDialog(strPaginaPopUp + '?identificador=' + identificador.getTime() + '&' + strParametrosPaginaPopUp,'','dialogHeight:' + altoPagina + ';dialogWidth:' + anchoPagina + ';');
	if (refrescar == 'true' && modificado == 'true'){
		window.location.reload();
		}
	}
/*Abre una ventana tipo Pop Up tipo modal para Editar o Agregar*/
function AbrirPopUpAgregarEditarPresupuesto(strPaginaOrigen, strPaginaPopUp, strParametrosPaginaPopUp, anchoPagina, altoPagina){
	identificador= new Date();
	Modificado= showModalDialog(strPaginaPopUp + '?identificador=' + identificador.getTime() + '&' + strParametrosPaginaPopUp,'','dialogHeight:' + altoPagina + ';dialogWidth:' + anchoPagina + ';');
	if (Modificado == 'true'){
		window.location.href= strPaginaOrigen;
		}
	}
/*Abre una ventana tipo Pop Up tipo modal para Editar o Agregar una tarjeta al presupuesto*/
function AbrirPopUpPresupuestoAgregarEditarEliminarTarjeta(funcionCallBack, strPaginaPopUp, strParametrosPaginaPopUp, anchoPagina, altoPagina){
	identificador= new Date();
	datosModificacionTarjeta= showModalDialog(strPaginaPopUp + '?identificador=' + identificador.getTime() + '&' + strParametrosPaginaPopUp,'','dialogHeight:' + altoPagina + ';dialogWidth:' + anchoPagina + ';');
	if (datosModificacionTarjeta.Modificado == 'true'){
		funcionCallBack(datosModificacionTarjeta);
		}
	}
	
	
/*Abre una ventana tipo Pop Up tipo modal para Editar o Agregar*/
function AbrirPopUpCopiarTarjeta(strPaginaPopUp, strParametrosPaginaPopUp, anchoPagina, altoPagina){
	identificador= new Date();
	modificado= showModalDialog(strPaginaPopUp + '?identificador=' + identificador.getTime() + '&' + strParametrosPaginaPopUp,'','dialogHeight:' + altoPagina + ';dialogWidth:' + anchoPagina + ';');
	return modificado;
	}
	
/*Abre una ventana tipo Pop Up tipo modal para Editar o Agregar, devuelve true si se realizo el agregar o el actualizar*/
function AbrirPopUpAgregarEditarDinamico(strPaginaPopUp, strParametrosPaginaPopUp, anchoPagina, altoPagina){
	identificador= new Date();
	modificado= showModalDialog(strPaginaPopUp + '?identificador=' + identificador.getTime() + '&' + strParametrosPaginaPopUp,'','dialogHeight:' + altoPagina + ';dialogWidth:' + anchoPagina + ';');
	return modificado;
	}
	
function FormToURL(id_forma){
	var forma= $x(id_forma);
	var cadena_url= '?';
	for (var ind = 0; ind < forma.length; ind++){
		cadena_url= cadena_url + forma.elements[ind].name + '=' + forma.elements[ind].value + '&';
		}
	cadena_url= cadena_url.substring(0, cadena_url.length - 1);
	return cadena_url;
	}
/*addEvent*/
/*
function addEvent(elm, evType, fn, useCapture) {
	if (elm.addEventListener) {
		elm.addEventListener(evType, fn, useCapture);
		return true;
	}
	else if (elm.attachEvent) {
		var r = elm.attachEvent('on' + evType, fn);
		return r;
	}
	else {
		elm['on' + evType] = fn;
	}
}
	*/
/*Pon el foco en un control por su id como parametro*/
function ponerFocus(idControl){
	try{
		document.getElementById(idControl).focus();
		}
	catch(err){};
	}
/**/
function AjaxTablaDinamica(strParametros, id_LabelDestino, paginaFuente){
	lblDestino= $("#" + id_LabelDestino);
	identificador= new Date();
	time= identificador.getTime();
	strParametros= strParametros + "&identificador_unico=" + time.toString();
  $.ajax({
		async:true,
		type: "GET",
	 	dataType: "text",
	 	contentType: "application/x-www-form-urlencoded",
		url: paginaFuente,
		data: strParametros,
		complete: function (){
			//$x('busqueda_de_Obra').focus();
			},
	 	beforeSend: function (){
			lblDestino.html('<img src="../images/cargando.gif" title="Descargando Datos"');
			},
	 	success: function (respuestaHtml){
			lblDestino.html(respuestaHtml);
			},
	 	timeout:5000,
	 	error:function (){
			lblDestino.html('<img src="../images/warning_20x18.gif" title="Error en Conexi�n">');
			}
 		}); 
	}

function TraerDescripcionPorAjax(strParametros, id_LabelDestino, id_BotonSubmit, paginaFuente){
	lblDestino= $("#" + id_LabelDestino);
	btnSubmit= $x(id_BotonSubmit);
  $.ajax({
		async:true,
		type: "POST",
	 	dataType: "text",
	 	contentType: "application/x-www-form-urlencoded",
		url: paginaFuente,
		data: strParametros,
	 	beforeSend: function (){
			lblDestino.html('<img src="../images/cargando.gif" title="Descargando Datos">');
			btnSubmit.disabled= "disabled";
			},
	 	success: function (respuestaHtml){
			lblDestino.html(respuestaHtml);
			btnSubmit.disabled= "";
			},
	 	timeout:5000,
	 	error:function (){
			lblDestino.html('<img src="../images/warning_20x18.gif" title="Error en Conexi�n">');
			}
 		}); 
	}
	//variable para mostrar mientras se descarga contenido ajax
	html_imagen_descarga= '<img src="../images/cargando.gif" title="Descargando Datos">';
/**/

function AjaxPaginaDinamica(strParametros, id_LabelDestino, paginaFuente, jsIniciaControles){
	lblDestino= $("#" + id_LabelDestino);
  $.ajax({
		async:true,
		type: "POST",
	 	dataType: "text",
	 	contentType: "application/x-www-form-urlencoded",
		url: paginaFuente,
		data: strParametros,
		complete: function (){
			$x('busqueda_de_Obra').focus();
			},
	 	beforeSend: function (){
			lblDestino.html('<img src="../images/cargando.gif" title="Descargando Datos"');
			},
	 	success: function (respuestaHtml){
			lblDestino.html(respuestaHtml);
			},
	 	timeout:5000,
	 	error:function (){
			lblDestino.html('<img src="../images/warning_20x18.gif" title="Error en Conexi�n">');
			}
 		}); 
	}
function AjaxSubmit(strParametros, id_CampoDestino, paginaFuente){
	lblDestino= $("#" + id_CampoDestino);
  $.ajax({
		async:true,
		type: "POST",
	 	dataType: "text",
	 	contentType: "application/x-www-form-urlencoded",
		url: paginaFuente,
		data: strParametros,
		complete: function (){
			//$x('busqueda_de_Obra').focus();
			},
	 	beforeSend: function (){
			lblDestino.html('<img src="../images/cargando.gif" title="Descargando Datos"');
			},
	 	success: function (respuestaHtml){
			lblDestino.html(respuestaHtml);
			},
	 	timeout:5000,
	 	error:function (){
			lblDestino.html('<img src="../images/warning_20x18.gif" title="Error en Conexi�n">');
			}
 		}); 
	}
	

// Funcion para formatear numeros
function oNumero(numero)
	{
	//Propiedades 
	this.valor = numero || 0
	this.dec = -1;
	//M�todos 
	this.formato = numFormat;
	this.ponValor = ponValor;
	//Definici�n de los m�todos 
	function ponValor(cad)
	{
	if (cad =='-' || cad=='+') return
	if (cad.length ==0) return
	if (cad.indexOf('.') >=0)
		this.valor = parseFloat(cad);
	else 
		this.valor = parseInt(cad);
	} 
	function numFormat(dec, miles)
	{
	var num = this.valor, signo=3, expr;
	var cad = ""+this.valor;
	var ceros = "", pos, pdec, i;
	for (i=0; i < dec; i++)
	ceros += '0';
	pos = cad.indexOf('.')
	if (pos < 0)
		cad = cad+"."+ceros;
	else
		{
		pdec = cad.length - pos -1;
		if (pdec <= dec)
			{
			for (i=0; i< (dec-pdec); i++)
				cad += '0';
			}
		else
			{
			num = num*Math.pow(10, dec);
			num = Math.round(num);
			num = num/Math.pow(10, dec);
			cad = new String(num);
			}
		}
	pos = cad.indexOf('.')
	if (pos < 0) pos = cad.lentgh
	if (cad.substr(0,1)=='-' || cad.substr(0,1) == '+') 
		   signo = 4;
	if (miles && pos > signo)
		do{
			expr = /([+-]?\d)(\d{3}[\.\,]\d*)/
			cad.match(expr)
			cad=cad.replace(expr, RegExp.$1+','+RegExp.$2)
			}
	while (cad.indexOf(',') > signo)
		if (dec<0) cad = cad.replace(/\./,'')
			return cad;
	}
}//Fin del objeto oNumero:

function compare_dates(fecha_final, fecha_inicial)   
  {  
	
	if (fecha_inicial == "" || fecha_final == "")
		{
			return(true);
		}else{
			var xDay=fecha_final.substring(0,2); 
			var xMonth=fecha_final.substring(3,5);   
			var xYear=fecha_final.substring(6,10);   
			var yDay=fecha_inicial.substring(0,2);   
			var yMonth=fecha_inicial.substring(3,5);
			var yYear=fecha_inicial.substring(6,10);   
			if (xYear> yYear)   
			{   
					return(true)   
			}   
			else  
			{   
				if (xYear == yYear)   
				{    
					if (xMonth> yMonth)   
					{   
							return(true)   
					}   
					else  
					{    
						if (xMonth == yMonth)
						
						{   
							if (xDay > yDay){   
								return(true);  
							}
							else if(xDay == yDay){
								return(true);
							}
							else{  
								return(false);   
							}
						}   
						else  
							return(false);   
					}   
				}   
				else  
					return(false);   
			} 
		}
}
function compare_periodos_contables(fecha_final, fecha_inicial){  

	if (fecha_inicial == "" || fecha_final == ""){
		return(true);
	}else{
		//	var xDay=fecha_final.substring(0,2); 
		var xMonth=fecha_final.substring(0,2);   
		var xYear=fecha_final.substring(3,7);   
		//	var yDay=fecha_inicial.substring(0,2);   
		var yMonth=fecha_inicial.substring(0,2);
		var yYear=fecha_inicial.substring(3,7);   
		if (xYear != yYear){   
			alert('El a' +String.fromCharCode(241) +'o del Periodo Contable Inicial y Final debe ser el mismo.');
			return(false);   
		}else{   
			if (xMonth>= yMonth){   
				return(true);   
			}else{    
				alert('El periodo Inicial es mayor al periodo Final.');
				return(false);   
			}
		}   
	}
}
/* Usado en las toolbar de als paginas */
function page_back(url){
	if(document.referrer == document.location) document.location = url;
	else history.go(-1);
}