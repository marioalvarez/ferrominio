����  - � 
SourceFile GC:\ColdFusion9\wwwroot\ferrominio\redproject\operaciones\peticiones.cfm cfpeticiones2ecfm453465740  coldfusion/runtime/CFPage  <init> ()V  
  	 com.macromedia.SourceModTime  J1� coldfusion/runtime/CfJspPage  pageContext #Lcoldfusion/runtime/NeoPageContext;  	   getOut ()Ljavax/servlet/jsp/JspWriter;   javax/servlet/jsp/PageContext 
   parent Ljavax/servlet/jsp/tagext/Tag;  	   com.adobe.coldfusion.*  bindImportPath (Ljava/lang/String;)V   !
  " 
 $ _whitespace %(Ljava/io/Writer;Ljava/lang/String;)V & '
  ( &class$coldfusion$tagext$lang$InvokeTag Ljava/lang/Class;  coldfusion.tagext.lang.InvokeTag , forName %(Ljava/lang/String;)Ljava/lang/Class; . / java/lang/Class 1
 2 0 * +	  4 _initTag P(Ljava/lang/Class;ILjavax/servlet/jsp/tagext/Tag;)Ljavax/servlet/jsp/tagext/Tag; 6 7
  8  coldfusion/tagext/lang/InvokeTag : _setCurrentLineNo (I)V < =
  > APPLICATION @ java/lang/String B RF D _resolve 9(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/Object; F G
  H getApp J java/lang/Object L 	catalogos N _invoke K(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object; P Q
  R setComponent (Ljava/lang/Object;)V T U
 ; V Recursos_Listar X 	setMethod Z !
 ; [ Recursos_Listado ] setReturnVariable _ !
 ; ` &coldfusion/runtime/AttributeCollection b 
id_empresa d REQUEST f SESSION h 
ID_EMPRESA j _resolveAndAutoscalarize l G
  m 	sn_activo o 1 q ([Ljava/lang/Object;)V  s
 c t setAttributecollection (Ljava/util/Map;)V v w
 ; x 	hasEndTag (Z)V z { coldfusion/tagext/GenericTag }
 ~ | _emptyTcfTag !(Ljavax/servlet/jsp/tagext/Tag;)Z � �
  �r

<script type="text/javascript">
	//Variables globales.
	var g_row_proyectos;
	var g_row_peticiones;
	
	
	//Funció para cuando se da clic sobre un proyecto.
	function fnc_ver_peticiones()
	{
		//Validamos que se haya seleccionado un renglón.
		var row = ggrids.obtener_renglon("grid_proyectos");

		if( row )
		{
			g_row_proyectos = row;
			
			//Establecemos los parámetros.
			$("#grid_proyectos_peticiones").attr({
				titulo: "Listado de Peticiones - " + row.nb_Proyecto, 
				parametros: "&id_Empresa=" + row.id_Empresa + "&id_Cliente=" + row.id_Cliente + "&id_Proyecto=" + row.id_Proyecto
			});
			
			ggrids.crear("grid_proyectos_peticiones");
			$("#div_proyectos_peticiones").show();
		}
		else
			fnc_notificacion( "Debe seleccionar un proyecto." );
	}
	
	//Función para abrir la opción de agregar, editar o eliminar.
	function fnc_peticiones_agregar_editar_eliminar(p_accion)
	{
		if( p_accion == 1 ) //Agregar.
		{
			$("#div_titulo_agregar_editar").html("NUEVA PETICI&Oacute;N");
			$("#btn_guardar").attr("title", "Guardar").text("Guardar");
			
			//Nos aseguramos de desbloquear los input.
			$("#txt_de_Descripcion, #txt_de_Comentarios, #txt_fh_Inicio, #txt_fh_fin, #txt_nu_Horas, #ddl_recursos").removeAttr("disabled");
			
			fnc_catalogos_mostrar_ocultar( "div_grid", "div_guardar_editar_eliminar" );
		}
		else if( p_accion == 2 || p_accion == 3 ) //Editar o Eliminar.
		{
			//Validamos que se haya seleccionado un renglón.
			var row = ggrids.obtener_renglon("grid_proyectos_peticiones");
			
			if( row )
			{	
				g_row_peticiones = row;

				//Llenamos los campos.
				$("#txt_de_Descripcion").val( row.de_Peticion );
				$("#txt_de_Comentarios").val( row.de_Comentarios );
				
				//Valdiar si debemos bloquear o desbloquear los input.
				if( p_accion == 3 )
				{
					$("#div_titulo_agregar_editar").html("ELIMINAR PETICI&Oacute;N");
					$("#btn_guardar").attr("title", "Eliminar").text("Eliminar");
					$("#txt_de_Descripcion, #txt_de_Comentarios, #txt_fh_Inicio, #txt_fh_fin, #txt_nu_Horas, #ddl_recursos").attr("disabled", "disabled");
				}
				else
				{
					$("#div_titulo_agregar_editar").html("EDITAR PETICI&Oacute;N");
					$("#btn_guardar").attr("title", "Guardar").text("Guardar");
					$("#txt_de_Descripcion, #txt_de_Comentarios, #txt_fh_Inicio, #txt_fh_fin, #txt_nu_Horas, #ddl_recursos").removeAttr("disabled");
				}
				
				fnc_catalogos_mostrar_ocultar( "div_grid", "div_guardar_editar_eliminar" );
			}
			else
				fnc_notificacion( "Debe seleccionar una petici&oacute;n." );
		}
		
		//Retringimos las fechas.
		$("#txt_fh_Inicio, #txt_fh_fin").datepicker( "option", "minDate", fnc_fecha_objeto( $("#hid_fec_actual").val(), true ) );
		
		$("#hid_accion").val( p_accion );
		
		//Limpiamos los campos.
		if( p_accion == 1 )
		{
			$("#txt_de_Descripcion").val("");
			$("#txt_de_Comentarios").val("");
		}
		
		$("#txt_de_Descripcion").focus();
	}
	
	//Función para guardar, editar o eliminar.
	function fnc_peticiones_crud()
	{ 
		if( fnc_validar_campos_obligatorios("div_nueva_peticion") )
		{
			//Armamos los parámtros.
			var parametros = "id_Empresa=" + g_row_proyectos.id_Empresa
				+ "&id_Cliente=" +g_row_proyectos.id_Cliente
				+ "&id_Proyecto=" + g_row_proyectos.id_Proyecto;
			
			switch( $("#hid_accion").val() )
			{
				case "1": //Agregar.
							parametros += "&method=Peticiones_Agregar&de_Peticion=" + fnc_codificar( $.trim( $("#txt_de_Descripcion").val() ) )
								+ "&de_Comentarios=" + fnc_codificar( $.trim( $("#txt_de_Comentarios").val() ) );
							break;
				case "2": //Editar
							parametros += "&method=Peticiones_Editar&id_Peticion=" + g_row_peticiones.id_Peticion
								+ "&de_Peticion=" + fnc_codificar( $.trim( $("#txt_de_Descripcion").val() ) )
								+ "&de_PeticionViejo=" + g_row_peticiones.de_Peticion
								+ "&de_Comentarios=" + fnc_codificar( $.trim( $("#txt_de_Comentarios").val() ) );
							break;
				case "3": //Eliminar
							parametros += "&method=Peticiones_Eliminar&id_Peticion=" + g_row_peticiones.id_Peticion;
							break;
			}
			
			//Guardamos.
			fnc_peticionAjax({
				url: "./application/adm_proy.cfc",
				parametros: parametros,
				f_callback: function(){
					//Actualizamos el grid.
					$("#grid_grid_proyectos_peticiones").trigger("reloadGrid");
												
					//Limpiamos los campos.
					if( $("#hid_accion").val() == "1" )
					{
						$("#txt_de_Descripcion").val("");
						$("#txt_de_Comentarios").val("");
					}
					else
						fnc_catalogos_mostrar_ocultar( "div_grid", "div_guardar_editar_eliminar" );
					
					//Si está visible el Gantt, lo actualizamos.
					//if( $("#div_gantt_prospeccion").is(":visible") )
						//fnc_gantt_prospeccion();
				}
			});
		}
	}
	
	//Funció para mostrar el Gantt.
	/*function fnc_gantt_prospeccion()
	{
		//Validar si existen peticiones.
		if( $("#grid_grid_proyectos_peticiones").getDataIDs().length > 0 )
		{
			fnc_peticionAjax({
				url: "./application/adm_proy.cfc",
				parametros: "method=Peticiones_ListarXML&id_Empresa=" + g_row_proyectos.id_Empresa + "&id_Cliente=" + g_row_proyectos.id_Cliente 
					+ "&id_Proyecto=" + g_row_proyectos.id_Proyecto,
				div_success: "div_peticion_ajax_gantt",
				div_error: "gd_errores",
				automensaje: false,
				f_callback: function(){
					var actividadesXML = $.parseXML( $.trim( $("#div_peticion_ajax_gantt").html() ) );
					
					if( $(actividadesXML).find("fechas").attr("fecha_inicio") != "" &&  $(actividadesXML).find("fechas").attr("fecha_fin") != "" )
					{	
						ggantt.crear({
							id: "div_gantt_prospeccion_contenido",
							peticionesXML: $.parseXML( $.trim( $("#div_peticion_ajax_gantt").html() ) )
						});
						
						$("#div_gantt_prospeccion").slideDown(200);
					}
					else
						fnc_mostrar_aviso( "gd_avisos", "No existen actividades con fecha definida.", 5 );
				}
			});
		}
		else
			fnc_mostrar_aviso( "gd_avisos", "No existen peticiones.", 5 );
	}*/
</script>

<center>
    <div style="margin-left: 5%; margin-right: 5%; width: 90%;">
        <div class="ui-widget-header titulo_opcion">PETICIONES</div>
        <div style="padding: 5px; margin-top: 10px;">
        	<div class="gfieldset" titulo="Proyectos"></div> 
			
            <div id="grid_proyectos" class="ggrids" url="application/catalogos_Victor.cfc?method=Grid_Proyectos&id_Empresa= � write � ! java/io/Writer �
 � � $class$coldfusion$tagext$io$OutputTag coldfusion.tagext.io.OutputTag � � +	  � coldfusion/tagext/io/OutputTag � 
doStartTag ()I � �
 � � _String &(Ljava/lang/Object;)Ljava/lang/String; � � coldfusion/runtime/Cast �
 � � doAfterBody � �
 � � doEndTag � � coldfusion/tagext/QueryLoop �
 � � doCatch (Ljava/lang/Throwable;)V � �
 � � 	doFinally � 
 � ��" titulo="Listado de Proyectos"
                    sortname="nb_Proyecto" autowidth="true" loadComplete="fnc_autoajustar_ancho_busqueda('grid_proyectos','div_busqueda')">
                    
                    <columna nom_columna="de_Iniciales">Regi&oacute;n</columna>
                    <columna nom_columna="nu_NumTienda">No.Tienda</columna>
                    <columna nom_columna="nb_Proyecto">Nombre</columna>
                    <columna nom_columna="nb_TipoObra">Tipo De Obra</columna>
                    <columna nom_columna="de_Proyecto">Descripci&oacute;n</columna>
                    <columna nom_columna="nb_Cliente">Cliente</columna>
                    <columna nom_columna="de_Estatus">Estatus</columna>
                    <columna nom_columna="nb_PrioridadProyecto">Prioridad</columna>
                    
                    
                    <columna nom_columna="id_Empresa" visible="false"></columna>
                    <columna nom_columna="id_Cliente" visible="false"></columna>
                    <columna nom_columna="id_Proyecto" visible="false"></columna>
                    <columna nom_columna="id_EstatusProyecto" visible="false"></columna>
                    <columna nom_columna="id_Horario" visible="false"></columna>
                    <columna nom_columna="id_TipoObra" visible="false"></columna>
                    <columna nom_columna="id_Prioridad" visible="false"></columna>
                    <columna nom_columna="id_Estado" visible="false"></columna>
                    <columna nom_columna="id_Ciudad" visible="false"></columna>
                    <columna nom_columna="id_Region" visible="false"></columna>
                    <columna nom_columna="de_Objetivo" visible="false"></columna>
                
                
                <accion accion="personalizada" class_icono="ui-icon ui-icon-search" titulo="Ver peticiones" evento="fnc_ver_peticiones();"></accion>
            </div>
        </div>

		
        <div id="div_proyectos_peticiones" style="padding: 5px; display: none;">
            <div class="gfieldset" titulo="Peticiones"></div>
            <div id="div_grid">
                
                <div id="grid_proyectos_peticiones" class="ggrids" url="application/adm_proy.cfc?method=Peticiones_Listar_JSON" paginado="10" 
                	titulo="Listado de Peticiones Proyectos" autocrear="false" ancho="800">
                    
                    <columna nom_columna="de_Peticion">Petici&oacute;n</columna>
                    <columna nom_columna="de_Comentarios">Comentarios</columna>
                    
                    
                    <columna nom_columna="id_Empresa" visible="false"></columna>
                    <columna nom_columna="id_Cliente" visible="false"></columna>
                    <columna nom_columna="id_Proyecto" visible="false"></columna>
                    <columna nom_columna="id_Peticion" visible="false"></columna>
                    
                    
                    <accion accion="agregar" titulo="Agregar" evento="fnc_peticiones_agregar_editar_eliminar(1);"></accion>
                    <accion accion="editar" evento="fnc_peticiones_agregar_editar_eliminar(2);"></accion>
                    <accion accion="eliminar" evento="fnc_peticiones_agregar_editar_eliminar(3);"></accion>
                </div>
            </div>
            
            
            <div id="div_guardar_editar_eliminar" class="catalogos_guardar_editar">
                <div class="ui-widget-header catalogos_guardar_editar_cabecero" id="div_titulo_agregar_editar"></div>
                <div class="catalogos_guardar_editar_cabecero_contenido" id="div_nueva_peticion">
                    <table style="width: 100%;"> 
                        <tr>
                            <td style="text-align: left;"><span>Descripci&oacute;n: </span></td>
                            <td style="width: 98%;">
                            	<input class="input_text obligatorio" acercade="Descripci&oacute;n" type="text" id="txt_de_Descripcion" 
                                	maxlength="120" style="width: 100%;" />
                            </td>
                        </tr> 
                        <tr>
                            <td style="text-align: left;"><span>Comentarios: </span></td>
                            <td>
                            	<textarea class="input_text" id="txt_de_Comentarios" style="width: 100%; height: 60px;"></textarea>
                            </td>
                        </tr> 
                    </table>
                    
                    <div style="margin-top: 10px;">
                        <button class="boton" title="Guardar" id="btn_guardar" onClick="fnc_peticiones_crud();">Guardar</button>
                        <button class="boton" title="Regresar" onClick="fnc_catalogos_mostrar_ocultar( 'div_grid', 'div_guardar_editar_eliminar' );">Regresar</button>
                    </div>
                </div>
            </div>
        </div>
        
        
        

        
        
        
        <div style="padding: 5px; display: none;" id="div_gantt_prospeccion">
        	<div class="gfieldset" titulo="Gantt"></div>
            <div style="width: 95%; overflow: auto; padding: 5px;">
            	<table style="width: 100%;" border="0">
                	<tr>
                    	<td style="width: 25%; text-align: right;"><span style="font-weight: bold;">Fecha de inicio:</span> </td>
                        <td style="width: 25%;"><span id="span_fecha_inicio_proyecto"></span></td>
                        <td style="width: 25%; text-align: right;"><span style="font-weight: bold;">Duraci&oacute;n en d&iacute;as: </span></td>
                        <td style="width: 25%;"><span id="span_duracion_dias_proyecto"></span></td>
                    </tr>
                    <tr>
                    	<td style="width: 25%; text-align: right;"><span style="font-weight: bold;">Fecha fin:</span> </td>
                        <td style="width: 25%;"><span id="span_fecha_fin_proyecto"></span></td>
                        <td style="width: 25%; text-align: right;"><span style="font-weight: bold;">Duraci&oacute;n en horas: </span></td>
                        <td style="width: 25%;"><span id="span_duracion_horas_proyecto"></span></td>
                    </tr>
                </table>
            </div>
			<div id="div_gantt_prospeccion_contenido" style="width: 95%; overflow: auto; padding: 5px;"></div>
		</div>
        
        
        

        
        
        
        
        
    </div>
</center>


<div id="div_peticion_ajax_gantt" style="display: none;"></div>

 � `
    <input type="hidden" id="hid_accion" />
	<input type="hidden" id="hid_fec_actual" value=" � Now "()Lcoldfusion/runtime/OleDateTime; � �
  � 
dd/mm/yyyy � 
DateFormat 6(Ljava/util/Date;Ljava/lang/String;)Ljava/lang/String; � �
  � " />
 � metaData Ljava/lang/Object; � �	  � this Lcfpeticiones2ecfm453465740; LocalVariableTable Code <clinit> getMetadata ()Ljava/lang/Object; runPage out Ljavax/servlet/jsp/JspWriter; value invoke0 "Lcoldfusion/tagext/lang/InvokeTag; output1  Lcoldfusion/tagext/io/OutputTag; mode1 I t7 t8 Ljava/lang/Throwable; t9 t10 output2 mode2 t13 t14 t15 t16 LineNumberTable java/lang/Throwable � 1       * +    � +    � �        �   #     *� 
�    �        � �    �   �   =     -� 3� 5�� 3� �� cY� M� u� ��    �        � �    � �  �   "     � ��    �        � �    � �  �  @    �*� � L*� N*� #*+%� )*� 5-� 9� ;:*� ?*� ?**A� CYES� IK� MYOS� S� WY� \^� a� cY� MYeSY*g� CYiSYkS� nSYpSYrS� u� y� � �� �+�� �*� �-� 9� �:* �� ?� � �Y6� &+*g� CYiSYkS� n� �� �� ����� �� :� #�� � #:� �� � :	� 	�:
� ��
+�� �*� �-� 9� �:*8� ?� � �Y6� 3+�� �+*:� ?**:� ?*� ��� �� �+�� �� ����� �� :� #�� � #:� �� � :� �:� ���  � �
 � � �
 � �$ �Q�� ���� �Q�� ���� ���� ���� �  �   �   � � �    � � �   � � �   �     � � �   � � �   � � �   � � �   � � �   � � � 	  � � � 
  � � �   � � �   � � �   � � �   � � �   � � �  �   V  L  4  4  W  ^  t  t  �    � � � � � � � �r:r:v:j:j:b:68              