����  -V 
SourceFile DC:\ColdFusion9\wwwroot\ferrominio\redproject\operaciones\riesgos.cfm cfriesgos2ecfm1422683080  coldfusion/runtime/CFPage  <init> ()V  
  	 bindPageVariables D(Lcoldfusion/runtime/VariableScope;Lcoldfusion/runtime/LocalScope;)V   coldfusion/runtime/CfJspPage 
   CATEGORIASRIESGO_LISTADO Lcoldfusion/runtime/Variable;  bindPageVariable r(Ljava/lang/String;Lcoldfusion/runtime/VariableScope;Lcoldfusion/runtime/LocalScope;)Lcoldfusion/runtime/Variable;  
    	   ID_CRONOGRAMA   	   RIESGOSASIGNADOS_LISTADO   	    
ID_CLIENTE " " 	  $ 
ID_EMPRESA & & 	  ( ID_PROYECTO * * 	  , com.macromedia.SourceModTime  J1� pageContext #Lcoldfusion/runtime/NeoPageContext; 1 2	  3 getOut ()Ljavax/servlet/jsp/JspWriter; 5 6 javax/servlet/jsp/PageContext 8
 9 7 parent Ljavax/servlet/jsp/tagext/Tag; ; <	  = com.adobe.coldfusion.* ? bindImportPath (Ljava/lang/String;)V A B
  C 


 E _whitespace %(Ljava/io/Writer;Ljava/lang/String;)V G H
  I &class$coldfusion$tagext$lang$InvokeTag Ljava/lang/Class;  coldfusion.tagext.lang.InvokeTag M forName %(Ljava/lang/String;)Ljava/lang/Class; O P java/lang/Class R
 S Q K L	  U _initTag P(Ljava/lang/Class;ILjavax/servlet/jsp/tagext/Tag;)Ljavax/servlet/jsp/tagext/Tag; W X
  Y  coldfusion/tagext/lang/InvokeTag [ _setCurrentLineNo (I)V ] ^
  _ APPLICATION a java/lang/String c RF e _resolve 9(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/Object; g h
  i getApp k java/lang/Object m catalogos_flavio o _invoke K(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object; q r
  s setComponent (Ljava/lang/Object;)V u v
 \ w CategoriasRiesgos_Listar y 	setMethod { B
 \ | CategoriasRiesgo_Listado ~ setReturnVariable � B
 \ � &coldfusion/runtime/AttributeCollection � 
id_empresa � _autoscalarize 1(Lcoldfusion/runtime/Variable;)Ljava/lang/Object; � �
  � typeresponse � struct � ([Ljava/lang/Object;)V  �
 � � setAttributecollection (Ljava/util/Map;)V � �
 \ � 	hasEndTag (Z)V � � coldfusion/tagext/GenericTag �
 � � _emptyTcfTag !(Ljavax/servlet/jsp/tagext/Tag;)Z � �
  � adm_proy � RiesgosProyectos_Listar � RiesgosAsignados_Listado � 
id_cliente � id_proyecto � id_cronograma ��

<style type="text/css">
	.riesgos_asignados{
		width: 100%;
		border: 1px solid #D3D3D3;
		border-collapse: collapse;
	}
	
	.riesgos_asignados th{
		border: 1px solid #D3D3D3;
		padding: 4px;
	}
	
	.riesgos_asignados td{
		border: 1px solid #D3D3D3;
		padding: 2px;
	}
	
	.riesgo_asignado{
		cursor: pointer;
	}
</style>


<script type="text/javascript">
	//Variables globales.
	var g_riesgo_asignar;
	var g_mitigacion_actual;
	var g_mit_cont_activada;
	
	$(document).ready(function(){
		//Si existen riesgos asignados, se oculta el catálogo de riesgos.
		if( $("#table_riesgos_asignados tr.sin_riesgos_asignados").size() == 0 )
			$("#chk_catalogo_riesgos").trigger("click");
				
		//Elementos draggables.
		fnc_elementos_draggables({
			elementos: ".riesgo_asignado",
			fnc_start: function(){
				fnc_mostrar_ocultar_cat_riesgos(true, 1);
			},
			fnc_stop: function(){
				fnc_mostrar_ocultar_cat_riesgos(false, 1);
			}
		});
		
		$("#ddl_fases_mit_cont optgroup").remove()
		$("#ddl_fases_mit_cont option[value!='0']").remove()

		//Cargamos el combo de fases.
		$("tr.cronograma_ciclos").each(function(){
			$("<optgroup>").attr({
				label: "Ciclo " + $(this).attr("nu_ciclo")
			}).appendTo("#ddl_fases_mit_cont");
			
			$("tr.cronograma_fases[nu_ciclo='" + $(this).attr("nu_ciclo") + "']").each(function(){
				$("<option>").attr({
					value: $(this).attr("id_fase"),
					nu_ciclo: $(this).attr("nu_ciclo")
				}).text( $(this).find("td:first").text() ).appendTo("#ddl_fases_mit_cont");
			});
		});
		
		//Elementos droppables.
		
		$("#div_table_catalogo_riesgos").droppable({
			accept: ".riesgo_asignado",
			activeClass: "ui-state-highlight",
			drop: function(event, ui){
				//Validamos si podemos eliminar el riesgo.
				if( $(ui.draggable).parent().parent().attr("id_EstatusRiesgo") == "1" )
				{
					//Eliminamos el riesgo.
					fnc_peticionAjax({
						url: "./application/cronograma.cfc",
						parametros: "method=RiesgosProyectos_Eliminar&id_Empresa=" + g_row_proyectos.id_Empresa
							+ "&id_Cliente=" + g_row_proyectos.id_Cliente + "&id_Proyecto=" + g_row_proyectos.id_Proyecto
							+ "&id_Cronograma=" + g_id_CronogramaActual + "&id_CategoriaRiesgo=" + $(ui.draggable).parent().parent().attr("id_CategoriaRiesgo")
							+ "&id_Riesgo=" + $(ui.draggable).parent().parent().attr("id_Riesgo"),
						f_callback: function(){
							//Quitamos el riesgo en pantalla.
							$("#table_riesgos_asignados tbody tr[id_CategoriaRiesgo='" + $(ui.draggable).parent().parent().attr("id_CategoriaRiesgo")
					 			+ "'][id_Riesgo='" + $(ui.draggable).parent().parent().attr("id_Riesgo") + "']").remove();
							
							//Validamos si aún existen riesgos.
							if( $("#table_riesgos_asignados tbody tr").size() == 0 )
							{
								$("#table_riesgos_asignados tbody").append(
									'<tr class="sin_riesgos_asignados">' +
										'<td colspan="3" style="text-align: center;"><span style="font-style: italic;">No existen riesgos asignados.</span></td>' +
									'</tr>'
								);
							}
						}
					});
				}
				else
					fnc_mostrar_aviso("gd_avisos", "Este riesgo no puede ser eliminado porque ya ha sido mitigado.", 5);
			}
		});
		
		$("#div_table_riesgos_asignados").droppable({
			accept: ".riesgo_disponible",
			activeClass: "ui-state-highlight",
			drop: function(event, ui){
				//Validamos que el riesgo no haya sido asignado.
				if( $("#table_riesgos_asignados tbody tr[id_CategoriaRiesgo='" + $(ui.draggable).parent().parent().attr("id_CategoriaRiesgo")
					 + "'][id_Riesgo='" + $(ui.draggable).parent().parent().attr("id_Riesgo") + "']").size() == 0 )
				{
					g_riesgo_asignar = ui.draggable;
					
					fnc_peticionAjax({
						url: "./operaciones/riesgos_parametros.cfm",
						parametros: "id_Empresa=" + g_row_proyectos.id_Empresa + "&de_Riesgo=" + fnc_codificar( ui.draggable.text() ),
						div_success: "div_asignacion_riesgos",
						automensaje: false,
						f_callback: function(){						
							//Abrimos el diálogo.
							gdialogo.abrir("gd_asignacion_riesgos");
							
							//Calculamos la exposición.
							fnc_calcular_exposicion();
						}
					});
				}
				else
					fnc_mostrar_aviso("gd_avisos", "Este riesgo ya ha sido asignado.", 5);
			}
		});
	});
	
	//Función para listar los riesgos por categoría.
	function fnc_riesgos_categorias_listar(p_id_CategoriaRiesgo)
	{
		if( p_id_CategoriaRiesgo > 0 )
		{
			//Consultamos.
			fnc_peticionAjax({
				url: "./application/adm_proy.cfc",
				parametros: "method=RiesgosCategoria_Listar&id_Empresa=" + g_row_proyectos.id_Empresa + "&id_CategoriaRiesgo=" + p_id_CategoriaRiesgo,
				automensaje: false,
				f_callback: function(respuestaJSON){
					//Llenamos el Table.
					$("#div_riesgos_disponibles tbody tr").remove();
					
					//Validamos si se encontraron registros.
					if( respuestaJSON.RS.DATA.ID_RIESGO.length > 0 )
					{
						$.each(respuestaJSON.RS.DATA.ID_RIESGO, function(index, valor){
							$("#table_riesgos_disponibles").append(
								"<tr id_CategoriaRiesgo='" + p_id_CategoriaRiesgo + "' id_Riesgo='" + valor + "'>" +
									"<td>" +
										"<span style='cursor: pointer;' class='riesgo_disponible'>" + respuestaJSON.RS.DATA.DE_RIESGO[index] + "</span>" +
									"</td>" +
								"</tr>"
							);
						});
						
						//Hacemos que los riesgos sean draggables.
						fnc_elementos_draggables({
							elementos: "#table_riesgos_disponibles span.riesgo_disponible"
						});
					}
					else
					{
						$("#table_riesgos_disponibles tbody").append(
							"<tr>" +
								"<td style='text-align: center;'>" +
									"<span style='font-style: italic;'>No se encontraron riesgos.</span>" +
								"</td>" +
							"</tr>"
						);
					}
					
					$("#div_riesgos_disponibles").hide();
					$("#div_riesgos_disponibles").show({
						effect: "fade",
						duration: 300
					});
				}
			});
		}
		else
		{
			$("#div_riesgos_disponibles").hide({
				effect: "blind",
				duration: 300
			});
		}
	}
	
	//Función para mostrar u ocultar el catálogo de los riesgos.
	function fnc_mostrar_ocultar_catalogo_riesgos(p_tipo)
	{
		var tipo = ( p_tipo == 1 ? "riesgos" : "mitigaciones" );
		
		if( $("#chk_catalogo_" + tipo).is(":checked") )
		{
			//Mostramos el catálogo.
			fnc_mostrar_ocultar_cat_riesgos(true, p_tipo);
		}
		else
		{
			//Ocultamos el catálogo.
			fnc_mostrar_ocultar_cat_riesgos(false, p_tipo);
		}
	}
	
	//Función para calcular la exposición del riesgo.
	function fnc_calcular_exposicion()
	{
		$("#span_exposicion").text( ( ( ( $("#ddl_prob_ocurrencia_riesgos option:selected").attr("nu_EvaluacionOcurrencia") * 1 )
			* ( $("#ddl_impactos_riesgos option:selected").attr("nu_EvaluacionImpacto") * 1 ) * 100 ) * 1 ).toFixed(2) * 1 );
	}
	
	//Función para asignar riesgos.
	function fnc_asignar_riesgo()
	{
		//Validamos los campos obligatorios.
		if( fnc_validar_campos_obligatorios("div_asignacion_riesgos") )
		{
			//Guardamos.
			fnc_peticionAjax({
				url: "./application/cronograma.cfc",
				parametros: "method=RiesgosProyectos_Agregar&id_Empresa=" + g_row_proyectos.id_Empresa + "&id_Cliente=" + g_row_proyectos.id_Cliente
					+ "&id_Proyecto=" + g_row_proyectos.id_Proyecto + "&id_Cronograma=" + g_id_CronogramaActual
					+ "&id_CategoriaRiesgo=" + $(g_riesgo_asignar).parent().parent().attr("id_CategoriaRiesgo") 
					+ "&id_Riesgo=" + $(g_riesgo_asignar).parent().parent().attr("id_Riesgo")
					+ "&id_PrioridadRiesgo=" + $("#ddl_prioridades_riesgos option:selected").val()
					+ "&id_RiesgoProbOcurrencia=" + $("#ddl_prob_ocurrencia_riesgos option:selected").val()
					+ "&id_RiesgoImpacto=" + $("#ddl_impactos_riesgos option:selected").val() + "&id_EstatusRiesgo=1"
					+ "&id_TipoFuente=" + $("#ddl_fuentes_riesgos option:selected").attr("id_TipoFuente")
					+ "&id_RiesgoFuente=" + $("#ddl_fuentes_riesgos option:selected").val()
					+ "&nu_Exposicion=" + $("# � write � B java/io/Writer �
 � ��span_exposicion").text()
					+ "&de_Observaciones=&nu_ImpactoHoras=0",
				f_callback: function(respuestaJSON){
					//Cerramos el diálogo.
					gdialogo.cerrar("gd_asignacion_riesgos");
					
					//Actualizamos los riesgos.
					fnc_peticionAjax({
						url: "./application/adm_proy.cfc",
						parametros: "method=RiesgosProyectos_Listar&id_Empresa=" + g_row_proyectos.id_Empresa
							+ "&id_Cliente=" + g_row_proyectos.id_Cliente + "&id_Proyecto=" + g_row_proyectos.id_Proyecto
							+ "&id_Cronograma=" + g_id_CronogramaActual,
						automensaje: false,
						f_callback: function(respuestaJSON){
							//Pintamos el riesgo en pantalla.
							$("#table_riesgos_asignados tbody tr").remove();
							
							$.each(respuestaJSON.RS.DATA.ID_RIESGO, function(index, value){
								$("#table_riesgos_asignados tbody").append(
									'<tr id_CategoriaRiesgo="' + respuestaJSON.RS.DATA.ID_CATEGORIARIESGO[index] + '" ' +
										'id_Riesgo="' + value + '" id_EstatusRiesgo="' + respuestaJSON.RS.DATA.ID_ESTATUSRIESGO[index] + '" ' +
										'title="Clic para ver Mitigaciones y Contingencias" onClick="fnc_ver_mitigaciones_contingencias(this);">' +
										'<td><span class="riesgo_asignado">' + respuestaJSON.RS.DATA.DE_RIESGO[index] + '</span></td>' +
										'<td>' + respuestaJSON.RS.DATA.DE_CATEGORIARIESGO[index] + '</td>' +
										'<td style="text-align: center;">' + ( respuestaJSON.RS.DATA.NU_EXPOSICION[index] * 1 ) + '%</td>' +
									'</tr>'
								);
							});	
							
							//Hacemos los elementos draggables.
							fnc_elementos_draggables({
								elementos: ".riesgo_asignado",
								fnc_start: function(){
									fnc_mostrar_ocultar_cat_riesgos(true, 1);
								},
								fnc_stop: function(){
									fnc_mostrar_ocultar_cat_riesgos(false, 1);
								}
							});
						}
					});
				}
			});
		}
	}
	
	//Función para mostrar el catálogo de los riesgos.
	function fnc_mostrar_ocultar_cat_riesgos(p_mostrar, p_tipo)
	{
		var div_asignados = "div_riesgos_asignados";
		var div_catalogo = "div_catalogo_riesgos";
		var chk = "chk_catalogo_riesgos";
		
		if( p_tipo == 2 )
		{
			div_asignados = "div_mitigaciones_asignadas";
			div_catalogo = "div_catalogo_mitigaciones";
			chk = "chk_catalogo_mitigaciones";
		}
		
		var tipo = ( p_tipo == 1 ? "riesgos" : "mitigaciones" );
		
		if( p_mostrar )
		{
			//Mostramos el catálogo.
			$("#" + div_asignados).animate({
				float: "right",
				width: "48%",
				duration: 300,
			}, function(){
				$("#" + div_catalogo).show(100);
			});
		}
		else
		{
			//Ocultamos el catálogo.
			$("#" + div_catalogo).hide(200);
			
			$("#" + div_asignados).animate({
				float: "none",
				width: "100%"
			}, 300);
			
			$("#" + chk)[0].checked = false;
		}
	}
	
	//Función para ver las mitigaciones y contingnecias asignados al riesgo del proyecto.
	function fnc_ver_mitigaciones_contingencias(p_tr)
	{
		fnc_peticionAjax({
			url: "./operaciones/mitigaciones_contingencias.cfm",
			parametros: "id_Empresa=" + g_row_proyectos.id_Empresa + "&id_Cliente=" + g_row_proyectos.id_Cliente
				+ "&id_Proyecto=" + g_row_proyectos.id_Proyecto + "&id_Cronograma=" + g_id_CronogramaActual
				+ "&id_CategoriaRiesgo=" + $(p_tr).attr("id_CategoriaRiesgo") + "&id_Riesgo=" + $(p_tr).attr("id_Riesgo")
				+ "&de_Riesgo=" + fnc_codificar( $(p_tr).find("td:eq(0)").text() )
				+ "&de_CategoriaRiesgo=" + fnc_codificar( $(p_tr).find("td:eq(1)").text() ),
			div_success: "div_asignacion_mitigaciones_contingencias",
			automensaje: false,
			f_callback: function(){
				gdialogo.abrir("gd_asignacion_mitigaciones_contingencias");
				
				//Retringimos las fechas.
				$("#txt_fh_Inicio_mit_cont, #txt_fh_Fin_mit_cont").datepicker( "option", "minDate", fnc_fecha_objeto( $("#txt_fh_Inicio_mit_cont").val(), true ) );
			}
		});
	}
	
	//Función para activar mitigaciones.
	function fnc_activar_mitigacion_dialogo(p_chk, p_id_TipoActividad)
	{
		if( $(p_chk).is(":checked") )
		{
			g_mitigacion_actual = $(p_chk).parent().parent();
			g_mit_cont_activada = false;
			
			//Validamos el tipo de actividad.
			if( p_id_TipoActividad == 4 )
			{
				$("#tr_mitigacion").show();
				$("#tr_contingencia").hide();
				
				$("#gd_activar_mitigacion_contingencia").attr("titulo", "Activar Mitigaci&oacute;n");
			}
			else if( p_id_TipoActividad == 5 )
			{
				$("#tr_mitigacion").hide();
				$("#tr_contingencia").show();
				
				$("#gd_activar_mitigacion_contingencia").attr("titulo", "Asignar Contingencia");
			}

			$("#span_mitigacion_asignar").text( $("#table_mitigaciones_asignadas tbody tr[id_CategoriaRiesgo='" 
				+ $(p_chk).parent().parent().attr("id_CategoriaRiesgo")
				+ "'][id_Riesgo='" + $(p_chk).parent().parent().attr("id_Riesgo") + "'][id_Mitigacion='" 
				+ $(p_chk).parent().parent().attr("id_Mitigacion") + "'][id_MitigacionProyecto='" 
				+ $(p_chk).parent().parent().attr("id_MitigacionProyecto") + "'] td.de_Mitigacion").text() );		
							
			$("#span_contingencia_asignar").text( $("#table_contingencias_asignadas tbody tr[id_CategoriaRiesgo='" 
				+ $(p_chk).parent().parent().attr("id_CategoriaRiesgo")
				+ "'][id_Riesgo='" + $(p_chk).parent().parent().attr("id_Riesgo") + "'][id_Mitigacion='" 
				+ $(p_chk).parent().parent().attr("id_Mitigacion") + "'][id_MitigacionProyecto='" 
				+ $(p_chk).parent().parent().attr("id_MitigacionProyecto") + "'] td.de_Contingencia").text()  );
			
			//Llenamos el combo de recursos.
			$("#ddl_responsable_mit_cont option[value!='0']").remove();
			
			$("#table_recursos_agregados .recurso_agregado").each(function(){
				$("<option>").attr({
					value: $(this).parent().parent().attr("id_equipo_trabajo"),
					id_Recurso: $(this).parent().parent().attr("id_Recurso")
				}).text( $(this).text() ).appendTo("#ddl_responsable_mit_cont");
			});
			
			//Reiniciamos los controles.
			$("#ddl_responsable_mit_cont").val(0);
			$("#chk_enviar_cronograma_mit_cont")[0].checked = true;
			$("#ddl_fases_mit_cont").val(0);
			$(".tr_fases_mit_cont").show(); 
			$(".tr_envio_cronograma").hide()
			
			gdialogo.abrir("gd_activar_mitigacion_contingencia");
		}
	}
	
	//Función para asignar mitigaciones.
	function fnc_asignar_mitigacion_contingencia()
	{
		//Validamos los campos obligatorios.
		if( fnc_validar_campos_obligatorios("div_activar_mitigacion_contingencia") )
		{
			var parametros = "method=";
			
			//Validamos el tipo de actividad.
			if( $("#tr_mitigacion").is(":visible") )	
				parametros += "MitigacionesProyectos_Editar&de_Mitigacion=" + fnc_codificar( $(g_mitigacion_actual).find("td.de_Mitigacion").text() );
			else
				parametros += "ContingenciasProyectos_Editar&id_Contingencia=" +  $(g_mitigacion_actual).attr("id_ContingenciaProyecto") 
					+ "&id_ContingenciaProyecto=" + $(g_mitigacion_actual).attr("id_ContingenciaProyecto")
					+ "&de_Contingencia=" + fnc_codificar( $(g_mitigacion_actual).find("td.de_Contingencia").text() );
				
			parametros += "&id_Empresa=" + g_row_proyectos.id_Empresa + "&id_Cliente=" + g_row_proyectos.id_Cliente
				+ "&id_Proyecto=" + g_row_proyectos.id_Proyecto + "&id_Cronograma=" + g_id_CronogramaActual
				+ "&id_CategoriaRiesgo=" + $(g_mitigacion_actual).attr("id_CategoriaRiesgo")
				+ "&id_Riesgo=" + $(g_mitigacion_actual).attr("id_Riesgo")
				+ "&id_Mitigacion=" + $(g_mitigacion_actual).attr("id_Mitigacion")
				+ "&id_MitigacionProyecto=" + $(g_mitigacion_actual).attr("id_MitigacionProyecto")
				+ "&sn_Activo=1"
				+ "&id_EquipoTrabajo=" + $("#ddl_responsable_mit_cont option:selected").val()
				+ "&id_Recurso=" + $("#ddl_responsable_mit_cont option:selected").attr("id_Recurso");

			//Validamos si la actividad se va a enviar al cronograma.
			if( $("#chk_enviar_cronograma_mit_cont").is(":checked") )
			{
				//Validar que se haya elegido una Fase.
				if( $("#ddl_fases_mit_cont option:selected").val() != "0" )
					parametros += "&sn_EnvioCronograma=1" + "&id_Fase=" + $("#ddl_fases_mit_cont option:selected").val()
						+ "&nu_Ciclo=" + $("# ��ddl_fases_mit_cont option:selected").attr("nu_ciclo");
				else
				{
					fnc_notificacion("La informaci&oacute;n acerca de <b>Fase</b> es requerida.");
					return;
				}
			}
			else
			{
				parametros += "&sn_EnvioCronograma=0"
					+ "&fh_Fin=" + fnc_fecha_string( fnc_fecha_objeto( $("#txt_fh_Fin_mit_cont").val(), true ), "yyyy-mm-dd", true )
			}
			
			//Activamos.
			fnc_peticionAjax({
				url: "./application/cronograma.cfc",
				parametros: parametros,
				f_callback: function(respuestaJSON){
					g_mit_cont_activada = true;
					
					//Cerramos el diálogo.
					gdialogo.cerrar("gd_activar_mitigacion_contingencia");
					
					//Actualizamos la pantalla de Mitigaciones y Contingencias.
					fnc_mitigaciones_contingencias_listar( $(g_mitigacion_actual).attr("id_CategoriaRiesgo")
						, $(g_mitigacion_actual).attr("id_Riesgo")
						, $(g_mitigacion_actual).attr("id_Mitigacion") );
					
					//Si la actividad se fue al cronograma, actualizamos el cronograma.
					if( $("#chk_enviar_cronograma_mit_cont").is(":checked") )
						fnc_recargar_cronograma(true);
					
					//El riesgo no puede ser eliminado porque ya ha sido mitigado.
					$("#table_riesgos_asignados tbody tr[id_CategoriaRiesgo='" + $(g_mitigacion_actual).attr("id_CategoriaRiesgo")
					 	+ "'][id_Riesgo='" + $(g_mitigacion_actual).attr("id_Riesgo") + "']").attr("id_EstatusRiesgo", "2");
				}
			});
		}
	}
	
	//Función para consultar las mitigaciones y contingencias.
	function fnc_mitigaciones_contingencias_listar(p_id_CategoriaRiesgo, p_id_Riesgo, p_id_Mitigacion)
	{
		var tr_riesgo = $("#table_riesgos_asignados tbody tr[id_CategoriaRiesgo='" + p_id_CategoriaRiesgo
			+ "'][id_Riesgo='" + p_id_Riesgo + "']");
			
		fnc_peticionAjax({
			url: "./operaciones/mitigaciones_contingencias.cfm",
			parametros: "id_Empresa=" + g_row_proyectos.id_Empresa + "&id_Cliente=" + g_row_proyectos.id_Cliente 
				+ "&id_Proyecto=" + g_row_proyectos.id_Proyecto + "&id_Cronograma=" + g_id_CronogramaActual
				+ "&id_CategoriaRiesgo=" + p_id_CategoriaRiesgo + "&id_Riesgo=" + p_id_Riesgo
				+ "&id_Mitigacion=" + p_id_Mitigacion
				+ "&de_Riesgo=" + fnc_codificar( $(tr_riesgo).find("td:eq(0)").text() )
				+ "&de_CategoriaRiesgo=" + fnc_codificar( $(tr_riesgo).find("td:eq(1)").text() ),
			div_success: "div_asignacion_mitigaciones_contingencias",
			automensaje: false
		});
	}
	
	//Función para validar si se activó la mitigación o la contingencia.
	function fnc_validar_mit_cont_activa()
	{
		if( g_mit_cont_activada == false )
			$(g_mitigacion_actual).find("input:checkbox")[0].checked = false;
			
	}
</script>

<div>
	<input type="checkbox" id="chk_catalogo_riesgos" checked="checked" onClick="fnc_mostrar_ocultar_catalogo_riesgos(1);" />
    	<label for="chk_catalogo_riesgos">Mostrar riesgos disponibles</label>
</div>


<div id="div_catalogo_riesgos" style="float: left; width: 48%;" title="Arrastre hasta aqu&iacute; para eliminar riesgos asignados al proyecto.">
	<div class="gfieldset" titulo="Riesgos Disponibles"></div>
    <div id="div_table_catalogo_riesgos" style="padding: 2px;">
        <div>
            <table>
                <tr>
                    <td>Categor&iacute;a: </td>
                    <td>
                        <select id="ddl_categorias" class="input_text" onChange="fnc_riesgos_categorias_listar(this.value);">
                            <option value="0">Seleccione...</option>
                             � $class$coldfusion$tagext$io$OutputTag coldfusion.tagext.io.OutputTag � � L	  � coldfusion/tagext/io/OutputTag � cfoutput � query � CategoriasRiesgo_Listado.rs � _validateTagAttrValue \(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String; � �
  � setQuery � B coldfusion/tagext/QueryLoop �
 � � 
doStartTag ()I � �
 � � 1
                                <option value=" � RS � ID_CATEGORIARIESGO � _resolveAndAutoscalarize D(Lcoldfusion/runtime/Variable;[Ljava/lang/String;)Ljava/lang/Object; � �
  � _String &(Ljava/lang/Object;)Ljava/lang/String; � � coldfusion/runtime/Cast �
 � � "> � DE_CATEGORIARIESGO � '</option>
                             � doAfterBody � �
 � � doEndTag � �
 � � doCatch (Ljava/lang/Throwable;)V � �
 � � 	doFinally � 
 � ��
                        </select>
                    </td>
                </tr>
            </table>
        </div>
        <div style="display: none;" id="div_riesgos_disponibles">
            <table class="riesgos_asignados" id="table_riesgos_disponibles">
                <thead>
                    <tr class="ui-state-default">
                        <th>Descripci&oacute;n</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</div>


<div id="div_riesgos_asignados" style="float: right; width: 48%;" title="Arrastre hasta aqu&iacute; para asignar riesgos al proyecto.">
	<div class="gfieldset" titulo="Riesgos Asignados al Proyecto"></div>
    <div id="div_table_riesgos_asignados" style="padding: 2px;">
        <table class="riesgos_asignados" style="width: 100%;" id="table_riesgos_asignados">
            <thead>
                <tr class="ui-state-default">
                    <th>Riesgo</th>
                    <th style="width: 30%;">Categor&iacute;a</th>
                    <th style="width: 100px;">Exposici&oacute;n</th>
                   
                </tr>
            </thead>
            <tbody>
                 � RECORDCOUNT � _compare (Ljava/lang/Object;D)D � �
  � 
                     � RiesgosAsignados_Listado.rs � 2
                        <tr id_CategoriaRiesgo="  '"
                        	id_Riesgo=" 	ID_RIESGO " id_EstatusRiesgo=" ID_ESTATUSRIESGO �"
                            title="Clic para ver Mitigaciones y Contingencias" onClick="fnc_ver_mitigaciones_contingencias(this);">
                            <td><span class="riesgo_asignado">
 	DE_RIESGO .</span></td>
                            <td> C</td>
                            <td style="text-align: center;"> NU_EXPOSICION Y%</td>
                            
                        </tr>
                     
                 �
                    <tr class="sin_riesgos_asignados">
                        <td colspan="3" style="text-align: center;"><span style="font-style: italic;">No existen riesgos asignados.</span></td>
                    </tr>
                
�
            </tbody>
        </table>
    </div>
</div>

<div style="clear: both;"></div>


<div id="gd_asignacion_riesgos" class="gdialogo" titulo="Parametrizaci&oacute;n del Riesgo" ancho="550px">
	<div id="div_asignacion_riesgos" class="gdcontenidodinamico" style="padding: 5px;"></div>
    
    <button class="gdboton" type="button" onClick="fnc_asignar_riesgo();">Guardar</button>
</div>


<div id="gd_asignacion_mitigaciones_contingencias" class="gdialogo" titulo="Mitigaciones y Contingencias" ancho="80%">
	<div id="div_asignacion_mitigaciones_contingencias" class="gdcontenidodinamico" style="padding: 5px;"></div>
</div>


<div id="gd_activar_mitigacion_contingencia" class="gdialogo" ancho="500px" alCerrar="fnc_validar_mit_cont_activa();">
	<div id="div_activar_mitigacion_contingencia" style="padding: 5px;">
    	<table style="width: 100%;">
        	<tr id="tr_mitigacion">
            	<td style="width: 75px;">Mitigaci&oacute;n: </td>
                <td>
                	<span style="font-weight: bold;" id="span_mitigacion_asignar"></span>
                </td>
            </tr>
            <tr id="tr_contingencia">
            	<td style="width: 75px;">Contingencia: </td>
                <td>
                	<span style="font-weight: bold;" id="span_contingencia_asignar"></span>
                </td>
            </tr>
            <tr>
            	<td style="width: 75px;">Responsable: </td>
                <td>
                	<select id="ddl_responsable_mit_cont" class="input_text obligatorio" acercade="Responsable">
                    	<option value="0">Seleccione...</option>
                    </select>
                </td>
            </tr>
            <tr>
            	<td>Enviar a Cronograma: </td>
                <td>
                	<input type="checkbox" id="chk_enviar_cronograma_mit_cont" checked="checked"
                    	onClick="if( this.checked ){ $('.tr_fases_mit_cont').show(); $('.tr_envio_cronograma').hide() }else{ $('.tr_fases_mit_cont').hide(); $('.tr_envio_cronograma').show() }" />
                </td>
            </tr>
            <tr class="tr_fases_mit_cont">
            	<td>Fase: </td>
                <td>
                	<select id="ddl_fases_mit_cont" class="input_text">
                    	<option value="0">Seleccione...</option>
                    </select>
                </td>
            </tr>
            
            <tr class="tr_envio_cronograma">
            	<td>Fecha Compromiso: </td>
            	<td>
                	<input type="text" id="txt_fh_Fin_mit_cont" class="txt_datepicker input_text"
                    	value=" Now "()Lcoldfusion/runtime/OleDateTime;
  
dd/mm/yyyy  
DateFormat 6(Ljava/util/Date;Ljava/lang/String;)Ljava/lang/String;"#
 $ �" />
                </td>
            </tr>
        </table>
    </div>
    
     <button type="button" class="gdboton" onClick="fnc_asignar_mitigacion_contingencia();">Guardar</button>
</div>
& metaData Ljava/lang/Object;()	 * this Lcfriesgos2ecfm1422683080; LocalVariableTable Code <clinit> varscope "Lcoldfusion/runtime/VariableScope; locscope Lcoldfusion/runtime/LocalScope; getMetadata ()Ljava/lang/Object; runPage out Ljavax/servlet/jsp/JspWriter; value invoke0 "Lcoldfusion/tagext/lang/InvokeTag; invoke1 output2  Lcoldfusion/tagext/io/OutputTag; mode2 I t8 t9 Ljava/lang/Throwable; t10 t11 output3 mode3 t14 t15 t16 t17 output4 mode4 t20 t21 t22 t23 LineNumberTable java/lang/ThrowableT 1     	                 "     &     *     K L    � L   ()       /   #     *� 
�   .       ,-   0  /   =     N� T� V�� T� �� �Y� n� ��+�   .       ,-      /   �     O*+,� **+,� � **+,� � **+,� � !**#+,� � %**'+,� � )**++,� � -�   .        O,-     O12    O34  56 /   "     �+�   .       ,-   76 /  �    Z*� 4� :L*� >N*@� D*+F� J*� V-� Z� \:*� `*� `**b� dYfS� jl� nYpS� t� xz� }� �� �Y� nY�SY**� )� �SY�SY�S� �� �� �� �� �*+F� J*� V-� Z� \:*
� `*
� `**b� dYfS� jl� nY�S� t� x�� }�� �� �Y
� nY�SY**� )� �SY�SY**� %� �SY�SY**� -� �SY�SY**� � �SY�SY	�S� �� �� �� �� �+�� �+�� �+�� �*� �-� Z� �:*� `���� Ƕ �� �� �Y6� W+Ҷ �+**� � dY�SY�S� ڸ � �+� �+**� � dY�SY�S� ڸ � �+� �� ���� �� :� #�� � #:		� � � :
� 
�:� �+�� �**� !� dY�SY�S� �� ���m*+�� J*� �-� Z� �:*6� `���� Ƕ �� �� �Y6� �+� �+**� !� dY�SY�S� ڸ � �+� �+**� !� dY�SYS� ڸ � �+� �+**� !� dY�SY	S� ڸ � �+� �+**� !� dY�SYS� ڸ � �+� �+**� !� dY�SY�S� ڸ � �+� �+**� !� dY�SYS� ڸ � �+� �� ��� �� :� #�� � #:� � � :� �:� �*+� J� 
+� �+� �*� �-� Z� �:*�� `� �� �Y6� (+*�� `**�� `*�!�%� �� ���� �� :� #�� � #:� � � :� �:� �+'� �� �UU� U U U % U���U���U���U���U���U���U�%1U+.1U�%@U+.@U1=@U@E@U .   �   Z,-    Z89   Z:)   Z ; <   Z;<   Z=<   Z>?   Z@A   ZB)   ZCD 	  ZED 
  ZF)   ZG?   ZHA   ZI)   ZJD   ZKD   ZL)   ZM?   ZNA   ZO)   ZPD   ZQD   ZR) S   @ L  4  4  W  ^  t  t  �    � 
 � 
 � 
 � 
 � 
    ! ! 3 3 E  � 
�������p75M5x6�7�7�7�8�8�8�8�8�8:::-;-;,;Q<Q<P<]6�@75�����������          .    /