����  - B 
SourceFile FC:\ColdFusion9\wwwroot\ferrominio\redproject\configuracion\modulos.cfm cfmodulos2ecfm2036312032  coldfusion/runtime/CFPage  <init> ()V  
  	 com.macromedia.SourceModTime  J1# coldfusion/runtime/CfJspPage  pageContext #Lcoldfusion/runtime/NeoPageContext;  	   getOut ()Ljavax/servlet/jsp/JspWriter;   javax/servlet/jsp/PageContext 
   parent Ljavax/servlet/jsp/tagext/Tag;  	   com.adobe.coldfusion.*  bindImportPath (Ljava/lang/String;)V   !
  "�

<script type="text/javascript">
	//Variables globales.
	var g_row;
	
	//FunciÃ³n para abrir la opciÃ³n de agregar, editar o eliminar.
	function fnc_modulos_agregar_editar(p_accion)
	{
		if( p_accion == 1 ) //Agregar.
		{
			$("#div_titulo_agregar_editar").html("NUEVO M&Oacute;DULO");
			$("#btn_guardar").attr("title", "Guardar").text("Guardar");
			
			//Nos aseguramos de desbloquear los input.
			$("#txt_nb_Modulo, #txt_nu_Orden").removeAttr("disabled");
			
			fnc_catalogos_mostrar_ocultar( "div_grid", "div_guardar_editar_eliminar" );
		}
		else if( p_accion == 2 || p_accion == 3 ) //Editar o Eliminar.
		{
			//Validamos que se haya seleccionado un renglÃ³n.
			var row = ggrids.obtener_renglon("grid_modulos");
			
			if( row )
			{	
				g_row = row;

				//Llenamos los campos.
				$("#txt_nb_Modulo").val( row.nb_Modulo );
				$("#txt_nu_Orden").val( row.nu_Orden );
				
				//Valdiar si debemos bloquear o desbloquear los input.
				if( p_accion == 3 )
				{
					$("#div_titulo_agregar_editar").html("ELIMINAR M&Oacute;DULO");
					$("#btn_guardar").attr("title", "Eliminar").text("Eliminar");
					$("#txt_nb_Modulo, #txt_nu_Orden").attr("disabled", "disabled");
				}
				else
				{
					$("#div_titulo_agregar_editar").html("EDITAR M&Oacute;DULO");
					$("#btn_guardar").attr("title", "Guardar").text("Guardar");
					$("#txt_nb_Modulo, #txt_nu_Orden").removeAttr("disabled");
				}
				
				fnc_catalogos_mostrar_ocultar( "div_grid", "div_guardar_editar_eliminar" );
			}
			else
				fnc_notificacion("Debe seleccionar un M&oacute;dulo.");
		}
		
		$("#hid_accion").val( p_accion );
		
		//Limpiamos los campos.
		if( p_accion == 1 )
		{
			$("#txt_nb_Modulo").val("");
			$("#txt_nu_Orden").val("");
		}
		
		$("#txt_nb_Modulo").focus();
	}
	
	//FunciÃ³n para guardar, editar o eliminar.
	function fnc_modulos_crud()
	{
		//Validamos los campos obligaorios.
		if( fnc_validar_campos_obligatorios("div_modulos_crud") )
		{
			//Armamos los parÃ¡metros.
			var parametros = "id_Empresa=" + $("#hid_id_Empresa").val() + "&method=";
			
			switch( $("#hid_accion").val() )
			{
				case "1": //Agregar.
							parametros += "Modulos_Agregar&nb_Modulo=" + fnc_codificar( $.trim( $("#txt_nb_Modulo").val() ) )
								+ "&nu_Orden=" + $("#txt_nu_Orden").val();
							break;
				case "2": //Editar
							parametros += "Modulos_Editar&id_Modulo=" + g_row.id_Modulo 
								+ "&nb_Modulo=" + fnc_codificar( g_row.nb_Modulo )
								+ "&nb_ModuloNuevo=" + fnc_codificar( $.trim( $("#txt_nb_Modulo").val() ) )
								+ "&nu_Orden=" + $("#txt_nu_Orden").val();
							break;
				case "3": //Eliminar
							parametros += "Modulos_Eliminar&id_Modulo=" + g_row.id_Modulo;
							break;
			}
			
			//Guardamos, Editamos, Eliminamos.
			fnc_peticionAjax({
				url: "./application/configuracion.cfc",
				parametros: parametros,
				f_callback: function(){
					//Recargamos el grid.
					ggrids.recargar("grid_modulos");
					
					if( $("#hid_accion").val() == "2" || $("#hid_accion").val() == "3" )
						fnc_catalogos_mostrar_ocultar( "div_grid", "div_guardar_editar_eliminar" );
					else
					{
						$("#txt_nb_Modulo").val("");
						$("#txt_nu_Orden").val("");
					}
				}
			});
		}
	}
	
	//FunciÃ³n para buscar mÃ³dulos.
	function fnc_buscar_modulos()
	{
		//Armamos los parÃ¡metros.
		var parametros = "&nb_Modulo=" + fnc_codificar( $.trim( $("#txt_nb_ModuloBuscar").val() ) );
			
		ggrids.recargar("grid_modulos", parametros);
	}
</script>

	<center>
        <div style="width: 800px;">
            <div class="ui-widget-header catalogos_cabecero titulo_opcion">CAT&Aacute;LOGO DE M&Oacute;DULOS</div>
            <div class="catalogos_cabecero_contenido" id="div_grid">
            	<div class="ui-state-default catalogos_busqueda" id="div_busqueda">
                	<table style="width: 100%;">
                    	<tr>
                        	<td style="text-align:center; padding-right:300px;"><span>M&oacute;dulo:</span>
								<input type="text" id="txt_nb_ModuloBuscar" class="input_text" onChange="fnc_buscar_modulos();" maxlength="150" />
                            </td>
                        </tr>
                    </table>
                </div>
                <div id="grid_modulos" class="ggrids" paginado="10" titulo="Listado de M&oacute;dulos"
                	url="application/configuracion.cfc?method=Modulos_ListarJSON"
                	ancho="700" sortname="nu_Orden" sortorder="ASC"
                    loadComplete="fnc_autoajustar_ancho_busqueda('grid_modulos','div_busqueda')">
                    
                    
                    <columna nom_columna="nb_Modulo">M&oacute;dulo</columna>
                    <columna nom_columna="nu_Orden" align="center" ancho="50">&Oacute;rden</columna>
                    
                    
                    <columna nom_columna="id_Modulo" visible="false"></columna>
                    
                    
                    <accion accion="agregar" evento="fnc_modulos_agregar_editar(1);"></accion>
                    <accion accion="editar" evento="fnc_modulos_agregar_editar(2);"></accion>
                    <accion accion="eliminar" evento="fnc_modulos_agregar_editar(3);"></accion>
                </div>
            </div>
            
            
            <div id="div_guardar_editar_eliminar" class="catalogos_guardar_editar">
            	<div class="ui-widget-header catalogos_guardar_editar_cabecero" id="div_titulo_agregar_editar"></div>
                <div class="catalogos_guardar_editar_cabecero_contenido" id="div_modulos_crud">
                    <table> 
                        <tr>
                            <td style="text-align: right;">M&oacute;dulo: </td>
                            <td style="text-align:left;"><input class="input_text obligatorio" acercade="M&oacute;dulo" type="text" id="txt_nb_Modulo" maxlength="150"
                            	style="width: 300px;" /><span> *</span></td>
                        </tr> 
                        <tr>
                            <td style="text-align: right;">&Oacute;rden: </td>
                            <td style="text-align:left;"><input class="input_text obligatorio" acercade="&Oacute;rden" type="text" id="txt_nu_Orden" maxlength="2"
                            	style="width: 40px; text-align: center;" /><span> *</span></td>
                        </tr> 
                    </table>
                    
                    <div style="margin-top: 10px;">
                    	<button class="boton" title="Guardar" id="btn_guardar" onClick="fnc_modulos_crud();">Guardar</button>
                        <button class="boton" title="Regresar" onClick="fnc_catalogos_mostrar_ocultar( 'div_grid', 'div_guardar_editar_eliminar' );">Regresar</button>
                    </div>
                </div>
            </div>
        </div>
    </center>
    
    <input type="hidden" id="hid_accion" />
 $ write & ! java/io/Writer (
 ) ' metaData Ljava/lang/Object; + ,	  - &coldfusion/runtime/AttributeCollection / java/lang/Object 1 ([Ljava/lang/Object;)V  3
 0 4 this Lcfmodulos2ecfm2036312032; LocalVariableTable Code <clinit> getMetadata ()Ljava/lang/Object; runPage out Ljavax/servlet/jsp/JspWriter; value LineNumberTable 1       + ,        9   #     *� 
�    8        6 7    :   9   -     � 0Y� 2� 5� .�    8        6 7    ; <  9   "     � .�    8        6 7    = <  9   c     *� � L*� N*� #+%� *�    8   *     6 7      > ?     @ ,        A                  