����  - � 
SourceFile GC:\ColdFusion9\wwwroot\ferrominio\redproject\configuracion\usuarios.cfm cfusuarios2ecfm333051864  coldfusion/runtime/CFPage  <init> ()V  
  	 com.macromedia.SourceModTime  J1# coldfusion/runtime/CfJspPage  pageContext #Lcoldfusion/runtime/NeoPageContext;  	   getOut ()Ljavax/servlet/jsp/JspWriter;   javax/servlet/jsp/PageContext 
   parent Ljavax/servlet/jsp/tagext/Tag;  	   com.adobe.coldfusion.*  bindImportPath (Ljava/lang/String;)V   !
  "�

<script type="text/javascript">
	//Variables globales.
	var g_row;
	
	//FunciÃ³n para abrir la opciÃ³n de agregar, editar o eliminar.
	function fnc_usuarios_agregar_editar(p_accion)
	{
		if( p_accion == 1 ) //Agregar.
		{
			$("#div_titulo_agregar_editar").html("NUEVO USUARIO");
			$("#btn_guardar").attr("title", "Guardar").text("Guardar");
			
			//Nos aseguramos de desbloquear los input.
			$("#ddl_recursos, #txt_nb_Usuario, #txt_de_Password, #txt_de_RepitaPassword, #chk_sn_Activo").removeAttr("disabled");
			
			fnc_limpiar_campos_usuarios();
			
			fnc_catalogos_mostrar_ocultar( "div_grid", "div_guardar_editar_eliminar" );
		}
		else if( p_accion == 2 || p_accion == 3 ) //Editar o Eliminar.
		{
			//Validamos que se haya seleccionado un renglÃ³n.
			var row = ggrids.obtener_renglon("grid_usuarios");
			
			if( row )
			{	
				g_row = row;

				//Llenamos los campos.$("#txt_id_Recurso").val()
				$("#txt_id_Recurso").val( row.id_Recurso );
				$("#txt_nb_NombreCompleto").val(row.nb_NombreCompletoRecurso);
				$("#txt_nb_Usuario").val( row.nb_Usuario );
				$("#txt_de_Password").val(row.de_Password);
				$("#txt_de_RepitaPassword").val(row.de_Password);
				$("#chk_sn_Activo")[0].checked = ( row.sn_Activo == "1" ? true : false );
				$("#buscar_recurso").hide();
				//Valdiar si debemos bloquear o desbloquear los input.
				if( p_accion == 3 )
				{
					$("#div_titulo_agregar_editar").html("ELIMINAR USUARIO");
					$("#btn_guardar").attr("title", "Eliminar").text("Eliminar");
					$("#ddl_recursos, #txt_nb_Usuario, #txt_de_Password, #txt_de_RepitaPassword, #chk_sn_Activo").attr("disabled", "disabled");
				}
				else
				{
					$("#div_titulo_agregar_editar").html("EDITAR USUARIO");
					$("#btn_guardar").attr("title", "Guardar").text("Guardar");
					$("#txt_nb_Usuario, #txt_de_Password, #txt_de_RepitaPassword, #chk_sn_Activo").removeAttr("disabled");
					$("#ddl_recursos").attr("disabled", "disabled");
				}
				
				fnc_catalogos_mostrar_ocultar( "div_grid", "div_guardar_editar_eliminar" );
				
				$("#tr_sn_Activo").show();
			}
			else
				fnc_notificacion("Debe seleccionar un Usuario.");
		}
		
		$("#hid_accion").val( p_accion );
		
		//Limpiamos los campos.
		if( p_accion == 1 )
			fnc_limpiar_campos_usuarios();
		
		$("#ddl_recursos").focus();
	}
	
	//FunciÃ³n para guardar, editar o eliminar.
	function fnc_usuarios_crud()
	{
		//Validamos los campos obligaorios.
		if( fnc_validar_campos_obligatorios("div_usuarios_crud") )
		{ 
			//Validar que la contraseÃ±a coincida.
			if( $.trim( $("#txt_de_Password").val() ) == $.trim( $("#txt_de_RepitaPassword").val() ) )
			{
				//Armamos los parÃ¡metros.
				var parametros = "id_Empresa=" + $("#hid_id_Empresa").val() + "&method=";
				
				switch( $("#hid_accion").val() )
				{
					case "1": //Agregar.
								parametros += "Usuarios_Agregar&id_Recurso=" + $("#txt_id_Recurso").val()
									+ "&nb_Usuario=" + fnc_codificar( $.trim( $("#txt_nb_Usuario").val() ) )
									+ "&de_Password=" + fnc_codificar( $.trim( $("#txt_de_Password").val() ) )
								break;
					case "2": //Editar
								parametros += "Usuarios_Editar&id_Recurso=" + $("#txt_id_Recurso").val()
									+ "&id_Usuario=" + g_row.id_Usuario
									+ "&nb_Usuario=" + g_row.nb_Usuario
									+ "&nb_UsuarioNuevo=" + fnc_codificar( $.trim( $("#txt_nb_Usuario").val() ) );
									
									if( g_row.de_Password !=  fnc_codificar( $.trim( $("#txt_de_Password").val())) )
									{
										parametros += "&de_Password=" + fnc_codificar( $.trim( $("#txt_de_Password").val() ) );
									}
									parametros += "&sn_Activo=" + ( $("#chk_sn_Activo").is(":checked") ? "1" : "0" );
								break;
					case "3": //Eliminar
								parametros += "Usuarios_Eliminar&id_Recurso=" + $("#txt_id_Recurso").val()
									+ "&id_Usuario=" + g_row.id_Usuario
								break;
				}
				
				//Guardamos, Editamos, Eliminamos.
				fnc_peticionAjax({
					url: "./application/configuracion.cfc",
					parametros: parametros,
					f_callback: function(){
						//Recargamos el grid.
						ggrids.recargar("grid_usuarios");
						
						if( $("#hid_accion").val() == "2" || $("#hid_accion").val() == "3" )
							fnc_catalogos_mostrar_ocultar( "div_grid", "div_guardar_editar_eliminar" );
						else
							fnc_limpiar_campos_usuarios();
					}
				});
			}
			else
			{
				fnc_mostrar_aviso("gd_avisos", "La contrase&ntilde;a no coincide.", 5);
			}
		}
	}
	
	//FunciÃ³n para limpiar los campos.
	function fnc_limpiar_campos_usuarios()
	{
		$("#txt_id_Recurso").val("")
		$("#txt_nb_Usuario").val("");
		$("#txt_de_Password").val("");
		$("#txt_nb_NombreCompleto").val("");
		$("#txt_de_RepitaPassword").val("");
		$("#tr_sn_Activo").hide();
		$("#buscar_recurso").show();
	}
	
	//FunciÃ³n para buscar mÃ³dulos.
	function fnc_buscar_usuarios()
	{
		//Armamos los parÃ¡metros.
		var parametros = ( $("#txt_nb_UsuarioBuscar").val() == "" ? "" : "&nb_Usuario=" + fnc_codificar($.trim($("#txt_nb_UsuarioBuscar").val())) )
				+ ( $("#txt_nb_recursosBuscar").val() == "" ? "" : "&nb_Recurso=" + fnc_codificar( $.trim($("#txt_nb_recursosBuscar").val())))
				+ ( $("#txt_apellidoPaterno_busqueda").val() == "" ? "" : "&nb_ApellidoPaterno=" + fnc_codificar($.trim($("#txt_apellidoPaterno_busqueda").val() )))
				+ "&sn_Activo=" + ( $("#txt_sn_ActivoBuscar").is(":checked") ? "1" : "0" ) + ( $("#txt_nu_Empleado").val() == "" ? "" : "&nu_Empleado=" + fnc_codificar($.trim($("#txt_nu_Empleado").val() )));
			
		//Buscamos.
		ggrids.recargar("grid_usuarios", parametros);
	}
	//Abrir grid para mostrar los recursos
	function fnc_abrir_grid_recursos()
	{
		fnc_catalogos_mostrar_ocultar('div_seleccionar_recurso','div_guardar_editar_eliminar');
	 	ggrids.crear("grid_recursos");
		//ggrids.recargar("grid_gerentes");	   
	}
	function fnc_seleccionar_recurso()
	{
		var row = ggrids.obtener_renglon("grid_recursos");
		// atributos de row nb_NombreCompleto,nb_Area,de_Puesto,id_Empresa,id_Recurso,id_Puesto,id_Area
		if(row)
		{		
				ggrids.recargar("grid_recursos");
				$("#txt_id_Recurso").val(row.id_Recurso);
				$("#txt_nb_NombreCompleto").val(row.nb_NombreCompleto);
				
				fnc_catalogos_mostrar_ocultar('div_guardar_editar_eliminar','div_seleccionar_recurso', function(){
					$("#txt_nb_Usuario").focus();
				});
		}
		else
		{
			// si no se selecciono ningun gerente mostrar el cuadro de notificacion;
			fnc_notificacion("Debe seleccionar un Recurso.");
		}
	}
	
	function fnc_div_recurso_regresar()
	{	
		fnc_catalogos_mostrar_ocultar('div_guardar_editar_eliminar','div_seleccionar_recurso');
	}
	//FunciÃ³n para buscar recursos.
	function fnc_buscar_recursos()
	{
		//Armamos los parÃ¡metros.
		var parametros = ( $("#txt_nb_RecursoBuscar").val() == "" ? "" : "&nb_NombreRecurso=" + fnc_codificar($.trim($("#txt_nb_RecursoBuscar").val())) )
				+ ( $("#txt_apellidoPaterno_busquedaRecurso").val() == "" ? "" : "&nb_ApellidoPaterno=" + fnc_codificar($.trim($("#txt_apellidoPaterno_busquedaRecurso").val() ))) + ( $("#txt_nu_EmpleadoRecursoBuscar").val() == "" ? "" : "&nu_Empleado=" + fnc_codificar($.trim($("#txt_nu_EmpleadoRecursoBuscar").val() )));
		
		ggrids.recargar("grid_recursos", parametros);
	}


</script>

	<center>
        <div style="width: 1100px;">
            <div class="ui-widget-header catalogos_cabecero titulo_opcion">CAT&Aacute;LOGO DE USUARIOS</div>
            <div class="catalogos_cabecero_contenido" id="div_grid">
            	<div class="ui-state-default catalogos_busqueda" id="div_busqueda">
								<table style="width: 100%;">
									<tr>
										<td style="text-align: left;"><span>No. Empleado Coppel:</span> 
											<input type="text" id="txt_nu_Empleado" class="input_text solo_numeros"   onChange="fnc_buscar_usuarios();" maxlength="50" />
										</td>
										<td style="text-align: left;"><span>Usuario:</span> 
											<input $ write & ! java/io/Writer (
 ) '	 type="text" id="txt_nb_UsuarioBuscar" class="input_text" onChange="fnc_buscar_usuarios();" maxlength="50" />
										</td>
										
										<td style="text-align: left;"><span>Nombre:</span>
											<input type="text" id="txt_nb_recursosBuscar" class="input_text"  onChange="fnc_buscar_usuarios();" maxlength="50" />
										</td>
										<td style="text-align: left;"><span>Apellido Paterno:</span>
											<input type="text" id="txt_apellidoPaterno_busqueda" class="input_text"  onChange="fnc_buscar_usuarios();" />
										</td>	
										<td style="text-align: left;"><span>Activo:</span>
											<input style="vertical-align:middle;" type="checkbox" checked="checked" id="txt_sn_ActivoBuscar" onChange="fnc_buscar_usuarios();" />
										</td>
									
									</tr>
								</table>
                   
                </div>
                <div id="grid_usuarios" class="ggrids" paginado="10" titulo="Listado de Usuarios"
                	url="application/configuracion.cfc?method=Usuarios_ListarJSON"
                	ancho="1000" sortname="nb_Usuario" sortorder="ASC"
                    parametros="&sn_Activo=1"
                    loadComplete="fnc_autoajustar_ancho_busqueda('grid_usuarios','div_busqueda')">
                    
                    
										<columna nom_columna="nu_Empleado">No. Empleado Coppel</columna>
                    <columna nom_columna="nb_Usuario">Usuario</columna>
                    <columna nom_columna="de_Password" visible="false">Contrase&ntilde;a</columna>
                    <columna nom_columna="nb_NombreCompletoRecurso">Recurso</columna>
                    <columna nom_columna="de_EstatusUsuario" align="center">Estatus</columna>
                    
                    
                    <columna nom_columna="id_Usuario" visible="false"></columna>
                    <columna nom_columna="id_Empresa" visible="false"></columna>
                    <columna nom_columna="id_Recurso" visible="false"></columna>
                    <columna nom_columna="sn_Activo" visible="false"></columna>
                    
                    
                    <accion accion="agregar" evento="fnc_usuarios_agregar_editar(1);"></accion>
                    <accion accion="editar" evento="fnc_usuarios_agregar_editar(2);"></accion>
                    <accion accion="eliminar" evento="fnc_usuarios_agregar_editar(3);"></accion>
                </div>
            </div>
            
            
            <div id="div_guardar_editar_eliminar" class="catalogos_guardar_editar">
            	<div class="ui-widget-header catalogos_guardar_editar_cabecero" id="div_titulo_agregar_editar"></div>
                <div class="catalogos_guardar_editar_cabecero_contenido" id="div_usuarios_crud">
                    <table> 
                    	<tr>
                            <td style="text-align: right;">Recurso: </td>
                             <input type="hidden" id="txt_id_Recurso"/>
                            <td>
                            	<input type="text" class="input_text obligatorio" disabled="disabled" id="txt_nb_NombreCompleto"
                                	acercade="Recurso" style="width: 200px;" maxlength="120"  /><span> *</span>
                            </td>
                            <td>
                            	<span><a id="buscar_recurso"  href="javascript:fnc_abrir_grid_recursos();">
                                	<img src="./images/lupa.gif" alt="Buscar Recurso" width="30" height="30" border="0" /></a>
                                </span>
                            </td>
                        </tr> 
                        <tr>
												
                            <td style="text-align: right;">Usuario: </td>
                            <td><input class="input_text obligatorio" acercade="Usuario" type="text" id="txt_nb_Usuario" maxlength="50"
                            	style="width: 200px;" onKeyUp="fnc_enter_presionado(event, 'fnc_usuarios_crud()');" /><span> *</span></td>
                        </tr> 
                        <tr>
                            <td style="text-align: right;">Contrase&ntilde;a: </td>
                            <td><input class="input_text obligatorio" acercade="Contrase&ntilde;a" type="password" id="txt_de_Password" maxlength="50"
                            	style="width: 200px;" onKeyUp="fnc_enter_presionado(event, 'fnc_usuarios_crud()');" /><span> *</span></td>
                        </tr> 
                        <tr>
                            <td style="text-align: right;">Repita Contrase&ntilde;a: </td>
                            <td><input class="input_text obligatorio" acercade="Repita Contrase&ntilde;a" type="password" id="txt_de_RepitaPassword"
                            	maxlength="25" style="width: 200px;" onKeyUp="fnc_enter_presionado(event, 'fnc_usuarios_crud()');" /><span> *</span></td>
                        </tr> 
                        <tr id="tr_sn_Activo" style="display: none;">
                            <td style="text-align: right;">Activo: </td>
                            <td style="text-align: left;"><input type="checkbox" id="chk_sn_Activo" /></td>
                        </tr> 
                    </table>
                    
                    <div style="margin-top: 10px;">
                    	<button class="boton" title="Guardar" id="btn_guardar" onClick="fnc_usuarios_crud();">Guardar</button>
                        <button class="boton" title="Regresar" onClick="fnc_catalogos_mostrar_ocultar( 'div_grid', 'div_guardar_editar_eliminar' );">Regresar</button>
                    </div>
                </div>
            </div>
         
  			
            <div id="div_seleccionar_recurso"  style="padding: 1px; margin-top: 1px;"  class="catalogos_guardar_editar">
                <div id="div_titulo_recurso" class="ui-widget-header catalogos_guardar_editar_cabecero">SELECCIONE RECURSO</div>
                <div class="catalogos_guardar_editar_cabecero_contenido" id="div_recurso_crud">
                    <div class="ui-state-default catalogos_busqueda" id="div_busqueda_recursos">
                        <table style="width: 100%;">
                            <tr>
																<td style="text-align: left;"><span>No. Empleado Coppel:</span>
																	<input type="text" id="txt_nu_EmpleadoRecursoBuscar" class="input_text solo_numeros" onChange="fnc_buscar_recursos();" maxlength="50" />
																</td>
                                <td style="text-align: left;"><span>Nombre:</span>
                                    <input type="text" id="txt_nb_RecursoBuscar" class="input_text" onChange="fnc_buscar_recursos();" maxlength="50" />
                                </td>
                                 <td style="text-align: left;"><span>Apellido Paterno:</span>
                                    <input type="text" id="txt_apellidoPaterno_busquedaRecurso" class="input_text"  onChange="fnc_buscar_recursos();" />
                                </td>	
                            </tr>
                        </table>
                    </div>

					
                    <div id="grid_recursos" class="ggrids" 
                    	url="application/catalogos_danielf.cfc?method=Recursos_Listar_Gerentes_JSON&id_Empresa= + $class$coldfusion$tagext$io$OutputTag Ljava/lang/Class; coldfusion.tagext.io.OutputTag / forName %(Ljava/lang/String;)Ljava/lang/Class; 1 2 java/lang/Class 4
 5 3 - .	  7 _initTag P(Ljava/lang/Class;ILjavax/servlet/jsp/tagext/Tag;)Ljavax/servlet/jsp/tagext/Tag; 9 :
  ; coldfusion/tagext/io/OutputTag = _setCurrentLineNo (I)V ? @
  A 	hasEndTag (Z)V C D coldfusion/tagext/GenericTag F
 G E 
doStartTag ()I I J
 > K REQUEST M java/lang/String O SESSION Q 
ID_EMPRESA S _resolveAndAutoscalarize 9(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/Object; U V
  W _String &(Ljava/lang/Object;)Ljava/lang/String; Y Z coldfusion/runtime/Cast \
 ] [ doAfterBody _ J
 > ` doEndTag b J coldfusion/tagext/QueryLoop d
 e c doCatch (Ljava/lang/Throwable;)V g h
 e i 	doFinally k 
 > l
2" 
                        autocrear="false"
                        titulo="Listado de Recursos"
                        ancho="800" 
                        ondblClickRow="fnc_seleccionar_recurso();"
                        loadComplete="fnc_autoajustar_ancho_busqueda('grid_recursos','div_busqueda_recursos');">
                        
                        
                        
                        
                        <columna nom_columna="nu_Empleado">No. Empleado Coppel</columna>
                        <columna nom_columna="nb_NombreCompleto">Nombre</columna>
                        <columna nom_columna="nb_Area">&Aacute;rea</columna>
                        <columna nom_columna="de_Puesto">Puesto</columna>
                        <columna nom_columna="nu_Telefono" visible="false">Tel&eacute;fono</columna>
                        <columna nom_columna="de_Email" visible="false">Correo</columna>
                        <columna nom_columna="nb_TipoRecurso" visible="false">Tipo de Recurso</columna>
                        
                        
                    
                        <columna nom_columna="nb_NombreRecurso" 	visible="false"></columna>
                        <columna nom_columna="nb_ApellidoPaterno" 	visible="false"></columna>
                        <columna nom_columna="nb_ApellidoMaterno" 	visible="false"></columna>
                        <columna nom_columna="id_Empresa" 			visible="false"></columna>
                        <columna nom_columna="id_Recurso" 			visible="false"></columna>
                        <columna nom_columna="id_Puesto" 			visible="false"></columna>
                        <columna nom_columna="sn_Activo" 			visible="false"></columna>
                        <columna nom_columna="id_Horario" 			visible="false"></columna>
                        <columna nom_columna="id_Area" 				visible="false"></columna>
                        <columna nom_columna="id_TipoRecurso" 		visible="false"></columna>
                        
                    </div>
                    
                    <div style="margin-top: 10px;">
                    	<button class="boton" title="Seleccionar" id="btn_guardar" onClick="fnc_seleccionar_recurso();">Seleccionar</button>
                        <button class="boton" title="Regresar" onClick="fnc_div_recurso_regresar();">Regresar</button>
                    </div>
                </div>
              	
  
            </div> 
   
            
        </div>
    </center>
    
    <input type="hidden" id="hid_accion" />
 n metaData Ljava/lang/Object; p q	  r &coldfusion/runtime/AttributeCollection t java/lang/Object v ([Ljava/lang/Object;)V  x
 u y this Lcfusuarios2ecfm333051864; LocalVariableTable Code <clinit> getMetadata ()Ljava/lang/Object; runPage out Ljavax/servlet/jsp/JspWriter; value output0  Lcoldfusion/tagext/io/OutputTag; mode0 I t6 t7 Ljava/lang/Throwable; t8 t9 LineNumberTable java/lang/Throwable � 1       - .    p q        ~   #     *� 
�    }        { |       ~   5     0� 6� 8� uY� w� z� s�    }        { |    � �  ~   "     � s�    }        { |    � �  ~  l  
   �*� � L*� N*� #+%� *+,� **� 8-� <� >:*C� B� H� LY6� &+*N� PYRSYTS� X� ^� *� a���� f� :� #�� � #:� j� � :� �:	� m�	+o� *�  : t � � z } � � : t � � z } � � � � � � � � � �  }   f 
   � { |     � � �    � � q    �      � � �    � � �    � � q    � � �    � � �    � � q 	 �     FC FC EC C              