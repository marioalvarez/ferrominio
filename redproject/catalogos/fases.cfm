����  - � 
SourceFile @C:\ColdFusion9\wwwroot\ferrominio\redproject\catalogos\fases.cfm cffases2ecfm683279230  coldfusion/runtime/CFPage  <init> ()V  
  	 com.macromedia.SourceModTime  J1 coldfusion/runtime/CfJspPage  pageContext #Lcoldfusion/runtime/NeoPageContext;  	   getOut ()Ljavax/servlet/jsp/JspWriter;   javax/servlet/jsp/PageContext 
   parent Ljavax/servlet/jsp/tagext/Tag;  	   com.adobe.coldfusion.*  bindImportPath (Ljava/lang/String;)V   !
  "�
<script type="text/javascript">
	//Variables globales.
	var g_row;
	
	//Función para abrir la opción de agregar y/o editar.
	function fnc_fases_agregar_editar(p_accion)
	{		
		if( p_accion == 1 ) //Agregar.
		{
			$("#div_titulo_agregar_editar").html("NUEVA FASE");
			$("#btn_guardar").attr("title", "Guardar").text("Guardar");
			/*Modificar*/
			//Nos aseguramos de desbloquear los input.      
			$("#txt_nb_Fase").removeAttr("disabled");
			
			fnc_catalogos_mostrar_ocultar( "div_grid", "div_guardar_editar_eliminar" );
		}
		else if( p_accion == 2 || p_accion == 3 ) //Editar o Eliminar.
		{
			//Validamos que se haya seleccionado un renglón.
			var row = ggrids.obtener_renglon("grid_fases");
			
			if( row )
			{	
				g_row = row;
				//Llenamos los campos.
				$("#txt_nb_Fase").val( row.nb_Fase );
				//$("#txt_nu_Orden").val( row.nu_Orden );

				//Valdiar si debemos bloquear o desbloquear los input.
				if( p_accion == 3 )
				{
					$("#div_titulo_agregar_editar").html("ELIMINAR FASE");
					$("#btn_guardar").attr("title", "Eliminar").text("Eliminar");
					$("#txt_nb_Fase").attr("disabled","disabled");
				}
				else
				{
					$("#div_titulo_agregar_editar").html("EDITAR FASE");
					$("#btn_guardar").attr("title", "Guardar").text("Guardar");
					$("#txt_nb_Fase").removeAttr("disabled");
				}
				
				fnc_catalogos_mostrar_ocultar( "div_grid", "div_guardar_editar_eliminar" );				
			}
			else
				fnc_notificacion("Debe seleccionar una Fase");
				
		}
		
		$("#hid_accion").val( p_accion );
		
		//Limpiamos los campos.
		if( p_accion == 1 )
		{
			$("#txt_nb_Fase").val("");			
		}
		
		$("#txt_nb_Fase").focus();
	}
	
	//Función para guardar y/o editar.
	function fnc_fases_crud()
	{	
		//Validamos los campos obligatorios.
		if(fnc_validar_campos_obligatorios("div_fases_crud"))
		{
			//Armamos los parámetros.
			var accion= new Array("Fases_Agregar","Fases_Editar","Fases_Eliminar");
			var parametros = "id_Empresa=" + $("#hid_id_Empresa").val() + "&method="+ accion[$("#hid_accion").val()-1];
			
			// VALORES DE LOS CAMPOS DEL DIV PARA AGREGAR EDITAR ELIMINAR
			var fase = fnc_codificar(  $.trim( $("#txt_nb_Fase").val())  );
			
			switch( $("#hid_accion").val() )
			{
				case "1": //Agregar.
							parametros+=
							"&nb_Fase="+ fase+
						    "&nu_orden="+ 0;
						
							break;
				case "2": //Editar	
						    parametros+=
							"&id_Fase="+ g_row.id_Fase+
							"&nb_Fase="+ fase+
							"&nu_orden="+ 0;			
							
							break;
				case "3" : //Eliminar
							parametros+=
							"&id_Fase="+ g_row.id_Fase;
							
							break;
			}
			fnc_peticionAjax(
			{
				url: "./application/catalogos_flavio.cfc",
				parametros: parametros,
				f_callback: function()
				{
					//Actualizamos el grid.
					ggrids.recargar("grid_fases");
			
					if( $("#hid_accion").val() == "1" )
						{
							$("#txt_nb_Fase").val("");
							$("#txt_nb_Fase").focus();
						}
					else
						fnc_catalogos_mostrar_ocultar( "div_grid", "div_guardar_editar_eliminar" );
				}
			});
		}
	}
	
	//Función para buscar fases.
	function fnc_buscar_fases()
	{
		//Armamos los parámetros.
		var parametros = "";
		if( $.trim( $("#txt_fase_busqueda").val() ) != "" )
			parametros = parametros + "&nb_Fase=" + fnc_codificar( $.trim( $("#txt_fase_busqueda").val() ) );

		ggrids.recargar("grid_fases", parametros);
	}
</script>
	<center>
        <div style="margin-left:5%; margin-right:5%; width:90%">
            <div class="ui-widget-header catalogos_cabecero titulo_opcion">CAT&Aacute;LOGO DE FASES</div>
            <div class="catalogos_cabecero_contenido" id="div_grid">
            	<div id="div_busqueda" class="ui-state-default catalogos_busqueda">
                	<table style="width: 100%;">
                    	<tr>                        	
                            <td style="text-align: left; padding-left:100px;"><span>Fase: </span>
                            <input type="text" id="txt_fase_busqueda" class="input_text" onkeydown="if (event.keyCode == 13) {fnc_buscar_fases();}" onChange="fnc_buscar_fases();" /></td>                            
                            
                        </tr>
                    </table>
                </div>
                <div id="grid_fases" class="ggrids" 
                	url="application/catalogos_flavio.cfc?method=Fases_ListarJSON&id_Empresa= $ write & ! java/io/Writer (
 ) ' $class$coldfusion$tagext$io$OutputTag Ljava/lang/Class; coldfusion.tagext.io.OutputTag - forName %(Ljava/lang/String;)Ljava/lang/Class; / 0 java/lang/Class 2
 3 1 + ,	  5 _initTag P(Ljava/lang/Class;ILjavax/servlet/jsp/tagext/Tag;)Ljavax/servlet/jsp/tagext/Tag; 7 8
  9 coldfusion/tagext/io/OutputTag ; _setCurrentLineNo (I)V = >
  ? 	hasEndTag (Z)V A B coldfusion/tagext/GenericTag D
 E C 
doStartTag ()I G H
 < I SESSION K java/lang/String M 
ID_EMPRESA O _resolveAndAutoscalarize 9(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/Object; Q R
  S _String &(Ljava/lang/Object;)Ljava/lang/String; U V coldfusion/runtime/Cast X
 Y W doAfterBody [ H
 < \ doEndTag ^ H coldfusion/tagext/QueryLoop `
 a _ doCatch (Ljava/lang/Throwable;)V c d
 a e 	doFinally g 
 < h�" 
                    titulo="Listado de Fases"
                	ancho="800" sortname="id_Fase" 
                    autowidth="false" 
                    loadComplete="fnc_autoajustar_ancho_busqueda('grid_fases','div_busqueda')">
                    
                    <columna nom_columna="id_Fase" visible="false"></columna>
                    <columna nom_columna="nb_Fase">Fase</columna>
                    

                    <columna nom_columna="id_Empresa" visible="false"></columna>
                    
                    <accion accion="agregar" evento="fnc_fases_agregar_editar(1);"></accion>
                    <accion accion="editar" evento="fnc_fases_agregar_editar(2);"></accion>
                    <accion accion="eliminar" evento="fnc_fases_agregar_editar(3);"></accion>
                </div>
            </div>
            
            
            <div id="div_guardar_editar_eliminar" class="catalogos_guardar_editar">
            	<div class="ui-widget-header catalogos_guardar_editar_cabecero" id="div_titulo_agregar_editar"></div>
                <div class="catalogos_guardar_editar_cabecero_contenido" id="div_fases_crud">
                    <table id="table_clientes_guardar_editar"> 
                        <tr>
                            <td style="text-align: right;"><span>Fase: </span></td>
                            <td style="text-align: left;"><input class="input_text obligatorio" type="text" acercade="Fase" id="txt_nb_Fase" maxlength="150" style="width: 250px;" /></td>
                        </tr>                       
                                               
                    </table>                    
                    <div style="margin-top: 10px;">
                    	<button class="boton" title="Guardar" id="btn_guardar" onClick="fnc_fases_crud();">Guardar</button>
                        <button class="boton" title="Regresar" onClick="fnc_catalogos_mostrar_ocultar( 'div_grid', 'div_guardar_editar_eliminar' );">Regresar</button>
                    </div>
                </div>
            </div>
        </div>
    </center>
    <input type="hidden" id="hid_accion" />

 j metaData Ljava/lang/Object; l m	  n &coldfusion/runtime/AttributeCollection p java/lang/Object r ([Ljava/lang/Object;)V  t
 q u this Lcffases2ecfm683279230; LocalVariableTable Code <clinit> getMetadata ()Ljava/lang/Object; runPage out Ljavax/servlet/jsp/JspWriter; value output0  Lcoldfusion/tagext/io/OutputTag; mode0 I t6 t7 Ljava/lang/Throwable; t8 t9 LineNumberTable java/lang/Throwable � 1       + ,    l m        z   #     *� 
�    y        w x    {   z   5     .� 4� 6� qY� s� v� o�    y        w x    | }  z   "     � o�    y        w x    ~ }  z  a  
   �*� � L*� N*� #+%� **� 6-� :� <:* �� @� F� JY6� !+*L� NYPS� T� Z� *� ]���� b� :� #�� � #:� f� � :� �:	� i�	+k� *�  4 i u � o r u � 4 i � � o r � � u � � � � � � �  y   f 
   � w x     �  �    � � m    �      � � �    � � �    � � m    � � �    � � �    � � m 	 �     @ � @ � ? �  �              