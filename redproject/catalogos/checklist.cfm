����  - � 
SourceFile DC:\ColdFusion9\wwwroot\ferrominio\redproject\catalogos\checklist.cfm cfchecklist2ecfm834792350  coldfusion/runtime/CFPage  <init> ()V  
  	 com.macromedia.SourceModTime  J1 coldfusion/runtime/CfJspPage  pageContext #Lcoldfusion/runtime/NeoPageContext;  	   getOut ()Ljavax/servlet/jsp/JspWriter;   javax/servlet/jsp/PageContext 
   parent Ljavax/servlet/jsp/tagext/Tag;  	   com.adobe.coldfusion.*  bindImportPath (Ljava/lang/String;)V   !
  "�
<script>

	//FunciÃ³n para abrir la opciÃ³n de agregar, editar o eliminar. check list y preguntas
	function fnc_checklist_agregar_editar(p_accion)
	{
		$("#tituloRespuestas").hide();
		$("#txt_nb_Nombre").removeAttr("disabled");
		if( p_accion == 1 ) //Agregar.
		{
			$("#div_titulo_agregar_editar").html("NUEVO CHECK LIST");
			$("#btn_guardar").attr("title", "Guardar").text("Guardar");
			$("#txt_nb_Nombre").val("");
			
			fnc_catalogos_mostrar_ocultar( "div_grid", "div_guardar_editar_eliminar", function(){
				$("#txt_nb_Nombre").focus();
			} );
			
			$("#div_checklist_contenido").fadeOut(200);
			$("#div_detalle_agregar_preguntas").fadeOut(200);
		}
		else if( p_accion == 2 || p_accion == 3 ) //Editar o Eliminar.
		{
			setTimeout(function() {$("#txt_nb_Nombre").focus();}, 100);
			$("#div_checklist_contenido").fadeOut(200);
			$("#div_detalle_agregar_preguntas").fadeOut(200);
			//Validamos que se haya seleccionado un renglÃ³n.
			var row = ggrids.obtener_renglon("grid_checklist");
			
			if( row )
			{	
					g_row = row;

					//Valdiar si debemos bloquear o desbloquear los input.
					if( p_accion == 3 )
					{
						$("#txt_nb_Nombre").val(row.nb_Checlist);
						$("#txt_nb_Nombre").attr("disabled", "disabled");
						$("#div_titulo_agregar_editar").html("ELIMINAR CHECK LIST");
						$("#btn_guardar").attr("title", "Eliminar").text("Eliminar");
					}
					else
					{
						$("#txt_nb_Nombre").val(row.nb_Checlist);
						$("#div_titulo_agregar_editar").html("EDITAR CHECK LIST");
						$("#btn_guardar").attr("title", "Guardar").text("Guardar");
					
					}
					
					fnc_catalogos_mostrar_ocultar( "div_grid", "div_guardar_editar_eliminar" );
			}
			else
				fnc_notificacion("Debe seleccionar un <b>Check List</b>.");
		}
	
		
		if(p_accion == 4)
		{
			$("#tituloRespuestas").show();
			$("#div_titulo_agregar_editar_preguntas").html("NUEVA PREGUNTA");
			$("#btn_agregar_pregunta").text("Agregar Pregunta");
			$("#btn_agregar_pregunta").attr("onClick","fnc_agregar_pregunta_nueva();");
			$("#txt_de_Pregunta").val("");

			$(".seleccion").removeClass("seleccion"); // LE quitamos la seleccion
			
			fnc_catalogos_mostrar_ocultar( 'div_checklist_contenido', 'div_detalle_agregar_preguntas', function(){
				$("#txt_de_Pregunta").focus();
				$("#btn_agregar_respuesta").text("Agregar Respuesta");
				$("#btn_agregar_respuesta").attr("title","Agregar");
				$("#btn_agregar_respuesta").attr("onClick","fnc_agregar_respuesta_nueva();");
				fnc_limpiar_campos();
			} );
			
		}else if( p_accion == 5 || p_accion == 6 ) //Editar o Eliminar.
		{
			$("#tituloRespuestas").show();
			if($(".seleccion").hasClass("seleccion")) //Si una esta seleccionada..
			{
					if(p_accion == 5 )
					{
						$("#div_titulo_agregar_editar_preguntas").html("EDITAR PREGUNTA");
						
						var nombre_pregunta = $.trim($(".seleccion").children().children('.titulo_pregunta').text()); //Sacamos el nombre de la pregunta
						/*nombre_pregunta = nombre_pregunta.split(".-");
						nombre_pregunta = $.trim(nombre_pregunta[1]);*/
					
						$("#txt_de_Pregunta").val(nombre_pregunta);
						$("#btn_agregar_pregunta").text("Editar Pregunta");
						$("#btn_agregar_pregunta").attr("onClick","fnc_editar_pregunta_nueva();");
						$("ddl_preguntas").val(0);
						$("#ddl_respuestas").val(0);
						$("#txt_descripcion").val("");
						$("#btn_agregar_respuesta").text("Agregar Respuesta");
						$("#btn_agregar_respuesta").attr("title","Agregar");
						$("#btn_agregar_respuesta").attr("onClick","fnc_agregar_respuesta_nueva();");
						
						fnc_catalogos_mostrar_ocultar( 'div_checklist_contenido', 'div_detalle_agregar_preguntas' );
						fnc_editar_pregunta();
						$("#txt_de_Pregunta").focus();
					}else if(p_accion == 6)
					{
						$(".seleccion").remove();
					}
					
			}else //Si no mandamos mensaje
			{
					fnc_notificacion("Debe seleccionar una <b>Pregunta</b>.");
			}
		
		}
		$("#hid_accion").val( p_accion );
	}
	

	//Funcion para guardar,editar y eliminar un checklist 
	function fnc_checklist_crud()
	{
		//Validamos los campos obligaorios.
		if( fnc_validar_campos_obligatorios("div_guardar_editar_eliminar") )
		{
			//Armamos los parÃ¡metros.
			var parametros = "id_Empresa=" + $("#hid_id_Empresa").val() + "&method=";
			
			switch( $("#hid_accion").val() )
			{
				case "1": //Agregar.
							parametros += "CheckList_Agregar&nb_Checlist=" + fnc_codificar( $.trim( $("#txt_nb_Nombre").val() ) );
							break;
				case "2": //Editar
							parametros += "CheckList_Editar&id_Checklist=" + g_row.id_Checklist 
								+ "&nb_Checlist=" + fnc_codificar( $("#txt_nb_Nombre").val() )
								+"&nb_Checlist_viejo="+g_row.nb_Checlist;
							break;
				case "3": //Eliminar
							parametros += "CheckList_Eliminar&id_Checklist=" + g_row.id_Checklist;
							break;
			}
			
			//Guardamos, Editamos, Eliminamos.
			fnc_peticionAjax({
				url: "./application/checklist.cfc",
				parametros: parametros,
				f_callback: function(){
					//Recargamos el grid.
					ggrids.recargar("grid_checklist");
					$("#txt_nb_Nombre").focus();
					if( $("#hid_accion").val() == "2" || $("#hid_accion").val() == "3" )
						fnc_catalogos_mostrar_ocultar( "div_grid", "div_guardar_editar_eliminar" );
					else
						fnc_limpiar_campos();
						
				}
				
			});
		}
	}
	
	//Funcion que muestra el detalle con las preguntas para un checklist
	function fnc_verDetalle()
	{
		var row = ggrids.obtener_renglon("grid_checklist");
		if( row )
		{
			// ARMAMOS LOS PARAMETORS A ENVIAR
			var parametros = "id_Empresa=" + $("#hid_id_Empresa").val() 
							+ "&id_CheckList=" + row.id_Checklist;
			//Guardamos, Editamos, Eliminamos.
			var nb_Checlist = row.nb_Checlist;
			
			fnc_peticionAjax({
				url: "./catalogos/checklist_preguntas.cfm",
				parametros: parametros,
				automensaje:false,
				div_success: "div_ver_detalle_checklist",
				f_callback: function(respuestaJSON)
				{
					$("#div_checklist_contenido").fadeIn(200);
					var Titulo = 'Check List "' + nb_Checlist  + '"' ;
					setTimeout(function() {$("#div_titulo_checklist").text(Titulo);}, 100);
				}
					
			});
		}else
		{
			fnc_notificacion("Debe seleccionar un <b>Check List</b>.");
		}
	}
	//Cuando se le da click a una pregunta agrega el fondo amarillo
	//O lo quita dependiendo si esta seleccionada o no
	function fnc_seleccionar_pregunta(pregunta)
	{
		if( $(pregunta).hasClass("seleccion") ) //Si una NO esta seleccionada..
		{	

			$(pregunta).removeClass("seleccion"); //Le quitamos la seleccion (esto por si se le da click a la misma pregunta
		}else
		{
			$("#contenedor_checklist").children().removeClass("seleccion"); // Le quitamos la seleccion a cualquier que este seleccionado.
		
			$(pregunta).addClass("seleccion"); //Le agregamos la seleccion al actual.
		}

	}
	//Funcion que quita una respuesta del checklist de vista preliminar 
	function fnc_remover_respuesta(respuesta)
	{
		//event.preventDefault();
		//event.stopPropagation();

		if($("#contenedor_checklist_temporal ul li").size() == 1 )
		{

			$("#contenedor_checklist_temporal").children().remove();
			
		}
		else
		{
			$(respuesta).parent().remove();
		}
		setTimeout(function() 
		{	
				$("#ddl_respuestas").val(0);
				$("#txt_descripcion").val("");
				$("#btn_agregar_respuesta").text("Agregar Respuesta");
				$("#btn_agregar_respuesta").attr("onClick","fnc_agregar_respuesta_nueva();");
				
		}, 10);
		 return false;			
	}
	
	function fnc_agregar_respuesta_nueva()
	{
		if( fnc_validar_campos_obligatorios("div_detalle_agregar_preguntas") )
		{
			var tipo_elemento = $("#ddl_respuestas").val();
			var respuesta="";
			var checklist = "<div class='preguntas' >"
							+"<div class='titulo ui-state-default'><span class='nu_Orden'></span><span class='titulo_pregunta'>" +  $("#txt_de_Pregunta").val() + "</span></div>"
								+"<div class='respuestas'>"
									+"<ul $ write & ! java/io/Writer (
 ) '� class = 'items_checklist'>";	
			if( tipo_elemento == 1 ) 
			{
				respuesta = '<li>'+
								'<span id_Tipo="1" >' + $("#txt_descripcion").val() + '</span>'+
								'<input type="checkbox" disabled="disabled" />'+
								'<span class="ui-icon ui-icon-close" onClick="fnc_remover_respuesta(this);"></span>'+
							'</li>';
							
			}
			if( tipo_elemento == 2 ) 
			{
				 respuesta = '<li><span id_Tipo="2" >' + $("#txt_descripcion").val()
								+ '</span><input type="textbox" disabled="disabled"/></li><span class="ui-icon ui-icon-close" onClick="fnc_remover_respuesta(this);"></span>';
			}
			if( tipo_elemento == 3 ) 
			{
				 respuesta = '<li><span id_Tipo="3" >' + $("#txt_descripcion").val() 
								+ '</span><textarea disabled="disabled"/></li><span class="ui-icon ui-icon-close" onClick="fnc_remover_respuesta(this);"></span>'
			}
			respuesta+="</ul></div></div>";
			checklist += respuesta;
			
			if($("#contenedor_checklist_temporal ul li").size() == 0 )
			{
				$("#contenedor_checklist_temporal").append(checklist);
			}
			else
			{
				$("#contenedor_checklist_temporal ul").first().append(respuesta);
				
			}
			
			$("#txt_descripcion").val("");
			//$("#ddl_respuestas").val(0);
			$("#txt_descripcion").focus();							
		}
	}
	
	function fnc_editar_respuesta_nueva()
	{
		if( fnc_validar_campos_obligatorios("div_detalle_agregar_preguntas") )
		{
			var tipo_elemento = $("#ddl_respuestas").val()
			var respuesta="";
			var checklist = "<div class='preguntas' >"
							+"<div class='titulo ui-state-default'><span class='nu_Orden'></span><span class='titulo_pregunta'>" +  $("#txt_de_Pregunta").val() + "</span></div>"
								+"<div class='respuestas'>"
									+"<ul class = 'items_checklist'>";	
			if( tipo_elemento == 1 ) 
			{
				respuesta = '<li><span id_Tipo="1" >' + $("#txt_descripcion").val()
								+ '</span><input type="checkbox" disabled="disabled" /><span class="ui-icon ui-icon-close" onClick="fnc_remover_respuesta(this);"></span></li>';
			}
			if( tipo_elemento == 2 ) 
			{
				 respuesta = '<li><span id_Tipo="2" >' + $("#txt_descripcion").val()
								+ '</span><input type="textbox" disabled="disabled"/><span class="ui-icon ui-icon-close" onClick="fnc_remover_respuesta(this);"></span></li>';
			}
			if( tipo_elemento == 3 ) 
			{
				 respuesta = '<li><span id_Tipo="3" >' + $("#txt_descripcion").val() 
								+ '</span><textarea disabled="disabled"/><span class="ui-icon ui-icon-close" onClick="fnc_remover_respuesta(this);"></span></li>'
			}
		
			$("#contenedor_checklist_temporal .seleccion").replaceWith(respuesta);	
			$("#contenedor_checklist_temporal ul li").attr("onClick","fnc_ediar_respuesta(this);");	
			
			$("#ddl_respuestas").val(0);
			$("#txt_descripcion").val("");
			$("#btn_agregar_respuesta").text("Agregar Respuesta");
			$("#btn_agregar_respuesta").attr("onClick","fnc_agregar_respuesta_nueva();");
			$("#txt_descripcion").focus();		
		}
	}
	
	//Funcion que copia la pregunta seleccionada y lo agrega a la vista preliminar para editar la pregunta o respuesta
	function fnc_editar_pregunta()
	{
		$(".seleccion").clone().appendTo("#contenedor_checklist_temporal");
		$("#contenedor_checklist_temporal .seleccion").removeClass("seleccion"); //Le quitamos la selccion
		$("#contenedor_checklist_temporal").children().removeAttr("onClick"); //Le quitamos para que no se pueda seleccionar
		$("#contenedor_checklist_temporal ul li").children('.ui-icon').show(); 	//Mostramos las X para cerrar
		$("#contenedor_checklist_temporal ul li").attr("onClick","fnc_ediar_respuesta(this);"); 	//Agregamos el evento para "EDITAR" la respuesta
		$("#hid_accion_agregar_pregunta").val(1);
	}
	//Funcion que le pone el titulo a la pregunta en vist apreliminar
	function fnc_ediar_respuesta(respuesta)
	{
		$("#contenedor_checklist_temporal ul li").removeClass("seleccion"); //Le quitamos la seleccion a cualquiera que la tenga
		var descripcion = $.trim( $(respuesta).children('span').text()); //Sacamos la descripcion de la pregunta
		var tipo = $(respuesta).children('span').attr('id_Tipo'); //Sacamos el tipo
		$(respuesta).addClass("seleccion"); //La seleccionamos
		$("#txt_descripcion").val(descripcion);
		$("#ddl_respuestas").val(tipo);
		
		$("#btn_agregar_respuesta").attr("onClick","fnc_editar_respuesta_nueva();");
		$("#btn_agregar_respuesta").text("Editar Respuesta");
		$("#btn_agregar_respuesta").attr("title","Editar");
		$("#txt_descripcion").focus();
	
	}
	//Funcion que le pone el titulo a la pregunta en vist apreliminar
	function fnc_setTitulo()
	{
		$("#contenedor_checklist_temporal .titulo").children('.titulo_pregunta').text(  $("#txt_de_Pregunta").val() );
	}
	//Funcion para regresar de la vista preliminar a la vista de las preguntas 
	function fnc_cancelar_edicion()
	{
		$("#contenedor_checklist_temporal").first().children().remove();
		fnc_catalogos_mostrar_ocultar( 'div_checklist_contenido', 'div_detalle_agregar_preguntas' );
	}
	
	//Funcion que agrega la pregunta de la vistas preliminar al contenedor de preguntas de ese checklist
	function fnc_editar_pregunta_nueva()
	{
		if( $("#contenedor_checklist_temporal").first().children().size() >= 1 && $.trim( $("#txt_de_Pregunta").val() ) != "" )
		{
		
			var pregunta = $("#contenedor_checklist > .seleccion");	
			var respuesta = $("#contenedor_checklist_temporal").first().children();
			$(pregunta).replaceWith( $(respuesta).attr("onClick","fnc_seleccionar_pregunta(this)") );
			
			$("#contenedor_checklist").children('.seleccion').remove(); //Le quitamos la seleccion
			$("#contenedor_checklist ul li").removeClass('seleccion');
			$("#contenedor_checklist ul li").children('.ui-icon').hide(); 	
			if($(".preguntas").size()!=0)
			{
				fnc_calcular_orden();
			}
			fnc_limpiar_campos();
			fnc_catalogos_mostrar_ocultar( 'div_checklist_contenido', 'div_detalle_agregar_preguntas' );
		}
		else if( fnc_validar_campos_obligatorios("div_detalle_agregar_preguntas") )
		{
			if( $("#contenedor_checklist_temporal").first().children().size() <= 0 )
			{
				fnc_notificacion("Debe agregar la <b>Respuesta</b>.");
			}
		}
	}
	
	function fnc_agregar_pregunta_nueva()
	{
		if( $("#contenedor_checklist_temporal").first().children().size() >= 1 && $.trim( $("#txt_de_Pregunta").val() ) != "" )
		{
			var respuesta = $("#contenedor_checklist_temporal").first().children().attr("onClick","fnc_seleccionar_pregunta(this);");
			$("#contenedor_checklist").append(respuesta);
			
			$("#contenedor_checklist").children('.seleccion').remove(); //Le quitamos la seleccion
			$("#contenedor_checklist ul li").children('.ui-icon').hide(); 	
			if($(".preguntas").size()!=0)
			{
				fnc_calcular_orden();
			}
			
			fnc_limpiar_campos();
		}
		else if( fnc_validar_campos_obligatorios("div_detalle_agregar_preguntas") )
		{
			if( $("#contenedor_checklist_temporal").first().children().size() <= 0 )
			{
				fnc_notificacion("Debe agregar la <b>Respuesta</b>.");
			}
		}
		
		$("#txt_de_Pregunta").focus();
	}

	//Funcion que calcula el orden del check list segun su posicion.<br />
	// es llamada cuando se arrastra un checklist o se agrega uno nuevo.
	function fnc_calcular_orden()
	{
	 	var nombreAux="";
		$('.nu_Orden').each(function(index){
			nombreAux=$(this).text().split(".-");
			var nombre = ( nombreAux.length == 1 ? nombreAux[0] : nombreAux[1] )
            $(this).html('');
            $(this).text( (index + 1) + ".- " + $.trim( nombre ) );
		}); 
		$("#hid_se_modifico").val("S");
	}
	function fnc_msj_autorizar()
	{
		if( ! $(".seleccion").hasClass("seleccion")) //Si una esta seleccionada..
		{
			fnc_notificacion("Debe seleccionar una <b>Pregunta</b>.");
			return;
		}
		fnc_mostrar_aviso("gd_eliminar_pregunta", "&iquest;Desea <b>Eliminar</b> esta pregunta?" , 2);
	}
	function fnc_limpiar_campos()
	{
		$("#txt_de_Pregunta").val("");
		$("#txt_descripcion").val("");
		$("#ddl_respuestas").val(0);
		$("#txt_nb_Nombre").val("");
	}
	function fnc_guardar_cambios()
	{
	 	var seModifico = $("# +Chid_se_modifico").val();
		if(seModifico == "S")
		{
			var parametros = "id_Empresa=" + $("#hid_id_Empresa").val() + "&method=";
			var xml_preguntas = "<root>";
		
			//CREAMOS EL XML
			$(".preguntas").each(function(){
					var pregunta = $(this).children('.titulo').text().split(".-");
					var nu_Orden = pregunta[0];
					var de_Pregunta = pregunta[1].trim();
					xml_preguntas +=  "<pregunta de_Pregunta = '" + fnc_codificar( $.trim(de_Pregunta) ) + "' nu_Orden = '" + nu_Orden+ "'>";
					
					
			$(this).children('.respuestas').children().children().each(function(){
					var de_Respuesta = $(this).text(); //Saccamos e
					var id_Tipo = $(this).children('span').attr("id_Tipo");
					xml_preguntas+="<respuesta de_Respuesta = '"+ fnc_codificar( $.trim( de_Respuesta ) ) +"' id_Tipo = '" + id_Tipo +"'> </respuesta>";				
				});
				xml_preguntas += "</pregunta>"
			});
			xml_preguntas += "</root>";
		
			// ARMAMOS LOS PARAMETORS A ENVIAR
			var row = ggrids.obtener_renglon("grid_checklist");
			parametros += "Preguntas_GuardarCambios&id_Checklist=" + row.id_Checklist + "&xml_preguntas="+ xml_preguntas;
			//Guardamos, Editamos, Eliminamos.
			
				fnc_peticionAjax({
					url: "./application/checklist.cfc",
					parametros: parametros,
					f_callback: function(){
						$("#hid_se_modifico").val("N");
						if( $("#hid_accion").val() == "2" || $("#hid_accion").val() == "3" )
							fnc_catalogos_mostrar_ocultar( "div_grid", "div_checklist_contenido" );
						else
							fnc_limpiar_campos();
					}
				});
		}else
		{
			fnc_notificacion("No ha hecho cambios en este <b>Check List</b>.");
				
		}
		
	}
	
	function fnc_buscar_checklist()
	{
		//Armamos los parÃ¡metros.
		var parametros = "&nb_Checlist=" + fnc_codificar( $.trim( $("#txt_de_PuestoBuscar").val() ) );
		ggrids.recargar("grid_checklist", parametros);
		$("#div_checklist_contenido").fadeOut(200);
	}
</script>

<style>
	.seleccion
	{
		background-color:#ffef8f;
	}
	.preguntas
	{
		cursor:pointer;
	}
	.titulo
	{
		color: #34495e;
		text-align: left;
		padding:5px 0px;
		border-radius: 3px;
	}
	.respuestas ul{
		margin:0;
		padding:0;
	}
	.items_checklist li
	{
		list-style:none;
		display:inline-block;
		margin-right: 15px;
		margin-top: 1px;
		margin-bottom: 1px;
	}
	li input[type="checkbox"]
	{
		vertical-align:middle;
	}
	.acciones_checklist button
	{
		float: left;
	}
	.contenedor_preguntas_checklist li
	{
		list-style:none;
		margin-right: 15px;
	}
	.ui-icon 
	{
		display: -webkit-inline-box;
		display: inline-block;
	}
	.ui-icon:hover
	{
		cursor:pointer;
	}
</style>
<center>
	<div style="margin-left:5%; margin-right:5%; width:90%">
		<div class="ui-widget-header catalogos_cabecero titulo_opcion">CHECK LIST</div>
            
        <div style="padding: 5px; margin-top: 10px;" id="div_grid">
			<div class="gfieldset" titulo="Check List"></div> 
            <div class="ui-state-default catalogos_busqueda" id="div_busqueda">
                <table style="width: 100%;">
                    <tr>
                        <td style="text-align: center; padding-right:350px;"><span>Nombre:</span>
                        <input type="text" id="txt_de_PuestoBuscar" class="input_text" onChange="fnc_buscar_checklist();" maxlength="120" />
                        </td>
                    </tr>
                </table>
            </div>
            <div id="grid_checklist" class="ggrids" paginado="10" titulo="Check List"
                url="application/checklist.cfc?method=CheckList_ListarJSON&id_Empresa= - $class$coldfusion$tagext$io$OutputTag Ljava/lang/Class; coldfusion.tagext.io.OutputTag 1 forName %(Ljava/lang/String;)Ljava/lang/Class; 3 4 java/lang/Class 6
 7 5 / 0	  9 _initTag P(Ljava/lang/Class;ILjavax/servlet/jsp/tagext/Tag;)Ljavax/servlet/jsp/tagext/Tag; ; <
  = coldfusion/tagext/io/OutputTag ? _setCurrentLineNo (I)V A B
  C 	hasEndTag (Z)V E F coldfusion/tagext/GenericTag H
 I G 
doStartTag ()I K L
 @ M REQUEST O java/lang/String Q SESSION S 
ID_EMPRESA U _resolveAndAutoscalarize 9(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/Object; W X
  Y _String &(Ljava/lang/Object;)Ljava/lang/String; [ \ coldfusion/runtime/Cast ^
 _ ] doAfterBody a L
 @ b doEndTag d L coldfusion/tagext/QueryLoop f
 g e doCatch (Ljava/lang/Throwable;)V i j
 g k 	doFinally m 
 @ n�"
                ancho="700" sortname="nb_Checlist" sortorder="ASC"
                loadComplete="fnc_autoajustar_ancho_busqueda('grid_checklist','div_busqueda')">
                
                
                <columna nom_columna="nb_Checlist" >Nombre</columna>
                
                
                <columna nom_columna="id_Empresa" visible="false"></columna>
                <columna nom_columna="id_Checklist" visible="false"></columna>
                
                
                <accion accion="agregar" 		evento="fnc_checklist_agregar_editar(1);"></accion>
                <accion accion="editar" 		evento="fnc_checklist_agregar_editar(2);"></accion>
                <accion accion="eliminar" 		evento="fnc_checklist_agregar_editar(3);"></accion>
                <accion accion="personalizada"  class_icono="ui-icon-search" titulo="Ver detalle" evento="fnc_verDetalle();"></accion>
            </div>
        </div>
            
            
        <div id="div_guardar_editar_eliminar" class="catalogos_guardar_editar">
        	<div class="ui-widget-header catalogos_guardar_editar_cabecero" id="div_titulo_agregar_editar"></div>
        	<div class="catalogos_guardar_editar_cabecero_contenido" id="div_puestos_crud">
                <table> 
                    <tr>
                        <td style="text-align: right;">Nombre: </td>
                        <td>
                            <input class="input_text obligatorio" acercade="Nombre" type="text" id="txt_nb_Nombre" maxlength="120"
                               onkeydown="fnc_enter_presionado(event,'fnc_checklist_crud();');" style=" width: 300px;" />
                        </td>
                    </tr> 
                </table>   
                <div style="margin-top: 10px;">
                    <button class="boton" title="Guardar" id="btn_guardar" onClick="fnc_checklist_crud();">Guardar</button>
                    <button class="boton" title="Regresar" onClick="fnc_catalogos_mostrar_ocultar( 'div_grid', 'div_guardar_editar_eliminar' );">Regresar</button>
                </div>
            </div>
        </div>
            
	
        <div id="div_detalle_checklist" class="catalogos_guardar_editar">
            <div class="ui-widget-header catalogos_guardar_editar_cabecero" id="div_titulo_agregar_editar"></div>
            <div class="catalogos_guardar_editar_cabecero_contenido" id="div_puestos_crud">
                <table> 
                    <tr>
                        <td style="text-align: right;">Nombre: </td>
                        <td>
                            <input class="input_text obligatorio" acercade="Nombre" type="text" id="txt_nb_Nombre" maxlength="120"
                                style=" width: 300px;" />
                        </td>
                    </tr> 
                </table>
                
                <div style="margin-top: 10px;">
                    <button class="boton" title="Guardar" id="btn_guardar" onClick="">Guardar</button>
                    <button class="boton" title="Regresar" onClick="fnc_catalogos_mostrar_ocultar( 'div_grid', 'div_guardar_editar_eliminar' );">Regresar</button>
                </div>
            </div>
        </div>
            

        <div id="div_ver_detalle_checklist"></div>
   	</div>
</center>


<div id="gd_eliminar_pregunta" class="gdialogo" titulo="Confirmaci&oacute;n" ancho="300px">
    <div id="div_confirmacion_eliminar_pregunta"></div>
    <button class="gdboton" id="btn_elimina_pregunta" onClick="fnc_checklist_agregar_editar(3);">Aceptar</button>
</div>

<input type="hidden" id="hid_accion" />
<input type="hidden" id="hid_accion_agregar_pregunta"/>
<input type="hidden" id="hid_se_modifico" value="N" />
 p metaData Ljava/lang/Object; r s	  t &coldfusion/runtime/AttributeCollection v java/lang/Object x ([Ljava/lang/Object;)V  z
 w { this Lcfchecklist2ecfm834792350; LocalVariableTable Code <clinit> getMetadata ()Ljava/lang/Object; runPage out Ljavax/servlet/jsp/JspWriter; value output0  Lcoldfusion/tagext/io/OutputTag; mode0 I t6 t7 Ljava/lang/Throwable; t8 t9 LineNumberTable java/lang/Throwable � 1       / 0    r s        �   #     *� 
�            } ~    �   �   5     2� 8� :� wY� y� |� u�            } ~    � �  �   "     � u�            } ~    � �  �  r  
   �*� � L*� N*� #+%� *+,� *+.� **� :-� >� @:*5� D� J� NY6� &+*P� RYTSYVS� Z� `� *� c���� h� :� #�� � #:� l� � :� �:	� o�	+q� *�  @ z � � � � � � @ z � � � � � � � � � � � � � �     f 
   � } ~     � � �    � � s    �      � � �    � � �    � � s    � � �    � � �    � � s 	 �     L5 L5 K5 %5              