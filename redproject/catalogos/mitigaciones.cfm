����  -) 
SourceFile GC:\ColdFusion9\wwwroot\ferrominio\redproject\catalogos\mitigaciones.cfm cfmitigaciones2ecfm195434978  coldfusion/runtime/CFPage  <init> ()V  
  	 bindPageVariables D(Lcoldfusion/runtime/VariableScope;Lcoldfusion/runtime/LocalScope;)V   coldfusion/runtime/CfJspPage 
   ID_CATEGORIARIESGO Lcoldfusion/runtime/Variable;  bindPageVariable r(Ljava/lang/String;Lcoldfusion/runtime/VariableScope;Lcoldfusion/runtime/LocalScope;)Lcoldfusion/runtime/Variable;  
    	   DE_CATEGORIARIESGO   	   com.macromedia.SourceModTime  J1 pageContext #Lcoldfusion/runtime/NeoPageContext; ! "	  # getOut ()Ljavax/servlet/jsp/JspWriter; % & javax/servlet/jsp/PageContext (
 ) ' parent Ljavax/servlet/jsp/tagext/Tag; + ,	  - com.adobe.coldfusion.* / bindImportPath (Ljava/lang/String;)V 1 2
  3 
 5 _whitespace %(Ljava/io/Writer;Ljava/lang/String;)V 7 8
  9 .class$coldfusion$tagext$html$ajax$AjaxProxyTag Ljava/lang/Class; (coldfusion.tagext.html.ajax.AjaxProxyTag = forName %(Ljava/lang/String;)Ljava/lang/Class; ? @ java/lang/Class B
 C A ; <	  E _initTag P(Ljava/lang/Class;ILjavax/servlet/jsp/tagext/Tag;)Ljavax/servlet/jsp/tagext/Tag; G H
  I (coldfusion/tagext/html/ajax/AjaxProxyTag K _setCurrentLineNo (I)V M N
  O cfajaxproxy Q cfc S APPLICATION U java/lang/String W RF Y _resolve 9(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/Object; [ \
  ] getPath _ java/lang/Object a app c catalogos_flavio e _invoke K(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object; g h
  i _String &(Ljava/lang/Object;)Ljava/lang/String; k l coldfusion/runtime/Cast n
 o m _validateTagAttrValue \(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String; q r
  s setCfc u 2
 L v jsclassname x jsApp_catalogos_flavio z setJsclassname | 2
 L } 	hasEndTag (Z)V  � coldfusion/tagext/GenericTag �
 � � _emptyTcfTag !(Ljavax/servlet/jsp/tagext/Tag;)Z � �
  �o

<script type="text/javascript">
	//Variables globales.
	var g_row;
	var jasApp  = new jsApp_catalogos_flavio();
	
	//Función para abrir la opción de agregar y/o editar.
	function fnc_mitigaciones_agregar_editar(p_accion)
	{
		if( p_accion == 1 ) //Agregar.
		{
			$("#div_titulo_agregar_editar").html("NUEVO MITIGACI&Oacute;N");
			$("#btn_guardar").attr("title", "Guardar").text("Guardar");
			
			//Nos aseguramos de desbloquear los input.      
			
			$("#ddl_id_CategoriaRiesgo, #txt_de_Riesgo").removeAttr("disabled");
			
			fnc_catalogos_mostrar_ocultar( "div_grid", "div_guardar_editar_eliminar" );
		}
		else if( p_accion == 2 || p_accion == 3 ) //Editar o Eliminar.
		{
			//Validamos que se haya seleccionado un renglón.
			var row = $("#grid_grid_mitigaciones").jqGrid("getGridParam", "selrow");
			
			if( row )
			{	
				row = $("#grid_grid_mitigaciones").jqGrid("getRowData", row);
				g_row = row;
				
				//Llenamos los campos.
				$("#ddl_id_CategoriaRiesgo").val( g_row.id_CategoriaRiesgo );
				$("#txt_de_Riesgo").val( g_row.de_Riesgo );
				
				
				//Valdiar si debemos bloquear o desbloquear los input.
				if( p_accion == 3 )
				{
					$("#div_titulo_agregar_editar").html("ELIMINAR MITIGACI&Oacute;N");
					$("#btn_guardar").attr("title", "Eliminar").text("Eliminar");
					$("#ddl_id_CategoriaRiesgo, #txt_de_Riesgo").attr("disabled","disabled");
				}
				else
				{
					$("#div_titulo_agregar_editar").html("EDITAR MITIGACI&Oacute;N");
					$("#btn_guardar").attr("title", "Guardar").text("Guardar");
					$("#ddl_id_CategoriaRiesgo, #txt_de_Riesgo").removeAttr("disabled");
				}
				
				fnc_catalogos_mostrar_ocultar( "div_grid", "div_guardar_editar_eliminar" );				
			}
			else
				fnc_notificacion("Debe seleccionar una mitigaci&oacute;n");
				
		}
		
		$("#hid_accion").val( p_accion );
		
		//Limpiamos los campos.
		if( p_accion == 1 )
		{
			$("#ddl_id_CategoriaRiesgo option:first").attr("selected", "selected");
			$("#txt_de_Riesgo").val("");			
			
		}
		
		$("#ddl_id_CategoriaRiesgo").focus();
	}
	
	//Función para guardar y/o editar.
	function fnc_mitigaciones_crud()
	{	
		if(fnc_validar_campos_obligatorios()){
		
			fnc_peticion_ajax(function(){
			switch( $("#hid_accion").val() )
			{			
				case "1": //Agregar.														
							return jasApp.Mitigaciones_Agregar( 
								$("#hid_id_Empresa").val()
								,$("#ddl_id_CategoriaRiesgo option:selected").val()
								, $.trim( $("#txt_de_Riesgo").val() )								
								, $("#hid_id_Recurso").val()
								);
							break;
				case "2": //Editar				
							return jasApp.Mitigaciones_Editar( 
								g_row.id_Empresa
								, g_row.id_CategoriaRiesgo
								, g_row.id_Riesgo
								, $.trim( $("#txt_de_Riesgo").val() )								
								, $("#hid_id_Recurso").val()
								);
							break;
				case "3" : //Eliminar
							return jasApp.Mitigaciones_Eliminar (
								g_row.id_Empresa
								, g_row.id_CategoriaRiesgo
								, g_row.id_Riesgo
							);
			}
		}, function(){
			//Actualizamos el grid.
			$("#grid_grid_mitigaciones").trigger("reloadGrid");
										
			fnc_catalogos_mostrar_ocultar( "div_grid", "div_guardar_editar_eliminar" );
		});
		
		}
		
	}
	
	//Función para buscar clientes.
	function fnc_buscar_mitigaciones()
		{		
			//Armamos los parámetros.
			var parametros = "";
				if($("#txt_descripcion_busqueda").val() != "")
					parametros = parametros + "&de_Riesgo=" + $.trim( $("#txt_descripcion_busqueda").val());
				if($("#ddl_id_Categoria_filtro option:selected").val() != "0")
					parametros = parametros + "&id_CategoriaRiesgo=" + $("#ddl_id_Categoria_filtro option:selected").val();
		
			$("#grid_grid_mitigaciones").jqGrid("setGridParam", {
				url: $("#grid_mitigaciones").attr("url") + parametros
				}
			).trigger("reloadGrid");
		}
		
</script>

 � write � 2 java/io/Writer �
 � � &class$coldfusion$tagext$lang$InvokeTag  coldfusion.tagext.lang.InvokeTag � � <	  �  coldfusion/tagext/lang/InvokeTag � getApp � setComponent (Ljava/lang/Object;)V � �
 � � CategoriasRiesgos_Listar � 	setMethod � 2
 � � Categorias_Listado � setReturnVariable � 2
 � � &coldfusion/runtime/AttributeCollection � 
id_empresa � REQUEST � SESSION � 
ID_EMPRESA � _resolveAndAutoscalarize � \
  � ([Ljava/lang/Object;)V  �
 � � setAttributecollection (Ljava/util/Map;)V � �
 � �
	<center>
        <div style="margin-left:5%; margin-right:5%; width:90%">
            <div class="ui-widget-header catalogos_cabecero">CAT&Aacute;LOGO DE MITIGACIONES Y CONTINGENCIAS</div>
            <div class="catalogos_cabecero_contenido" id="div_grid">
            	<div id="div_busqueda" class="ui-state-default catalogos_busqueda">
                	<table style="width: 100%;">
                    	<tr> 
                        	<td style="text-align: right;">Categoria: </td>
                        	<td>
                            	 <select class="input_text" id="ddl_id_Categoria_filtro" onchange="fnc_buscar_riesgos();" style="width:153px">
                                 	<option value="0">Seleccione...</option>
                                     � $class$coldfusion$tagext$io$OutputTag coldfusion.tagext.io.OutputTag � � <	  � coldfusion/tagext/io/OutputTag � cfoutput � query � Categorias_Listado.rs � setQuery � 2 coldfusion/tagext/QueryLoop �
 � � 
doStartTag ()I � �
 � � 9
                                        <option value=" � _autoscalarize 1(Lcoldfusion/runtime/Variable;)Ljava/lang/Object; � �
  � "> � /</option>
                                     � doAfterBody � �
 � � doEndTag � �
 � � doCatch (Ljava/lang/Throwable;)V � �
 � � 	doFinally � 
 � �3
                                </select>
                            </td>                       	
                            <td style="text-align: right;">Riesgo: </td>
                            <td><input type="text" id="txt_descripcion_busqueda" class="input_text" onChange="fnc_buscar_riesgos();" /></td>
                        </tr>
                    </table>
                </div>
                
                <div id="grid_mitigaciones" class="ggrids" url="application/catalogos_flavio.cfc?method=Mitigaciones_Listar_JSON&id_Empresa= ��" paginado="15" titulo="Listado de Riesgos"
                    sortname="de_CategoriaRiesgo" ancho="450" autowidth="false" loadComplete="fnc_autoajustar_ancho_busqueda('grid_mitigaciones','div_busqueda')">
                    
                    <columna nom_columna="de_Mitigacion">Mitigaci&oacute;n</columna>
                    <columna nom_columna="de_Contingencia">Contingencia</columna>
                    
                    
                    <columna nom_columna="id_Empresa" visible="false"></columna>                    
                    <columna nom_columna="id_CategoriaRiesgo" visible="false"></columna>
                    <columna nom_columna="id_Riesgo" visible="false"></columna>
                    <columna nom_columna="id_Mitigacion" visible="false"></columna>

                    <accion accion="agregar" evento="fnc_mitigaciones_agregar_editar(1);"></accion>
                    <accion accion="editar" evento="fnc_mitigaciones_agregar_editar(2);"></accion>
                    <accion accion="eliminar" evento="fnc_mitigaciones_agregar_editar(3);"></accion>
                </div>
            </div>
            
            
            <div id="div_guardar_editar_eliminar" class="catalogos_guardar_editar">
            	<div class="ui-widget-header catalogos_guardar_editar_cabecero" id="div_titulo_agregar_editar"></div>
                <div class="catalogos_guardar_editar_cabecero_contenido">
                    <table id="table_clientes_guardar_editar">                      
                    	<tr>
                        	<td style="text-align:right"><span>Categoria:</span></td>
	                        <td>
                            	 <select class="input_text obligatorio" acercade="Categoria" id="ddl_id_CategoriaRiesgo" style="width:153px">
                                 	<option value="0">Seleccione...</option>
                                     �
                                </select>
                            </td>
                        </tr>
                        <tr>
                        	<td style="text-align:right"><span>Riesgo:</span></td>
                            <td><input type="text" class="input_text obligatorio" id="txt_de_Riesgo" acercade="Riesgo" style="width:224px" maxlength="200"  /></td>
                        </tr>                        
                    </table>                    
                    <div style="margin-top: 10px;">
                    	<button class="boton" title="Guardar" id="btn_guardar" onClick="fnc_mitigaciones_crud();">Guardar</button>
                        <button class="boton" title="Regresar" onClick="fnc_catalogos_mostrar_ocultar( 'div_grid', 'div_guardar_editar_eliminar' );">Regresar</button>
                    </div>
                </div>
            </div>
        </div>
    </center>
    <input type="hidden" id="hid_accion" />
    <input type="hidden" id="hid_id_Recurso" value=" � 
ID_RECURSO � " />

 � metaData Ljava/lang/Object; � �	  � this Lcfmitigaciones2ecfm195434978; LocalVariableTable Code <clinit> varscope "Lcoldfusion/runtime/VariableScope; locscope Lcoldfusion/runtime/LocalScope; getMetadata ()Ljava/lang/Object; runPage out Ljavax/servlet/jsp/JspWriter; value 
ajaxproxy0 *Lcoldfusion/tagext/html/ajax/AjaxProxyTag; invoke1 "Lcoldfusion/tagext/lang/InvokeTag; output2  Lcoldfusion/tagext/io/OutputTag; mode2 I t8 t9 Ljava/lang/Throwable; t10 t11 output3 mode3 t14 t15 t16 t17 output4 mode4 t20 t21 t22 t23 output5 mode5 t26 t27 t28 t29 LineNumberTable java/lang/Throwable' 1                 ; <    � <    � <    � �        �   #     *� 
�    �        � �    �   �   E     '>� D� F�� D� ��� D� �� �Y� b� �� ��    �       ' � �       �   Q     *+,� **+,� � **+,� � �    �         � �      � �     �     �   "     � ��    �        � �     �  #    *� $� *L*� .N*0� 4*+6� :*� F-� J� L:*� PRT*� P**V� XYZS� ^`� bYdSYfS� j� p� t� wRy{� t� ~� �� �� �+�� �*� �-� J� �:* �� P* �� P**V� XYZS� ^�� bYfS� j� ��� ��� �� �Y� bY�SY*�� XY�SY�S� �S� �� �� �� �� �+�� �*� �-� J� �:* �� P���� t� �� �� �Y6� ;+Զ �+**� � ظ p� �+ڶ �+**� � ظ p� �+ܶ �� ߚ��� �� :� #�� � #:		� � � :
� 
�:� �+� �*� �-� J� �:* �� P� �� �Y6� &+*�� XY�SY�S� �� p� �� ߚ��� �� :� #�� � #:� � � :� �:� �+�� �*� �-� J� �:* �� P���� t� �� �� �Y6� ;+Զ �+**� � ظ p� �+ڶ �+**� � ظ p� �+ܶ �� ߚ��� �� :� #�� � #:� � � :� �:� �+� �*� �-� J� �:* Ͷ P� �� �Y6� &+*�� XY�SY�S� �� p� �� ߚ��� �� :� #�� � #:� � � :� �:� �+� �� C��(���(C��(���(���(���(�%("%(�4("4(%14(494(u��(���(u��(���(���(���(KW(QTW(Kf(QTf(Wcf(fkf(  �  .    � �        �    + ,      	
          �    	   
   �          �          �          �          �       !   " �   #   $   % � &   � ' P  U  8  8  k    � � � � � � � � � � � � � � � �4 �U �U �T �j �j �i � �� �� �� �� �f �� �� �� �� �� �� �K � � � �� �              