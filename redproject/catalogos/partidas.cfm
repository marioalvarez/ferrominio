����  - � 
SourceFile CC:\ColdFusion9\wwwroot\ferrominio\redproject\catalogos\partidas.cfm cfpartidas2ecfm2021209216  coldfusion/runtime/CFPage  <init> ()V  
  	 bindPageVariables D(Lcoldfusion/runtime/VariableScope;Lcoldfusion/runtime/LocalScope;)V   coldfusion/runtime/CfJspPage 
   LOCAL Lcoldfusion/runtime/Variable;  bindPageVariable r(Ljava/lang/String;Lcoldfusion/runtime/VariableScope;Lcoldfusion/runtime/LocalScope;)Lcoldfusion/runtime/Variable;  
    	   com.macromedia.SourceModTime  J1 pageContext #Lcoldfusion/runtime/NeoPageContext;  	   getOut ()Ljavax/servlet/jsp/JspWriter; ! " javax/servlet/jsp/PageContext $
 % # parent Ljavax/servlet/jsp/tagext/Tag; ' (	  ) com.adobe.coldfusion.* + bindImportPath (Ljava/lang/String;)V - .
  / 

 1 _whitespace %(Ljava/io/Writer;Ljava/lang/String;)V 3 4
  5 	PLANTILLA 7 LOCAL.PLANTILLA 9  isDefinedCanonicalVariableAndKey D(Lcoldfusion/runtime/Variable;Ljava/lang/String;Ljava/lang/String;)Z ; <
  = _Object (Z)Ljava/lang/Object; ? @ coldfusion/runtime/Cast B
 C A _boolean (Ljava/lang/Object;)Z E F
 C G java/lang/String I _resolveAndAutoscalarize D(Lcoldfusion/runtime/Variable;[Ljava/lang/String;)Ljava/lang/Object; K L
  M _compare (Ljava/lang/Object;D)D O P
  Q 
	 S TRUE U _structSetAt E(Lcoldfusion/runtime/Variable;[Ljava/lang/Object;Ljava/lang/Object;)V W X
  Y 
 [ FALSE ]

<script type="text/javascript">
	//Variables globales.
	var g_row;
	
	//FunciÃ³n para abrir la opciÃ³n de agregar, editar o eliminar.
	function fnc_Partidas_agregar_editar(p_accion)
	{
		if( p_accion == 1 ) //Agregar.
		{
			$("#div_titulo_agregar_editar_partida").html("NUEVA PARTIDA");
			$("#btn_guardar_partidas").attr("title", "Guardar").text("Guardar");
			
			//Nos aseguramos de desbloquear los input.
			$("#txt_nb_Partida").removeAttr("disabled");
			
			fnc_catalogos_mostrar_ocultar( "div_grid_partida", "div_guardar_editar_eliminar_partida" );
		}
		else if( p_accion == 2 || p_accion == 3 ) //Editar o Eliminar.
		{
			//Validamos que se haya seleccionado un renglÃ³n.
			var row = ggrids.obtener_renglon("grid_Partidas");
			 _ write a . java/io/Writer c
 d b �
				if(row.length > 1)
				{
					fnc_notificacion("Debe seleccionar s&oacute;lo una Partida.");
					return;
				}
				else
					row = row[0];
			 f
�

			if( row )
			{	
				g_row = row;

				//Llenamos los campos.
				$("#txt_nb_Partida").val( row.nb_Partida);
				
				//Valdiar si debemos bloquear o desbloquear los input.
				if( p_accion == 3 )
				{
					$("#div_titulo_agregar_editar_partida").html("ELIMINAR PARTIDA");
					$("#btn_guardar_partidas").attr("title", "Eliminar").text("Eliminar");
					$("#txt_nb_Partida").attr("disabled", "disabled");
				}
				else
				{
					$("#div_titulo_agregar_editar_partida").html("EDITAR PARTIDA");
					$("#btn_guardar_partidas").attr("title", "Guardar").text("Guardar");
					$("#txt_nb_Partida").removeAttr("disabled");
				}
				
				fnc_catalogos_mostrar_ocultar( "div_grid_partida", "div_guardar_editar_eliminar_partida" );
			}
			else
				fnc_notificacion("Debe seleccionar una Partida.");
		}
		
		$("#hid_accion_partida").val( p_accion );
		
		//Limpiamos los campos.
		if( p_accion == 1 )
		{
			$("#txt_nb_Partida").val("");
		}
		
		$("#txt_nb_Partida").focus();
	}
	
	//FunciÃ³n para guardar, editar o eliminar.
	function fnc_Estados_crud()
	{
		//Validamos los campos obligaorios.
		if( fnc_validar_campos_obligatorios("div_Estados_crud") )
		{
			//Armamos los parÃ¡metros.
			var parametros = "id_Empresa=" + $("#hid_id_Empresa").val() + "&method=";
			
			switch( $("#hid_accion_partida").val() )
			{
				case "1": //Agregar.
							parametros += "Partidas_Agregar&nb_Partida=" + fnc_codificar( $.trim( $("#txt_nb_Partida").val() ) );
							break;
				case "2": //Editar
							parametros += "Partidas_Editar&id_Partida=" + g_row.id_Partida + "&nb_Partida=" + g_row.nb_Partida 
								+ "&nb_PartidaNuevo=" + fnc_codificar( $.trim( $("#txt_nb_Partida").val() ) );
							break;
				case "3": //Eliminar
							parametros += "Partidas_Eliminar&id_Partida=" + g_row.id_Partida;
							break;
			}
			
			//Guardamos, Editamos, Eliminamos.
			fnc_peticionAjax({
				url: "./application/catalogos_Victor.cfc",
				parametros: parametros,
				f_callback: function(){
					//Recargamos el grid.
					ggrids.recargar("grid_Partidas");
					
					if( $("#hid_accion_partida").val() == "2" || $("#hid_accion_partida").val() == "3" )
						fnc_catalogos_mostrar_ocultar( "div_grid_partida", "div_guardar_editar_eliminar_partida" );
					else
					{
						$("#txt_nb_Partida").val("");
					}
				}
			});
		}
	}
	
	//FunciÃ³n para buscar Estados.
	function fnc_buscar_Estados()
	{
		//Armamos los parÃ¡metros.
		var parametros = ( $.trim( $("#txt_nb_PartidaBusqueda").val() ) == "" ? "" : "&nb_Partida=" + fnc_codificar( $.trim( $("#txt_nb_PartidaBusqueda").val() ) ) );
		
		//Recargamos el grid.
		ggrids.recargar("grid_Partidas", parametros);
	}
</script>

<center>
    <div  h style="width: 900px;" j >
    	 l u
        	<div class="ui-widget-header catalogos_cabecero titulo_opcion">CAT&Aacute;LOGO DE PARTIDAS</div>
         n�
        <div class="catalogos_cabecero_contenido" id="div_grid_partida">
            <div class="ui-state-default catalogos_busqueda" id="div_busqueda_partida">
                <table style="width: 100%;">
                    <tr>
                        <td style="text-align: right;">Partida: </td>
                        <td><input type="text" id="txt_nb_PartidaBusqueda" class="input_text" onChange="fnc_buscar_Estados();" style="width: 200px;" /></td>
                    </tr>
                </table>
            </div>
            <div id="grid_Partidas" class="ggrids" paginado="10" titulo="Listado de Partidas"
                url="application/catalogos_Victor.cfc?method=Partidas_Listar_JSON&id_Empresa= p $class$coldfusion$tagext$io$OutputTag Ljava/lang/Class; coldfusion.tagext.io.OutputTag t forName %(Ljava/lang/String;)Ljava/lang/Class; v w java/lang/Class y
 z x r s	  | _initTag P(Ljava/lang/Class;ILjavax/servlet/jsp/tagext/Tag;)Ljavax/servlet/jsp/tagext/Tag; ~ 
  � coldfusion/tagext/io/OutputTag � _setCurrentLineNo (I)V � �
  � 	hasEndTag (Z)V � � coldfusion/tagext/GenericTag �
 � � 
doStartTag ()I � �
 � � REQUEST � SESSION � 
ID_EMPRESA � 9(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/Object; K �
  � _String &(Ljava/lang/Object;)Ljava/lang/String; � �
 C � doAfterBody � �
 � � doEndTag � � coldfusion/tagext/QueryLoop �
 � � doCatch (Ljava/lang/Throwable;)V � �
 � � 	doFinally � 
 � � " 
                 � ancho="700" � Qancho="100%" multiselect="true" autocrear="false" onSelectRow="fnc_accion_grid()" ��
                sortname="nb_Partida" sortorder="ASC"
                loadComplete="fnc_autoajustar_ancho_busqueda('grid_Partidas','div_busqueda_partida')">
                
                
                <columna nom_columna="nb_Partida">Partida</columna>
                
                
                <columna nom_columna="id_Empresa" visible="false"></columna>
                <columna nom_columna="id_Partida" visible="false"></columna>
                
                
                <accion accion="agregar" evento="fnc_Partidas_agregar_editar(1);"></accion>
                <accion accion="editar" evento="fnc_Partidas_agregar_editar(2);"></accion>
                <accion accion="eliminar" evento="fnc_Partidas_agregar_editar(3);"></accion>
            </div>
        </div>
        
        
        <div id="div_guardar_editar_eliminar_partida" class="catalogos_guardar_editar">
            <div class="ui-widget-header catalogos_guardar_editar_cabecero" id="div_titulo_agregar_editar_partida"></div>
            <div class="catalogos_guardar_editar_cabecero_contenido" id="div_Estados_crud">
                 <table style="width: 90%;" align="center">
                    <tr>
                        <td style="text-align: right;">Partida:&nbsp;</td>
                        <td><input class="input_text obligatorio" acercade="Partida" 
                        		type="text" id="txt_nb_Partida" 
                                maxlength="120" style="width: 90%;" 
                                onkeyup="fnc_enter_presionado( event, 'fnc_Estados_crud()' )"/></td>
                    </tr> 
                </table>
                
                <div style="margin-top: 10px;">
                    <button class="boton" title="Guardar" id="btn_guardar_partidas" onClick="fnc_Estados_crud();">Guardar</button>
                    <button class="boton" title="Regresar" onClick="fnc_catalogos_mostrar_ocultar( 'div_grid_partida', 'div_guardar_editar_eliminar_partida' );">Regresar</button>
                </div>
            </div>
        </div>
    </div>
</center>

<input type="hidden" id="hid_accion_partida" />
 � metaData Ljava/lang/Object; � �	  � &coldfusion/runtime/AttributeCollection � java/lang/Object � ([Ljava/lang/Object;)V  �
 � � this Lcfpartidas2ecfm2021209216; LocalVariableTable Code <clinit> varscope "Lcoldfusion/runtime/VariableScope; locscope Lcoldfusion/runtime/LocalScope; getMetadata ()Ljava/lang/Object; runPage out Ljavax/servlet/jsp/JspWriter; value output0  Lcoldfusion/tagext/io/OutputTag; mode0 I t6 t7 Ljava/lang/Throwable; t8 t9 LineNumberTable java/lang/Throwable � 1            r s    � �        �   #     *� 
�    �        � �    �   �   5     u� {� }� �Y� �� �� ��    �        � �       �   E     *+,� **+,� � �    �         � �      � �     � �   � �  �   "     � ��    �        � �    � �  �    
  �*�  � &L*� *N*,� 0*+2� 6**� 8:� >� DY� H� "W**� � JY8S� N� R�~�� D� H� '*+T� 6**� � JY8SV� Z*+\� 6� $*+T� 6**� � JY8S^� Z*+\� 6+`� e**� � JY8S� N� R�� 	+g� e+i� e**� � JY8S� N� R�� 	+k� e+m� e**� � JY8S� N� R�� 	+o� e+q� e*� }-� �� �:* �� �� �� �Y6� &+*�� JY�SY�S� �� �� e� ����� �� :� #�� � #:� �� � :� �:	� ��	+�� e**� � JY8S� N� R�� +�� e� 	+�� e+�� e� -gs �mps �-g� �mp� �s� ���� �  �   f 
  � � �    � � �   � � �   � ' (   � � �   � � �   � � �   � � �   � � �   � � � 	 �   � '       !      1  B  1  1    j  j  \  \  �  �  �  �  y    �  �  �  � � � � � � � � � � � �9 �9 �8 � �� �� �� �� �              