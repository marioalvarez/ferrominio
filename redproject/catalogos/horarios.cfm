����  -c 
SourceFile CC:\ColdFusion9\wwwroot\ferrominio\redproject\catalogos\horarios.cfm cfhorarios2ecfm886097838  coldfusion/runtime/CFPage  <init> ()V  
  	 bindPageVariables D(Lcoldfusion/runtime/VariableScope;Lcoldfusion/runtime/LocalScope;)V   coldfusion/runtime/CfJspPage 
   I Lcoldfusion/runtime/Variable;  bindPageVariable r(Ljava/lang/String;Lcoldfusion/runtime/VariableScope;Lcoldfusion/runtime/LocalScope;)Lcoldfusion/runtime/Variable;  
    	   com.macromedia.SourceModTime  J1 pageContext #Lcoldfusion/runtime/NeoPageContext;  	   getOut ()Ljavax/servlet/jsp/JspWriter; ! " javax/servlet/jsp/PageContext $
 % # parent Ljavax/servlet/jsp/tagext/Tag; ' (	  ) com.adobe.coldfusion.* + bindImportPath (Ljava/lang/String;)V - .
  / 
 1 _whitespace %(Ljava/io/Writer;Ljava/lang/String;)V 3 4
  5 .class$coldfusion$tagext$html$ajax$AjaxProxyTag Ljava/lang/Class; (coldfusion.tagext.html.ajax.AjaxProxyTag 9 forName %(Ljava/lang/String;)Ljava/lang/Class; ; < java/lang/Class >
 ? = 7 8	  A _initTag P(Ljava/lang/Class;ILjavax/servlet/jsp/tagext/Tag;)Ljavax/servlet/jsp/tagext/Tag; C D
  E (coldfusion/tagext/html/ajax/AjaxProxyTag G _setCurrentLineNo (I)V I J
  K cfajaxproxy M cfc O APPLICATION Q java/lang/String S RF U _resolve 9(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/Object; W X
  Y getPath [ java/lang/Object ] app _ catalogos_flavio a _invoke K(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object; c d
  e _String &(Ljava/lang/Object;)Ljava/lang/String; g h coldfusion/runtime/Cast j
 k i _validateTagAttrValue \(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String; m n
  o setCfc q .
 H r jsclassname t jsApp_catalogos_flavio v setJsclassname x .
 H y 	hasEndTag (Z)V { | coldfusion/tagext/GenericTag ~
  } _emptyTcfTag !(Ljavax/servlet/jsp/tagext/Tag;)Z � �
  ��

<script type="text/javascript">
	//Variables globales.
	var g_row_horarios;
	var g_row_horariosdetalles;
	var g_row_mitigaciones;	
	var jasApp  = new jsApp_catalogos_flavio();
	



	//Función para abrir la opción de agregar y/o editar.
	function fnc_horarios_agregar_editar(p_accion)
	{
		$("#div_horariosdetalles").hide();
		$("#fieldsetdetalle").hide();
		$("#div_guardar_editar_eliminar_horariosdetalles").hide();
		
		if( p_accion == 1 ) //Agregar.
		{
			$("#div_titulo_agregar_editar").html("NUEVO HORARIO");
			$("#btn_guardar").attr("title", "Guardar").text("Guardar");
			
			//Nos aseguramos de desbloquear los input.      
			$("#txt_de_Horario").removeAttr("disabled");
			
			fnc_catalogos_mostrar_ocultar( "div_grid", "div_guardar_editar_eliminar" );

			//Se oculta el catalogo.
			//$("#div_horariosdetalles").slideUp(200);
			//$("#fieldsetdetalle").slideUp(200);
		}
		else if( p_accion == 2 || p_accion == 3 ) //Editar o Eliminar.
		{
			//Validamos que se haya seleccionado un renglón.
			var row = ggrids.obtener_renglon("grid_horarios")
			
			if( row )
			{	
				g_row_horarios = row;
				
				//Llenamos los campos.
				$("#txt_de_Horario").val( g_row_horarios.de_Horario );
				
				//Valdiar si debemos bloquear o desbloquear los input.
				if( p_accion == 3 )
				{
					$("#div_titulo_agregar_editar").html("ELIMINAR HORARIO");
					$("#btn_guardar").attr("title", "Eliminar").text("Eliminar");
					$("#txt_de_Horario").attr("disabled","disabled");					
				}
				else
				{
					$("#div_titulo_agregar_editar").html("EDITAR HORARIO");
					$("#btn_guardar").attr("title", "Guardar").text("Guardar");
					$("#txt_de_Horario").removeAttr("disabled");
					
				}
				
				fnc_catalogos_mostrar_ocultar( "div_grid", "div_guardar_editar_eliminar" );				
				$("#div_horariosdetalles").slideUp(200);
				$("#fieldsetdetalle").hide();
			}
			else
				fnc_notificacion("Debe seleccionar un horario");
				
		}
		
		$("#hid_accion").val( p_accion );
		
		//Limpiamos los campos y pasamos el foco al control correspondiente
		if( p_accion == 1 )
		{
			$("#txt_de_Horario").val("");
			$("#txt_de_Horario").focus();
		}
		if( p_accion == 2 )			
			$("#txt_de_Horario").focus();
			
		if( p_accion == 3 )
			$("#btn_guardar").focus();		
		
	}
	
	//Función para guardar y/o editar.
	function fnc_horarios_crud()
	{	
		//Validamos los campos obligatorios.
		if(fnc_validar_campos_obligatorios('div_guardar_editar_eliminar'))
		{	
			//Armamos los parámetros.
			var accion = new Array("Horarios_Agregar","Horarios_Editar","Horarios_Eliminar");
			var parametros = "id_Empresa=" + $("#hid_id_Empresa").val() + "&method="+ accion[$("#hid_accion").val()-1];
			
			// VALORES DE LOS CAMPOS DEL DIV PARA AGREGAR EDITAR ELIMINAR
			var hor = fnc_codificar(  $.trim( $("#txt_de_Horario").val())  );
			
				switch( $("#hid_accion").val() )
				{			
					case "1": //Agregar.		
								parametros+=
								"&de_Horario="+ hor;												
								
								break;
					case "2": //Editar	
								parametros+=
								"&id_Horario="+ g_row_horarios.id_Horario+
								"&de_Horario="+ hor; 		
								
								break;
					case "3" : //Eliminar
								parametros+=
								"&id_Horario="+ g_row_horarios.id_Horario;
								
				}
				
				fnc_peticionAjax(
				{
					url: "./application/catalogos_flavio.cfc",
					parametros: parametros,
					f_callback: function()
					{	
						//Actualizamos el grid.
						ggrids.recargar("grid_horarios")
						
						//Se verifica si es guardar o editar/eliminar
						if($("#hid_accion").val() == "1")
						{
							$("#txt_de_Horario").val("");
							$("#txt_de_Horario").focus();
						}
						else
						{
							fnc_catalogos_mostrar_ocultar( "div_grid", "div_guardar_editar_eliminar" );
						}		
					}						
			});
		}
	}
	
	//Función para buscar horarios.
	function fnc_buscar_horarios()
		{		
			//Armamos los parámetros.
			var parametros = "";				
				if( $.trim( $("#txt_de_Horario_busqueda").val() ) != "" )
					parametros = parametros + "&de_Horario=" + fnc_codificar( $.trim( $("#txt_de_Horario_busqueda").val() ) );
		
			ggrids.recargar("grid_horarios", parametros);
		}
		
		
		/*
			Funciones para catalogo de entradas y salidas
		*/
		
		
		//Función para abrir la opción de agregar y/o editar.
	function fnc_horariosdetalles_agregar_editar(p_accion)
	{	
		if( p_accion == 1 ) //Agregar.
		{			
			$("#div_titulo_agregar_editar_horariosdetalles").html("AGREGAR ENTRADA SALIDA");
			$("#btn_guardar_horariosdetalles").attr("title", "Guardar").text("Guardar");
			
			//Nos aseguramos de desbloquear los input.      
			
			$("#ddl_hora_entrada, #ddl_minuto_entrada, #ddl_hora_salida, #ddl_minuto_salida").removeAttr("disabled");
			$("input:checkbox[id^='id_Dia_']").removeAttr("disabled");
			$("input:checkbox[id^='id_Dia_']").removeAttr("checked");
			$("#ddl_hora_entrada").val(0);
			$("#ddl_minuto_entrada").val(0);
			$("#ddl_hora_salida").val(0);
			$("#ddl_minuto_salida").val(0);		
			fnc_catalogos_mostrar_ocultar( "div_horariosdetalles", "div_guardar_editar_eliminar_horariosdetalles" );			
			
		}
		else if( p_accion == 2 || p_accion == 3 ) //Editar o Eliminar.
		{
			
			//Validamos que se haya seleccionado un renglón.
			var row = ggrids.obtener_renglon("grid_horariosdetalle");
			
			if( row )
			{	
				g_row_horariosdetalles = row;
				//Llenamos los campos.
				$(".chk_dias").removeAttr("checked");

				/*Se verifican los dìas de acuerdo a su id, no a su nu_dia*/
				$(".chk_dias[nu_Dia='" + (g_row_horariosdetalles.id_Dia) + "']")[0].checked = true;				
				$("#ddl_hora_entrada").val( g_row_horariosdetalles.hr_HoraEntrada );
				$("#ddl_minuto_entrada").val( g_row_horariosdetalles.hr_MinutoEntrada );
				$("#ddl_hora_salida").val( g_row_horariosdetalles.hr_HoraSalida );
				$("#ddl_minuto_salida").val( g_row_horariosdetalles.hr_MinutoSalida );
				
				//Valdiar si debemos bloquear o desbloquear los input.
				if( p_accion == 3 )
				{
					$("#div_titulo_agregar_editar_horariosdetalles").html("ELIMINAR ENTRADA SALIDA");
					$("#btn_guardar_horariosdetalles").attr("title", "Eliminar").text("Eliminar");
				
					$("#ddl_hora_entrada, #ddl_minuto_entrada, #ddl_hora_salida, #ddl_minuto_salida").attr("disabled","disabled");
					$("input:checkbox[id^='id_Dia_']").attr("disabled","disabled");
				}
				else
				{
					$("#div_titulo_agregar_editar_horariosdetalles").html("EDITAR ENTRADA SALIDA");
					$("#btn_guardar_horariosdetalles").attr("title", "Guardar").text("Guardar");
					
					$("#ddl_hora_entrada, #ddl_minuto_entrada, #ddl_hora_salida, #ddl_minuto_salida").removeAttr("disabled");
					$("input:checkbox[id^='id_Dia_']").attr("disabled","disabled");
				}
				
				fnc_catalogos_mostrar_ocultar( "div_horariosdetalles", "div_guardar_editar_eliminar_horariosdetalles" );				
				$("#div_horariosdetalles").slideUp(200);
				$("#fieldsetmitigaciones").hide();
			}
			else
				fnc_notificacion("Debe seleccionar un rengl&oacute;n de entrada salida.");
				
		}
		$("#ddl_hora_entrada").val(8);
		$("#ddl_hora_salida").val(18);
		$("#hid_accion").val( p_accion );
		if( p_accion == 1 || p_accion == 2)
		{			
			$("#ddl_hora_entrada").focus();
		}
		
	}
	
	//Función para guardar y/o editar.
	function fnc_horariosdetalles_crud()
	{	
		if(fnc_validar_campos_obligatorios('div_guardar_editar_eliminar_horariosdetalles'))
		{  
			var accion = new Array("HorariosDetalle_Agregar","HorariosDetalle_Editar","HorariosDetalle_Eliminar");
			var action = $("#hid_accion").val();
			var parametros = "id_Empresa=" + $("#hid_id_Empresa").val() + "&method="+ accion[$("#hid_accion").val()-1];
			
			validarHorario(action,function()
			{
							switch( $("#hid_accion").val() )
							{			
								case "1": //Agregar
											parametros +=
											"&id_Horario="+ g_row_horarios.id_Horario+
											"&dias="+ $.trim( $("#div_dias_enviar").html())+
											"&hr_Entrada="+ $("# � write � . java/io/Writer �
 � ��hr_Entrada").val()+
											"&hr_Salida="+ $("#hr_Salida").val();
		
										
											break;
								case "2": //Editar	
											parametros +=
											"&id_Horario="+ g_row_horarios.id_Horario+
											"&id_Dia="+ g_row_horariosdetalles.id_Dia+	
											"&id_HorarioEntradaSalida="+ g_row_horariosdetalles.id_HorarioEntradaSalida+
											"&hr_Entrada="+ $("#hr_Entrada").val()+
											"&hr_Salida="+ $("#hr_Salida").val();		
											
											break;
								case "3" : //Eliminar
											parametros +=
											"&id_Horario="+ g_row_horarios.id_Horario+
											"&id_Dia="+ g_row_horariosdetalles.id_Dia+	
											"&id_HorarioEntradaSalida="+ g_row_horariosdetalles.id_HorarioEntradaSalida;
											
											break;
								default : 
											return false;
							}
						
						
							fnc_peticionAjax(
							{
								url: "./application/catalogos_flavio.cfc",
								parametros: parametros,
								f_callback: function()
								{	
									//Actualizamos el grid.
									ggrids.recargar("grid_horariosdetalle");
									
									if($("#hid_accion").val() == "2" || $("#hid_accion").val() == "3")
									{
									//fnc_catalogos_mostrar_ocultar( "div_horariosdetalles", "div_guardar_editar_eliminar_horariosdetalles" );
										fnc_catalogos_mostrar_ocultar( "div_horariosdetalles", "div_guardar_editar_eliminar_horariosdetalles" );	
									}
									else
									{
										//Limpiamos la seleccion de los combos.
										$("#ddl_hora_entrada option:first").attr('selected', 'selected');
										//$("#ddl_hora_entrada").val($("#ddl_hora_entrada option:first").val());
										$("#ddl_minuto_entrada").val($("#ddl_minuto_entrada option:first").val());
										$("#ddl_hora_salida").val($("#ddl_hora_salida option:first").val());
										$("#ddl_minuto_salida").val($("#ddl_minuto_salida option:first").val());
										$("input:checkbox[id^='id_Dia_']").removeAttr("checked");
									
									}	
								}
						 });
				});		
			}
	}
	
function validarHorario(accion,p_funcion)
	{
		//alert(accion);
		if(accion == 1 || accion == 2)
		{	
			var hora_Salida = parseInt($("#ddl_hora_salida option:selected").val());
			var hora_Entrada = parseInt($("#ddl_hora_entrada option:selected").val());
			var minuto_Entrada = parseInt($("#ddl_minuto_entrada option:selected").val());
			var minuto_Salida = parseInt($("#ddl_minuto_salida option:selected").val());
	
	
			//Validacion de que se haya seleccionado al menos un dia laboral.
			if($("input:checkbox[id^='id_Dia_']:checked").size() == 0)
			{
				fnc_notificacion("Debe seleccionar al menos un d&iacute;a de la semana.");
				return false;
			}
			
			//Validaciones de horas.
			
			if( hora_Salida < hora_Entrada )
			{		
				fnc_notificacion("La hora de salida debe ser mayor a la hora de entrada.");	
				return false;
			}
			else if (hora_Salida == hora_Entrada)
			{			
				if(minuto_Salida <= minuto_Entrada)
				{
					fnc_notificacion("La hora de salida debe ser mayor a la hora de entrada.");
					return false;
					
				}
				else
				{
					//alert('todo bien');
					$("#hr_Entrada").val( hora_Entrada +":"+ minuto_Entrada);
					$("#hr_Salida").val( hora_Salida +":"+ minuto_Salida);
				}
			}
			else if( hora_Salida > hora_Entrada )
			{
				//alert('todo bien');
				$("#hr_Entrada").val( hora_Entrada +":"+ minuto_Entrada);
				$("#hr_Salida").val( hora_Salida +":"+ minuto_Salida);
			}
			
			if( accion == 1)
			{
				var id_Empresa = $("#hid_id_Empresa").val();
				var id_Horario = g_row_horarios.id_Horario;
				var hr_Entrada = hora_Entrada + ":" + minuto_Entrada;
				var hr_Salida = hora_Salida + ":" + minuto_Salida;
				var xml = "";
				//var dias = "";
				var diasEnviar = "";
				//Verificar que el dia laboral y sus tiempos de entrada y salida, no se traslapen con otro horario guardado antes.		
				var diasXML = "";
				$("input:checkbox[id^='id_Dia_']:checked").each(function(){
					//Armamos un XML para enviar los dias.
					diasXML += 	"<dia>" +
									"<nu_Dia>" + $(this).attr('nu_Dia') + "</nu_Dia>" +												
								"</dia>";
				});
				diasXML = "<root>" + diasXML + "</root>";
				//var flag = true;
				fnc_peticionAjax(
				{
					url: "./application/catalogos_flavio.cfc",
					parametros: "method=HorariosDetalle_Buscar&id_Empresa=" + id_Empresa + "&id_Horario=" + id_Horario
						+ "&diasXML=" + diasXML + "&hr_Entrada=" + hr_Entrada + "&hr_Salida=" + hr_Salida,
					div_success:"div_dias_xml",
					f_callback: function()
						{
							xml = $.parseXML( $.trim( $("#div_dias_xml").html() ) );
						
							$(xml).find("dia").each(function(){
	
								var dia = $(this).find("nu_dia").text();
								var existe = $(this).find("existe").text();
								if(existe == 'Si')
								{
									var de_Dia = $("#id_Dia_"+dia).attr("de_Dia");
									fnc_mostrar_aviso( "gd_avisos", "El d&iacute;a "+ de_Dia + " ya cuenta con este horario.", 5 );
									
									diasEnviar = "";
									//alert(de_Dia);
									return false;																				
								}
								else
								{
									diasEnviar = diasEnviar + 
															"<dia>" 
																+ "<nu_dia>" + dia + "</nu_dia>" +
															"</dia>";
								}												
							});
							
							if( diasEnviar != "" )
							{
								diasEnviar = "<root>" + diasEnviar + "</root>";
								$("#div_dias_enviar").html( diasEnviar );
								p_funcion();	
																																		
							}	
						},
					automensaje: false
				});		
			}
			else
			{				
				p_funcion();
			}		
		}
		else
		{
			p_funcion();
		}
		

	}
	
	//Función para buscar clientes.
	function fnc_buscar_riesgos()
		{	
			var row = $("#grid_grid_categoriasriesgos").jqGrid("getGridParam", "selrow");
			var url = "application/catalogos_flavio.cfc?method=Riesgos_Listar_JSON&id_Empresa="+ $("#hid_id_Empresa").val();	
			//Armamos los parámetros.
			var parametros = "";
				if($("#txt_descripcion_busqueda_riesgo").val() != "")
					parametros = parametros + "&de_Riesgo=" + $.trim( $("#txt_descripcion_busqueda_riesgo").val());					
					
				if( row )
				{
					row = $("#grid_grid_categoriasriesgos").jqGrid("getRowData", row);
					g_row_categorias = row;
					
					parametros = parametros + "&id_CategoriaRiesgo=" + g_row_categorias.id_CategoriaRiesgo;
					//alert(g_row_categorias.id_CategoriaRiesgo);
				}
					
		
			$("#grid_grid_riesgos").jqGrid("setGridParam", {
				url: url + parametros
				}
			).trigger("reloadGrid");
		}
			
	function fnc_ver_horariodetalle()
	{
		//Validamos que se haya seleccionado un renglón.
		var row = ggrids.obtener_renglon("grid_horarios");	

		if( row )
		{
			g_row_horarios = row;

			var url = "application/catalogos_flavio.cfc?method=HorariosDetalle_ListarJSON&id_Empresa="+ $("#hid_id_Empresa").val();
			var parametros = "&id_Horario=" + g_row_horarios.id_Horario;
			
			$("#grid_horariosdetalle").attr("titulo", "Detalle de Horarios");
			$("#grid_horariosdetalle").attr('url', url + parametros);
									
			$("#div_horariosdetalles").slideDown(200);
			ggrids.crear("grid_horariosdetalle");
			$("#fieldsetdetalle").show(200);			
			//Despues de crear el grid de riesgos se redimensiona el grid de busqueda
			$("#div_guardar_editar_eliminar_horariosdetalles").slideUp(200);
		
		}
		else
			fnc_notificacion( "Debe seleccionar un horario." );
	}	
</script>


	<center>
        <div style="margin-left:5%; margin-right:5%; width:90%">
            
            <div class="ui-widget-header catalogos_cabecero titulo_opcion">CAT&Aacute;LOGO DE HORARIOS</div>
            <div style="padding: 5px; margin-top: 10px;" id="div_grid">
            <div class="gfieldset" titulo="Horarios"></div>
            	<div id="div_busqueda_horarios" class="ui-state-default catalogos_busqueda">
                	<table style="width: 100%;">
                    	<tr>                        	
                            <td �� style="text-align: left; padding-left:100px;"><span>Horario:</span>
                            	<input type="text" id="txt_de_Horario_busqueda" class="input_text" onChange="fnc_buscar_horarios();" />
                            </td>
                        </tr>
                    </table>
                </div>
                
                <div id="grid_horarios" class="ggrids" 
                	 url="application/catalogos_flavio.cfc?method=Horarios_ListarJSON&id_Empresa= � $class$coldfusion$tagext$io$OutputTag coldfusion.tagext.io.OutputTag � � 8	  � coldfusion/tagext/io/OutputTag � 
doStartTag ()I � �
 � � REQUEST � SESSION � 
ID_EMPRESA � _resolveAndAutoscalarize � X
  � doAfterBody � �
 � � doEndTag � � coldfusion/tagext/QueryLoop �
 � � doCatch (Ljava/lang/Throwable;)V � �
 � � 	doFinally � 
 � ��" 
                     titulo="Listado de Horarios"
                     sortname="id_Horario" ancho="700" 
                     autowidth="false" autocrear="true" 
                     loadComplete="fnc_autoajustar_ancho_busqueda('grid_horarios','div_busqueda_horarios')"
                     >
                     
                    
                    <columna nom_columna="de_Horario">Horario</columna>
                    
                    
                    <columna nom_columna="id_Empresa" visible="false"></columna>
                    <columna nom_columna="id_Horario" visible="false"></columna>

                    <accion accion="agregar"  evento="fnc_horarios_agregar_editar(1);"></accion>
                    <accion accion="editar"   evento="fnc_horarios_agregar_editar(2);"></accion>
                    <accion accion="eliminar" evento="fnc_horarios_agregar_editar(3);"></accion>
                    
                	<accion accion="personalizada" class_icono="ui-icon ui-icon-search" titulo="Detalle Horario" evento="fnc_ver_horariodetalle();"></accion>
                </div>
            </div>
            
            
            <div id="div_guardar_editar_eliminar" class="catalogos_guardar_editar">
            	<div class="ui-widget-header catalogos_guardar_editar_cabecero" id="div_titulo_agregar_editar"></div>
                <div class="catalogos_guardar_editar_cabecero_contenido">
                    <table id="table_clientes_guardar_editar">                      
                        <tr>
                        	<td style="text-align:right"><span>Horario:</span></td>
                            <td>
                            	<input type="text" class="input_text obligatorio" id="txt_de_Horario" acercade="Horario" style="width:224px"
                            		 maxlength="120"  onkeydown="fnc_enter_presionado(event,'fnc_horarios_crud();');"/>
                             </td>
                        </tr>                        
                    </table>                    
                    <div style="margin-top: 10px;">
                    	<button class="boton" title="Guardar" id="btn_guardar" onClick="fnc_horarios_crud();">Guardar</button>
                        <button class="boton" title="Regresar" onClick="fnc_catalogos_mostrar_ocultar( 'div_grid', 'div_guardar_editar_eliminar' );">Regresar</button>
                    </div>
                </div>
            </div>            
            
            
            <div class="gfieldset" titulo="Horarios Detalle" id="fieldsetdetalle" style="display:none"></div>
            <div id="div_horariosdetalles" style="display:none">
            	
                <div id="grid_horariosdetalle" class="ggrids" 
                	 titulo="Listado de Horarios Detalle" 
                     autocrear="false" ancho="800" 
                     autowidth="false" sortname="nu_Dia"
                     >
                                        
                    <columna nom_columna="de_Dia">Dia</columna>
                    <columna nom_columna="hr_Entrada" align="center">Entrada</columna>
                    <columna nom_columna="hr_Salida" align="center">Salida</columna>
                    <columna nom_columna="nu_HorasLaborales" align="center">Horas Laborales</columna>
                    
                    
                    <columna nom_columna="hr_HoraEntrada" visible="false"></columna>
                    <columna nom_columna="hr_MinutoEntrada" visible="false"></columna>
                    <columna nom_columna="hr_HoraSalida" visible="false"></columna>
                    <columna nom_columna="hr_MinutoSalida" visible="false"></columna>
                    <columna nom_columna="id_Empresa" visible="false"></columna>
                    <columna nom_columna="id_Horario" visible="false"></columna>
                    <columna nom_columna="id_HorarioEntradaSalida" visible="false"></columna>
                    <columna nom_columna="id_Dia" visible="false"></columna>
                    <columna nom_columna="nu_Dia" visible="false"></columna>

                    <accion accion="agregar"  evento="fnc_horariosdetalles_agregar_editar(1);"></accion>
                    <accion accion="editar"   evento="fnc_horariosdetalles_agregar_editar(2);"></accion>
                    <accion accion="eliminar" evento="fnc_horariosdetalles_agregar_editar(3);"></accion>
                </div>           
            </div>
            
            
            <div id="div_guardar_editar_eliminar_horariosdetalles" class="catalogos_guardar_editar">
            	<div class="ui-widget-header catalogos_guardar_editar_cabecero" id="div_titulo_agregar_editar_horariosdetalles"></div>
                <div class="catalogos_guardar_editar_cabecero_contenido">
                    <table id="table_horarios_guardar_editar">                      
                    	<tr>
                        	<td style="text-align:right;"><span style="font-weight: bold;">D&iacute;as Laborales:</span></td>
	                        <td style="text-align:left;">
                            
                                <label for="id_Dia_1">Lu</label><input type="checkbox" id="id_Dia_1" nu_Dia="1" de_dia="Lunes" 
                               											class="chk_dias" />
                                
                                <label for="id_Dia_2">Ma</label><input type="checkbox" id="id_Dia_2" nu_Dia="2" de_dia="Martes" 
                                										class="chk_dias" />
                                
                                <label for="id_Dia_3">Mi</label><input type="checkbox" id="id_Dia_3" nu_Dia="3" de_dia="Mi&eacute;rcoles" 
                                										class="chk_dias" />
                                
                                <label for="id_Dia_4">Ju</label><input type="checkbox" id="id_Dia_4" nu_Dia="4" de_dia="Jueves" 
                                										class="chk_dias" />
                                
                                <label for="id_Dia_5">Vi</label><input type="checkbox" id="id_Dia_5" nu_Dia="5" de_dia="Viernes" 
                                										class="chk_dias" />
                                
                                <label for="id_Dia_6">Sa</label><input type="checkbox" id="id_Dia_6" nu_Dia="6" de_dia="Sabado" 
                                										class="chk_dias" />
                                
                                <label for="id_Dia_7">Do</label><input type="checkbox" id="id_Dia_7"nu_Dia="7" de_dia="Domingo" 
                                										class="chk_dias" />
                            </td>
                        </tr>
                        <tr>
                        	<td style="text-align:right"><span style="font-weight: bold;">Entrada:</span></td>
                            <td>
                            	<table style="float:left">
                                	<tr>
                                    	<td>                                        
                                        <select id="ddl_hora_entrada" class="input_text" title="Elija una hora">
                                             � 1 � _double (Ljava/lang/String;)D � �
 k � 23 � 8 � _Object (D)Ljava/lang/Object; � �
 k � P(Ljava/lang/String;Lcoldfusion/runtime/LocalScope;)Lcoldfusion/runtime/Variable;  �
  � set (Ljava/lang/Object;)V � � coldfusion/runtime/Variable �
 � � 
												 � 
													 � _autoscalarize 1(Lcoldfusion/runtime/Variable;)Ljava/lang/Object; � �
  �@$       _compare (Ljava/lang/Object;D)D � �
  � F
                                                    	<option value=" � ">0 � ?</option>
                                                     � "> � 2
                                                 � +
                                       		 � CFLOOP � checkRequestTimeout � .
  � _checkCondition (DDD)Z � �
  �k
                                        </select>
                                        </td>
                                        <td>:</td>
                                        <td>
                                        	<select id="ddl_minuto_entrada" class="input_text" title="Elija los minutos">
                                            	 � 5 � 59 � 0 � 
														 � J
                                                        	<option value=" � C</option>
                                                         � J
                                                       		<option value=" � 6
                                                     ��
                                            </select>
                                        </td>
                                        <td>Hrs.</td>                                        
                                    </tr>                                
                                </table>
                                <table style="float:left; margin-left:30px">
                                	<tr>
                                    	<td style="text-align:right"><span style="font-weight: bold;">Salida:</span></td>
                                    	<td>
                                        	<select id="ddl_hora_salida" class="input_text" title="Elija una hora">
                                                 :
                                                         M
                                                            <option value="Q
                                        	</select>
                                        </td>
                                        <td>:</td>
                                        <td>
                                        	<select id="ddl_minuto_salida" class="input_text">
                                            	=
                                            </select>
                                        </td>
                                        <td>Hrs.</td>
                                    </tr>                                
                                </table>
                            </td>
                        </tr>                        
                    </table>                    
                    <div style="margin-top: 10px;">
                    	<button class="boton" title="Guardar" id="btn_guardar_horariosdetalles" onClick="fnc_horariosdetalles_crud();">Guardar</button>
                        <button class="boton" title="Regresar" onClick="fnc_catalogos_mostrar_ocultar( 'div_horariosdetalles', 'div_guardar_editar_eliminar_horariosdetalles' );">Regresar</button>
                        
                    </div>                    
                </div>
            </div>

        </div>
    </center>
    
	
	<div id="div_dias_xml" style="display: none;"></div>
    
    
	<div id="div_dias_enviar" style="display: none;"></div>
    
    
    <input type="hidden" id="hr_Entrada" />
    <input type="hidden" id="hr_Salida" />
    <input type="hidden" id="Dias" />

    <input type="hidden" id="hid_accion"  />
    <input type="hidden" id="hid_id_Recurso" value="	 
ID_RECURSO " /> metaData Ljava/lang/Object;	  &coldfusion/runtime/AttributeCollection ([Ljava/lang/Object;)V 
 this Lcfhorarios2ecfm886097838; LocalVariableTable Code <clinit> varscope "Lcoldfusion/runtime/VariableScope; locscope Lcoldfusion/runtime/LocalScope; getMetadata ()Ljava/lang/Object; runPage out Ljavax/servlet/jsp/JspWriter; value 
ajaxproxy0 *Lcoldfusion/tagext/html/ajax/AjaxProxyTag; output1  Lcoldfusion/tagext/io/OutputTag; mode1 t7 t8 Ljava/lang/Throwable; t9 t10 t11 D t13 t15 t17 output2 mode2 t20 t21 t22 t23 t24 t26 t28 t30 output3 mode3 t33 t34 t35 t36 t37 t39 t41 t43 output4 mode4 t46 t47 t48 t49 t50 t52 t54 t56 output5 mode5 t59 t60 t61 t62 output6 mode6 t65 t66 t67 t68 LineNumberTable java/lang/Throwablea 1            7 8    � 8             #     *� 
�                  =     :� @� B�� @� ��Y� ^���                   E     *+,� **+,� � �                       !"    "     ��             #"   � 
 E  �*�  � &L*� *N*,� 0*+2� 6*� B-� F� H:*� LNP*� L**R� TYVS� Z\� ^Y`SYbS� f� l� p� sNuw� p� z� �� �� �+�� �+�� �+�� �*� �-� F� �:*� L� �� �Y6� &+*�� TY�SY�S� �� l� �� ����� �� :� #�� � #:� �� � :	� 	�:
� ��
+�� ��� �9�� �9�� �9� �M*� �:,� ˧*+Ͷ 6*� �-� F� �:*~� L� �� �Y6� �*+϶ 6**� � � Ը ��� 6+۶ �+**� � Ӹ l� �+ݶ �+**� � Ӹ l� �+߶ �� 3+۶ �+**� � Ӹ l� �+� �+**� � Ӹ l� �+߶ �*+� 6� ���w� �� :� #�� � #:� �� � :� �:� ��*+� 6c\9� �M,� �� �� ���+� �� �9�� �9�� �9� �M*� �:,� ˧*+϶ 6*� �-� F� �:*�� L� �� �Y6 � �*+�� 6**� � � Ը ��� 6+�� �+**� � Ӹ l� �+ݶ �+**� � Ӹ l� �+�� �� 3+�� �+**� � Ӹ l� �+� �+**� � Ӹ l� �+�� �*+ � 6� ���v� �� :!� #!�� � #:""� �� � :#� #�:$� ��$*+� 6c\9� �M,� �� �� ���+� ��� �9%�� �9'�� �9))� �M*� �:++,� ˧*+ � 6*� �-� F� �:,*�� L,� �,� �Y6-� �*+� 6**� � � Ը ��� 7+� �+**� � Ӹ l� �+ݶ �+**� � Ӹ l� �+�� �� 4+� �+**� � Ӹ l� �+� �+**� � Ӹ l� �+�� �*+ � 6,� ���s,� �� :.� #.�� � #:/,/� �� � :0� 0�:1,� ��1*+� 6)%c\9)� �M+,� �� �%)'� ���+� �� �92�� �94�� �966� �M*� �:88,� ˧*+϶ 6*� �-� F� �:9*�� L9� �9� �Y6:� �*+�� 6**� � � Ը ��� 6+�� �+**� � Ӹ l� �+ݶ �+**� � Ӹ l� �+�� �� 3+�� �+**� � Ӹ l� �+� �+**� � Ӹ l� �+�� �*+ � 69� ���v9� �� :;� #;�� � #:<9<� �� � :=� =�:>9� ��>*+� 662c\96� �M8,� �� �264� ���+
� �*� �-� F� �:?*Զ L?� �?� �Y6@� '+*�� TY�SYS� �� l� �?� ����?� �� :A� #A�� � #:B?B� �� � :C� C�:D?� ��D+� �� $ � � �b � � �b � �b � �b �bblbbl*b*b'*b*/*b�^jbdgjb�^ybdgybjvyby~yb��b���b��b���b���b���b^bb^bbb"b{��b���b{��b���b���b���b   < 9  �    �$%   �&   � ' (   �'(   �)*   �+    �,   �-.   �/. 	  �0 
  �12   �32   �42   �5    �6*   �7    �8   �9.   �:.   �;   �<2   �=2   �>2   �?    �@*   �A     �B !  �C. "  �D. #  �E $  �F2 %  �G2 '  �H2 )  �I  +  �J* ,  �K  -  �L .  �M. /  �N. 0  �O 1  �P2 2  �Q2 4  �R2 6  �S  8  �T* 9  �U  :  �V ;  �W. <  �X. =  �Y >  �Z* ?  �[  @  �\ A  �]. B  �^. C  �_ D`  � g P  U  8  8  k    � � � �}$}+}~���������������������������~Q~b}}k�r�y��������������������.�.�-��������k��������&�9�9�8�N�N�M�m�m�l�������e����������p�x���������������������������p�C�U��������_�              