����  - � 
SourceFile LC:\ColdFusion9\wwwroot\ferrominio\redproject\catalogos\categoriasriesgos.cfm "cfcategoriasriesgos2ecfm2091127905  coldfusion/runtime/CFPage  <init> ()V  
  	 com.macromedia.SourceModTime  J1 coldfusion/runtime/CfJspPage  pageContext #Lcoldfusion/runtime/NeoPageContext;  	   getOut ()Ljavax/servlet/jsp/JspWriter;   javax/servlet/jsp/PageContext 
   parent Ljavax/servlet/jsp/tagext/Tag;  	   com.adobe.coldfusion.*  bindImportPath (Ljava/lang/String;)V   !
  " 
 $ _whitespace %(Ljava/io/Writer;Ljava/lang/String;)V & '
  ( .class$coldfusion$tagext$html$ajax$AjaxProxyTag Ljava/lang/Class; (coldfusion.tagext.html.ajax.AjaxProxyTag , forName %(Ljava/lang/String;)Ljava/lang/Class; . / java/lang/Class 1
 2 0 * +	  4 _initTag P(Ljava/lang/Class;ILjavax/servlet/jsp/tagext/Tag;)Ljavax/servlet/jsp/tagext/Tag; 6 7
  8 (coldfusion/tagext/html/ajax/AjaxProxyTag : _setCurrentLineNo (I)V < =
  > cfajaxproxy @ cfc B APPLICATION D java/lang/String F RF H _resolve 9(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/Object; J K
  L getPath N java/lang/Object P app R catalogos_flavio T _invoke K(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object; V W
  X _String &(Ljava/lang/Object;)Ljava/lang/String; Z [ coldfusion/runtime/Cast ]
 ^ \ _validateTagAttrValue \(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String; ` a
  b setCfc d !
 ; e jsclassname g jsApp_catalogos_flavio i setJsclassname k !
 ; l 	hasEndTag (Z)V n o coldfusion/tagext/GenericTag q
 r p _emptyTcfTag !(Ljavax/servlet/jsp/tagext/Tag;)Z t u
  v 

<script type="text/javascript">
	//Variables globales.
	var g_row_categorias;
	var g_row_riesgos;
	var g_row_mitigaciones;
	var jasApp  = new jsApp_catalogos_flavio();
	
	//Función para abrir la opción de agregar y/o editar.
	function fnc_categoriasriesgos_agregar_editar(p_accion)
	{
		
		if( p_accion == 1 ) //Agregar.
		{
			$("#div_titulo_agregar_editar").html("NUEVA CATEGOR&Iacute;A");
			$("#btn_guardar").attr("title", "Guardar").text("Guardar");
			
			//Nos aseguramos de desbloquear los input.      
			$("#txt_de_CategoriaRiesgo").removeAttr("disabled");
			
			fnc_catalogos_mostrar_ocultar( "div_grid", "div_guardar_editar_eliminar" );

			//Se oculta el catalogo de riesgos
			$("#div_riesgos").slideUp(200);
			//$("#div_guardar_editar_eliminar_riesgos").slideUp(200);
			$("#fieldsetriesgos").slideUp(200);
			//$("#div_busqueda_riesgos").slideUp(200);

			//Se oculta el catalogo de mitigaciones
			$("#div_mitigaciones").slideUp(200);
			$("#fieldsetmitigaciones").slideUp(200);
		}
		else if( p_accion == 2 || p_accion == 3 ) //Editar o Eliminar.
		{
			//Validamos que se haya seleccionado un renglón.
			var row = ggrids.obtener_renglon("grid_categoriasriesgos");
			
			if( row )
			{	
				g_row_categorias = row;
				
				//Llenamos los campos.
				$("#txt_de_CategoriaRiesgo").val( g_row_categorias.de_CategoriaRiesgo );
				
				//Valdiar si debemos bloquear o desbloquear los input.
				if( p_accion == 3 )
				{
					$("#div_titulo_agregar_editar").html("ELIMINAR CATEGOR&iacute;A");
					$("#btn_guardar").attr("title", "Eliminar").text("Eliminar");
					$("#txt_de_CategoriaRiesgo").attr("disabled","disabled");					
				}
				else
				{
					$("#div_titulo_agregar_editar").html("EDITAR CATEGOR&Iacute;A");
					$("#btn_guardar").attr("title", "Guardar").text("Guardar");
					$("#txt_de_CategoriaRiesgo").removeAttr("disabled");
					
				}
				
				fnc_catalogos_mostrar_ocultar( "div_grid", "div_guardar_editar_eliminar" );				
				$("#div_riesgos").slideUp(200);
				$("#div_guardar_editar_eliminar_riesgos").slideUp(200);
				$("#fieldsetriesgos").hide();
				$("#div_busqueda_riesgos").slideUp(200);
				
				//Se oculta el catalogo de mitigaciones
				$("#div_mitigaciones").slideUp(200);
				$("#fieldsetmitigaciones").hide();
			}
			else
				fnc_notificacion("Debe seleccionar una Categor&iacute;a.");
				
		}
		
		$("#hid_accion").val( p_accion );
		
		//Limpiamos los campos y pasamos el foco al control correspondiente
		if( p_accion == 1 )
		{
			$("#txt_de_CategoriaRiesgo").val("");
			$("#txt_de_CategoriaRiesgo").focus();
		}
		if( p_accion == 2 )			
			$("#txt_de_CategoriaRiesgo").focus();
			
		if( p_accion == 3 )
			$("#btn_guardar").focus();		
			
		
	}
	
	//Función para guardar y/o editar.
	function fnc_categoriasriesgos_crud()
	{	
		if( fnc_validar_campos_obligatorios('div_guardar_editar_eliminar') )
		{	
		    //Armamos los parámetros.
			var accion = new Array("CategoriasRiesgos_Agregar","CategoriasRiesgos_Editar","CategoriasRiesgos_Eliminar");
			var parametros = "id_Empresa=" + $("#hid_id_Empresa").val() + "&method="+ accion[$("#hid_accion").val()-1];
			
		    // VALORES DE LOS CAMPOS DEL DIV PARA AGREGAR EDITAR ELIMINAR
			var cat = fnc_codificar(  $.trim( $("#txt_de_CategoriaRiesgo").val())  );
		
				switch( $("#hid_accion").val() )
				{			
					case "1": //Agregar.														
								parametros += "&de_CategoriaRiesgo=" +cat;
								break;
					case "2": //Editar				
								parametros += 
								"&id_CategoriaRiesgo="+g_row_categorias.id_CategoriaRiesgo+
								"&de_CategoriaRiesgo="+cat;
								
								break;
					case "3" : //Eliminar
								parametros += "&id_CategoriaRiesgo=" + g_row_categorias.id_CategoriaRiesgo;
								break;
				}

				//Guardamos, Editamos, Eliminamos.
					fnc_peticionAjax(
					{	
						url: "./application/catalogos_flavio.cfc",
						parametros: parametros,
						f_callback: function()
						{
							//Actualizamos el grid.
							ggrids.recargar("grid_categoriasriesgos");
								
								// si es modificar o eliminar 
								if( $("#hid_accion").val() == "2" || $("#hid_accion").val() == "3" )
								{
									fnc_catalogos_mostrar_ocultar( "div_grid", "div_guardar_editar_eliminar" );
								}
								else
								{
									$("#txt_de_CategoriaRiesgo").val("");
									$("#txt_de_CategoriaRiesgo").focus();
								}		
						}
					});	
				  /*  $("#btn_cerrar_dialogo_gd_avisos").click(function(){
						    $("#txt_de_CategoriaRiesgo").focus();
				            });*/		
			}
		}
	//Función para buscar clientes.
	function fnc_buscar_categoriasriesgos()
	{		
		//Armamos los parámetros.
		var parametros = "";				
			if($("#txt_descripcion_busqueda").val() != "")
				parametros = parametros + "&de_CategoriaRiesgo=" + $.trim( $("#txt_descripcion_busqueda").val());
	
		ggrids.recargar("grid_categoriasriesgos", parametros);
		
		//Ocultaremos los otros grids dependientes.
		$("#div_mitigaciones").slideUp(200);
		$("#fieldsetmitigaciones").hide();
		
		$("#div_riesgos").slideUp(200);			
		$("#fieldsetriesgos").slideUp(200);
		
		//$("#div_mitigaciones").slideUp(200);
		//$("#fieldsetmitigaciones").hide(200);
		
	}
		
		
		/*
			Funciones para catalogo de riesgos
		*/
		
		
		//Función para abrir la opción de agregar y/o editar.
	function fnc_riesgos_agregar_editar(p_accion)
	{
		$("#txt_de_Categoria").val(g_row_categorias.de_CategoriaRiesgo);
		if( p_accion == 1 ) //Agregar.
		{			
			$("#div_titulo_agregar_editar_riesgos").html("NUEVO RIESGO");
			$("#btn_guardar_riesgos").attr("title", "Guardar").text("Guardar");
			
			//Nos aseguramos de desbloquear los input.      
			
			$("#ddl_id_CategoriaRiesgo, #txt_de_Riesgo").removeAttr("disabled");
			$("#div_mitigaciones").slideUp(200);
			$("#fieldsetmitigaciones").hide();
			
			fnc_catalogos_mostrar_ocultar( "div_riesgos", "div_guardar_editar_eliminar_riesgos" );
			
		}
		else if( p_accion == 2 || p_accion == 3 ) //Editar o Eliminar.
		{
			
			//Validamos que se haya seleccionado un renglón.
			var row = ggrids.obtener_renglon("grid_riesgos");
			
			if( row )
			{	
				g_row_riesgos = row;
				
				//Llenamos los campos.
				$("#ddl_id_CategoriaRiesgo").val( g_row_riesgos.id_CategoriaRiesgo );
				$("#txt_de_Riesgo").val( g_row_riesgos.de_Riesgo );
				
				
				//Valdiar si debemos bloquear o desbloquear los input.
				if( p_accion == 3 )
				{
					$("#div_titulo_agregar_editar_riesgos").html("ELIMINAR RIESGO");
					$("#btn_guardar_riesgos").attr("title", "Eliminar").text("Eliminar");
					$("#ddl_id_CategoriaRiesgo, #txt_de_Riesgo").attr("disabled","disabled");
				}
				else
				{
					$("#div_titulo_agregar_editar_riesgos").html("EDITAR RIESGO");
					$("#btn_guardar_riesgos").attr("title", "Guardar").text("Guardar");
					$("#ddl_id_CategoriaRiesgo, #txt_de_Riesgo").removeAttr("disabled");
				}
				
				fnc_catalogos_mostrar_ocultar( "div_riesgos", "div_guardar_editar_eliminar_riesgos" );				
				$("#div_mitigaciones").slideUp(200);
				$("#fieldsetmitigaciones").hide();
			}
			else
				fnc_notificacion("Debe seleccionar un Riesgo.");
				
		}
		
		$("#hid_accion").val( p_accion );
		
		//Limpiamos los campos y pasamos el foco al control correspondiente
		if( p_accion == 1 )
		{
			$("#txt_de_Riesgo").val("");
			$("#txt_de_Riesgo").focus();
		}
		if( p_accion == 2 )			
			$("#txt_de_Riesgo").focus();
			
		if( p_accion == 3 )
			$("#btn_guardar_riesgos").focus();	
		
	}
	
	//Función para guardar y/o editar.
	function fnc_riesgos_crud()
	{	
		if( fnc_validar_campos_obligatorios('div_guardar_editar_eliminar_riesgos') )
		{	
		    //Armamos los parámetros.
		    var accion = new Array("Riesgos_Agregar","Riesgos_Editar","Riesgos_Eliminar");
			var parametros = "id_Empresa=" + $("#hid_id_Empresa").val() + "&method="+ accion[$("#hid_accion").val()-1];
			
			// VALORES DE LOS CAMPOS DEL DIV PARA AGREGAR EDITAR ELIMINAR
			var catR = fnc_codificar(  $.trim( $("# x write z ! java/io/Writer |
 } {Mtxt_de_Categoria").val())  );
			var rgo = fnc_codificar(  $.trim( $("#txt_de_Riesgo").val())  );
			
			//concatenando el metodo
			switch( $("#hid_accion").val() )
			{			
				case "1": //Agregar.														
							parametros +=
							"&id_CategoriaRiesgo="+ g_row_categorias.id_CategoriaRiesgo+
							"&de_Riesgo="+rgo+
							"&id_RecursoRegistro="+$("#hid_id_Recurso").val();
							break;
				case "2": //Editar				
							 parametros +=
							"&id_Riesgo="+ g_row_riesgos.id_Riesgo+
							"&id_CategoriaRiesgo="+ g_row_categorias.id_CategoriaRiesgo+ 
							"&de_Riesgo="+rgo+
							"&id_RecursoRegistro="+$("#hid_id_Recurso").val();
							
							break;
				case "3" : //Eliminar
							parametros += 
							"&id_CategoriaRiesgo="+ g_row_categorias.id_CategoriaRiesgo+
							"&id_Riesgo=" + g_row_riesgos.id_Riesgo;
							break;
			}
			
			fnc_peticionAjax({
				url: "./application/catalogos_flavio.cfc",
				parametros: parametros,
				f_callback: function()
				{
					//Actualizamos el grid.
					ggrids.recargar("grid_riesgos");
				
					if( $("#hid_accion").val() == "2" || $("#hid_accion").val() == "3" )
					{
						fnc_catalogos_mostrar_ocultar( "div_riesgos", "div_guardar_editar_eliminar_riesgos" );
					}
					
					else
					{
						$("#txt_de_Riesgo").val("");
						$("#txt_de_Riesgo").focus();
					}		
		
				}
			});
			
		}
	}
	
	//Función para buscar clientes.
	function fnc_buscar_riesgos()
		{	
			var row = $("#grid_grid_categoriasriesgos").jqGrid("getGridParam", "selrow");
			var url = "application/catalogos_flavio.cfc?method=Riesgos_Listar_JSON&id_Empresa="+ $("#hid_id_Empresa").val();	
			//Armamos los parámetros.
			var parametros = "";
				if($("#txt_descripcion_busqueda_riesgo").val() != "")
					parametros = parametros + "&de_Riesgo=" + $.trim( $("#txt_descripcion_busqueda_riesgo").val());					
					
				if( row )
				{
					row = $("#grid_grid_categoriasriesgos").jqGrid("getRowData", row);
					g_row_categorias = row;
					
					parametros = parametros + "&id_CategoriaRiesgo=" + g_row_categorias.id_CategoriaRiesgo;
					//alert(g_row_categorias.id_CategoriaRiesgo);
				}
					
		
			$("#grid_grid_riesgos").jqGrid("setGridParam", {
				url: url + parametros
				}
			).trigger("reloadGrid");
		}
			
	function fnc_ver_riesgos()
	{
		//Validamos que se haya seleccionado un renglón.
		var row = ggrids.obtener_renglon("grid_categoriasriesgos");	

		if( row )
		{
			g_row_categorias = row;

			var url = "application/catalogos_flavio.cfc?method=Riesgos_Listar_JSON&id_Empresa="+ $("#hid_id_Empresa").val();
			var parametros = "&id_CategoriaRiesgo=" + g_row_categorias.id_CategoriaRiesgo;
			
			$("#grid_riesgos").attr("titulo", "Listado de Riesgos");
			$("#grid_riesgos").attr('url', url + parametros);
			
			ggrids.crear("grid_riesgos");			
			$("#div_riesgos").slideDown(200);
			$("#fieldsetriesgos").show();
			$("#div_busqueda_riesgos").slideDown(200);
			//Despues de crear el grid de riesgos se redimensiona el grid de busqueda.
			$("#div_guardar_editar_eliminar_riesgos").slideUp(200);
			$("#div_mitigaciones").slideUp(200);
			$("#fieldsetmitigaciones").hide(200);
			
			fnc_autoajustar_ancho_busqueda('grid_riesgos','div_busqueda_riesgos');
		}
		else
			fnc_notificacion( "Debe seleccionar una Categor&iacute;a." );
	}
	
	/*
		Funciones para el manejo de el catalogo de mitigaciones y contingencias.
	
	*/
	
	//Función para abrir la opción de agregar y/o editar.
	function fnc_mitigaciones_agregar_editar(p_accion)
	{
		if( p_accion == 1 ) //Agregar.
		{
			$("#div_titulo_agregar_editar_mitigaciones").html("NUEVA MITIGACI&Oacute;N");
			$("#btn_guardar_mitigaciones").attr("title", "Guardar").text("Guardar");
			//Establecemos la informacion de categoria y riesgo seleccionadas para la mitigacion		
			$("#txt_de_Categoria_mitigacion").val( g_row_categorias.de_CategoriaRiesgo );
			$("#txt_de_Riesgo_mitigacion").val( g_row_riesgos.de_Riesgo );
			
			//Nos aseguramos de desbloquear los input. 
			$("#txt_de_Mitigacion, #txt_de_Contingencia").removeAttr("disabled");
			
			fnc_catalogos_mostrar_ocultar( "div_mitigaciones", "div_guardar_editar_eliminar_mitigaciones" );
		}
		else if( p_accion == 2 || p_accion == 3 ) //Editar o Eliminar.
		{
			//Validamos que se haya seleccionado un renglón.
			var row = ggrids.obtener_renglon("grid_mitigaciones");
			
			if( row )
			{	
				g_row_mitigaciones = row;
				
				//Llenamos los campos.
				$("#txt_de_Categoria_mitigacion").val( g_row_categorias.de_CategoriaRiesgo );
				$("#txt_de_Riesgo_mitigacion").val( g_row_riesgos.de_Riesgo );
				$("#txt_de_Mitigacion").val( g_row_mitigaciones.de_Mitigacion );
				$("#txt_de_Contingencia").val( g_row_mitigaciones.de_Contingencia );
				
				//Valdiar si debemos bloquear o desbloquear los input.
				if( p_accion == 3 )
				{
					$("#div_titulo_agregar_editar_mitigaciones").html("ELIMINAR MITIGACI&Oacute;N");
					$("#btn_guardar_mitigaciones").attr("title", "Eliminar").text("Eliminar");
					$("#txt_de_Mitigacion, #txt_de_Contingencia").attr("disabled","disabled");
				}
				else
				{
					$("#div_titulo_agregar_editar_mitigaciones").html("EDITAR MITIGACI&Oacute;N");
					$("#btn_guardar_mitigaciones").attr("title", "Guardar").text("Guardar");
					$("#txt_de_Mitigacion, #txt_de_Contingencia").removeAttr("disabled");
				}
				
				fnc_catalogos_mostrar_ocultar( "div_mitigaciones", "div_guardar_editar_eliminar_mitigaciones" );				
			}
			else
				fnc_notificacion("Debe seleccionar una Mitigaci&oacute;n.");
				
		}
		
		$("#hid_accion").val( p_accion );
		
		//Limpiamos los campos.
		if( p_accion == 1 )
		{
			$("#ddl_id_CategoriaRiesgo option:first").attr("selected", "selected");
			$("#txt_de_Riesgo").val("");			
			
		}
		
		//Limpiamos los campos y pasamos el foco al control correspondiente
		if( p_accion == 1 )		{
			
			$("#txt_de_Mitigacion").val("");
			$("#txt_de_Contingencia").val("");
			$("#txt_de_Mitigacion").focus();
		}
		if( p_accion == 2 )			
			$("#txt_de_Mitigacion").focus();
			
		if( p_accion == 3 )
			$("#btn_guardar_mitigaciones").focus();

	}
	
	//Función para guardar y/o editar.
	function fnc_mitigaciones_crud()
	{	
		var accion = $("#hid_accion").val();
		if( fnc_validar_campos_obligatorios('div_guardar_editar_eliminar_mitigaciones') )
		{
		    //Armamos los parámetros.
			var accion = new Array("Mitigaciones_Agregar","Mitigaciones_Editar","Mitigaciones_Eliminar");
			var parametros = "id_Empresa=" + $("#hid_id_Empresa").val() + "&method="+ accion[$("#hid_accion").val()-1];
			
			// VALORES DE LOS CAMPOS DEL DIV PARA AGREGAR EDITAR ELIMINAR
			var mit = fnc_codificar(  $.trim( $("#txt_de_Mitigacion").val() )  );
			var con = fnc_codificar(  $.trim($ ("#txt_de_Contingencia").val() ) );
			
			//concatenando el metodo
			switch( $("#hid_accion").val() )
			{			
				case "1": //Agregar.														
							parametros +=
							"&id_CategoriaRiesgo="+ g_row_categorias.id_CategoriaRiesgo+
							"&id_Riesgo="+ g_row_riesgos.id_Riesgo+
							"&de_Mitigacion="+ mit+
							"&de_Contingencia="+ con+ 
							"&id_RecursoRegistro="+$("#hid_id_Recurso").val();
							break;
							
				case "2": //Editar				
							parametros +=
							"&id_CategoriaRiesgo="+ g_row_categorias.id_CategoriaRiesgo+
							"&id_Riesgo="+ g_row_riesgos.id_Riesgo+
							"&id_Mitigacion="+ g_row_mitigaciones.id_Mitigacion+
							"&id_Contingencia="+ g_row_mitigaciones.id_Contingencia+
							"&de_Mitigacion="+ mit+
							"&de_Contingencia="+ con+ 
							"&id_RecursoRegistro="+$("# \hid_id_Recurso").val();
							break;
				case "3" : //Eliminar
							parametros +=
							"&id_CategoriaRiesgo="+ g_row_categorias.id_CategoriaRiesgo+
							"&id_Riesgo="+ g_row_riesgos.id_Riesgo+
							"&id_Mitigacion="+ g_row_mitigaciones.id_Mitigacion;
							break;
			}
			
			fnc_peticionAjax({
				url: "./application/catalogos_flavio.cfc",
				parametros: parametros,
				f_callback: function()
				{
					//Actualizamos el grid.
					ggrids.recargar("grid_mitigaciones");
				
					if( $("#hid_accion").val() == "2" || $("#hid_accion").val() == "3" )
					{
						fnc_catalogos_mostrar_ocultar( "div_mitigaciones", "div_guardar_editar_eliminar_mitigaciones" );
					}
					
					else
					{
						$("#txt_de_Mitigacion").val("");
						$("#txt_de_Contingencia").val("");
						$("#txt_de_Mitigacion").focus();
					}		
		
				}
			});
			
		}
	}
	
	//Función para buscar clientes.
	function fnc_buscar_mitigaciones()
		{		
			//Armamos los parámetros.
			var parametros = "";
				if($("#txt_descripcion_busqueda_mitigacion").val() != "")
					parametros = parametros + "&de_Mitigacion=" + $.trim( $("#txt_descripcion_busqueda_mitigacion").val());
				if($("#txt_descripcion_busqueda_contingencia").val() != "")
					parametros = parametros + "&de_Contingencia=" + $.trim( $("#txt_descripcion_busqueda_contingencia").val());			
		
			ggrids.recargar("grid_mitigaciones", parametros);
		}
		
		
	function fnc_ver_mitigaciones()
	{
		//Validamos que se haya seleccionado un renglón.
		var row = ggrids.obtener_renglon("grid_riesgos");
		//alert(row);
		if( row )
		{
			g_row_riesgos = row;

			var url = "application/catalogos_flavio.cfc?method=Mitigaciones_Listar_JSON&id_Empresa="+ $("#hid_id_Empresa").val();
			var parametros = "&id_CategoriaRiesgo=" + g_row_categorias.id_CategoriaRiesgo + "&id_Riesgo=" + g_row_riesgos.id_Riesgo;
							 
			//alert(g_row_riesgos.id_Riesgo);
			$("#grid_mitigaciones").attr("titulo", "Listado de Mitigaciones");
			$("#grid_mitigaciones").attr('url', url + parametros);
			
			ggrids.crear("grid_mitigaciones");
			$("#fieldsetmitigaciones").show(200);
			$("#div_mitigaciones").slideDown(200);
			//$("#div_riesgos").slideDown(200);
			//$("#fieldsetriesgos").slideDown(200);
			//$("#div_busqueda_riesgos").slideDown(200);
			//Despues de crear el grid de riesgos se redimensiona el grid de busqueda
			$("#div_guardar_editar_eliminar_mitigaciones").slideUp(200);				
			fnc_autoajustar_ancho_busqueda('grid_mitigaciones','div_busqueda_mitigaciones');
		}
		else
			fnc_notificacion( "Debe seleccionar un Riesgo." );
	}	
</script>

	<center>
        <div style="margin-left:5%; margin-right:5%; width:90%">
            
            <div class="ui-widget-header catalogos_cabecero titulo_opcion">CAT&Aacute;LOGO DE CATEGOR&Iacute;AS DE RIESGOS</div>
            <div style="padding: 5px; margin-top: 10px;" id="div_grid">
            <div class="gfieldset" titulo="Categor&iacute;as"></div>
            	<div id="div_busqueda" class="ui-state-default catalogos_busqueda">
                	<table style="width: 100%;">
                    	<tr>                        	
                            <td style="text-align: left; padding-left:80px;"><span>Categor&iacute;a: </span>
                            
                            	<input type="text" id="txt_descripcion_busqueda" onkeydown="if (event.keyCode == 13) {fnc_buscar_categoriasriesgos();}"  class="input_text" onChange="fnc_buscar_categoriasriesgos();"
                                	style="width: 200px;" />
                            </td>
                        </tr>
                    </table>
                </div>
                
                
                <div id="grid_categoriasriesgos" class="ggrids" url="application/catalogos_flavio.cfc?method=CategoriasRiesgos_Listar_JSON&id_Empresa= � $class$coldfusion$tagext$io$OutputTag coldfusion.tagext.io.OutputTag � � +	  � coldfusion/tagext/io/OutputTag � 
doStartTag ()I � �
 � � REQUEST � SESSION � 
ID_EMPRESA � _resolveAndAutoscalarize � K
  � doAfterBody � �
 � � doEndTag � � coldfusion/tagext/QueryLoop �
 � � doCatch (Ljava/lang/Throwable;)V � �
 � � 	doFinally � 
 � ��" paginado="10" titulo="Listado de Categor&iacute;as"
                    sortname="de_CategoriaRiesgo" ancho="600" autowidth="false" 
                    loadComplete="fnc_autoajustar_ancho_busqueda('grid_categoriasriesgos','div_busqueda')">
                    
                                        
                    <columna nom_columna="de_CategoriaRiesgo">Categor&iacute;a</columna>
                    
                    
                    <columna nom_columna="id_Empresa" visible="false"></columna>                    
                    <columna nom_columna="id_CategoriaRiesgo" visible="false"></columna>                    

					
                    <accion accion="agregar" evento="fnc_categoriasriesgos_agregar_editar(1);"></accion>
                    <accion accion="editar" evento="fnc_categoriasriesgos_agregar_editar(2);"></accion>
                    <accion accion="eliminar" evento="fnc_categoriasriesgos_agregar_editar(3);"></accion>
                    
                	<accion accion="personalizada" class_icono="ui-icon ui-icon-search" titulo="Ver Riesgos" evento="fnc_ver_riesgos();"></accion>
                </div>
            </div>
            
            
            <div id="div_guardar_editar_eliminar" class="catalogos_guardar_editar">
            	<div class="ui-widget-header catalogos_guardar_editar_cabecero" id="div_titulo_agregar_editar"></div>
                <div class="catalogos_guardar_editar_cabecero_contenido">
                    <table>                      
                        <tr>
                        	<td style="text-align:right;"><span>Categor&iacute;a:</span></td>
                            <td style="text-align:left;">
                            	<input type="text" class="input_text obligatorio" id="txt_de_CategoriaRiesgo" acercade="Categor&iacute;a"
                            		style="width: 200px" maxlength="150"  />
                            </td>
                        </tr>                        
                    </table>                    
                    <div style="margin-top: 10px;">
                    	<button class="boton" title="Guardar" id="btn_guardar" onClick="fnc_categoriasriesgos_crud();">Guardar</button>
                        <button class="boton" title="Regresar" onClick="fnc_catalogos_mostrar_ocultar( 'div_grid', 'div_guardar_editar_eliminar' );">Regresar</button>
                    </div>
                </div>
            </div>            
            <div class="gfieldset" titulo="Riesgos" id="fieldsetriesgos" style="display:none"></div>            	
            
            <div id="div_riesgos" style="display:none">            
            	<div id="div_busqueda_riesgos" class="ui-state-default catalogos_busqueda" style="display:none">
                	<table style="width: 100%;">
                    	<tr>                       	
                            <td style="text-align: left;"><span>Riesgo: </span>
                            	<input type="text" id="txt_descripcion_busqueda_riesgo"  onkeydown="if (event.keyCode == 13) {fnc_buscar_riesgos();}"  class="input_text" onChange="fnc_buscar_riesgos();"
                                	style=" width: 450px;" maxlength="350" />
                            </td>
                        </tr>
                    </table>
                </div>
                
                <div id="grid_riesgos" class="ggrids" url="application/catalogos_flavio.cfc?method=Riesgos_Listar_JSON&id_Empresa= ��" titulo="Listado de Riesgos"
                    sortname="de_CategoriaRiesgo" autocrear="false" ancho="900" autowidth="false">
                    
                                        
                    <columna nom_columna="de_Riesgo">Riesgo</columna>
                    <columna nom_columna="de_CategoriaRiesgo">Categor&iacute;a</columna>
                    
                    
                    <columna nom_columna="id_Empresa" visible="false"></columna>                    
                    <columna nom_columna="id_CategoriaRiesgo" visible="false"></columna>
                    <columna nom_columna="id_Riesgo" visible="false"></columna>                    

                    <accion accion="agregar" evento="fnc_riesgos_agregar_editar(1);"></accion>
                    <accion accion="editar" evento="fnc_riesgos_agregar_editar(2);"></accion>
                    <accion accion="eliminar" evento="fnc_riesgos_agregar_editar(3);"></accion>
                    
                    <accion accion="personalizada" class_icono="ui-icon ui-icon-search" titulo="Ver Mitigaciones" evento="fnc_ver_mitigaciones();"></accion>
                </div>           
            </div>
            
            <div id="div_guardar_editar_eliminar_riesgos" class="catalogos_guardar_editar">
            	<div class="ui-widget-header catalogos_guardar_editar_cabecero" id="div_titulo_agregar_editar_riesgos"></div>
                <div class="catalogos_guardar_editar_cabecero_contenido">
                    <table>                      
                    	<tr>
                        	<td style="text-align:right"><span>Categor&iacute;a: </span></td>
	                        <td style="text-align:left">
                                <input type="text" class="input_text obligatorio" id="txt_de_Categoria" acercade="Categor&iacute;a" 
                                	style="width: 300px" disabled="disabled" maxlength="150"  />
                            </td>
                        </tr>
                        <tr>
                        	<td style="text-align:right"><span>Riesgo: </span></td>
                            <td style="text-align:left">
                            	<input type="text" class="input_text obligatorio" id="txt_de_Riesgo" acercade="Riesgo" 
                                	style="width: 300px" maxlength="350"  />
                            </td>
                        </tr>                        
                    </table>                    
                    <div style="margin-top: 10px;">
                    	<button class="boton" title="Guardar" id="btn_guardar_riesgos" onClick="fnc_riesgos_crud();">Guardar</button>
                        <button class="boton" title="Regresar" onClick="fnc_catalogos_mostrar_ocultar( 'div_riesgos', 'div_guardar_editar_eliminar_riesgos' );">Regresar</button>
                    </div>
                </div>
            </div>
            
            
            <div class="gfieldset" titulo="Mitigaciones y Contingencias" id="fieldsetmitigaciones" style="display:none"></div>            	
            
            <div id="div_mitigaciones" style="display:none">            
            	<div id="div_busqueda_mitigaciones" class="ui-state-default catalogos_busqueda">
                	<table style="width: 100%;">
                    	<tr>                        	                     	
                            <td style="text-align: left;"><span>Mitigaci&oacute;n:</span>
                            	<input type="text" id="txt_descripcion_busqueda_mitigacion"  onkeydown="if (event.keyCode == 13) {fnc_buscar_mitigaciones();}"  class="input_text" onChange="fnc_buscar_mitigaciones();"
                                	style="width: 50%;" maxlength="350" />
                            </td>
                            
                            <td style="text-align: left;"><span>Contingencia: </span>
                            	<input type="text" id="txt_descripcion_busqueda_contingencia" onkeydown="if (event.keyCode == 13) {fnc_buscar_mitigaciones();}" class="input_text" onChange="fnc_buscar_mitigaciones();"
                                	style="width: 50%;" maxlength="350" />
                            </td>
                        </tr>
                    </table>
                </div>
                
                <div id="grid_mitigaciones" class="ggrids" url="application/catalogos_flavio.cfc?method=Mitigaciones_Listar_JSON&id_Empresa= ��" titulo="Listado de Mitigaciones y Contingencias"
                    sortname="de_Mitigacion" ancho="900" autowidth="false" autocrear="false">
                    
                    
                    <columna nom_columna="de_Mitigacion">Mitigaci&oacute;n</columna>
                    <columna nom_columna="de_Contingencia">Contingencia</columna>
                    
                    
                    <columna nom_columna="id_Empresa" visible="false"></columna>
                    <columna nom_columna="id_CategoriaRiesgo" visible="false"></columna>
                    <columna nom_columna="id_Riesgo" visible="false"></columna>
                    <columna nom_columna="id_Mitigacion" visible="false"></columna>
                    <columna nom_columna="id_Contingencia" visible="false"></columna>

                    <accion accion="agregar" evento="fnc_mitigaciones_agregar_editar(1);"></accion>
                    <accion accion="editar" evento="fnc_mitigaciones_agregar_editar(2);"></accion>
                    <accion accion="eliminar" evento="fnc_mitigaciones_agregar_editar(3);"></accion>
                </div>           
            </div>
            
            <div id="div_guardar_editar_eliminar_mitigaciones" class="catalogos_guardar_editar">
            	<div class="ui-widget-header catalogos_guardar_editar_cabecero" id="div_titulo_agregar_editar_mitigaciones"></div>
                <div class="catalogos_guardar_editar_cabecero_contenido">
                    <table>                      
                    	<tr>
                        	<td style="text-align:right"><span>Categor&iacute;a:</span></td>
	                        <td style="text-align:left">
                                <input type="text" class="input_text obligatorio" id="txt_de_Categoria_mitigacion" acercade="Categor&iacute;a"
                                	style="width: 450px;" disabled="disabled" maxlength="150"  />
                            </td>
                        </tr>
                        <tr>
                        	<td style="text-align:right"><span>Riesgo:</span></td>
                            <td style="text-align:left">
                            	<input type="text" class="input_text obligatorio" id="txt_de_Riesgo_mitigacion" acercade="Riesgo" 
                                	style="width: 450px;" disabled="disabled" maxlength="350"  />
                            </td>
                        </tr>
                        <tr>
                        	<td style="text-align:right"><span>Mitigaci&oacute;n:</span></td>
                            <td style="text-align:left">
                            	<input type="text" class="input_text obligatorio" id="txt_de_Mitigacion" acercade="Mitigaci&oacute;n" 
                                	style="width: 450px;" maxlength="350" />
                            </td>
                        </tr>
                        <tr>
                        	<td style="text-align:right"><span>Contingencia:</span></td>
                            <td style="text-align:left">
                            	<input type="text" class="input_text obligatorio" id="txt_de_Contingencia" acercade="Contingencia" 
                                	style="width: 450px;" maxlength="350"  />
                            </td>
                        </tr>
                    </table>                    
                    <div style="margin-top: 10px;">
                    	<button class="boton" title="Guardar" id="btn_guardar_mitigaciones" onClick="fnc_mitigaciones_crud();">Guardar</button>
                        <button class="boton" title="Regresar" onClick="fnc_catalogos_mostrar_ocultar( 'div_mitigaciones', 'div_guardar_editar_eliminar_mitigaciones' );">Regresar</button>
                    </div>
                </div>
            </div>
            
        </div>
    </center>

<input type="hidden" id="hid_accion"  />
<input type="hidden" id="hid_id_Recurso" value=" � 
ID_RECURSO � " /> � metaData Ljava/lang/Object; � �	  � &coldfusion/runtime/AttributeCollection � ([Ljava/lang/Object;)V  �
 � � this $Lcfcategoriasriesgos2ecfm2091127905; LocalVariableTable Code <clinit> getMetadata ()Ljava/lang/Object; runPage out Ljavax/servlet/jsp/JspWriter; value 
ajaxproxy0 *Lcoldfusion/tagext/html/ajax/AjaxProxyTag; output1  Lcoldfusion/tagext/io/OutputTag; mode1 I t7 t8 Ljava/lang/Throwable; t9 t10 output2 mode2 t13 t14 t15 t16 output3 mode3 t19 t20 t21 t22 output4 mode4 t25 t26 t27 t28 LineNumberTable java/lang/Throwable � 1       * +    � +    � �        �   #     *� 
�    �        � �    �   �   =     -� 3� 5�� 3� �� �Y� Q� �� ��    �        � �    � �  �   "     � ��    �        � �    � �  �   
   �*� � L*� N*� #*+%� )*� 5-� 9� ;:*� ?AC*� ?**E� GYIS� MO� QYSSYUS� Y� _� c� fAhj� c� m� s� w� �+y� ~+�� ~+�� ~*� �-� 9� �:*G� ?� s� �Y6� &+*�� GY�SY�S� �� _� ~� ����� �� :� #�� � #:� �� � :	� 	�:
� ��
+�� ~*� �-� 9� �:*|� ?� s� �Y6� &+*�� GY�SY�S� �� _� ~� ����� �� :� #�� � #:� �� � :� �:� ��+�� ~*� �-� 9� �:*�� ?� s� �Y6� &+*�� GY�SY�S� �� _� ~� ����� �� :� #�� � #:� �� � :� �:� ��+�� ~*� �-� 9� �:*�� ?� s� �Y6� &+*�� GY�SY�S� �� _� ~� ����� �� :� #�� � #:� �� � :� �:� ��+�� ~�  � � � � � � � � � � � � � � � � �8r~ �x{~ �8r� �x{� �~�� ���� ��� �� ��� �� � � �F�� ���� �F�� ���� ���� ���� �  �  $   � � �    � � �   � � �   �     � � �   � � �   � � �   � � �   � � �   � � � 	  � � � 
  � � �   � � �   � � �   � � �   � � �   � � �   � � �   � � �   � � �   � � �   � � �   � � �   � � �   � � �   � � �   � � �   � � �   � � �  �   ^  P  U  8  8  k    �G �G �G �GD|D|C||��������R�R�Q�+�              