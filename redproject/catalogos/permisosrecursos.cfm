����  -� 
SourceFile KC:\ColdFusion9\wwwroot\ferrominio\redproject\catalogos\permisosrecursos.cfm  cfpermisosrecursos2ecfm202430168  coldfusion/runtime/CFPage  <init> ()V  
  	 bindPageVariables D(Lcoldfusion/runtime/VariableScope;Lcoldfusion/runtime/LocalScope;)V   coldfusion/runtime/CfJspPage 
   TIPOSPERMISO_LISTADO Lcoldfusion/runtime/Variable;  bindPageVariable r(Ljava/lang/String;Lcoldfusion/runtime/VariableScope;Lcoldfusion/runtime/LocalScope;)Lcoldfusion/runtime/Variable;  
    	   I   	   com.macromedia.SourceModTime  J1 pageContext #Lcoldfusion/runtime/NeoPageContext; ! "	  # getOut ()Ljavax/servlet/jsp/JspWriter; % & javax/servlet/jsp/PageContext (
 ) ' parent Ljavax/servlet/jsp/tagext/Tag; + ,	  - com.adobe.coldfusion.* / bindImportPath (Ljava/lang/String;)V 1 2
  3 


 5 _whitespace %(Ljava/io/Writer;Ljava/lang/String;)V 7 8
  9 &class$coldfusion$tagext$lang$InvokeTag Ljava/lang/Class;  coldfusion.tagext.lang.InvokeTag = forName %(Ljava/lang/String;)Ljava/lang/Class; ? @ java/lang/Class B
 C A ; <	  E _initTag P(Ljava/lang/Class;ILjavax/servlet/jsp/tagext/Tag;)Ljavax/servlet/jsp/tagext/Tag; G H
  I  coldfusion/tagext/lang/InvokeTag K _setCurrentLineNo (I)V M N
  O APPLICATION Q java/lang/String S RF U _resolve 9(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/Object; W X
  Y getApp [ java/lang/Object ] 	catalogos _ _invoke K(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object; a b
  c setComponent (Ljava/lang/Object;)V e f
 L g TiposPermiso_Listar i 	setMethod k 2
 L l TiposPermiso_Listado n setReturnVariable p 2
 L q &coldfusion/runtime/AttributeCollection s typeresponse u struct w ([Ljava/lang/Object;)V  y
 t z setAttributecollection (Ljava/util/Map;)V | }
 L ~ 	hasEndTag (Z)V � � coldfusion/tagext/GenericTag �
 � � _emptyTcfTag !(Ljavax/servlet/jsp/tagext/Tag;)Z � �
  � 

<script type="text/javascript">
	//Variables globales.
	var g_row;
	
	$(document).ready(function(){
		//Retringimos las fechas.
		setTimeout(function(){
			$("#txt_fh_InicioPermiso, #txt_fh_FinPermiso").datepicker( "option", "minDate", fnc_fecha_objeto( $("#hid_fh_Actual").val(), true ) );
		}, 100);
	});
	
	//FunciÃ³n para abrir la opciÃ³n de agregar, editar o eliminar.
	function fnc_permisos_recursos_agregar_editar(p_accion)
	{
		if( p_accion == 1 ) //Agregar.
		{
			$("#div_titulo_agregar_editar").html("NUEVO PERMISO");
			$("#btn_guardar").attr("title", "Guardar").text("Guardar");
			
			fnc_limpiar_campos_permisos_recuros();
			
			//Nos aseguramos de desbloquear los input.
			$("#ddl_tipo_permiso, #txt_nb_Permiso, #txt_de_Permiso, #txt_fh_InicioPermiso, #txt_fh_FinPermiso, #ddl_horas_inicio, #ddl_minutos_inicio, #txt_nu_HorasPermiso, #txt_nu_MinutosPermiso").removeAttr("disabled");
			
			fnc_catalogos_mostrar_ocultar( "div_grid", "div_guardar_editar_eliminar" );
		}
		else if( p_accion == 2 || p_accion == 3 ) //Editar o Eliminar.
		{
			//Validamos que se haya seleccionado un renglÃ³n.
			var row = ggrids.obtener_renglon("grid_permisos_recursos");
			
			if( row )
			{	
				g_row = row;	
						
				//Si la fecha fin ya pasÃ³, no se puede modificar el permiso.
				
				//Consultamos la fecha actual.
				fnc_peticionAjax({
					url: "./application/adm_proy.cfc",
					parametros: "method=FechaActual",
					automensaje: false,
					div_success: "div_peticiones_ajax_ph",
					f_callback: function(){
						var fh_Actual = fnc_fecha_objeto( $.trim( $("#div_peticiones_ajax_ph").html() ), true );
						var fh_FinPermiso = fnc_fecha_objeto( row.fh_Fin , true );
						
						if( fh_FinPermiso > fh_Actual )
						{
							//Llenamos los campos.
							$("#txt_id_Recurso").val(row.id_Recurso);
							$("#txt_nb_Recurso").val(row.nb_NombreCompletoRecurso);
							$("#ddl_recursos").val( row.id_Recurso );
							$("#ddl_tipo_permiso").val( row.id_TipoPermiso ).trigger("change");
							$("#txt_nb_Permiso").val( row.nb_Permiso );
							$("#txt_de_Permiso").val( row.de_Permiso );
							$("#txt_fh_InicioPermiso").val( row.fh_Inicio.split(" ")[0] );
							$("#txt_fh_FinPermiso").val( row.fh_Fin.split(" ")[0] );
							
							var horas = row.nu_HorasPermiso.split(".")[0] * 1;
							var minutos = ( ( ( row.nu_HorasPermiso.split(".").length == 2 ? "." + row.nu_HorasPermiso.split(".")[1] : 0 ) * 1 ) * 60 ).toFixed(2) * 1;
							if( row.id_TipoPermiso == "1" )
							{
								$("#ddl_horas_inicio").val( row.fh_Inicio.split(" ")[1].split(":")[0] * 1 );
								$("#ddl_minutos_inicio").val( row.fh_Inicio.split(" ")[1].split(":")[1] * 1 );
								$("#txt_nu_HorasPermiso").val( ( horas == 0 ? "" : Math.round(horas, 0) ) );
								$("#txt_nu_MinutosPermiso").val( ( minutos == 0 ? "" : Math.round(minutos, 0) ) );
							}
							
							//Valdiar si debemos bloquear o desbloquear los input.
							if( p_accion == 3 )
							{
								$("#div_titulo_agregar_editar").html("ELIMINAR PERMISO");
								$("#btn_guardar").attr("title", "Eliminar").text("Eliminar");
								$("#ddl_recursos, #ddl_tipo_permiso, #txt_nb_Permiso, #txt_de_Permiso, #txt_fh_InicioPermiso, #txt_fh_FinPermiso, #ddl_horas_inicio, #ddl_minutos_inicio, #txt_nu_HorasPermiso, #txt_nu_MinutosPermiso").attr("disabled", "disabled");
							}
							else
							{
								$("#div_titulo_agregar_editar").html("EDITAR PERMISO");
								$("#btn_guardar").attr("title", "Guardar").text("Guardar");
								$("#txt_nb_Permiso, #txt_de_Permiso, #txt_fh_InicioPermiso, #txt_fh_FinPermiso, #ddl_horas_inicio, #ddl_minutos_inicio, #txt_nu_HorasPermiso, #txt_nu_MinutosPermiso").removeAttr("disabled");
								
								$("#ddl_recursos, #ddl_tipo_permiso").attr("disabled", "disabled");
							}
								
							
							fnc_catalogos_mostrar_ocultar( "div_grid", "div_guardar_editar_eliminar" );
						}
						else
							fnc_mostrar_aviso( "gd_avisos", "Este permiso no puede ser modificado porque ya ha transcurrido.", 5 );
					}
				});
			}
			else
				fnc_notificacion("Debe seleccionar un Permiso.");
		}
		
		$("#hid_accion").val( p_accion );
		
		$("#ddl_recursos").focus();
	}
	
	//FunciÃ³n para limpiar los campos.
	function fnc_limpiar_campos_permisos_recuros()
	{
		$("#txt_id_Recurso").val("");
		$("#txt_nb_Recurso").val("");
		$("#ddl_tipo_permiso").val(1).trigger("change");
		$("#txt_nb_Permiso").val("");
		$("#txt_de_Permiso").val("");
		$("#txt_fh_InicioPermiso").val( $("#hid_fh_Actual").val() );
		$("#txt_fh_FinPermiso").val( $("#hid_fh_Actual").val() );
		$("#ddl_horas_inicio").val(8);
		$("#ddl_minutos_inicio").val(0);
		$("#ddl_horas_fin").val(8);
		$("#ddl_minutos_fin").val(0);
		$("#txt_nu_HorasPermiso").val("");
		$("#txt_nu_MinutosPermiso").val("");
	}
	
	//FunciÃ³n para guardar, editar o eliminar.
	function fnc_permisos_recursos_crud()
	{
		//Validamos los campos obligaorios.
		if( fnc_validar_campos_obligatorios("div_permisos_recursos_crud") )
		{
			//Validamos el tiempo del permiso.
			var nu_HorasPermiso = $("#txt_nu_HorasPermiso").val() * 1;
			nu_HorasPermiso += $("#txt_nu_MinutosPermiso").val() * 1;
			if( nu_HorasPermiso > 0 || $("#ddl_tipo_permiso option:selected").val() == "2" )
			{
				//Obtenemos la fecha de inicio y la fecha fin.
				var fh_Inicio = fnc_fecha_string( fnc_fecha_objeto( $("#txt_fh_InicioPermiso").val() + ( $("#ddl_tipo_permiso option:selected").val() == "1" ? " " + $("#ddl_horas_inicio option:selected").text() + ":" + $("#ddl_minutos_inicio option:selected").text() : "" ), true ), "yyyy-mm-dd HH:mm", true );
				
				var fh_Fin = fnc_fecha_string( fnc_fecha_objeto( ( $("#ddl_tipo_permiso option:selected").val() == "2" ? $("#txt_fh_FinPermiso").val() : $("#txt_fh_InicioPermiso").val() + ( $("#ddl_tipo_permiso option:selected").val() == "1" ? " " + ( $("#ddl_horas_inicio option:selected").text() * 1 + $("#txt_nu_HorasPermiso").val() * 1 ) + ":" + ( $("#ddl_minutos_inicio option:selected").text() * 1 + $("#txt_nu_MinutosPermiso").val() * 1 ) : "" ) ), true ), "yyyy-mm-dd HH:mm", true );
				
				//Validar que la fecha fin no exceda la fecha de inicio.
				if( $("#ddl_tipo_permiso option:selected").val() == "2" 
					|| ( $("#ddl_tipo_permiso option:selected").val() == "1" && fh_Inicio.split(" ")[0] == fh_Fin.split(" ")[0] ) )
				{
					//Armamos los parÃ¡metros.
					var parametros = "id_Empresa=" + $("#hid_id_Empresa").val() + "&method=";
					
					if( fnc_fecha_objeto( fh_Fin,true ) < fnc_fecha_objeto( fh_Inicio, true ) ) 
					{
						fnc_notificacion("La fecha fin debe ser mayor a la fecha de inicio.");
						return;
					}
					switch( $("#hid_accion").val() )
					{
						case "1": //Agregar.
									parametros += "PermisosHorario_Agregar&id_Recurso=" + $("#txt_id_Recurso").val()
										+ "&id_TipoPermiso=" + $("#ddl_tipo_permiso option:selected").val()
										+ "&nb_Permiso=" + fnc_codificar( $.trim( $("#txt_nb_Permiso").val() ) )
										+ ( $.trim( $("#txt_de_Permiso").val() ) == "" ? "" : "&de_Permiso=" + fnc_codificar( $.trim( $("#txt_de_Permiso").val() ) ) )
										+ "&fh_Inicio=" + fh_Inicio
										+ "&fh_Fin=" + fh_Fin
										+ ( $("#ddl_tipo_permiso option:selected").val() == "2" ? "" : "&nu_HorasPermiso=" + ( fnc_convertir_a_horas( $("#txt_nu_MinutosPermiso").val(), "m" ) * 1 + $("#txt_nu_HorasPermiso").val() * 1 ) );
									break;
						case "2": //Editar
									parametros += "PermisosHorario_Editar&id_Recurso=" + g_row.id_Recurso
										+ "&id_TipoPermiso=" + g_row.id_TipoPermiso
										+ "&id_PermisoHorario=" + g_row.id_PermisoHorario
										+ "&nb_Permiso=" + fnc_codificar( $.trim( $("#txt_nb_Permiso").val() ) )
										+ ( $.trim( $("#txt_de_Permiso").val() ) == "" ? "" : "&de_Permiso=" + fnc_codificar( $.trim( $("#txt_de_Permiso").val() ) ) )
										+ "&fh_Inicio=" + fh_Inicio
										+ "&fh_Fin=" + fh_Fin
										+ ( $("#ddl_tipo_permiso option:selected").val() == "2" ? "" : "&nu_HorasPermiso=" + ( fnc_convertir_a_horas( $("# � write � 2 java/io/Writer �
 � �6txt_nu_MinutosPermiso").val(), "m" ) * 1 + $("#txt_nu_HorasPermiso").val() * 1 ) );
									break;
						case "3": //Eliminar
									parametros += "PermisosHorario_Eliminar&id_Recurso=" + g_row.id_Recurso
										+ "&id_TipoPermiso=" + g_row.id_TipoPermiso
										+ "&id_PermisoHorario=" + g_row.id_PermisoHorario;
									break;
					}
		
					//Guardamos, Editamos, Eliminamos.
					fnc_peticionAjax({
						url: "./application/catalogos.cfc",
						parametros: parametros,
						f_callback: function(){
							//Recargamos el grid.
							ggrids.recargar("grid_permisos_recursos");
							
							if( $("#hid_accion").val() == "2" || $("#hid_accion").val() == "3" )
								fnc_catalogos_mostrar_ocultar( "div_grid", "div_guardar_editar_eliminar" );
							else
								fnc_limpiar_campos_permisos_recuros();
						}
					});
				}
				else
					fnc_mostrar_aviso( "gd_avisos", "Excede el total de horas para este d&iacute;a.", 5 );
			}
			else
				fnc_notificacion("La informaci&oacute;n acerca de <b>Horas del Permiso</b> es requerida.");
		}
	}
	
	//FunciÃ³n para cuando se cambia de Tipo de Permiso.
	function fnc_tipo_permiso(p_id_TipoPermiso)
	{
		if( p_id_TipoPermiso == 1 )
		{
			$("#tr_InicioPermiso").show();
			$("#tr_HorasPermiso").show();
			$("#ddl_horas_inicio").show();
			$("#ddl_minutos_inicio").show();
			$("#ddl_horas_fin").show();
			$("#ddl_minutos_fin").show();
			$("#span_HorasInicio").show();
			$("#span_HorasFin").show()
			
			$("#tr_FinPermiso").hide();
		}
		else if( p_id_TipoPermiso == 2 )
		{
			$("#tr_InicioPermiso").show();
			$("#tr_HorasPermiso").hide();
			$("#ddl_horas_inicio").hide();
			$("#ddl_minutos_inicio").hide();
			$("#ddl_horas_fin").hide();
			$("#ddl_minutos_fin").hide();
			$("#span_HorasInicio").hide();
			$("#span_HorasFin").hide()
			
			$("#tr_FinPermiso").show();
		}
	}
	
	//FunciÃ³n para buscar mÃ³dulos.
	function fnc_buscar_permisos_recursos()
	{
		//Armamos los parÃ¡metros.
		var parametros="";
		/*if($("#ddl_recursosBuscar option:selected").val() != "0")
			parametros += "&id_Recurso=" + $("#ddl_recursosBuscar option:selected").val();*/
			
		if(fnc_codificar( $.trim( $("#txt_recursosBuscar_nu_Empleado").val() ) ) != "")
			parametros += "&nu_Empleado=" + $("#txt_recursosBuscar_nu_Empleado").val();
			
		if(fnc_codificar( $.trim( $("#txt_recursosBuscar_nb_NombreRecurso").val() ) ) != "")
			parametros += "&nb_NombreRecurso=" + $("#txt_recursosBuscar_nb_NombreRecurso").val();
		
		if(fnc_codificar( $.trim( $("#txt_recursosBuscar_nb_ApellidoPaterno").val() ) ) != "")
			parametros += "&nb_ApellidoPaterno=" + $("#txt_recursosBuscar_nb_ApellidoPaterno").val();

		ggrids.recargar("grid_permisos_recursos", parametros);
	}
	function fnc_abrir_grid_recurso()
	{
		fnc_catalogos_mostrar_ocultar('div_guardar_editar_eliminar','div_seleccionar_recurso');
	 	ggrids.crear("grid_recursos");
	}
	function fnc_div_recurso_regresar()
	{	
		fnc_catalogos_mostrar_ocultar('div_guardar_editar_eliminar','div_seleccionar_recurso');
		$("#buscar_recurso").focus();
	}
	
	function fnc_seleccionar_recurso()
	{
	
		
		var row = ggrids.obtener_renglon("grid_recursos");
		// atributos de row nb_NombreCompleto,nb_Area,de_Puesto,id_Empresa,id_Recurso,id_Puesto,id_Area
		if(row)
		{		
				ggrids.recargar("grid_recursos");
				$("#txt_id_Recurso").val(row.id_Recurso);
				$("#txt_nb_Recurso").val(row.nb_NombreCompleto);
				fnc_catalogos_mostrar_ocultar('div_guardar_editar_eliminar','div_seleccionar_recurso');
				$("#ddl_requerimientos").focus();
		}
		else
		{
			// si no se selecciono ningun gerente mostrar el cuadro de notificacion;
			fnc_notificacion("Debe seleccionar un Recurso");
		}
	}
</script>

	<center>
        <div style="margin-left: 2%; margin-right: 2%; width: 96%;">
            <div class="ui-widget-header catalogos_cabecero titulo_opcion">REGISTRO DE PERMISOS</div>
            <div class="catalogos_cabecero_contenido" id="div_grid">
            	<div class="ui-state-default catalogos_busqueda" id="div_busqueda">
                	<table style="width: 100%;">
                    	<tr>
													<td style="text-align: right;"><span>No. Empleado Coppel:</span> </td>
													<td style="text-align: left;">
														<input type="text" id="txt_recursosBuscar_nu_Empleado" class="solo_numeros" onchange="fnc_buscar_permisos_recursos();" maxlength="15" />
													</td>
                        	
													<td style="text-align: left;"><span>Nombre:</span> </td>
													<td style="text-align: left;">
													<input type="text" id="txt_recursosBuscar_nb_NombreRecurso" class="input_text" onchange="fnc_buscar_permisos_recursos();" maxlength="15" />
													</td>
												<td style="text-align: left;"><span>Apellido Paterno:</span> </td>
												<td style="text-align: left;">
												<input type="text" id="txt_recursosBuscar_nb_ApellidoPaterno" class="input_text" onchange="fnc_buscar_permisos_recursos();" maxlength="15" />
												</td>
                        </tr>
                    </table>
                </div>
                <div id="grid_permisos_recursos" class="ggrids" titulo="Listado de Permisos por Recurso"
                	url="application/catalogos.cfc?method=PermisosHorariosRecurso_ListarJSON&id_Empresa= � $class$coldfusion$tagext$io$OutputTag coldfusion.tagext.io.OutputTag � � <	  � coldfusion/tagext/io/OutputTag � 
doStartTag ()I � �
 � � REQUEST � SESSION � 
ID_EMPRESA � _resolveAndAutoscalarize � X
  � _String &(Ljava/lang/Object;)Ljava/lang/String; � � coldfusion/runtime/Cast �
 � � doAfterBody � �
 � � doEndTag � � coldfusion/tagext/QueryLoop �
 � � doCatch (Ljava/lang/Throwable;)V � �
 � � 	doFinally � 
 � �k"
                	autowidth="true" sortname="id_TipoPermiso" sortorder="ASC"
                    loadComplete="fnc_autoajustar_ancho_busqueda('grid_permisos_recursos','div_busqueda')">
                    
                    
										 <columna nom_columna="nu_Empleado">No. Empleado Coppel</columna>
                    <columna nom_columna="nb_Permiso">Permiso</columna>
                    <columna nom_columna="de_Permiso">Detalle Permiso</columna>
                    <columna nom_columna="nb_NombreCompletoRecurso">Recurso</columna>
                    <columna nom_columna="fh_Inicio" align="center">Fecha Inicio</columna>
                    <columna nom_columna="fh_Fin" align="center">Fecha Fin</columna>
                    <columna nom_columna="nu_HorasPermiso" align="center">Horas del Permiso</columna>
                    
                    
                    <columna nom_columna="id_Empresa" visible="false"></columna>
                    <columna nom_columna="id_Recurso" visible="false"></columna>
                    <columna nom_columna="id_TipoPermiso" visible="false"></columna>
                    <columna nom_columna="id_PermisoHorario" visible="false"></columna>
                    
                    
                    <accion accion="agregar" evento="fnc_permisos_recursos_agregar_editar(1);"></accion>
                    <accion accion="editar" evento="fnc_permisos_recursos_agregar_editar(2);"></accion>
                    <accion accion="eliminar" evento="fnc_permisos_recursos_agregar_editar(3);"></accion>
                </div>
            </div>
            
            
            <div id="div_guardar_editar_eliminar" class="catalogos_guardar_editar">
            	<div class="ui-widget-header catalogos_guardar_editar_cabecero" id="div_titulo_agregar_editar"></div>
                <div class="catalogos_guardar_editar_cabecero_contenido" id="div_permisos_recursos_crud">
                    <table> 
                    	<tr>
                        	<td style="text-align:right"><span>Recurso :</span></td> 
                                <input type="hidden" id="txt_id_Recurso"/>
                            <td>
                            	<input type="text" class="input_text obligatorio" disabled="disabled" id="txt_nb_Recurso" acercade="Recurso" style="width: 97%;" maxlength="120"  />
                            </td>
                            
        
                            <td>
                            	<span><a id="buscar_recurso"  href="javascript:fnc_abrir_grid_recurso();"><img src="./images/lupa.gif" alt="Buscar Recurso" width="30" height="30" border="0" /></a></span>
                            </td>
                        </tr>
                        <tr>
                        	<td style="text-align: right;"><span>Tipo de Permiso:</span> </td>
                            <td style="text-align: left;">
                            	<select id="ddl_tipo_permiso" class="input_text" onChange="fnc_tipo_permiso(this.value);" 
                                        style="width: 305px; height:25px;" >
                                	 � cfoutput � query � TiposPermiso_Listado.rs � _validateTagAttrValue \(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String; � �
  � setQuery � 2
 � � 6
                                    	<option value=" � RS � ID_TIPOPERMISO � D(Lcoldfusion/runtime/Variable;[Ljava/lang/String;)Ljava/lang/Object; � �
  � "> � DE_TIPOPERMISO � /</option>
                                     �:
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right;">Descripci&oacute;n del Permiso: </td>
                            <td>
                            	<input class="input_text obligatorio" acercade="Descripci&oacute;n del Permiso" type="text" id="txt_nb_Permiso" 
                                       maxlength="150" style="width: 300px;" /> <span> *</span>
                            </td>
                        </tr> 
                        <tr>
                            <td style="text-align: right;">Detalle del Permiso: </td>
                            <td style="text-align: left;">
                            	<textarea class="input_text" id="txt_de_Permiso" style="width: 300px; height: 80px;"></textarea>
                            </td>
                        </tr> 
                        <tr id="tr_InicioPermiso">
                            <td style="text-align: right;">Inicio del Permiso: </td>
                            <td id="td_HorasInicio" style="text-align: left;">
                            	<input type="text" class="input_text txt_datepicker" id="txt_fh_InicioPermiso"
                                	readonly="readonly"	value=" � Now "()Lcoldfusion/runtime/OleDateTime; � �
  � 
dd/mm/yyyy � 
DateFormat 6(Ljava/util/Date;Ljava/lang/String;)Ljava/lang/String; � �
  � �" />
                                <select id="ddl_horas_inicio" class="input_text" title="Elija una hora">
                                     � 1 � _double (Ljava/lang/String;)D � �
 � � 23 � 8 � _Object (D)Ljava/lang/Object; � �
 � � P(Ljava/lang/String;Lcoldfusion/runtime/LocalScope;)Lcoldfusion/runtime/Variable;  �
  � set � f coldfusion/runtime/Variable �
 � � *
                                         � .
                                              _autoscalarize 1(Lcoldfusion/runtime/Variable;)Ljava/lang/Object;
 @$       _compare (Ljava/lang/Object;D)D	
 
 A
                                                <option value=" ">0 7</option>
                                             &
                                     CFLOOP checkRequestTimeout 2
  _checkCondition (DDD)Z
 
                                </select>
                                <span id="span_HorasInicio">:</span>
                                <select id="ddl_minutos_inicio" class="input_text" title="Elija los minutos">
                                     5 59! 0#�
                                </select>
                            </td>
                        </tr> 
                        <tr id="tr_FinPermiso" style="display: none;">
                            <td style="text-align: right;">Fin del Permiso: </td>
                            <td id="td_HorasFin" style="text-align: left;">
                            	<input type="text" class="input_text txt_datepicker" id="txt_fh_FinPermiso"
                                	readonly="readonly"	value="% �" />
                                <select id="ddl_horas_fin" class="input_text" title="Elija una hora">
                                    ' 
                                </select>
                                <span id="span_HorasFin">:</span>
                                <select id="ddl_minutos_fin" class="input_text" title="Elija los minutos">
                                    )
                                </select>
                            </td>
                        </tr>
                        <tr id="tr_HorasPermiso">
                        	<td style="text-align: right;">Horas del Permiso: </td>
                            <td style="text-align: left;">
                            	<input type="text" id="txt_nu_HorasPermiso" class="input_text solo_numeros"
                                	style="width: 40px; text-align: center;" maxlength="1" />
                                <span>:</span>
                                <input type="text" id="txt_nu_MinutosPermiso" class="input_text solo_numeros"
                                	style="width: 40px; text-align: center;" maxlength="2" />
                            </td>
                        </tr>
                    </table>
                    
                    <div style="margin-top: 10px;">
                    	<button class="boton" title="Guardar" id="btn_guardar" onClick="fnc_permisos_recursos_crud();">Guardar</button>
                        <button class="boton" title="Regresar" onClick="fnc_catalogos_mostrar_ocultar( 'div_grid', 'div_guardar_editar_eliminar' );">Regresar</button>
                    </div>
                </div>
            </div>
             
            <div id="div_seleccionar_recurso"  style="padding: 1px; margin-top: 1px;"  class="catalogos_guardar_editar">
                <div id="div_titulo_gerente" class="ui-widget-header catalogos_guardar_editar_cabecero">SELECCIONE RECURSO</div>
                
                <div class="catalogos_guardar_editar_cabecero_contenido">
                    <div class="ui-state-default catalogos_busqueda" id="div_busqueda_recursos">
                        <table style="width: 100%;">
                            <tr>
                             	<td style="text-align:left;" >
                                    <span>No. Empleado Coppel:</span>
                                    <input type="text" id="txt_no_Empleado" class="input_text solo_numeros" onChange="fnc_buscar_recursos();" maxlength="50" />
                                </td>
                                <td style="text-align:left;" >
                                    <span>Nombre: </span>
                                    <input type="text" id="txt_nb_RecursoBuscar" class="input_text" onChange="fnc_buscar_recursos();" maxlength="50" />
                                </td>
                                <td style="text-align:left;" >
                                    <span>Apellido Paterno: </span>
                                    <input type="text" id="txt_apellidoPaterno_busqueda" class="input_text" maxlength="50" onChange="fnc_buscar_recursos();" />
                                </td>	
                            </tr>
                        </table>
                    </div>
					
                    <div id="grid_recursos" class="ggrids" 
                    	url="application/catalogos_danielf.cfc?method=Recursos_Listar_Gerentes_JSON&id_Empresa=+
n" 
                        autocrear="false"
                        titulo="Listado de Recursos"
                        ancho="700" 
                        ondblClickRow="fnc_seleccionar_recurso();" 
                        loadComplete="fnc_autoajustar_ancho_busqueda('grid_recursos','div_busqueda_recursos');">
                        
                        
                        
                        
                        
                        <columna nom_columna="nu_Empleado" >No. Empleado Coppel</columna>
                        <columna nom_columna="nb_NombreCompleto">Nombre</columna>
                        <columna nom_columna="nb_Area">&Aacute;rea</columna>
                        <columna nom_columna="de_Puesto">Puesto</columna>
                        <columna nom_columna="nu_Telefono" visible="false">Tel&eacute;fono</columna>
                        <columna nom_columna="de_Email" visible="false">Correo</columna>
                        <columna nom_columna="nb_TipoRecurso" visible="false">Tipo de Recurso</columna>
                        
                        
                    
                        <columna nom_columna="nb_NombreRecurso" 	visible="false"></columna>
                        <columna nom_columna="nb_ApellidoPaterno" 	visible="false"></columna>
                        <columna nom_columna="nb_ApellidoMaterno" 	visible="false"></columna>
                        <columna nom_columna="id_Empresa" 			visible="false"></columna>
                        <columna nom_columna="id_Recurso" 			visible="false"></columna>
                        <columna nom_columna="id_Puesto" 			visible="false"></columna>
                        <columna nom_columna="sn_Activo" 			visible="false"></columna>
                        <columna nom_columna="id_Horario" 			visible="false"></columna>
                        <columna nom_columna="id_Area" 				visible="false"></columna>
                        <columna nom_columna="id_TipoRecurso" 		visible="false"></columna>
                        
                    </div>
                    
                    <div style="margin-top: 10px;">
                    	<button class="boton" title="Seleccionar" id="btn_guardar" onClick="fnc_seleccionar_recurso();">Seleccionar</button>
                        <button class="boton" title="Regresar" onClick="fnc_div_recurso_regresar();">Regresar</button>
                    </div>
                </div>
              	
  
            </div> 
        </div>
    </center>
    
    <input type="hidden" id="hid_accion" />
    <input type="hidden" id="hid_fh_Actual" value="- P" />
    
    <div id="div_peticiones_ajax_ph" style="display: none;"></div>
/ metaData Ljava/lang/Object;12	 3 this "Lcfpermisosrecursos2ecfm202430168; LocalVariableTable Code <clinit> varscope "Lcoldfusion/runtime/VariableScope; locscope Lcoldfusion/runtime/LocalScope; getMetadata ()Ljava/lang/Object; runPage out Ljavax/servlet/jsp/JspWriter; value invoke0 "Lcoldfusion/tagext/lang/InvokeTag; output1  Lcoldfusion/tagext/io/OutputTag; mode1 t7 t8 Ljava/lang/Throwable; t9 t10 output2 mode2 t13 t14 t15 t16 output3 mode3 t19 t20 t21 t22 t23 D t25 t27 t29 output4 mode4 t32 t33 t34 t35 t36 t38 t40 t42 output5 mode5 t45 t46 t47 t48 output6 mode6 t51 t52 t53 t54 t55 t57 t59 t61 output7 mode7 t64 t65 t66 t67 t68 t70 t72 t74 output8 mode8 t77 t78 t79 t80 output9 mode9 t83 t84 t85 t86 output10 mode10 t89 t90 t91 t92 LineNumberTable java/lang/Throwable� 1                 ; <    � <   12       8   #     *� 
�   7       56   9  8   =     >� D� F�� D� �� tY� ^� {�4�   7       56      8   Q     *+,� **+,� � **+,� � �   7        56     :;    <=  >? 8   "     �4�   7       56   @? 8  �  ]  	r*� $� *L*� .N*0� 4*+6� :*� F-� J� L:*� P*� P**R� TYVS� Z\� ^Y`S� d� hj� mo� r� tY� ^YvSYxS� {� � �� �� �+�� �+�� �*� �-� J� �:*C� P� �� �Y6� &+*�� TY�SY�S� �� �� �� ����� �� :� #�� � #:� �� � :	� 	�:
� ��
+�� �*� �-� J� �:*s� P���� Ƕ �� �� �Y6� W+̶ �+**� � TY�SY�S� Ӹ �� �+ն �+**� � TY�SY�S� Ӹ �� �+ٶ �� ����� �� :� #�� � #:� �� � :� �:� ��+۶ �*� �-� J� �:*�� P� �� �Y6� '+*�� P**�� P*� �� � �� ����� �� :� #�� � #:� �� � :� �:� ��+� �� �9� �9� �9� �M*� �:,� ��*+�� :*� �-� J� �:*�� P� �� �Y6� �*+� :**� ���� 9+� �+**� �� �� �+� �+**� �� �� �+� �� 5+� �+**� �� �� �+ն �+**� �� �� �+� �*+�� :� ���q� �� : � # �� � #:!!� �� � :"� "�:#� ��#*+� :c\9� �M,� ������+� � � �9$"� �9&$� �9((� �M*� �:**,� ��*+�� :*� �-� J� �:+*�� P+� �+� �Y6,� �*+� :**� ���� 9+� �+**� �� �� �+� �+**� �� �� �+� �� 5+� �+**� �� �� �+ն �+**� �� �� �+� �*+�� :+� ���q+� �� :-� #-�� � #:.+.� �� � :/� /�:0+� ��0*+� :($c\9(� �M*,� ��$(&����+&� �*� �-� J� �:1*�� P1� �1� �Y62� '+*�� P**�� P*� �� � �1� ����1� �� :3� #3�� � #:414� �� � :5� 5�:61� ��6+(� �� �97� �99� �9;;� �M*� �:==,� ��*+�� :*� �-� J� �:>*�� P>� �>� �Y6?� �*+� :**� ���� 9+� �+**� �� �� �+� �+**� �� �� �+� �� 5+� �+**� �� �� �+ն �+**� �� �� �+� �*+�� :>� ���q>� �� :@� #@�� � #:A>A� �� � :B� B�:C>� ��C*+� :;7c\9;� �M=,� ��7;9����+*� � � �9D"� �9F$� �9HH� �M*� �:JJ,� ��*+�� :*� �-� J� �:K*�� PK� �K� �Y6L� �*+� :**� ���� 9+� �+**� �� �� �+� �+**� �� �� �+� �� 5+� �+**� �� �� �+ն �+**� �� �� �+� �*+�� :K� ���qK� �� :M� #M�� � #:NKN� �� � :O� O�:PK� ��P*+� :HDc\9H� �MJ,� ��DHF����+,� �*� �	-� J� �:Q*�� PQ� �Q� �Y6R� &+*�� TY�SY�S� �� �� �Q� ����Q� �� :S� #S�� � #:TQT� �� � :U� U�:VQ� ��V+.� �*� �
-� J� �:W*� PW� �W� �Y6X� '+*� P**� P*� �� � �W� ����W� �� :Y� #Y�� � #:ZWZ� �� � :[� [�:\W� ��\+0� �� < � � �� � � �� � �	� � �	� �	�		�J�������J���������������=I�CFI�=X�CFX�IUX�X]X��gs�mps��g��mp��s����������������������������<w��}���<w��}�����������������������������������U�
�
�U���
��y�������y���������������		=	I�	C	F	I�		=	X�	C	F	X�	I	U	X�	X	]	X� 7  , Q  	r56    	rAB   	rC2   	r + ,   	rDE   	rFG   	rH    	rI2   	rJK   	rLK 	  	rM2 
  	rNG   	rO    	rP2   	rQK   	rRK   	rS2   	rTG   	rU    	rV2   	rWK   	rXK   	rY2   	rZ[   	r\[   	r][   	r^    	r_G   	r`    	ra2    	rbK !  	rcK "  	rd2 #  	re[ $  	rf[ &  	rg[ (  	rh  *  	riG +  	rj  ,  	rk2 -  	rlK .  	rmK /  	rn2 0  	roG 1  	rp  2  	rq2 3  	rrK 4  	rsK 5  	rt2 6  	ru[ 7  	rv[ 9  	rw[ ;  	rx  =  	ryG >  	rz  ?  	r{2 @  	r|K A  	r}K B  	r~2 C  	r[ D  	r�[ F  	r�[ H  	r�  J  	r�G K  	r�  L  	r�2 M  	r�K N  	r�K O  	r�2 P  	r�G Q  	r�  R  	r�2 S  	r�K T  	r�K U  	r�2 V  	r�G W  	r�  X  	r�2 Y  	r�K Z  	r�K [  	r�2 \�   � L  4  4  W  ^  t    �C �C �C �C;s\t\t[ttt~t s��!������o�v�}��������������"�"�!�7�7�6��������o�������+�3�F�F�E�\�\�[�|�|�{�������t�+������W�W�[�O�O�G� ���������(�(�'�>�>�=�^�^�]�s�s�r�V���������
��h�p���������������������������h�9�S��������]�			!			�              