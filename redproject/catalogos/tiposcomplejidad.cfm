����  - � 
SourceFile KC:\ColdFusion9\wwwroot\ferrominio\redproject\catalogos\tiposcomplejidad.cfm  cftiposcomplejidad2ecfm800846334  coldfusion/runtime/CFPage  <init> ()V  
  	 com.macromedia.SourceModTime  J1 coldfusion/runtime/CfJspPage  pageContext #Lcoldfusion/runtime/NeoPageContext;  	   getOut ()Ljavax/servlet/jsp/JspWriter;   javax/servlet/jsp/PageContext 
   parent Ljavax/servlet/jsp/tagext/Tag;  	   com.adobe.coldfusion.*  bindImportPath (Ljava/lang/String;)V   !
  " 
 $ _whitespace %(Ljava/io/Writer;Ljava/lang/String;)V & '
  ( .class$coldfusion$tagext$html$ajax$AjaxProxyTag Ljava/lang/Class; (coldfusion.tagext.html.ajax.AjaxProxyTag , forName %(Ljava/lang/String;)Ljava/lang/Class; . / java/lang/Class 1
 2 0 * +	  4 _initTag P(Ljava/lang/Class;ILjavax/servlet/jsp/tagext/Tag;)Ljavax/servlet/jsp/tagext/Tag; 6 7
  8 (coldfusion/tagext/html/ajax/AjaxProxyTag : _setCurrentLineNo (I)V < =
  > cfajaxproxy @ cfc B APPLICATION D java/lang/String F RF H _resolve 9(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/Object; J K
  L getPath N java/lang/Object P app R catalogos_flavio T _invoke K(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object; V W
  X _String &(Ljava/lang/Object;)Ljava/lang/String; Z [ coldfusion/runtime/Cast ]
 ^ \ _validateTagAttrValue \(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String; ` a
  b setCfc d !
 ; e jsclassname g jsApp_catalogos_flavio i setJsclassname k !
 ; l 	hasEndTag (Z)V n o coldfusion/tagext/GenericTag q
 r p _emptyTcfTag !(Ljavax/servlet/jsp/tagext/Tag;)Z t u
  v�
<script type="text/javascript">
	//Variables globales.
	var g_row;
	var jasApp  = new jsApp_catalogos_flavio();
	
	//Función para abrir la opción de agregar y/o editar.
	function fnc_tiposcomplejidad_agregar_editar(p_accion)
	{
		if( p_accion == 1 ) //Agregar.
		{
			$("#div_titulo_agregar_editar").html("NUEVO TIPO COMPLEJIDAD");
			$("#btn_guardar").attr("title", "Guardar").text("Guardar");
			
			//Nos aseguramos de desbloquear los input.      
			$("#txt_de_TipoComplejidad").removeAttr("disabled");
			
			fnc_catalogos_mostrar_ocultar( "div_grid", "div_guardar_editar_eliminar" );
		}
		else if( p_accion == 2 || p_accion == 3 ) //Editar o Eliminar.
		{
			//Validamos que se haya seleccionado un renglón.
			var row = ggrids.obtener_renglon("grid_tiposcomplejidad");
			
			if( row )
			{	
				g_row = row;

				//Llenamos los campos.
				$("#txt_de_TipoComplejidad").val( row.de_TipoComplejidad );

				//Valdiar si debemos bloquear o desbloquear los input.
				if( p_accion == 3 )
				{
					$("#div_titulo_agregar_editar").html("ELIMINAR TIPO DE COMPLEJIDAD");
					$("#btn_guardar").attr("title", "Eliminar").text("Eliminar");
					$("#txt_de_TipoComplejidad").attr("disabled","disabled");
				}
				else
				{
					$("#div_titulo_agregar_editar").html("EDITAR TIPO DE COMPLEJIDAD");
					$("#btn_guardar").attr("title", "Guardar").text("Guardar");
					$("#txt_de_TipoComplejidad").removeAttr("disabled");
				}
				
				fnc_catalogos_mostrar_ocultar( "div_grid", "div_guardar_editar_eliminar" );				
			}
			else
				fnc_notificacion("Debe seleccionar un tipo de complejidad.");
				
		}
		
		$("#hid_accion").val( p_accion );
		
		//Limpiamos los campos.
		if( p_accion == 1 )
		{
			$("#txt_de_TipoComplejidad").val("");			
		}
		
		$("#txt_de_TipoComplejidad").focus();
	}
	
	//Función para guardar y/o editar.
	function fnc_tiposcomplejidad_crud()
	{	//Validamos los campos obligaorios.
		if( fnc_validar_campos_obligatorios("div_TiposComplejidad_crud") )
		{
			//Armamos los parámetros.
			var accion = new Array("TiposComplejidad_Agregar","TiposComplejidad_Editar","TiposComplejidad_Eliminar");
			var parametros = "id_Empresa=" + $("#hid_id_Empresa").val() + "&method="+ accion[$("#hid_accion").val()-1];
			
			// VALORES DE LOS CAMPOS DEL DIV PARA AGREGAR EDITAR ELIMINAR
			var com = fnc_codificar(  $.trim( $("#txt_de_TipoComplejidad").val())  );
			
			switch( $("#hid_accion").val() )
			{
				case "1": //Agregar.
							parametros+=
							"&de_TipoComplejidad="+ com;
							
							break;
							
				case "2": //Editar
							parametros+=
							"&id_TipoComplejidad="+ g_row.id_TipoComplejidad+
							"&de_TipoComplejidad="+ com;	
							
							break;
				case "3" : //Eliminar
							parametros+=
							"&id_TipoComplejidad="+ g_row.id_TipoComplejidad;
							
							break;
			}
		 	fnc_peticionAjax(
			 {
				url: "./application/catalogos_flavio.cfc",
				parametros: parametros,
				f_callback: function()
				{
					//Actualizamos el grid.
					ggrids.recargar("grid_tiposcomplejidad");
				    // si es modificar o eliminar
					if( $("#hid_accion").val() == "2" || $("#hid_accion").val() == "3" )
					{
						fnc_catalogos_mostrar_ocultar( "div_grid", "div_guardar_editar_eliminar" );
					}
					else
					{
						$("#txt_de_TipoComplejidad").val("");
						$("#txt_de_TipoComplejidad").focus();
					}	
				}
			});
		}
	}
	
	//Función para buscar clientes.
	function fnc_buscar_tipos_complejidad()
	{
		//Armamos los parámetros.
		var parametros = "";				
			if($("#txt_descripcion").val() != "")
				parametros = parametros + "&de_TipoComplejidad=" + $.trim( $("#txt_descripcion").val());				
		
		ggrids.recargar("grid_tiposcomplejidad", parametros);
	}
</script>
	<center>
        <div style="margin-left:5%; margin-right:5%; width:90%">
            <div class="ui-widget-header catalogos_cabecero titulo_opcion">CAT&Aacute;LOGO DE TIPOS DE COMPLEJIDAD</div>
            <div class="catalogos_cabecero_contenido" id="div_grid">
            	<div id="div_busqueda" class="ui-state-default catalogos_busqueda">
                	<table style="width: 100%;">
                    	<tr>                        	
                            <td style="text-align: right;">Tipo de Complejidad: </td>
                            <td><input type="text" id="txt_descripcion" class="input_text" onChange="fnc_buscar_tipos_complejidad();" /></td>                            
                        </tr>
                    </table>
                </div>
                <div id="grid_tiposcomplejidad" class="ggrids" url="application/catalogos_flavio.cfc?method=tiposcomplejidad_Listar_JSON&id_Empresa= x write z ! java/io/Writer |
 } { $class$coldfusion$tagext$io$OutputTag coldfusion.tagext.io.OutputTag �  +	  � coldfusion/tagext/io/OutputTag � 
doStartTag ()I � �
 � � REQUEST � SESSION � 
ID_EMPRESA � _resolveAndAutoscalarize � K
  � doAfterBody � �
 � � doEndTag � � coldfusion/tagext/QueryLoop �
 � � doCatch (Ljava/lang/Throwable;)V � �
 � � 	doFinally � 
 � �[" titulo="Listado de Tipos de Complejidad"
                	ancho="500" sortname="id_TipoComplejidad" autowidth="false" loadComplete="fnc_autoajustar_ancho_busqueda('grid_tiposcomplejidad','div_busqueda')">
                    <columna nom_columna="id_TipoComplejidad" visible="false"></columna>
                    <columna nom_columna="de_TipoComplejidad">Tipo de Complejidad</columna>

                    <columna nom_columna="id_Empresa" visible="false"></columna>
                    <accion accion="agregar" evento="fnc_tiposcomplejidad_agregar_editar(1);"></accion>
                    <accion accion="editar" evento="fnc_tiposcomplejidad_agregar_editar(2);"></accion>
                    <accion accion="eliminar" evento="fnc_tiposcomplejidad_agregar_editar(3);"></accion>
                </div>
            </div>
            
            
            <div id="div_guardar_editar_eliminar" class="catalogos_guardar_editar">
            	<div class="ui-widget-header catalogos_guardar_editar_cabecero" id="div_titulo_agregar_editar"></div>
                <div class="catalogos_guardar_editar_cabecero_contenido" id="div_TiposComplejidad_crud">
                    <table id="table_clientes_guardar_editar"> 
                        <tr>
                            <td style="text-align: right;"><span>Tipo de Complejidad: </span></td>
                            <td><input class="input_text obligatorio" type="text" acercade="Tipo De Complejidad" id="txt_de_TipoComplejidad" maxlength="120" /></td>
                        </tr>                       
                    </table>                    
                    <div style="margin-top: 10px;">
                    	<button class="boton" title="Guardar" id="btn_guardar" onClick="fnc_tiposcomplejidad_crud();">Guardar</button>
                        <button class="boton" title="Regresar" onClick="fnc_catalogos_mostrar_ocultar( 'div_grid', 'div_guardar_editar_eliminar' );">Regresar</button>
                    </div>
                </div>
            </div>
        </div>
    </center>
    <input type="hidden" id="hid_accion" />

 � metaData Ljava/lang/Object; � �	  � &coldfusion/runtime/AttributeCollection � ([Ljava/lang/Object;)V  �
 � � this "Lcftiposcomplejidad2ecfm800846334; LocalVariableTable Code <clinit> getMetadata ()Ljava/lang/Object; runPage out Ljavax/servlet/jsp/JspWriter; value 
ajaxproxy0 *Lcoldfusion/tagext/html/ajax/AjaxProxyTag; output1  Lcoldfusion/tagext/io/OutputTag; mode1 I t7 t8 Ljava/lang/Throwable; t9 t10 LineNumberTable java/lang/Throwable � 1       * +     +    � �        �   #     *� 
�    �        � �    �   �   =     -� 3� 5�� 3� �� �Y� Q� �� ��    �        � �    � �  �   "     � ��    �        � �    � �  �  � 
   *� � L*� N*� #*+%� )*� 5-� 9� ;:*� ?AC*� ?**E� GYIS� MO� QYSSYUS� Y� _� c� fAhj� c� m� s� w� �+y� ~*� �-� 9� �:* �� ?� s� �Y6� &+*�� GY�SY�S� �� _� ~� ����� �� :� #�� � #:� �� � :	� 	�:
� ��
+�� ~�  � � � � � � � � � � � � � � � � � � � � � � � �  �   p    � �     � �    � �         � �    � �    � �    � �    � �    � � 	   � � 
 �   .  P  U  8  8  k    � � � � � � � �              