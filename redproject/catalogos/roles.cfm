����  - � 
SourceFile @C:\ColdFusion9\wwwroot\ferrominio\redproject\catalogos\roles.cfm cfroles2ecfm109307670  coldfusion/runtime/CFPage  <init> ()V  
  	 com.macromedia.SourceModTime  J1 coldfusion/runtime/CfJspPage  pageContext #Lcoldfusion/runtime/NeoPageContext;  	   getOut ()Ljavax/servlet/jsp/JspWriter;   javax/servlet/jsp/PageContext 
   parent Ljavax/servlet/jsp/tagext/Tag;  	   com.adobe.coldfusion.*  bindImportPath (Ljava/lang/String;)V   !
  "�

<script type="text/javascript">
	//Variables globales.
	var g_row;
	
	//FunciÃ³n para abrir la opciÃ³n de agregar, editar o eliminar.
	function fnc_roles_agregar_editar(p_accion)
	{
		if( p_accion == 1 ) //Agregar.
		{
			$("#div_titulo_agregar_editar").html("NUEVO ROL");
			$("#btn_guardar").attr("title", "Guardar").text("Guardar");
			
			//Nos aseguramos de desbloquear los input.
			$("#txt_de_Rol, #chk_GestionaProyectos").removeAttr("disabled");
			
			fnc_catalogos_mostrar_ocultar( "div_grid", "div_guardar_editar_eliminar" );
		}
		else if( p_accion == 2 || p_accion == 3 ) //Editar o Eliminar.
		{
			//Validamos que se haya seleccionado un renglÃ³n.
			var row = ggrids.obtener_renglon("grid_roles")

			if( row )
			{	
				g_row = row;

				//Llenamos los campos.
				$("#txt_de_Rol").val( row.de_Rol);
				$("#chk_GestionaProyectos")[0].checked = ( row.sn_GestionaProyectos == "1" ? true : false );
				
				//Valdiar si debemos bloquear o desbloquear los input.
				if( p_accion == 3 )
				{
					$("#div_titulo_agregar_editar").html("ELIMINAR ROL");
					$("#btn_guardar").attr("title", "Eliminar").text("Eliminar");
					$("#txt_de_Rol, #chk_GestionaProyectos").attr("disabled", "disabled");
				}
				else
				{
					$("#div_titulo_agregar_editar").html("EDITAR ROL");
					$("#btn_guardar").attr("title", "Guardar").text("Guardar");
					$("#txt_de_Rol, #chk_GestionaProyectos").removeAttr("disabled");
				}
				
				fnc_catalogos_mostrar_ocultar( "div_grid", "div_guardar_editar_eliminar" );
			}
			else
				fnc_notificacion("Debe seleccionar un Rol.");
		}
		
		$("#hid_accion").val( p_accion );
		
		//Limpiamos los campos.
		if( p_accion == 1 )
		{
			$("#txt_de_Rol").val("");
			$("#chk_GestionaProyectos")[0].checked = false;
		}
		
		$("#txt_de_Rol").focus();
	}
	
	//FunciÃ³n para guardar, editar o eliminar.
	function fnc_roles_crud()
	{
		//Validamos los campos obligaorios.
		if( fnc_validar_campos_obligatorios("div_roles_crud") )
		{
			//Armamos los parÃ¡metros.
			var parametros = "id_Empresa=" + $("#hid_id_Empresa").val() + "&method=";
			
			switch( $("#hid_accion").val() )
			{
				case "1": //Agregar.
							parametros += "Roles_Agregar&de_Rol=" + fnc_codificar( $.trim( $("#txt_de_Rol").val() ) )
								+ "&sn_GestionaProyectos=" + ( $("#chk_GestionaProyectos").is(":checked") ? 1 : 0 );
							break;
				case "2": //Editar
							parametros += "Roles_Editar&id_Rol=" + g_row.id_Rol + "&de_Rol=" + g_row.de_Rol 
								+ "&de_RolNuevo=" + fnc_codificar( $.trim( $("#txt_de_Rol").val() ) )
								+ "&sn_GestionaProyectos=" + ( $("#chk_GestionaProyectos").is(":checked") ? 1 : 0 );
							break;
				case "3": //Eliminar
							parametros += "Roles_Eliminar&id_Rol=" + g_row.id_Rol;
							break;
			}
			
			//Guardamos, Editamos, Eliminamos.
			fnc_peticionAjax({
				url: "./application/catalogos.cfc",
				parametros: parametros,
				f_callback: function(){
					//Recargamos el grid.
					ggrids.recargar("grid_roles");
					
					if( $("#hid_accion").val() == "2" || $("#hid_accion").val() == "3" )
						fnc_catalogos_mostrar_ocultar( "div_grid", "div_guardar_editar_eliminar" );
					else
					{
						$("#txt_de_Rol").val("");
						$("#chk_GestionaProyectos")[0].checked = false;
					}
				}
			});
		}
	}
	
	//FunciÃ³n para buscar roles.
	function fnc_buscar_roles()
	{
		//Armamos los parÃ¡metros.
		var parametros = ( $.trim( $("#txt_de_RolBusqueda").val() ) == "" ? "" : "&de_Rol=" + fnc_codificar( $.trim( $("#txt_de_RolBusqueda").val() ) ) );
		
		//Recargamos el grid.
		ggrids.recargar("grid_roles", parametros);
	}
</script>

<center>
    <div style="width: 900px;">
        <div class="ui-widget-header catalogos_cabecero titulo_opcion">CAT&Aacute;LOGO DE ROLES</div>
        <div class="catalogos_cabecero_contenido" id="div_grid">
            <div class="ui-state-default catalogos_busqueda" id="div_busqueda">
                <table style="width: 100%;">
                    <tr>
                        <td style="text-align: left; padding-left:100px;"><span>Rol: </span>
                        <input type="text" id="txt_de_RolBusqueda" class="input_text" onChange="fnc_buscar_roles();" style="width: 200px;" /></td>
                    </tr>
                </table>
            </div>
            <div id="grid_roles" class="ggrids" paginado="10" titulo="Listado de Roles"
                url="application/catalogos.cfc?method=Roles_ListarJSON&id_Empresa= $ write & ! java/io/Writer (
 ) ' $class$coldfusion$tagext$io$OutputTag Ljava/lang/Class; coldfusion.tagext.io.OutputTag - forName %(Ljava/lang/String;)Ljava/lang/Class; / 0 java/lang/Class 2
 3 1 + ,	  5 _initTag P(Ljava/lang/Class;ILjavax/servlet/jsp/tagext/Tag;)Ljavax/servlet/jsp/tagext/Tag; 7 8
  9 coldfusion/tagext/io/OutputTag ; _setCurrentLineNo (I)V = >
  ? 	hasEndTag (Z)V A B coldfusion/tagext/GenericTag D
 E C 
doStartTag ()I G H
 < I REQUEST K java/lang/String M SESSION O 
ID_EMPRESA Q _resolveAndAutoscalarize 9(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/Object; S T
  U _String &(Ljava/lang/Object;)Ljava/lang/String; W X coldfusion/runtime/Cast Z
 [ Y doAfterBody ] H
 < ^ doEndTag ` H coldfusion/tagext/QueryLoop b
 c a doCatch (Ljava/lang/Throwable;)V e f
 c g 	doFinally i 
 < j	�&sn_ValidarDescripcion=0" 
                ancho="700"
                sortname="de_Rol" sortorder="ASC"
                loadComplete="fnc_autoajustar_ancho_busqueda('grid_roles','div_busqueda')">
                
                
                <columna nom_columna="de_Rol">Rol</columna>
                <columna nom_columna="de_GestionaProyectos" align="center" visible="false">Gestiona Proyectos</columna>
                
                
                <columna nom_columna="id_Empresa" visible="false"></columna>
                <columna nom_columna="id_Rol" visible="false"></columna>
                <columna nom_columna="sn_GestionaProyectos" visible="false"></columna>
                
                
                <accion accion="agregar" evento="fnc_roles_agregar_editar(1);"></accion>
                <accion accion="editar" evento="fnc_roles_agregar_editar(2);"></accion>
                <accion accion="eliminar" evento="fnc_roles_agregar_editar(3);"></accion>
            </div>
        </div>
        
        
        <div id="div_guardar_editar_eliminar" class="catalogos_guardar_editar">
            <div class="ui-widget-header catalogos_guardar_editar_cabecero" id="div_titulo_agregar_editar"></div>
            <div class="catalogos_guardar_editar_cabecero_contenido" id="div_roles_crud">
                <table align="center"> 
                    <tr>
                        <td style="text-align: right;">Rol:&nbsp;</td>
                        <td style="text-align: left;"><input class="input_text obligatorio" acercade="Rol" type="text" id="txt_de_Rol" maxlength="120" style="width: 200px;" /></td>
                    </tr> 
                    <tr style="display: none;">
                        <td style="text-align: right;">Gestiona Proyectos:&nbsp;</td>
                        <td style="text-align: left;"><input type="checkbox" id="chk_GestionaProyectos" /></td>
                    </tr> 
                </table>
                
                <div style="margin-top: 10px;">
                    <button class="boton" title="Guardar" id="btn_guardar" onClick="fnc_roles_crud();">Guardar</button>
                    <button class="boton" title="Regresar" onClick="fnc_catalogos_mostrar_ocultar( 'div_grid', 'div_guardar_editar_eliminar' );">Regresar</button>
                </div>
            </div>
        </div>
    </div>
</center>

<input type="hidden" id="hid_accion" />
 l metaData Ljava/lang/Object; n o	  p &coldfusion/runtime/AttributeCollection r java/lang/Object t ([Ljava/lang/Object;)V  v
 s w this Lcfroles2ecfm109307670; LocalVariableTable Code <clinit> getMetadata ()Ljava/lang/Object; runPage out Ljavax/servlet/jsp/JspWriter; value output0  Lcoldfusion/tagext/io/OutputTag; mode0 I t6 t7 Ljava/lang/Throwable; t8 t9 LineNumberTable java/lang/Throwable � 1       + ,    n o        |   #     *� 
�    {        y z    }   |   5     .� 4� 6� sY� u� x� q�    {        y z    ~   |   "     � q�    {        y z    �   |  f  
   �*� � L*� N*� #+%� **� 6-� :� <:* �� @� F� JY6� &+*L� NYPSYRS� V� \� *� _���� d� :� #�� � #:� h� � :� �:	� k�	+m� *�  4 n z � t w z � 4 n � � t w � � z � � � � � � �  {   f 
   � y z     � � �    � � o    �      � � �    � � �    � � o    � � �    � � �    � � o 	 �     @ � @ � ? �  �              