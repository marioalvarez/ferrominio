����  - � 
SourceFile CC:\ColdFusion9\wwwroot\ferrominio\redproject\catalogos\regiones.cfm cfregiones2ecfm1818720766  coldfusion/runtime/CFPage  <init> ()V  
  	 com.macromedia.SourceModTime  J1 coldfusion/runtime/CfJspPage  pageContext #Lcoldfusion/runtime/NeoPageContext;  	   getOut ()Ljavax/servlet/jsp/JspWriter;   javax/servlet/jsp/PageContext 
   parent Ljavax/servlet/jsp/tagext/Tag;  	   com.adobe.coldfusion.*  bindImportPath (Ljava/lang/String;)V   !
  "�

<script type="text/javascript">
	//Variables globales.
	var g_row;
	
	//FunciÃ³n para abrir la opciÃ³n de agregar, editar o eliminar.
	function fnc_regiones_agregar_editar(p_accion)
	{
		if( p_accion == 1 ) //Si es Agregar.
		{
			$("#txt_id_Division").val($("#txt_id_Division option:first").val());
			$("#div_titulo_agregar_editar").html("NUEVA REGI&Oacute;N");
			$("#btn_guardar").attr("title", "Guardar").text("Guardar");
			// Nos aseguramos de desbloquear los input.
			$("#txt_de_Iniciales").removeAttr("disabled");
			$("#txt_nb_Region").removeAttr("disabled");
			$("#txt_id_Division").removeAttr("disabled");
			
			fnc_catalogos_mostrar_ocultar( "div_grid", "div_guardar_editar_eliminar" );
		}
		else if( p_accion == 2 || p_accion == 3 ) //Editar o Eliminar.
		{
			//Validamos que se haya seleccionado un renglÃ³n.
			var row = ggrids.obtener_renglon("grid_regiones")

			if( row )
			{	
				g_row = row;
				
				//Llenamos los campos.
				$("#txt_de_Iniciales").val( g_row.de_Iniciales);
				$("#txt_nb_Region").val( g_row.nb_Region);
			
				$("#txt_id_Division option:eq("+(g_row.id_Division-1)+")").attr('selected', true);
				
				// Valdiar si debemos bloquear o desbloquear los input.
				if( p_accion == 3 )
				{
					$("#div_titulo_agregar_editar").html("ELIMINAR REGI&Oacute;N");
					$("#btn_guardar").attr("title", "Eliminar").text("Eliminar");
					$("#txt_de_Iniciales").attr("disabled","disabled");
					$("#txt_nb_Region").attr("disabled", "disabled");
					$("#txt_id_Division").attr("disabled", "disabled");
					
				}
				else
				{
					$("#div_titulo_agregar_editar").html("EDITAR REGI&Oacute;N");
					$("#btn_guardar").attr("title", "Guardar").text("Guardar");
					$("#txt_de_Iniciales").removeAttr("disabled");
					$("#txt_nb_Region").removeAttr("disabled");
					$("#txt_id_Division").removeAttr("disabled");
				}
				
				fnc_catalogos_mostrar_ocultar( "div_grid", "div_guardar_editar_eliminar" );
			}
			else
				fnc_notificacion("Debe seleccionar una Regi&oacute;n.");
		}
		
		$("#hid_accion").val(p_accion);
		
		//Limpiamos los campos.
		if( p_accion == 1 )
		{
			$("#txt_de_Iniciales").val("");
			$("#txt_nb_Region").val("");
			$("#txt_id_Division option:eq(0)").attr('selected', true);
		}
		
		$("#txt_de_Iniciales").focus();
	}
	
	//FUNCIONES QUE SE ENCARGAN DE AGREGAR MODIFICAR O ELIMINAR
	function fnc_regiones_crud()
	{
		//Validamos los campos obligaorios.
		if( fnc_validar_campos_obligatorios("div_regiones_crud") )
		{
			//Armamos los parÃ¡metros.
			var actions = new Array("Regiones_Agregar","Regiones_Editar","Regiones_Eliminar");
			var parametros = "id_Empresa=" + $("#hid_id_Empresa").val() + "&method="+ actions[$("#hid_accion").val()-1];
			
			// VALORES DE LOS CAMPOS DEL DIV PARA AGREGAR EDITAR ELIMINAR
			var ini = fnc_codificar(  $.trim( $("#txt_de_Iniciales").val())  );
			var reg = fnc_codificar(  $.trim( $("#txt_nb_Region").val()   )  );
			var div = fnc_codificar(  $.trim( $("#txt_id_Division").val() )  );
			
			//concatenando el metodo
			switch($("#hid_accion").val())
			{
				case "1":   
							parametros +=
							"&de_Iniciales="+ini+
							"&nb_Region="+reg+
							"&id_Division="+div;
							
							break;
				case "2": //FUNCION PARA EDITAR
				
							parametros +=
							"&id_Region="+g_row.id_Region+
							"&de_Iniciales="+g_row.de_Iniciales+
							"&nb_Region="+g_row.nb_Region+
							"&id_Division="+g_row.id_Division+
							"&de_InicialesNuevo="+ini+
							"&nb_RegionNuevo="+reg+
							"&id_DivisionNuevo="+div;

							break;
				case "3": //FUNCION PARA ELIMINAR
							parametros += "&id_Region=" + g_row.id_Region;
							break;
			}

		
			
			//Guardamos, Editamos, Eliminamos.
			fnc_peticionAjax({
				url: "./application/catalogos_danielf.cfc",
				parametros: parametros,
				f_callback: function(){
					//Recargamos el grid.
					ggrids.recargar("grid_regiones");
					
					// si es modificar o eliminar
					if( $("#hid_accion").val() == "2" || $("#hid_accion").val() == "3" )
					{
						fnc_catalogos_mostrar_ocultar( "div_grid", "div_guardar_editar_eliminar" );
					}
					else
					{
						$("#txt_de_Iniciales").val("");
						$("#txt_nb_Region").val("");
						$("#txt_id_Division option:eq(0)").attr('selected', true);
						$("#txt_de_Iniciales").focus();
					}
			
				}
			});
			
		}
	}
	
	//FunciÃ³n para buscar ciclos.
	function fnc_buscar_region()
	{
		//Armamos los parÃ¡metros.
		//trim quitar espacios en blanco
		var parametros = ( $.trim( $("#txt_nb_RegionBusqueda").val() ) == "" ? "" : "&nb_Region=" + fnc_codificar( $.trim( $("#txt_nb_RegionBusqueda").val() ) ) );
		//Recargamos el grid.
		ggrids.recargar("grid_regiones", parametros);
	}
</script>

<center>
    <div style="width: 900px;">
        <div class="ui-widget-header catalogos_cabecero titulo_opcion">CAT&Aacute;LOGO DE REGIONES</div>
        <div class="catalogos_cabecero_contenido" id="div_grid">
            <div class="ui-state-default catalogos_busqueda" id="div_busqueda">
                <table style="width: 100%;">
                    <tr>
                        <td style="text-align: left; padding-left:10px;"><span>Regi&oacute;n: </span>
                        <input type="text" id="txt_nb_RegionBusqueda" class="input_text" onChange="fnc_buscar_region();" style="width: 200px;" /></td>
                    </tr>
                </table>
            </div>
            <div id="grid_regiones" class="ggrids" paginado="10" titulo="Listado de Regiones"
                url="application/catalogos_danielf.cfc?method=Regiones_Listar_JSON&id_Empresa= $ write & ! java/io/Writer (
 ) ' $class$coldfusion$tagext$io$OutputTag Ljava/lang/Class; coldfusion.tagext.io.OutputTag - forName %(Ljava/lang/String;)Ljava/lang/Class; / 0 java/lang/Class 2
 3 1 + ,	  5 _initTag P(Ljava/lang/Class;ILjavax/servlet/jsp/tagext/Tag;)Ljavax/servlet/jsp/tagext/Tag; 7 8
  9 coldfusion/tagext/io/OutputTag ; _setCurrentLineNo (I)V = >
  ? 	hasEndTag (Z)V A B coldfusion/tagext/GenericTag D
 E C 
doStartTag ()I G H
 < I REQUEST K java/lang/String M SESSION O 
ID_EMPRESA Q _resolveAndAutoscalarize 9(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/Object; S T
  U _String &(Ljava/lang/Object;)Ljava/lang/String; W X coldfusion/runtime/Cast Z
 [ Y doAfterBody ] H
 < ^ doEndTag ` H coldfusion/tagext/QueryLoop b
 c a doCatch (Ljava/lang/Throwable;)V e f
 c g 	doFinally i 
 < jB" 
                ancho="800"
                sortname="nb_Region" sortorder="ASC"
                loadComplete="fnc_autoajustar_ancho_busqueda('grid_regiones','div_busqueda')">
                
                
                <columna align="center"  ancho = "100" nom_columna="de_Iniciales">Clave</columna>
                <columna ancho = "280" nom_columna="nb_Region">Regi&oacute;n</columna>
                <columna align="center"  ancho = "100" nom_columna="id_Division">Divisi&oacute;n</columna>
                
                
                
                <columna nom_columna="id_Empresa" visible="false"></columna>
                <columna nom_columna="id_Region" visible="false"></columna>
                
                
                <accion accion="agregar" evento="fnc_regiones_agregar_editar(1);"></accion>
                <accion accion="editar" evento="fnc_regiones_agregar_editar(2);"></accion>
                <accion accion="eliminar" evento="fnc_regiones_agregar_editar(3);"></accion>
            </div>
        </div>
        
        
        <div id="div_guardar_editar_eliminar" class="catalogos_guardar_editar">
            <div class="ui-widget-header catalogos_guardar_editar_cabecero" id="div_titulo_agregar_editar"></div>
            <div class="catalogos_guardar_editar_cabecero_contenido" id="div_regiones_crud">
                
                <table id="table_regiones_guardar_editar">                      
                        <tr>
                        	<td style="text-align:right"><span>Clave:</span></td>
                            <td style="text-align:left"><input type="text" class="input_text obligatorio" id="txt_de_Iniciales" acercade="Clave" style="width:100px" maxlength="10"  /></td>
                        </tr>
                        
                         <tr>
                        	<td style="text-align:right"><span>Regi&oacute;n:</span></td>
                            <td style="text-align:left"><input type="text" class="input_text obligatorio" id="txt_nb_Region" acercade="Regi&oacute;n" style="width:225px" maxlength="120"  /></td>
                        </tr>   
                         
                         <tr>
                        	<td style="text-align:right"><span>Divisi&oacute;n:</span></td>
                            <td style="text-align:left">
                                <select id="txt_id_Division" class="input_text obligatorio" acercade="Divisi&oacute;n"  maxlength="1" >
                                  <option id="1" value="1">1</option>
                                  <option id="2" value="2">2</option>
                                  <option id="3" value="3">3</option>
                                </select> 
                            </td>
                        </tr>                            
                </table> 
                
  
                <div style="margin-top: 10px;">
                    <button class="boton" title="Guardar" id="btn_guardar" onClick="fnc_regiones_crud();">Guardar</button>
                    <button class="boton" title="Regresar" onClick="fnc_catalogos_mostrar_ocultar( 'div_grid', 'div_guardar_editar_eliminar' );">Regresar</button>
                </div>
            </div>
        </div>
    </div>
</center>

<input type="hidden" id="hid_accion" /> l metaData Ljava/lang/Object; n o	  p &coldfusion/runtime/AttributeCollection r java/lang/Object t ([Ljava/lang/Object;)V  v
 s w this Lcfregiones2ecfm1818720766; LocalVariableTable Code <clinit> getMetadata ()Ljava/lang/Object; runPage out Ljavax/servlet/jsp/JspWriter; value output0  Lcoldfusion/tagext/io/OutputTag; mode0 I t6 t7 Ljava/lang/Throwable; t8 t9 LineNumberTable java/lang/Throwable � 1       + ,    n o        |   #     *� 
�    {        y z    }   |   5     .� 4� 6� sY� u� x� q�    {        y z    ~   |   "     � q�    {        y z    �   |  f  
   �*� � L*� N*� #+%� **� 6-� :� <:* �� @� F� JY6� &+*L� NYPSYRS� V� \� *� _���� d� :� #�� � #:� h� � :� �:	� k�	+m� *�  4 n z � t w z � 4 n � � t w � � z � � � � � � �  {   f 
   � y z     � � �    � � o    �      � � �    � � �    � � o    � � �    � � �    � � o 	 �     @ � @ � ? �  �              