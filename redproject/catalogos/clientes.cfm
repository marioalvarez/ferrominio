����  - � 
SourceFile CC:\ColdFusion9\wwwroot\ferrominio\redproject\catalogos\clientes.cfm cfclientes2ecfm1619708233  coldfusion/runtime/CFPage  <init> ()V  
  	 com.macromedia.SourceModTime  J1 coldfusion/runtime/CfJspPage  pageContext #Lcoldfusion/runtime/NeoPageContext;  	   getOut ()Ljavax/servlet/jsp/JspWriter;   javax/servlet/jsp/PageContext 
   parent Ljavax/servlet/jsp/tagext/Tag;  	   com.adobe.coldfusion.*  bindImportPath (Ljava/lang/String;)V   !
  "u
<script type="text/javascript">
	//Variables globales.
	var g_row;
	//var jasApp  = new jsApp_catalogos_flavio();
	

	//Función para abrir la opción de agregar y/o editar.
	function fnc_clientes_agregar_editar(p_accion)
	{
		if( p_accion == 1 ) //Agregar.
		{
			$("#div_titulo_agregar_editar").html("NUEVO CLIENTE");
			$("#btn_guardar").attr("title", "Guardar").text("Guardar");
			
			//Nos aseguramos de desbloquear los input.      
			$("#txt_nb_NombreCliente, #txt_de_Direccion, #txt_nu_Fax, #txt_nu_Telefono, #txt_de_Email, #txt_nb_Contacto, #ddl_TiposCliente").removeAttr("disabled");
			
			fnc_limpiar_campos_clientes();
			
			fnc_catalogos_mostrar_ocultar( "div_grid", "div_guardar_editar_eliminar" );
		}
		else if( p_accion == 2 || p_accion == 3 ) //Editar o Eliminar.
		{
			//Validamos que se haya seleccionado un renglón.
			var row = ggrids.obtener_renglon("grid_clientes");
			
			if( row )
			{	
				g_row = row;

				//Llenamos los campos.
				$("#txt_nb_NombreCliente").val( row.nb_Cliente );
				$("#txt_de_Direccion").val( row.de_Direccion );
				$("#txt_nu_Fax").val( row.nu_Fax );
				$("#txt_nu_Telefono").val( row.nu_Telefono );
				$("#txt_de_Email").val( row.de_Email );
				$("#txt_nb_Contacto").val( row.nb_Contacto );				
				//$("#ddl_TiposCliente").val( row.id_TipoCliente );				
				
				//Valdiar si debemos bloquear o desbloquear los input.
				if( p_accion == 3 )
				{
					$("#div_titulo_agregar_editar").html("ELIMINAR CLIENTE");
					$("#btn_guardar").attr("title", "Eliminar").text("Eliminar");
					$("#txt_nb_NombreCliente, #txt_de_Direccion, #txt_nu_Fax, #txt_nu_Telefono, #txt_de_Email, #txt_nb_Contacto, #ddl_TiposCliente").attr("disabled","disabled");
				}
				else
				{
					$("#div_titulo_agregar_editar").html("EDITAR CLIENTE");
					$("#btn_guardar").attr("title", "Guardar").text("Guardar");
					$("#txt_nb_NombreCliente, #txt_de_Direccion, #txt_nu_Fax, #txt_nu_Telefono, #txt_de_Email, #txt_nb_Contacto, #ddl_TiposCliente").removeAttr("disabled");
				}
				
				fnc_catalogos_mostrar_ocultar( "div_grid", "div_guardar_editar_eliminar" );				
			}
			else
				fnc_notificacion("Debe seleccionar un Cliente.");
				
		}
		
		$("#hid_accion").val( p_accion );
		
		$("#txt_nb_NombreCliente").focus();
	}
	
	//Función para guardar y/o editar.
	function fnc_clientes_crud()
	{	
		//Validamos los campos obligaorios.
		if(fnc_validar_campos_obligatorios('div_guardar_editar_eliminar'))
		{
			//Armamos los parámetros.
			var accion= new Array("Clientes_Agregar","Clientes_Editar","Clientes_Eliminar");
			var parametros = "id_Empresa=" + $("#hid_id_Empresa").val() + "&method="+ accion[$("#hid_accion").val()-1];
			// VALORES DE LOS CAMPOS DEL DIV PARA AGREGAR EDITAR ELIMINAR
			var nom = fnc_codificar(  $.trim( $("#txt_nb_NombreCliente").val())  );
			var dir = fnc_codificar(  $.trim( $("#txt_de_Direccion").val())  );
			var fax = fnc_codificar(  $.trim( $("#txt_nu_Fax").val())  );
			var tel = fnc_codificar(  $.trim( $("#txt_nu_Telefono").val())  );
			var mail= fnc_codificar(  $.trim( $("#txt_de_Email").val())  );
			var con = fnc_codificar(  $.trim( $("#txt_nb_Contacto").val())  );
			var org = fnc_codificar(  $.trim( $("#txt_ar_Organigrama").val())  );
			
			switch( $("#hid_accion").val() )
			{
				case "1": //Agregar.
						 	parametros +=
							"&nb_NombreCliente="+ nom+
							"&de_Direccion="+ dir+
							"&nu_Fax="+ fax+
							"&nu_Telefono="+ tel+
							"&de_Email="+ mail+
							"&nb_Contacto="+ con+
							"&ar_Organigrama="+ org+
							"&id_TipoCliente="+ 2;
							
							break;
							
				case "2": //Editar				
							parametros +=
							"&id_Cliente="+ g_row.id_Cliente+
							"&nb_NombreCliente="+ nom+
							"&nb_NombreClienteViejo="+ g_row.nb_Cliente+
							"&de_Direccion="+ dir+
							"&nu_Fax="+ fax+
							"&nu_Telefono="+ tel+
							"&de_Email="+ mail+
							"&nb_Contacto="+ con+
							"&ar_Organigrama="+ org+
							"&id_TipoCliente="+ 2;							
							
								//$("#ddl_TiposCliente option:selected").val()*/
							break;
				case "3" : //Eliminar
							parametros += 
							"&id_Cliente=" + g_row.id_Cliente;
							
			}

		    fnc_peticionAjax(
			{
				url: "./application/catalogos_flavio.cfc",
				parametros: parametros,
				f_callback: function()
				{
					//Actualizamos el grid.
					ggrids.recargar("grid_clientes");
							
					if( $("#hid_accion").val() == "1" )
						fnc_limpiar_campos_clientes();
					else			
						fnc_catalogos_mostrar_ocultar( "div_grid", "div_guardar_editar_eliminar" );
				}
			});	
		}
	}
	
	//Función para limpiar los campos.
	function fnc_limpiar_campos_clientes()
	{
		$("#txt_nb_NombreCliente").val("");
		$("#txt_de_Direccion").val("");
		$("#txt_nu_Fax").val("");
		$("#txt_nu_Telefono").val("");
		$("#txt_de_Email").val("");
		$("#txt_nb_Contacto").val("");
		$("#ddl_TiposCliente").val(0);
	}
	
	//Función para buscar clientes.
	function fnc_buscar_clientes()
	{
		//Armamos los parámetros.
		var parametros = (  $.trim( $("#txt_nombre").val() ) == "" ? "" : "&nb_Cliente=" + $.trim( $("#txt_nombre").val() ) );

		ggrids.recargar("grid_clientes", parametros);
	}

</script>


	<center>
        <div style="margin-left:5%; margin-right:5%; width:90%">
            <div class="ui-widget-header catalogos_cabecero titulo_opcion">CAT&Aacute;LOGO DE CLIENTES</div>
            <div class="catalogos_cabecero_contenido" id="div_grid">
            	<div id="div_busqueda" class="ui-state-default catalogos_busqueda">
                	<table style="width: 100%;">
                    	<tr>
                        	<td style="text-align: left; padding-left:100px;"><span>Nombre Cliente: </span>
                            <input type="text" id="txt_nombre" onChange="fnc_buscar_clientes()" style="width: 200px;" /></td>
                                                        
                        </tr>
                    </table>
                </div>
                <div id="grid_clientes" class="ggrids" 
                	url="application/catalogos_flavio.cfc?method=Clientes_ListarJSON&id_Empresa= $ write & ! java/io/Writer (
 ) ' $class$coldfusion$tagext$io$OutputTag Ljava/lang/Class; coldfusion.tagext.io.OutputTag - forName %(Ljava/lang/String;)Ljava/lang/Class; / 0 java/lang/Class 2
 3 1 + ,	  5 _initTag P(Ljava/lang/Class;ILjavax/servlet/jsp/tagext/Tag;)Ljavax/servlet/jsp/tagext/Tag; 7 8
  9 coldfusion/tagext/io/OutputTag ; _setCurrentLineNo (I)V = >
  ? 	hasEndTag (Z)V A B coldfusion/tagext/GenericTag D
 E C 
doStartTag ()I G H
 < I REQUEST K java/lang/String M SESSION O 
ID_EMPRESA Q _resolveAndAutoscalarize 9(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/Object; S T
  U _String &(Ljava/lang/Object;)Ljava/lang/String; W X coldfusion/runtime/Cast Z
 [ Y doAfterBody ] H
 < ^ doEndTag ` H coldfusion/tagext/QueryLoop b
 c a doCatch (Ljava/lang/Throwable;)V e f
 c g 	doFinally i 
 < j�" 
                	titulo="Listado de Clientes"ancho="800" sortname="nb_Cliente" autowidth="true" 
                    loadComplete="fnc_autoajustar_ancho_busqueda('grid_clientes','div_busqueda')">
                    
                    <columna nom_columna="nb_Cliente">Nombre Cliente</columna>
                    <columna nom_columna="de_Direccion">Direcci&oacute;n</columna>
                    <columna nom_columna="nu_Fax">Fax</columna>
                    <columna nom_columna="nu_Telefono">Tel&eacute;fono</columna>
                    <columna nom_columna="de_Email">E-Mail</columna>
                    <columna nom_columna="nb_Contacto" >Nombre Contacto</columna>                  

                    <columna nom_columna="id_Empresa" visible="false"></columna>
                    <columna nom_columna="id_Cliente" visible="false"></columna>
                    
                    <accion accion="agregar" evento="fnc_clientes_agregar_editar(1);"></accion>
                    <accion accion="editar" evento="fnc_clientes_agregar_editar(2);"></accion>
                    <accion accion="eliminar" evento="fnc_clientes_agregar_editar(3);"></accion>
                </div>
            </div>
            
            
            <div id="div_guardar_editar_eliminar" class="catalogos_guardar_editar">
            	<div class="ui-widget-header catalogos_guardar_editar_cabecero" id="div_titulo_agregar_editar"></div>
                <div class="catalogos_guardar_editar_cabecero_contenido">
                    <table id="table_clientes_guardar_editar"> 
                        <tr>
                            <td style="text-align: right;"><span>Nombre Cliente: </span></td>
                            <td style="text-align: left;">
                            	<input class="input_text obligatorio" acercade="Nombre Cliente" type="text" id="txt_nb_NombreCliente" 
                            		maxlength="250" style="width: 300px" />
                             </td>
                        </tr> 
                        <tr>
                            <td style="text-align: right;"><span>Direcci&oacute;n: </span></td>
                            <td style="text-align: left;"><input class="input_text" type="text" id="txt_de_Direccion" maxlength="300" style="width: 300px" /></td>
                        </tr> 
                        <tr>
                            <td style="text-align: right;"><span>Fax: </span></td>
                            <td style="text-align: left;"><input class="input_text" type="text" id="txt_nu_Fax" maxlength="15" style="width: 300px" /></td>
                        </tr>
                        <tr>
                            <td style="text-align: right;"><span>Tel&eacute;fono: </span></td>
                            <td style="text-align: left;"><input class="input_text" type="text" id="txt_nu_Telefono" maxlength="15" style="width: 300px" /></td>
                        </tr>
                        <tr>
                            <td style="text-align: right;"><span>E-Mail: </span></td>
                            <td style="text-align: left;"><input class="input_text" type="text" id="txt_de_Email" maxlength="250" style="width: 300px" /></td>
                        </tr> 
                        <tr>
                            <td style="text-align: right;"><span>Nombre Contacto: </span></td>
                            <td style="text-align: left;"><input class="input_text" type="text" id="txt_nb_Contacto" maxlength="250" style="width: 300px"/></td>
                        </tr>
                        
                                              
                    </table>                    
                    <div style="margin-top: 10px;">
                    	<button class="boton" title="Guardar" id="btn_guardar" onClick="fnc_clientes_crud();">Guardar</button>
                        <button class="boton" title="Regresar" onClick="fnc_catalogos_mostrar_ocultar( 'div_grid', 'div_guardar_editar_eliminar' );">Regresar</button>
                    </div>
                </div>
            </div>
        </div>
    </center>    
    <input type="hidden" id="hid_accion" />

 l metaData Ljava/lang/Object; n o	  p &coldfusion/runtime/AttributeCollection r java/lang/Object t ([Ljava/lang/Object;)V  v
 s w this Lcfclientes2ecfm1619708233; LocalVariableTable Code <clinit> getMetadata ()Ljava/lang/Object; runPage out Ljavax/servlet/jsp/JspWriter; value output0  Lcoldfusion/tagext/io/OutputTag; mode0 I t6 t7 Ljava/lang/Throwable; t8 t9 LineNumberTable java/lang/Throwable � 1       + ,    n o        |   #     *� 
�    {        y z    }   |   5     .� 4� 6� sY� u� x� q�    {        y z    ~   |   "     � q�    {        y z    �   |  f  
   �*� � L*� N*� #+%� **� 6-� :� <:* Զ @� F� JY6� &+*L� NYPSYRS� V� \� *� _���� d� :� #�� � #:� h� � :� �:	� k�	+m� *�  4 n z � t w z � 4 n � � t w � � z � � � � � � �  {   f 
   � y z     � � �    � � o    �      � � �    � � �    � � o    � � �    � � �    � � o 	 �     @ � @ � ? �  �              