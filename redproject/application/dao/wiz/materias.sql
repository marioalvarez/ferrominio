EXEC sp_executesql N'/*Materias - ObtenerPorID */
CREATE PROCEDURE bop_Materias_ObtenerPorID 
@id_Materia int 
AS
BEGIN
SELECT id_Materia, de_Materia
FROM Materias
WHERE id_Materia= @id_Materia
END
'

EXEC sp_executesql N'/*Materias - ObtenerDinamico */
CREATE PROCEDURE bop_Materias_ObtenerDinamico 
@WhereDinamico varchar(1000)
AS
BEGIN
Declare @sql varchar(1000)
Set @sql = ''SELECT id_Materia, de_Materia FROM Materias '' + @WhereDinamico
Execute (@sql)
END
'

EXEC sp_executesql N'/*Materias - ObtenerTodos */
CREATE PROCEDURE bop_Materias_ObtenerTodos 
AS
BEGIN
SELECT id_Materia, de_Materia
FROM Materias
END
'

EXEC sp_executesql N'/*Materias - Agregar */
CREATE PROCEDURE bop_Materias_Agregar 
@id_Materia int , @de_Materia varchar (250)
AS
BEGIN
INSERT INTO Materias (id_Materia, de_Materia)
VALUES (@id_Materia, @de_Materia)

END
'

EXEC sp_executesql N'/*Materias - Actualizar */
CREATE PROCEDURE bop_Materias_Actualizar 
@id_Materia int , @de_Materia varchar (250)
AS
BEGIN
UPDATE Materias 
SET 
de_Materia= @de_Materia
WHERE 
id_Materia= @id_Materia
SELECT @@ROWCOUNT AS filasAfectadas
END
'

EXEC sp_executesql N'/*Materias - Eliminar */
CREATE PROCEDURE bop_Materias_Eliminar 
@id_Materia int 
AS
BEGIN
DELETE FROM Materias 
WHERE 
id_Materia= @id_Materia
SELECT @@ROWCOUNT AS filasAfectadas
END
'

