//Creado por: Gil Verduzco.
var ggrids = {
	crear: function(p_id_ggrid, p_opciones){
		var ggrid = $( "#" + p_id_ggrid );
		var p_paginado = ( ( $( "#" + p_id_ggrid ).attr("paginado") ) ? $( "#" + p_id_ggrid ).attr("paginado") : 10 );
		var p_width = ( ( $( "#" + p_id_ggrid ).attr("ancho") ) ? $( "#" + p_id_ggrid ).attr("ancho") : "600" );
		var p_autowidth = ( ( $( "#" + p_id_ggrid ).attr("autowidth") ) ? ( $( "#" + p_id_ggrid ).attr("autowidth") == "true" || $( "#" + p_id_ggrid ).attr("auto_ajustar") == "TRUE" ? true : false ) : false );
		var p_grouping = ( ( $( "#" + p_id_ggrid ).attr("grouping") ) ? ( $( "#" + p_id_ggrid ).attr("grouping") == "true" || $( "#" + p_id_ggrid ).attr("auto_ajustar") == "TRUE" ? true : false ) : false );
		var p_groupingView = ( ( $( "#" + p_id_ggrid ).attr("groupingView") ) ? $( "#" + p_id_ggrid ).attr("groupingView") : "" );
		var p_titulo = $( "#" + p_id_ggrid ).attr("titulo");
		var p_sortname = ( ( $( "#" + p_id_ggrid ).attr("sortname") ) ? $( "#" + p_id_ggrid ).attr("sortname") : $(ggrid).find("columna:first").attr("nom_columna") );
		var p_sortorder = ( ( $( "#" + p_id_ggrid ).attr("sortorder") ) ? $( "#" + p_id_ggrid ).attr("sortorder") : "ASC" );
		var p_beforeSelectRow = ( ( $( "#" + p_id_ggrid ).attr("beforeSelectRow") ) ? $( "#" + p_id_ggrid ).attr("beforeSelectRow") + ";" : "" );
		var p_onSelectRow = ( ( $( "#" + p_id_ggrid ).attr("onSelectRow") ) ? $( "#" + p_id_ggrid ).attr("onSelectRow") + ";" : "void(0);" );
		var p_loadComplete = ( ( $( "#" + p_id_ggrid ).attr("loadComplete") ) ? $( "#" + p_id_ggrid ).attr("loadComplete") + "; $('.jqgroup td').css('font-weight', 'bold');" : "$('.jqgroup td').css('font-weight', 'bold');" );
		var p_gridComplete = ( ( $( "#" + p_id_ggrid ).attr("gridComplete") ) ? $( "#" + p_id_ggrid ).attr("gridComplete") + ";" : "void(0);" );
		var p_ondblClickRow = ( ( $( "#" + p_id_ggrid ).attr("ondblClickRow") ) ? $( "#" + p_id_ggrid ).attr("ondblClickRow") + ";" : "void(0);" );
		var p_multiselect = ( ( $( "#" + p_id_ggrid ).attr("multiselect") ) ? ( $( "#" + p_id_ggrid ).attr("multiselect") == "true" ? true : false ) : false );
		var p_loadError = ( ( $( "#" + p_id_ggrid ).attr("loadError") ) ? $( "#" + p_id_ggrid ).attr("loadError") + ";" : "void(0);" );
		
		var width_grid = p_width;

		//Validamos si el grid tiene el ancho declarado en pixeles o en porcentaje.

		var px = p_width.toString().substring( p_width.toString().length - 2 );
		var prc = p_width.toString().substring( p_width.toString().length - 1 );
		var px_prc = px;

		if( px_prc != "px" )
		{
			px_prc = prc;

			if( prc != "%" )
				px_prc = "px";
		}
		
		//Validamos el width_grid.
		if( p_width != "" )
		{
			if( isNaN( p_width * 1 ) )
			{
				width_grid = ( px_prc == "px" ? p_width.toString().substring( 0,  p_width.toString().length - 2 ) * 1 : p_width.toString().substring( 0,  p_width.toString().length - 1 ) * 1 );
				
				if( isNaN( width_grid * 1 ) || ( px != "px" && prc != "%" ) || ( width_grid * 1 ) == 0 )
				{
					width_grid = 600;
					px_prc = "px";
				}
			}
			else if( ( p_width * 1 ) == 0 )
			{
				width_grid = 600;
				px_prc = "px";
			}
		}
		else
		{
			width_grid = 600;
			px_prc = "px";
		}

		//Calculamos el porcentaje en pixeles.
		if( px_prc == "%" )
		{
			var ancho_parent;

			if( $(ggrid).parent().is(":visible") )
				ancho_parent = $(ggrid).parent().width();
			else
			{
				//Itero sobre un ci
				$(ggrid).parent().show();
				ancho_parent = $(ggrid).parent().width();
				$(ggrid).parent().hide();
			}	
			
			width_grid = ( width_grid > 100 ? 100 : width_grid );
			width_grid = ( ( width_grid * $(ggrid).parent().width() ) / 100 ) - 2;
			px_prc = "px"; 
		}

		//Si existe lo eliminamos.
		if( document.getElementById( "div_grid_" + p_id_ggrid ) )
		{
			$( "#div_grid_" + p_id_ggrid ).remove();
		}

		//Creamos el table.
		var table = $('<div>').attr('id', 'div_grid_' + p_id_ggrid).html( '<table id="grid_' + p_id_ggrid + '"></table><div id="pager_' + p_id_ggrid + '"></div>' );
		$(ggrid).after( table );

		var col_names = '';
		var col_model = '';
		var visible;
		var width;
		var sortable;
		var align;
		$(ggrid).find("columna").each(function(){
			visible = ( ( $(this).attr("visible") ) ? $(this).attr("visible") : "true" ).toUpperCase();
			width = ( ( $(this).attr("ancho") ) ? ", width: " + $(this).attr("ancho") : "" );
			sortable = ( ( $(this).attr("sortable") ) ? ", sortable: " + $(this).attr("sortable") : "" );
			align = ( ( $(this).attr("align") ) ? ", align: '" + $(this).attr("align") + "'" : "" );
			
			if( visible == "FALSE" )
				visible = "hidden: true";
			else
				visible = "hidden: false";
			
			if( col_names != '' )
			{
				col_model += ', '
				col_names += ', ';
			}
			
			col_names += '"' + $(this).text() + '"';
			col_model += '{name: "' + $(this).attr("nom_columna") + '", index: "' + $(this).attr("nom_columna") + '", ' 
				+ visible + width + sortable + align + '}';
		});
		
		col_names = "[" + col_names + "]";
		col_model = "[" + col_model + "]";

		var url_grid = ( $(ggrid).attr("url") ? $(ggrid).attr("url") : "" );
		var parametros_grid = ( $(ggrid).attr("parametros") ? $(ggrid).attr("parametros") : "" );

		//Creamos el grid.
		$( "#grid_" + p_id_ggrid ).jqGrid({
			url: url_grid + parametros_grid,
			height: "auto",
			width: width_grid,
			autowidth: p_autowidth,
			datatype: ( url_grid == "" ? "local" : "json" ),
			colNames: eval( col_names ),
			colModel: eval( col_model ),
			rowNum: p_paginado,
			rowList: [10, 20, 30],
			pager: "#pager_" + p_id_ggrid,
			sortname: p_sortname,
			/*viewrecords: true,*/
			forceFit: true,
			gridview: true,
			multiselect: p_multiselect,
			sortorder: p_sortorder,
			grouping: p_grouping,
			groupingView: { groupField : [p_groupingView],
						    groupColumnShow: [true]
			},
			caption: p_titulo,
			beforeSelectRow: function(){
				if( p_beforeSelectRow === "" )
					return true;
				else
				{
					eval( p_beforeSelectRow );
					return true;
				}
			},
			onSelectRow: function(){
				eval( p_onSelectRow );
			},
			loadComplete: function(){
				eval( p_loadComplete );
			},
			gridComplete: function(){
				eval( p_gridComplete );
			},
			ondblClickRow: function(){
				eval( p_ondblClickRow );
			},
			loadError: function(){
				eval( p_loadError );
			}
		});
		
		$( "#grid_" + p_id_ggrid ).jqGrid("navGrid", "#pager_" + p_id_ggrid, { edit: false, add:false, del: false, search: false, view: false, refresh: false});
		
		//Acciones.
		$( "#" + $(ggrid).attr("id") ).find("accion").each(function(){
			var accion = ( ( ( $(this).attr("accion") ) ? $(this).attr("accion") : "" ) == "" ? false : $(this).attr("accion") );
			var evento = ( ( $(this).attr("evento") ) ? $(this).attr("evento") : "" );
			var titulo = ( ( $(this).attr("titulo") ) ? $(this).attr("titulo") : "" );
			var icono = ( ( $(this).attr("class_icono") ) ? $(this).attr("class_icono") : "" );
			var visible = ( ( ( $(this).attr("visible") ) ? $(this).attr("visible") : "true" ) == "true" ? true : false );
			
			if( accion && visible )
			{
				accion = accion.toUpperCase();
				
				if( accion == "AGREGAR" )
				{
					icono = ( icono == "" ? "ui-icon ui-icon-plusthick" : icono );
					titulo = ( titulo == "" ? "Agregar" : titulo );
				}
				else if( accion == "EDITAR" )
				{
					icono = ( icono == "" ? "ui-icon ui-icon-pencil" : icono );
					titulo = ( titulo == "" ? "Editar" : titulo );
				}
				else if( accion == "ELIMINAR" )
				{
					icono = ( icono == "" ? "ui-icon ui-icon-trash" : icono );
					titulo = ( titulo == "" ? "Eliminar" : titulo );
				}
				
				if( accion == "AGREGAR" || accion == "EDITAR" || accion == "ELIMINAR" || accion == "PERSONALIZADA" )
				{
					$( "#grid_" + p_id_ggrid ).navButtonAdd( "pager_" + p_id_ggrid, {
						caption: "<span style='margin-right: 5px;'>" + titulo + "</span>",
						buttonicon: icono, 
						onClickButton: function(){ eval( evento ) }
					});
				}
			}
		});
		
		//$(".ui-pg-table").removeAttr("style");
		//$(".ui-pg-table").css("float", "left");
		//$("table[class='ui-pg-table navtable']").css("table-layout", "auto");
		$(".ui-jqgrid-pager").css({ overflow: "auto", height: "100%" });
		$("#pg_pager_" + p_id_ggrid + " table:first").css("table-layout", "auto");
		$("#pg_pager_" + p_id_ggrid + " .ui-pg-input, #pg_pager_" + p_id_ggrid + " .ui-pg-selbox").addClass("input_text").css({
			textAlign: "center"
		});
	},
	obtener_renglon: function(p_id){
		var multiselect = $("#grid_" + p_id).jqGrid('getGridParam', 'multiselect');
		
		if( !multiselect )
		{
			var row = $("#grid_" + p_id).jqGrid("getGridParam", "selrow");
			if( row )
			{
				var obj = $("#grid_" + p_id).jqGrid("getRowData", row);
				obj.id_row = row;
				
				return obj;
			}
		}
		else
		{
			var array_obj = new Array();
			var array_rows = $('#grid_' + p_id).jqGrid('getGridParam', 'selarrrow');
			
			$.each(array_rows, function(index, value){
				array_obj.push( $("#grid_" + p_id).jqGrid("getRowData", value) );				
			});
			
			if( array_obj.length > 0 )
				return array_obj;
		}
		
		return false;
	},
	recargar: function(p_id, p_parametros){
		/*var row = $("#grid_" + p_id).jqGrid("getGridParam", "selrow");
		var parametros = null;
		var loadComplete_recargar = "";
		var recordar_seleccion = "";
		var obj = null;

		//Validamos el objeto.
		if( p_parametros && typeof(p_parametros) == "object" )
			obj = p_parametros;
		else 
		{
			if( p_parametros != "undefined" )
				parametros = p_parametros;		
		}
			
		if( obj && obj.loadComplete )
			loadComplete_recargar = obj.loadComplete;

		if( obj && obj.recordar_seleccion && obj.recordar_seleccion == true )
			recordar_seleccion = "$( '#grid_" + p_id + "' ).jqGrid('setSelection', '" + row + "')";
				
		$( "#" + p_id ).attr({
			loadComplete_recargar: loadComplete_recargar + " " + recordar_seleccion
		});
		
		//Validamos si debemos actualizar los parámetros.
		if( parametros != "undefined" )
		{
			$( "#" + p_id ).attr({
				parametros: parametros
			});
		}
		
		setTimeout(function(){
			ggrids.crear( p_id );
			
			$( "#" + p_id ).attr({
				loadComplete_recargar: ""
			});
			
		}, 300)*/
		
		if( p_parametros || p_parametros == "" )
		{		
			$("#grid_" + p_id).jqGrid("setGridParam", {
				url: $("#" + p_id).attr("url") + p_parametros
			}).trigger("reloadGrid");
		}
		else
			$("#grid_" + p_id).trigger("reloadGrid");
	},
	existe: function(p_id_grid){
		return ( document.getElementById( "grid_" + p_id_grid ) ? true : false );
	},
	quitar_seleccion: function(p_id){
		$("#grid_" + p_id).jqGrid('resetSelection')
	},
	recordar_seleccion: function(p_id, p_id_row){
		$( "#grid_" + p_id ).jqGrid('setSelection', p_id_row)
	},
	ocultar_registros: function(p_id){
		$( "#gview_grid_" + p_id + " a[role='link']" ).trigger("click");
	}
}