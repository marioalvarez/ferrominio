//Funci�n para calcular el cronograma.
function fnc_calcular_cronograma()
{
	var cronogramaXML = $.parseXML( $.trim( $("#div_plantilla_cronograma").html() ) );
	var id_fase_actual;
	var requerimiento_actual;
	var fase;
	var total_horas_requerimiento = 0;
	var contador_actividades = 0;
	var next_id_actividad = 0;
	var nu_ciclo_actual;
	
	//Creamos el cronograma.
	$("#div_cronograma_" + g_id_CronogramaActual + " table tbody tr").remove();
	$(".ddl_requerimientos").each(function(){
		if( $(this).val() != "0" )
		{
			requerimiento_actual = $(this).find("option:selected");
			nu_ciclo_actual = $(requerimiento_actual).parent().parent().parent().attr("nu_ciclo");

			$(cronogramaXML).find("fase[id_plantilla='" + $(requerimiento_actual).attr("id_plantilla") + "']").each(function(){
				id_fase_actual = $(this).attr("id_fase");
				
				//Validamos si debemos pintar el ciclo.
				if( $("tr.cronograma_ciclos[nu_ciclo='" + nu_ciclo_actual + "']").size() == 0 )
				{
					$("#div_cronograma_" + g_id_CronogramaActual + " table tbody").append(
						'<tr class="ui-widget-header cronograma_ciclos" nu_ciclo="' + nu_ciclo_actual + 
							'" title="Clic para mostrar u ocultar las fases y las actividades" onClick="fnc_mostrar_ocultar_actividades("div_cronograma_' + g_id_CronogramaActual + '", true, this);">' +
							'<td colspan="2">Ciclo ' + nu_ciclo_actual + '</td>' +
						'</tr>'
					);
				}
				
				//Pintamos las fases.
				
				//Validamos que la fase no haya sido agregada.
				if( $(".cronograma_fases[id_fase='" + $(this).attr("id_fase") + "'][nu_ciclo='" + nu_ciclo_actual + "']").size() == 0 )
				{
					fase =
						'<tr class="ui-state-default cronograma_fases" pj_fase="' + $(this).find("pj_fase").text() + '" ' +
							'sn_pivote="' + $(this).find("sn_pivote").text() + '" id_fase="' + $(this).attr("id_fase") + '" ' +
							'id_plantilla="' + $(this).attr("id_plantilla") + '" nu_ciclo="' + nu_ciclo_actual + 
							'" title="Clic para mostrar u ocultar las actividades" onClick="fnc_mostrar_ocultar_actividades("div_cronograma_' + g_id_CronogramaActual + '", false, this);">' +
							'<td><div>' + $(this).find("nb_fase").text() + '</div></td>' +
							'<td style="text-align: center;"></td>' +
						'</tr>';
				}
				else
				{
					//Actualizamos los atributos.
					$(".cronograma_fases[id_fase='" + $(this).attr("id_fase") + "']").attr({
						pj_fase: $(this).find("pj_fase").text(),
						sn_pivote: $(this).find("sn_pivote").text(),
						id_plantilla: $(this).attr("id_plantilla")
					});
				}

				//Si la fase es la fase pivote, entonces metemos los requerimientos correspondientes.
				if( $(this).find("sn_pivote").text() == "1" )
				{
					if( $(".cronograma_fases[id_fase='" + $(this).attr("id_fase") + "'][nu_ciclo='" + nu_ciclo_actual + "']").size() == 0 )
					{
						fase +=
							'<tr class="cronograma_actividades" sn_pivote="1" pj_fase="100" ' + 
								'id_fase="' + $(this).attr("id_fase") + '"id_plantilla="' + $(this).attr("id_plantilla") +
								'" id_actividad="d_' + (++next_id_actividad) + '" nu_ciclo="' + nu_ciclo_actual +
								'" id_requerimiento="' + $(requerimiento_actual).parent().parent().parent().attr("id_requerimiento") +
								'" sn_general="0" sn_obligatoria="1">' +
								'<td>' +
									'<div>' +		
										$(requerimiento_actual).parent().parent().parent().find("td:first").text() +
									'</div>' +
								'</td>' +
								'<td style="text-align: center;">' +
									( ( $(requerimiento_actual).parent().parent().parent().find("td:eq(1)").text() * 1 ).toFixed(2) * 1 ) +
								 '</td>' +
							'</tr>'
					}
					else
					{ 
						$(".cronograma_fases[id_fase='" + $(this).attr("id_fase") + "'][nu_ciclo='" + nu_ciclo_actual + "']").after(
							'<tr class="cronograma_actividades" sn_pivote="1" pj_fase="100" ' + 
								'id_fase="' + $(this).attr("id_fase") + '"id_plantilla="' + $(this).attr("id_plantilla") +
								'" id_actividad=d_"' + (++next_id_actividad) + '" nu_ciclo="' + nu_ciclo_actual +
								'" id_requerimiento="' + $(requerimiento_actual).parent().parent().parent().attr("id_requerimiento") +
								'" sn_general="0" sn_obligatoria="1">' +
								'<td>' +
									'<div>' +		
										$(requerimiento_actual).parent().parent().parent().find("td:eq(0)").text() +
									'</div>' +
								'</td>' +
								'<td style="text-align: center;">' +
									( ( $(requerimiento_actual).parent().parent().parent().find("td:eq(1)").text() * 1 ).toFixed(2) * 1 ) +
								'</td>' +
							'</tr>'
						);
					}
					
					total_horas_requerimiento = $(requerimiento_actual).parent().parent().parent().find("td:eq(1)").text() * 1;
				}
				else
				{
					//Pintamos las actividades correspondientes a esta fase.
					$(cronogramaXML).find("actividad[id_plantilla='" + $(this).attr("id_plantilla") + "'][id_fase='" + $(this).attr("id_fase") + "']").each(function(){
						//Validamos que la actividad no haya sido agregada.
						if( $(".cronograma_actividades[id_fase='" + $(this).attr("id_fase") + "'][id_actividad='" + $(this).attr("id_actividad") + "'][nu_ciclo='" + nu_ciclo_actual + "']").size() == 0 )
						{
							//Validar si la fase ya existe.
							if( $(".cronograma_fases[id_fase='" + $(this).attr("id_fase") + "'][nu_ciclo='" + nu_ciclo_actual + "']").size() == 0 )
							{
								fase +=
									'<tr class="cronograma_actividades" sn_porcentaje="' + $(this).find("sn_porcentaje").text() 
										+ '" pj_actividad="' + $(this).find("pj_actividad").text() 
										+ '" nu_horas_fijas="' + $(this).find("nu_horas_fijas").text() 
										+ '" id_fase="' + $(this).attr("id_fase")
										+ '" id_plantilla="' + $(this).attr("id_plantilla")
										+ '" id_actividad="' + $(this).attr("id_actividad") 
										+ '" nu_ciclo="' + nu_ciclo_actual +
										'" id_requerimiento="" sn_general="' + $(this).find("sn_general").text() +
										'" sn_obligatoria="' + $(this).find("sn_obligatoria").text() + '">' +
										'<td><div>' + $(this).find("de_actividad").text() + '</div></td>' +
										'<td style="text-align: center;"></td>' +
									'</tr>';
							}
							else
							{
								$(".cronograma_fases[id_fase='" + $(this).attr("id_fase") + "'][nu_ciclo='" + nu_ciclo_actual + "']").after(
									'<tr class="cronograma_actividades" sn_porcentaje="' + $(this).find("sn_porcentaje").text() 
										+ '" pj_actividad="' + $(this).find("pj_actividad").text() 
										+ '" nu_horas_fijas="' + $(this).find("nu_horas_fijas").text() 
										+ '" id_fase="' + $(this).attr("id_fase")
										+ '" id_plantilla="' + $(this).attr("id_plantilla")
										+ '" id_actividad="' + $(this).attr("id_actividad") 
										+ '" nu_ciclo="' + nu_ciclo_actual +
										'" id_requerimiento="" sn_general="' + $(this).find("sn_general").text() +
										'" sn_obligatoria="' + $(this).find("sn_obligatoria").text() + '">' +
										'<td><div>' + $(this).find("de_actividad").text() + '</div></td>' +
										'<td style="text-align: center;"></td>' +
									'</tr>'
								);
							}
						}
						else
						{
							//Actualizamos los atributos.
							$(".cronograma_actividades[id_fase='" + $(this).attr("id_fase") + "'][id_actividad='" + $(this).attr("id_actividad") + "']").attr({
								sn_porcentaje: $(this).find("sn_porcentaje").text(),
								pj_actividad: $(this).find("pj_actividad").text(),
								nu_horas_fijas: $(this).find("nu_horas_fijas").text(),
								id_plantilla: $(this).attr("id_plantilla")
							});
						}
					});
				}
				
				$("#div_cronograma_' + g_id_CronogramaActual + ' table tbody").append( fase );
				fase = "";
			});
			
			//Distribuimos los tiempos.
			
			//Sumamos los tiempos de la fase pivote.
			var total_horas = 0;
			$(".cronograma_fases[sn_pivote='1'][nu_ciclo='" + nu_ciclo_actual + "']").each(function(){
				$(this).parent().find(".cronograma_actividades").each(function(){
					total_horas += $(this).find("td:eq(1)").text() * 1;
				});
				
				$(this).find("td:eq(1)").text( total_horas.toFixed(2) * 1 );
			});
			
			var total_horas_fase;
			$(".cronograma_fases[sn_pivote='0'][nu_ciclo='" + nu_ciclo_actual + "']").each(function(){
				total_horas = 0;
				
				//Calculamos las horas que le corresponden a la fase.
				total_horas_fase = ( total_horas_requerimiento * ( $(this).attr("pj_fase") * 1 ) ) / 100;

				//Calculamos las horas para cada actividad.
				$(".cronograma_actividades[id_fase='" + $(this).attr("id_fase") + "'][nu_ciclo='" + $(this).attr("nu_ciclo") + "']").each(function(){
					//S�lo se actualizan las actividades que corresponden a la plantilla.
					if( $(this).attr("id_plantilla") == $(requerimiento_actual).attr("id_plantilla") )
					{
						$(this).find("td:eq(1)").text( ( ( total_horas_fase * ( $(this).attr("pj_actividad") * 1 ) ) / 100
							+ ( $(this).find("td:eq(1)").text() * 1 ) ).toFixed(2) * 1 );
					}
							
					total_horas += $(this).find("td:eq(1)").text() * 1;
				});
				
				$(this).find("td:eq(1)").text( total_horas.toFixed(2) * 1 );
			});
		}
	});
	
	//Enumeramos las actividades.
	if( $(".ddl_requerimientos").size() > 0 )
	{
		$("tr.cronograma_actividades").each(function(index){
			$(this).find("td:eq(0) div").html( "<span style='font-weight: bold;'>" + (index + 1) + "</span><span></span><span style='font-weight: bold;'>. </span><span>" + $(this).find("td:eq(0)").text() + "</span>" );
		});
	}
	
	
	/*$("#div_cronograma_contenido").sortable({
		stop: function(){
			fnc_enumerar_cronograma();
		}
	});*/
	//$("#div_cronograma_contenido tr.cronograma_actividades").sortable();
	
	//Evento para cuando se da clic sobre una actividad.
	/*$("tr.cronograma_actividades").click(function(){
		if( $(this).hasClass("seleccionado") )
			$(this).removeClass("ui-state-highlight seleccionado");
		else
			$(this).addClass("ui-state-highlight seleccionado");
	});*/
}


//Funci�n para mostrar u ocultar las actividades.
function fnc_mostrar_ocultar_actividades(p_div_contenedor, p_opcion, p_tr, p_ggantt, p_evt)
{
	p_evt.preventDefault();
	p_evt.stopPropagation();
	
	//if( p_evt.target.nodeName.toLowerCase() == "span" )
	//{
		var $contenedor = $( "#" + p_div_contenedor );
	
		switch( p_opcion )
		{
			case "etapa":
						var siguiente_nivel = "tr.cronograma_partidas" + p_ggantt + "[id_cronograma='" + $(p_tr).attr("id_cronograma") + "']" 
							+ "[id_etapa='" + $(p_tr).attr("id_etapa") + "']";
								
						if( $(p_tr).hasClass("oculto") || $contenedor.find( siguiente_nivel ).is(":hidden") )
						{ 
							//Cronograma.
							$contenedor.find( siguiente_nivel ).show();
							
							$(p_tr).removeClass("oculto");
							
							//Gantt.
							$contenedor.parent().find( siguiente_nivel.replace("cronograma_partidas", "gantt_partidas") ).show();
								
							$contenedor.parent().find("tr.gantt_etapas[id_cronograma='" + $(p_tr).attr("id_cronograma") + "']"
								+ "[id_etapa='" + $(p_tr).attr("id_etapa") + "']").removeClass("oculto");
							
							//Cambiamos el icono.
							$(p_tr).find("td:first span.cronograma-mm").removeClass("ui-icon ui-icon-circle-triangle-s")
								.addClass("ui-icon ui-icon-circle-triangle-n").fadeOut(300).fadeIn(300);
						}
						else
						{
							//Cronograma.
							$contenedor.find("tr.cronograma_actividades" + p_ggantt + "[id_cronograma='" + $(p_tr).attr("id_cronograma") + "']"
								+ "[id_etapa='" + $(p_tr).attr("id_etapa") + "']").hide();
							
							$contenedor.find("tr.cronograma_subpartidas" + p_ggantt + "[id_cronograma='" + $(p_tr).attr("id_cronograma") + "']"
								+ "[id_etapa='" + $(p_tr).attr("id_etapa") + "']").hide();
	
							$contenedor.find("tr.cronograma_partidas" + p_ggantt + "[id_cronograma='" + $(p_tr).attr("id_cronograma") + "']"
								+ "[id_etapa='" + $(p_tr).attr("id_etapa") + "']").hide();
							
							$(p_tr).addClass("oculto");
							
							//Cambiamos el icono.
							
							$(p_tr).find("td:first span.cronograma-mm").removeClass("ui-icon ui-icon-circle-triangle-n")
								.addClass("ui-icon ui-icon-circle-triangle-s").fadeOut(300).fadeIn(300);
								
							$contenedor.find("tr.cronograma_subpartidas" + p_ggantt + "[id_cronograma='" + $(p_tr).attr("id_cronograma") + "']"
								+ "[id_etapa='" + $(p_tr).attr("id_etapa") + "'] td span.cronograma-mm")
								.removeClass("ui-icon ui-icon-circle-triangle-n")
								.addClass("ui-icon ui-icon-circle-triangle-s").fadeOut(300).fadeIn(300);
							
							$contenedor.find("tr.cronograma_partidas" + p_ggantt + "[id_cronograma='" + $(p_tr).attr("id_cronograma") + "']"
								+ "[id_etapa='" + $(p_tr).attr("id_etapa") + "'] td span.cronograma-mm")
								.removeClass("ui-icon ui-icon-circle-triangle-n")
								.addClass("ui-icon ui-icon-circle-triangle-s").fadeOut(300).fadeIn(300);
							
							//Gantt.
							$contenedor.parent().find("tr.gantt_actividades" + p_ggantt + "[id_cronograma='" + $(p_tr).attr("id_cronograma") + "']"
								+ "[id_etapa='" + $(p_tr).attr("id_etapa") + "']").hide();
							
							$contenedor.parent().find("tr.gantt_subpartidas" + p_ggantt + "[id_cronograma='" + $(p_tr).attr("id_cronograma") + "']"
								+ "[id_etapa='" + $(p_tr).attr("id_etapa") + "']").hide();
							
							$contenedor.parent().find("tr.gantt_partidas" + p_ggantt + "[id_cronograma='" + $(p_tr).attr("id_cronograma") + "']"
								+ "[id_etapa='" + $(p_tr).attr("id_etapa") + "']").hide();
							
							$contenedor.parent().find("tr.gantt_partidas" + p_ggantt + "[id_cronograma='" + $(p_tr).attr("id_cronograma") + "']"
								+ "[id_etapa='" + $(p_tr).attr("id_etapa") + "']").addClass("oculto");
						}
						
						break;
			case "partida":
							var siguiente_nivel;
							if( $contenedor.find("tr.cronograma_subpartidas" + p_ggantt + "[id_cronograma='" + $(p_tr).attr("id_cronograma") + "']"
									+ "[id_etapa='" + $(p_tr).attr("id_etapa") + "']" + "[id_partida='" + $(p_tr).attr("id_partida") + "']").size() > 0 )
							{
								siguiente_nivel = "tr.cronograma_subpartidas" + p_ggantt + "[id_cronograma='" + $(p_tr).attr("id_cronograma") + "']"
									+ "[id_etapa='" + $(p_tr).attr("id_etapa") + "']" + "[id_partida='" + $(p_tr).attr("id_partida") + "']";
							}
							else
							{
								siguiente_nivel = "tr.cronograma_actividades" + p_ggantt + "[id_cronograma='" + $(p_tr).attr("id_cronograma") + "']"
									+ "[id_etapa='" + $(p_tr).attr("id_etapa") + "']" + "[id_partida='" + $(p_tr).attr("id_partida") + "']";
							}
							
							if( $(p_tr).hasClass("oculto") || $contenedor.find( siguiente_nivel ).is(":hidden") )
							{  
								//Cronograma.
								$contenedor.find( siguiente_nivel ).show();
								
								$(p_tr).removeClass("oculto");
	
								//Gantt.
								$contenedor.parent().find( siguiente_nivel.replace("cronograma_subpartidas", "gantt_subpartidas")
									.replace("cronograma_actividades", "gantt_actividades")).show();
								
								$contenedor.parent().find("tr.gantt_partidas[id_cronograma='" + $(p_tr).attr("id_cronograma") + "']"
									+ "[id_etapa='" + $(p_tr).attr("id_etapa") + "']" + "[id_partida='" + $(p_tr).attr("id_partida") + "']").removeClass("oculto");
								
								//Cambiamos el icono.
								$(p_tr).find("td:first span.cronograma-mm").removeClass("ui-icon ui-icon-circle-triangle-s")
									.addClass("ui-icon ui-icon-circle-triangle-n").fadeOut(300).fadeIn(300);
							}
							else
							{
								//Cronograma.
								$contenedor.find("tr.cronograma_subpartidas" + p_ggantt + "[id_cronograma='" + $(p_tr).attr("id_cronograma") + "']"
									+ "[id_etapa='" + $(p_tr).attr("id_etapa") + "']" + "[id_partida='" + $(p_tr).attr("id_partida") + "']").hide();
								
								$contenedor.find("tr.cronograma_actividades" + p_ggantt + "[id_cronograma='" + $(p_tr).attr("id_cronograma") + "']"
									+ "[id_etapa='" + $(p_tr).attr("id_etapa") + "']" + "[id_partida='" + $(p_tr).attr("id_partida") + "']").hide();
								
								$(p_tr).addClass("oculto");
								
								//Cambiamos el icono.
								
								$(p_tr).find("td:first span.cronograma-mm").removeClass("ui-icon ui-icon-circle-triangle-n")
									.addClass("ui-icon ui-icon-circle-triangle-s").fadeOut(300).fadeIn(300);
								
								$contenedor.find("tr.cronograma_subpartidas" + p_ggantt + "[id_cronograma='" + $(p_tr).attr("id_cronograma") + "']"
									+ "[id_etapa='" + $(p_tr).attr("id_etapa") + "']" + "[id_partida='" + $(p_tr).attr("id_partida") + "'] span.cronograma-mm")
									.removeClass("ui-icon ui-icon-circle-triangle-n")
									.addClass("ui-icon ui-icon-circle-triangle-s").fadeOut(300).fadeIn(300);
								
								//Gantt.
								$contenedor.parent().find("tr.gantt_subpartidas" + p_ggantt + "[id_cronograma='" + $(p_tr).attr("id_cronograma") + "']"
									+ "[id_etapa='" + $(p_tr).attr("id_etapa") + "']" + "[id_partida='" + $(p_tr).attr("id_partida") + "']").hide();
								$contenedor.parent().find("tr.gantt_actividades" + p_ggantt + "[id_cronograma='" + $(p_tr).attr("id_cronograma") + "']"
									+ "[id_etapa='" + $(p_tr).attr("id_etapa") + "']" + "[id_partida='" + $(p_tr).attr("id_partida") + "']").hide();
							
								$contenedor.parent().find("tr.gantt_partidas[id_cronograma='" + $(p_tr).attr("id_cronograma") + "']"
									+ "[id_etapa='" + $(p_tr).attr("id_etapa") + "']" + "[id_partida='" + $(p_tr).attr("id_partida") + "']").addClass("oculto");
							}
							break;
			case "subpartida":
							//Cronograma.
							$contenedor.find("tr.cronograma_actividades" + p_ggantt + "[id_cronograma='" + $(p_tr).attr("id_cronograma") + "']"
								+ "[id_partida='" + $(p_tr).attr("id_partida") + "']" + "[id_etapa='" + $(p_tr).attr("id_etapa") + "']"
								+ "[id_subpartida='" + $(p_tr).attr("id_subpartida") + "']").toggle();
							
							$(p_tr).addClass("oculto");
							
							//Gantt.
							$contenedor.parent().find("tr.gantt_actividades" + p_ggantt + "[id_cronograma='" + $(p_tr).attr("id_cronograma") + "']" 
								+ "[id_partida='" + $(p_tr).attr("id_partida") + "']" + "[id_etapa='" + $(p_tr).attr("id_etapa") + "']"
								+ "[id_subpartida='" + $(p_tr).attr("id_subpartida") + "']").toggle();
							
							$contenedor.parent().find("tr.gantt_actividades" + p_ggantt + "[id_cronograma='" + $(p_tr).attr("id_cronograma") + "']"
								+ "[id_partida='" + $(p_tr).attr("id_partida") + "']" + "[id_etapa='" + $(p_tr).attr("id_etapa") + "']"
								+ "[id_subpartida='" + $(p_tr).attr("id_subpartida") + "']").addClass("oculto");
							
							if( $(p_tr).hasClass("oculto") )
							{
								//Cambiamos el icono.
								$(p_tr).find("td:first span.cronograma-mm").removeClass("ui-icon ui-icon-circle-triangle-s")
									.addClass("ui-icon ui-icon-circle-triangle-n").fadeOut(300).fadeIn(300);
							}
							else
							{
								//Cambiamos el icono.
								$(p_tr).find("td:first span.cronograma-mm").removeClass("ui-icon ui-icon-circle-triangle-n")
									.addClass("ui-icon ui-icon-circle-triangle-s").fadeOut(300).fadeIn(300);
							}
							
							break;
		}
		
		//Igualamos las alturas.
		fnc_igualar_alturas_cro_gantt( $(p_tr).attr("id_cronograma") );
		//fnc_tamanio_cronograma_gantt();
		
		//Deslizamos el scroll autom�ticamente.
		fnc_deslizar_scroll( p_tr );
		
		//Ocultamos las opciones del men� chiquito.
		$( "#div_acciones_cronograma_" + g_id_CronogramaActual ).hide();
	//}
}

//Funci�n para enumerar el cronograma.
function fnc_enumerar_cronograma()
{
	//Enumeramos las actividades.
	var numero_anterior;
	$("tr.cronograma_actividades").each(function(index){
		//Actualizamos el n�mero de la actividad.
		numero_anterior = $(this).find("td:eq(0) div span:first").text(); 
		$(this).find("td:eq(0) div span:first").text( index + 1 );
		
		//Actualizamos las actividades que son paralelas a esta actividad.
		//$("tr.cronograma_actividades[paralela='" + numero_anterior + "'] td:eq(0) div span:eq(1)").text( "-" + (index + 1) );
	});
	
	//Deshacemos todas las actividades paralelas.
	$("tr.cronograma_actividades").each(function(){
		$(this).find("td:eq(0) div span:eq(1)").text("");
		$(this).removeAttr("paralela");
	});
}

//Funci�n para establecer las actividades paralelas.
function fnc_establecer_paralelas()
{
	if( $("#div_cronograma_" + g_id_CronogramaActual + " tr.cronograma_actividades[class*='seleccionado']").size() > 1 )
	{
		var actividadesXML = "";
		var primer_actividad = $("#div_cronograma_" + g_id_CronogramaActual + " tr.cronograma_actividades[class*='seleccionado']:first");
		var id_Fase;
		var nu_Ciclo;
		var nu_OrdenActividad

		$("#div_cronograma_" + g_id_CronogramaActual + " tr.cronograma_actividades[class*='seleccionado']:gt(0)").each(function(index){
			if( index == 0 )
			{
				id_Fase = $(this).attr("id_fase");
				nu_Ciclo = $(this).attr("nu_ciclo");
				nu_OrdenActividad = $(this).attr("nu_orden_actividad");
			}
			
			//Armamos un XML para enviar a guardar todas las actividades paralelas.
			actividadesXML += "<actividad>" +
									"<id_fase>" + $(this).attr("id_fase") + "</id_fase>" +
									"<id_cronograma_actividad>" + $(this).attr("id_cronograma_actividad") + "</id_cronograma_actividad>" +
									"<nb_actividad>" + fnc_codificar( $(this).find("td.nombre_Actividad span:eq(1)").text() ) + "</nb_actividad>" +
									"<nu_horas_planeadas>" + $(this).find("td.horasP").text() + "</nu_horas_planeadas>" +
									"<nu_orden_actividad>" + $(this).attr("nu_orden_actividad") + "</nu_orden_actividad>" +
									"<nu_paralela>" + $(primer_actividad).attr("nu_orden_actividad") + "</nu_paralela>" +
									"<nu_ciclo>" + $(this).attr("nu_ciclo") + "</nu_ciclo>" +
							  "</actividad>";
		});
		
		actividadesXML = "<root>" + actividadesXML + "</root>";
	
		//Actualizamos
		fnc_peticionAjax({
			url: "./application/cronograma.cfc",
			parametros: "method=CronogramaActividades_EditarXML"
				+ "&id_Empresa=" + g_row_proyectos.id_Empresa
				+ "&id_Cliente=" + g_row_proyectos.id_Cliente
				+ "&id_Proyecto=" + g_row_proyectos.id_Proyecto
				+ "&id_Cronograma=" + g_id_CronogramaActual
				+ "&id_Fase=" + id_Fase
				+ "&nu_Ciclo=" + nu_Ciclo
				+ "&nu_OrdenActividad=" +  nu_OrdenActividad
				+ "&sn_calcularFechas=1"
				+ "&actividadesXML=" + actividadesXML
				+ "&sn_Cotizacion=" + $("#hid_sn_Cotizacion_" + g_id_CronogramaActual).val(),
			f_callback: function(){
				/*
				$("tr.cronograma_actividades[class*='seleccionado']:gt(0)").each(function(){
					//Establecemos las paralelas en pantalla.
					$(this).find("td:eq(0) div span:first").text( $(this).attr("nu_orden_actividad") + "-" 
						+ $(primer_actividad).attr("nu_orden_actividad") + "." );
					$(this).attr( "nu_paralela", $(primer_actividad).attr("nu_orden_actividad") );
				});
				
				$("tr.cronograma_actividades").removeClass("ui-state-highlight seleccionado");
				*/
				
				//Actualizamos el cronograma en pantalla.
				fnc_recargar_cronograma(true);
				
				//Cronograma modificado.
				$("#hid_sn_CronogramaModificado_" + g_id_CronogramaActual).val(1);
			}
		});
	}
	else
		fnc_notificacion("Debe elegir por lo menos dos actividades.");
}

//Funci�n para deshacer las paralelas.
function fnc_deshacer_paralelas()
{
	var actividadesXML = "";
	
	if( $("#div_cronograma_" + g_id_CronogramaActual + " tr.cronograma_actividades[class*='seleccionado']").size() > 0 )
	{ 
		var id_Fase;
		var nu_Ciclo;
		var nu_OrdenActividad
		
		$("#div_cronograma_" + g_id_CronogramaActual + " tr.cronograma_actividades[class*='seleccionado']").each(function(index){
			if( index == 0 )
			{
				id_Fase = $(this).attr("id_fase");
				nu_Ciclo = $(this).attr("nu_ciclo");
				nu_OrdenActividad = $(this).attr("nu_orden_actividad");
			}
			
			//Armamos un XML para enviar a guardar todas las actividades paralelas.
			actividadesXML += "<actividad>" +
									"<id_fase>" + $(this).attr("id_fase") + "</id_fase>" +
									"<id_cronograma_actividad>" + $(this).attr("id_cronograma_actividad") + "</id_cronograma_actividad>" +
									"<nb_actividad>" + fnc_codificar( $(this).find("td.nombre_Actividad span:eq(1)").text() ) + "</nb_actividad>" +
									"<nu_horas_planeadas>" + $(this).find("td.horasP").text() + "</nu_horas_planeadas>" +
									"<nu_orden_actividad>" + $(this).attr("nu_orden_actividad") + "</nu_orden_actividad>" +
									"<nu_paralela></nu_paralela>" +
									"<nu_ciclo>" + $(this).attr("nu_ciclo") + "</nu_ciclo>" +
							  "</actividad>";
		});
		
		actividadesXML = "<root>" + actividadesXML + "</root>";
	
		//Actualizamos
		fnc_peticionAjax({
			url: "./application/cronograma.cfc",
			parametros: "method=CronogramaActividades_EditarXML"
				+ "&id_Empresa=" + g_row_proyectos.id_Empresa
				+ "&id_Cliente=" + g_row_proyectos.id_Cliente
				+ "&id_Proyecto=" + g_row_proyectos.id_Proyecto
				+ "&id_Cronograma=" + g_id_CronogramaActual
				+ "&id_Fase=" + id_Fase
				+ "&nu_Ciclo=" + nu_Ciclo
				+ "&nu_OrdenActividad=" +  nu_OrdenActividad
				+ "&sn_calcularFechas=1"
				+ "&actividadesXML=" + actividadesXML
				+ "&sn_Cotizacion=" + $("#hid_sn_Cotizacion_" + g_id_CronogramaActual).val(),
			f_callback: function(){
				//Deshacemos las paralelas en pantalla.
				/*
				$("#div_cronograma tr.cronograma_actividades[class*='seleccionado']").each(function(){
					$(this).find("td:eq(0) div span:first").text( $(this).attr("nu_orden_actividad") + "." );
					$(this).attr("nu_paralela", "");
				});
		
				$("tr.cronograma_actividades").removeClass("ui-state-highlight seleccionado");*/
				
				//Actualizamos el cronograma.
				fnc_recargar_cronograma(true);
				
				//Cronograma modificado.
				$("#hid_sn_CronogramaModificado_" + g_id_CronogramaActual).val(1);
			}
		});
	}
	else
		fnc_notificacion("Debe elegir por lo menos una actividad.");
}

//Funci�n para guardar el cronograma.
function fnc_guardar_conograma()
{
	//Armamos un XML para mandar a guardar el cronograma.
	var cronogramaXML = "";
	
	//Actividades.
	$("tr.cronograma_actividades").each(function(){
		cronogramaXML +=
			"<actividad id_fase='" + $(this).attr("id_fase") + "'>" +
				"<id_fase>" + $(this).attr("id_fase") + "</id_fase>" +
				"<id_requerimiento>" + $(this).attr("id_requerimiento") + "</id_requerimiento>" +
				"<id_tipo_actividad>1</id_tipo_actividad>" +
				"<sn_general>" + $(this).attr("sn_general") + "</sn_general>" +
				"<sn_obligatoria>" + $(this).attr("sn_obligatoria") + "</sn_obligatoria>" +
				"<nu_ciclo>" + $(this).attr("nu_ciclo") + "</nu_ciclo>" +
				"<nb_actividad>" + fnc_codificar( $.trim( $(this).find("td:eq(0) div span:eq(3)").text() ) ) + "</nb_actividad>" +
				"<nu_horas>" + $(this).find("td:eq(1)").text() + "</nu_horas>" +
			"</actividad>";
	});	
	
	cronogramaXML = "<root>" + cronogramaXML + "</root>";
	
	var parametros = "method=Cronograma_Guardar&id_Empresa=" + $("#hid_id_Empresa").val()
		+ "&id_Cliente=" + g_row_proyectos.id_Cliente + "&id_Proyecto=" + g_row_proyectos.id_Proyecto
		+ "&cronogramaXML=" + cronogramaXML;
	
	fnc_peticionAjax({
		url: "./application/cronograma.cfc",
		parametros: parametros,
		f_callback: function(){
			//Cargamos el cronograma.
			fnc_recargar_cronograma(true);
		}
	});
}

//Funci�n para mostrar u ocultar el cat�logo de los riesgos.
function fnc_mostrar_ocultar_catalogo_recursos()
{ 
	if( $("#chk_catalogo_recursos").is(":checked") )
	{
		//Mostramos el cat�logo.
		fnc_mostrar_ocultar_cat_recursos(true);
		g_ocultar_catalogo_recursos = false;
	}
	else
	{
		//Ocultamos el cat�logo.
		fnc_mostrar_ocultar_cat_recursos(false);
		g_ocultar_catalogo_recursos = true;
	}
}

//Funci�n para mostrar el cat�logo de los recursos.
function fnc_mostrar_ocultar_cat_recursos(p_mostrar)
{
	if( p_mostrar )
	{
		//Mostramos el cat�logo.
		$("#div_recursos_asignados").css({
			float: "right",
			width: "49%"
		});
		
		$("#div_recursos_disponibles").show();
	}
	else
	{
		//Ocultamos el cat�logo.
		$("#div_recursos_disponibles").hide();
		
		$("#div_recursos_asignados").css({
			float: "none",
			width: "100%"
		});
		
		$("#chk_catalogo_recursos")[0].checked = false;
	}
}

//Funci�n para recargar el cronograma.
function fnc_recargar_cronograma(p_reemplazar, p_fnc_callback)
{ 
	//Obtenemos las partidas, subpartidas y actividades visibles.
	var llave = "";
	
	var array_actividades_visibles = new Array();
	$( "#div_cronograma_contenido_" + g_id_CronogramaActual + " tr.cronograma_actividades:visible" ).each(function(){
		llave = $(this).attr("id_requerimiento") + "_" + $(this).attr("id_partida") + "_" + $(this).attr("id_etapa") + "_" + $(this).attr("id_subpartida");
		
		if( $.inArray( llave, array_actividades_visibles ) == - 1 )
			array_actividades_visibles.push( llave );
	});
	
	var array_subpartidas_visibles = new Array();
	$( "#div_cronograma_contenido_" + g_id_CronogramaActual + " tr.cronograma_subpartidas:visible" ).each(function(){
		llave = $(this).attr("id_requerimiento") + "_" + $(this).attr("id_partida") + "_" + $(this).attr("id_etapa") + "_" + $(this).attr("id_subpartida");
		
		if( $.inArray( llave, array_subpartidas_visibles ) == - 1 )
			array_subpartidas_visibles.push( llave );
	});
	
	var array_partidas_visibles = new Array();
	$( "#div_cronograma_contenido_" + g_id_CronogramaActual + " tr.cronograma_partidas:visible" ).each(function(){
		llave = $(this).attr("id_requerimiento") + "_" + $(this).attr("id_etapa") + "_" + $(this).attr("id_partida");
		
		if( $.inArray( llave, array_partidas_visibles ) == - 1 )
			array_partidas_visibles.push( llave );
	});
	
	array_actividades_visibles = array_actividades_visibles.join(",");
	array_subpartidas_visibles = array_subpartidas_visibles.join(",");
	array_partidas_visibles = array_partidas_visibles.join(",");

	fnc_peticionAjax({
		url: "./operaciones/cronograma.cfm",
		parametros: "id_Empresa=" + g_row_proyectos.id_Empresa 
			+ "&id_Cliente=" + g_row_proyectos.id_Cliente 
			+ "&id_Proyecto=" + g_row_proyectos.id_Proyecto
			+ "&id_Cronograma=" + g_id_CronogramaActual 
			+ "&sn_Cotizacion=" + $("#hid_sn_Cotizacion_" + g_id_CronogramaActual).val()
			+ "&sn_Cancelado=" + $("#hid_sn_Cancelado_" + g_id_CronogramaActual).val()
			+ "&sn_OcultarAcitividades=" + ( p_reemplazar ? "0" : "1" )
			+ "&sn_OcultarSubPartidas=" + ( p_reemplazar ? "0" : "1" )
			+ "&sn_OcultarPartidas=" + ( p_reemplazar ? "0" : "1" )
			+ "&partidas_visibles=" + array_partidas_visibles
			+ "&subpartidas_visibles=" + array_subpartidas_visibles
			+ "&actividades_visibles=" + array_actividades_visibles
			+ "&ancho_cronograma=" + $( "#div_cronograma_" + g_id_CronogramaActual ).css("width")
			+ "&ancho_gantt=" + $( "#div_gantt_" + g_id_CronogramaActual ).css("width"),
		div_success: ( p_reemplazar ? "div_cronograma_contenido_" + g_id_CronogramaActual : "div_respuesta_ajax" ),
		automensaje: false,
		f_callback: function(){
			if( p_fnc_callback )
				p_fnc_callback();
			
			fnc_tamanio_cronograma_gantt();
		}
	});
}

//Funci�n para cuando se da clic en el bot�n de Contrl de Actividades.
function fnc_control_actividades(p_accion, p_id_Cronograma)
{
	g_id_CronogramaActual = p_id_Cronograma;
	
	switch(p_accion)
	{
		case 1: //Agregar.
				$("#div_ordenar_cronograma").hide();
				$("#div_ca_fases, #div_nueva_actividad").show();
				$("#btn_ca_guardar").attr("onClick", "fnc_guardar_actividad_cronograma();");
				
				gdialogo.abrir("gd_control_actividades");
				
				$("#ddl_ca_fases").focus();
				break;
		case 2: //Editar.
				fnc_editar_actividad_cronograma(false);
				break;
		case 3: //Eliminar.
				fnc_eliminar_actividad_cronograma(1);
				break;
		case 4: //Ordenar.
				fnc_ordenar_actividad_cronograma();				
				break;
		case 5: //Establecer paralelas.
				fnc_establecer_paralelas();
				break;
		case 6: //Deshacer paralelas.
				fnc_deshacer_paralelas();
				break;
		case 7: //Crear L�nea Base.
				fnc_crear_linea_base_justificacion();
				break;
		case 8: //Asignaci�n masiva de recursos.
				fnc_agregar_quitar_recurso_masiva();
				break;
		case 9: //Encadenar actividades.
				fnc_encadenar_actividades();
				break;
		case 10: //Ver configuraci&oacute;n de la actividad.
				fnc_ver_configuracion_actividad();
				break;
		case 11: //Agregar Nueva Etapa al Cronograma.
					fnc_abrir_configuracion(1);
				break;
		case 12: //Agregar Nueva Partida.
					fnc_abrir_configuracion(2);
				break;
		case 13: //Cambiar nombre de etapa.
					fnc_abrir_configuracion(5);
				break;
		case 14: //Eliminar etapa.
					fnc_abrir_configuracion(6);
				break;
		case 15: //Ordenar etapas.
					fnc_abrir_configuracion(9);
				break;
		case 16: //Eliminar etapa.
					fnc_abrir_configuracion(7);
				break;
		case 17: //Agregar Nueva Partida.
					fnc_abrir_configuracion(3);
				break;
		case 18: //Agregar Nueva actividad.
					fnc_abrir_configuracion(4);
				break;
		case 19: //Ordenar Partidas.
					fnc_abrir_configuracion(10);
				break;
		case 20: //Eliminar etapa.
					fnc_abrir_configuracion(8);
				break;
		case 21: //Eliminar SubPartidas.
					fnc_abrir_configuracion(11);
				break;
		case 22: //configurar ponderacion.
					fnc_abrir_configuracion(12);
				break;
		case 23: //Proponer Cambios.
				fnc_mostrar_justificacion_proponer_cambios();
				break;
		case 24: //Desencadenar actividades.
					fnc_desencadenar_actividades();
				break;
		case 25: //Agregar planos
					 fnc_agregar_planos(1,g_id_CronogramaActual);
				break;
		case 26: //Terminar cambios.
				fnc_terminar_cambios_cronograma();
				break;
		default: break;
	}
	
	$( "#div_acciones_cronograma_" + g_id_CronogramaActual ).hide(300);
}

//Funci�n para terminar los cambios propuestos al cronograma.
function fnc_terminar_cambios_cronograma()
{
	var parametros = {
		method: "CronogramaCambios_Terminar",
		id_Empresa: g_row_proyectos.id_Empresa,
		id_Cliente: g_row_proyectos.id_Cliente,
		id_Proyecto: g_row_proyectos.id_Proyecto,
		id_Cronograma: g_id_CronogramaActual,
		nb_Proyecto: g_row_proyectos.nb_Proyecto,
		id_TipoRequerimiento: $( "#hid_id_TipoRequerimiento_" + g_id_CronogramaActual ).val(),
		id_Requerimiento: $( "#hid_id_Requerimiento_" + g_id_CronogramaActual ).val(),
		nb_TipoRequerimiento: $( "#hid_nb_TipoRequerimiento_" + g_id_CronogramaActual ).val(),
		nu_NumTienda: g_row_proyectos.nu_NumTienda,
		sn_Horas: $( "#hid_sn_Horas_" + g_id_CronogramaActual ).val(),
		id_Area: $("#hid_id_Area").val()
	};
	
	if( $( "#hid_sn_Horas_" + g_id_CronogramaActual ).val() === "0" )
		parametros.nu_EquivalenteHorasDia = $( "#hid_nu_EquivalenteHorasDia_" + g_id_CronogramaActual ).val();
	
	fnc_peticionAjax({
		url: "./application/cronograma.cfc",
		parametros: parametros,
		f_callback: function(){
			//Recargamos el cronograma.
			fnc_recargar_cronograma(true);
		}
	})
}

//Funci�n para edtiar la configuraci�n de una actividad.
function fnc_configuracion_actividad_editar(p_de_Negrita, p_de_Color, p_nu_Tamanio)
{
	//Guardamos la configuraci�n de la actividad.
	fnc_peticionAjax({
		url: "./application/app_config_act.cfc",
		parametros: {
			method: "ConfiguracionActividades_Editar",
			id_Empresa: g_row_proyectos.id_Empresa,
			id_Cliente: g_row_proyectos.id_Cliente,
			id_Proyecto: g_row_proyectos.id_Proyecto,
			id_Cronograma: g_id_CronogramaActual,
			id_CronogramaActividad: $(g_tr_actividad_seleccionado).attr("id_cronograma_actividad"),
			de_Negrita: p_de_Negrita,
			de_Color: p_de_Color,
			nu_Tamanio: p_nu_Tamanio
		},
		f_callback: function(){
			$("#div_color_actual").css({
				background: p_de_Color
			});
			
			$('#div_ul_colores_actividades').hide();
			
			$(g_tr_actividad_seleccionado).find("td").css({
				color: p_de_Color,
				fontWeight: p_de_Negrita,
				fontSize: p_nu_Tamanio + "px"
			});
			
			$(g_tr_actividad_seleccionado).attr({
				de_colorA: p_de_Color,
				de_negrita: p_de_Negrita,
				nu_tamanio: p_nu_Tamanio
			});
		}
	});
}

//Funci�n para poner en negrita la actividad.
function fnc_negrita_actividad(p_checked)
{
	//Configuramos.
	fnc_configuracion_actividad_editar( ( p_checked == true ? "bold" : "normal" ), $(g_tr_actividad_seleccionado).attr("de_colorA")
		, $(g_tr_actividad_seleccionado).attr("nu_tamanio") );
}

//Funci�n para seleccionar el color de la actividad.
function fnc_color_actividad(p_div)
{
	//Configuramos.
	fnc_configuracion_actividad_editar( ( $("#chk_de_Negrita")[0].checked == true ? "bold" : "normal" ), $(p_div).attr("de_color")
		, $(g_tr_actividad_seleccionado).attr("nu_tamanio") );
}

//Funci�n para cambiar el tama�o de la actividad.
function fnc_tamanio_actividad(p_value)
{
	//Configuramos.
	fnc_configuracion_actividad_editar( ( $("#chk_de_Negrita")[0].checked == true ? "bold" : "normal" ), $(g_tr_actividad_seleccionado).attr("de_colorA")
		, p_value );
}

//Funci�n para ver la configuraci�n de la actividad.
function fnc_ver_configuracion_actividad()
{
	if( $("#div_cronograma_" + g_id_CronogramaActual + " tr.cronograma_actividades[class*='seleccionado']").size() == 1 )
	{
		//Validamos las areas
		var id_Area = $( "#div_cronograma_" + g_id_CronogramaActual + " tr.cronograma_actividades.seleccionado" ).attr("id_Area");

		if( $( "#hid_id_CronogramaEstatus_" + g_id_CronogramaActual ).val() === "6" && id_Area != $("#hid_id_Area").val() )
		{
			fnc_mostrar_aviso( "gd_avisos", "Estas actividades no corresponden a su &Aacute;rea.", 5 );
			return;
		}
		
		g_tr_actividad_seleccionado = $("#div_cronograma_" + g_id_CronogramaActual + " tr.cronograma_actividades[class*='seleccionado']");
		
		$("#ul_tipos_dependencias input").attr("checked", false).attr("disabled", "disabled");
		
		//Elegimos el color actual de la actividad.
		$("#div_color_actual").css({
			background: $(g_tr_actividad_seleccionado).attr("de_colorA")
		});
		
		//Elegimos si es negrita.
		$("#chk_de_Negrita")[0].checked = ( $(g_tr_actividad_seleccionado).attr("de_negrita") == "normal" ? false : true );
		
		//Elegimos el tama�o de la actividad.
		$("#ddl_tamanio_act").val( $(g_tr_actividad_seleccionado).attr("nu_tamanio") );
		
		$("#div_contenido").css({
			height: $("#div_contenido").height() + 300,
			overflow: "auto"
		});
		
		//Mostramos el panel de configuraci�n.
		$("#div_configuraciones_cronograma, #div_configuraciones_cronograma_fondo").slideToggle();
		
		setTimeout(function(){
			//Par�metros para las predecesoras y sucesoras.
			var parametros = "&id_Empresa=" + g_row_proyectos.id_Empresa
				+ "&id_Cliente=" + g_row_proyectos.id_Cliente
				+ "&id_Proyecto=" + g_row_proyectos.id_Proyecto
				+ "&id_Cronograma=" + g_id_CronogramaActual
				+ "&id_CronogramaActividad=" +$(g_tr_actividad_seleccionado).attr("id_cronograma_actividad");
				
			//Predecesoras.
			$("#grid_predecesoras").attr("parametros", parametros);
			ggrids.crear("grid_predecesoras");
			
			//Sucesoras.
			$("#grid_sucesoras").attr("parametros", parametros)
			ggrids.crear("grid_sucesoras");
		}, 200);
	}
	else
		fnc_notificacion("S&oacute;lo puede configurar <b>una</b> actividad a la vez.");
}

//Funci�n para encadenar actividades.
function fnc_encadenar_actividades()
{
	//Validar que haya m�s de una actividad seleccionada.
	if( $("#div_cronograma_contenido_" + g_id_CronogramaActual + " tr.cronograma_actividades.seleccionado").size() > 1 )
	{
		var id_Requerimiento = null;
		var id_Partida = null;
		var id_CronogramaActividad = null;
		//Guardamos las actividades en un array para ordenarlas de acuerdo al �rden del encadenamiento.
		var array_actividades = new Array();
		
		$("#div_cronograma_contenido_" + g_id_CronogramaActual + " tr.cronograma_actividades.seleccionado").each(function(index, tr){
			array_actividades.push({
				id_Requerimiento: $(tr).attr("id_requerimiento"),
				nu_Ciclo: $(tr).attr("nu_ciclo"),
				id_Partida: $(tr).attr("id_partida"),
				id_CronogramaActividad: $(tr).attr("id_cronograma_actividad"),
				orden_encadenamiento: $(tr).attr("orden_encadenamiento") * 1,
			});
		});
		
		//Ordenamos.
		array_actividades.sort(function(a, b){
			if (a.orden_encadenamiento < b.orden_encadenamiento)
				return -1;
			if(a.orden_encadenamiento > b.orden_encadenamiento)
				return 1;
				
			return 0;
		});
		
		//Armamos un XML para mandar a guardar todas las actividades.
		var actividadesXML = "";
		var actividad_dependiente;
		
		$.each(array_actividades, function(index, obj){
			actividad_dependiente = $("#div_cronograma_contenido_" + g_id_CronogramaActual + " tr.cronograma_actividades.seleccionado[orden_encadenamiento='" + ( obj.orden_encadenamiento - 1 ) + "']");
			//La primer relaci�n no se guarda porque las dem�s actividades se encadenan a �sta.
			if( obj.orden_encadenamiento > 1 )
			{
				actividadesXML += '<actividad>' +
									'<id_CronogramaActividad>' + obj.id_CronogramaActividad + '</id_CronogramaActividad>' +
									'<id_CronogramaActividadRelacion>' + $(actividad_dependiente).attr("id_cronograma_actividad") + '</id_CronogramaActividadRelacion>' +
								 '</actividad>';
				
				if( obj.orden_encadenamiento == 2 )
				{
					id_Requerimiento = obj.id_Requerimiento;
					id_Partida = obj.id_Partida;
					id_CronogramaActividad = obj.id_CronogramaActividad;		
				}
			}
		});

		//Guardamos.
		fnc_peticionAjax({
			url: "./application/app_config_act.cfc",
			parametros: {
				method: "CronogramaActividadesDependencias_Agregar",
				id_Empresa: g_row_proyectos.id_Empresa,
				id_Cliente: g_row_proyectos.id_Cliente,
				id_Proyecto: g_row_proyectos.id_Proyecto,
				id_Cronograma: g_id_CronogramaActual,
				id_Requerimiento: id_Requerimiento,
				id_CronogramaActividad: id_CronogramaActividad,
				sn_Cotizacion: $("#hid_sn_Cotizacion_" + g_id_CronogramaActual).val(),
				actividadesXML: "<root>" + actividadesXML + "</root>",
				sn_ActividadesRetrasadas: 0,
				id_TipoRequerimiento: $( "#hid_id_TipoRequerimiento_" + g_id_CronogramaActual ).val(),
				id_TipoRelacionActividad: 1
			},
			f_callback: function(){
				//Cargamos el cronograma.
				fnc_recargar_cronograma(true);
			}
		});
	}
	else
		fnc_notificacion("Debe seleccionar por lo menos <b>dos</b> actividades.");
}

//Funci�n para desencadenar actividades.
function fnc_desencadenar_actividades()
{
	var id_Partida = null;
	var id_CronogramaActividad = null;
		
	var $actividades = $("#div_cronograma_contenido_" + g_id_CronogramaActual + " tr.cronograma_actividades.seleccionado");
	var actividadesXML = '';

	if( $actividades.size() === 1 )
	{
		//Obtenemos las dependencias a eliminar.
		$actividades.find(".div_predecesoras span").each(function(){
			actividadesXML += '' +
				'<actividad>' +
					'<id_CronogramaActividad>' + $(this).attr("id_cronograma_actividad") + '</id_CronogramaActividad>' +
					'<id_CronogramaActividadRelacion>' + $(this).attr("id_cronograma_actividad_relacion") + '</id_CronogramaActividadRelacion>' +
				'</actividad>';
		});
		
		id_Partida = $actividades.attr("id_partida");
		id_CronogramaActividad = $actividades.attr("id_cronograma_actividad");
	}
	else
	{
		//Recorremos las Actividades
		$actividades.each(function(index){
			var $act = $(this);
			var $act_eliminar = null;
			
			$act_eliminar = $actividades.find('.div_predecesoras span[id_cronograma_actividad_relacion="' + $act.attr('id_cronograma_actividad') + '"]');

			if( $act_eliminar.size() > 0 )
			{
				actividadesXML += '' +
					'<actividad>' +
						'<id_CronogramaActividad>' + $act_eliminar.attr("id_cronograma_actividad") + '</id_CronogramaActividad>' +
						'<id_CronogramaActividadRelacion>' + $act_eliminar.attr("id_cronograma_actividad_relacion") + '</id_CronogramaActividadRelacion>' +
					'</actividad>';
			}
			
			if( index === 0 )
			{
				id_Partida = $act.attr("id_partida");
				id_CronogramaActividad = $act.attr("id_cronograma_actividad");	
			}
		});
	}

	//Eliminamos las actividades predecesoras.
	if( actividadesXML !== "" )
	{
		fnc_peticionAjax({
			url: "./application/app_config_act.cfc",
			parametros: {
				method: "CronogramaActividadesDependencias_EliminarXML",
				id_Empresa: g_row_proyectos.id_Empresa,
				id_Cliente: g_row_proyectos.id_Cliente,
				id_Proyecto: g_row_proyectos.id_Proyecto,
				id_Cronograma: g_id_CronogramaActual,
				id_Requerimiento: $( "#hid_id_Requerimiento_" + g_id_CronogramaActual ).val(),
				id_CronogramaActividad: id_CronogramaActividad,
				sn_Cotizacion: $("#hid_sn_Cotizacion_" + g_id_CronogramaActual).val(),
				actividadesXML: "<root>" + actividadesXML + "</root>",
				sn_ActividadesRetrasadas: 0,
				id_TipoRequerimiento: $( "#hid_id_TipoRequerimiento_" + g_id_CronogramaActual ).val()
			},
			f_callback: function(){
				//Cargamos el cronograma.
				fnc_recargar_cronograma(true);
			}
		});
	}
}

//Funci�n para agregar o quitar un recurso masivamente.
function fnc_agregar_quitar_recurso_masiva()
{
	//Armamos los par�metros.
	var parametros = {
		id_Empresa: g_row_proyectos.id_Empresa,
		id_Cliente: g_row_proyectos.id_Cliente,
		id_Proyecto: g_row_proyectos.id_Proyecto,
		id_Cronograma: g_id_CronogramaActual,
		sn_Cotizacion: $( "#hid_sn_Cotizacion_" + g_id_CronogramaActual ).val()
	};
	
	if( $( "#hid_sn_Cotizacion_" + g_id_CronogramaActual ).val() == "0" )
	{
		//Validamos que las actividades pertenezcan a la misma �rea.
		var id_Area = null;
		$( "#div_cronograma_" + g_id_CronogramaActual + " tr.cronograma_actividades[class*='seleccionado']" ).each(function(index){
			if( index === 0 )
			{
				id_Area = $(this).attr("id_Area");
			}
			else
			{
				if( id_Area !== $(this).attr("id_Area") )
				{
					fnc_mostrar_aviso( "gd_avisos", "Las actividades deben pertenecer a la misma &Aacute;rea.", 5 );
					id_Area = null;
					return false;
				}
			}
		});
		
		if( id_Area )
		{
			parametros.id_Area = id_Area
			
			//Validamos si se pueden asignar los recursos masivamente.
			if( $( "#hid_id_CronogramaEstatus_" + g_id_CronogramaActual ).val() === "6" && id_Area != $("#hid_id_Area").val() )
			{
				fnc_mostrar_aviso( "gd_avisos", "Estas actividades no corresponden a su &Aacute;rea.", 5 );
				return;
			}
		}
		else
			return;
	}

	//Consultamos el Equipo de Trabajo actual.
	fnc_peticionAjax({
		url: "./application/adm_proy.cfc?method=EquipoTrabajo_Listar",
		parametros: parametros,
		automensaje: false,
		f_callback: function(respuestaJSON){
			
			//Llenamos el combo.
			$("#ddl_recursos_asig_masiva option[value!='0']").remove();
			
			$.each(respuestaJSON.RS.DATA.ID_EQUIPOTRABAJO, function(index, value){
				var nombre_recurso = respuestaJSON.RS.DATA.NB_NOMBRECOMPLETORECURSO[index];
				if( $( "#hid_sn_Cotizacion_" + g_id_CronogramaActual ).val() == "1" )
					nombre_recurso = respuestaJSON.RS.DATA.NB_ALIAS[index];
				
				$("<option>").attr({
					value: value,
				}).text( nombre_recurso ).appendTo("#ddl_recursos_asig_masiva");
			});
			
			$("#table_tiempos_actividades_masiva tbody tr").remove();
			$( "#div_cronograma_" + g_id_CronogramaActual + " tr.cronograma_actividades[class*='seleccionado']" ).each(function(){
				var sn_Horas = $(this).attr("sn_horas");

				if( sn_Horas == "1" )
				{
					/*var nu_HorasPLantilla = $(this).attr("nu_horas_plantilla").split(".")[0] * 1;
					var nu_MinutosPlantilla = ( ( ( $(this).attr("nu_horas_plantilla").split(".").length == 2 ? "."
						+ $(this).attr("nu_horas_plantilla").split(".")[1] : 0 ) * 1 ) * 60 ).toFixed(2) * 1;*/
					
					var nu_HorasPLantilla = $(this).find("td.horasP span:first").text().split(".")[0] * 1;
					var nu_MinutosPlantilla = ( ( ( $(this).find("td.horasP span:first").text().split(".").length == 2 ? "."
						+ $(this).find("td.horasP span:first").text().split(".")[1] : 0 ) * 1 ) * 60 ).toFixed(2) * 1;
					
					nu_HorasPLantilla = ( nu_HorasPLantilla == 0 ? "" : Math.round(nu_HorasPLantilla, 0) );
					nu_MinutosPlantilla = ( nu_MinutosPlantilla == 0 ? "" : Math.round(nu_MinutosPlantilla, 0) );
					
					var td_tiempo = '' +
							'<input type="text" id="txt_horas_act_cro_masivo_' + $(this).attr("id_requerimiento") +
								'_' + $(this).attr("id_cronograma_actividad") + '" class="solo_numeros" style="width: 50px; text-align: center;"' +
                                ' maxlength="3" title="Ingrese las horas" value="' + nu_HorasPLantilla + '" />' +
							'<span>:</span>' +
							'<input type="text" id="txt_minutos_act_cro_masivo_' + $(this).attr("id_requerimiento") + '_' + 
								$(this).attr("id_cronograma_actividad") + '" class="input_text solo_numeros" style="width: 50px; text-align: center;"' + 
                                ' maxlength="2" title="Ingrese los minutos" value="' + nu_MinutosPlantilla + '" /> horas';
				}
				else
				{
					var dias = $(this).find("td.horasP span:first").text();
					
					var td_tiempo = '' +
						'<input type="text" id="txt_dias_act_cro_masivo_' + $(this).attr("id_requerimiento") + '_' + $(this).attr("id_cronograma_actividad") +
								'" class="solo_numeros" style="width: 50px; text-align: center;"' +
								' maxlength="3" title="Ingrese los d&iacute;as" value="' + ( dias == "0" ? "" : dias ) + '" /> d&iacute;as'
				}
				
				$("#table_tiempos_actividades_masiva").append(
					'<tr>' +
						'<td title="' + $(this).find("td.nombre_Actividad div:first span:eq(1)").text() + '">' +
							$(this).find("td.nombre_Actividad div:first span:eq(1)").text() +
						'</td>' +
						'<td align="center" style="text-align: center;">' +
							td_tiempo +
						'</td>' +
					'</tr>'
				)
			});
			
			fnc_inicializar_controles("#div_asig_masiva_recursos");
			
			gdialogo.abrir("gd_asig_masiva_recursos");
			$(".chk_asignacion_masiva[valor='asignar']").trigger("click");
		}
	});
}

//Funci�n para agregar o quitar un recurso masivamente.
function fnc_asignar_eliminar_recurso_masivamente_guardar()
{
	if( fnc_validar_campos_obligatorios("div_asignar_eliminar_recurso_masivamente_campos") )
	{
		//Armamos un XML con todas las actividades seleccionadas.
		var actividadesXML = "";
		var primer_actividad = $( "#div_cronograma_" + g_id_CronogramaActual + " tr.cronograma_actividades[class*='seleccionado']:first" );
		
		var bandera = true;
		$( "#div_cronograma_" + g_id_CronogramaActual + " tr.cronograma_actividades[class*='seleccionado']" ).each(function(){
			var id = $(this).attr("id_requerimiento") + "_" + $(this).attr("id_cronograma_actividad");
			
			//Validar que se haya ingresado una duraci�n para todas las actividades.
			var validacion_horas = $(this).attr("sn_horas") == "1" && ( $("#txt_horas_act_cro_masivo_" + id).val() * 1 > 0 || $("#txt_minutos_act_cro_masivo_" + id).val() * 1 > 0 );
			var validacion_dias = $(this).attr("sn_horas") == "0" && $("#txt_dias_act_cro_masivo_" + id).val() * 1 > 0;

			if( ( $(".chk_asignacion_masiva[valor='asignar']").hasClass("chk_asignacion_masiva_seleccionado") 
					&& ( validacion_horas || validacion_dias ) )
				|| $(".chk_asignacion_masiva[valor='eliminar']").hasClass("chk_asignacion_masiva_seleccionado") )
			{ 
				actividadesXML += "<actividad>" +
										"<nu_ciclo>" + $(this).attr("nu_ciclo") + "</nu_ciclo>" +
										"<id_cronograma_actividad>" + $(this).attr("id_cronograma_actividad") + "</id_cronograma_actividad>" +
										"<nu_orden_actividad>" + $(this).attr("nu_orden_actividad") + "</nu_orden_actividad>" +
										"<nu_tiempo_planeado>" + 
											( $(this).attr("sn_horas") == "1" 
												?
													( $("#txt_horas_act_cro_masivo_" + id).val() * 1 
														+ fnc_convertir_a_horas( $("#txt_minutos_act_cro_masivo_" + id).val(), "m" ) )
												: 
													( ( $("#txt_dias_act_cro_masivo_" + id).val() * 1 ) * $("#hid_nu_EquivalenteHorasDia_" + g_id_CronogramaActual).val() * 1 )
											) +
										"</nu_tiempo_planeado>" +
								  "</actividad>";
			}
			else
			{
				fnc_mostrar_aviso("gd_avisos", "Debe ingresar una duraci&oacute;n para todas las actividades.", 5);
				bandera = false;
				return false;
			}
		});

		if( bandera )
		{
			actividadesXML = "<root>" + actividadesXML + "</root>";
			
			//Asignamos/Eliminamos.
			fnc_peticionAjax({
				url: "./application/cronograma.cfc",
				parametros: {
					method: ( $(".chk_asignacion_masiva[valor='asignar']").hasClass("chk_asignacion_masiva_seleccionado") ? "CronogramaRecursosActividadMasivo_Agregar" : "CronogramaRecursosActividadMasivo_Eliminar" ),
					id_Empresa: g_row_proyectos.id_Empresa,
					id_Cliente: g_row_proyectos.id_Cliente,
					id_Proyecto: g_row_proyectos.id_Proyecto,
					id_Cronograma: g_id_CronogramaActual,
					nu_Ciclo: $(primer_actividad).attr("nu_ciclo"),
					nu_OrdenActividad: $(primer_actividad).attr("nu_orden_actividad"),
					id_EquipoTrabajo: $("#ddl_recursos_asig_masiva option:selected").val(),
					sn_Cotizacion: $( "#hid_sn_Cotizacion_" + g_id_CronogramaActual ).val(),
					id_Requerimiento: $( "#hid_id_Requerimiento_" + g_id_CronogramaActual ).val(),
					actividadesXML: actividadesXML,
					sn_ActividadesRetrasadas: 0,
					id_TipoRequerimiento: $( "#hid_id_TipoRequerimiento_" + g_id_CronogramaActual ).val(),
				},
				automensaje: true,
				f_callback: function(respuestaJSON){
					//Cargamos el cronograma.
					fnc_recargar_cronograma(true);
					
					gdialogo.cerrar("gd_asig_masiva_recursos");
				}
			});
		}
	}
}

//Funci�n para cuando se elige una fase.
function fnc_onChage_ca_fases()
{
	g_array_posiciones = new Array();
	$("#div_ordenar_cronograma").show();
	$("#div_ordenar_cronograma table tbody tr").remove();
	
	$("#div_cronograma_" + g_id_CronogramaActual + " tr.cronograma_actividades[nu_ciclo='" + $("#ddl_ca_fases option:selected").attr("nu_ciclo") + "'][id_fase='" + $("#ddl_ca_fases option:selected").attr("id_fase") + "']").each(function(){

		$("#div_ordenar_cronograma table thead tr th").text( $("#ddl_ca_fases option:selected").text() );
		
		$("#div_ordenar_cronograma table tbody").append(
			"<tr class='cronograma_actividades' id_fase='" + $(this).attr("id_fase") +
				"' id_cronograma_actividad='" + $(this).attr("id_cronograma_actividad") +
				"' nu_horas_planeadas='" + $(this).find("td:eq(3)").text() +
				"' nu_paralela='" + $(this).attr("nu_paralela") + 
				"' nu_orden_actividad='" + $(this).attr("nu_orden_actividad") + "'>" +
				"<td>" +
					"<span style='font-weight: bold;'>" + $(this).attr("nu_orden_actividad") + ". </span>" +
					"<span>" + $(this).find("td:first span:eq(1)").text() + "</span>" +
				"</td>" +
			"</tr>"
		);
		
		//Guardamos las posiciones.
		g_array_posiciones.push( $(this).attr("nu_orden_actividad") );
	});
	
	$("#div_ordenar_cronograma table tbody").sortable({
		items: "tr[class='cronograma_actividades']",
		stop: function(){
			//Reordenamos las actividades.
			$("#div_ordenar_cronograma table tbody tr").each(function(index){
				$(this).find("td:first span:first").text( g_array_posiciones[index] + ". " );
				$(this).attr( "nu_orden_actividad", g_array_posiciones[index] );
			});
		}
	});

	gdialogo.re_ajustar("gd_control_actividades");
}

//Funci�n para guardar una nueva actividad en el cronograma.
function fnc_guardar_actividad_cronograma()
{
	if( fnc_validar_campos_obligatorios("div_control_actividades") )
	{
		fnc_peticionAjax({
			url: "./application/cronograma.cfc",
			parametros: "method=CronogramaActividades_Agregar&id_Fase=" + $("#ddl_ca_fases option:selected").attr("id_Fase")
				+ "&nb_Actividad=" + fnc_codificar( $.trim( $("#txt_nueva_actividad").val() ) )
				+ "&id_TipoActividad=3" + "&id_Empresa=" + g_row_proyectos.id_Empresa
				+ "&id_Cliente=" + g_row_proyectos.id_Cliente
				+ "&id_Proyecto=" + g_row_proyectos.id_Proyecto
				+ "&id_Cronograma=" + g_id_CronogramaActual
				+ "&nu_Ciclo=" + $("#ddl_ca_fases option:selected").attr("nu_ciclo")
				+ "&id_Ciclo=" + $( "#div_cronograma_contenido_" + g_id_CronogramaActual + " tr.cronograma_actividades[nu_ciclo='" + $("#ddl_ca_fases option:selected").attr("nu_ciclo") + "']:first" ).attr("id_ciclo")
				+ "&sn_General=" + ( $("#chk_actividad_general").is(":checked") ? 1 : 0 )
				+ "&sn_Obligatoria=0"
				+ "&id_Requerimiento=" + $( "#div_cronograma_contenido_" + g_id_CronogramaActual + " tr.cronograma_actividades[nu_ciclo='" + $("#ddl_ca_fases option:selected").attr("nu_ciclo") + "']:first" ).attr("id_requerimiento")
				+ "&nu_TiempoPlaneado=0"
				+ "&sn_Horas=" + $( "#div_cronograma_contenido_" + g_id_CronogramaActual + " tr.cronograma_actividades[nu_ciclo='" + $("#ddl_ca_fases option:selected").attr("nu_ciclo") + "']:first" ).attr("sn_horas")
				+ "&id_TipoRestriccion=1",
			f_callback: function(){
				//Cargamos el cronograma.
				fnc_recargar_cronograma(true);
				
				gdialogo.cerrar("gd_control_actividades");
				
				//Cronograma modificado.
				$("#hid_sn_CronogramaModificado_" + g_id_CronogramaActual).val(1);
			}
		});
	}
}

//Funci�n para elegir un Recurso del Equipo de Trabajo asignado.
function fnc_elegir_et_asignado(p_tr)
{
	$("#table_recursos_agregados .tr_et_asignado").removeClass("ui-state-highlight seleccionado");
	$(p_tr).addClass("ui-state-highlight seleccionado");
} 

//Funci�n para asignar horas extras a los Recursos.
function fnc_horas_extras_et()
{
	//Validar que se haya seleccionado un registro.
	if( $("#table_recursos_agregados tr.seleccionado").size() == 1 )
	{
		$("#span_recurso_horas_extras").text( $("#table_recursos_agregados tr.seleccionado td.td_nom_Recurso").text() );
		fnc_catalogos_mostrar_ocultar("div_et", "div_et_horas_extras");

		//Limpiamos los campos.
		if( $.trim( $("#table_recursos_agregados tr.seleccionado td.td_HorasExtras").text() ) == "0" )
		{
			$("#txt_horas_extras").val("");
			$("#txt_fh_InicioVigencia").val( $("#hid_fh_Actual").val() );
			$("#txt_fh_FinVigencia").val( $("#hid_fh_Actual").val() );
		}
		else
		{
			$("#txt_horas_extras").val( $.trim( $("#table_recursos_agregados tr.seleccionado td.td_HorasExtras").text() ) );
			$("#txt_fh_InicioVigencia").val(  $.trim( $("#table_recursos_agregados tr.seleccionado td.td_fh_InicioVigencia").text() ) );
			$("#txt_fh_FinVigencia").val( $.trim( $("#table_recursos_agregados tr.seleccionado td.td_fh_FinVigencia").text() ) );
		}
		
		$("#txt_horas_extras").focus();
	}
	else
		fnc_notificacion("Debe seleccionar un Recurso.");
}

//Funci�n para editar actividades del cronograma.
function fnc_editar_actividad_cronograma(p_guardar)
{
	if( p_guardar )
	{
		if( fnc_validar_campos_obligatorios("div_editar_act_cro") )
		{
			//Editamos.
			fnc_peticionAjax({
				url: "./application/cronograma.cfc",
				parametros:{
					method: 'CronogramaActividades_Editar',
					id_Empresa				: g_row_proyectos.id_Empresa,
					id_Cliente				: g_row_proyectos.id_Cliente,
					id_Proyecto   		    : g_row_proyectos.id_Proyecto,
					id_Cronograma			: g_id_CronogramaActual,
					id_CronogramaActividad  : $( g_tr_cronograma ).attr("id_cronograma_actividad"),
					nb_Actividad			: $.trim( $("#txt_nb_Actividad_Editar").val() )
				},
				f_callback: function(){
					//Cerramos el dialogo.
					gdialogo.cerrar("gd_editar_act_cro");
				
					var nb_actividad = $("#txt_nb_Actividad_Editar").val().trim();
					var fh_inicio = $( g_tr_cronograma ).find('.fecha_InicioP').text().trim();
					var fh_fin = $( g_tr_cronograma ).find('.fecha_FinP').text().trim();
					
					//Actualizamos la actividad en pantalla.
					$( g_tr_cronograma ).children('.nombre_Actividad').find('span').eq(1).text( nb_actividad );			
					
					//Gantt
					var title = nb_actividad + ' ( '+ fh_inicio +' - '+ fh_fin +' )';
					$("#div_gantt_" + g_id_CronogramaActual + " tr.gantt_actividades[class*='seleccionado']").attr( "title", title);
					
					//Cronograma modificado.
					$("#hid_sn_CronogramaModificado_" + g_id_CronogramaActual).val(1);
				}
			});
		}
	}
	else
	{
		//Validar que se haya elegido una actividad.
		if( $("#div_cronograma_" + g_id_CronogramaActual + " tr.cronograma_actividades[class*='seleccionado']").size() == 1 )
		{
			//Validamos las areas
			var id_Area = $( "#div_cronograma_" + g_id_CronogramaActual + " tr.cronograma_actividades.seleccionado" ).attr("id_Area");

			if( $( "#hid_id_CronogramaEstatus_" + g_id_CronogramaActual ).val() === "6" && id_Area != $("#hid_id_Area").val() )
			{
				fnc_mostrar_aviso( "gd_avisos", "Estas actividades no corresponden a su &Aacute;rea.", 5 );
				return;
			}
			
			//Validar que la actividad no sea obligatoria.
			if( $("#div_cronograma_" + g_id_CronogramaActual + " tr.cronograma_actividades[class*='seleccionado']").attr("sn_obligatorio") == "0" )
			{
				$("#txt_nb_Actividad_Editar").val( $("#div_cronograma_" + g_id_CronogramaActual + " tr.cronograma_actividades[class*='seleccionado'] td:first span:eq(1)").text() );
				gdialogo.abrir("gd_editar_act_cro");
				$("#txt_nb_Actividad_Editar").focus();
			}
			else
				fnc_mostrar_aviso( "gd_avisos", "Esta actividad no puede ser editada porque es obligatoria.", 5 );
		}
		else
		{
			if( $("#div_cronograma_" + g_id_CronogramaActual + " tr.cronograma_actividades[class*='seleccionado']").size() == 0 )
				fnc_notificacion("Debe elegir una actividad.");
			else
				fnc_notificacion("S&oacute;lo puede editar <b>una</b> actividad a la vez.");
		}
	}
}

//Funci�n para eliminar actividades del cronograma.
function fnc_eliminar_actividad_cronograma( opcion )
{
	switch( opcion )
	{
		case 1:
				//Validar que se haya elegido una actividad.
				if( $("#div_cronograma_" + g_id_CronogramaActual + " tr.cronograma_actividades[class*='seleccionado']").size() == 1 )
				{
					//Validamos las areas
					var id_Area = $( "#div_cronograma_" + g_id_CronogramaActual + " tr.cronograma_actividades.seleccionado" ).attr("id_Area");
		
					if( $( "#hid_id_CronogramaEstatus_" + g_id_CronogramaActual ).val() === "6" && id_Area != $("#hid_id_Area").val() )
					{
						fnc_mostrar_aviso( "gd_avisos", "Estas actividades no corresponden a su &Aacute;rea.", 5 );
						return;
					}
					//Validar que la actividad no sea obligatoria.
					if( $("#div_cronograma_" + g_id_CronogramaActual + " tr.cronograma_actividades[class*='seleccionado']").attr("sn_obligatorio") == "0" )
					{
						//Validar que la fase no se quede sin actividades.
						var cont = $("#div_cronograma_" + g_id_CronogramaActual )
										.find('.cronograma_actividades[id_etapa="'+ $( g_tr_cronograma ).attr("id_etapa") +'"][id_partida="'+ $( g_tr_cronograma ).attr("id_partida") +'"][id_subpartida="'+ $( g_tr_cronograma ).attr("id_subpartida") +'"]').size();
						if( cont > 1 )
						{
							$('#gd_confirmacion #btn_gd_confirmacion').attr('onClick','fnc_eliminar_actividad_cronograma(2)');
							
							fnc_mostrar_aviso("gd_confirmacion", "<p>Est&aacute; a punto de eliminar esta actividad.</p><p style='font-weight: bold;'>&iquest;Desea continuar?</p>", 2);
						}
						else
							fnc_mostrar_aviso( "gd_avisos", "No puede quedar sin actividades.", 5 );
					}
					else
					{
						$("#div_cronograma_" + g_id_CronogramaActual + " tr.cronograma_actividades[class*='seleccionado']").removeClass("ui-state-highlight seleccionado");
						$("#div_gantt_" + g_id_CronogramaActual + " tr.gantt_actividades[class*='seleccionado']").removeClass("ui-state-highlight seleccionado");
						
						fnc_mostrar_aviso( "gd_avisos", "Esta actividad no puede ser eliminada porque es obligatoria.", 5 );
					}
				}
				else
				{
					if( $("#div_cronograma_" + g_id_CronogramaActual + " tr.cronograma_actividades[class*='seleccionado']").size() == 0 )
						fnc_notificacion("Debe elegir una actividad.");
					else
						fnc_notificacion("S&oacute;lo puede eliminar <b>una</b> actividad a la vez.");
				}			
			break;
		case 2:
				var id_CronogramaActividad = $( g_tr_cronograma ).attr("id_cronograma_actividad");
				var id_cronograma_subpartida = $( g_tr_cronograma ).attr("id_cronograma_subpartida");
				var id_cronograma_partida = $( g_tr_cronograma ).attr("id_cronograma_partida");
				
				fnc_peticionAjax({
					url: "./application/cronograma.cfc",
					parametros:{
						method					: "CronogramaActividades_Eliminar",
						id_Empresa				: g_row_proyectos.id_Empresa,
						id_Cliente				: g_row_proyectos.id_Cliente,
						id_Proyecto   		    : g_row_proyectos.id_Proyecto,
						id_Cronograma			: g_id_CronogramaActual,
						id_CronogramaActividad  : $( g_tr_cronograma ).attr("id_cronograma_actividad"),
						id_CronogramaSubPartida	: id_cronograma_subpartida,
						id_CronogramaPartida	: id_cronograma_partida,
						id_CronogramaEtapa		: $( g_tr_cronograma ).attr("id_etapa"),
						nu_Ciclo				: 1,
						sn_Cotizacion			: 0,
						sn_ActividadesRetrasadas: 1,
						id_Requerimiento		: $('#hid_id_Requerimiento_' + g_id_CronogramaActual).val()
					},
					f_callback: function(){
						//Cargamos el cronograma.
						fnc_recargar_cronograma(true);
						gdialogo.cerrar('gd_confirmacion');
						//Cronograma modificado.
						$("#hid_sn_CronogramaModificado_" + g_id_CronogramaActual).val(1);
					}
				});
			break;
	}
}
function fnc_ordenar_actividad_cronograma()
{
	//Validamos las areas
	var id_Area = $( "#div_cronograma_" + g_id_CronogramaActual + " tr.cronograma_actividades.seleccionado" ).attr("id_Area");

	if( $( "#hid_id_CronogramaEstatus_" + g_id_CronogramaActual ).val() === "6" && id_Area != $("#hid_id_Area").val() )
	{
		fnc_mostrar_aviso( "gd_avisos", "Estas actividades no corresponden a su &Aacute;rea.", 5 );
		return;
	}
	
	var id_etapa = $('.cronograma_actividades.seleccionado').attr('id_etapa');
	var id_partida = $('.cronograma_actividades.seleccionado').attr('id_partida');
	var id_subpartida = $('.cronograma_actividades.seleccionado').attr('id_subpartida');
	
	$('#ul_OrdenActividad li').remove();
	
	$('.cronograma_actividades[id_etapa="'+ id_etapa +'"][id_partida="'+ id_partida +'"][id_subpartida="'+ id_subpartida +'"]')
		.each(function(index){
			var $Actividad = $(this);
			var nom_act = $.trim( $Actividad.find('.nombre_Actividad').find('span').eq(1).text() );
			
			g_array_orden_actividades[ index ] = $Actividad.attr('nu_orden_actividad');
			
			//Agregamos el elemento al ul
			$('#ul_OrdenActividad').append(
				'<li class="li_actividad" '+
					'id_CronogramaActividad="'+ $Actividad.attr('id_cronograma_actividad') +'" >'+ nom_act +
				'</li>'
			)
		});
	
	$('#ul_OrdenActividad').sortable();
	$("#btn_ca_guardar").attr("onClick", "fnc_actualizar_orden_act_cro();");
	gdialogo.abrir("gd_control_actividades");
}
//Funci�n para actualizar el �rden de las actividades del cronograma.
function fnc_actualizar_orden_act_cro()
{
	//Armamos un XML para mandar a actualizar las actividades.
	var actividadesXML = "";
	$("#ul_OrdenActividad li").each(function(index){
		actividadesXML += '<actividad id_CronogramaActividad="'+ $(this).attr('id_CronogramaActividad') +'" ' +
									' nu_orden_actividad="'+ g_array_orden_actividades[ index ] +'"></actividad>'
	});
	actividadesXML = "<root>" + actividadesXML + "</root>";
	
	//Editamos.
	fnc_peticionAjax({
		url: "./application/cronograma.cfc",
		parametros:{
			method					: "CronogramaActividades_EditarXML",
			id_Empresa				: g_row_proyectos.id_Empresa,
			id_Cliente				: g_row_proyectos.id_Cliente,
			id_Proyecto   		    : g_row_proyectos.id_Proyecto,
			id_Cronograma			: g_id_CronogramaActual,
			nu_Ciclo				: 1,
			sn_Cotizacion			: 0,
			actividadesXML			: actividadesXML
		},
		f_callback: function(){
			gdialogo.cerrar("gd_control_actividades");
			
			//Cargamos el cronograma.
			fnc_recargar_cronograma(true);
			
			//Cronograma modificado.
			$("#hid_sn_CronogramaModificado_" + g_id_CronogramaActual).val(1);
		}
	});
}


//Funci�n para asignar recursos.
/*function f(p_tr, p_abrir_dialogo)
{
	g_id_CronogramaActual = $(p_tr).attr("id_cronograma");
	g_tr_actividad_seleccionado = p_tr;
	fnc_peticionAjax({
		url: "./operaciones/asignacion_recursos.cfm",
		parametros: "id_Empresa=" + g_row_proyectos.id_Empresa + "&id_Cliente=" + g_row_proyectos.id_Cliente 
			+ "&id_Proyecto=" + g_row_proyectos.id_Proyecto + "&id_Fase=" + $(p_tr).attr("id_Fase")
			+ "&id_Cronograma=" + g_id_CronogramaActual + "&id_CronogramaActividad=" + $(p_tr).attr("id_cronograma_actividad")
			+ "&nb_Actividad=" + fnc_codificar( $(p_tr).find("td:first span:eq(1)").text() )
			+ "&nu_Ciclo=" + $(p_tr).attr("nu_ciclo") + "&nu_OrdenActividad=" + $(p_tr).attr("nu_orden_actividad")
			+ "&sn_General=" + $(p_tr).attr("sn_general") + "&sn_Cotizacion=" + $("#hid_sn_Cotizacion_" + g_id_CronogramaActual).val(),
		div_success: "div_asignar_recursos_act_cro",
		automensaje: false,
		f_callback: function(){
			if( p_abrir_dialogo )
				gdialogo.abrir("gd_asignar_recursos_act_cro");
				
			$("#div_cronograma_" + g_id_CronogramaActual + " tr.cronograma_actividades").removeClass("ui-state-highlight seleccionado");
		}
	});
}*/

//Funci�n para pedir la justificaci�n del Cronograma L�nea Base.
function fnc_crear_linea_base_justificacion()
{
	/*var bandera = false;
	//Validar que hayan actividades planificadas.
	$("#div_cronograma_" + g_id_CronogramaActual + " tr.cronograma_actividades").each(function(){
		if( $(this).find("td.fecha_InicioP").text() != "" )
		{
			bandera = true
			return false;
		}
	});
	
	if( bandera )
	{*/
		//Cargamos los motivos para la justificacion
		
		$("#tr_motivoLB").show();
		$("#ddl_motivo_lineabase").addClass('obligatorio');
		var parametros = "id_Empresa=" + $("#hid_id_Empresa").val() + "&method=Listar_MotivosLB";
		fnc_peticionAjax({
			url: "./application/App_motivos.cfc",
			parametros: parametros,
			automensaje:false,
			f_callback: function(respuestaJSON)
			{
				//console.log(respuestaJSON);
				
				$("#ddl_motivo_lineabase option[value!=0]").remove();
				
				$.each(respuestaJSON.RS.DATA.ID_MOTIVOLB, function(index, value){
						$("<option>").attr({
							value: value
						}).text( respuestaJSON.RS.DATA.NB_MOTIVOLB[index] ).appendTo("#ddl_motivo_lineabase");
					});
				
				//Pedimos la justificaci�n.
				$("#txt_justificacion_lb").val("");
				$('#btn_justificacion_lb').removeAttr("onClick").attr('onClick', 'fnc_crear_linea_base();');
				$("#txt_justificacion_lb").removeAttr("onKeyUp").attr("onKeyUp", "fnc_enter_presionado(event, '" + $("#btn_justificacion_lb").attr("onClick") + "');");
				gdialogo.abrir("gd_justificacion_lb");
				$("#txt_justificacion_lb").focus();
			}
		});
		
	/*}
	else
		fnc_mostrar_aviso("gd_avisos", "Deben existir fechas definidas.", 5);*/
}

//Funci�n para crear el Cronograma L�nea Base.
function fnc_crear_linea_base()
{
	//Validamos los campos obligatorios.
	if( fnc_validar_campos_obligatorios("div_justificacion_lb") )
	{
		fnc_peticionAjax({
			url: "./application/cronograma.cfc",
			parametros: "method=CronogramaLineaBase_Crear&id_Empresa=" + g_row_proyectos.id_Empresa 
				+ "&id_Cliente=" + g_row_proyectos.id_Cliente 
				+ "&id_Proyecto=" + g_row_proyectos.id_Proyecto 
				+ "&id_Cronograma=" + g_id_CronogramaActual
				+ "&de_Justificacion=" + fnc_codificar( $.trim( $("#txt_justificacion_lb").val() ) )
				+ "&sn_Autorizacion=0"
				+ "&sn_GestionCambios=0"
				+ "&id_TipoRequerimiento=" + $("#hid_id_TipoRequerimiento_" + g_id_CronogramaActual).val()
				+ "&nb_TipoRequerimiento=" + $("#hid_nb_TipoRequerimiento_" + g_id_CronogramaActual).val()
				+ ( $("#ddl_motivo_lineabase option:selected").val() != 0 ? "&id_MotivoLB=" + $("#ddl_motivo_lineabase option:selected").val() : "" )
				+ "&nu_NumTienda=" + g_row_proyectos.nu_NumTienda
				+ "&nb_Proyecto=" + fnc_codificar( g_row_proyectos.nb_Proyecto )
				+ "&id_Area=" + $("#hid_id_Area").val(),
			f_callback: function(respuestaJSON){
				gdialogo.cerrar("gd_justificacion_lb");
				
				//Eliminamos las l�nea base anteriores.
				$("#ddl_cronograma_lb option[value!='0']").remove();
				
				//Agregamos la nueva versi�n al combo.
				$("<option>").attr({
					value: respuestaJSON.RS.DATA.ID_VERSION
				}).text( respuestaJSON.RS.DATA.DE_VERSION ).appendTo("#ddl_cronograma_lb");
				
				//Recargamos el cronograma.
				 fnc_recargar_cronograma(true);
				
				//Si es la primera l�nea base, ya no se puede modificar el cronograma.
				/*if( $("#ddl_cronograma_lb option").size() > 1 )
				{
					var $contenedor = $( "#div_cronograma_" + g_id_CronogramaActual );
					
					//Quitamos todos los eventos que modifican el cronograma.
					$contenedor.find("tr").removeAttr("onClick");
					
					$( "#hid_sn_Gestionar_" + g_id_CronogramaActual ).val(0);
					
					//Mostramos la opci�n de inicializar cambios.
					$( "#div_acciones_cronograma_contenido_" + g_id_CronogramaActual + " ul" ).append( '' +
						'<li onClick="fnc_control_actividades(23, ' + g_id_CronogramaActual + ');" accion="23" num_vez="0"' +
                            'class="configuracion_contenido cc-etapa cc-partida cc-subpartida cc-actividad"' +
                            'id="li_accion_proponer_cambios">Proponer Cambios</li>'
					);
				}*/
			}
		});
	}
}

//Funci�n para asignar recursos.
function fnc_asignar_recursos_actividades(p_tr, p_abrir_dialogo)
{
	g_id_CronogramaActual = $(p_tr).attr("id_cronograma");
	g_tr_actividad_seleccionado = p_tr;

	//Consultamos.
	fnc_peticionAjax({
		url: "./operaciones/asignacion_recursos.cfm",
		parametros: "id_Empresa=" + g_row_proyectos.id_Empresa 
			+ "&id_Cliente=" + g_row_proyectos.id_Cliente 
			+ "&id_Proyecto=" + g_row_proyectos.id_Proyecto 
			+ ( $(p_tr).attr("sn_TipoPlantilla") ? "&sn_TipoPlantilla=" + $(p_tr).attr("sn_TipoPlantilla") : "")
			+ "&id_Cronograma=" + g_id_CronogramaActual 
			+ "&id_CronogramaActividad=" + $(p_tr).attr("id_cronograma_actividad")
			+ "&nb_Actividad=" + fnc_codificar( $(p_tr).find("td:first span:eq(1)").text() )
			+ "&nu_Ciclo=" + $(p_tr).attr("nu_ciclo") 
			+ "&nu_OrdenActividad=" + $(p_tr).attr("nu_orden_actividad")
			+ "&sn_Cotizacion=" + $("#hid_sn_Cotizacion_" + g_id_CronogramaActual).val()
			+ "&sn_Cancelado=" + $("#hid_sn_Cancelado_" + g_id_CronogramaActual).val()
			+ "&sn_Horas=" + $(p_tr).attr("sn_horas")
			+ "&id_Requerimiento=" + $(p_tr).attr("id_requerimiento")
			+ ( $( "#hid_sn_Cotizacion_" + g_id_CronogramaActual ).val() == "0" ? "&id_Area=" + $("#hid_id_Area").val() + "&id_AreaActividad=" + $(p_tr).attr("id_area") : "" )
			+ "&id_EstatusActividad=" + $(p_tr).attr("id_estatus_actividad"),
		div_success: "div_asignar_recursos_act_cro",
		automensaje: false,
		f_callback: function(){
			if( p_abrir_dialogo )
				gdialogo.abrir("gd_asignar_recursos_act_cro");
			else
				gdialogo.re_ajustar("gd_asignar_recursos_act_cro");
				
			$("#div_cronograma_" + g_id_CronogramaActual + " tr.cronograma_actividades").removeClass("ui-state-highlight seleccionado");
			
			//Gantt.
			$("#div_gantt_" + g_id_CronogramaActual + " tr.gantt_actividades").removeClass("ui-state-highlight seleccionado");
			
			$( "#div_acciones_cronograma_" + g_id_CronogramaActual ).hide(300);
		}
	});
}

//Funci�n para cuando se da clic sobre una actividad.
function fnc_clic_actividad(p_tr, event)
{ 
	g_id_CronogramaActual = $(p_tr).attr("id_cronograma");

	//var nodeName = event.target.nodeName.toLowerCase();
	//var sn_otras_acciones = (  nodeName == "div" || nodeName == "td" || $(p_tr).hasClass("cronograma_actividades") );
	var sn_otras_acciones = true;
	
	//event.preventDefault();
	//event.stopPropagation();
	if( sn_otras_acciones )
	{
		var id_Area = $(p_tr).parent().find("tr.cronograma_actividades[id_etapa='" + $(p_tr).attr("id_etapa") + "']"
			+ "[id_partida='" + $(p_tr).attr("id_partida") + "']:first").attr("id_Area");

		if( ( ( id_Area == $("#hid_id_Area").val() 
			|| ( ( $(p_tr).attr("id_tipo_requerimiento") == "1" && $("#hid_sn_GestionaCronogramaProyecto").val() == "1" )
				|| ( $(p_tr).attr("id_tipo_requerimiento") == "2" && $("#hid_sn_GestionaProgramaObra").val() == "1" ) ) )
			|| $( "#hid_sn_Cotizacion_" + g_id_CronogramaActual ).val() == "1" )
		   	|| sn_otras_acciones )
		{
			g_tr_cronograma = p_tr;
			
			//Vaidar si se di� clic en una Etapa, Partida o SubPartida.
			sn_otras_acciones = ( $(p_tr).hasClass("cronograma_etapas") || $(p_tr).hasClass("cronograma_partidas")
				|| $(p_tr).hasClass("cronograma_subpartidas") );

			if( sn_otras_acciones == false )
			{
				var total_rows = $("#div_cronograma_" + g_id_CronogramaActual + " tr.cronograma_actividades[class*='seleccionado']").size();
				
				if( $(p_tr).hasClass("seleccionado") )
				{
					$(p_tr).removeClass("ui-state-highlight seleccionado");
					
					//Gantt.
					$("tr.gantt_actividades[id_cronograma='" + $(p_tr).attr("id_cronograma") + "'][id_partida='" + $(p_tr).attr("id_partida") + "']"
						+ "[id_etapa='" + $(p_tr).attr("id_etapa") + "'][id_subpartida='" + $(p_tr).attr("id_subpartida") + "']"
						+ "[id_cronograma_actividad='" + $(p_tr).attr("id_cronograma_actividad") + "']").removeClass("ui-state-highlight seleccionado");
					
					//Ocultamos el panel de configuraci�n.
					$("#div_configuraciones_cronograma, #div_configuraciones_cronograma_fondo").hide();
					
					total_rows = $("#div_cronograma_" + g_id_CronogramaActual + " tr.cronograma_actividades[class*='seleccionado']").size();
				}
				else
				{
					//Validar que no se mezclen los ciclos.
					if( total_rows == 0 || $("#div_cronograma_" + g_id_CronogramaActual + " tr.cronograma_actividades[class*='seleccionado']:first").attr("nu_ciclo") == $(p_tr).attr("nu_ciclo") )
					{
						$(p_tr).addClass("ui-state-highlight seleccionado");
						
						//Gantt.
						$("tr.gantt_actividades[id_cronograma='" + $(p_tr).attr("id_cronograma") + "'][id_partida='" + $(p_tr).attr("id_partida") + "']"
							+ "[id_etapa='" + $(p_tr).attr("id_etapa") + "'][id_subpartida='" + $(p_tr).attr("id_subpartida") + "']"
							+ "[id_cronograma_actividad='" + $(p_tr).attr("id_cronograma_actividad") + "']").addClass("ui-state-highlight seleccionado");
					}
					
					total_rows = $("#div_cronograma_" + g_id_CronogramaActual + " tr.cronograma_actividades[class*='seleccionado']").size();
					
					//Si existe m�s de una actividad seleccionada ocultamos el panel de configuraci�n.
					if( total_rows > 1 )
						$("#div_configuraciones_cronograma, #div_configuraciones_cronograma_fondo").hide();
					
				}
			}
		
			//Mostramos las acciones.
			if( total_rows > 0 || sn_otras_acciones )
			{  
				//Validamos los li que podemos mostrar.
				if( sn_otras_acciones )
				{
					$( "#div_acciones_cronograma_contenido_" + g_id_CronogramaActual + " li" ).hide()

					var clase_filtrar = "";
					if( $(p_tr).hasClass("cronograma_etapas") )
						clase_filtrar = "cc-etapa";
					else if( $(p_tr).hasClass("cronograma_partidas") )
						clase_filtrar = "cc-partida";
					else if( $(p_tr).hasClass("cronograma_subpartidas") )
						clase_filtrar = "cc-subpartida";
					
					$( "#div_acciones_cronograma_contenido_" + g_id_CronogramaActual + " li[class*='" + clase_filtrar + "']" ).show()
				}
				else
				{
					$( "#div_acciones_cronograma_contenido_" + g_id_CronogramaActual + " li" ).show()
					
					var num_vez = total_rows;
					if( total_rows > 1 )
						num_vez = 2;
					
					$( "#div_acciones_cronograma_contenido_" + g_id_CronogramaActual + " li[num_vez='" + num_vez + "']" ).show();
					$( "#div_acciones_cronograma_contenido_" + g_id_CronogramaActual + " li[num_vez!='" + num_vez + "']" ).hide();
					
					$( "#div_acciones_cronograma_contenido_" + g_id_CronogramaActual + " li[num_vez='0']" ).show();
				}
				
				$( "#div_acciones_cronograma_" + g_id_CronogramaActual ).css({
					top: event.clientY
				});
				
				if( $( "#div_acciones_cronograma_" + g_id_CronogramaActual + " li" ).size() > 0 )
					$( "#div_acciones_cronograma_" + g_id_CronogramaActual ).show();
		
				//Validamos el top.
				var alto_pagina = $(window).height() * 1;
				var alto_acciones = $( "#div_acciones_cronograma_" + g_id_CronogramaActual ).height() * 1;
				var position_top_acciones = $( "#div_acciones_cronograma_" + g_id_CronogramaActual ).position().top * 1;
				var top_acciones = $( "#div_acciones_cronograma_" + g_id_CronogramaActual ).css("top");
				top_acciones = top_acciones.substring( 0, top_acciones.length - 2 ) * 1;
				
				if( ( position_top_acciones + alto_acciones ) > alto_pagina )
					$( "#div_acciones_cronograma_" + g_id_CronogramaActual ).css( "top", ( top_acciones - alto_acciones ) );
				
				//Esablecemos el �rden del encadenamitno.
				$(p_tr).attr({
					orden_encadenamiento: $( "#div_cronograma_" + g_id_CronogramaActual + " tr.cronograma_actividades[class*='seleccionado']" ).size()
				});
			}
			else 
			{
				if( total_rows == 0 )
				{
					$( "#div_acciones_cronograma_" + g_id_CronogramaActual ).hide(300);
					
					$(p_tr).removeAttr("orden_encadenamiento");
				}
			}
		}
	}
}

//Funci�n para consultar el Cronograma L�nea Base.
function fnc_consultar_cronograma_lb(p_id_Version)
{
	if( p_id_Version == "0" )
		$("#div_contenido_cronograma_lb").empty();
	else
	{
		//Cargamos el Cronograma L�nea Base.
		fnc_peticionAjax({
			url: "./operaciones/cronograma_linea_base.cfm",
			parametros: "method=CronogramaLineaBase_Listar&id_Empresa=" + g_row_proyectos.id_Empresa + "&id_Cliente=" + g_row_proyectos.id_Cliente
				+ "&id_Proyecto=" + g_row_proyectos.id_Proyecto + "&id_Cronograma=" + g_id_CronogramaActual + "&id_Version=" + p_id_Version
				+ "&sn_MostrarPorcentajes=0"
				+ "&sn_LineaBase=1",
			div_success: "div_contenido_cronograma_lb",
			automensaje: false,
			f_callback: function(){
				fnc_tamanio_cronograma_gantt();
				
				//Deslizamos el scroll autom�ticamente.
				fnc_deslizar_scroll( $("#div_contenido_cronograma_lb") );
			}
		});
	}
}

//Funci�n para mostrar el Gantt.
function fnc_mostrar_gantt(p_id_Empresa, p_id_Cliente, p_id_Proyecto, p_sn_Cotizacion, p_id_Cronograma)
{
	if( p_id_Cronograma != "" )
	{
		//Validar si ya se carg� el Gantt.
		if( $("#hid_sn_CronogramaModificado_" + p_id_Cronograma).val() == "1" || $.trim( $("#div_gantt_" + p_id_Cronograma).html() ) == "" )
		{
			fnc_peticionAjax({
				url: "./application/adm_proy.cfc",
				parametros: "method=Gantt_Listar&id_Empresa=" + p_id_Empresa + "&id_Cliente=" + p_id_Cliente
					+ "&id_Proyecto=" + p_id_Proyecto + "&sn_Cotizacion=" + p_sn_Cotizacion + "&id_Cronograma=" + p_id_Cronograma,
				div_success: "div_respuesta_ajax",
				automensaje: false,
				f_callback: function(){
					var actividadesXML = $.parseXML( $.trim( $("#div_respuesta_ajax").html() ) );
					
					if( $(actividadesXML).find("fechas").attr("fecha_inicio") != "" &&  $(actividadesXML).find("fechas").attr("fecha_fin") != "" )
					{	
						ggantt.crear({
							id: "div_gantt_" + p_id_Cronograma,
							actividadesXML: actividadesXML
						});
						
						$("#div_gantt_" + p_id_Cronograma).show();
						$("#div_cronograma_" + p_id_Cronograma).hide();
						
						$("#btn_mostrar_gantt_" + p_id_Cronograma).hide();
						$("#btn_mostrar_cronograma_" + p_id_Cronograma).show();
					}
					else
						fnc_mostrar_aviso( "gd_avisos", "No existen actividades con fecha definida.", 5 );
					
					$("#div_respuesta_ajax").empty();
					
					$("#hid_sn_CronogramaModificado_" + p_id_Cronograma).val(0);
				}
			});
		}
		else
		{
			$("#div_gantt_" + p_id_Cronograma).show();
			$("#div_cronograma_" + p_id_Cronograma).hide();
			
			$("#btn_mostrar_gantt_" + p_id_Cronograma).hide();
			$("#btn_mostrar_cronograma_" + p_id_Cronograma).show();
		}
	}
	else
		fnc_mostrar_aviso( "gd_avisos", "No existe un Cronograma.", 5 );
}

//Funci�n para mostrar el Cronograma.
function fnc_mostrar_cronograma(p_id_Cronograma)
{
	$("#div_gantt_" + p_id_Cronograma).hide();
	$("#div_cronograma_" + p_id_Cronograma).show();
	
	$("#btn_mostrar_gantt_" + p_id_Cronograma).show();
	$("#btn_mostrar_cronograma_" + p_id_Cronograma).hide();
}

//EQUIPO DEL PROYECTO.

//Funci�n para agregar Roles al Equipo de Trabajo.
function fnc_equipo_trabajo_roles_crud(p_accion, p_id_Cronograma)
{
	g_id_CronogramaActual = p_id_Cronograma;
	
	if( p_accion == 1 ) //Agregar
	{
		$("#div_titulo_agregar_editar_roles_et_" + p_id_Cronograma).html("NUEVO RECURSO");
		$("#btn_guardar_et_roles_" + p_id_Cronograma).attr("title", "Guardar").text("Guardar");
		
		//Nos aseguramos de desbloquear los input.
		$("#ddl_roles_" + p_id_Cronograma + ",#txt_nb_Alias_" + p_id_Cronograma + ",#txt_horas_rol_cotizacion_" + p_id_Cronograma + ",#txt_costo_hora_" + p_id_Cronograma).removeAttr("disabled");
		
		fnc_catalogos_mostrar_ocultar("div_equipo_trabajo_roles_" + p_id_Cronograma, "div_roles_cronograma_" + p_id_Cronograma, function(){
			gdialogo.re_ajustar("gd_asignar_recursos_act_cro_" + p_id_Cronograma);
		});
	}
	else if( p_accion == 2 || p_accion == 3 ) //Editar o Eliminar.
	{
		//Validamos que se haya seleccionado un rengl�n.			
		if( $("#table_recursos_et_listado_"+ p_id_Cronograma +" tbody tr.seleccionado").size() == 1 )
		{
			//Llenamos los campos.
			$("#ddl_roles_" + p_id_Cronograma).val( $("#table_recursos_et_listado_"+ p_id_Cronograma + " tbody tr.seleccionado").attr("id_rol") );
			$("#txt_nb_Alias_" + p_id_Cronograma).val( $("#table_recursos_et_listado_"+ p_id_Cronograma + " tbody tr.seleccionado td:eq(0) span").text() );
			$("#txt_horas_rol_cotizacion_" + p_id_Cronograma).val( $("#table_recursos_et_listado_"+ p_id_Cronograma + " tbody tr.seleccionado td:eq(2) span").text() );
			$("#txt_costo_hora_" + p_id_Cronograma).val( $("#table_recursos_et_listado_"+ p_id_Cronograma + " tbody tr.seleccionado").attr("nu_CostoRol") );
			
			//Valdiar si debemos bloquear o desbloquear los input.
			if( p_accion == 3 )
			{
				$("#div_titulo_agregar_editar_roles_et_"+ p_id_Cronograma ).html("ELIMINAR RECURSO");
				$("#btn_guardar_et_roles_" +p_id_Cronograma).attr("title", "Eliminar").text("Eliminar");
				$("#ddl_roles_" + p_id_Cronograma +",#txt_nb_Alias_" + p_id_Cronograma +",#txt_horas_rol_cotizacion_" + p_id_Cronograma +",#txt_costo_hora_" + p_id_Cronograma).attr("disabled", "disabled");
			}
			else
			{
				$("#div_titulo_agregar_editar_roles_et_" + p_id_Cronograma).html("EDITAR RECURSO");
				$("#btn_guardar_et_roles_" + p_id_Cronograma).attr("title", "Guardar").text("Guardar");
				$("#ddl_roles_" + p_id_Cronograma +",#txt_nb_Alias_" + p_id_Cronograma +",#txt_horas_rol_cotizacion_" + p_id_Cronograma +",#txt_costo_hora_" + p_id_Cronograma).removeAttr("disabled");
			
			}
			
			fnc_catalogos_mostrar_ocultar("div_equipo_trabajo_roles_" + p_id_Cronograma , "div_roles_cronograma_" + p_id_Cronograma, function(){
				gdialogo.re_ajustar("gd_asignar_recursos_act_cro_" + p_id_Cronograma);
				
				if( p_accion == 2 )
					$("#txt_costo_hora_" + p_id_Cronograma).select();
			});
		}
		else
			fnc_notificacion("Debe seleccionar un recurso.");
	}
	
	$("#hid_accion_roles_act_" + p_id_Cronograma).val( p_accion );
	
	//Limpiamos los campos.
	if( p_accion == 1 )
	{
		$("#ddl_roles_" + p_id_Cronograma).val(0);
		$("#txt_nb_Alias_" + p_id_Cronograma).val("");
		$("#txt_horas_rol_cotizacion_" + p_id_Cronograma).val("");
		$("#txt_costo_hora_" + p_id_Cronograma).val("");
	}
	
	$("#ddl_roles_" + p_id_Cronograma).focus();
}

// Funci�n para autorizar o desautorizar cotizaciones
function fnc_autorizar_cotizacion()
{
	if(v_sn_CotizacionAutorizada == 0)
	{
	
		// - Autorizamos.
		fnc_peticionAjax({
			url: "./application/adm_proy.cfc",
			parametros: "method=AutorizarCotizacion&id_Empresa=" + g_row_proyectos.id_Empresa + "&id_Cliente=" + g_row_proyectos.id_Cliente
				+ "&id_Proyecto=" + g_row_proyectos.id_Proyecto + "&id_Cronograma=" + g_id_CronogramaCotizacion
				+ "&id_Requerimiento=" + $("#hid_id_Requerimiento_" + g_id_CronogramaCotizacion).val(),
			f_callback: function()
			{
				gdialogo.cerrar("gd_confirmacion_cotizacion_" + g_id_CronogramaCotizacion);
				$("#btn_autorizar_cotizacion_" + g_id_CronogramaCotizacion).html('<span style="font-size: 18px;">Desautorizar Cotizaci&oacute;n</span>');
				$("#hid_autorizar_cotizacion_estado_" + g_id_CronogramaCotizacion).val(1);
				$("#div_info_cotizacion_autorizada_" + g_id_CronogramaCotizacion).html('<span style="font-size: 20px; color: #00D200; text-decoration: underline;">COTIZACI&Oacute;N AUTORIZADA</span>');
				$("#table_recursos_et_listado_" + g_id_CronogramaCotizacion +" tfoot").remove();
				
				//Actualizamos grid de proyectos
				ggrids.recargar("grid_proyectos",{
					recordar_seleccion: true
				});
			}
		});
	}
	else
	{
		// - DesAutorizamos.
		fnc_peticionAjax({
			url: "./application/adm_proy.cfc",
			parametros: "method=DesAutorizarCotizacion&id_Empresa=" + g_row_proyectos.id_Empresa + "&id_Cliente=" + g_row_proyectos.id_Cliente
				+ "&id_Proyecto=" + g_row_proyectos.id_Proyecto + "&id_Cronograma=" + g_id_CronogramaCotizacion,
			f_callback: function()
			{
				gdialogo.cerrar("gd_confirmacion_cotizacion_" + g_id_CronogramaCotizacion);
				$("#btn_autorizar_cotizacion_" + g_id_CronogramaCotizacion.toString()).html('<span style="font-size: 18px;">Autorizar Cotizaci&oacute;n</span>');
				$("#hid_autorizar_cotizacion_estado_" + g_id_CronogramaCotizacion.toString()).val(0);
				$("#div_info_cotizacion_autorizada_" + g_id_CronogramaCotizacion).html('');
				$("#table_recursos_et_listado_" + g_id_CronogramaCotizacion + " tfoot").remove();
				
				//Actualizamos grid de proyectos
				ggrids.recargar("grid_proyectos",{
					recordar_seleccion: true
				});
			}
		});

	}
	
	
}

//Funci�n para agregar Roles al Equipo del Proyecto y a las Actividades.
function fnc_cronograma_rol_agregar_editar_eliminar(p_cotizacion)
{
	var parametros = "id_Empresa=" + g_row_proyectos.id_Empresa + "&id_Cliente=" + g_row_proyectos.id_Cliente
		+ "&id_Proyecto=" + g_row_proyectos.id_Proyecto + "&id_Cronograma=" + g_id_CronogramaActual
		+ "&id_Rol=" + $("#ddl_roles_"+ g_id_CronogramaActual +" option:selected").val() + "&sn_Cotizacion=" + $("#hid_sn_Cotizacion_" + g_id_CronogramaActual).val()
		+ "&nu_HorasReales=" + ( $("#txt_horas_rol_cotizacion_" +g_id_CronogramaActual ).val() ? $("#txt_horas_rol_cotizacion_" + g_id_CronogramaActual).val() : "0" )
		+ "&nu_CostoHora=" + ( $("#txt_costo_hora_" + g_id_CronogramaActual ).val() ? $("#txt_costo_hora_" + g_id_CronogramaActual).val() : "0" )
		+ "&sn_Cancelado=" + $("#hid_sn_Cancelado_" + g_id_CronogramaActual).val();
	

	switch( $("#hid_accion_roles_act_" + g_id_CronogramaActual).val() )
	{
		case "1": //Agregar.
					parametros += "&method=EquipoTrabajo_Agregar&nb_Alias=" + fnc_codificar( $.trim( $("#txt_nb_Alias_" + g_id_CronogramaActual).val() ) );
					break;
		case "2": //Editar.
					parametros += "&method=EquipoTrabajo_Editar&nb_Alias=" + fnc_codificar( $.trim( $("#txt_nb_Alias_" + g_id_CronogramaActual).val() ) )
						+ "&id_EquipoTrabajo=" + $("#table_recursos_et_listado_" + g_id_CronogramaActual +" tbody tr.seleccionado").attr("id_equipo_trabajo");
					break;
		case "3": //Eliminar.
					parametros += "&method=EquipoTrabajo_Eliminar" + "&id_EquipoTrabajo=" + $("#table_recursos_et_listado_" + g_id_CronogramaActual +" tbody tr.seleccionado").attr("id_equipo_trabajo");
					break;
	}
	
	//Validamos los campos obligatorios.
	if( fnc_validar_campos_obligatorios("div_roles_cronograma_" + g_id_CronogramaActual) )
	{
	
		fnc_peticionAjax({
			url: "./application/adm_proy.cfc",
			parametros: parametros,
			automensaje: p_cotizacion,
			f_callback: function(respuestaJSON){
				//Validar si se agreg� el Recurso desde la Cotizaci�n.
				if( $("#hid_accion_roles_act_" + g_id_CronogramaActual).val() == "1" )
				{
					$("tr.sin_EquipoProyecto").remove();

					if( p_cotizacion )
					{
						$("#table_recursos_et_listado_" + g_id_CronogramaActual).append(
							'<tr class="et_recurso_disponible" id_cronograma="' + g_id_CronogramaActual + '" id_recurso="" id_rol="' + 
								$("#ddl_roles_"+ g_id_CronogramaActual + " option:selected").val() + '" id_equipo_trabajo="' + respuestaJSON.RS.DATA.ID_EQUIPOTRABAJO + '"' +
								'onClick="$(\'.et_recurso_disponible\').removeClass(\'ui-state-highlight seleccionado\');' +
									'$(this).addClass(\'ui-state-highlight seleccionado\');" nu_CostoRol="' + $("#txt_costo_hora_" + g_id_CronogramaActual).val() +
								'" style="cursor: pointer;">' +
								'<td title="' + $.trim( $("#txt_nb_Alias_" + g_id_CronogramaActual).val() ) + '">' +
									'<span>' + $.trim( $("#txt_nb_Alias_" +g_id_CronogramaActual ).val() ) + '</span>' +
								'</td>' +
								'<td title="' + $("#ddl_roles_" + g_id_CronogramaActual + " option:selected").text() + '">' +
									'<span>' + $("#ddl_roles_" + g_id_CronogramaActual  + " option:selected").text() + '</span>' +
								'</td>' +
								'<td style="text-align: center;" title="' + $("#txt_horas_rol_cotizacion_" + g_id_CronogramaActual).val() + '">' +
									'<span>' + $("#txt_horas_rol_cotizacion_" + g_id_CronogramaActual).val() + '</span>' +
								'</td>' +
								'<td style="text-align: right;">' +
									'<span style="font-weight: bold;">' + fnc_formatear_numeros( $("#txt_costo_hora_" + g_id_CronogramaActual).val(), 2, ['.', "'", '.'], "$" ) + '</span>' +
								'</td>' +
							'</tr>'
						);
						
						$("#ddl_roles_" + g_id_CronogramaActual).val(0);
						$("#txt_nb_Alias_" + g_id_CronogramaActual).val("");
						$("#txt_horas_rol_cotizacion_" + g_id_CronogramaActual).val("");
						$("#txt_costo_hora_" + g_id_CronogramaActual).val("");
					}
					else
					{
						//Recargamos el di�logo.
						fnc_asignar_recursos_actividades(g_tr_actividad_seleccionado, false);
					}
				}
				else if( $("#hid_accion_roles_act_" + g_id_CronogramaActual).val() == "2" )
				{
					//Atributos.
					$("#table_recursos_et_listado_" + g_id_CronogramaActual +" tbody tr.seleccionado").attr( "id_rol", $("#ddl_roles_" +g_id_CronogramaActual+" option:selected").val());
					$("#table_recursos_et_listado_" + g_id_CronogramaActual +" tbody tr.seleccionado").attr( "nu_CostoRol", $("#txt_costo_hora_" +g_id_CronogramaActual).val() );
					$("#table_recursos_et_listado_" + g_id_CronogramaActual +" tbody tr.seleccionado td:eq(0)").attr( "title", $.trim( $("#txt_nb_Alias_" + g_id_CronogramaActual).val()));
					$("#table_recursos_et_listado_" + g_id_CronogramaActual +" tbody tr.seleccionado td:eq(1)").attr( "title", $("#ddl_roles_" +g_id_CronogramaActual+" option:selected").text());
					$("#table_recursos_et_listado_" + g_id_CronogramaActual +" tbody tr.seleccionado td:eq(2)").attr( "title", $("#txt_horas_rol_cotizacion_" + g_id_CronogramaActual).val());
					
					//Valores.
					$("#table_recursos_et_listado_" + g_id_CronogramaActual +" tbody tr.seleccionado td:first span").text( $.trim( $("#txt_nb_Alias_" + g_id_CronogramaActual).val() ) );
					$("#table_recursos_et_listado_" + g_id_CronogramaActual +" tbody tr.seleccionado td:eq(1) span").text( $("#ddl_roles_" +g_id_CronogramaActual+" option:selected").text());
					$("#table_recursos_et_listado_" + g_id_CronogramaActual +" tbody tr.seleccionado td:eq(2) span").text( $("#txt_horas_rol_cotizacion_" + g_id_CronogramaActual).val());
					$("#table_recursos_et_listado_" + g_id_CronogramaActual +" tbody tr.seleccionado td:eq(3) span").text( fnc_formatear_numeros( $("#txt_costo_hora_"+g_id_CronogramaActual).val(), 2, [',', "'", '.'], "$" ) );
					
					fnc_catalogos_mostrar_ocultar("div_equipo_trabajo_roles_" + g_id_CronogramaActual,"div_roles_cronograma_" + g_id_CronogramaActual);
				}
				else if( $("#hid_accion_roles_act_" + g_id_CronogramaActual).val() == "3" )
				{
					$("#table_recursos_et_listado_" + g_id_CronogramaActual +" tbody tr.seleccionado").remove();
					
					fnc_catalogos_mostrar_ocultar("div_equipo_trabajo_roles_" + g_id_CronogramaActual, "div_roles_cronograma_" + g_id_CronogramaActual, function(){
						gdialogo.re_ajustar("gd_asignar_recursos_act_cro_" + g_id_CronogramaActual);
					});
					
					//Validar si ya no existen Recursos.
					if( $("#table_recursos_et_listado_" + g_id_CronogramaActual +" tbody tr").size() == 0 )
					{
						$("#table_recursos_et_listado_" + g_id_CronogramaActual +" tbody").append(
							'<tr class="sin_EquipoProyecto">' +
								'<td style="text-align: center;">' +
									'<span style="font-style: italic;">No existe un Equipo de Trabajo definido.</span>' +
								 '</td>' +
							'</tr>'
						);
					}
				}
				
				if(p_cotizacion)
				{
					$("#span_total_horas_cotizacion_" + g_id_CronogramaActual).text( respuestaJSON.RS.DATA.NU_TOTALHORAS );
					$("#span_costo_proyecto_" + g_id_CronogramaActual).text( respuestaJSON.RS.DATA.NU_COSTOPROYECTO );
				}
				
				//Reajustamos el di�logo.
				gdialogo.re_ajustar("gd_asignar_recursos_act_cro_" + g_id_CronogramaActual);
			}
		});
	}
}

//Funci�n para guardar recursos a las actividades.
function fnc_recurso_actividad_guardar(p_horas)
{
	//Validar que se haya ingresado una duraci�n.
	if( ( $(g_tr_actividad_seleccionado).attr('sn_Horas') == "1" && ( p_horas || ( $("#txt_horas_act_cro").val() * 1 > 0 || $("#txt_minutos_act_cro").val() * 1 > 0 ) ) )
		|| $(g_tr_actividad_seleccionado).attr('sn_Horas') == "0" && $("#txt_dias_act_cro").val() != "" )
	{
		if( $(g_tr_actividad_seleccionado).attr('sn_Horas') == "1" )
		{
			var nu_TiempoPlaneado = ( p_horas ? p_horas : ( $("#txt_horas_act_cro").val() * 1 
					+ fnc_convertir_a_horas( $("#txt_minutos_act_cro").val(), "m" ) ) )
				+ ( $("#txt_fh_InicioPropuesta").val() != "" ? "&fh_InicioPropuesta=" 
					+ fnc_fecha_string( fnc_fecha_objeto( $("#txt_fh_InicioPropuesta").val(), true ), "yyyy-mm-dd", true ) : "" )
		}
		else if( $(g_tr_actividad_seleccionado).attr('sn_Horas') == "0" )
			var nu_TiempoPlaneado = ( $("#txt_dias_act_cro").val() * 1 ) * ( $("#hid_nu_EquivalenteHorasDia_" + g_id_CronogramaActual).val() * 1 );
		
		//Armamos los par�metros.
		var parametros = "method=CronogramaRecursosActividad_Agregar&id_EquipoTrabajo=" + $(g_recurso_et_seleccionado).attr("id_equipo_trabajo")
			+ "&id_Empresa=" + g_row_proyectos.id_Empresa 
			+ "&id_Cliente=" + g_row_proyectos.id_Cliente 
			+ "&id_Proyecto=" + g_row_proyectos.id_Proyecto 
			+ "&id_Cronograma=" + g_id_CronogramaActual 
			+ "&id_CronogramaActividad=" + $("#hid_id_CronogramaActividad").val() 
			+ "&nu_Ciclo=" + $("#hid_nu_Ciclo").val()
			+ "&nu_OrdenActividad=" + $("#hid_nu_OrdenActividad").val()
			+ "&sn_Cotizacion=" + $("#hid_sn_Cotizacion_" + g_id_CronogramaActual).val()
			+ "&nu_TiempoPlaneado=" + nu_TiempoPlaneado
			+ "&id_Requerimiento=" + $(g_tr_actividad_seleccionado).attr("id_requerimiento")
			+ "&sn_ActividadesRetrasadas=0"
			+ "&id_TipoRequerimiento=" + $( "#hid_id_TipoRequerimiento_" + g_id_CronogramaActual ).val();
		
		//Validamos si se propuso una fecha.
		if( $("#chk_fh_InicioPropuesta").is(":checked") )
		{
			if( $("#txt_fh_InicioPropuesta").val() !== "" )
				parametros += "&fh_InicioPropuesta=" + fnc_fecha_string( fnc_fecha_objeto( $("#txt_fh_InicioPropuesta").val(), true ), "yyyy-mm-dd", true );
		}
		
		//Planos a realizar.
		var $planos_disponibles = $("#tbl_planos_realizar");
		var planosXML = '';
		if( $planos_disponibles.find("tr").size() > 0 )
		{
			$planos_disponibles.find("tr input:checkbox:checked").each(function(){
				planosXML += '<plano id_Plano="' + $(this).attr("id_Plano") + '" />';
			});
			
			//Validamos que se haya seleccionado un plano.
			if( planosXML == "" )
			{
				fnc_notificacion( "Debe seleccionar por lo menos un Plano." );
				return;
			}
		}
		
		if( planosXML != "" )
			planosXML = "<root>" + planosXML + "</root>";
		else
			planosXML = "<root></root>";
			
		parametros += "&planosXML=" + planosXML;
			
		//Asignamos el Recurso.
		fnc_peticionAjax({
			url: "./application/cronograma.cfc",
			parametros: parametros,
			div_success: "div_respuesta_ajax",
			f_callback: function(){
				//Recargamos el di�logo.
				fnc_asignar_recursos_actividades(g_tr_actividad_seleccionado, false);
				
				//Cargamos el cronograma.
				setTimeout(function(){
					fnc_recargar_cronograma(true);
				}, 100);
				
				//Validamos si se pudo asignar una fecha para la actividad.
				var respuesta_json = ( $.trim( $("#div_respuesta_ajax").html() ) == "" ? "" : $.parseJSON( $.trim( $("#div_respuesta_ajax").html() ) ) );
				
				if( ( respuesta_json != "" && respuesta_json.RS ) && respuesta_json.RS.DATA.DE_RESULTADO != "OK" )
					if( respuesta_json.RS.DATA.DE_RESULTADO )
						fnc_mostrar_aviso( "gd_avisos", respuesta_json.RS.DATA.DE_RESULTADO, 5 );
				
				//Cronograma modificado.
				$("#hid_sn_CronogramaModificado_" + g_id_CronogramaActual).val(1);
			},
			automensaje: false
		});
	}
	else
		fnc_mostrar_aviso("gd_avisos", "Debe ingresar una duraci&oacute;n para la actividad.", 5);
}

//Funci�n para asignar las horas extras a los recursos.
function fnc_recursos_horas_extras_et_edita()
{
	//Validamos los campos obligatorios.
	if( fnc_validar_campos_obligatorios("div_et_horas_extras_crud") )
	{
		//Validamos que la fecha fin no sea menor que la fecha de inicio.
		if( fnc_fecha_objeto( $("#txt_fh_FinVigencia").val(), true ) >= fnc_fecha_objeto( $("#txt_fh_InicioVigencia").val(), true ) )
		{
			var parametros = "method=EquipoTrabajo_Editar&id_Empresa=" + g_row_proyectos.id_Empresa + "&id_Cliente=" + g_row_proyectos.id_Cliente
				+ "&id_Proyecto=" + g_row_proyectos.id_Proyecto + "&id_Cronograma=" + g_id_CronogramaActual
				+ "&sn_Cotizacion=" + $("#hid_sn_Cotizacion_" + g_id_CronogramaActual).val()
				+ "&id_EquipoTrabajo=" + $("#table_recursos_agregados tr.seleccionado").attr("id_equipo_trabajo")
				+ "&id_Rol=" + $("#table_recursos_agregados tr.seleccionado").attr("id_Rol")
				+ ( $("#txt_horas_extras").val() == "0" ? "" : "&nu_HorasExtras=" + $("#txt_horas_extras").val() )
				+ ( $("#txt_horas_extras").val() == "0" ? "" : "&fh_InicioVigencia=" 
					+ fnc_fecha_string( fnc_fecha_objeto( $("#txt_fh_InicioVigencia").val(), true ), "yyyy-mm-dd", true ) )
				+ ( $("#txt_horas_extras").val() == "0" ? "" : "&fh_FinVigencia=" 
					+ fnc_fecha_string( fnc_fecha_objeto( $("#txt_fh_FinVigencia").val(), true ), "yyyy-mm-dd", true ) );
			
			//Editamos.
			fnc_peticionAjax({
				url: "./application/adm_proy.cfc",
				parametros: parametros,
				f_callback: function(){
					$("#table_recursos_agregados tr.seleccionado td.td_HorasExtras").text( $("#txt_horas_extras").val() );
					$("#table_recursos_agregados tr.seleccionado td.td_fh_InicioVigencia").text( (  $("#txt_horas_extras").val() == "0" ? "---" : $("#txt_fh_InicioVigencia").val() ) );
					$("#table_recursos_agregados tr.seleccionado td.td_fh_FinVigencia").text( (  $("#txt_horas_extras").val() == "0" ? "---" : $("#txt_fh_FinVigencia").val() ) );
					
					fnc_catalogos_mostrar_ocultar("div_et", "div_et_horas_extras");
				}
			});
		}
		else
			fnc_mostrar_aviso("gd_avisos", "La fecha Fin debe ser mayor que la fecha de Inicio", 5);
	}
}

//Funci�n para igualar alturas del cronograma y el gantt.
function fnc_igualar_alturas_cro_gantt(p_id_Cronograma)
{
	var sn_LineaBase = $( ".div_cronograma[id_cronograma='" + p_id_Cronograma + "']" ).attr("sn_linea_base");

	var concatenar = "";
	if( sn_LineaBase == "1" )
		concatenar = "lb_";

	var altura = ( $( "#div_cronograma_" + concatenar + p_id_Cronograma ).height() + 10 ) + "px";
	
	$( "#div_gantt_" + concatenar + p_id_Cronograma ).css({
		height: altura
	});
	
	$( "#div_medio_cg_" + concatenar + p_id_Cronograma ).css({
		height: altura,
		left: "0px"
	});
}

//Funci�n para cambiar el tama�o del cronograma y del gantt.
function fnc_tamanio_cronograma_gantt()
{ 
	$(".div_medio_cg").each(function(){
		var sn_LineaBase = $(this).attr("sn_linea_base");
		var concatenar = "";
		var contenedor = "div_cronograma_contenido_" + $(this).attr("id_cronograma");

		if( sn_LineaBase == "1" )
		{
			concatenar = "lb_";
			contenedor = "div_contenido_cronograma_lb";
		}	
	
		var altura = $( "#div_cronograma_" + concatenar + $(this).attr("id_cronograma" ) ).height() + 10 + "px";

		$( "#div_gantt_" + concatenar + $(this).attr("id_cronograma") ).css({
			height: altura
		});
		
		$( "#div_medio_cg_" + concatenar + $(this).attr("id_cronograma") ).css({
			height: altura
		});
			
		$( "#div_medio_cg_" + concatenar + $(this).attr("id_cronograma") ).draggable({
			containment: contenedor,
			axis: "x",
			drag: function(event){
				var id_cronograma = $(this).attr("id_cronograma");
				var ancho_contenedor = $( "#" + contenedor ).width() - 20;
				var ancho_cronograma = event.clientX;
				var ancho_gantt = ancho_contenedor - event.clientX;

				if( ancho_gantt >= 200 && ancho_cronograma >= 200 )
				{
					$( "#div_cronograma_" + concatenar + id_cronograma ).css({
						width: ancho_cronograma
					});
					
					$( "#div_gantt_" + concatenar + id_cronograma ).css({
						width: ancho_gantt
					});
				}
				//$("#gil").append('<span>' + ancho_contenedor + ', </span>');
				
			},
			stop: function(){
				var id_cronograma = $(this).attr("id_cronograma");
				
				$( "#div_medio_cg_" + concatenar + id_cronograma ).css({
					left: "0px"
				});
				
				var altura = ( $( "#div_cronograma_" + concatenar + id_cronograma ).height() + 10 ) + "px";

				$( "#div_gantt_" + concatenar + id_cronograma ).css({
					height: altura 
				});
				
				$( "#div_medio_cg_" + concatenar + id_cronograma ).css({
					height: altura
				});
			}
		});
	});
}

//Funci�n para cuando se cierra el panel de configuraci�n de las actividades.
function fnc_panel_configuracion_cerrar()
{
	$('#div_configuraciones_cronograma, #div_configuraciones_cronograma_fondo').slideToggle();
	
	$("#div_contenido").css({
		height: $("#div_contenido").height() - 300
	});
	
	//Desmarcamos las actividades seleccionadas.
	$("#div_cronograma_" + g_id_CronogramaActual + " tr.cronograma_actividades").removeClass("ui-state-highlight seleccionado");

	//Gantt.
	$("#div_gantt_" + g_id_CronogramaActual + " tr.gantt_actividades").removeClass("ui-state-highlight seleccionado");
}

//Funci�n para eliminar las dependencias entre actividades.
function fnc_dependencias_actividades_eliminar(p_tipo)
{
	var grid = "";
	var id_CronogramaActividad;
	var id_CronogramaActividadRelacion;
	
	if( p_tipo == 1 )
		grid = "grid_predecesoras";
	else if( p_tipo == 2 )
		grid = "grid_sucesoras";
	
	//Validar que se haya seleccionado una actividad.
	var row = ggrids.obtener_renglon( grid );
	
	if( row )
	{
		id_Requerimiento = $(g_tr_actividad_seleccionado).attr("id_requerimiento");
		id_Partida = $(g_tr_actividad_seleccionado).attr("id_partida");
		
		if( p_tipo == 1 ) //Predecesora.
		{
			id_CronogramaActividad = $(g_tr_actividad_seleccionado).attr("id_cronograma_actividad");
			
			id_CronogramaActividadRelacion = row.id_CronogramaActividadRelacion;
		}
		else if( p_tipo == 2 ) //Sucesora.
		{
			id_CronogramaActividad = row.id_CronogramaActividad;
			
			id_CronogramaActividadRelacion = $(g_tr_actividad_seleccionado).attr("id_cronograma_actividad");
		}
	
		//Eliminamos.
		fnc_peticionAjax({
			url: "./application/app_config_act.cfc",
			parametros: {
				method: "CronogramaActividadesDependencias_Eliminar",
				id_Empresa: g_row_proyectos.id_Empresa,
				id_Cliente: g_row_proyectos.id_Cliente,
				id_Proyecto: g_row_proyectos.id_Proyecto,
				id_Cronograma: g_id_CronogramaActual,
				id_CronogramaActividad: id_CronogramaActividad,
				id_CronogramaActividadRelacion: id_CronogramaActividadRelacion,
				id_Requerimiento: $(g_tr_actividad_seleccionado).attr("id_requerimiento"),
				sn_Cotizacion: 0,
				sn_ActividadesRetrasadas: 0,
				id_TipoRequerimiento: $( "#hid_id_TipoRequerimiento_" + g_id_CronogramaActual ).val()
			},
			f_callback: function(){
				//Recargamos el cronograma.
				fnc_recargar_cronograma(true);
				
				//Actualizamos el grid.
				ggrids.recargar( grid );
			}
		});
	}
	else
		fnc_mostrar_aviso("gd_avisos", "Debe seleccionar una actividad.", 5);
	
}

//Muestra el mensaje de confirmaci�n para iniciar cambios  
function fnc_mostrar_justificacion_proponer_cambios()
{
	
		if( $( "#hid_id_CronogramaEstatus_" + g_id_CronogramaActual ).val() == "6" ) //Si el estatus del proyecto es "en cambios"
		{
			fnc_notificacion("Este Requerimiento cuenta con cambios Pendientes de Autorizar.");
			return;
		}//Si no muestro el dialogo de confirmacion	
		
		//Cargamos los motivos para la justificacion
		$("#tr_motivoLB").show();
		$("#ddl_motivo_lineabase").addClass('obligatorio');
		var parametros = "id_Empresa=" + $("#hid_id_Empresa").val() + "&method=Listar_MotivosLB";
		fnc_peticionAjax({
			url: "./application/App_motivos.cfc",
			parametros: parametros,
			automensaje:false,
			f_callback: function(respuestaJSON)
			{
				//console.log(respuestaJSON);
				
				$("#ddl_motivo_lineabase option[value!=0]").remove();
				
				$.each(respuestaJSON.RS.DATA.ID_MOTIVOLB, function(index, value){
						$("<option>").attr({
							value: value
						}).text( respuestaJSON.RS.DATA.NB_MOTIVOLB[index] ).appendTo("#ddl_motivo_lineabase");
					});
				
				//Pedimos la justificaci�n.
				$("#txt_justificacion_lb").val("");
				$('#btn_justificacion_lb').removeAttr("onClick").attr('onClick', 'fnc_iniciar_cambios();');
				$("#txt_justificacion_lb").removeAttr("onKeyUp").attr("onKeyUp", "fnc_enter_presionado(event, '" + $("#btn_justificacion_lb").attr("onClick") + "');");
				gdialogo.abrir("gd_justificacion_lb");
				$("#txt_justificacion_lb").focus();
			}
		});
	/*var row = ggrids.obtener_renglon("grid_proyectos");
	if( row )
	{*/
		
	/*}
	else
	{
		fnc_notificacion("Debe seleccionar un <b>proyecto</b>.");
	}*/
		
}

//Funci�n para pedir confirmaci�n antes de proponer cambios.
function fnc_mostrar_confirmacion_proponer_cambios()
{
	//Validamos que se haya seleccionado un rengl�n.
	/*var row = ggrids.obtener_renglon("grid_proyectos");

	if( row )
	{*/
		if( $( "#hid_id_CronogramaEstatus_" + g_id_CronogramaActual ).val() == 6 ) //Si el estatus del proyecto es "en cambios"
		{
			fnc_notificacion("Este Requerimiento cuenta con cambios Pendientes de Autorizar.");
			return;
		}
		
		if( fnc_validar_campos_obligatorios("div_justificacion_lb") )
		{
			//Cambiamos el onClick.
			$("#btn_gd_confirmacion").removeAttr("onClick").attr("onClick", "fnc_iniciar_cambios();");
			
			fnc_mostrar_aviso("gd_confirmacion", "<p>Los cambios surgir&aacute;n efecto hasta que sean <b>autorizados</b> por las personas correspondientes.</p><b>&iquest;Desea continuar?</b>", 2);
		}
	/*}
	else
	{
		fnc_notificacion("Debe seleccionar un <b>proyecto</b>.");
	}*/
} 

//Funcion para iniciar cambios //Crea un registro en la linea base con sn_Autorizacion = 1
function fnc_iniciar_cambios()
{
	//Validamos los campos obligatorios.
	/*var row = ggrids.obtener_renglon("grid_proyectos");
	
	if(row)
	{*/

	if( fnc_validar_campos_obligatorios("div_justificacion_lb") )
		{
			fnc_peticionAjax({
				url: "./application/cronograma.cfc",
				parametros: "method=CronogramaLineaBase_Crear&id_Empresa=" + g_row_proyectos.id_Empresa 
					+ "&id_Cliente=" + g_row_proyectos.id_Cliente 
					+ "&id_Proyecto=" + g_row_proyectos.id_Proyecto 
					+ "&id_Cronograma=" + g_id_CronogramaActual
					+ "&id_MotivoLB=" + $("#ddl_motivo_lineabase option:selected").val()
					+ "&de_Justificacion="+ fnc_codificar( $.trim( $("#txt_justificacion_lb").val() ) )
					+ "&sn_Autorizacion=1" 
					+ "&id_TipoRequerimiento=" + $("#hid_id_TipoRequerimiento_" + g_id_CronogramaActual).val()
					+ "&nb_TipoRequerimiento=" + $( "#hid_nb_TipoRequerimiento_" + g_id_CronogramaActual ).val()
					+ "&sn_GestionCambios=1"
					+ "&nu_NumTienda=" + g_row_proyectos.nu_NumTienda
					+ "&nb_Proyecto=" + fnc_codificar( g_row_proyectos.nb_Proyecto )
					+ "&id_Area=" + $("#hid_id_Area").val()
					+ "&id_CronogramaEstatus=" + $( "#hid_id_CronogramaEstatus_" + g_id_CronogramaActual ).val(),
				f_callback: function(respuestaJSON){
					gdialogo.cerrar("gd_confirmacion");
					gdialogo.cerrar("gd_justificacion_lb");
					//ggrids.recargar("grid_proyectos");
					
					//Recargamos el cronograma.
					fnc_recargar_cronograma(true);
				}
			});
		}
	//}

}

//Funcion para agregar planos a las actividades
function fnc_agregar_planos( opcion, id_CronogramaActual )
{
	switch(opcion)
	{
		case 1:
			//Validamos las areas
			var id_Area = $( "#div_cronograma_" + g_id_CronogramaActual + " tr.cronograma_actividades.seleccionado" ).attr("id_Area");

			if( $( "#hid_id_CronogramaEstatus_" + g_id_CronogramaActual ).val() === "6" && id_Area != $("#hid_id_Area").val() )
			{
				fnc_mostrar_aviso( "gd_avisos", "Estas actividades no corresponden a su &Aacute;rea.", 5 );
				return;
			}
			$( "#div_acciones_cronograma").slideUp(200);
			$('#planos_list li').remove();
			
			fnc_peticionAjax({
				url: "./application/app_config_act.cfc",
				parametros: "method=Listar_Planos"
				+ "&id_Empresa=" + g_row_proyectos.id_Empresa
				+ "&id_Cliente=" + g_row_proyectos.id_Cliente
				+ "&id_Proyecto=" + g_row_proyectos.id_Proyecto
				+ "&id_Cronograma=" + g_id_CronogramaActual
				+"&id_CronogramaActividad="+ $( g_tr_cronograma ).attr("id_cronograma_actividad"),
				automensaje: false,
				f_callback: function(respuestaJSON)
				{
					var mostrar = false;
					var nu_row = respuestaJSON.RS.DATA.ID_PLANO.length;
					
					if ( nu_row == 0 )
							mostrar = true
							
					$.each(respuestaJSON.RS.DATA.ID_PLANO, function(index, value)
					{
						var ObjPlano = {
							nb_Plano : respuestaJSON.RS.DATA.NB_PLANO[index],
							pj_Ponderacion : respuestaJSON.RS.DATA.PJ_PONDERACION[index],
							clv_Plano : respuestaJSON.RS.DATA.CLV_PLANO[index],
							sn_Asignado : respuestaJSON.RS.DATA.SN_ASIGNADO[index],
						}
						
						if ( ObjPlano.sn_Asignado == 0 )
							mostrar = true
						
						
						$('#planos_list').append
						(
						'<li class="li_plano '+ ( ObjPlano.sn_Asignado == 1 ? 'disabled' : '' ) +'"' +
							'id_Plano="'+ value +'" '+
							'nb_Plano="'+ ObjPlano.nb_Plano +'">' +
							'<span>'+ ObjPlano.clv_Plano +' - '+ ObjPlano.nb_Plano +'</span>' +
							'<span class="pj_PonderacionPlano"><input class="solo_numeros" value="'+ ObjPlano.pj_Ponderacion +'" '+ ( ObjPlano.sn_Asignado == 1 ? 'disabled' : '' ) +'></input> %</span>' +
						'</li>'
						)
					});
					
					if( mostrar )
						gdialogo.abrir('gd_planos');
					else
						fnc_mostrar_aviso( "gd_avisos", "Ya han sido asignados todos los planos para esta actividad.", 5 );
					
					$('.seleccionado').removeClass('seleccionado ui-state-highlight')
					
					//Agregamos el evento al li
					$('.li_plano').on('click', function(){
						if( !$(this).hasClass('disabled') )
							$(this).toggleClass('ui-state-highlight')
							.siblings()
							.removeClass('ui-state-highlight');
					});
					//Agregamos el evento al input
					
					$('.li_plano').on('click', 'input', function(e){
						e.preventDefault();
						e.stopPropagation();
					});
					$('.li_plano .solo_numeros').on("keypress", function(e){
						return fnc_solo_numeros(e)
					});
					
					if( !ggrids.existe('grid_planos') )
						ggrids.crear('grid_planos')
				
				}
			});

			break;
		case 2:
				var $li = $('#planos_list li');
				var planosXML = "";
				
				//Borramos los planos que tenia en el div
				$('.tr_actividades.seleccionado .span_planos').remove();
				
				
				//Agregamos el numero de planos nuevos
				$('.tr_actividades.seleccionado .nu_Planos').text( ( $li.size() ? $li.size() : '' ) );
				
				if( $li.size() )
				{
					//Validamos que no haya inputs vacios
					var error = false;
					$li.each(function(){
						
						if( $(this).find('.pj_PonderacionPlano input').val().trim() == '' )
						{
							fnc_mostrar_aviso("gd_avisos","Tiene que llenar el Porcentaje de todos los <b>Planos</b>.",5);
							error = true;
							return false;
						}
					});
					
					if( error ) return false;
					
					var suma = 0;
					//Validamos que el porcentaje sea del 100%
					$li.each(function(){
						suma += $(this).find('.pj_PonderacionPlano input').val() * 1;
					});
					
					if( suma != 100 )
					{
						fnc_mostrar_aviso("gd_avisos","Los Porcentaje de los <b>Planos</b> deben sumar un 100%.",5);
						return false;
					}
					
					$li.each(function(){
						var $Plano = $(this);
						var pj_Ponderacion = $Plano.find('.pj_PonderacionPlano input').val().trim();
						
						$('.tr_actividades.seleccionado .div_planos')
						.append('<span class="span_planos" id_Plano="'+ $Plano.attr('id_Plano') +'" nb_Plano="'+ $Plano.attr('nb_Plano') +'" pj_Ponderacion="'+ pj_Ponderacion +'"></span>');
					});
						
					$li.each(function(){
						
						planosXML += "<plano>" +
						"<id_plano>" + $(this).attr("id_plano") + "</id_plano>" +
						"<pj_PonderacionPlano>" + $(this).find('.pj_PonderacionPlano input').val() + "</pj_PonderacionPlano>"
						+
						"</plano>";
					
					});
				}
				
				planosXML = "<root>" + planosXML + "</root>";
					
				
				//Actualizamos
				fnc_peticionAjax(
				{
					url: "./application/app_config_act.cfc",
					parametros: "method=cronogramaPlanosAgregar"
						+ "&id_Empresa=" + g_row_proyectos.id_Empresa
						+ "&id_Cliente=" + g_row_proyectos.id_Cliente
						+ "&id_Proyecto=" + g_row_proyectos.id_Proyecto
						+ "&id_Cronograma=" + g_id_CronogramaActual
						+ "&planosXML=" + planosXML
						+"&id_CronogramaActividad="+ $( g_tr_cronograma ).attr("id_cronograma_actividad"),
					f_callback: function()
					{
						$('.planos tr.ui-state-highlight').removeClass('ui-state-highlight seleccionado')
						gdialogo.cerrar('gd_planos');
					
					}
				});		
			
		break;
	}
}

//Funcion para agregar o eliminar elementos de la plantilla
function fnc_crud_plano( p_accion )
{
	switch( p_accion )
	{
		case 1://Agregar.
				//Obtenemos los renglones
				var row_Planos = ggrids.obtener_renglon("grid_planos");
				
				if( row_Planos ) 
				{
					$.each(row_Planos, function(index, row){
																		
						if( $('#planos_list li[id_Plano="'+ row.id_Plano +'"]').size() )
							return;

						$('#planos_list').append
						(
							'<li class="li_plano"' +
								'id_Plano="'+ row.id_Plano +'" '+
								'nb_Plano="'+ row.nb_Plano +'">' +
								'<span>'+row.clv_Plano+' - '+ row.nb_Plano +'</span>' +
								'<span class="pj_PonderacionPlano"><input class="solo_numeros"></input> %</span>' +
							'</li>'
						)
					});
					
					//Agregamos el evento al li
					$('.li_plano').off();
					$('.li_plano').on('click', function(){
						if( !$(this).hasClass('disabled') )
							$(this).toggleClass('ui-state-highlight')
								.siblings()
								.removeClass('ui-state-highlight');
					});
					//Agregamos el evento al input
					$('.li_plano input').off
					$('.li_plano').on('click', 'input', function(e){
						e.preventDefault();
						e.stopPropagation();
					});
					$('.li_plano .solo_numeros').on("keypress", function(e){
						return fnc_solo_numeros(e)
					});
					ggrids.quitar_seleccion("grid_planos");
				}
				else
					fnc_notificacion('Debe de seleccionar un <b>Plano</b> del grid.');					
			break;
					
		case 2://Eliminar.
				//Obtenemos los elementos seleccionados.
				var $Plano = $('.li_plano.ui-state-highlight');
				
				if( $Plano.size() ) 
				{
					$Plano.remove();
				}
				else
					fnc_notificacion('Debe de seleccionar un <b>Plano</b>.');
			break;
	}
}

//Funci�n para editar el tipo de relaci�n de las actividades.
function fnc_tipos_relaciones_editar()
{
	var $actividad = $("#div_cronograma_contenido_" + g_id_CronogramaActual + " tr.cronograma_actividades.seleccionado");
	var id_CronogramaActividad = $actividad.attr("id_cronograma_actividad");
	var row = ggrids.obtener_renglon("grid_predecesoras");
	
	fnc_peticionAjax({
		url: "./application/app_config_act.cfc",
		parametros: {
			method: "CronogramaActividadesDependencias_Editar",
			id_Empresa: g_row_proyectos.id_Empresa,
			id_Cliente: g_row_proyectos.id_Cliente,
			id_Proyecto: g_row_proyectos.id_Proyecto,
			id_Cronograma: g_id_CronogramaActual,
			id_CronogramaActividad:  id_CronogramaActividad,
			id_CronogramaActividadRelacion: row.id_CronogramaActividadRelacion,
			id_TipoRelacionActividad: $("#ul_tipos_dependencias input:checked" ).attr("id_tipo_relacion_actividad"),
			sn_Cotizacion: 0,
			sn_ActividadesRetrasadas: 1,
			id_Requerimiento: $( "#hid_id_Requerimiento_" + g_id_CronogramaActual ).val(),
			id_TipoRequerimiento: $( "#hid_id_TipoRequerimiento_" + g_id_CronogramaActual ).val()
		},
		f_callback: function(){
			//Recargamos el cronograma.
			fnc_recargar_cronograma(true, function(){
				//Seleccionamos la actividad que estaba seleccionada.
				$("#div_cronograma_contenido_" + g_id_CronogramaActual + " tr.cronograma_actividades[id_cronograma_actividad='" + id_CronogramaActividad + "']")
					.addClass("ui-state-highlight seleccionado");
				
				$("#div_cronograma_contenido_" + g_id_CronogramaActual + " tr.gantt_actividades[id_cronograma_actividad='" + id_CronogramaActividad + "']")
					.addClass("ui-state-highlight seleccionado");
			});
		}
	});
}