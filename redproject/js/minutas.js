function fnc_cargar_formato_minuta(p_id)
	{	
		if(p_id==0)
			$("#minuta_contenido").html("");
		else
		{
			if( p_id == 'N' )
			{		
				fnc_peticionAjax({
					url: "./operaciones/formato_minuta.cfm",
					parametros: "id_Empresa=" + g_row_proyectos.id_Empresa + "&id_Cliente=" + g_row_proyectos.id_Cliente
						+ "&id_Proyecto=" + g_row_proyectos.id_Proyecto + "&id_Cronograma=" + g_id_CronogramaActual
						+ "&sn_Cotizacion=0",
					div_success: "minuta_contenido",
					automensaje: false,
					f_callback: function(){
						//Retringimos las fechas.
						$("#minuta_contenido #txt_fecha_minuta").datepicker( "option", "maxDate", fnc_fecha_objeto( $("#hid_fec_actual").val(), true ) );
						
						$("#minuta_contenido #fh_compromiso").datepicker( "option", "minDate", fnc_fecha_objeto( $("#hid_fec_actual").val(), true ) );
						
						$("#txt_des_minuta").focus();
					}
				});
			}
			else
			{
				//mandamos cargar la minuta					
				fnc_peticionAjax({
					url: "./operaciones/formato_minuta_consultar.cfm",
					parametros: "id_Empresa=" + g_row_proyectos.id_Empresa + "&id_Cliente=" + g_row_proyectos.id_Cliente
						+ "&id_Proyecto=" + g_row_proyectos.id_Proyecto + "&id_Cronograma=" + g_id_CronogramaActual + "&id_Minuta=" + p_id,
					div_success: "minuta_contenido",
					automensaje: false
				});
			}
		}
	}
	
	function fnc_buscar_recurso_externo(p_nombre,p_paterno,p_materno)
	{
		var params="method=Recursos_no_proyecto_Listar_JSON&id_Empresa=" + g_row_proyectos.id_Empresa
			+ "&id_Cliente=" + g_row_proyectos.id_Cliente + "&id_Proyecto=" + g_row_proyectos.id_Proyecto
			+ "&nb_recurso="+$("#nb_recurso").val()+"&nb_paterno="+$("#nb_paterno").val()+"&nb_materno="+$("#nb_materno").val() ;					

		var url = "./application/minutas.cfc?"
		$("#grid_grid_externo_equipo").jqGrid("setGridParam",{
			url: url + params
		}).trigger("reloadGrid");
	}
	
	function fnc_agregar_participante_externo_equipo()
	{
		//Tomar los datos del rengl�n seleccionado.
		var row = $("#grid_grid_externo_equipo").jqGrid("getGridParam", "selrow");
		
		//Validar si se eligi� un rengl�n.
		if( row )
		{	
				var rowData = $("#grid_grid_externo_equipo").getRowData( row );						
				$("#nb_participante_externo_proyecto").val(rowData.nb_NombreCompletoRecurso);
				$("#hdn_id_recurso").val(rowData.id_recurso);
				//limpiar campos de captura
				$("#ddl_roles_externo_proyecto").val(0);
										
				gdialogo.abrir("gd_agregar_externo_proyecto");
				
				$("#ddl_roles_externo_proyecto").focus();
																	
		}
		else
		{
			fnc_notificacion( "Debe seleccionar un recurso." );
		}	
		
	}
	
	function fnc_agregar_externo_proyecto_grid(p_id_recurso)
	{
		
		if( fnc_validar_campos_obligatorios("div_contenido_captura_externo_proyecto") )
		{	
			var encontro_recurso = false;
			  //validar que el recurso no este agregado como participante ya
			  var ids_participantes = $("#grid_grid_participantes").getDataIDs();
			  if(ids_participantes != '')
			  {
				
					for ( var i=0; i<ids_participantes.length; i++)
					{
						if(ids_participantes[i] == p_id_recurso )
						{
							encontro_recurso = true;
						}	
					} 
			  }
			
			  if(!encontro_recurso)
			  {			
				$("#grid_grid_participantes").addRowData($("#hdn_id_recurso").val(),{
					nombre: $.trim( $("#nb_participante_externo_proyecto").val() ),					
					rol: $.trim( $("#ddl_roles_externo_proyecto option:selected").text()),
					id_tipo: "externo_proyecto"	,
					id_recurso: $("#hdn_id_recurso").val(),
					id_rol: $("#ddl_roles_externo_proyecto option:selected").val()		
				});				
					
				gdialogo.cerrar("gd_agregar_externo_proyecto");
			}
			else
			{
				fnc_mostrar_aviso("gd_avisos", "Este participante ya ha sido agregado.", 5);
			}
		}
		
	}
	
function fnc_guardar_minuta()
{
	if( fnc_validar_campos_obligatorios("div_contenido_general_minuta") )
	{		
		var fecha_evaluar = fnc_fecha_objeto( $("#txt_fecha_minuta").val() 
			+ " " + $("#txt_hora_inicia_hrs_reunion").val() + ":" + $("#txt_hora_inicia_mins_reunion").val(), true );			
		
		var fecha_actual = fnc_fecha_objeto( $("#hid_fec_actual").val(), true );
	
		if( fecha_evaluar > fecha_actual )
		{
			fnc_mostrar_aviso("gd_avisos", "La fecha de la minuta debe ser mayor a la fecha acutal.", 5);
			return;
		}		

		var xml_participantes="";
		//traer los ids del grid de accionistas
		var ids_participantes= $("#grid_grid_participantes").getDataIDs(); 				
		//preguntar si los la variable idesta vacia de ser asi se termina de lo contrario continua
		
		if(ids_participantes=='')
		{								
			fnc_mostrar_aviso("gd_avisos", "Debe agregar al menos dos participantes.", 5);								
			return false;
		}
		else
		{
			if(ids_participantes.length<2)
			{
				fnc_mostrar_aviso("gd_avisos", "Debe agregar al menos dos participantes.", 5);	
				return false;
			}
			else
			{
				// ciclo para recorrer el grid del consejo y formar un XML con sus datos
				for(var i=0;i<ids_participantes.length;i++)
				{
					var rowdata= $("#grid_grid_participantes").getRowData(ids_participantes[i]);
														
					 xml_participantes+="<participantes>" 
											+"<id_recurso>" + rowdata.id_recurso + "</id_recurso>" 
											+"<nombre>"+ rowdata.nombre + "</nombre>" 
											+"<rol>"+ rowdata.rol +"</rol>"
											+"<tipo>"+ rowdata.id_tipo +"</tipo>"
											+"<id_rol>"+ rowdata.id_rol +"</id_rol>"																																																																			
									   +"</participantes>";													
				
				}
			
				xml_participantes="<root>"+xml_participantes+"</root>";	
			}						
		}
	
		var bandera_ActividadesCronograma = false;
		
		// elaboramos el xml que contendra las acciones tomadas
		var xml_acciones_tomadas="";
		//traer los ids del grid de accionistas
		var ids_acciones_tomadas = $("#grid_grid_acciones_tomadas").getDataIDs(); 				
		//preguntar si los la variable idesta vacia de ser asi se termina de lo contrario continua
		if(ids_acciones_tomadas=='')
		{
				//fnc_notificacion( "Debe agregar alguna accion." );								
				//return false;
		}
		else
		{
			// ciclo para recorrer el grid del consejo y formar un XML con sus datos
			for(var i=0;i<ids_acciones_tomadas.length;i++)
			{
				var rowdata = $("#grid_grid_acciones_tomadas").getRowData(ids_acciones_tomadas[i]);
				var fecha_formateada="";
				if(rowdata.fecha_compromiso == "Por Calcular")
				{	
					fecha_formateada="N/A";
				}
				else
				{
					var fecha_compromiso_picker=rowdata.fecha_compromiso.split("/");
					fecha_formateada=fecha_compromiso_picker[2]+"-"+fecha_compromiso_picker[1]+"-"+fecha_compromiso_picker[0];															
				}					
														
				 xml_acciones_tomadas+="<acciones_tomadas>" 
											+"<id_recurso>" + rowdata.id_recurso + "</id_recurso>"
											+"<actividad>"+ rowdata.actividad + "</actividad>"  
											+"<fase>"+ rowdata.fase + "</fase>" 
											+"<responsable>"+ rowdata.responsable + "</responsable>" 
											+"<fecha_compromiso>"+ fecha_formateada + "</fecha_compromiso>"
											+"<id_tipo>"+ rowdata.id_tipo  + "</id_tipo>"
											+"<nu_ciclo>"+ rowdata.nu_ciclo  + "</nu_ciclo>"
											+"<id_fase>"+ rowdata.id_fase  + "</id_fase>"
											+"<id_rol>"+ rowdata.id_rol + "</id_rol>" 	
											+"<id_equipo_trabajo>" + rowdata.id_equipo_trabajo + "</id_equipo_trabajo>" 
											+"<sn_enviar_cronograma>"+ rowdata.sn_EnviarCronograma + "</sn_enviar_cronograma>" 																																																																																									
									   +"</acciones_tomadas>";		
															
				if( bandera_ActividadesCronograma == false )
				{
					bandera_ActividadesCronograma = ( rowdata.sn_EnviarCronograma == "1" );
				}
			}
			
			xml_acciones_tomadas="<root>"+xml_acciones_tomadas+"</root>";	
									
		}	

		var params="method=Minuta_Guardar&id_Empresa=" + g_row_proyectos.id_Empresa
			+ "&id_Cliente=" + g_row_proyectos.id_Cliente + "&id_Proyecto=" + g_row_proyectos.id_Proyecto
			+ "&id_Cronograma=" + g_id_CronogramaActual
			+ "&de_minuta=" + fnc_codificar( $.trim( $("#txt_des_minuta").val() ) ) 
			+ "&de_lugar=" + fnc_codificar( $.trim( $("#txt_des_lugar").val() ) )
			+ "&realizada_por=" + $("#hid_id_RecursoConvoca").val() 
			+ "&fh_reunion=" + fnc_fecha_string( fecha_evaluar, "yyyy-mm-dd HH:mm", true )
			+ "&duracion=" + ( $("#txt_duracion_hrs_reunion").val() * 1 + fnc_convertir_a_horas( $("#txt_duracion_mins_reunion").val() * 1, "m" ) )
			+ "&de_correos=" + $("#txt_correos").val() 
			+ "&temas_revisados=" + fnc_codificar( $.trim( $("#txt_des_temas_reunion").val() ) )
			+ "&desarrollo_reunion=" + fnc_codificar( $.trim( $("#txt_des_desarrollo_reunion").val() ) )
			+ "&participantesXML=" + fnc_codificar( xml_participantes ) + "&accionesXML=" + fnc_codificar( xml_acciones_tomadas );
			
			//mandamos llamar por ajax la funcion que guardara la minuta y sus detalles
			fnc_peticionAjax({
				url: "./application/minutas.cfc",
				parametros: params,
				f_callback: function(respuestaJSON){
					//Agregamos la nueva minuta al combo.
					$("<option>").attr({
						value: respuestaJSON.VALUE
					}).text( $.trim( $("#txt_des_minuta").val() ) ).appendTo("#slt_minutas");  
					
					$("#slt_minutas").val(0);
					$("#slt_minutas").trigger("change");
					
					//Si se agregaron acitivdades al cronograma, lo cargamos de nuevo.
					if( bandera_ActividadesCronograma )
						fnc_recargar_cronograma(true);
					
					//mandar llamar la funcion que lista las minutas para refrescar el combo
					/*fnc_peticionAjax({
						url: "./application/minutas.cfc",
						parametros: "method=Minutas_Listar&id_Empresa=" + g_row_proyectos.id_Empresa
							+ "&id_Cliente=" + g_row_proyectos.id_Cliente
							+ "&id_Proyecto=" + g_row_proyectos.id_Proyecto,
						div_success: "recargar_minutas", //ponemos el resultado en este div para luego parsear el jSon
						automensaje: false,
						cache: false,
						f_callback: function(){
						
								//convertimos a objeto el Json
								var objeto = $.parseJSON($.trim($("#recargar_minutas").html()));						
								//colocar los datos en los respectivos campos de texto...
								// borramos el combo para despues volverlo a cargar con la informacion actualizada												
								$("#slt_minutas option[value!='0']").remove();
								//agregamos al combo la opcion de Nueva minuta
								$("#slt_minutas").append(
											$("<option>").attr({value:'N'}).text("Nueva minuta")
											);
								//recorremos los elementos del objeto para agregarlos al combo de minutas
								for (var i=0; i<objeto.RS.ROWCOUNT; i++)
								{
									
										$("#slt_minutas").append(
											$("<option>").attr({value:objeto.RS.DATA.ID_MINUTA[i]}).text(objeto.RS.DATA.DE_MINUTA[i])
											);
																																									
									//alert(objeto.RS.DATA.ID_MINUTA[i]);
//													alert(objeto.RS.DATA.DE_MINUTA[i]);
								}
								
						}
					});*/
				},
				automensaje: true
			});	
																																	
} // fin de la funcion que valida los campos obligatorios


}
		
		
function fnc_opciones_tipo_participante(combo)
{																				
	var tipo_responsable=$("#ddl_responsable option:selected  ").attr("tipo");								
	$("#ddl_fase_acciones").removeClass("obligatorio");
	$("#fh_compromiso").removeClass("obligatorio");			
	if(tipo_responsable != 'equipo')
	{
		$("#td_check_cronograma").html("");
		$("#td_check_cronograma").append("<input id='check_enviar_a_cronograma'  type='checkbox' onclick='fnc_ocultar_fase(this.checked);'   />")
		$("#check_enviar_a_cronograma").attr('checked',false);					
		$("#tr_enviar_cronograma").hide();
		$("#tr_fase").hide();
		//limpiar campo de fecha
		$("#fh_compromiso").val('');					
		$("#tr_fh_compromiso").show();
		//limpiar el combo de la fase
		$("#ddl_fase_acciones").val(0);
		//ver si se seleccione el valor cero para no mostrar nada					
		if(combo.value == 0)
		{
			$("#tr_fh_compromiso").hide();
		}
		//quitar la clase obligatorio
		$("#ddl_fase_acciones").removeClass("obligatorio");
		$("#fh_compromiso").addClass("obligatorio");
	}
	else
	{
												
		$("#td_check_cronograma").html("");
		$("#td_check_cronograma").append("<input id='check_enviar_a_cronograma'  type='checkbox' onclick='fnc_ocultar_fase(this.checked);' checked='checked'  />")
		$("#tr_enviar_cronograma").show();
		$("#check_enviar_a_cronograma").attr('checked',true);
		$("#tr_fase").show();					
		//limpiar campo de fecha
		$("#fh_compromiso").val('');					
		$("#tr_fh_compromiso").hide();
		//limpiar el combo de la fase
		$("#ddl_fase_acciones").val(0);
		//agregar la clase obligatorio a la fase
		$("#ddl_fase_acciones").addClass("obligatorio");
		$("#fh_compromiso").removeClass("obligatorio");					
		
	}				
}
	
function fnc_ocultar_fase(checado)
{
	//agregar la clase obligatorio a la fase
	$("#ddl_fase_acciones").removeClass("obligatorio");
	$("#fh_compromiso").removeClass("obligatorio");		
	
	if(checado)
	{
		$("#tr_fase").show();
		$("#tr_fh_compromiso").hide();
		//limpiar campo de fecha
		$("#fh_compromiso").val('');
		//limpiar el combo de la fase
		$("#ddl_fase_acciones").val(0);
		//agregar la clase obligatorio a la fase
		$("#ddl_fase_acciones").addClass("obligatorio");
		$("#fh_compromiso").removeClass("obligatorio");					
	}
	else
	{
		$("#tr_fase").hide();
		//limpiar campo de fecha
		$("#fh_compromiso").val('');
		$("#tr_fh_compromiso").show();
		//limpiar el combo de la fase
		$("#ddl_fase_acciones").val(0);
		//agregar la clase obligatorio a la fase
		$("#ddl_fase_acciones").removeClass("obligatorio");
		$("#fh_compromiso").addClass("obligatorio");
	}
}

function fnc_agregar_participante()
{
	//limpiar los campos de texto del dialogo y el combo de tipo de participante
	$("#gd_agregar_participantes input:text").each(function(index)
		{$(this).val('');
	});
	$("#ddl_tipo_participante").val(0);
	$("#div_agregar_equipo_trabajo").hide();
	$("#div_agregar_externo_equipo").hide();
	$("#div_contenido_captura_externo_rr").hide();
	$("#btn_dg_agregar_participante_externo_rr").hide();
	
	gdialogo.abrir("gd_agregar_participantes");
}

function fnc_agregar_participante_grid()
{
	if( fnc_validar_campos_obligatorios("div_gd_capturar_participante") )
	{
		$("#grid_grid_participantes").addRowData(id_participante,{
			nombre: $.trim( $("#nb_participante").val() ),
			rol: $.trim( $("#de_rol").val() ),
			id_rol: "",
			id_equipo_trabajo: ""		
		});
		
		id_participante = id_participante + 1;
		gdialogo.cerrar("gd_agregar_participantes");
	}
}
//Funci�n para eliminar participantes despu�s la confirmaci�n.
	function fnc_eliminar_participantes()
	{
		//Tomar los datos del rengl�n seleccionado.
		var row = $("#grid_grid_participantes").jqGrid("getGridParam", "selrow");		
		
		//Validar si se eligi� un rengl�n.
		if( row )
		{				
			//obtenemos los valores de los grids para despues cotejarlos
			 var ids_participantes_agregados = $("#grid_grid_participantes").getDataIDs();
			 var ids_responsables_agregados = $("#grid_grid_acciones_tomadas").getDataIDs();
			 
			 //verificamos si es un recurso de el proyecto para agregarlo otra vez como recurso disponible
				var rowData = $("#grid_grid_participantes").getRowData( row );	
				if(rowData.id_tipo=='equipo')
				{
					$("tr.tr_participantes_agregados").each(function(index){
						if( row == $(this).attr("id_recurso") )
						{
							var nombre=$(this).attr("nb_completo");
							var rol=$(this).attr("de_rol");
							var id_recurso=$(this).attr("id_recurso");							
							$(this).remove();

							$("table.empleados_agregar").append(
								"<tr class='tr_empleados_disponibles' nb_completo='" + nombre + "' de_rol='" + rol + "' id_recurso='" + id_recurso +
									"' id_equipo_trabajo='" + $(this).attr("id_equipo_trabajo") + "' >" +
									$(this).html() + 
								"</tr>");						
							
							fnc_elementos_draggables({
								elementos: ".tr_empleados_disponibles, .tr_participantes_agregados"
							});	
						}						
					});
				}
				
				//aqui eliminamos al participante del grid										
				$("#grid_grid_participantes").delRowData( row );
				//verificamos si el participante que se eliminara tiene responsabilidades asignadas
				for(var i=0; i<ids_participantes_agregados.length; i++)
			 	{			 	
					for(var j=0; j<ids_responsables_agregados.length; j++)
					{
						var rowData_resp = $("#grid_grid_acciones_tomadas").getRowData( ids_responsables_agregados[j] );
						if(row==rowData_resp.id_recurso)
						{
							$("#grid_grid_acciones_tomadas").delRowData(ids_responsables_agregados[j]);					
							
						}
					}
			 	} 
			
			$("tr.sin_recursos_disponibles").remove();
				
			if( $("table.empleados_agregados tbody tr").size() == 0 )
			{
				$("table.empleados_agregados tbody").append(
					'<tr class="sin_particiapntes">' +
						'<td colspan="2" style="text-align: center;">' +
							'<span style="font-style: italic">No existen participantes agregados.</span>' +
						'</td>' +
					 '</tr>');
			}			
		}
		else
		{
			fnc_notificacion( "Debe seleccionar un participante." );
		}		
	}
	
function fnc_agregar_accion()
{
	//limpiar los campos de texto del dialogo
	$("#gd_agregar_acciones_tomadas input:text").each(function(index)
		{$(this).val('');
	});
	
	var ids_responsables = $("#grid_grid_participantes").getDataIDs();
	var cadena_combo="<select id='ddl_responsable' class='input_text obligatorio' acercade='Responsable' onchange='fnc_opciones_tipo_participante(this)'> <option value='0'>Seleccione...</option> ";
	var cadena_combo_options="";
	var combo_final="</select>";
	
	if(ids_responsables != '')
	{						

		for ( var i=0; i<ids_responsables.length; i++)
		{
			var renglon = $("#grid_grid_participantes").getRowData(ids_responsables[i]);
			cadena_combo_options = cadena_combo_options + 
				"<option id_rol='" + renglon.id_rol + "' value='" + ids_responsables[i] + "' tipo='" + renglon.id_tipo 
					+ "' id_equipo_trabajo='" + renglon.id_equipo_trabajo + "'>" + renglon.nombre + "</option>";								
		} 
		
	}
	
	var combo_terminado=cadena_combo+cadena_combo_options+combo_final;	
	
	$("#ddl_responsable").remove();				  
	$("#td_ddl_responsable").append(combo_terminado);
	
	//limpiamos el combo
	$("#ddl_fase_acciones option[value!='0']").remove();
	$("#ddl_fase_acciones optgroup").remove();
	//Cargamos las fases que existen en el combo por medio de las que estan en el cronograma.
	$("tr.cronograma_ciclos").each(function(){
		
		$("<optgroup>").attr({
			label: "Ciclo " + $(this).attr("nu_ciclo")
		}).appendTo("#ddl_fase_acciones");
		
		$("tr.cronograma_fases[nu_ciclo='" + $(this).attr("nu_ciclo") + "']").each(function(){
			$("<option>").attr({
				nu_ciclo: $(this).attr("nu_ciclo"),
				id_fase: $(this).attr("id_fase")
			}).text( $.trim( $(this).find("td:first div").text() ) ).appendTo("#ddl_fase_acciones");
		});
	});
	//limpiar el dialogo de las cosas ocultas
	$("#tr_enviar_cronograma").hide();
	$("#tr_fase").hide();
	$("#tr_fh_compromiso").hide();
	//abrir el dialogo para la captura de acciones tomadas		
	gdialogo.abrir("gd_agregar_acciones_tomadas");
	
}
function fnc_agregar_accion_grid()
{
	
	if( fnc_validar_campos_obligatorios("div_gd_capturar_accion") )
	{
		
		var fecha_correcta=true;
		var fecha_evaluar=$("#fh_compromiso").val().split("/");
				var fecha_actual = fnc_fecha_objeto( $("#hid_fec_actual").val(), true );				
								
				if(parseInt(fecha_evaluar[2]) < parseInt(fecha_actual.getFullYear()))
				{					
					fnc_notificacion( "La fecha no puede ser menor a la actual." );
					fecha_correcta=false;
					return false;
					
				}
				else
				{
					if(parseInt(fecha_evaluar[2]) == parseInt(fecha_actual.getFullYear()))
					{	
							if(fecha_evaluar[1] < parseInt(fecha_actual.getMonth()+1))
						{					
							fnc_notificacion( "La fecha no puede ser menor a la actual." );
							fecha_correcta=false;
							return false;
							
						}
						else
						{
							if(fecha_evaluar[1] == parseInt(fecha_actual.getMonth()+1))
							{	
											
									if(fecha_evaluar[0] < parseInt(fecha_actual.getDate()))
									{	
												
										fnc_notificacion( "La fecha no puede ser menor a la actual." );
										fecha_correcta=false;
										return false;
									}
								
							}	
						}				
						
					}
						
				}
				
				
		if(fecha_correcta)
		{
			var fecha_agregar="";
			var fase_agregar="";
			var nu_ciclo_agregar="";
			var id_fase_agregar="";
			
			if( $("#ddl_responsable option:selected").attr("tipo") == "equipo" )
			{
				if( $("#check_enviar_a_cronograma").is(":checked") )
				{
					fase_agregar = $.trim( $("#ddl_fase_acciones option:selected").text() );
					nu_ciclo_agregar = $.trim($("#ddl_fase_acciones option:selected").attr("nu_ciclo"));
					id_fase_agregar = $.trim($("#ddl_fase_acciones option:selected").attr("id_fase"));
					fecha_agregar = "Por Calcular";
				}
				else
				{
					fase_agregar = "No Aplica";
					nu_ciclo_agregar = "NULL";
					id_fase_agregar = "NULL";
					fecha_agregar = $("#fh_compromiso").val();
				}
			}
			else 
			{
				if( $("#ddl_responsable option:selected").attr("tipo") == "externo_rr" )
				{
					fase_agregar = "No Aplica";
					nu_ciclo_agregar = "NULL";
					id_fase_agregar = "NULL";
					fecha_agregar = $("#fh_compromiso").val();
				}
				else
				{
					fase_agregar = "No Aplica";
					nu_ciclo_agregar = "NULL";
					id_fase_agregar = "NULL";
					fecha_agregar = $("#fh_compromiso").val();
				}
			}
			
			/*if( $("#ddl_fase_acciones option:selected").val() == 0 )
			{
				fase_agregar="Por Calcular";
			}
			else
			{
				fase_agregar = $.trim( $("#ddl_fase_acciones option:selected").text() );
				nu_ciclo_agregar = $.trim($("#ddl_fase_acciones option:selected").attr("nu_ciclo"));
				id_fase_agregar = $.trim($("#ddl_fase_acciones option:selected").attr("id_fase"));
				
			}
			
			if( $("#fh_compromiso").val() == '')
			{
				fecha_agregar = "Por Calcular";
				nu_ciclo_agregar = "NULL";
				id_fase_agregar = "NULL";
			}
			else
			{
				fecha_agregar=$.trim( $("#fh_compromiso").val() );
			}*/

			$("#grid_grid_acciones_tomadas").addRowData(id_accion_tomada,{
					actividad: $.trim( $("#de_actividad").val() ),
					fase: fase_agregar,
					fecha_compromiso: fecha_agregar ,
					responsable: $.trim( $("#ddl_responsable option:selected").text() ),
					id_recurso: $.trim( $("#ddl_responsable option:selected").val()) ,
					id_tipo: $.trim( $("#ddl_responsable option:selected").attr("tipo") ),
					nu_ciclo: nu_ciclo_agregar ,
					id_fase: id_fase_agregar,
					id_rol: $("#ddl_responsable option:selected").attr("id_rol"),
					sn_EnviarCronograma: ( $("#check_enviar_a_cronograma").is(":checked") ? 1 : 0 ),
					id_equipo_trabajo: $("#ddl_responsable option:selected").attr("id_equipo_trabajo")
				});			
				
				id_accion_tomada=id_accion_tomada+1;
				$("#div_gd_capturar_accion input").each(function(){
					$(this).val('');
				});
																	
				$("#ddl_fase_acciones").val(0);
				$("#ddl_responsable").val(0);
				//gdialogo.cerrar("gd_agregar_acciones_tomadas");
				//limpiar el dialogo de las cosas ocultas
				$("#tr_enviar_cronograma").hide();
				$("#tr_fase").hide();
				$("#tr_fh_compromiso").hide();
		}
	}	
	
}
//Funci�n para eliminar participantes despu�s la confirmaci�n.
	function fnc_eliminar_accion()
	{
		//Tomar los datos del rengl�n seleccionado.
		var row = $("#grid_grid_acciones_tomadas").jqGrid("getGridParam", "selrow");
		
		//Validar si se eligi� un rengl�n.
		if( row )
		{	
				var rowData = $("#grid_grid_acciones_tomadas").getRowData( row );							
				$("#grid_grid_acciones_tomadas").delRowData( row );
		}
		else
		{
			fnc_notificacion( "Debe seleccionar una acci&oacute;n." );
		}		
	}
	
function fnc_modal_agregar_participante(p_valor)
{
	if(p_valor==0)
	{
		$("#gd_agregar_participantes input:text").each(function(index){
			$(this).val('');
		});
			
		$("#btn_dg_agregar_participante_externo_rr").hide();
		$("#div_agregar_equipo_trabajo").hide();
		$("#div_agregar_externo_equipo").hide();
		$("#div_contenido_captura_externo_rr").hide();
		//limpiamnos los campos de captura del filtro de recursos al proyecto
		$("#nb_recurso").val('');
		$("#nb_paterno").val('');
		$("#nb_materno").val('');		
	}
	
	if(p_valor ==1)
	{
		$("#btn_dg_agregar_participante_externo_rr").hide();
		$("#div_agregar_externo_equipo").hide();
		$("#div_contenido_captura_externo_rr").hide();
		$("#div_agregar_equipo_trabajo").show();
		//limpiamnos los campos de captura del filtro de recursos al proyecto
		$("#nb_recurso").val('');
		$("#nb_paterno").val('');
		$("#nb_materno").val('');		
		
	}
	
	if(p_valor==2)
	{
	
		$("#btn_dg_agregar_participante_externo_rr").hide();
		$("#div_agregar_equipo_trabajo").hide();
		$("#div_contenido_captura_externo_rr").hide();
		$("#div_agregar_externo_equipo").show();
		ggrids.crear("grid_externo_equipo");
		//limpiamnos los campos de captura del filtro de recursos al proyecto
		$("#nb_recurso").val('');
		$("#nb_paterno").val('');
		$("#nb_materno").val('');
		
		$("#nb_recurso").focus();
	}
	
	if(p_valor==3)
	{
		$("#div_agregar_equipo_trabajo").hide();
		$("#div_agregar_externo_equipo").hide();
		$("#div_contenido_captura_externo_rr").show();
		
		$("#btn_dg_agregar_participante_externo_rr").show();
		//limpiamnos los campos de captura del filtro de recursos al proyecto
		$("#nb_recurso").val('');
		$("#nb_paterno").val('');
		$("#nb_materno").val('');
		
		$("#nb_participante_externo_rr").focus();
	}
	
	gdialogo.re_ajustar("gd_agregar_participantes");
}

function fnc_agregar_externo_rr_grid()
{
	if( fnc_validar_campos_obligatorios("div_contenido_captura_externo_rr") )
	{
	$("#grid_grid_participantes").addRowData("externo"+id_participante_externo_rr,{
				nombre: $.trim( $("#nb_participante_externo_rr").val() ),
				rol: $.trim( $("#de_rol_externo_rr").val() )	,
				id_tipo: "externo_rr",
				id_recurso: "externo"+id_participante_externo_rr,
				id_rol: "",
				id_equipo_trabajo: ""			
			});
			
			//gdialogo.cerrar("gd_agregar_participantes");
			$("#div_contenido_captura_externo_rr input").each(function(index)
			{$(this).val('');			
			});			
	}
	
	id_participante_externo_rr=id_participante_externo_rr+1;
}	