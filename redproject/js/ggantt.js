ggantt = {
	array_meses: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
	actividadesXML: "",
	fec_inicio_gantt: "",
	fec_fin_gantt: "",
	cuerpo: "",
	fec_sig: "",
	fec_inicio_act: "",
	fec_fin_act: "",
	dias: "",
	meses_gantt: 0,
	colspan_meses_gantt: 0,
	
	crear: function(p_opciones){
		ggantt.actividadesXML = p_opciones.actividadesXML;
		
		if( $(p_opciones.actividadesXML).find("fechas").attr("fecha_inicio") != "" && $(p_opciones.actividadesXML).find("fechas").attr("fecha_fin") != "" )
		{
			$("#span_fecha_inicio_proyecto").text( fnc_fecha_string( fnc_fecha_objeto( $(p_opciones.actividadesXML).find("fechas").attr("fecha_inicio"), true ), "dd/mm/yyyy", true ) );
			$("#span_fecha_fin_proyecto").text( fnc_fecha_string( fnc_fecha_objeto( $(p_opciones.actividadesXML).find("fechas").attr("fecha_fin"), true ), "dd/mm/yyyy", true ) );
			$("#span_duracion_horas_proyecto").text( $(p_opciones.actividadesXML).find("fechas").attr("total_horas") );
			$("#span_duracion_dias_proyecto").text( $(p_opciones.actividadesXML).find("fechas").attr("dias_duracion") );
						
			ggantt.fec_inicio_gantt = fnc_fecha_objeto( $(p_opciones.actividadesXML).find("fechas").attr("fecha_inicio"), true );
			ggantt.fec_fin_gantt = fnc_fecha_objeto( $(p_opciones.actividadesXML).find("fechas").attr("fecha_fin"), true );
	
			var table_principal = "<table class='ggantt'>";
			var thead = "<thead><tr class='ui-state-default'><th colspan='5' style='text-align: left;'>Gantt</th>";
			
			//Calculamos cu�ntos meses hay entre las dos fechas.
			ggantt.fec_sig = new Date( ggantt.fec_inicio_gantt.getFullYear(), ggantt.fec_inicio_gantt.getMonth(), 1 );
			ggantt.meses_gantt = 0;
			while( ggantt.fec_sig.getTime() <= ggantt.fec_fin_gantt.getTime() )
			{
				ggantt.fec_sig = new Date( ggantt.fec_sig.getFullYear(), ggantt.fec_sig.getMonth() + 1, 1 );
				ggantt.meses_gantt++;
			}
			
			//Pintamos todos los meses.
			ggantt.fec_sig = ggantt.fec_inicio_gantt;
			ggantt.colspan_meses_gantt = 0;
			for( x=0; x<ggantt.meses_gantt; x++ )
			{	
				thead += "<th style='text-align: left; border-right: 1px solid #D3D3D3;'>" + ggantt.array_meses[ ggantt.fec_sig.getMonth() ] + "</th>";
				
				ggantt.fec_sig = new Date( ggantt.fec_sig.getFullYear(), ggantt.fec_sig.getMonth() + 1, 1 );
				ggantt.colspan_meses_gantt++;
			}
	
			thead += "</tr></thead>";
			table_principal += thead;
			ggantt.cuerpo = "<tbody>" +
						"<tr class='ui-state-default'>" +
							"<td style='border: 1px solid #D3D3D3; text-align: center; padding: 2px;'>Actividad</td>" +
							"<td style='border: 1px solid #D3D3D3; text-align: center; padding: 2px;'>Inicia</td>" +
							"<td style='border: 1px solid #D3D3D3; text-align: center; padding: 2px;'>Termina</td>" +
							"<td style='border: 1px solid #D3D3D3; text-align: center; padding: 2px;'>Horas</td>" +
							"<td style='border: 1px solid #D3D3D3; text-align: center; padding: 2px;'>Recurso</td>";
			
			//Calculamos los d�as de diferencia que hay entre las dos fechas.
			var dias = ggantt.fec_fin_gantt.getTime() - ggantt.fec_inicio_gantt.getTime();
			dias = dias / 1000;
			dias = dias / 60;
			dias = dias / 60;
			ggantt.dias = Math.ceil( dias / 24 ) + 1;
			
			//Recorremos mes por mes y pintamos los d�as correspondientes.
			ggantt.fec_sig = new Date( ggantt.fec_inicio_gantt.getFullYear(), ggantt.fec_inicio_gantt.getMonth(), 1 );
			var mes_actual_gantt = ggantt.fec_sig.getMonth();
			var dia_sig_gantt;
			for( x=0; x<ggantt.meses_gantt; x++ )
			{
				dia_sig_gantt = 1;
				ggantt.cuerpo += "<td style='width: 100%; text-align: center;'><table style='width: 100%; border-collapse: collapse;'><tr>";
				
				while( mes_actual_gantt == ggantt.fec_sig.getMonth() )
				{	
					ggantt.cuerpo += "<td style='border-right: 1px solid #D3D3D3; padding: 4px;'><span>" + ggantt.fec_sig.getDate() + "</span></td>";	
					ggantt.fec_sig = new Date( ggantt.fec_sig.getFullYear(), ggantt.fec_sig.getMonth(), ++dia_sig_gantt );
				}
				
				ggantt.cuerpo += "</tr></table></td>";
				mes_actual_gantt = ggantt.fec_sig.getMonth(); 
			}
			
			ggantt.cuerpo += "</tr>";
			
			//Pintamos los ciclos.
			var fec_inicio_act;
			var fec_fin_act;
			var clase_actividad_ggantt = "";
			$(p_opciones.actividadesXML).find("ciclo").each(function(){
				ggantt.fec_inicio_act = fnc_fecha_objeto( $(this).find("inicia").text(), true );
				ggantt.fec_fin_act = fnc_fecha_objeto( $(this).find("termina").text(), true );
				
				fec_inicio_act = ( $(this).find("inicia").text() == "" ? "" : fnc_fecha_string( fnc_fecha_objeto( $(this).find("inicia").text(), true ), "dd/mm/yyyy", true ) );
				fec_fin_act = ( $(this).find("termina").text() == "" ? "" : fnc_fecha_string( fnc_fecha_objeto( $(this).find("termina").text(), true ), "dd/mm/yyyy", true ) );
				ggantt.cuerpo += "<tr class='ui-widget-header cronograma_ciclos_ggantt' nu_ciclo='" + $(this).attr("nu_ciclo") + "' " +
									" style='cursor: pointer;' " + " id_cronograma='" + $(this).find("id_cronograma").text() + "' " +
									"onClick='fnc_mostrar_ocultar_actividades(\"div_gantt_" + $(this).find("id_cronograma").text() + "\", true, this, \"_ggantt\");'>" +
									"<td nowrap='nowrap' class='td_actividad_ggantt' title='" + $(this).find("descripcion").text() + 
										"' style='border: 1px solid #D3D3D3; padding: 2px;'>" +
										$(this).find("descripcion").text() +
									"</td>" +
									"<td class='td_actividad_fec_inicio_ggantt' style='text-align: center; border: 1px solid #D3D3D3; padding: 2px;' " +
										"nowrap='nowrap' >" + 
										fec_inicio_act + 
									"</td>" +
									"<td class='td_actividad_fec_fin_ggantt' style='text-align: center; border: 1px solid #D3D3D3; padding: 2px;' " +
										"nowrap='nowrap'>" +
										fec_fin_act + 
									"</td>" +
									"<td class='td_actividad_horas_ggantt' style='text-align: center; border: 1px solid #D3D3D3; padding: 2px;'>" +
										$(this).find("horas").text() + 
									"</td>" +
									"<td class='td_recurso_ggantt' style='border: 1px solid #D3D3D3; padding: 2px; text-align: center;'>" +
										". . ."	
									"</td>";
				
				//Obtenemos los recursos asignados a esta actividad.
				
				ggantt.fec_sig = new Date( ggantt.fec_inicio_gantt.getFullYear(), ggantt.fec_inicio_gantt.getMonth(), 1 );
				if( ggantt.fec_inicio_act == null || ggantt.fec_fin_act == null )
				{
					ggantt.cuerpo += "<td colspan='" + ggantt.colspan_meses_gantt + "' style='padding: 4px;'><div style='visibility: hidden; height: 2px;'></div></td>";
					ggantt.cuerpo += "</tr></td>";
				}
				else
				{
					mes_actual_gantt = ggantt.fec_sig.getMonth();
					var dia_sig_gantt;
					for( x=0; x<ggantt.meses_gantt; x++ )
					{
						dia_sig_gantt = 1;
						ggantt.cuerpo += "<td style='width: 100%;'><table style='width: 100%; border-collapse: collapse;'><tr>";
	
						while( mes_actual_gantt == ggantt.fec_sig.getMonth() )
						{
							//Marcamos el rango de d�as que abarca la petici�n.
							if( ggantt.fec_sig >= ggantt.fec_inicio_act && ggantt.fec_sig <= ggantt.fec_fin_act )
							{		
								if( fnc_fecha_string( ggantt.fec_sig, "dd/mm/yyyy", true ) == fnc_fecha_string( ggantt.fec_inicio_act, "dd/mm/yyyy", true ) )
									clase_actividad_ggantt = "class='td_dia_actividad_gantt border_linea_tiempo_gantt_izq'";
								else
								{
									if( fnc_fecha_string( ggantt.fec_sig, "dd/mm/yyyy", true ) == fnc_fecha_string( ggantt.fec_fin_act, "dd/mm/yyyy", true ) )
									{
										clase_actividad_ggantt = "class='td_dia_actividad_gantt border_linea_tiempo_gantt_der'";
									}
									else
										clase_actividad_ggantt = "class='td_dia_actividad_gantt'";
								}
								
								if( fnc_fecha_string( ggantt.fec_inicio_act, "dd/mm/yyyy", true ) == fnc_fecha_string( ggantt.fec_fin_act, "dd/mm/yyyy", true ) )
									clase_actividad_ggantt = "class='td_dia_actividad_gantt border_linea_tiempo_gantt_ambos'";
								
								ggantt.cuerpo += "<td " + clase_actividad_ggantt + " title='" + fnc_poner_ceros_izq(ggantt.fec_sig.getDate(), 2) + "/" + ggantt.array_meses[ ggantt.fec_sig.getMonth() ] + "/" + ggantt.fec_sig.getFullYear() + "'><div style='visibility: hidden; height: 2px;'>" + ggantt.fec_sig.getDate() + "</div></td>";
							}
							else
								ggantt.cuerpo += "<td style='padding: 4px;'><div style='visibility: hidden; height: 2px;'>" + ggantt.fec_sig.getDate() + "</div></td>";
							
							ggantt.fec_sig = new Date( ggantt.fec_sig.getFullYear(), ggantt.fec_sig.getMonth(), ++dia_sig_gantt );
						}
						
						ggantt.cuerpo += "</tr></table></td>";
						mes_actual_gantt = ggantt.fec_sig.getMonth();
					}
				}
				
				ggantt.cuerpo += "</tr>";
				
				//Pintamos las fases.
				if( $(p_opciones.actividadesXML).find("fase[nu_ciclo='" + $(this).attr("nu_ciclo") +"']").size() > 0 )
					ggantt.pintar_fases(p_opciones, "fase[nu_ciclo='" + $(this).attr("nu_ciclo") +"']");
			});
			
			ggantt.cuerpo += "</tbody></table>";
			table_principal += ggantt.cuerpo;
			
			$( "#" + p_opciones.id ).html( table_principal );
			
			//Le agregamos el "onClick" a las actividades.
			$( "#" + p_opciones.id + " tr.cronograma_actividades_ggantt" ).attr("onClick", "$('#" + p_opciones.id 
				+ " tr.cronograma_actividades_ggantt').removeClass('ui-state-highlight'); $(this).addClass('ui-state-highlight');")
		}
		else
			fnc_mostrar_aviso( "gd_avisos", "No existen actividades con fecha definida.", 5 );
	},
	pintar_fases: function(p_opciones, p_des_busqueda){
		//Recorremos todos los ciclos.
		var fec_inicio_act;
		var fec_fin_act;
		var clase_actividad_ggantt = "";
		$(p_opciones.actividadesXML).find(p_des_busqueda).each(function(){
			ggantt.fec_inicio_act = fnc_fecha_objeto( $(this).find("inicia").text(), true );
			ggantt.fec_fin_act = fnc_fecha_objeto( $(this).find("termina").text(), true );
			
			fec_inicio_act = ( $(this).find("inicia").text() == "" ? "" : fnc_fecha_string( fnc_fecha_objeto( $(this).find("inicia").text(), true ), "dd/mm/yyyy", true ) );
			fec_fin_act = ( $(this).find("termina").text() == "" ? "" : fnc_fecha_string( fnc_fecha_objeto( $(this).find("termina").text(), true ), "dd/mm/yyyy", true ) );
			ggantt.cuerpo += "<tr class='ui-state-default cronograma_fases_ggantt' style='cursor: pointer;'" + " id_cronograma='" + $(this).find("id_cronograma").text() + "' " +
								"nu_ciclo='" + $(this).attr("nu_ciclo") + "' id_fase='" + $(this).attr("id_fase") + "' " +
								"onClick='fnc_mostrar_ocultar_actividades(\"div_gantt_" + $(this).find("id_cronograma").text() + "\", false, this, \"_ggantt\");'>" +
								"<td nowrap='nowrap' class='td_actividad_ggantt' title='" + $(this).find("descripcion").text() + 
									"' style='border: 1px solid #D3D3D3; padding: 2px;'>" +
									"<div style='margin-left: 15px;'>" + $(this).find("descripcion").text() + "</div>" +
								"</td>" +
								"<td class='td_actividad_fec_inicio_ggantt' style='text-align: center; border: 1px solid #D3D3D3; padding: 2px;' " +
									"nowrap='nowrap' >" + 
									fec_inicio_act + 
								"</td>" +
								"<td class='td_actividad_fec_fin_ggantt' style='text-align: center; border: 1px solid #D3D3D3; padding: 2px;' " +
									"nowrap='nowrap'>" +
									fec_fin_act + 
								"</td>" +
								"<td class='td_actividad_horas_ggantt' style='text-align: center; border: 1px solid #D3D3D3; padding: 2px;'>" +
									$(this).find("horas").text() + 
								"</td>" +
								"<td class='td_recurso_ggantt' style='border: 1px solid #D3D3D3; padding: 2px; text-align: center;'>" +
									". . ."	
								"</td>";
			
			//Obtenemos los recursos asignados a esta actividad.
			
			ggantt.fec_sig = new Date( ggantt.fec_inicio_gantt.getFullYear(), ggantt.fec_inicio_gantt.getMonth(), 1 );
			if( ggantt.fec_inicio_act == null || ggantt.fec_fin_act == null )
			{
				ggantt.cuerpo += "<td colspan='" + ggantt.colspan_meses_gantt + "' style='padding: 4px;'><div style='visibility: hidden; height: 2px;'></div></td>";
				ggantt.cuerpo += "</tr></td>";
			}
			else
			{
				mes_actual_gantt = ggantt.fec_sig.getMonth();
				var dia_sig_gantt;
				for( x=0; x<ggantt.meses_gantt; x++ )
				{
					dia_sig_gantt = 1;
					ggantt.cuerpo += "<td style='width: 100%;'><table style='width: 100%; border-collapse: collapse;'><tr>";

					while( mes_actual_gantt == ggantt.fec_sig.getMonth() )
					{
						//Marcamos el rango de d�as que abarca la petici�n.
						if( ggantt.fec_sig >= ggantt.fec_inicio_act && ggantt.fec_sig <= ggantt.fec_fin_act )
						{		
							if( fnc_fecha_string( ggantt.fec_sig, "dd/mm/yyyy", true ) == fnc_fecha_string( ggantt.fec_inicio_act, "dd/mm/yyyy", true ) )
								clase_actividad_ggantt = "class='td_dia_actividad_gantt border_linea_tiempo_gantt_izq'";
							else
							{
								if( fnc_fecha_string( ggantt.fec_sig, "dd/mm/yyyy", true ) == fnc_fecha_string( ggantt.fec_fin_act, "dd/mm/yyyy", true ) )
								{
									clase_actividad_ggantt = "class='td_dia_actividad_gantt border_linea_tiempo_gantt_der'";
								}
								else
									clase_actividad_ggantt = "class='td_dia_actividad_gantt'";
							}
							
							if( fnc_fecha_string( ggantt.fec_inicio_act, "dd/mm/yyyy", true ) == fnc_fecha_string( ggantt.fec_fin_act, "dd/mm/yyyy", true ) )
								clase_actividad_ggantt = "class='td_dia_actividad_gantt border_linea_tiempo_gantt_ambos'";
							
							ggantt.cuerpo += "<td " + clase_actividad_ggantt + " title='" + fnc_poner_ceros_izq(ggantt.fec_sig.getDate(), 2) + "/" + ggantt.array_meses[ ggantt.fec_sig.getMonth() ] + "/" + ggantt.fec_sig.getFullYear() + "'><div style='visibility: hidden; height: 2px;'>" + ggantt.fec_sig.getDate() + "</div></td>";
						}
						else
							ggantt.cuerpo += "<td style='padding: 4px;'><div style='visibility: hidden; height: 2px;'>" + ggantt.fec_sig.getDate() + "</div></td>";
						
						ggantt.fec_sig = new Date( ggantt.fec_sig.getFullYear(), ggantt.fec_sig.getMonth(), ++dia_sig_gantt );
					}
					
					ggantt.cuerpo += "</tr></table></td>";
					mes_actual_gantt = ggantt.fec_sig.getMonth();
				}
			}
			
			ggantt.cuerpo += "</tr>";
			
			//Pintamos las actividades.
			if( $(p_opciones.actividadesXML).find("actividad[nu_ciclo='" + $(this).attr("nu_ciclo") +"'][id_fase='" + $(this).attr("id_fase") + "']").size() > 0 )
				ggantt.pintar_acttividades(p_opciones, "actividad[nu_ciclo='" + $(this).attr("nu_ciclo") +"'][id_fase='" + $(this).attr("id_fase") + "']");
		});
	},
	pintar_acttividades: function(p_opciones, p_des_busqueda){
		//Recorremos todos los ciclos.
		var fec_inicio_act;
		var fec_fin_act;
		var clase_actividad_ggantt = "";
		$(p_opciones.actividadesXML).find(p_des_busqueda).each(function(){
			ggantt.fec_inicio_act = fnc_fecha_objeto( $(this).find("inicia").text(), true );
			ggantt.fec_fin_act = fnc_fecha_objeto( $(this).find("termina").text(), true );
			
			fec_inicio_act = ( $(this).find("inicia").text() == "" ? "" : fnc_fecha_string( fnc_fecha_objeto( $(this).find("inicia").text(), true ), "dd/mm/yyyy", true ) );
			fec_fin_act = ( $(this).find("termina").text() == "" ? "" : fnc_fecha_string( fnc_fecha_objeto( $(this).find("termina").text(), true ), "dd/mm/yyyy", true ) );
			ggantt.cuerpo += "<tr class='tr_actividad_ggantt cronograma_actividades_ggantt' nu_ciclo='" + $(this).attr("nu_ciclo") +
								"' id_cronograma='" + $(this).find("id_cronograma").text() + "' id_fase='" + $(this).attr("id_fase") +
								"' id_cronograma_actividad='" + $(this).attr("id_cronograma_actividad") + "'>" +
								"<td nowrap='nowrap' class='td_actividad_ggantt' title='" + $(this).find("descripcion").text() + 
									"' style='border: 1px solid #D3D3D3; padding: 2px;'>" +
									"<div style='margin-left: 30px;'>" + $(this).find("descripcion").text() + "</div>" +
								"</td>" +
								"<td class='td_actividad_fec_inicio_ggantt' style='text-align: center; border: 1px solid #D3D3D3; padding: 2px;' " +
									"nowrap='nowrap' >" + 
									fec_inicio_act + 
								"</td>" +
								"<td class='td_actividad_fec_fin_ggantt' style='text-align: center; border: 1px solid #D3D3D3; padding: 2px;' " +
									"nowrap='nowrap'>" +
									fec_fin_act + 
								"</td>" +
								"<td class='td_actividad_horas_ggantt' style='text-align: center; border: 1px solid #D3D3D3; padding: 2px;'>" +
									$(this).find("horas").text() + 
								"</td>" +
								"<td class='td_recurso_ggantt' style='border: 1px solid #D3D3D3; padding: 2px; text-align: center;'>" +
									". . ."	
								"</td>";
			
			//Obtenemos los recursos asignados a esta actividad.
			
			ggantt.fec_sig = new Date( ggantt.fec_inicio_gantt.getFullYear(), ggantt.fec_inicio_gantt.getMonth(), 1 );
			if( ggantt.fec_inicio_act == null || ggantt.fec_fin_act == null )
			{
				ggantt.cuerpo += "<td colspan='" + ggantt.colspan_meses_gantt + "' style='padding: 4px;'><div style='visibility: hidden; height: 2px;'></div></td>";
				ggantt.cuerpo += "</tr></td>";
			}
			else
			{
				mes_actual_gantt = ggantt.fec_sig.getMonth();
				var dia_sig_gantt;
				for( x=0; x<ggantt.meses_gantt; x++ )
				{
					dia_sig_gantt = 1;
					ggantt.cuerpo += "<td style='width: 100%;'><table style='width: 100%; border-collapse: collapse;'><tr>";

					while( mes_actual_gantt == ggantt.fec_sig.getMonth() )
					{
						//Marcamos el rango de d�as que abarca la petici�n.
						if( ggantt.fec_sig >= ggantt.fec_inicio_act && ggantt.fec_sig <= ggantt.fec_fin_act )
						{		
							if( fnc_fecha_string( ggantt.fec_sig, "dd/mm/yyyy", true ) == fnc_fecha_string( ggantt.fec_inicio_act, "dd/mm/yyyy", true ) )
								clase_actividad_ggantt = "class='td_dia_actividad_gantt border_linea_tiempo_gantt_izq'";
							else
							{
								if( fnc_fecha_string( ggantt.fec_sig, "dd/mm/yyyy", true ) == fnc_fecha_string( ggantt.fec_fin_act, "dd/mm/yyyy", true ) )
								{
									clase_actividad_ggantt = "class='td_dia_actividad_gantt border_linea_tiempo_gantt_der'";
								}
								else
									clase_actividad_ggantt = "class='td_dia_actividad_gantt'";
							}
							
							if( fnc_fecha_string( ggantt.fec_inicio_act, "dd/mm/yyyy", true ) == fnc_fecha_string( ggantt.fec_fin_act, "dd/mm/yyyy", true ) )
								clase_actividad_ggantt = "class='td_dia_actividad_gantt border_linea_tiempo_gantt_ambos'";
							
							ggantt.cuerpo += "<td " + clase_actividad_ggantt + " title='" + fnc_poner_ceros_izq(ggantt.fec_sig.getDate(), 2) + "/" + ggantt.array_meses[ ggantt.fec_sig.getMonth() ] + "/" + ggantt.fec_sig.getFullYear() + "'><div style='visibility: hidden; height: 2px;'>" + ggantt.fec_sig.getDate() + "</div></td>";
						}
						else
							ggantt.cuerpo += "<td style='padding: 4px;'><div style='visibility: hidden; height: 2px;'>" + ggantt.fec_sig.getDate() + "</div></td>";
						
						ggantt.fec_sig = new Date( ggantt.fec_sig.getFullYear(), ggantt.fec_sig.getMonth(), ++dia_sig_gantt );
					}
					
					ggantt.cuerpo += "</tr></table></td>";
					mes_actual_gantt = ggantt.fec_sig.getMonth();
				}
			}
			
			ggantt.cuerpo += "</tr>";
		});
	},
	re_crear: function(p_control){
		//Validamos si debemos actualizar el gantt.
		var tr_actividad_ggantt = $(p_control).parent().parent();
		var tr_id_ggantt = $(tr_actividad_ggantt).attr("id");
		if( $(tr_actividad_ggantt).find(".td_actividad_fec_inicio_ggantt input").val() != ""
			&& $(tr_actividad_ggantt).find(".td_actividad_fec_fin_ggantt input").val() != "" )
		{
			if( fnc_fecha_objeto( $(tr_actividad_ggantt).find(".td_actividad_fec_inicio_ggantt input").val(), true )
				<= fnc_fecha_objeto( $(tr_actividad_ggantt).find(".td_actividad_fec_fin_ggantt input").val(), true ) )
			{
				//Armamos un array con todas las actividades.
				var actividades_gganttArray = new Array();
				var fec_min = null;
				var fec_max = null;
				var total_horas = 0;
				var total_dias = 0;
		
				$(".tr_actividad_ggantt").each(function(){
					actividades_gganttArray.push({
						id_empresa: $(this).attr("id_empresa"),
						id_cliente: $(this).attr("id_cliente"),
						id_proyecto: $(this).attr("id_proyecto"),
						id_peticion: $(this).attr("id_peticion"),
						descripcion: $(this).find("td:eq(0) span:eq(1)").text(),
						fec_inicio: ( $(this).find(".td_actividad_fec_inicio_ggantt input").val() == "" ? null : fnc_fecha_objeto( $(this).find(".td_actividad_fec_inicio_ggantt input").val(), true ) ),
						fec_fin: ( $(this).find(".td_actividad_fec_fin_ggantt input").val() == "" ? null : fnc_fecha_objeto( $(this).find(".td_actividad_fec_fin_ggantt input").val(), true ) ),
						horas: $(this).find(".td_actividad_horas_ggantt input").val(),
						id_recurso: $(this).find(".select_recurso_ggantt option:selected").val(),
						nb_nombre_completo: $(this).find(".select_recurso_ggantt option:selected").text()
					});
					
					//Guardamos fecha m�nima y la fecha m�xima.
					if( $(this).find("td:eq(1) input").val() != "" 
						&& ( fec_min == null || fnc_fecha_objeto( $(this).find("td:eq(1) input").val(), true ) < fec_min ) )
						fec_min = fnc_fecha_objeto( $(this).find(".td_actividad_fec_inicio_ggantt input").val(), true );
					
					if( $(this).find("td:eq(2) input").val() != "" 
						&& ( fec_max == null || fnc_fecha_objeto( $(this).find("td:eq(2) input").val(), true ) > fec_max ) )
						fec_max = fnc_fecha_objeto( $(this).find(".td_actividad_fec_fin_ggantt input").val(), true );
					
					//Sumamos las horas.
					total_horas += $(this).find(".td_actividad_horas_ggantt input").val() * 1;

					//Calculamos los d�as de diferencia que hay entre las dos fechas.
					if( $(this).find(".td_actividad_fec_inicio_ggantt input").val() != "" && $(this).find(".td_actividad_fec_fin_ggantt input").val() != "" )
					{
						var dias = fnc_fecha_objeto( $(this).find(".td_actividad_fec_fin_ggantt input").val(), true ).getTime() - fnc_fecha_objeto( $(this).find(".td_actividad_fec_inicio_ggantt input").val(), true ).getTime();
						dias = dias / 1000;
						dias = dias / 60;
						dias = dias / 60;
						dias = Math.ceil( dias / 24 ) + 1;
						
						//Sumamos los d�as.
						total_dias += dias;
					}
				});
				
				//Ordenamos el array.
				actividades_gganttArray.sort( function(a, b) { return a.fec_inicio - b.fec_inicio } );

				//Armamos un XML con todas las actividades.
				var actividades_gganttXML = ""
				$.each(actividades_gganttArray, function(index, value){
					actividades_gganttXML += "<peticion>" +
												 "<id_empresa>" + value.id_empresa + "</id_empresa>" +
												 "<id_cliente>" + value.id_cliente + "</id_cliente>" +
												 "<id_proyecto>" + value.id_proyecto + "</id_proyecto>" +
												 "<id_peticion>" + value.id_peticion + "</id_peticion>" +
												 "<descripcion>" + value.descripcion + "</descripcion>" +
												 "<inicia>" + ( value.fec_inicio == null ? "" : fnc_fecha_string( value.fec_inicio, "dd/mm/yyyy", true ) ) + "</inicia>" +
												 "<termina>" + ( value.fec_fin == null ? "" : fnc_fecha_string( value.fec_fin, "dd/mm/yyyy", true ) ) + "</termina>" +
												 "<horas>" + value.horas + "</horas>" +
												 "<id_recurso>" + value.id_recurso + "</id_recurso>" +
												 "<nb_nombre_completo>" + value.nb_nombre_completo + "</nb_nombre_completo>" +
											 "</peticion>";
				});
				
				actividades_gganttXML += "<fechas fecha_inicio='" + fnc_fecha_string( fec_min, "dd/mm/yyyy", true ) 
					+ "' fecha_fin='" + fnc_fecha_string( fec_max, "dd/mm/yyyy", true ) + "' total_horas='" + total_horas 
					+ "' dias_duracion='" + total_dias + "'></fechas>";
		
				ggantt.crear({
					id: "div_gantt_prospeccion_contenido",
					peticionesXML: $.parseXML( "<root>" + actividades_gganttXML + "</root>" )
				});
				
				//Seleccionamos el row que estaba seleccionado.
				$( "#" + tr_id_ggantt ).addClass("ui-state-active");
			}
		}
	},
	calcular_horas: function(){
		var total_horas = 0;
		$(".tr_actividad_ggantt").each(function(){
			//Sumamos las horas.
			total_horas += $(this).find(".td_actividad_horas_ggantt input").val() * 1;
		});
		
		$("#span_duracion_horas_proyecto").text( total_horas );
	},
	guardar_cambios_ggantt: function(){
		//Obtenemos todas las actividades.
		var actividades_gganttXML = ""
		
		$(".tr_actividad_ggantt").removeClass("ui-state-active");
		$(".tr_actividad_ggantt").each(function(){
			//Si existen actividades con fechas no correctas, entonces no se permite el guardado.
			if( ( $(this).find(".td_actividad_fec_inicio_ggantt input").val() != ""
					&& $(this).find(".td_actividad_fec_fin_ggantt input").val() != "" )
				|| ( $(this).find(".td_actividad_fec_inicio_ggantt input").val() == ""
					&& $(this).find(".td_actividad_fec_fin_ggantt input").val() == "" ) )
			{
				if( ( $(this).find(".td_actividad_fec_inicio_ggantt input").val() == ""
						&& $(this).find(".td_actividad_fec_fin_ggantt input").val() == "" )
					|| ( fnc_fecha_objeto( $(this).find(".td_actividad_fec_inicio_ggantt input").val(), true )
						<= fnc_fecha_objeto( $(this).find(".td_actividad_fec_fin_ggantt input").val(), true ) ) )
				{
					actividades_gganttXML += "<peticion>" +
												 "<id_empresa>" + $(this).attr("id_empresa") + "</id_empresa>" +
												 "<id_cliente>" + $(this).attr("id_cliente") + "</id_cliente>" +
												 "<id_proyecto>" + $(this).attr("id_proyecto") + "</id_proyecto>" +
												 "<id_peticion>" + $(this).attr("id_peticion") + "</id_peticion>" +
												 "<de_peticion>" + $(this).find(".td_actividad_ggantt span:eq(1)").text() + "</de_peticion>" +
												 "<inicia>" + ( $(this).find(".td_actividad_fec_inicio_ggantt input").val() == "" ? "" : fnc_fecha_string( fnc_fecha_objeto( $(this).find(".td_actividad_fec_inicio_ggantt input").val(), true ), "yyyy-mm-dd", true ) ) + "</inicia>" +
												 "<termina>" + ( $(this).find(".td_actividad_fec_fin_ggantt input").val() == "" ? "" : fnc_fecha_string( fnc_fecha_objeto( $(this).find(".td_actividad_fec_fin_ggantt input").val(), true ), "yyyy-mm-dd", true ) ) + "</termina>" +
												 "<horas>" + $(this).find(".td_actividad_horas_ggantt input").val() + "</horas>" +
												 "<id_recurso>" + $(this).find(".td_recurso_ggantt select option:selected").val() + "</id_recurso>" +
											 "</peticion>";
				}
				else
					$(this).addClass("ui-state-active");
			}
			else
				$(this).addClass("ui-state-active");
			
		});
		
		if( $(".tr_actividad_ggantt[class*='ui-state-active']").size() > 0 )
			fnc_mostrar_aviso( "gd_avisos", "Existen acitividades con fechas incorrectas. Favor de corregirlas para continuar.", 5 );
		else
		{
			fnc_peticionAjax({
				url: "./application/adm_proy.cfc",
				parametros: "method=Peticiones_EditarXML&peticionesXML=" + "<root>" + actividades_gganttXML + "</root>",
				data_type_json: true,
				f_callback: function(){
					//Actualizamos el grid.
					$("#grid_grid_proyectos_peticiones").trigger("reloadGrid");
				}
			});
		}
		
	}
}