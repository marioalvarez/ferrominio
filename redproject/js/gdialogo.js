//Funci�nes para crear y manipular los gdi�logos.
//Elaborado por: Gil Verduzco.
var gdialogo = {
	crear: function(p_id){
		//Si ya existe lo eliminamos.
		$('#gd_' + p_id + ' .gdbotones .gdboton').appendTo( $('#' + p_id) );
		$('#gd_' + p_id + ' .gdcontenido').children().appendTo( $('#' + p_id) );
		//$('#' + p_id + ' .gdcontenidodinamico').empty();
		$('#gd_' + p_id + ', #gdf_' + p_id).remove();
		
		var p_id = $("#" + p_id).attr("id");
		var p_width = $("#" + p_id).attr("ancho");
		p_width = ( p_width ? p_width : "300px" );
		var p_titulo = $("#" + p_id).attr("titulo");
		var posicion = $("#" + p_id).attr("posicion");
		var fnc_al_cerrar = $("#" + p_id).attr("alCerrar");
		
		//Validamos el ancho.
		var width = fnc_calcular_ancho_alto( p_width, true );
		
		var height;
		var max_height = $(window).height();
		var margin_top;
		var margin_left;
		var top = ( posicion == "arriba" ? "0" : "50%" );
		
		//Calculamos el margin-left para poder centrar el gdi�logo.
		margin_left = "-" + ( width / 2 + 25  ) + "px";
		
		//Creamos el gdi�logo.
		$("#div_contenido").append(
							'<div id="gd_'+ p_id + '" class="gdialog" style="width: ' + ( width + "px") + '; top: ' + top + '; ' +
								'margin-left: ' + margin_left + ';" autocerrar="true">' +
								'<div class="gdtitulo">' +
									'<span id="span_cerrar_dialogo_' + p_id + '" title="Cerrar" class="gdcerrar">X</span>' +
									'<span>' + ( (p_titulo) ? p_titulo : "Cuadro de di&aacute;logo" ) + '</span>' +
								'</div>' +
								'<div class="gdcontenido" style="max-height: ' + ( max_height - 100 ) + 'px;"></div>' +
								'<div class="gdbotones">' +
									'<button style="float: right;" title="Cerrar" id="btn_cerrar_dialogo_' + p_id + '">' +
										'<span>Cerrar</span>' +
									'</button>' +
								'</div>' +
							'</div>'
						);
		
		//Agregamos los botones.
		$("#" + p_id).children().each(function(){
			if( $(this).hasClass("gdboton") )
			{
				$(this).css({
					float: "right",
					marginRight: "3px"
				}).appendTo("#gd_" + p_id + " .gdbotones");	
			}
		});
	
		//Agregamos el contenido.
		$("#" + p_id).children().appendTo( $("#gd_" + p_id + " .gdcontenido") );
		
		//Agregamos un div de fondo.
		$("body").append(
							'<div id="gdf_' + p_id + '" style="position: fixed; top: 0px; bottom: 0px; left: 0px; right: 0px; background-color: #000000; ' + 
								'width: 100%; height: 100%; z-index: 999; filter: alpha(opacity = 20); -khtml-opacity: 0.2; ' +
								'-moz-opacity: 0.2; opacity: 0.2; display: none;" class="gdfondo">' +
							'</div>'
						);

		//Validar que lo alto del gdi�logo no sea m�s alto que la ventana.
		height = $("#gd_" + p_id).outerHeight();
		if( height > max_height )
			height = max_height;
			
		//Calculamos el margin-top para poder centrar el gdi�logo.
		if( posicion == "arriba" )
			margin_top = "0px";
		else
			margin_top = "-" + ( height / 2 ) + "px";

		$("#gd_" + p_id).css("margin-top", margin_top);
		
		//Le aplicamos los estilos a los botones del gdi�logo.
		$("#gd_" + p_id + " .gdbotones button").addClass("boton");
		
		//Agregamos el "onClick" a los botones de cerrar.
		var onClick = '$(\'#gd_' + p_id + '\').animate( { top: \'' + "-" + ( top == "0" ? height + 100 : top ) + '\'}, 300 ); setTimeout( function(){ $(\'#gdf_' + p_id + '\').animate( { marginLeft: \'-100%\' }, function(){ $(\'#gd_' + p_id + ' .gdbotones .gdboton\').appendTo( $(\'#' + p_id + '\') ); $(\'#gd_' + p_id + ' .gdcontenido\').children().appendTo( $(\'#' + p_id + '\') ); $(\'#' + p_id + ' .gdcontenidodinamico\').empty(); $(\'#gd_' + p_id + ', #gdf_' + p_id + '\').remove(); } ) }, 100 ); ' + ( (fnc_al_cerrar) ? fnc_al_cerrar : "");
		$("#gd_" + p_id + " .gdtitulo span:first, #gd_" + p_id + " .gdbotones button:first").attr("onClick", onClick);
		
		//Hacemos que el gdi�logo sea draggable.
		$("#gd_" + p_id).draggable({ handle: ".gdtitulo"/*, containment: "body"*/ });
	},
	abrir: function(p_id){
		//Creamoa el gdi�logo.
		gdialogo.crear(p_id);
		
		//Calculamos el z-index.
		var z_index_gd = 1000;		

		$(".gdialog:visible").each(function(){
			if( ( $(this).css("z-index") * 1 ) >= z_index_gd )
				z_index_gd = $(this).css("z-index") * 1 + 1
		});
		
		$(".ui-dialog:visible").each(function(){
			if( ( $(this).css("z-index") * 1 ) >= z_index_gd )
				z_index_gd = $(this).css("z-index") * 1 + 1
		});
		
		//Establecemos el z-index.
		$("#gd_" + p_id).css("z-index", z_index_gd);
		
		if( $("#" + p_id).attr("tipo_dialogo") == "1" || $("#" + p_id).attr("tipo_dialogo") == "2" || $("#" + p_id).attr("tipo_dialogo") == "5" )
		{
			
			if( $("#" + p_id).attr("tipo_dialogo") != "5" )
				$("#gdf_" + p_id).css("z-index", z_index_gd - 1);
			else //Eliminaos el bot�n de Cerrar.
				$(" #gd_" + p_id + " .gdbotones button:first").remove();
			
			
			$("#gd_" + p_id).css({
				left: "50%"
			});
			
			//Agregamos el efecto de arriba-abajo.
			$("#gd_" + p_id).css("top", "0%");
			
			if( $("#" + p_id).attr("tipo_dialogo") != "5" )
				$("#gdf_" + p_id).css("margin-left", "-100%");
			
			//Mostramos.
			$("#gd_" + p_id).fadeIn(1);
			
			if( $("#" + p_id).attr("tipo_dialogo") != "5" )
			{
				$("#gdf_" + p_id).fadeIn(1);
			
				$("#gdf_" + p_id).animate({marginLeft: "0"}, 300);
			}
			
			$("#gd_" + p_id).animate({top: "50%"}, 300);
		}
		else
		{
			//Eliminaos el bot�n de Cerrar.
			$(" #gd_" + p_id + " .gdbotones button:first").remove();
			
			//Agregamos el efecto de arriba-abajo.
			$("#gd_" + p_id).css({
				"top": "0px",
				"right": "0px",
				"marginLeft": "0px",
				"marginTop": "0px"
			});
			
			//Mostramos.
			$("#gd_" + p_id).fadeIn(1).animate({
				top: 90
			}, 500);
		}
		
		//Poner el focus en el bot�n de Cerrar.
		//$( "#btn_cerrar_dialogo_" + p_id ).focus();
	},
	cerrar: function(p_id){		
		//Cerramos.
		$( "#span_cerrar_dialogo_" + p_id ).trigger("click");
	},
	re_ajustar: function(p_id, p_obj){
		if( gdialogo.esta_visible(p_id) )
		{
			$( "#gd_" + p_id + " .gdcontenido" ).css({
				height: 100
			});
			
			var width = ( p_obj && p_obj.ancho ? fnc_calcular_ancho_alto( p_obj.ancho, true ) : $( "#gd_" + p_id ).outerWidth() );
			var height = ( p_obj && p_obj.height ? fnc_calcular_ancho_alto( p_obj.height, false ) : $( "#gd_" + p_id + " .gdcontenido" )[0].scrollHeight );
	
			//Validar que lo alto del gdi�logo no sea m�s alto que la ventana.
			if( height > $(window).height() )
				height = $(window).height();
			
			//Validar que lo ancho del gdi�logo no sea m�s ancho que la ventana.
			if( width > $(window).width() )
				width = $(window).width();
			
			$( "#gd_" + p_id ).css({
				width: width + "px",
				marginLeft: "-" + (width / 2) + "px",
			});
			
			$( "#gd_" + p_id + " .gdcontenido" ).css({
				height: height + 10
			});
			
			height = $( "#gd_" + p_id ).outerHeight();
			$( "#gd_" + p_id ).css({
				marginTop: "-" + (height / 2) + "px"
			});
		}
	},
	esta_visible: function(p_id){
		return $("#gd_" + p_id).is(":visible");
	}
}