//Funci�n para hacer peticiones por Ajax.
//Elaborado por: Gil Verduzco.
function fnc_peticionAjax( p_obj_opciones )
{		
	try
	{
		//Validar si tenemos que mostramos la espera.
		if( !( p_obj_opciones.mostrar_espera == false ) )
		{
			//Validamos si debemos bloquear la pantalla.
			var bloquear_pantalla = false;
			var url = "";
			
			if( p_obj_opciones.url && p_obj_opciones.parametros )
			{
				url = p_obj_opciones.url.toUpperCase();
				
				if( typeof(p_obj_opciones.parametros) == "object" )
					url += ( p_obj_opciones.parametros.method ? p_obj_opciones.parametros.method.toUpperCase() : "" );
				else
					url += p_obj_opciones.parametros.toUpperCase();
			}
			
			
			var array_url = url.split("GUARDAR");
			if( array_url.length > 1 )
				bloquear_pantalla = true;
			
			array_url = url.split("INSERTAR");
			if( array_url.length > 1 )
				bloquear_pantalla = true;
				
			var array_url = url.split("ASIGNAR");
			if( array_url.length > 1 )
				bloquear_pantalla = true;
			
			array_url = url.split("EDITAR");
			if( array_url.length > 1 )
				bloquear_pantalla = true;
			
			array_url = url.split("ACTUALIZAR");
			if( array_url.length > 1 )
				bloquear_pantalla = true;
			
			var array_url = url.split("AUTORIZAR");
			if( array_url.length > 1 )
				bloquear_pantalla = true;
			
			array_url = url.split("ELIMINAR");
			if( array_url.length > 1 )
				bloquear_pantalla = true;
			
			array_url = url.split("BORRAR");
			if( array_url.length > 1 )
				bloquear_pantalla = true;
			
			array_url = url.split("AGREGAR");
			if( array_url.length > 1 )
				bloquear_pantalla = true;
				
			array_url = url.split("QUITAR");
			if( array_url.length > 1 )
				bloquear_pantalla = true;
				
			array_url = url.split("CREAR");
			if( array_url.length > 1 )
				bloquear_pantalla = true;
			
			array_url = url.split("CRUD");
			if( array_url.length > 1 )
				bloquear_pantalla = true;

			//Mostramos la espera.
			if( !( p_obj_opciones.bloquear_pantalla == false ) )
				fnc_mostrar_espera( bloquear_pantalla );
			else
				fnc_mostrar_espera( p_obj_opciones.bloquear_pantalla );
		}
		
		$.ajax({
			url: p_obj_opciones.url,
			async: true, //p_obj_opciones.async,
			type: ( ( p_obj_opciones.get_post ) ? p_obj_opciones.get_post.toUpperCase() : "POST" ),
			data: ( ( p_obj_opciones.parametros ) ? p_obj_opciones.parametros : "" ),
			cache: ( p_obj_opciones.cache ? p_obj_opciones.cache : false ),
			encoding: "UTF-8",
			success: function(data){
				try
				{
					//Ocultamos la espera.
					fnc_ocultar_espera();
					
					//Guardamos la respuesta en un div.
					var respuesta_json;

					//Validamos si tenemos que validar el resultado.
					try
					{ 
						respuesta_json = $.parseJSON(data);
					}
					catch(e)
					{
						respuesta_json = null;
					}

					//Validamos la respuesta por JSON.
					if( ( respuesta_json != null && respuesta_json.ERRORTYPE && respuesta_json.ERRORTYPE != "" ) )
					{
						//Validar si se acab� la sesi�n.
						if( ( respuesta_json != null && respuesta_json.ERRORTYPE && respuesta_json.ERRORTYPE == "Login" ) )
							window.location = "./login.cfm";
						else
						{
							//Validamos el tipo de error.
							if( respuesta_json.ERRORTYPE == "Operacional" )
								fnc_mostrar_aviso( "gd_avisos", respuesta_json.MESSAGE, 5 );
							else
								fnc_mostrar_error( respuesta_json.MESSAGE, "<p>" + respuesta_json.ERRORMESSAGE + "</p><p>" + respuesta_json.ERRORDETAIL + "</p>" );
						}
					}
					else
					{	
						if( p_obj_opciones.div_success )
						{
							$( "#" + p_obj_opciones.div_success ).html( data );
							
							//Inicializamos los controles.
							fnc_inicializar_controles( "#" + p_obj_opciones.div_success );
							
							$( "#" + p_obj_opciones.div_success + " script" ).remove();
						}

						//Validar si debemos de mostrar un mensaje.
						if( respuesta_json != null && !( p_obj_opciones.automensaje == false ) )
							fnc_mostrar_aviso( "gd_avisos", respuesta_json.MESSAGE, 3 );

						if( p_obj_opciones.f_callback )
							p_obj_opciones.f_callback(respuesta_json);
					}
				}
				catch(e)
				{
					//Ocultamos la espera.
					fnc_ocultar_espera();
					
					fnc_mostrar_error( "Error de Ejecuci&oacute;n de C&oacute;digo.", e.toString() );
				}
			},
			error: function(request, status, error){
				//Ocultamos la espera.
				fnc_ocultar_espera();

				var div_responseText = $("<div>").html( request.responseText );
				
				fnc_mostrar_error( "Error Interno del Servidor.", $(div_responseText).find("#textSection1").text() );
			}
		});
	}
	catch(e)
	{
		fnc_mostrar_error( "Error de Ejecuci&oacute;n de C&oacute;digo.", e.toString() );
	}
}

function fnc_peticion_ajax(p_peticion, p_success, p_auto_mensaje)
{
	try
	{
		//Validamos si debemos bloquear la pantalla.
		var bloquear_pantalla = false;
		var url = p_peticion.toString().toUpperCase();
		
		var array_url = url.split("GUARDAR");
		if( array_url.length > 1 )
			bloquear_pantalla = true;
		
		array_url = url.split("INSERTAR");
		if( array_url.length > 1 )
			bloquear_pantalla = true;
		
		array_url = url.split("EDITAR");
		if( array_url.length > 1 )
			bloquear_pantalla = true;
		
		array_url = url.split("ACTUALIZAR");
		if( array_url.length > 1 )
			bloquear_pantalla = true;
		
		array_url = url.split("ELIMINAR");
		if( array_url.length > 1 )
			bloquear_pantalla = true;
		
		array_url = url.split("BORRAR");
		if( array_url.length > 1 )
			bloquear_pantalla = true;
		
		array_url = url.split("AGREGAR");
		if( array_url.length > 1 )
			bloquear_pantalla = true;
		
		array_url = url.split("CREAR");
		if( array_url.length > 1 )
			bloquear_pantalla = true;
		
		array_url = url.split("CRUD");
		if( array_url.length > 1 )
			bloquear_pantalla = true;
			
		//Mostramos la espera.
		fnc_mostrar_espera( bloquear_pantalla );
		
		//Ejecutamos la petici�n.
		var respuesta = p_peticion();

		if( respuesta )
		{
			if( respuesta.ERRORTYPE != "" )
			{
				//Ocultamos la espera.
				fnc_ocultar_espera();
				
				//Validar si se acab� la sesi�n.
				if( respuesta.ERRORTYPE == "Login" )
					window.location = "./login.cfm";
				else
				{
					//Validamos el tipo de error.
					if( respuesta.ERRORTYPE == "Operacional" )
						fnc_mostrar_aviso( "gd_avisos", respuesta.MESSAGE, 5 );
					else
						fnc_mostrar_error( respuesta.MESSAGE, "<p>" + respuesta.ERRORMESSAGE + "</p><p>" + respuesta.ERRORDETAIL + "</p>" );
				}
			}
			else
			{
				//Ocultamos la espera.
				fnc_ocultar_espera();
					
				if( !p_auto_mensaje )
					fnc_mostrar_aviso( "gd_avisos", respuesta.MESSAGE, 3 );
				
				if( p_success() )
					p_success();
			}
		}
		else
		{
			fnc_mostrar_aviso( "gd_avisos", "No se ejecut&oacute; la petici&oacute;n al servidor. Revise su c&oacute;digo fuente, por favor.", 5 )
			fnc_ocultar_espera();
		}
	}
	catch(e)
	{ 
		//Ocultamos la espera.
		fnc_ocultar_espera();
			
		fnc_mostrar_error( "Error de Ejecuci&oacute;n de C&oacute;digo.", e.toString() );
	}
}

//Funci�n para mostrar errores al usuario.
//Elaborado por: Gil Verduzco.
function fnc_mostrar_error(p_mensaje_principal, p_mensaje_detalle)
{
	$("#div_mensaje_error_principal").html( p_mensaje_principal );
	$("#div_mensaje_error_detalle").html( p_mensaje_detalle );
	
	gdialogo.abrir("gd_errores");
	
	setTimeout(function(){
		if( $("#gd_gd_errores").attr("autocerrar") == "true" )
			gdialogo.cerrar("gd_errores");
	}, 10000);
}

//Funci�n para mostrar avisos al usuario.
//Elaborado por: Gil Verduzco.
function fnc_mostrar_aviso(p_nombre_dialogo, p_mensaje, p_tipo_mensaje)
{
	//Tipos de mensajes
	/*
		Tipo 1: Informativo.
		Tipo 2: Confirmaci�n.
		Tipo 3: �xito.
		Tipo 4: Error.
		Tipo 5: Adverntencia
	*/
	
	var nombre_imagen = "";
	switch( p_tipo_mensaje )
	{
		case 1: nombre_imagen = "informativo.png";
				break;
		case 2: nombre_imagen = "confirmacion.jpg";
				break;
		case 3: nombre_imagen = "exito.png";
				break;
		case 4: nombre_imagen = "error.jpg";
				break;
		case 5: nombre_imagen = "advertencia.jpg";
				break;
		default: break;
	}
	
	var estructura_mensaje = '<table style="width: 100%;">' +
							  	'<tr>' +
									'<td>' +
										'<img src="./images/' + nombre_imagen + '" width="50" height="50" />' +
									'</td>' +
									'<td>' +
										'<span>' + p_mensaje + '</span>' + 
									'</td>'
								'</tr>' +
						     '</table>';
	
	//Hacemos una copia del di�logo.
	var id;
	if( p_tipo_mensaje == 1 || p_tipo_mensaje == 2 )
	{
		id = p_nombre_dialogo;
		
		$( "#" + p_nombre_dialogo ).attr( "tipo_dialogo", p_tipo_mensaje );
		$( "#" + p_nombre_dialogo + " div:first" ).html( estructura_mensaje );
	}
	else
	{
		var $dialogo = $( "#" + p_nombre_dialogo ).clone();
		id = $( "#" + p_nombre_dialogo ).attr("id") + "_" + ( new Date() ).getTime();
		
		$dialogo.find("div:first").html( estructura_mensaje );
		
		$dialogo.attr({
			id: id,
			tipo_dialogo: p_tipo_mensaje
		});
		
		$dialogo.appendTo("#div_contenido");
	}
	
	gdialogo.abrir( id );
	
	if( p_tipo_mensaje != 1 && p_tipo_mensaje != 2 )
	{
		var tiempo = 4000;
		if( p_tipo_mensaje == 4 )
			tiempo = 10000;
		else if( p_tipo_mensaje == 5 )
			tiempo = 5000;
			
		setTimeout(function(){
			if( $("#gd_" + id).attr("autocerrar") == "true" )
				gdialogo.cerrar( id );
			
			if( p_tipo_mensaje != 1 && p_tipo_mensaje != 2 )
				$( "#" + id ).remove();
		}, tiempo);
	}
}

//Funci�n para bloquear la pantalla.
//Elaborado por: Gil Verduzco.
function fnc_mostrar_espera(p_bloquear_pantalla)
{	
	$(".blocker, .espera").remove();
		
	if( p_bloquear_pantalla )
		$("<div>").attr("class", "blocker").appendTo("body");
		
	$("<div>").attr("class", "espera")
		.html( '<div>' +
					'<table style="width: 100%;">' +
						'<tr>' +
							'<td>' +
								'<img src="./images/loadin_bar.gif" />' +
							'</td>' +
						'</tr>' +
					'</table>' +
        	   '</div>' 
			 ).appendTo("body");
}

//Funci�n para desbloquear la pantalla.
//Elaborado por: Gil Verduzco.
function fnc_ocultar_espera()
{
	$(".blocker, .espera").remove();
}

//Funci�n para acompletar con ceros a la izquierda.
//Elaborado por: Gil Verduzco.
function fnc_poner_ceros_izq( p_numero, p_cantidad_digitos )
{
	if( !isNaN( p_numero * 1 ) )
	{
		var numero = p_numero.toString();
		while( numero.length < p_cantidad_digitos )
		{
			numero = "0" + numero;
		}
	}
	else
		return null;
	
	return numero;
}

//Funci�n para reemplazar caracteres.
function replaceAll( txt, rep, with_this )
{
	return txt.replace( new RegExp( rep, 'g' ), with_this );
}

//Funci�n que recibe una fecha string y la convierte a un objeto fecha.
//Elaborado por: Gil Verduzco.
function fnc_fecha_objeto(p_fecha, p_restar_mes)
{
	if( p_fecha )
	{
		//Nos aseguramos de que la fecha venga separada por guiones.
		var fecha = replaceAll( p_fecha, "/", "-" );
		
		//Quitamos los espacios en blanco.
		fecha = $.trim( fecha );
		
		//Validamos el formato de la fecha.
		if( fecha.split("-")[0].length == 4 ) //Formato: yyyy-mm-dd
		{
			var anio = fecha.split("-")[0] * 1;
			var mes = (p_restar_mes) ? fecha.split("-")[1] * 1 - 1 : fecha.split("-")[1] * 1;
			var dia = fecha.split("-")[2].split(" ")[0] * 1;
		}
		else //Formato: dd-mm-yyyyy
		{
			var anio = fecha.split("-")[2].split(" ")[0] * 1;
			var mes = (p_restar_mes) ? fecha.split("-")[1] * 1 - 1 : fecha.split("-")[1] * 1;
			var dia = fecha.split("-")[0] * 1;
		}
		
		//Validar si la fecha trae horas, minutos, segundos.
		var hora = 0;
		var minutos = 0;
		var segundos = 0;
		if( fecha.split(" ")[1] )
		{
			hora = fecha.split(" ")[1].split(":")[0] * 1;
			
			if( fecha.split(" ")[1].split(":")[1] )
				minutos = fecha.split(" ")[1].split(":")[1] * 1;
				
			if( fecha.split(" ")[1].split(":")[2] )
				segundos = fecha.split(" ")[1].split(":")[2] * 1;
		}
		
		//Creamos un objeto fecha.
		return new Date( anio, mes, dia, hora, minutos, segundos );
	}
	
	return null;
}

//Funci�n que recibe un objeto fecha y la regresa en un string.
//Elaborado por: Gil Verduzco.
function fnc_fecha_string( p_obj_fecha, p_formato, p_sumar_mes )
{
	if( typeof( p_obj_fecha ) == "object" && p_formato )
	{	
		//Validar el formato de la fecha a regresar.
		var fecha_array = ( p_formato ? p_formato : "yyyy-mm-dd HH:mm:ss" ).split("-");
		var separador = "-";
		
		if( fecha_array.length == 1 )
		{
			fecha_array = p_formato.split("/");
			separador = "/";
		}
		
		if( fecha_array.length > 0 )
		{
			var anio = p_obj_fecha.getFullYear();
			var mes = fnc_poner_ceros_izq( ( (p_sumar_mes) ? ( p_obj_fecha.getMonth() + 1 ) : p_obj_fecha.getMonth() ), 2 );
			var dia = fnc_poner_ceros_izq( p_obj_fecha.getDate(), 2 );
			var hora = "";
			var minutos = "";
			var segundos = "";

			//Validar si el formato trae horas, minutos, segundos.
			if( p_formato.split(" ")[1] )
			{
				hora = " " + fnc_poner_ceros_izq( p_obj_fecha.getHours(), 2 );
				
				if( p_formato.split(" ")[1].split(":")[1] )
					minutos = ":" + fnc_poner_ceros_izq( p_obj_fecha.getMinutes(), 2 );
				
				if( p_formato.split(" ")[1].split(":")[2] )
					segundos = ":" + fnc_poner_ceros_izq( p_obj_fecha.getSeconds(), 2 );
			}
			
			//Regresamos la fecha formateada.
			if( fecha_array[0] == "yyyy" ) //Formato "yyyy[/, -]mm[/, -]dd [HH:mm:ss]".
				return ( anio + separador + mes + separador + dia + hora + minutos + segundos );
			else
				if( fecha_array[0] == "dd" ) //Formato "dd[/, -]mm[/, -]yyyy [HH:mm:ss]".
					return ( dia + separador + mes + separador + anio + hora + minutos + segundos );
		}
	}
	
	return null;
}

//Funci�n para respetar los caracteres especiales.
//Elaborado por: Gil Verduzco.
function fnc_codificar(p_string)
{
	var cadena = replaceAll( p_string, "&", "&amp;" );
	cadena = replaceAll( cadena, "<", "&lt;" );
	cadena = replaceAll( cadena, ">", "&gt;" );
	
	return encodeURIComponent( cadena );
}

//Funci�n para formatear n�meros.
function fnc_formatear_numeros(value, decimals, separators, simbolo_moneda)
{
    decimals = decimals >= 0 ? parseInt(decimals, 0) : 2;
    separators = separators || ['.', "'", '.'];
	
    var number = (parseFloat(value) || 0).toFixed(decimals);
    if (number.length <= (4 + decimals))
        return simbolo_moneda + ( number.replace('.', separators[separators.length - 1]) );
    
	var parts = number.split(/[-.]/);
    value = parts[parts.length > 1 ? parts.length - 2 : 0];
    var result = value.substr(value.length - 3, 3) + (parts.length > 1 ?
        separators[separators.length - 1] + parts[parts.length - 1] : '');
    
	var start = value.length - 6;
    var idx = 0;
    while (start > -3)
	{
        result = (start > 0 ? value.substr(start, 3) : value.substr(0, 3 + start))
            + separators[idx] + result;
        idx = (++idx) % 2;
        start -= 3;
    }
    return simbolo_moneda + ( (parts.length == 3 ? '-' : '') + result );
}

//Funci�n para convertir minutos, segundos o milisegundos a horas.
//Elaborado por: Gil Verduzco.
function fnc_convertir_a_horas(p_tiempo, p_tipo)
{
	if( p_tipo )
	{
		var horas;
		switch( p_tipo )
		{
			case "m":  horas = ( ( ( p_tiempo * 1 ) * 60 ) / 3600 ).toFixed(2); break;
			case "s":  horas = ( ( p_tiempo * 1 ) / 3600 ).toFixed(2); break;
			case "ms": horas = ( ( ( p_tiempo * 1 ) / 1000 ) / 3600 ).toFixed(2); break;	
			default: horas = null; break;		
		}
		
		return horas * 1;
	}
	
	return null;
}

//Funci�n para validar el formato de los correos.
//Elaborado por: Gil Verduzco.
function fnc_validar_formato_correo(p_correo)
{
	var expresion_regular_correos = /^[A-Za-z]\w*@{1}\w+(\.\w+)+$/;
	
	return expresion_regular_correos.test( p_correo );
}

//Funci�n para introducir s�lo numeros.
//Elaborado por: Gil Verduzco.
//NOTA: Este m�todo va en el "onKeyPress".
function fnc_solo_numeros(p_evt)
{
	var expresion_regular_numeros = /\d+/;
	
	var charCode = (p_evt.which) ? p_evt.which : p_evt.keyCode;
	
	var entrada = parseInt( String.fromCharCode(charCode) );

	return expresion_regular_numeros.test( entrada ) || charCode == 8 || charCode == 9 || charCode == 13 || charCode == 46 || charCode == 37 || charCode == 39;
}

//Funci�n para validar n�meros con decimales.
//Elaborado por: Gil Verduzco.
//NOTA: Este m�todo va en el "onKeyPress".
function fnc_solo_numeros_decimales(p_evt)
{
	var expresion_regular_numeros =  /^([0-9])*[.]?[0-9]*$/;
	
	var charCode = (p_evt.which) ? p_evt.which : p_evt.keyCode;
	
	var entrada = String.fromCharCode(charCode);
	
	return expresion_regular_numeros.test( entrada ) || charCode == 8 || charCode == 9 || charCode == 13 || charCode == 46 || charCode == 37 || charCode == 39;
}

//Funci�n para mostrar notificaciones.
//Elaborado por: Gil Verduzco.
function fnc_mostrar_notificacion(p_mensaje)
{
	$(".notificacion").remove();
	
	$("<div>").attr("class", "notificacion")
		.html( '<div>' +
			  		'<table>' +
						'<tr>' +
							'<td>' +
								 '<img src="./aps/images/adm_proy_imagenes/prueba.jpg" width="60" height="60" />' +
							 '</td>' +
							'<td>' + p_mensaje + '</td>' +
						'</tr>' +
					'</table>' +
			   '</div>'
			  ).appendTo("body");
	
	$(".notificacion").show();
	
	var notificacion = $(".notificacion");
	
	notificacion.show();
	
	var margin_left = notificacion.css("margin-left");
	var alto = notificacion.css("height").replace("px", "") * 1;

 	notificacion.css({ marginLeft: "-100%", marginTop: "-" + ( alto / 2 ) + "px" });
	notificacion.animate({ marginLeft: margin_left }, 500);
	
	notificacion.delay(1000);
	
	notificacion.animate({ marginLeft: "100%" }, 1000);
}

//Funci�n para crear los gfieldset.
//Elaborado por: Gil Verduzco.
function fnc_gfieldset(p_contenedor)
{
	$( ( p_contenedor ? p_contenedor + " " : "" ) + ".gfieldset" ).each(function(){
		$(this).html('' +
			'<table style="width: 100%;">' +
			'<tr>' +
				'<td>' +
					'<table style="width: 100%;">' +
						'<tr>' +
							'<td class="gfieldset_borde_izq">' +
								'<div style="height: 1px; width: 30px;"></div>' +
							'</td>' +
						'</tr>' +
					'</table>' +
				'</td>' +
				'<td nowrap="nowrap"><span style="font-weight: bold;">' + $(this).attr("titulo") + '</span></td>' +
				'<td style="width: 100%;">' +
					'<table style="width: 100%;">' +
						'<tr>' +
							'<td class="gfieldset_borde_der">' +
								'<div style="height: 1px;"></div>' +
							'</td>' +
						'</tr>' +
					'</table>' +
				'</td>' +
			'</tr>' +
		'</table>');
	});
}

//Funci�n para mostrar notificaciones.
//Elaborado por: Gil Verduzco.
function fnc_notificacion(p_mensaje)
{
	 $("<div>").appendTo(document.body).html("<div>" + p_mensaje + "</div>").addClass("notificacion")
		.show({
			effect: "bounce",
			duration: "fast"
		})
		.delay(1200)
		.hide({
			effect: "fade",
			duration: "slow"
		}, function(){
			$(this).remove();
		});
		
		//Calculamos el margin-top.
		$(".notificacion").css( "margin-top", "-" + $(".notificacion").height() / 2 + "px" );
}

//Funci�n para inicializar los controles.
//Elaborado por: Gil Verduzco.
function fnc_inicializar_controles(p_contenedor)
{
	//Creamos los campos num�ricos.
	$( ( p_contenedor ? p_contenedor + " " : "" ) + ".solo_numeros" ).attr("onKeyPress", "return fnc_solo_numeros(event)");
	$( ( p_contenedor ? p_contenedor + " " : "" ) + ".solo_numeros_decimales" ).attr("onKeyPress", "return fnc_solo_numeros_decimales(event)");
	
	//Creamos los datepicker.
	$( ( p_contenedor ? p_contenedor + " " : "" ) + ".txt_datepicker" ).datepicker({
		dateFormat: "dd/mm/yy",
		changeMonth: true,
		changeYear: true,
		firstDay: 1,
		showAnim: "drop"
	});
	
	$( ( p_contenedor ? p_contenedor + " " : "" ) + ".txt_datepicker" ).css({
		readonly: "readonly"
	});
	
	//Creamos los gfieldset.
	fnc_gfieldset(p_contenedor);
	
	//Creamos los grids.
	$( ( p_contenedor ? p_contenedor + " " : "" ) + ".ggrids" ).each(function(){
		//Se crean s�lo los grids que sean autocreables.
		if( $(this).attr("autocrear") != "false" )
			ggrids.crear( $(this).attr("id") );
	});
}

//Funci�n para hacer elementos dragabbles.
//Elaborado por: Gil Verduzco.
function fnc_elementos_draggables(p_obj_opciones)
{
	$(p_obj_opciones.elementos).draggable({
		helper: "clone",
		cursor: "move",
		revert: "invalid",
		cursorAt: { top: -1, left: -1 },
		start: function(){
			if( p_obj_opciones.fnc_start )
				p_obj_opciones.fnc_start();
		},
		stop: function(){
			if( p_obj_opciones.fnc_stop )
				p_obj_opciones.fnc_stop();
		}
	});
}

//Funci�n para crear la agenda.
//Elaborado por: Gil Verduzco.
function fnc_crear_agenda( p_opciones )
{
	$( "#" + p_opciones.id ).fullCalendar({
		header:              { left: "title", center: "today", right: p_opciones.vistas }
		,theme:               true
		,firstDay:            1
		,isRTL:               false
		,weekends:            true
		,weekMode:            "fixed"
		,displayweeksnum:	  true
		,height:              "90%"
		,contentHeight:       "400"
		,aspectRatio:         "1.35"
		,defaultView:         "month"
		,allDaySlot:          true
		,allDayText:          "Todo el d&iacute;a"
		,axisFormat:          "h(:mm)tt"
		,slotMinutes:         "60"
		,defaultEventMinutes: "120"
		,firstHour:           "7"
		,minTime:             "7"
		,maxTime:             "22"
		,timeFormat:          "H:mm{ - H:mm}"
		,columnFormat:        { month: "ddd", week: "ddd M/d", day: "dddd M/d" }
		,titleFormat:         { month: "MMMM yyyy", week: "MMM d[ yyyy]", day: "dddd, MMM d, yyyy" }
		,buttonText:          {
				today:    "D&iacute;a de hoy"
				,month:    "Mes"
				,week:     "Semana"
				,day:      "D&iacute;a"
		  }
		,monthNames:          ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
		,monthNamesShort:     ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
		,dayNames:            ['Domingo', 'Lunes', 'Martes', 'Mi&eacute;rcoles', 'Jueves', 'Viernes', 'S&aacute;bado']
		,dayNamesShort:       ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab']
		,selectable:          false
		,selectHelper:        true
		,unselectAuto:        true
		,unselectCancel:      ""
		,events:              p_opciones.url
		,eventSources:        ""
		,allDayDefault:       true
		,ignoreTimezone:      true
		,startParam:          "start"
		,endParam:            "end"
		,lazyFetching:        true
		,editable:            p_opciones.editable
		,disableDragging:     p_opciones.disableDragging
		,disableResizing:     true
		,dragRevertDuration:  "500"
		,dragOpacity:         { agenda: ".5", "": "1.0" }
		,year:                p_opciones.year_actual
		,month:               p_opciones.month_actual
		,date:                p_opciones.date_actual
		,droppable:           true
		,dropAccept:          "*"
		,viewDisplay:         function(view) { void(0); }
		,windowResize:        function(view) { void(0); }
		,dayClick:            function(date, allDay, jsEvent, view) { void(0); }
		,eventClick:          function(calEvent, jsEvent, view) { ( p_opciones.eventClick ? p_opciones.eventClick(calEvent, jsEvent, view) : void(0) ) }
		,eventMouseover:      function(event, jsEvent, view) { ( p_opciones.eventMouseOver ? p_opciones.eventMouseOver(event, jsEvent, view) : void(0) ) }
		,eventMouseout:       function(event, jsEvent, view) { ( p_opciones.eventMouseOut ? p_opciones.eventMouseOut(event, jsEvent, view) : void(0) ) }
		,select:              function(startDate, endDate, allDay, jsEvent, view) { ( p_opciones.eventSelect ? p_opciones.eventSelect(startDate, endDate, allDay, jsEvent, view) : void(0) ) }
		,unselect:            function(view, jsEvent ) { void(0); }
		,loading:             function(isLoading, view) { if( isLoading ){ p_opciones.eventIsLoading(); }else{ p_opciones.eventIsNotLoading(); } }
		,eventDragStart:      function(event, jsEvent, ui, view) { void(0); }
		,eventDragStop:       function(event, jsEvent, ui, view) { void(0); }
		,eventDrop:           function(event, dayDelta, minuteDelta, allDay, revertFunc, jsEvent, ui, view) { void(0); }
		,eventResizeStart:    function(event, jsEvent, ui, view) { void(0); }
		,eventResizeStop:     function(event, jsEvent, ui, view) { void(0); }
		,eventResize:         function(event, dayDelta, minuteDelta, revertFunc, jsEvent, ui, view) { void(0); }
		,drop:                function(date, allDay, jsEvent, ui) { void(0); }
	});
}

//Funci�n para ejecutar una funci�n al presionar la tecla Enter.
function fnc_enter_presionado( p_evt, p_fnc )
{
	if( ( p_evt.which ? p_evt.which : p_evt.keyCode ) === 13 )
	{
		if( p_fnc )
			//Ejecuta el string como funcion
			eval( p_fnc );
	}
}

//Funci�n para bajar el cursor de acuerdo al elemento seleccionado.
function fnc_deslizar_scroll(p_elemento)
{
	/*$("#gil").css({
		height: "100px",
		border: "1px solid black"
	});*/
	/*setTimeout(function(){*/
		/*var alto_ventana = $(window).height();
		var alto_elemento = $(p_elemento).offset().top - 140;
		
		if( alto_elemento > alto_ventana )
			alto_elemento = alto_ventana - 140;
		
		//alert( alto_elemento - $(window).height() )
		
		alto_elemento = $(window).height() - $("#div_contenido").height();
		
		if( alto_elemento < 0 )
			alto_elemento = alto_elemento * -1;
			alert( alto_elemento );
		$("body, html").animate({
			scrollTop: alto_elemento - 10
		}, 350);
	//}, 100);*/
}

//Funci�n para calcular el ancho.
//Elaborado por: Gil Verduzco.
function fnc_calcular_ancho_alto(p_width, p_opc_width)
{
	var width = p_width;
	var max_width = ( p_opc_width ? $(window).width() : $(window).height() );
	
	//Validamos si el modal tiene el ancho declarado en pixeles o en porcentaje.
	var px = p_width.toString().substring( p_width.toString().length - 2 );
	var prc = p_width.toString().substring( p_width.toString().length - 1 );
	var px_prc = px;
	if( px_prc != "px" )
	{
		px_prc = prc;
		if( prc != "%" )
			px_prc = "px";
	}
	
	//Validamos el width.
	if( p_width != "" )
	{
		if( isNaN( p_width * 1 ) )
		{
			width = ( px_prc == "px" ? p_width.toString().substring( 0,  p_width.toString().length - 2 ) * 1 : p_width.toString().substring( 0,  p_width.toString().length - 1 ) * 1 );
			
			if( isNaN( width * 1 ) || ( px != "px" && prc != "%" ) || ( width * 1 ) == 0 )
			{
				width = 300;
				px_prc = "px";
			}
		}
		else if( ( p_width * 1 ) == 0 )
		{
			width = 300;
			px_prc = "px";
		}
	}
	else
	{
		width = 300;
		px_prc = "px";
	}
	
	//Calculamos el porcentaje en pixeles.
	if( px_prc == "%" )
	{
		width = ( width > 100 ? 100 : width );
		width = ( width * ( p_opc_width ? $(window).width() : $(window).height() ) ) / 100;
		px_prc = "px";
	}
	
	//Validamos que lo ancho del gdi�logo no sea mas ancho que la ventana.
	if( width > max_width )
		width = max_width - 50;
	
	return width
}

//Funci�n para obtener los campos de un formulario.
//Elaborado por: Gil Verduzco.
function fnc_parametros(p_div_contenedor)
{
	//Validamos los campos obligatorios.
	if( fnc_validar_campos_obligatorios( p_div_contenedor ) )
	{
		var parametros = {};
		
		$( ( p_div_contenedor ? p_div_contenedor : "" ) + " .pa" ).each(function(){
			switch( this.type )
			{
				case "text":
				case "password":
				case "textarea":
					if( $.trim( $(this).val() ) !== "" )
						parametros[ $(this).attr("campo") ] = $.trim( $(this).val() );
					break;
				case "select-one":
					if( $(this).find("option:selected").val() !== "" )
						parametros[ $(this).attr("campo") ] = $.trim( $(this).val() );
					break;
				case "checkbox":
				case "radio":
					parametros[ $(this).attr("campo") ] = $(this).is(":checked") ? 1 : 0;
					break;
			}
		});
		
		return parametros;
	}
	
	return false;
}