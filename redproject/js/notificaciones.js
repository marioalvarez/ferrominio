var notificaciones_ajax = null;
var notificaciones_sin_leer_ajax = null;

//Funci�n para cargar las notificaciones.
function fnc_cargar_notificaciones(p_cargar)
{
	if( $("#contenedor_notificaciones").is(":visible") == false || p_cargar )
	{
		//Calculamos la m�xima y la m�nima altura.
		var altura_original = $(window).height() - 130;
		$("#contenedor_notificaciones #div_contenido_notificaciones").css({
			maxHeight: altura_original,
			minHeight: altura_original
		});
		
		//Calculamos cu�ntas notificaciones caben en la pantalla.
		var total_notificaciones_por_pantalla = Math.floor( Math.round( ( altura_original / 75 ), 2 ) );

		//Consultamos si hay nuevas notificaciones.
		if( !$("#contenedor_notificaciones").is(":visible") )
			fnc_consultar_notificaciones_total();
		
		//Validamos el paginado.
		$("#hid_nu_PageNotificacion").val( p_cargar ? $("#hid_nu_PageNotificacion").val() : 1 );
		
		if( notificaciones_ajax != null )
			notificaciones_ajax.abort();

		//Consultamos las notificaciones.
		notificaciones_ajax = fnc_peticionAjax({
			url: "./application/app_notificaciones.cfc",
			parametros: {
				method: "ListarJSON",
				id_Empresa: $("#hid_id_Empresa").val(),
				id_Recurso: $("#hid_id_Recurso").val(),
				page: $("#hid_nu_PageNotificacion").val(),
				rows: total_notificaciones_por_pantalla,
				sidx: "fh_RegistroSF DESC, hr_RegistroSF",
				sord: "DESC"
			},
			automensaje: false,
			f_callback: function(json){
				if( json.records > 0 )
				{
					$("#contenedor_notificaciones #div_contenido_notificaciones").empty();
					
					var notificaciones = "";
					
					$(json.rows).each(function(index){
						array_notificaciones = this.cell;
						
						//Validamos si esta notificaci�n es nueva.
						if( array_notificaciones[5] == 1 )
						{
							clase = 'noti_nueva ui-icon ui-icon-mail-closed';
							title = 'Nuevo';
						}
						else if( array_notificaciones[5] == 2 )
						{
							clase = 'noti_leida ui-icon ui-icon ui-icon-mail-open';
							title = 'Leido';
						}
	
						notificaciones += '' +
							'<div class="notificacionUsuario" id_TipoNotificacion="' + array_notificaciones[2] + '" id_Notificacion="' + array_notificaciones[3] + '">' +
								'<span class="cerrar_notificacion ' + clase + '" title="' + title + '"></span>' +
								'<div class="fecha_notificacion">' + array_notificaciones[6] +' '+ array_notificaciones[7] + '</div>' +
								'<div class="titulo_notificacion">' + array_notificaciones[8] + '</div>' +
								'<div class="mensaje_notificacion">' + array_notificaciones[4] + '</div>' +
							'</div>';
					});
	
					$("#contenedor_notificaciones #div_contenido_notificaciones").append( notificaciones );
					$("#contenedor_notificaciones ").show("clip");

					$(".notificacionUsuario").on("click", ".cerrar_notificacion[class*='noti_nueva']", function(event)
					{	
						var span_cerrar = this;
	
						if( $(span_cerrar).hasClass("noti_nueva") )
						{
							//Marcamos como le�da esta notificaci�n.
							fnc_peticionAjax({
								url: "./application/app_notificaciones.cfc",
								parametros: {
									method: "EditarEstatus",
									id_Empresa: $("#hid_id_Empresa").val(),
									id_Recurso: $("#hid_id_Recurso").val(),
									id_TipoNotificacion: $(this).parent(".notificacionUsuario").attr("id_TipoNotificacion"),
									id_Notificacion: $(this).parent(".notificacionUsuario").attr("id_Notificacion"),
									id_NotificacionEstatus: 2
								},
								bloquear_pantalla: false,
								automensaje: false,
								f_callback: function(json){
									$(span_cerrar).removeClass("noti_nueva ui-icon ui-icon-mail-closed").addClass("noti_leida ui-icon ui-icon-mail-open")
										.attr("title", "Leido");
					
									$("#num_notificaciones").text( json.RS.DATA.NU_TOTAL[0] );
								}
							});
						}
					});

					//Validamos si debemos ocultar las flechitas.
					if( json.page == json.total )
					{
						//Validar si estoy en la �ltima p�gina.
						if( json.total > 1 )
							$("#span_next_noti").hide();
						else
							$("#span_prev_noti, #span_next_noti").hide();

						//Calculamos la m�xima y la m�nima altura.
						if( json.rows.length < total_notificaciones_por_pantalla )
						{
							var altura = json.rows.length * 75;
							
							$("#contenedor_notificaciones #div_contenido_notificaciones").css({
								maxHeight: altura,
								minHeight: altura
							});
							
							//Validamos que no haya scroll.
							if( altura < $("#contenedor_notificaciones #div_contenido_notificaciones")[0].scrollHeight )
							{
								altura = $("#contenedor_notificaciones #div_contenido_notificaciones")[0].scrollHeight;
								
								$("#contenedor_notificaciones #div_contenido_notificaciones").css({
									maxHeight: altura,
									minHeight: altura
								});
							}
						}
					}
					else if( json.page == 1 )
					{
						$("#span_prev_noti").hide();
						$("#span_next_noti").show();
					}
				}
			}
		});
	}
	else
		$("#contenedor_notificaciones ").hide("clip");
}

//Funci�n para cambiar la p�gina de las notificaciones.
function fnc_cambiar_page_notificacion(p_sumar)
{
	$("#span_prev_noti, #span_next_noti").show();
	
	if( p_sumar === true )
	{
		$("#hid_nu_PageNotificacion").val( $("#hid_nu_PageNotificacion").val() * 1 + 1 );
	}
	else
	{
		if( $("#hid_nu_PageNotificacion").val() == "1" )
			$("#hid_nu_PageNotificacion").val(1);
		else
			$("#hid_nu_PageNotificacion").val( $("#hid_nu_PageNotificacion").val() * 1 - 1 )
	}

	//Cargamos las notificaciones.
	fnc_cargar_notificaciones(true);
}

//Funci�n para consultar cu�ntas notificaciones existen sin leer.
function fnc_consultar_notificaciones_total()
{
	if( notificaciones_sin_leer_ajax != null )
		notificaciones_sin_leer_ajax.abort();
	
	notificaciones_sin_leer_ajax = fnc_peticionAjax({
		url: "./application/app_notificaciones.cfc",
		parametros: {
			method: "ListarNotificacionesSinLeer",
			id_Empresa: $("#hid_id_Empresa").val(),
			id_Recurso: $("#hid_id_Recurso").val(),
			id_NotificacionEstatus: 1
		},
		automensaje: false,
		mostrar_espera: false,
		f_callback: function(json){
			//Si el usuario tiene visible la p�gina 1, entonces cargamos las notificaciones.
			if( $("#contenedor_notificaciones").is(":visible") && $("#hid_nu_PageNotificacion").val() == "1" 
				&& json.RS.DATA.NU_TOTAL[0] > ( $("#num_notificaciones").text() * 1 ) )
			{
				fnc_cargar_notificaciones(true);
			}
			
			$("#num_notificaciones").text( json.RS.DATA.NU_TOTAL[0] );
		}
	});
}