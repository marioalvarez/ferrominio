var ggrids = {
	crear: function(p_id_ggrid, p_opciones){
		
		var ggrid = $("#" + p_id_ggrid);
		var p_pagina = ( ( p_opciones ) ? ( ( p_opciones.pagina ) ? p_opciones.pagina : 1 ) : 1 );
		var p_paginado = ( ( $( "#" + p_id_ggrid ).attr("paginado") ) ? $( "#" + p_id_ggrid ).attr("paginado") : 15 );
		
		//Si existe lo eliminamos.
		$( "#grd_" + $(ggrid).attr("id") ).remove();

		var cabecero = '<thead><tr>';
		$(ggrid).find("columna").each(function(){
			var visible = ( ( $(this).attr("visible") ) ? $(this).attr("visible") : "true" ).toUpperCase();
			var td_oculto;
			
			if( visible == "FALSE" )
				td_oculto = "ggrid_td_oculto";
			else
				td_oculto = "";
					
			cabecero += '<th class="ggrid_header ' + td_oculto + '">' +
								$(this).text() +
						'</th>';
		});
		cabecero += '</tr></thead>';
		
		var cuerpo = '<tbody><tr><td class="ggrid_rows" colspan="' + ( $(ggrid).find(":not(columna[visible])").size() + $(ggrid).find("columna[visible='true']").size() ) + '">Espere, por favor...</td></tr></tbody>';
		
		//Creamos el ggrid.
		var table = '<table class="ggrid" style="width: 99%;" id="grd_' + $(ggrid).attr("id") + '">' + cabecero + cuerpo + '</table>';
		
		$(ggrid).after(table);
		
		//Consultamos los registros.
		if( p_opciones )
		{
			if( !p_opciones.registrosXML )
			{
				$.ajax({
					url: $(ggrid).attr("url"),
					async: true,
					type: "POST",
					data: $(ggrid).attr("parametros"),
					cache: false,
					dataType: "xml",
					success: function(registrosXML){
						//Armamos el cuerpo del ggrid.
						ggrids.crear_cuerpo( p_id_ggrid, p_pagina, p_paginado, registrosXML, p_opciones.grid_completo );
					},
					error: function(e){
						$( "#grd_" + $(ggrid).attr("id") + " tbody td:first" ).text("Error al consultar registros.");
						//alert( "Error al consultar registros." );
					}
				});
			}
			else
				ggrids.crear_cuerpo( p_id_ggrid, p_pagina, p_paginado, p_opciones.registrosXML, p_opciones.grid_completo );
		}
		else
		{
			$.ajax({
				url: $(ggrid).attr("url"),
				async: true,
				type: "POST",
				data: $(ggrid).attr("parametros"),
				cache: false,
				dataType: "xml",
				success: function(registrosXML){
					//Armamos el cuerpo del ggrid.
					ggrids.crear_cuerpo( p_id_ggrid, p_pagina, p_paginado, registrosXML );
				},
				error: function(e){
					$( "#grd_" + $(ggrid).attr("id") + " tbody td:first" ).text("Error al consultar registros.");
					//alert( "Error al consultar registros." );
				}
			});
		}
		
		//Guardamos las opciones en una variable din�mica.
		window[ "grd_" + $(ggrid).attr("id") + "_opciones" ] = p_opciones;
	}, 
	crear_cuerpo: function(p_id_ggrid, p_pagina, p_paginado, p_registrosXML, p_grid_completo){
		var cabecero = '<thead>' + $("#grd_" + p_id_ggrid + " thead" ).html() + '</thead>';
		var ggrid = $("#" + p_id_ggrid);
		
		//Si existe lo eliminamos.
		$("#grd_" + $(ggrid).attr("id")).remove();
		
		//Armamos el cuerpo del ggrid.
		var cuerpo = '';
		$(p_registrosXML).find("registros").slice( ( p_pagina - 1 ) * p_paginado, p_pagina * p_paginado ).each(function(){
			var infoRegistro = $(this);
			var trs = '<tr class="ggrid_rows" onClick="$(this).parent().children().removeClass(\'ggrid_rows_selected\'); $(this).addClass(\'ggrid_rows_selected\')">';
			$(ggrid).find("columna").each(function(){
				var visible = ( ( $(this).attr("visible") ) ? $(this).attr("visible") : "true" ).toUpperCase();
				var td_oculto;
				
				if( visible == "FALSE" )
					td_oculto = "ggrid_td_oculto";
				else
					td_oculto = "";
				
				trs += '<td class="' + td_oculto + '">' +
								'<div style="' + ( ( $(this).attr("style") ) ? $(this).attr("style") : "" ) + '">' + $.trim( $(infoRegistro).find( $(this).attr("nom_columna") ).text() ) + '</div>' +
					   '</td>';
			});
			trs += "</tr>";
			cuerpo += trs;
		});
		cuerpo = '<tbody>' + cuerpo + '</tbody>';
		
		//Guardamos los registros en una variable din�mica.
		window[ "grd_" + $(ggrid).attr("id") + "_registros" ] = p_registrosXML;
		
		//Creamos el ggrid.
		var table = '<table class="ggrid" style="width: 99%;" id="grd_' + $(ggrid).attr("id") + '">' + cabecero + cuerpo + '</table>';
		
		$(ggrid).after(table);

		//Agregamos el paginado.
		
		//Primeramente, obtenemos las acciones.
		var acciones = "";
		$( "#" + $(ggrid).attr("id") ).find("accion").each(function(){
			var accion = ( ( ( $(this).attr("accion") ) ? $(this).attr("accion") : "" ) == "" ? false : $(this).attr("accion") );
			var evento = ( ( $(this).attr("evento") ) ? $(this).attr("evento") : "" );
			var icono;
			var titulo;
			
			if( accion )
			{
				accion = accion.toUpperCase();
				
				if( accion == "AGREGAR" )
				{
					icono = "ui-icon ui-icon-plusthick";
					titulo = "Agregar";
				}
				else if( accion == "EDITAR" )
				{
					icono = "ui-icon ui-icon-pencil";
					titulo = "Editar";
				}
				else if( accion == "ELIMINAR" )
				{
					icono = "ui-icon ui-icon-trash";
					titulo = "Eliminar";
				}
				
				acciones += "<div title='" + titulo + "' onClick='" + evento + "'><span class='" + icono + "'></span><span>" + titulo + "</span></div>"
			}
			
		});
		
		$( "#grd_" + $(ggrid).attr("id") ).append("<tr class='ggrid_footer'>" +
												  		"<td colspan='" + ( $(ggrid).find(":not(columna[visible])").size() +
																$(ggrid).find("columna[visible='true']").size() ) + "' align='center'>" +
															"<table style='width: 100%;'>" +
																"<tr class='ggrid_acciones'>" +
																	"<td style='width: 100%;'>" +
																		"<div onClick='ggrids.crear(\"" + $(ggrid).attr("id") + "\");' title='Actualizar'><span class='ui-icon ui-icon-refresh'></span><span>Actualizar</span></div>" +
																		acciones +
																	"</td>" +
																"</tr>" +
																"<tr class='ggrid_pager'>" +
																	"<td align='center' nowrap='nowrap'>" +
																		"<span> </span>" +
																		"<img align='absmiddle' src='./imagenes/izq.png' width='20' height='20' " +
																			"title='Anterior' style='cursor: pointer;' " +
																			"onClick='ggrids.cambiar_paginado(\"" + $(ggrid).attr("id") + "\"," + 
																				p_paginado + ", \"restar\", grd_" + $(ggrid).attr("id") + "_registros);' />" +
																		"<span> P&aacute;gina </span>" +
																		"<input type='text' id='txt_paginado' class='input_text' style='width: 30px; height: 16px; text-align: center;' value='" + p_pagina + "' onBlur='ggrids.validar_max_page(\"" + $(ggrid).attr("id") + "\"," + p_paginado + ", grd_" + $(ggrid).attr("id") + "_registros );' onKeyPress='return fnc_solo_numeros(event)' onKeyUp='ggrids.validar_max_page_enter(event, \"" + $(ggrid).attr("id") + "\"," + p_paginado + ", grd_" + $(ggrid).attr("id") + "_registros );' onFocus='$(this).select();' />" +
																		"<span> de </span><span id='span_max_page'>" + Math.ceil( $(p_registrosXML).find("registros").size() / p_paginado ) + "</span>" +
																		"<span> </span>" +
																		"<img align='absmiddle' src='./imagenes/der.png' width='20' height='20' title='Siguiente' " + "onClick='ggrids.cambiar_paginado(\"" + $(ggrid).attr("id") + "\"," + p_paginado + ", \"sumar\", grd_" + $(ggrid).attr("id") + "_registros);' style='cursor: pointer;' />" +
																	"</td>" +
																"</tr>" +
															"</table>" +
														"</td>" +
													"</tr>");
		
		if( p_grid_completo )
			p_grid_completo();
	},
	cambiar_paginado: function(p_id, p_paginado, p_opcion, p_registros){
		//Obtenemos la p�gina actual.
		var pagina_actual = $("#grd_" + p_id + " #txt_paginado").val() * 1;
		var nueva_pagina = pagina_actual;

		//Calcuamos la nueva p�gina.
		if( p_opcion )
		{
			nueva_pagina = pagina_actual + 1;
			if( p_opcion == "restar" )
				nueva_pagina = pagina_actual - 1;
		}
		
		//Obtenemos el max page.
		var max_page = $("#grd_" + p_id + " #span_max_page").text() * 1;
		
		//Validamos la nueva p�gina.
		if( !( nueva_pagina > 0 && nueva_pagina <= max_page ) )
		{
			if( nueva_pagina == 0 )
			{
				$("#grd_" + p_id + " #txt_paginado").val(1)
				nueva_pagina = 1;
			}
			else if( nueva_pagina > max_page )
			{
				$("#grd_" + p_id + " #txt_paginado").val( max_page );
				nueva_pagina = max_page;
			}
		}
		
		ggrids.crear( p_id, { pagina: nueva_pagina, registrosXML: p_registros } );
	},
	validar_max_page: function(p_id, p_paginado, p_registros){
		ggrids.cambiar_paginado( p_id, p_paginado, false, p_registros );
	},
	validar_max_page_enter: function(evt, p_id, p_paginado, p_registros){
		var charCode = (evt.which) ? evt.which : evt.keyCode;
		
		if( charCode == 13 )
			ggrids.cambiar_paginado( p_id, p_paginado, false, p_registros );
	},
	renglon_seleccionado: function(p_id){
		//Obtenemos todos los datos del rengl�n seleccionado.
		var datos = "";
		
		if( $( "#grd_" + p_id + " tbody tr.ggrid_rows_selected").size() == 1 )
		{
			$( "#grd_" + p_id + " tbody tr.ggrid_rows_selected").each(function(index){
				$(this).find("td").each(function(index){
					datos += $("#" + p_id + " columna:eq(" + index + ")").attr("nom_columna") + ": \"" + $.trim( $(this).text() ) + "\", ";
				});
			});
			
			//Eliminamos la �ltima coma.
			datos = datos.substring( 0, datos.length - 2 );
	
			//Creamos el objeto y lo regresamos.
			datos = "var objDatosRenglonSeleccionado = {" + datos + "}";
			eval( datos );
		}
		else
			var objDatosRenglonSeleccionado = false;

		return objDatosRenglonSeleccionado;
	},
	actualizar: function(p_id, p_parametros){
		//Cambiamos los par�metros del grid.
		$( "#" + p_id ).attr( "parametros", ( ( p_parametros ) ? p_parametros : $( "#" + p_id ).attr( "parametros") ) );
		
		//Actualizamos.
		ggrids.crear( p_id );
	}
}