/*Codigo para iniciar controles segun su className*/
doc_x=$(document);
doc_x.ready(inicializarControles);
function inicializarControles(){
	$(".contenido").keypress(escanear_enter);
	$(".contenido").blur(escanear_lcase);
	$(".contenido_minusculas").keypress(escanear_enter);
	$(".contenido_minusculas").blur(escanear_lcase);
	$(".contenido_textarea").blur(escanear_lcase);
	$(".contenido_readOnly").keydown(escanear_readOnly);
	$(".contenido_readOnly").keydown(escanear_enter);
	$(".contenido_libre").keydown(escanear_enter);
	
	$(".contenido_numerico").keydown(escanear_numerico);
	/*
	$(".contenido_numerico").keydown(function(event) {
		if(event.shiftKey == false && event.altKey == false){
			// Allow: backspace, delete, tab and escape
			if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || 
				 // Allow: Ctrl+A
				(event.keyCode == 65 && event.ctrlKey === true) || 
				 // Allow: home, end, left, right
				(event.keyCode >= 35 && event.keyCode <= 39)) {
					 // let it happen, don't do anything
					 return;
			}
			else {
				// Number typed from keyboard
				if (event.keyCode >= 48 && event.keyCode <= 57) return;
				// Number typed from numpad
				if (event.keyCode >= 96 && event.keyCode <= 105) return;
				//anything else 
				event.preventDefault(); 
			}
		}
		else event.preventDefault(); 
    });
	*/
	$(".contenido_numerico").keydown(escanear_enter);
	$(".contenido_alfanumerico_con_espacios").keydown(escanear_enter);
	$(".contenido_alfanumerico_con_espacios").alphanumeric({allow:' '});
	$(".contenido_alfanumerico_con_espacios").blur(escanear_lcase);
	$(".contenido_flotante").numeric({allow: '.'});
	$(".contenido_flotante").keydown(escanear_enter);
	$(".contenido_flotante_negativo").numeric({allow: '.-'});
	$(".contenido_flotante_negativo").keydown(escanear_enter);
	$(".contenido_moneda").numeric({allow:'.'});
	$(".contenido_moneda").keydown(escanear_enter);
	$(".contenido_moneda").focus(focus_money);
	$(".contenido_moneda").blur(blur_money);
	$(".contenido_numerico_0000").keydown(escanear_numerico);
	$(".contenido_numerico_0000").keydown(escanear_enter);
	$(".contenido_numerico_0000").blur(blur_numerico_0000);
	$(".boton_popup").keydown(escanear_enter);
	$(".renglon_grid").mouseover(mouseOverRenglon);
	$(".renglon_grid").mouseout(mouseOutRenglon);
	}

/*HGB: funci�n que comprueba si hay hot keys definidas del control jquery-hotkey y regresa true si existe el hotkey del evento*/
function comprueba_hotkeys(event){
	if (typeof(jQuery.hotkeys.redKeys) != "undefined"){
		return jQuery.hotkeys.redFindKey(event);
		}
	}

/*funci�n que filtra las teclas numericas solamente*/
function escanear_numerico(event) {
	if (comprueba_hotkeys(event)) return;
	
	if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || 
		 // Allow: Ctrl+A
		(event.keyCode == 65 && event.ctrlKey === true) || 
		 // Allow: home, end, left, right
		(event.keyCode >= 35 && event.keyCode <= 39)) {
			 // let it happen, don't do anything
			 return;
		}
	else {
		// Number typed from keyboard
		if (event.keyCode >= 48 && event.keyCode <= 57) return;
		// Number typed from numpad
		if (event.keyCode >= 96 && event.keyCode <= 105) return;
		//anything else 
		event.preventDefault(); 
		}
	}

function cambiarFoco(Campo){
	var seEncontro = false;
	var seCambio = false;
	var elements = document.getElementsByTagName('*');
	for (var i = 0; i < elements.length; i++) {
		if(!seEncontro){
			if (Campo == elements[i]) seEncontro = true;
		}
		else{
			var element = elements[i];
			if(element.tagName.toUpperCase() == 'INPUT' 
					&& element.type.toUpperCase() != 'HIDDEN' 
					&& element.style.display.toUpperCase() != 'NONE' 
					&& element.style.visibility.toUpperCase() != 'HIDDEN'
					&& element.disabled.toString().toUpperCase() != 'TRUE'){
				element.focus(); 
				seCambio=true; 
				break;
			}
			if(element.tagName.toUpperCase() == 'BUTTON'
					&& element.style.display.toUpperCase() != 'NONE' 
					&& element.style.visibility.toUpperCase() != 'HIDDEN'
					&& element.disabled.toString().toUpperCase() != 'TRUE'){
				element.focus(); 
				seCambio=true; 
				break; 
			}
			if(element.tagName.toUpperCase() == 'SELECT'
					&& element.style.display.toUpperCase() != 'NONE' 
					&& element.style.visibility.toUpperCase() != 'HIDDEN'
					&& element.disabled.toString().toUpperCase() != 'TRUE'){
				element.focus(); 
				seCambio=true; 
				break; 
			}
			if(element.tagName.toUpperCase() == 'TEXTAREA'
					&& element.style.display.toUpperCase() != 'NONE' 
					&& element.style.visibility.toUpperCase() != 'HIDDEN'
					&& element.disabled.toString().toUpperCase() != 'TRUE'){
				element.focus(); 
				seCambio=true; 
				break; 
			}
			if(element.tagName.toUpperCase() == 'A'
					&& element.style.display.toUpperCase() != 'NONE' 
					&& element.style.visibility.toUpperCase() != 'HIDDEN'){
				element.focus(); 
				seCambio=true; 
				break; 
			}
		}	
	}
	if(!seCambio){
		for (var i = 0; i < elements.length; i++) {
			var element = elements[i];
			if(element.tagName.toUpperCase() == 'INPUT' && element.type.toUpperCase() != 'HIDDEN'){
				element.focus(); seCambio=true; break;
			}
			if(element.tagName.toUpperCase() == 'BUTTON'){element.focus(); seCambio=true; break; }
			if(element.tagName.toUpperCase() == 'SELECT'){element.focus(); seCambio=true; break; }
			if(element.tagName.toUpperCase() == 'TEXTAREA'){element.focus(); seCambio=true; break; }
			if(element.tagName.toUpperCase() == 'A'){element.focus(); seCambio=true; break; }
		}
	}
	return !seCambio; 
}
/* Modificada Por: Porfirio Padilla, Mayo 2011 */
function escanear_readOnly(e){
	if (comprueba_hotkeys(event)) return;
	
	var keyCode = 0;
	if(e) keyCode = e.which;
	else keyCode = event.keyCode;
	if(keyCode == 13 || keyCode == 9 || (36 < keyCode && keyCode < 41)) return true;
	else return false;
	}
function escanear_enter(e){
	var keyCode = 0;
	if(e) keyCode = e.which;
	else keyCode = event.keyCode ? event.keyCode : event.charCode; 
	if (keyCode == 13) { 
		var comp = ($(this).attr('id') != '') ? $(this).attr('id') : $(this).attr('name');
		return cambiarFoco($x(comp)); 
		}
	else return true;
	}
function tecla_escape(e){
	var keyCode = 0;
	if(e) keyCode = e.which;
	else keyCode = event.keyCode ? event.keyCode : event.charCode; 
	if (keyCode == 27) { 
		return 'escape';
		}
	else return true;
	}
	
function tecla_enter(e){
	var keyCode = 0;
	if(e) keyCode = e.which;
	else keyCode = event.keyCode ? event.keyCode : event.charCode; 
	if (keyCode == 13) { 
		return 'enter';
		}
	else return true;
	}

function mouseOverRenglon(){
	$(this).css("background", '#99CCCC');
	}
function mouseOutRenglon(){
	$(this).css("background", '');
	}
function setEstatusWindowEnBlanco(){
	window.status= '';
	return true;
	}

function focus_money(){
	this.value= unformatNumber(this.value);
	this.select();
	}

function blur_money(){
	this.value= formatMoney(this.value, '$');
	}

function blur_numerico_0000(){
	this.value= formatNumerico(this.value, '0000');
	}
	
function formatNumerico(numero, formato){
	var caracteres_por_rellenar= formato.length - numero.length;
	if (caracteres_por_rellenar > 0){
		numero= formato.substring(0, caracteres_por_rellenar) + numero;
		}
	return numero;
	}
	
function reverseString(texto){
	var texto_reverse= "";
	var longitud_texto= texto.length;
	for (var ind = 0; ind < longitud_texto; ind++){
		texto_reverse= texto.charAt(ind) & texto_reverse;
		}
	return texto_reverse;
	}
	
function formatMoney(numero, prefijo){
	if (numero == "")
		return "";
	numero = Math.round(parseFloat(numero)*Math.pow(10,4))/Math.pow(10,4)
	prefijo = prefijo || '';
	numero += '';
	var splitStr = numero.split('.');
	var splitLeft = splitStr[0];
	var splitRight = splitStr.length > 1 ? '.' + splitStr[1] : '.0000';
	splitRight = splitRight + '0000';
	splitRight = splitRight.substr(0,5);
	var regx = /(\d+)(\d{3})/;
	while (regx.test(splitLeft)){
		splitLeft = splitLeft.replace(regx, '$1' + ',' + '$2');
		}
	return prefijo + splitLeft + splitRight;
	}
	
function formatDecimal(numero){
	if (numero == "")
		return "";
	numero = Math.round(parseFloat(numero)*Math.pow(10,2))/Math.pow(10,2)
	//prefijo = prefijo || '';
	numero += '';
	var splitStr = numero.split('.');
	var splitLeft = splitStr[0];
	var splitRight = splitStr.length > 1 ? '.' + splitStr[1] : '.00';
	splitRight = splitRight + '00';
	splitRight = splitRight.substr(0,3);
	var regx = /(\d+)(\d{3})/;
	while (regx.test(splitLeft)){
		splitLeft = splitLeft.replace(regx, '$1' + ',' + '$2');
		}
	return /*prefijo*/ + splitLeft + splitRight;
	}

function unformatNumber(numero){
	if (numero == "")
		return "";
	return numero.replace(/([^0-9\.\-])/g,'')*1;
	} 
	
/*Modificaci�n por compatibilidad con nuevas versiones de navegadores*/
function $x(id_or_name) {
	element = document.getElementById(id_or_name);
	if (!element){
		elements= document.getElementsByName(id_or_name);
		if (elements)
			element= elements[0];
		}
	return element;
	}

/*Funcion que asigna el uppercase del value de algun control con la propiedad val de jquery*/
function escanear_lcase(){
	if($(this).is('input') || $(this).is('textarea')){
		$(this).val( $(this).val().toUpperCase());
	}
}
	
/*obtiene un elemento por su ID*/
/*se modifico esta funcion, para compatibilidad con nuevos navegadores
function $x() {
	var elements = new Array();
	for (var i = 0; i < arguments.length; i++) {
		var element = arguments[i];
		if (typeof element == 'string')
			element = document.getElementById(element);
		if (arguments.length == 1)
			return element;
		elements.push(element);
		}
	return elements;
	}
*/
/*
//------------------ DEPRECADA --------------------------------------
function escanear_minusculas(){
	if (window.event.keyCode < 123 && window.event.keyCode > 96)
		window.event.keyCode-= 32;
	}
//------------------ DEPRECADA --------------------------------------
//------------------ DEPRECADA --------------------------------------
function escanear_retorno_readOnly(){
	if (window.event.keyCode==8 || window.event.keyCode== 46){
		//el returnValue= false, es para detener el evento de backspace
		window.event.returnValue= false;
		window.event.keyCode= 0;
		}
	}
//------------------ DEPRECADA --------------------------------------
//------------------ DEPRECADA --------------------------------------
function escanear_numerico(){
	if (event.keyCode < 48 || event.keyCode > 57) 
		event.returnValue = false;
	}
//------------------ DEPRECADA --------------------------------------
//------------------ DEPRECADA --------------------------------------
function escanear_alfanumerico_con_espacios(){
	if ( !(event.keyCode >= 48 && event.keyCode <= 57) && !(event.keyCode >= 65 && event.keyCode <= 90) && !(event.keyCode >= 97 && event.keyCode <= 122) && !(event.keyCode == 32)) 
		event.returnValue = false;
	//else
	//	event.returnValue = true;
	}
//------------------ DEPRECADA --------------------------------------
function escanear_flotante(input_txt){
	if (event.keyCode < 48 || event.keyCode > 57){
		if (event.keyCode != 46){
			event.returnValue = false;
			}
		else {
			if (arguments[0].type == 'keypress'){
				if ( this.value.indexOf('.') > -1 )
					event.returnValue = false;
				}				
			else {
				if ( arguments[0].value.indexOf('.') > -1 ){
					event.returnValue = false;
					}
				}				
			}
		}
	}
//------------------ DEPRECADA --------------------------------------
//------------------ DEPRECADA --------------------------------------
function escanear_flotante_negativo(input_txt){
	if (event.keyCode < 48 || event.keyCode > 57){
		if (event.keyCode != 46 && event.keyCode != 45){
			event.returnValue = false;
			}
		//si es 46 o 45
		else {
			//si es el punto decimal
			if (event.keyCode == 46){
				if (arguments[0].type == 'keypress'){
					if ( this.value.indexOf('.') > -1 )
						event.returnValue = false;
					}				
				else {
					if ( arguments[0].value.indexOf('.') > -1 ){
						event.returnValue = false;
						}
					}				
				}
			//si no es entonces es el signo negativo
			else{			
				if (arguments[0].type == 'keypress'){
					if ( this.value != '' )
						event.returnValue = false;
					}				
				else {
					if ( arguments[0].value != '' ){
						event.returnValue = false;
						}
					}
				}
			//aqui termina el else de si es 46 o 45
			}
		//aqui termina el primer if de si es menor a 48 y mayor a 57
		}
	//aqui termina la funcion
	}
//------------------ DEPRECADA --------------------------------------
*/

/*funciones que quita los espacios en blanco de una cadena*/
String.prototype.trim = function() {
	return this.replace(/^\s+|\s+$/g,"");
}
String.prototype.ltrim = function() {
	return this.replace(/^\s+/,"");
}
String.prototype.rtrim = function() {
	return this.replace(/\s+$/,"");
}

function RegresaValorPopUpBusqueda(valor, id_control_recibe_valor){
	if(opener != null) {// for Chrome 
		opener.document.getElementById(id_control_recibe_valor).value= valor;
		}
	window.returnValue= valor;
	}

/*Abre una ventana tipo Pop Up tipo modal*/
function AbrirPopUpBusqueda(idControlRecibeValor, strPaginaBusqueda, strParametrosPagina, anchoPagina, altoPagina){
	identificador= new Date();
	ControlRecibeValor= $x(idControlRecibeValor);
	valor_anterior= ControlRecibeValor.value;
	valor_devuelto= showModalDialog(strPaginaBusqueda + '?identificador=' + identificador.getTime() + '&idControlRecibeValor=' + idControlRecibeValor + '&' + strParametrosPagina,'','dialogHeight:' + altoPagina + ';dialogWidth:' + anchoPagina + ';');
	//se comprueba que el valor anterior y el valor de ControlRecibeValor sea igual, en ese caso no se asigno directamente por opener.document desde la pagina pop
	if (valor_anterior == ControlRecibeValor.value)		
		ControlRecibeValor.value = valor_devuelto;
	ponerFocus(idControlRecibeValor);
	ControlRecibeValor.select();	
	}	
	
/*Abre una ventana tipo Pop Up tipo modal*/
function AbrirPopUpBusquedaRegresaArreglo(idControlRecibeValor, strPaginaBusqueda, strParametrosPagina, anchoPagina, altoPagina){
	identificador= new Date();
	ControlRecibeValor= $x(idControlRecibeValor);
	valor_anterior= ControlRecibeValor.value;
	valor_devuelto= showModalDialog(strPaginaBusqueda + '?identificador=' + identificador.getTime() + '&idControlRecibeValor=' + idControlRecibeValor + '&' + strParametrosPagina,'','dialogHeight:' + altoPagina + ';dialogWidth:' + anchoPagina + ';');
	//se comprueba que el valor anterior y el valor de ControlRecibeValor sea igual, en ese caso no se asigno directamente por opener.document desde la pagina pop
	if (valor_anterior == ControlRecibeValor.value)		
		ControlRecibeValor.value = valor_devuelto;
	}	

/*funcion que cambia el valor de un input invocando al evento onchange*/
function cambiarValorConEventoOnChange(id_input, valor_nuevo){
	document.getElementById(id_input).value = valor_nuevo;
	if (document.getElementById(id_input).onchange)
		document.getElementById(id_input).onchange();
}

/*agrega funciones al body.onload, parametro: funcion*/
function addLoadEvent(func){
	var oldonload = window.onload;
	if (typeof window.onload != 'function'){
		window.onload = func;
		}
	else {
		window.onload = function(){
			oldonload();
			func();
			}
		}
	}
/*Abre una ventana tipo Pop Up tipo modal para Editar o Agregar*/
function AbrirPopUpAgregarEditar(refrescar, strPaginaPopUp, strParametrosPaginaPopUp, anchoPagina, altoPagina){
	identificador= new Date();
	modificado= showModalDialog(strPaginaPopUp + '?identificador=' + identificador.getTime() + '&' + strParametrosPaginaPopUp,'','dialogHeight:' + altoPagina + ';dialogWidth:' + anchoPagina + ';');
	if (refrescar == 'true' && modificado == 'true'){
		window.location.reload();
		}
	}
	
/*Abre una ventana tipo Pop Up tipo modal para Editar o Agregar, devuelve true si se realizo el agregar o el actualizar*/
function AbrirPopUpAgregarEditarDinamico(strPaginaPopUp, strParametrosPaginaPopUp, anchoPagina, altoPagina){
	identificador= new Date();
	modificado= showModalDialog(strPaginaPopUp + '?identificador=' + identificador.getTime() + '&' + strParametrosPaginaPopUp,'','dialogHeight:' + altoPagina + ';dialogWidth:' + anchoPagina + ';');
	return modificado;
	}
	
function FormToURL(id_forma){
	var forma= $x(id_forma);
	//var cadena_url= '?';
	var cadena_url= '';
	for (var ind = 0; ind < forma.length; ind++){
		cadena_url= cadena_url + forma.elements[ind].name + '=' + forma.elements[ind].value + '&';
		}
	cadena_url= cadena_url.substring(0, cadena_url.length - 1);
	return cadena_url;
	}
/*Pon el foco en un control por su id como parametro*/
function ponerFocus(idControl){
	try{
		document.getElementById(idControl).focus();
		}
	catch(err){};
	}

function TraerDescripcionPorAjax(strParametros, id_LabelDestino, id_BotonSubmit, paginaFuente){
	lblDestino= $("#" + id_LabelDestino);
	btnSubmit= $x(id_BotonSubmit);
  $.ajax({
		async:true,
		type: "POST",
	 	dataType: "text",
	 	contentType: "application/x-www-form-urlencoded",
		url: paginaFuente,
		data: strParametros,
	 	beforeSend: function (){
			lblDestino.html('<img src="../images/cargando.gif" title="Descargando Datos">');
			btnSubmit.disabled= "disabled";
			},
	 	success: function (respuestaHtml){
			lblDestino.html(respuestaHtml);
			btnSubmit.disabled= "";
			},
	 	timeout:5000,
	 	error:function (){
			lblDestino.html('<img src="../images/warning_20x18.gif" title="Error en Conexi�n">');
			}
 		}); 
	}
	//variable para mostrar mientras se descarga contenido ajax
	html_imagen_descarga= '<img src="../images/cargando.gif" title="Descargando Datos">';
/**/

function AjaxPaginaDinamica(paginaFuente, strParametros, id_LabelDestino, errorFunction){
	lblDestino= $("#" + id_LabelDestino);
  $.ajax({
		async:true,
		type: "POST",
	 	dataType: "text",
	 	contentType: "application/x-www-form-urlencoded",
		url: paginaFuente,
		data: strParametros,
		complete: function (){
			},
	 	beforeSend: function (){
			},
	 	success: function (respuestaHtml){
			lblDestino.html(respuestaHtml);
			},
	 	timeout:5000,
	 	error:function (){
			errorFunction();
			}
 		}); 
	}
function AjaxSubmit(strParametros, id_CampoDestino, paginaFuente){
	lblDestino= $("#" + id_CampoDestino);
  $.ajax({
		async:true,
		type: "POST",
	 	dataType: "text",
	 	contentType: "application/x-www-form-urlencoded",
		url: paginaFuente,
		data: strParametros,
		complete: function (){
			//$x('busqueda_de_Obra').focus();
			},
	 	beforeSend: function (){
			lblDestino.html('<img src="../images/cargando.gif" title="Descargando Datos"');
			},
	 	success: function (respuestaHtml){
			lblDestino.html(respuestaHtml);
			},
	 	timeout:5000,
	 	error:function (){
			lblDestino.html('<img src="../images/warning_20x18.gif" title="Error en Conexi�n">');
			}
 		}); 
	}
	

// Funcion para formatear numeros
function oNumero(numero)
	{
	//Propiedades 
	this.valor = numero || 0
	this.dec = -1;
	//M�todos 
	this.formato = numFormat;
	this.ponValor = ponValor;
	//Definici�n de los m�todos 
	function ponValor(cad)
	{
	if (cad =='-' || cad=='+') return
	if (cad.length ==0) return
	if (cad.indexOf('.') >=0)
		this.valor = parseFloat(cad);
	else 
		this.valor = parseInt(cad);
	} 
	function numFormat(dec, miles)
	{
	var num = this.valor, signo=3, expr;
	var cad = ""+this.valor;
	var ceros = "", pos, pdec, i;
	for (i=0; i < dec; i++)
	ceros += '0';
	pos = cad.indexOf('.')
	if (pos < 0)
		cad = cad+"."+ceros;
	else
		{
		pdec = cad.length - pos -1;
		if (pdec <= dec)
			{
			for (i=0; i< (dec-pdec); i++)
				cad += '0';
			}
		else
			{
			num = num*Math.pow(10, dec);
			num = Math.round(num);
			num = num/Math.pow(10, dec);
			cad = new String(num);
			}
		}
	pos = cad.indexOf('.')
	if (pos < 0) pos = cad.lentgh
	if (cad.substr(0,1)=='-' || cad.substr(0,1) == '+') 
		   signo = 4;
	if (miles && pos > signo)
		do{
			expr = /([+-]?\d)(\d{3}[\.\,]\d*)/
			cad.match(expr)
			cad=cad.replace(expr, RegExp.$1+','+RegExp.$2)
			}
	while (cad.indexOf(',') > signo)
		if (dec<0) cad = cad.replace(/\./,'')
			return cad;
	}
}//Fin del objeto oNumero:

function compare_dates(fecha_final, fecha_inicial)   
  {  
	
	if (fecha_inicial == "" || fecha_final == "")
		{
			return(true);
		}else{
			var xDay=fecha_final.substring(0,2); 
			var xMonth=fecha_final.substring(3,5);   
			var xYear=fecha_final.substring(6,10);   
			var yDay=fecha_inicial.substring(0,2);   
			var yMonth=fecha_inicial.substring(3,5);
			var yYear=fecha_inicial.substring(6,10);   
			if (xYear> yYear)   
			{   
					return(true)   
			}   
			else  
			{   
				if (xYear == yYear)   
				{    
					if (xMonth> yMonth)   
					{   
							return(true)   
					}   
					else  
					{    
						if (xMonth == yMonth)
						
						{   
							if (xDay > yDay){   
								return(true);  
							}
							else if(xDay == yDay){
								return(true);
							}
							else{  
								return(false);   
							}
						}   
						else  
							return(false);   
					}   
				}   
				else  
					return(false);   
			} 
		}
}

/*Selecciona el segundo elemento de un select si la primera opci�n tiene como value = '' y solo tienes dos opciones*/
function combo_default(id_select){
	combo= $x(id_select);
	if (combo.length == 2 && combo.options[0].value == ""){
		combo.selectedIndex= 1;
		}
	}
/*Selecciona el segundo elemento de una lista de select si la primera opci�n de cada select tiene como value = '' y solo tienes dos opciones*/
function combos_default(lista_combos){
	for (var ind = 0; ind < arguments.length; ind++){
		combo= $x(arguments[ind]);
		if (combo.length == 2 && combo.options[0].value == ""){
			combo.selectedIndex= 1;
			}
		}
	}

/* Usado en las toolbar de als paginas */
function page_back(url){
	if(document.referrer == document.location) document.location = url;
	else history.go(-1);
	}
/* redirecciona a una pagina en especifico, preguntando un mensaje de confirmaci�n antes si es qu as� se manda*/
function page_goto(pagina, confirmText){
	
	if(!confirmText || confirmText == "") location = pagina;
	else if(confirm(confirmText)) location = pagina;
	}

function page_open_pop(url, config){
	window.open(url);
	}
	
//funcion para mostrar un alert de tipo validacion
function alertMessage(mensaje, tipo, id_control_focus){
	switch (tipo){
		case 'inline':
			inlineMsg(id_control_focus, mensaje, 2, true);
			break;
		case 'alert':
			alert(mensaje);
			break;
		default:
			alert(mensaje);
		}
	}
	
	
function invokeAppAjax(levelSubDirectory, app, method, params, typeResponse){
	try{
		var upLevel= '';
		for(var ind= 0; ind < levelSubDirectory; ind++){
			upLevel= upLevel + '../';
			}
		upLevel= upLevel + 'application/';
		var urlString= upLevel + app;
		var dataString= "Method=" + method + "&" + params;
		if (typeResponse)
			dataString= dataString + "&typeResponse=" + typeResponse;
		dataString= dataString + "&returnFormat=json";
		var resultado= $.ajax({
						type: "POST",
						url: urlString,
						data: dataString,
						async: false
		}).responseText;
		return $.parseJSON(resultado);
		}
	catch(error){
		var struct= new Object();
		struct.ERRORTYPE= "unknown";
		struct.MESSAGE= "Error al invocar la funci�n invokeAppAjax";
		struct.VALUE= "";
		return struct;
		}
	}