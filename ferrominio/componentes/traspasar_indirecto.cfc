<cfcomponent extends='conexion'>
	<!---Funcion para obtener el listado de Empresas----------------->
    <!---Desarrollador: Miguel Tiznado ------------------------------>
    <!---Fecha: 25/08/2015------------------------------------------->
	<cffunction name="get_listado_Empresas" access="public" returntype="struct">
        <cfargument name="id_Empresa" 	type="string" required="false" default="NULL">
		<cfif id_Empresa EQ ''>
			<cfset id_Empresa = 'NULL'>
		</cfif>
        <cftry>
            <cfquery name="RSDatos" datasource="#session.cnx#">                
				SELECT 
					id_Empresa, nb_Empresa
				FROM 
					Empresas
				WHERE
					id_Empresa = ISNULL( #id_Empresa#, id_Empresa )
            </cfquery>
            <cfset Result.SUCCESS = true>
            <cfset Result.MESSAGE = 'Datos obtenidos con exito'>
            <cfset Result.RS = RSDatos>
            <cfreturn result>
         	<cfcatch type='database'>
                <cfset Result.SUCCESS = false>
				<cfset Result.TipoError = "Error en la consulta de base de datos.">
                <cfset Result.MESSAGE = "Error al recuperar los datos => #cfcatch.Detail#">    
                <cfreturn Result>
            </cfcatch>
        </cftry>
    </cffunction>
	
	<!---Funcion para traspasar montos de gastos indirectos------------>
    <!---Desarrollador: Miguel Tiznado -------------------------------->
    <!---Fecha: 22/07/2015--------------------------------------------->
	<cffunction name="Indirectos_Montos_Traspasar" access="public" returntype="struct">
		<cfargument name="id_EmpresaOrigen" 		type="string" required="true">
        <cfargument name="id_ObraOrigen" 			type="string" required="true">
		<cfargument name="id_EmpresaDestino" 		type="string" required="true">
		<cfargument name="id_ObraDestino" 			type="string" required="true">
		<cfargument name="im_MontoTraspaso" 		type="string" required="true">
		<cfargument name="id_Empleado"				type="string" required="true">
		<cftransaction action="begin">
			<cftry>
				<cfset Result.SQL = "
					EXECUTE	Indirectos_Montos_Traspasar
							@id_EmpresaOrigen 	= #id_EmpresaOrigen#,
							@id_ObraOrigen		= #id_ObraOrigen#,
							@id_EmpresaDestino 	= #id_EmpresaDestino#,
							@id_ObraDestino		= #id_ObraDestino#,
							@im_MontoTraspaso	= #im_MontoTraspaso#,
							@id_Empleado		= #id_Empleado#
					">
	            <cfquery name="RSDatos" datasource="#session.cnx#">                
					#PreserveSingleQuotes(Result.SQL)#
	            </cfquery>
	            <cfset Result.SUCCESS = true>
	            <cfset Result.MESSAGE = 'La operacion se realizo correctamente.'>
				<cftransaction action="commit">
	            <cfreturn result>
	         	<cfcatch type='database'>
				 	<cftransaction action="rollback">
	                <cfset Result.SUCCESS = false>
					<cfset Result.TipoError = "Error en la consulta de base de datos.">
	                <cfset Result.MESSAGE = "Error al recuperar los datos => #cfcatch.Detail#">    
	                <cfreturn Result>
	            </cfcatch>
	        </cftry>
       	</cftransaction>
    </cffunction>

    <!---Funcion que genera OC para traspasos---------->
    <!---Desarrollador: Miguel Tiznado -------------------------------->
    <!---Fecha: 21/07/2015--------------------------------------------->
    <cffunction name="RSInserta_ordenCompra_transferencia" access="public" returntype="struct">
		<cfargument name='id_Empresa' 		  type='numeric' required='yes' default='0'>
		<cfargument name="id_Obra"        	  type="string" required="true">
		<cfargument name="id_empleado"    	  type="string" required="true">
		<cfargument name="id_Almacen"     	  type="string" required="true">
		<cfargument name="id_Requisicion"     type="string" required="true">
		<cfargument name="id_Proveedor"		  type="string"	required="true">
		<cfif id_Requisicion EQ ''>
			<cfset id_Requisicion='NULL'>
		</cfif>
		<cfif id_Almacen EQ ''>
			<cfset id_Almacen='NULL'>
		</cfif>
		<cftransaction>
			<cfquery name="inserta_OC" datasource="#cnx#" username='#user_sql#' password='#password_sql#'>
				EXECUTE [Indirectos_Generar_OC]
							@id_Empresa		= #id_Empresa#,
							@id_Obra		= #id_Obra#,
							@id_Requisicion	= #id_Requisicion#,
							@id_Proveedor	= #id_Proveedor#,
							@id_empleado	= #id_empleado#,
							@id_Almacen		= #id_Almacen#
			</cfquery>
		</cftransaction>
		<cfset RS.OK=TRUE>
		<cfset RS.id_OrdenCompra=inserta_OC.id_OrdenCompra>
	<cfreturn RS>
	</cffunction>

	<cffunction name="RSInserta_ordenCompradetalle_transferencia" access="public" returntype="struct">
		<cfargument name='id_Empresa' type='numeric' required='yes' default='0'>
		<cfargument name="id_OrdenCompra"        type="string" required="true">
		<cfargument name="id_Obra"        type="string" required="true">
		<cfargument name="id_empleado"    type="string" required="true">
		<cfargument name="id_insumo"     type="string" required="true">
		<cfargument name="im_Precio"     type="string" required="true">
		<cfargument name="cantidad"     type="string" required="true">
	
		<cftransaction>
				
			<cfquery name="inserta_OCD" datasource="#cnx#" username='#user_sql#' password='#password_sql#'>
				DECLARE @nd_OrdenCompra INT
				SET @nd_OrdenCompra = (select isnull(max(nd_OrdenCompra)+1,1) from OrdenesCompraDetalle
				where id_Empresa = #id_Empresa# and id_Obra = #id_Obra# and id_OrdenCompra = #id_OrdenCompra#)	
				
				INSERT INTO OrdenesCompraDetalle
					([id_Empresa],[id_Obra],[id_OrdenCompra],[nd_OrdenCompra],[id_CentroCosto],[id_Insumo],[fh_Entrega],
					[nu_Cantidad],[nu_CantidadAjustada],[im_Precio],[im_PrecioPresupuesto],[im_CantidadPrecio],[id_Marca], [id_Frente], [id_Partida], de_Detalle)
				VALUES
					(#id_Empresa#,#id_Obra#,#id_OrdenCompra#,@nd_OrdenCompra,900,'#id_Insumo#',getdate(),
					(#cantidad#),0,#im_Precio#,NULL, (#cantidad#) * #im_Precio#,NULL, 901 , 1, '')	
					
					
	
	
				SELECT * FROM OrdenesCompraDetalle WHERE id_Empresa=#id_Empresa# AND id_Obra=#id_Obra# AND id_OrdenCompra=#id_OrdenCompra# 		
			</cfquery>
			<cfquery name="requisicion" datasource="#cnx#" username='#user_sql#' password='#password_sql#'>
				SELECT id_requisicion
				FROM OrdenesCompra
				WHERE id_Empresa=#id_Empresa# AND id_Obra=#id_Obra# AND id_OrdenCompra=#id_OrdenCompra#
			</cfquery>
			<cfquery name="nd_detalle" datasource="#cnx#" username='#user_sql#' password='#password_sql#'>
				SELECT 
					ISNULL(MAX(nd_requisicion)+1,1) as nd_requisicion 
				FROM 
					Requisiciones_OrdenesCompra
				WHERE id_Empresa=#id_Empresa# AND id_Obra=#id_Obra# AND id_OrdenCompra=#id_OrdenCompra#
			</cfquery>
			<cfquery name="inserta_nu_por_comprar" datasource="#cnx#" username='#user_sql#' password='#password_sql#'>
				INSERT INTO Requisiciones_OrdenesCompra
					(
					[id_Empresa],
					[id_Obra],
					[id_OrdenCompra],
					[id_requisicion],
					[nd_requisicion],
					[id_insumo],
					[nu_comprado],
					[id_centroCosto],
					[id_Frente],
					[id_Partida],
					[im_Precio],
					[id_Estatus]
					)
				VALUES(	
					#id_Empresa#,
					#id_Obra#,
					#id_OrdenCompra#,
					#requisicion.id_requisicion#,
					#nd_detalle.nd_requisicion#,
					'#id_insumo#',
					#cantidad#,
					900,
					901,
					1,
					#im_Precio#,
					103
					)
				</cfquery>
		</cftransaction>
		<cfset RS.OK=TRUE>
		<cfset RS.QUERY=inserta_OCD>
		<cfreturn RS>
	</cffunction>
	
	<cffunction name="RS_actualizaTotales_OC" access="public" returntype="struct">
		<cfargument name='id_Empresa' type='numeric' required='yes' default='0'>
		<cfargument name="id_OrdenCompra"        type="string" required="true">
		<cfargument name="id_Obra"        type="string" required="true">
		<cftransaction>
			<cfquery name="inserta_OCD" datasource="#cnx#" username='#user_sql#' password='#password_sql#'>
				DECLARE @im_SubTotal FLOAT=(select sum(im_CantidadPrecio) from OrdenesCompraDetalle where id_OrdenCompra=#id_OrdenCompra# and id_Obra=#id_Obra# AND id_Empresa=#id_Empresa#)
	
				UPDATE 
					OrdenesCompra
				SET
					im_SubTotal=@im_SubTotal,im_Total=@im_SubTotal
				WHERE
					id_Obra=#id_Obra# AND id_Empresa=#id_Empresa# AND id_OrdenCompra=#id_OrdenCompra#	
	
				
				SELECT * from OrdenesCompraDetalle 	WHERE
					id_Obra=#id_Obra# AND id_Empresa=#id_Empresa# AND id_OrdenCompra=#id_OrdenCompra#	
					
			</cfquery>
		</cftransaction>
		<cfset RS.OK=TRUE>
		<cfset RS.QUERY=inserta_OCD>
		<cfreturn RS>
	</cffunction>

	<cffunction name="Agregar_insumo_requisicion" access="remote" returntype="struct">
		<cfargument name="id_Requisicion" 			type="string" default="" required="yes">
		<cfargument name="id_obra" 					type="string" default="" required="yes">
		<cfargument name="id_Empresa" 				type="string" default="" required="yes">
		<cfargument name="id_Insumo" 				type="string" default="" required="yes">
		<cfargument name="nu_cantidad" 				type="string" default="" required="yes">
		<cfargument name="Unidad_Medida_Especial" 	type="string" default="" required="yes">
		<cfargument name="Estatus_det"              type="string" default="" required="yes">
		<cftry>
			<cfquery name="TipoPresupuesto" datasource="#cnx#" username='#user_sql#' password='#password_sql#'>
	            SELECT 
	              COUNT(*) as contador 
	            FROM 
	              ExplosionInsumos e 
	              INNER JOIN insumos i on e.id_insumo=i.id_insumo
	              INNER JOIN Agrupadores_fe A on a.id_Agrupador=i.id_Agrupador
	            WHERE id_empresa=#id_Empresa# and id_Obra=#id_obra#
			</cfquery>

			<cfif #TipoPresupuesto.contador# GT 1>
				
				<cfquery name="RSCC" datasource="#cnx#" username='#user_sql#' password='#password_sql#'>
					SELECT TOP 1
					    EI.id_CentroCosto,EI.id_Frente,EI.id_Partida
					 FROM 
							ExplosionInsumos EI
				        LEFT JOIN RequisicionesDetalle RD   ON 
							Ei.id_Obra=RD.id_Obra AND 
							Ei.id_Empresa=RD.id_Empresa AND 
							RD.id_CentroCosto=EI.id_CentroCosto AND 
							RD.id_Frente=EI.id_Frente AND 
							RD.id_Partida=EI.id_Partida AND 
							EI.id_Insumo=RD.id_insumo
					WHERE 
						EI.id_Insumo='#id_Insumo#' and Ei.id_Obra=#id_obra# and Ei.id_Empresa=#id_Empresa# 
						AND EI.nu_Cantidad > ISNULL((select SUM(nu_cantidad) 
													 from RequisicionesDetalle 
													 where id_Empresa=RD.id_Empresa and id_Obra=Rd.id_Obra and 
													       id_CentroCosto=Ei.id_CentroCosto and id_Frente=Ei.id_Frente and
															id_Partida=Ei.id_Partida and id_Insumo=Ei.id_Insumo AND
															id_estatus <> 102
													),0)

	
				</cfquery>
				
				<cfif #RSCC.recordCount# EQ 0>
					<cfquery name="RSCC" datasource="#cnx#" username='#user_sql#' password='#password_sql#'>
						SELECT TOP 1
						    EI.id_CentroCosto,EI.id_Frente,EI.id_Partida
						 FROM 
   							ExplosionInsumos EI
					        LEFT JOIN RequisicionesDetalle RD   ON 
								Ei.id_Obra=RD.id_Obra AND 
								Ei.id_Empresa=RD.id_Empresa AND 
								RD.id_CentroCosto=EI.id_CentroCosto AND 
								RD.id_Frente=EI.id_Frente AND 
								RD.id_Partida=EI.id_Partida AND 
								EI.id_Insumo=RD.id_insumo
						WHERE 
							EI.id_Insumo='#id_Insumo#' and Ei.id_Obra=#id_obra# and Ei.id_Empresa=#id_Empresa#
					</cfquery>
					<cfif #RSCC.recordCount# EQ 0>
						<cfquery name="Agrupador" datasource="#cnx#" username='#user_sql#' password='#password_sql#'>
							SELECT 
								distinct I.id_Agrupador
							 from 
								insumos I inner join Agrupadores_fe A	ON
								I.id_Agrupador=i.id_Agrupador
							 where I.id_Insumo='#id_Insumo#'
						</cfquery>
						<cfquery name="RSCC" datasource="#cnx#" username='#user_sql#' password='#password_sql#'>
							SELECT TOP 1
							    EI.id_CentroCosto,EI.id_Frente,EI.id_Partida
							 FROM 
       							ExplosionInsumos EI
						        LEFT JOIN RequisicionesDetalle RD   ON 
									Ei.id_Obra=RD.id_Obra AND 
									Ei.id_Empresa=RD.id_Empresa AND 
									RD.id_CentroCosto=EI.id_CentroCosto AND 
									RD.id_Frente=EI.id_Frente AND 
									RD.id_Partida=EI.id_Partida AND 
									EI.id_Insumo=RD.id_insumo
							WHERE 
								EI.id_Insumo='#Agrupador.id_agrupador#' and Ei.id_Obra=#id_obra# and Ei.id_Empresa=#id_Empresa# 
								AND EI.nu_Cantidad > ISNULL((select SUM(nu_cantidad) 
															 from RequisicionesDetalle 
															 where id_Empresa=RD.id_Empresa and id_Obra=Rd.id_Obra and 
															       id_CentroCosto=Ei.id_CentroCosto and id_Frente=Ei.id_Frente and
																	id_Partida=Ei.id_Partida and id_Insumo=Ei.id_Insumo  AND
																	id_estatus <> 102
															),0)
						</cfquery>
						<cfif #RSCC.recordCount# EQ 0>
							<cfquery name="RSCC" datasource="#cnx#" username='#user_sql#' password='#password_sql#'>

								SELECT TOP 1
								    EI.id_CentroCosto,EI.id_Frente,EI.id_Partida
								 FROM 
	       							ExplosionInsumos EI
							        LEFT JOIN RequisicionesDetalle RD   ON 
										Ei.id_Obra=RD.id_Obra AND 
										Ei.id_Empresa=RD.id_Empresa AND 
										RD.id_CentroCosto=EI.id_CentroCosto AND 
										RD.id_Frente=EI.id_Frente AND 
										RD.id_Partida=EI.id_Partida AND 
										EI.id_Insumo=RD.id_insumo
								WHERE 
									EI.id_Insumo='#Agrupador.id_agrupador#' and Ei.id_Obra=#id_obra# and Ei.id_Empresa=#id_Empresa#
							</cfquery>
						</cfif>
					</cfif>
				</cfif>
				<cfquery name="RS_Detalles" datasource="#cnx#" username='#user_sql#' password='#password_sql#'>
				 	Requisiciones_Guardar_Detalle  #id_Requisicion#, #id_obra#,#id_Empresa#, '#id_Insumo#', #nu_cantidad#, '#Replace(Unidad_Medida_Especial,"'","")#',#Estatus_det# ,#RSCC.id_CentroCosto#,#RSCC.id_Frente#,#RSCC.id_Partida#    
				</cfquery>	
			<cfelse>
				<cfquery name="RS_Detalles" datasource="#cnx#" username='#user_sql#' password='#password_sql#'>
				 	Requisiciones_Guardar_Detalle  #id_Requisicion#, #id_obra#,#id_Empresa#, '#id_Insumo#', #nu_cantidad#, '#Replace(Unidad_Medida_Especial,"'","")#',#Estatus_det# ,900,901,1    
				</cfquery>
			</cfif>
			<cfif not isDefined( 'RS_Detalles' )>
				<cfset mensaje='El insumo ya existe en esta requisicion!'>
				<cfset repetido=true>
			<cfelse>
				<cfset mensaje='El insumo ha sido agregado correctamente!'>
				<cfset repetido=false>
			</cfif>
				<cfset RS.OK=true>
				<cfset RS.repetido=repetido>
				<cfset RS.mensaje=mensaje>
				<cfreturn RS>
			<cfcatch type='database'>
				<cfset RS.OK=false>
				<cfset RS.mensaje='ha ocurrido un error en el servidor'>
				<cfreturn RS>				
			</cfcatch>
		</cftry>
	</cffunction> 
	
	<cffunction name="get_Indirectos_Montos" access="remote" returntype="struct" returnformat="JSON" >
        <cfargument name="id_Empresa" 			type="string" required="true">
        <cfargument name="id_Obra" 			type="string" required="true">
		<cftry>
			<cfset Result.SQL = "
				EXECUTE Indirectos_Montos 
							@id_Empresa = #id_Empresa#,
							@id_Obra = #id_Obra#
				">
            <cfquery name="RSDatos" datasource="#session.cnx#">                
				#PreserveSingleQuotes(Result.SQL)#
            </cfquery>
            <cfif IsDefined('RSDatos.RecordCount') AND RSDatos.RecordCount GT 0>
	            <cfset Result.SUCCESS = true>
	            <cfset Result.MESSAGE = 'Datos obtenidos con exito'>
	            <cfset Result.RS = RSDatos>
	            <cfreturn result>
			<cfelse>
				<cfset Result.SUCCESS = false>
	            <cfset Result.MESSAGE = 'Sin datos disponibles'>
	            <cfreturn result>
			</cfif>	
         	<cfcatch type='database'>
                <cfset Result.SUCCESS = false>
				<cfset Result.TipoError = "Error en la consulta de base de datos.">
                <cfset Result.MESSAGE = "Error al recuperar los datos => #cfcatch.Detail#">    
                <cfreturn Result>
            </cfcatch>
        </cftry>
    </cffunction>
    
	<!---Funcion para obtener el listado de Obras de tipo Indirecto---->
    <!---Desarrollador: Miguel Tiznado -------------------------------->
    <!---Fecha: 16/07/2015--------------------------------------------->
	<cffunction name="get_listado_Obras_Indirectos" access="public" returntype="struct">
        <cftry>
            <cfquery name="RSDatos" datasource="#session.cnx#">                
				SELECT 
					id_Obra, de_Obra
				FROM 
					Obras
				WHERE
					id_TipoObra = 0 --Tipo de Obra de Indirectos
				GROUP BY
					id_Obra,
					de_Obra
            </cfquery>
            <cfset Result.SUCCESS = true>
            <cfset Result.MESSAGE = 'Datos obtenidos con exito'>
            <cfset Result.RS = RSDatos>
            <cfreturn result>
         	<cfcatch type='database'>
                <cfset Result.SUCCESS = false>
				<cfset Result.TipoError = "Error en la consulta de base de datos.">
                <cfset Result.MESSAGE = "Error al recuperar los datos => #cfcatch.Detail#">    
                <cfreturn Result>
            </cfcatch>
        </cftry>
    </cffunction>
	
	
	<!---Funcion para obtener descripcion de Obra-------------------->
    <!---Desarrollador: Miguel Tiznado ------------------------------>
    <!---Fecha: 16/07/2015------------------------------------------->
	<cffunction name="get_de_Obra" access="remote" returntype="string">
        <cfargument name="id_Obra" 	type="string" required="true">
        <cftry>
            <cfquery name="RSDatos" datasource="#session.cnx#">                
				SELECT 
					de_Obra
				FROM 
					Obras
				WHERE
					id_Obra = #id_Obra#
					AND id_TipoObra != 0
				GROUP BY
					id_Obra,
					de_Obra
            </cfquery>
            <cfreturn RSDatos.de_Obra>
         	<cfcatch type='any'>
                <cfreturn ''>
            </cfcatch>
        </cftry>
    </cffunction>
    
    <!---Funcion para obtener Obras con Montos Indirectos------------>
    <!---Desarrollador: Miguel Tiznado ------------------------------>
    <!---Fecha: 21/07/2015------------------------------------------->
    <cffunction name="get_Indirectos_Montos_Obras_Listado" access="public" returntype="struct">
        <cfargument name="id_Empresa" 		type="string" required="false">
        <cfargument name="id_Obra" 			type="string" required="false">
		<cftry>
			<cfset Result.SQL = "
				EXECUTE Indirectos_Montos_Obras_Listado 
							@id_Empresa 	= #IIF(id_Empresa NEQ '', 	id_Empresa, DE('NULL'))#,
							@id_Obra 		= #IIF(id_Obra NEQ '', 		id_Obra, 	DE('NULL'))#
				">
            <cfquery name="RSDatos" datasource="#session.cnx#">                
				#PreserveSingleQuotes(Result.SQL)#
            </cfquery>
            <cfset Result.SUCCESS = true>
            <cfset Result.MESSAGE = 'Datos obtenidos con exito'>
            <cfset Result.RS = RSDatos>
            <cfreturn result>
			<cfcatch type='database'>
                <cfset Result.SUCCESS = false>
				<cfset Result.TipoError = "Error en la consulta de base de datos.">
                <cfset Result.MESSAGE = "Error al recuperar los datos => #cfcatch.Detail#">    
                <cfreturn Result>
            </cfcatch>
        </cftry>
    </cffunction>
    
    <!---Funcion para Ajustar Montos Indirectos---------------------->
    <!---Desarrollador: Miguel Tiznado ------------------------------>
    <!---Fecha: 21/07/2015------------------------------------------->
    <cffunction name="set_Indirectos_Montos_Ajustar" access="remote" returntype="struct" returnformat="JSON" >
        <cfargument name="id_Empresa" 		type="string" required="true">
        <cfargument name="id_Obra" 			type="string" required="true">
		<cfargument name="im_Monto" 		type="string" required="true">
		<cfargument name="id_Empleado" 		type="string" required="true">
		<cftransaction>
			<cftry>
				<cfset Result.SQL = "
					EXECUTE Indirectos_Montos_Ajustar 
								@id_Empresa 	= #id_Empresa#,
								@id_Obra 		= #id_Obra#,
								@im_Monto		= #im_Monto#,  
								@id_Empleado	= #id_Empleado#  
					">
	            <cfquery name="RSDatos" datasource="#session.cnx#">                
					#PreserveSingleQuotes(Result.SQL)#
	            </cfquery>
	            <cfset Result.SUCCESS = true>
	            <cfset Result.MESSAGE = 'La operaci&oacute;n se realiz&oacute; correctamente.'>
	            <cfreturn result>
				<cfcatch type='database'>
					<cftransaction action="rollback">
	                <cfset Result.SUCCESS = false>
					<cfset Result.TipoError = "Error en la consulta de base de datos.">
	                <cfset Result.MESSAGE = "Error al recuperar los datos => #cfcatch.Detail#">    
	                <cfreturn Result>
	            </cfcatch>
	        </cftry>
		</cftransaction>
    </cffunction>

	<!---Funcion para obtener el listado de Generadores-------------->
    <!---Desarrollador: Miguel Tiznado ------------------------------>
    <!---Fecha: 09/03/2015------------------------------------------->
	<cffunction name="get_Presupuestos_Indirectos_Reporte" access="public" returntype="struct">
        <cfargument name="id_EmpresaOrigen" 	type="string" required="false" default="NULL">
		<cfargument name="id_EmpresaDestino" 	type="string" required="false" default="NULL">
		<cfargument name="id_ObraOrigen" 		type="string" required="false" default="NULL">
		<cfargument name="id_ObraDestino" 		type="string" required="false" default="NULL">
		<cfargument name="id_Empleado"			type="string" required="false" default="NULL">
		<cfargument name="fh_Inicio" 			type="string" required="true">
		<cfargument name="fh_Fin" 				type="string" required="true">
		<cfset id_EmpresaOrigen = IIF(id_EmpresaOrigen NEQ '', 	id_EmpresaOrigen, 	DE('NULL'))>
		<cfset id_EmpresaDestino = IIF(id_EmpresaDestino NEQ '', id_EmpresaDestino, DE('NULL'))>
		<cfset id_ObraOrigen 	= IIF(id_ObraOrigen NEQ '', 	id_ObraOrigen, 		DE('NULL'))>
		<cfset id_ObraDestino 	= IIF(id_ObraDestino NEQ '', 	id_ObraDestino, 	DE('NULL'))>
		<cfset id_Empleado 		= IIF(id_Empleado NEQ '', 		id_Empleado, 		DE('NULL'))>
		<cftry>
			<cfset Result.SQL = "
				SELECT 
					p.*,
					emp_origen.nb_Empresa AS nb_EmpresaOrigen,
					o_origen.de_Obra AS de_ObraOrigen,
					emp_destino.nb_Empresa AS nb_EmpresaDestino,
					o_destino.de_Obra AS de_ObraDestino,
					UPPER(emp.nb_Nombre + ' ' + emp.nb_ApellidoPaterno + ' ' + emp.nb_ApellidoMaterno) AS de_Empleado
				FROM 
					Presupuestos_Indirectos p
				LEFT JOIN Empresas emp_origen
					ON emp_origen.id_Empresa = p.id_Empresaorigen
				LEFT JOIN Obras o_origen
					ON o_origen.id_Empresa = p.id_Empresaorigen
					AND o_origen.id_Obra = p.id_ObraOrigen
				INNER JOIN Empresas emp_destino
					ON emp_Destino.id_Empresa = p.id_EmpresaDestino
				INNER JOIN Obras o_destino
					ON o_destino.id_Empresa = p.id_EmpresaDestino
					AND o_destino.id_Obra = p.id_ObraDestino
				INNER JOIN Empleados emp
					ON emp.id_Empleado = p.id_EmpleadoRegistro
				WHERE
					ISNULL(p.id_EmpresaOrigen,-1) = ISNULL( #id_EmpresaOrigen#, ISNULL(p.id_EmpresaOrigen,-1))
					AND p.id_EmpresaDestino = ISNULL( #id_EmpresaDestino#, p.id_EmpresaDestino)
					AND ISNULL(p.id_ObraOrigen,-1) = ISNULL( #id_ObraOrigen#, ISNULL(p.id_ObraOrigen,-1))
					AND p.id_ObraDestino = ISNULL( #id_ObraDestino#, p.id_ObraDestino)
					AND p.fh_Registro BETWEEN '#fh_Inicio# 00:00' AND '#fh_Fin# 23:59:59'
				ORDER BY 
					p.fh_Registro
			">
            <cfquery name="RSDatos" datasource="#session.cnx#">                
				#PreserveSingleQuotes(Result.SQL)#
            </cfquery>
            <cfset Result.SUCCESS = true>
            <cfset Result.MESSAGE = 'Datos obtenidos con exito'>
            <cfset Result.RS = RSDatos>
            <cfreturn result>
         	<cfcatch type='database'>
                <cfset Result.SUCCESS = false>
				<cfset Result.TipoError = "Error en la consulta de base de datos.">
                <cfset Result.MESSAGE = "Error al recuperar los datos => #cfcatch.Detail#">    
                <cfreturn Result>
            </cfcatch>
        </cftry>
    </cffunction>
    
	<cffunction name="Indirectos_Montos_Listado" access="public" returntype="struct">
        <cfargument name="id_EmpresaOrigen" 	type="string" required="false" default="NULL">
		<cfargument name="id_EmpresaDestino" 	type="string" required="false" default="NULL">
		<cfargument name="id_ObraOrigen" 		type="string" required="false" default="NULL">
		<cfargument name="id_ObraDestino" 		type="string" required="false" default="NULL">
        
        <cfif id_EmpresaOrigen EQ ''>
        	<cfset id_EmpresaOrigen = 'NULL'>		
		</cfif>
        <cfif id_EmpresaDestino EQ ''>
        	<cfset id_EmpresaDestino = 'NULL'>		
		</cfif>
        <cfif id_ObraOrigen EQ ''>
        	<cfset id_ObraOrigen = 'NULL'>		
		</cfif>
        <cfif id_ObraDestino EQ ''>
        	<cfset id_ObraDestino = 'NULL'>		
		</cfif>

		<cftry>
            <cfquery name="RSDatos" datasource="#session.cnx#">                
				Presupuestos_Indirectos_Listado #id_EmpresaOrigen#, #id_EmpresaDestino#, #id_ObraOrigen#, #id_ObraDestino#
            </cfquery>
			<cfset Result.listado= TRUE>
            <cfset Result.tipoError= ''>
            <cfset Result.rs= RSDatos>
            <cfset Result.mensaje= 'Se obtuvo el listado correctamente'>
            <cfreturn Result>
            <cfcatch type='database'>
                <cfset Result.listado= FALSE>
                <cfset Result.tipoError= 'database-indefinido'>
                <cfset Result.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
                <cfreturn Result>
            </cfcatch>
        </cftry>
    </cffunction>
</cfcomponent>