<cfcomponent extends='conexion'>
	


	<cffunction name="RsInsumoPedido" access="remote" returntype="query" returnFormat="json">
	
    <cfargument name="id_Insumo" type="string" required="yes">
    
    <cfquery name="RsInsumo" datasource='#cnx#' username='#user_sql#' password='#password_sql#'>
    	SELECT 
            i.id_Insumo,
            i.de_Insumo,
            u.de_UnidadMedida,
            (select SUM(nu_cantidad) from ExplosionInsumos where id_Insumo='#id_Insumo#') as presupuestdo
        FROM
            Insumos i
        INNER JOIN UnidadesMedida u 
            ON u.id_UnidadMedida = i.id_UnidadMedida
        WHERE i.id_Insumo = '#id_Insumo#'
    </cfquery>
    
    <cfreturn RsInsumo>
    
</cffunction>

	<cffunction name='RSMostrarPorIDInsumos' access='public' returntype='struct'>

	<cfargument name='id_Insumo' type='string' required='yes'>
    
	<cftry>
		<cfquery datasource='#cnx#' name='RS_MostrarInsumos' username='#user_sql#' password='#password_sql#'>
			EXECUTE bop_Insumos_ObtenerPorID '#id_Insumo#'
		</cfquery>
		<cfset Insumos.listado= TRUE>
		<cfset Insumos.tipoError= ''>
		<cfset Insumos.rs= RS_MostrarInsumos>
		<cfset Insumos.mensaje= 'Se obtuvo el recordset de Insumos correctamente'>
		<cfreturn Insumos>
		<cfcatch type='database'>
			<cfset Insumos.listado= FALSE>
			<cfset Insumos.tipoError= 'database-indefinido'>
			<cfset Insumos.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn Insumos>
		</cfcatch>
	</cftry>
</cffunction>
<cffunction name='RSMostrarInsumosFiltrado' access='public' returntype='struct'>
	<cfargument name='id_Insumo' 	type='string' default=''>
	<cfargument name='de_Insumo' 	type='string' default=''>
	<cfargument name='id_Agrupador' type='string' default=''>
	<cfargument name='id_TipoPieza' type='string' default=''>
	<cfargument name='sn_Activo' 	type='string' default=''>
	<cftry>
		<cfquery datasource='#cnx#' name='RS_MostrarInsumos'>
			SELECT
				id_Insumo, de_Insumo, de_UnidadMedida, de_TipoInsumo,de_GrupoInsumo, de_SubGrupoInsumo, I.sn_Universal,
				T.id_TipoInsumo,I.id_GrupoInsumo, I.id_SubGrupoInsumo, I.sn_EnviarACompras,I.sn_Activo,i.id_TipoPieza,Tp.de_TipoPieza,
				i.id_Agrupador,A.de_Agrupador
			FROM
				Insumos I 
			INNER JOIN 
				TiposInsumo T 
				ON i.id_TipoInsumo=T.id_TipoInsumo 
			INNER JOIN 
				GruposInsumos G 
				ON i.id_GrupoInsumo=G.id_GrupoInsumo 
				AND i.id_TipoInsumo=G.id_TipoInsumo
			INNER JOIN SubGruposInsumos S 
				ON i.id_SubGrupoInsumo=S.id_SubGrupoInsumo 
				AND i.id_TipoInsumo=S.id_TipoInsumo 
				AND s.id_GrupoInsumo=i.id_GrupoInsumo
			LEFT JOIN 
				Agrupadores_fe A 
				ON i.id_Agrupador=A.id_Agrupador
			LEFT JOIN TipoPiezas_fe TP 
				ON i.id_TipoPieza=TP.id_TipoPieza 
			LEFT JOIN UnidadesMedida U 
				ON i.id_UnidadMedida=u.id_UnidadMedida
			WHERE 
				i.id_TipoInsumo NOT IN (8,9)
			<cfif #id_Insumo# NEQ ''>
				AND  I.id_Insumo like '%#id_Insumo#%'
            </cfif>
			<cfif #de_Insumo# NEQ ''>
				AND I.de_Insumo LIKE '%#de_Insumo#%'
			</cfif>		
			<cfif #id_Agrupador# NEQ ''>
				AND I.id_Agrupador='#id_Agrupador#'
			</cfif>
			<cfif #id_TipoPieza# NEQ ''>
				AND I.id_TipoPieza=#id_TipoPieza#
			</cfif>
			<cfif #sn_Activo# NEQ ''>
				AND I.sn_Activo = #sn_Activo#
			</cfif>
			ORDER BY I.id_Insumo
				<!---G.id_GrupoInsumo, S.id_SubGrupoInsumo, I.id_Insumo--->
		</cfquery>
		<cfset Insumos.listado= TRUE>
		<cfset Insumos.tipoError= ''>
		<cfset Insumos.rs= RS_MostrarInsumos>
		<cfset Insumos.mensaje= 'Se obtuvo el recordset de Insumos correctamente'>
		<cfreturn Insumos>
		<cfcatch type='database'>
			<cfset Insumos.listado= FALSE>
			<cfset Insumos.tipoError= 'database-indefinido'>
			<cfset Insumos.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn Insumos>
		</cfcatch>
	</cftry>
</cffunction>

<cffunction name='RSMostrarInsumosFiltradoJSON' access='remote' returntype='struct' returnformat="JSON" >
	<cfargument name="id_Empresa"   type="string" default="">
	<cfargument name="id_Obra"      type="string" default="">
	<cfargument name='id_Insumo' 	type='string' default=''>
	<cfargument name='de_Insumo' 	type='string' default=''>
	<cfargument name='id_Agrupador' type='string' default=''>
	<cfargument name='id_TipoPieza' type='string' default=''>
	<cfargument name='sn_Activo' 	type='string' default=''>
   
	<cftry>

		
		<cfquery datasource='#cnx#' name='RS_MostrarInsumos'>
			
		 	SELECT 
				i.id_Insumo,i.de_Insumo, UM.de_UnidadMedida,i.id_Agrupador,A.de_Agrupador
			FROM
				Insumos I 
				INNER JOIN Agrupadores_fe A ON i.id_Agrupador=A.id_Agrupador
				INNER JOIN ExplosionInsumos_agrupadores_fe EA ON EA.id_Agrupador=i.id_Agrupador
				LEFT JOIN UnidadesMedida UM ON I.id_UnidadMedida=UM.id_UnidadMedida
		
			WHERE 1=1
				
				<cfif #id_Empresa# NEQ ''>
					AND EA.id_Empresa=#id_Empresa#
					

				</cfif>
				<cfif #id_Agrupador# NEQ ''>
					AND I.id_Agrupador='#id_Agrupador#'
					

				</cfif>
				<cfif #id_TipoPieza# NEQ ''>
					AND I.id_TipoPieza=#id_TipoPieza#
				
				</cfif>
				<cfif #sn_Activo# NEQ ''>
					AND I.sn_Activo = #sn_Activo#
				</cfif>
				<cfif #id_obra# NEQ ''>
					AND EA.id_obra = #id_obra#
				</cfif>
				<cfif #id_Insumo# NEQ ''>
					AND ( I.id_Insumo like '%#id_Insumo#%'
                </cfif>
				<cfif #de_Insumo# NEQ ''>
					OR I.de_Insumo LIKE '%#de_Insumo#%')
				</cfif>		

				
				        
			ORDER BY I.de_Insumo
				
		</cfquery>

		
		
		<cfset Insumos.listado= TRUE>
		<cfset Insumos.rs= RS_MostrarInsumos>
	
		<cfset Insumos.mensaje= 'Se obtuvo el recordset de Insumos correctamente'>
		<cfreturn Insumos>
		<cfcatch type='database'>
			<cfset Insumos.listado= FALSE>
			<cfset Insumos.tipoError= 'database-indefinido'>
			<cfset Insumos.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn Insumos>
		</cfcatch>
	</cftry>
</cffunction>
<cffunction name='RSMostrarInsumosFiltradoJSONAgrupador' access='remote' returntype='struct' returnformat="JSON" >
	<cfargument name="id_Empresa"   type="string"  default="">
	<cfargument name="id_Obra"      type="string"  default="">
	<cfargument name='id_Insumo' 	type='string'  default=''>
	<cfargument name='de_Insumo' 	type='string'  default=''>
	<cfargument name='id_Agrupador' type='string'  default=''>
	<cfargument name='id_TipoPieza' type='string'  default=''>
	<cfargument name='sn_Activo' 	type='string'  default=''>
   	<cfargument name="opcion"       type='numeric' default="0" require="YES">
	<cftry>

		
		<cfif #opcion# EQ 1>
			
			<cfquery datasource='#cnx#' name='RS_MostrarInsumos'>
					
				 	SELECT 
						i.id_Insumo,i.de_Insumo, UM.de_UnidadMedida,i.id_Agrupador,A.de_Agrupador,SUM(EI.nu_cantidad) as nu_cantidad,SUM(EI.im_SubTotal) as im_subtotal
					FROM
						ExplosionInsumos ei 
						inner join insumos i on ei.id_Insumo=i.id_Insumo
						INNER JOIN Agrupadores_fe A ON i.id_Agrupador=A.id_Agrupador
						left JOIN UnidadesMedida UM on i.id_UnidadMedida=UM.id_UnidadMedida

				
					WHERE i.id_TipoInsumo in(1,3)
						<cfif #id_Agrupador# NEQ ''>
							AND I.id_Agrupador='#id_Agrupador#'
							

						</cfif>
						<cfif #id_obra# NEQ ''>
							AND EI.id_obra='#id_Obra#'
							

						</cfif>
					
						<cfif #sn_Activo# NEQ ''>
							AND I.sn_Activo = #sn_Activo#
						</cfif>
						<cfif #id_Insumo# NEQ ''>
							AND ( I.id_Insumo like '%#id_Insumo#%'
		                </cfif>
						<cfif #de_Insumo# NEQ ''>
							OR I.de_Insumo LIKE '%#de_Insumo#')
						</cfif>		
					
					GROUP BY i.id_Insumo,i.de_Insumo, UM.de_UnidadMedida,i.id_Agrupador,A.de_Agrupador	
					ORDER BY I.de_Insumo
					 	
					
					
				
						
				</cfquery>

		
		<cfelse>	
		
			<cfquery datasource='#cnx#' name='RS_MostrarInsumos'>
			
		 	SELECT 
				i.id_Insumo,i.de_Insumo, UM.de_UnidadMedida,i.id_Agrupador,A.de_Agrupador
			FROM
				Insumos I 
				INNER JOIN UnidadesMedida UM ON I.id_UnidadMedida=UM.id_UnidadMedida
				INNER JOIN Agrupadores_fe A ON i.id_Agrupador=A.id_Agrupador
				
		
			WHERE 1=1
				
				
				<cfif #id_Agrupador# NEQ ''>
					AND I.id_Agrupador='#id_Agrupador#'
					

				</cfif>
				<cfif #id_TipoPieza# NEQ ''>
					AND I.id_TipoPieza=#id_TipoPieza#
				
				</cfif>
				<cfif #sn_Activo# NEQ ''>
					AND I.sn_Activo = #sn_Activo#
				</cfif>
				
				<cfif #id_Insumo# NEQ ''>
					AND ( I.id_Insumo like '%#id_Insumo#%'
                </cfif>
				<cfif #de_Insumo# NEQ ''>
					OR I.de_Insumo LIKE '%#de_Insumo#%')
				</cfif>		

				
				        
			ORDER BY I.de_Insumo
				
		</cfquery>
			
		</cfif>		

				
		
		<cfset Insumos.listado= TRUE>
		<cfset Insumos.rs= RS_MostrarInsumos>
	
		<cfset Insumos.mensaje= 'Se obtuvo el recordset de Insumos correctamente'>
		<cfreturn Insumos>
		<cfcatch type='database'>
			<cfset Insumos.listado= FALSE>
			<cfset Insumos.tipoError= 'database-indefinido'>
			<cfset Insumos.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn Insumos>
		</cfcatch>
	</cftry>
</cffunction>
<cffunction name='RSMostrarInsumosFiltradoJSONMixto' access='remote' returntype='struct' returnformat="JSON" >
	<cfargument name="id_Empresa"   type="string" default="">
	<cfargument name="id_Obra"      type="string" default="">
	<cfargument name='id_Insumo' 	type='string' default=''>
	<cfargument name='de_Insumo' 	type='string' default=''>
	<cfargument name='id_Agrupador' type='string' default=''>
	<cfargument name='id_TipoPieza' type='string' default=''>
	<cfargument name='sn_Activo' 	type='string' default=''>
   
	<cftry>

		<cfquery datasource='#cnx#' name='RsTipo'>
      		select 
      			count(*) as tipo 
      		from 
      			Obras 
      		where 
      			id_tipoobra=0 AND id_Empresa=#id_Empresa# AND id_obra=#id_obra#

      	</cfquery>


      	<cfif #RsTipo.tipo# Eq 1>

			<cfquery datasource="#cnx#" name="RS_MostrarInsumos">
					SELECT 
						replace(aux.id_Insumo,' ', '') as id_Insumo,aux.de_Insumo,aux.de_UnidadMedida,
						'GAO-001' as id_Agrupador,'INDIRECTOS' as de_Agrupador
					FROM(
						SELECT 
							i.id_insumo as id_insumo,i.de_insumo,UM.de_UnidadMedida,
							A.id_Agrupador,A.de_Agrupador
							
						FROM 
							insumos i 
							INNER JOIN Agrupadores_fe A on a.id_Agrupador=i.id_Agrupador
							INNER JOIN UnidadesMedida UM ON I.id_UnidadMedida=UM.id_UnidadMedida
							
						WHERE 
							I.id_TipoInsumo in(1,3)
				
						GROUP BY I.id_insumo,i.de_insumo,UM.de_UnidadMedida,A.id_Agrupador,A.de_Agrupador) aux
					
					WHERE 1=1 
					
						AND aux.id_Agrupador LIKE '%#id_Agrupador#%'
						

					
				
						AND ( aux.id_Insumo like '%#id_Insumo#%'
	            
				
						OR aux.de_Insumo LIKE '%#de_Insumo#%')
			
			</cfquery>

		<cfelse>	
			<cfquery datasource='#cnx#' name='RS_MostrarInsumos'>
			
		 		SELECT 
					replace(aux.id_Insumo,' ', '') as id_Insumo,aux.de_Insumo,aux.de_UnidadMedida,aux.id_Agrupador,aux.de_Agrupador
				FROM(SELECT 
						a.id_Agrupador as id_Insumo,a.de_Agrupador as de_Insumo,UM.de_UnidadMedida,
						a.id_Agrupador,a.de_Agrupador,
						sum(nu_Cantidad) as nu_Cantidad, 
						sum(im_SubTotal) as im_SubTotal 
					FROM 
						ExplosionInsumos e
						INNER JOIN Insumos I ON I.id_Insumo=e.id_Insumo
						INNER JOIN Agrupadores_fe a ON a.id_Agrupador =i.id_Insumo
						INNER JOIN UnidadesMedida UM ON I.id_UnidadMedida=UM.id_UnidadMedida
						
					WHERE 
				  
				  		e.id_Obra = #id_Obra# AND I.id_TipoInsumo in(1,3)
					GROUP BY
						a.id_Agrupador,a.de_Agrupador,UM.de_UnidadMedida
					UNION
						SELECT 
						e.id_insumo as id_insumo,i.de_insumo,UM.de_UnidadMedida,
						A.id_Agrupador,A.de_Agrupador,
						sum(nu_Cantidad) as nu_Cantidad, 
						sum(im_SubTotal) as im_SubTotal 
					FROM 
						ExplosionInsumos e 
						INNER JOIN insumos i on e.id_insumo=i.id_insumo
						INNER JOIN Agrupadores_fe A on a.id_Agrupador=i.id_Agrupador
						INNER JOIN UnidadesMedida UM ON I.id_UnidadMedida=UM.id_UnidadMedida
						
					WHERE 
						e.id_Empresa = #id_Empresa# AND
						e.id_Obra = #id_Obra# AND I.id_TipoInsumo in(1,3)
			
					GROUP BY e.id_insumo,i.de_insumo,UM.de_UnidadMedida,A.id_Agrupador,A.de_Agrupador) aux
				
				WHERE 1=1 
				<cfif #id_Agrupador# NEQ ''>
					AND aux.id_Agrupador LIKE '%#id_Agrupador#%'
					

				</cfif>
				<cfif #id_Insumo# NEQ ''>
					AND ( aux.id_Insumo like '%#id_Insumo#%'
                </cfif>
				<cfif #de_Insumo# NEQ ''>
					OR aux.de_Insumo LIKE '%#de_Insumo#%')
				</cfif>		

				
				        
				ORDER BY aux.id_Insumo
				
			</cfquery>
      		
      	</cfif>
            
		<cfset Insumos.listado= TRUE>
		<cfset Insumos.rs= RS_MostrarInsumos>
	
		<cfset Insumos.mensaje= 'Se obtuvo el recordset de Insumos correctamente'>
		<cfreturn Insumos>
		<cfcatch type='database'>
			<cfset Insumos.listado= FALSE>
			<cfset Insumos.tipoError= 'database-indefinido'>
			<cfset Insumos.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn Insumos>
		</cfcatch>
	</cftry>
</cffunction>


<cffunction name="obtenerCampos" access="public" returntype="struct">
	<cfargument name='id_Insumo' type='string' required='yes'>
	
	<cftry>
		<cfquery name="campos" datasource="#cnx#">
			SELECT 
				id_Agrupador,id_TipoPieza
			FROM 
				insumos
			WHERE 
				id_insumo='#id_insumo#'




		</cfquery>
	
	<cfset Insumos.id_Agrupador=#campos.id_Agrupador# >
	<cfset Insumos.id_TipoPieza=#campos.id_TipoPieza#>
	<cfreturn Insumos>	
	
	<cfcatch type="database">
			
		<cfset Insumos.error= 'No ha podido cargar los datos'>
		<cfset Insumos.mensaje='error al cargar los datos'>
		<cfreturn Insumos>	
  </cfcatch>

	</cftry>


</cffunction>


<cffunction name='obtenerAgrupadores' access='public' returntype='query'>
	<cfquery name="agrupadores" datasource="#cnx#">
		SELECT
			id_Agrupador,de_Agrupador
		FROM
			Agrupadores_FE	

	</cfquery>
	<cfreturn agrupadores>


</cffunction>

<cffunction name='obtenerPiezas' access='public' returntype='query'>
	<cfquery name="piezas" datasource="#cnx#">
		SELECT 
			id_TipoPieza,de_TipoPieza 
		FROM 
			TipoPiezas_fe

	</cfquery>
	<cfreturn piezas>


</cffunction>
<!---  SELECT MAX(CONVERT(INT, REPLACE(id_Insumo, 'FE', '')))+1 as new
 from insumos 
 where id_TipoPieza is not null --->
 
<cffunction name='RSAgregarInsumos' access='public' returntype='struct'>
	<cfargument name='id_Insumo' type='string' required='yes' default='0'>
	<cfargument name='de_Insumo' type='string' required='yes' default=''>
	<cfargument name='id_UnidadMedida' type='numeric' required='yes' default='0'>
	<cfargument name='id_Agrupador' type='string' required='yes' default='0'>
	<!--- <cfargument name='id_TipoPieza' type='numeric' required='yes' default='0'> --->
	<cfargument name='id_TipoInsumo' type='numeric' required='yes' default='0'>
	<cfargument name='id_GrupoInsumo' type='numeric' required='yes' default='0'>
	<cfargument name='id_SubGrupoInsumo' type='numeric' required='no' default='0'>
	<cfargument name='sn_Activo' type='boolean' required='yes' default='1'>
	<cfargument name='sn_Universal' type='boolean' required='yes' default='0'>
    <cfargument name='sn_EnviarACompras' type="boolean" required="yes" default="0">
    <cfargument name='marcas' type='string' required='no' default=''>
    <cfargument name='compradores' type='string' required='no' default=''>
    <cfargument name="listaMarcas" 			type="string" required="false">
    <cfargument name="listaCompradores" 	type="string" required="false">
    <cfargument name="listaProveedores" 	type="string" required="false">

    
	<cftry>
    	<cfset arrayMarcas = ArrayNew(1)>
        <cfset arrayMarcas = ListToArray(marcas, ',', false)>
        
        <cfset arrayCompradores = ArrayNew(1)>
        <cfset arrayCompradores = ListToArray(compradores, ',', false)>
       

        <cfquery datasource='#cnx#' name='RSAgregar' username='#user_sql#' password='#password_sql#'>
			EXECUTE bop_Insumos_Agregar_FE '#id_Insumo#', '#de_Insumo#','#id_Agrupador#',NULL,#id_UnidadMedida#,#id_TipoInsumo#, #id_GrupoInsumo#, #id_SubGrupoInsumo#, #sn_Activo#, #sn_Universal#, #sn_EnviarACompras#
		</cfquery>
    
        <cfif IsDefined('listaMarcas') AND listaMarcas NEQ ''>
            <cfset aux = ArrayNew(1)>
            <cfset aux = ListToArray( listaMarcas )>
            
         
        </cfif>
  
        <cfif isDefined('listaCompradores') AND listaCompradores NEQ ''>
            <cfset aux = ArrayNew(1)>
            <cfset aux = ListToArray( listaCompradores )>
                
            <!--- <cfloop array=#aux# index="i">
                <cfquery name="comprador" datasource='#cnx#'>						
                        INSERT INTO InsumosCompradores VALUES( '#id_Insumo#', #i#, 1, GETDATE(), 	#Session.id_empleado# )
                </cfquery>
            </cfloop> --->
        </cfif>

        <cfif isDefined('listaProveedores') AND listaProveedores NEQ ''>
            <cfset aux = ArrayNew(1)>
            <cfset aux = ListToArray( listaProveedores )>
                
         <!---    <cfloop array=#aux# index="i">
                <cfquery name="proveedor" datasource='#cnx#'>						
                        INSERT INTO InsumosProveedores VALUES( '#id_Insumo#', #i#, 1, GETDATE(), 	#Session.id_empleado# )	                    	
                </cfquery>
            </cfloop> --->
        </cfif>
        
		<!---         <cfif ArrayLen(arrayMarcas) GT 0>
        	
            <cfloop array=#arrayMarcas# index="i">
            	<cfquery name='RsInsumoMarca' datasource='#cnx#' username='#user_sql#' password='#password_sql#'>
                	EXECUTE bop_Insumos_Agregar_Marca #id_Insumo#, #i#, #Session.id_Usuario#
                </cfquery>
            </cfloop>
            
		</cfif>
        
        <cfif ArrayLen(arrayCompradores) GT 0>
        	<cfloop array=#arrayCompradores# index="i">
            	<cfquery name='RsInsumoMarca' datasource='#cnx#' username='#user_sql#' password='#password_sql#'>
                	EXECUTE bop_Insumos_Agregar_Comprador #id_Insumo#, #i#, #Session.id_Usuario#
                </cfquery>
            </cfloop>
		</cfif> --->
                                
		<!---Log--->
		<!--- <cf_log datasource='#cnx#' username='#user_sql#' password='#password_sql#' id_Empresa="#Session.id_Empresa#" id_Obra="#Session.id_Obra#" id_Usuario="#Session.id_Usuario#" id_MenuOpcion="#Session.id_MenuOpcion#">
			<cfoutput>
				EXECUTE bop_Insumos_Agregar_FE '#id_Insumo#', '#de_Insumo#',#id_Agrupador#,#id_TipoPieza#,#id_UnidadMedida#,#id_TipoInsumo#, #id_GrupoInsumo#, #id_SubGrupoInsumo#, #sn_Activo#, #sn_Universal#, #sn_EnviarACompras#
			</cfoutput>
		</cf_log>
 --->		<!---Termina Log --->
		<cfset Insumos.agregado= TRUE>
		<cfset Insumos.tipoError= ''>
		<cfset Insumos.mensaje= 'Insumo guardado correctamente'>
		<cfreturn Insumos>
		<cfcatch type='database'>
			<cfset Insumos.agregado= FALSE>
			<cfif cfcatch.NativeErrorCode EQ '2627'>
				<cfset Insumos.tipoError= 'database-registro_duplicado'>
				<cfset Insumos.mensaje= 'El Insumo no se pudo guardar porque ya existe'>
			<cfelse>
				<cfset Insumos.tipoError= 'database-indefinido'>
				<cfset Insumos.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			</cfif>
			<cfreturn Insumos>
		</cfcatch>
	</cftry>
</cffunction>

<cffunction name='RSEditarInsumos' access='public' returntype='struct'>
	<cfargument name='id_Insumo' type='string' required='yes'>
	<cfargument name='de_Insumo' type='string' required='yes' default=''>
	<cfargument name='id_UnidadMedida' type='numeric' required='yes' default='0'>
	<cfargument name='id_Agrupador' type='string' required='yes' default='0'>
	<!--- <cfargument name='id_TipoPieza' type='numeric' required='yes' default='0'> --->
	<cfargument name='id_TipoInsumo' type='numeric' required='yes' default='0'>
	<cfargument name='id_GrupoInsumo' type='numeric' required='yes' default='0'>
	<cfargument name='id_SubGrupoInsumo' type='numeric' required='no' default='0'>
	<cfargument name='sn_Activo' type='boolean' required='yes' default='0'>
	<cfargument name='sn_Universal' type='boolean' required='yes' default='0'>
   	<cfargument name='sn_EnviarACompras' type='boolean' required='yes' default='0'>
	<cftry>
		<cfquery datasource='#cnx#' name='RSFilasAfectadas' username='#user_sql#' password='#password_sql#'>
			EXECUTE bop_Insumos_Actualizar_FE '#id_Insumo#', '#de_Insumo#', #id_UnidadMedida#,'#id_Agrupador#', #id_TipoInsumo#, #id_GrupoInsumo#, #id_SubGrupoInsumo#, #sn_Activo#, #sn_Universal#, #sn_EnviarACompras#
		</cfquery>
		<!---Log--->
		<!--- <cf_log datasource='#cnx#' username='#user_sql#' password='#password_sql#' id_Empresa="#Session.id_Empresa#" id_Obra="#Session.id_Obra#" id_Usuario="#Session.id_Usuario#" id_MenuOpcion="#Session.id_MenuOpcion#">
			<cfoutput>
				EXECUTE bop_Insumos_Actualizar '#id_Insumo#', '#de_Insumo#', #id_UnidadMedida#, #id_TipoInsumo#, #id_GrupoInsumo#, #id_SubGrupoInsumo#, #sn_Activo#, #sn_Universal#, #sn_EnviarACompras#
			</cfoutput>
		</cf_log> --->
		<!---Termina Log --->
		
		<cfset Insumos.actualizado= TRUE>
		<cfset Insumos.filasAfectadas= RSFilasAfectadas.filasAfectadas>
		<cfset Insumos.tipoError= ''>
		<cfset Insumos.mensaje= 'Insumo actualizado correctamente'>
		<cfreturn Insumos>
		<cfcatch type='database'>
			<cfset Insumos.actualizado= FALSE>
			<cfset Insumos.tipoError= 'database-indefinido'>
			<cfset Insumos.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn Insumos>
		</cfcatch>
	</cftry>
</cffunction>

<cffunction name='RSIdInsumos' access='public' returntype='struct'>
		
	<cftry>

			<cfquery datasource='#cnx#' name='RSNuevo' username='#user_sql#' password='#password_sql#'>
					
				 SELECT 
				 	MAX(CONVERT(INT, REPLACE(id_Insumo, 'FE', '')))+1 as id_insumo
				 FROM 
				 	insumos 
				 
				 



			</cfquery>
			
			<cfset nuevo='FE'&RSNuevo.id_Insumo>
			<cfset Insumos.id_insumo= nuevo>
			<cfset Insumos.mensaje='id_Insumo creado'>
			<cfreturn Insumos>	
			
		<cfcatch type="database">
			
			<cfset Insumos.error= 'No se ha podido el id'>
			<cfset Insumos.mensaje='error al crear id_insumo'>
			<cfreturn Insumos>	
		</cfcatch>

	</cftry>		
	


</cffunction>
<cffunction name='RSMostrarInsumosParecidosEnDescripcion' access='public' returntype='struct'>
	<cfargument name="de_Insumo" type="string" required="yes">

	<cftry>
		<cfquery datasource='#cnx#' name='RS_MostrarInsumosParecidos' username='#user_sql#' password='#password_sql#'>
			EXECUTE Insumos_ObtenerDescripcionesParecidas '#de_Insumo#'
		</cfquery>
		<cfset Insumos.listado= TRUE>
		<cfset Insumos.tipoError= ''>
		<cfset Insumos.rs= RS_MostrarInsumosParecidos>
		<cfset Insumos.mensaje= 'Se obtuvo el recordset de Insumos correctamente'>
		<cfreturn Insumos>
		<cfcatch type='database'>
			<cfset Insumos.listado= FALSE>
			<cfset Insumos.tipoError= 'database-indefinido'>
			<cfset Insumos.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
			<cfreturn Insumos>
		</cfcatch>
	</cftry>
</cffunction>

<cffunction name='RSEliminarInsumos' access='public' returntype='struct'>

	<cfargument name='id_Insumo' type='string' required='yes'>

	<cftry>
		<cfquery datasource='#cnx#' name='RSFilasAfectadas' username='#user_sql#' password='#password_sql#'>
			EXECUTE bop_Insumos_Eliminar '#id_Insumo#'
		</cfquery>
		<!---Log--->
		<cf_log datasource='#cnx#' username='#user_sql#' password='#password_sql#' id_Empresa="#Session.id_Empresa#" id_Obra="#Session.id_Obra#" id_Usuario="#Session.id_Usuario#" id_MenuOpcion="#Session.id_MenuOpcion#">
			<cfoutput>
				EXECUTE bop_Insumos_Eliminar '#id_Insumo#'
			</cfoutput>
		</cf_log>
		<!---Termina Log --->
		<cfset Insumos.eliminado= TRUE>
		<cfset Insumos.filasAfectadas= RSFilasAfectadas.filasAfectadas>
		<cfset Insumos.tipoError= ''>
		<cfset Insumos.mensaje= 'Insumo eliminado correctamente'>
		<cfreturn Insumos>
		<cfcatch type='database'>
			<cfset Insumos.eliminado= FALSE>
			<cfif cfcatch.NativeErrorCode EQ '547'>
				<cfset Insumos.mensaje= 'El Insumo no se pudo eliminar ya que tiene registros relacionados'>
				<cfset Insumos.tipoError= 'database-integridad'>
			<cfelse>
				<cfset Insumos.mensaje= 'Error en el acceso a base de datos(#cfcatch.Detail#)'>
				<cfset Insumos.tipoError= 'database-indefinido'>
			</cfif>
			<cfreturn Insumos>
		</cfcatch>
	</cftry>
</cffunction>

<cffunction name="RS_InsumosDesglosado" access="remote">
	<cfargument name='id_obra' type='string' required='yes'>
	<cfargument name='id_agrupador' type='string' required='yes'>

		<cfquery name="desglose" datasource="#cnx#">
			 
				SELECT A.id_Agrupador,A.de_Agrupador,i.id_Insumo,i.de_Insumo,UM.de_UnidadMedida,nu_Cantidad as CantPresupuestada,
				EI.im_InsumoPrecio as PrecioPresupuesto,SUM(EI.im_SubTotal) as Presupuestado,Ei.nu_CompradoContratado as CantComprada,
				EI.im_InsumoPrecioActual as PrecioCompra,
				SUM(EI.im_AplicadoEjecutado) as Comprado,
				SUM(EI.im_SubTotal)-SUM(EI.im_AplicadoEjecutado) as Disponible
				FROM ExplosionInsumos EI 
					INNER JOIN insumos I on I.id_Insumo=EI.id_Insumo
					left JOIN Agrupadores_fe A on I.id_Agrupador=A.id_Agrupador
					INNER JOIN UnidadesMedida UM on I.id_UnidadMedida=UM.id_UnidadMedida

				WHERE id_TipoPieza is not null	
				
				<cfif #id_obra# NEQ ''>
					 AND id_Obra=#id_obra#

				</cfif>	
				<cfif #id_agrupador# NEQ ''>
					AND A.id_agrupador='#id_agrupador#' 
					

				</cfif>
					
				GROUP BY
					A.id_Agrupador,A.de_Agrupador,i.id_Insumo,i.de_Insumo,UM.de_UnidadMedida,nu_Cantidad,EI.Im_insumoPrecio,
					Ei.nu_CompradoContratado,EI.im_InsumoPrecioActual	
		</cfquery>
		<cfset json='['>

		
		<cfset aux=0>
		<cfoutput query="desglose" group="de_Agrupador">
			<cfset aux=aux+1>

			<cfquery name="Agrupadores" dbtype="query">
				select de_agrupador
				from desglose
				group by de_agrupador

			</cfquery>
		<!--- 	<cfoutput>
		
				Agrupadores:#Agrupadores.recordcount#	

			</cfoutput>
			<br> --->
			<cfquery name="cont" dbtype="query">
				select id_insumo
				from desglose
				where de_agrupador='#de_Agrupador#'

			</cfquery>
			<!--- <cfoutput>
				agrupador:#de_agrupador#
				insumos:#cont.recordcount#	

			</cfoutput>
			<br>
		 ---> 
		 <cfquery name="Agrupados" dbtype="query">
		 	select de_agrupador,SUM(Presupuestado) as presupuestado,SUM(Comprado) as comprado
		 	from desglose
			where de_agrupador='#de_Agrupador#'
			Group by de_agrupador
		 	

		 </cfquery>
		 	<cfset contador=0>
			<cfset json=json&'{"fila":"'&aux&'","Agrupador":"'&desglose.de_Agrupador&'","presupuestado":'&agrupados.presupuestado&',"comprado":'&agrupados.comprado&',"insumos":['>
			<cfoutput group="id_insumo">
				<cfset contador=contador+1>
				<cfif #contador# EQ #cont.recordcount#>
					<cfset json=json&'{"id_insumo":"'&id_insumo&'","de_insumo":"'&de_insumo&'","de_Unidad":"'&de_UnidadMedida&'","cant_presupuestada":'&CantPresupuestada&',"precio_presupuesto":'&PrecioPresupuesto&',"presupuestado":'&presupuestado&',"cant_comprada":'&CantComprada&',"precio_compra":'&PrecioCompra&',"comprado":'&comprado&',"disponible":'&disponible&'}]'>
		
				<cfelse>	
					<cfset json=json&'{"id_insumo":"'&id_insumo&'","de_insumo":"'&de_insumo&'","de_Unidad":"'&de_UnidadMedida&'","cant_presupuestada":'&CantPresupuestada&',"precio_presupuesto":'&PrecioPresupuesto&',"presupuestado":'&presupuestado&',"cant_comprada":'&CantComprada&',"precio_compra":'&PrecioCompra&',"comprado":'&comprado&',"disponible":'&disponible&'},'>
			

				</cfif>

				
			</cfoutput>

			<cfif #aux# EQ #Agrupadores.recordcount#>
				<cfset json=json&'}'>
			<cfelse>
				<cfset json=json&'},'>
			</cfif>


			
		</cfoutput>

		<cfset json=json&']'>
		<cfoutput>
			#json#
		</cfoutput>

</cffunction>
<cffunction name="RSPresupuestoXAgrupador" access="remote" returntype="query" returnformat="JSON">
	<cfargument name="id_obra"      type="numeric" required="true">
	<cfargument name="Id_Agrupador" type="numeric" required="true">

	<cfquery name="Presupuesto" datasource="#cnx#">
		SELECT 
			nu_cantidad,im_precio 
		FROM 
			explosioninsumos_agrupadores_fe 
		WHERE 
			id_obra=#id_obra# and id_agrupador='#id_agrupador#'
			


	</cfquery>
	<cfreturn Presupuesto> 



</cffunction>
</cfcomponent>