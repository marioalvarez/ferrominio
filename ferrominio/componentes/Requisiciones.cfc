<cfcomponent extends='conexion'>
	<!---Guarda una requisicion cargada desde exel------------------->
    <!---Desarrollador: Miguel Tiznado ------------------------------>
    <!---Fecha: 15/02/2016------------------------------------------->
	<cffunction name="RequsicionesExcel_Enviar" access="public" returntype="struct">
        <cfargument name="id_Empresa" 				type="string" required="true">
        <cfargument name="id_Obra" 					type="string" required="true">
        <cfargument name="Json_Details" 			type="string" required="true">
        <cftransaction>
	        <cftry>
	        	<!--- Guardamos el encabezado de la requisicion --->
	            <cfset Result.SQL = "
	            		EXECUTE Requisiciones_Guardar          
								 @id_Obra  				= #id_Obra#,            
								 @id_Empresa  			= #id_Empresa# ,            
								 @id_Empleado  			= #Session.id_Empleado#,            
								 @de_Comentario 		= 'REQUISICION CARGADA DESDE EXCEL',            
								 @de_DomicilioBodega 	= '',            
								 @nb_Recibe   			= '',            
								 @de_Telefono  			= '',          
								 @id_Estatus 			= 203,        
								 @id_tipoRequisicion 	= 9,
								 @id_Requisicion 		= NULL

					">
				<cfquery name="RSDatos" datasource="#cnx#">                
	                #PreserveSingleQuotes(Result.SQL)#
	            </cfquery>
	            <cfset Result.id_Requisicion = RSDatos.id_Requisicion>


	            <!--- Generamos el script para los detalles --->
	            <cfset RequisicionesDetalle = deserializeJSON(Json_Details)>
	            <cfset Result.SQLDetails = "">
				<cfloop array="#RequisicionesDetalle#" index="Detail">
					<cfset Result.SQLDetails &= "
				            		EXECUTE Requisiciones_Guardar_Detalle_Generador          
											 @id_Empresa			= #id_Empresa#,
											 @id_Obra				= #id_Obra#,
											 @id_Requisicion		= #Result.id_Requisicion#,
											 @id_CentroCosto		= 900,
											 @id_Frente				= 901,
											 @id_Partida			= 1,
											 @id_Insumo				='#Detail.id_Insumo#',
											 @nu_Cantidad			= #Detail.nu_cantidad#,
											 @de_MedidaEspecial		='#UCASE(Detail.de_MedidaEspecial)#',
											 @nu_Tramos				= #Detail.nu_Tramos#,
											 @nu_LongitudCom		= #Detail.nu_LongitudCom# 
				            	">
				</cfloop>
				<cfquery name="RSDetails" datasource="#cnx#">                
	                #PreserveSingleQuotes(Result.SQLDetails)#
	            </cfquery>
	            <cftransaction action="commit">
	            <cfset Result.RS = RSDatos>
	            <cfset Result.SUCCESS = true>
	            <cfset Result.MESSAGE = 'La operaci&oacute;n se realiz&oacute; correctamente.'>
	            <cfset Result.TipoError = ''>
	            <cfreturn result>
	         	<cfcatch type='database'>
	         		<cftransaction action="rollback">
	         		<cfset Result.SUCCESS = false>
					<cfset Result.TipoError = "Error en la consulta de base de datos.">
	                <cfset Result.MESSAGE = "Error al recuperar los datos => #cfcatch.Detail#">    
	                <cfreturn Result>
	            </cfcatch>
	        </cftry>
        </cftransaction>
    </cffunction>


	<!---Lista el encabezado de una requisicion --------------------->
    <!---Desarrollador: Miguel Tiznado ------------------------------>
    <!---Fecha: 24/12/2015------------------------------------------->
	<cffunction name="RequsicionesEncabezado_Listar" access="public" returntype="struct">
        <cfargument name="id_Empresa" 				type="string" required="true">
        <cfargument name="id_Obra" 					type="string" required="true">
        <cfargument name="id_Requisicion" 			type="string" required="true">
        <cftry>
            <cfset Result.SQL = "
            		EXECUTE RequsicionesEncabezado_Listar
							@id_Empresa					= #id_Empresa#,
							@id_Obra 					= #id_Obra#,
							@id_Requisicion 			= #id_Requisicion#
				">
            <cfquery name="RSDatos" datasource="#cnx#">                
                #PreserveSingleQuotes(Result.SQL)#
            </cfquery>
            <cfset Result.RS = RSDatos>
            <cfset Result.SUCCESS = true>
            <cfset Result.MESSAGE = 'OK'>
            <cfset Result.TipoError = ''>
            <cfreturn result>
         	<cfcatch type='database'>
                <cfset Result.SUCCESS = false>
				<cfset Result.TipoError = "Error en la consulta de base de datos.">
                <cfset Result.MESSAGE = "Error al recuperar los datos => #cfcatch.Detail#">    
                <cfreturn Result>
            </cfcatch>
        </cftry>
    </cffunction>

    <!---Lista el encabezadp de una requisiciones ------------------->
    <!---Desarrollador: Miguel Tiznado ------------------------------>
    <!---Fecha: 24/12/2015------------------------------------------->
	<cffunction name="RequsicionesDetalle_Listar" access="public" returntype="struct">
        <cfargument name="id_Empresa" 				type="string" required="true">
        <cfargument name="id_Obra" 					type="string" required="true">
        <cfargument name="id_Requisicion" 			type="string" required="true">
        <cftry>
            <cfset Result.SQL = "
            		EXECUTE RequsicionesDetalle_Listar
							@id_Empresa					= #id_Empresa#,
							@id_Obra 					= #id_Obra#,
							@id_Requisicion 			= #id_Requisicion#
				">
            <cfquery name="RSDatos" datasource="#cnx#">                
                #PreserveSingleQuotes(Result.SQL)#
            </cfquery>
            <cfset Result.RS = RSDatos>
            <cfset Result.SUCCESS = true>
            <cfset Result.MESSAGE = 'OK'>
            <cfset Result.TipoError = ''>
            <cfreturn result>
         	<cfcatch type='database'>
                <cfset Result.SUCCESS = false>
				<cfset Result.TipoError = "Error en la consulta de base de datos.">
                <cfset Result.MESSAGE = "Error al recuperar los datos => #cfcatch.Detail#">    
                <cfreturn Result>
            </cfcatch>
        </cftry>
    </cffunction>

	<!---Funcion para generar una requisicion desde un archivo de Excel--------->
	<!---Desarrollador: Miguel Tiznado ----------------------------------------->
	<!---Fecha: 21/12/2015------------------------------------------------------>
	<cffunction name="Upload_Excel" access="public" returntype="Struct">
		<cfargument name="fileField" 		type="string" required="true">
		<cfset Result.SUCCESS = true>
		<cfset Result.TipoError = ''>
		<cfset Result.MESSAGE = ''>
		<cftry>
			<cfset UploadDir = "#ExpandPath('../anexos/requisiciones_excel/')#">
			<!--- Crear directorio si no existe--->
			<cfif not DirectoryExists(UploadDir)>
				<cfdirectory action="create" directory="#UploadDir#">
			</cfif>
			
			<!---Subir el archivo de Excel --->
			<cffile action="upload" fileField="#fileField#" destination="#UploadDir#"  nameConflict="makeunique" result="Upload">
			
			<cfset fileDir = UploadDir & '\' & Upload.serverFile>

				
			<!---Instanciamos el componente POIUtility--->
			<!---Nota:
						- El componente debe estar ubicado en la misma carpeta 
						  donde esta el archivo que usa esta funcion--->
			<cfset objPOI = CreateObject("component", "POIUtility" ).Init() />

			<!---Regresa un arreglo de estructuras con los datos de cada hoja del libro de excel--->
			<cfset arrSheetsE = objPOI.ReadExcel(FilePath = #fileDir#,HasHeaderRow = FALSE) />

			<!---Obtenemos el recorset de la lectura de la primera hoja de excel--->
			<cfset QueryExcel = arrSheetsE[1].QUERY>
			<cfset Result.RSexcel_Original = QueryExcel>
			
			<!--- Obtenemos los valores de los registros --->
			<cfset Result.Detalles = ArrayNew(1)>
			<cfif QueryExcel.RecordCount GTE 2>
				<!--- Recorremos el query--->
				<cfloop from="2" to="#QueryExcel.RecordCount#" index="i">
					<!--- Validar que no sea una fila vacia --->
					<cfif NOT (	Trim(QueryExcel.Column1[i]) EQ '' AND 
								Trim(QueryExcel.Column2[i]) EQ '' AND 
								Trim(QueryExcel.Column3[i]) EQ '' AND 
								Trim(QueryExcel.Column4[i]) EQ '' AND 
								Trim(QueryExcel.Column5[i]) EQ '')>
						<!--- Obtenemos lo valores sin espacios en blanco--->
						<cfset RequisicionDetalle = StructNew() >
						<cfset RequisicionDetalle.id_Insumo 		= Replace(Trim(QueryExcel.Column1[i]), " ", "", "ALL")>
						<cfset RequisicionDetalle.nu_Cantidad 		= Replace(Trim(QueryExcel.Column2[i]), " ", "", "ALL")>
						<cfset RequisicionDetalle.de_MedidaEspecial = Trim(QueryExcel.Column3[i])>
						<cfset RequisicionDetalle.nu_Tramos 		= Replace(Trim(QueryExcel.Column4[i]), " ", "", "ALL")>
						<cfset RequisicionDetalle.nu_LongitudCom 	= Replace(Trim(QueryExcel.Column5[i]), " ", "", "ALL")>

						<!--- Quitamos signos especiales--->
						<cfset RequisicionDetalle.nu_Cantidad 		= Replace(RequisicionDetalle.nu_Cantidad, ",", "", "ALL")>
						<cfset RequisicionDetalle.nu_Cantidad 		= Replace(RequisicionDetalle.nu_Cantidad, "$", "", "ALL")>
						<cfset RequisicionDetalle.nu_Tramos 		= Replace(RequisicionDetalle.nu_Tramos, ",", "", "ALL")>
						<cfset RequisicionDetalle.nu_Tramos 		= Replace(RequisicionDetalle.nu_Tramos, "$", "", "ALL")>
						<cfset RequisicionDetalle.nu_LongitudCom 	= Replace(RequisicionDetalle.nu_LongitudCom, ",", "", "ALL")>
						<cfset RequisicionDetalle.nu_LongitudCom 	= Replace(RequisicionDetalle.nu_LongitudCom, "$", "", "ALL")>

						<cfset RequisicionDetalle.nu_Tramos 		= IIF(IsNumeric(RequisicionDetalle.nu_Tramos), 			RequisicionDetalle.nu_Tramos, 		DE('NULL') )>
						<cfset RequisicionDetalle.nu_LongitudCom	= IIF(IsNumeric(RequisicionDetalle.nu_LongitudCom), 	RequisicionDetalle.nu_LongitudCom, 	DE('NULL') )>

						<cfif (RequisicionDetalle.id_Insumo EQ '') OR
							(RequisicionDetalle.nu_Cantidad EQ '') OR
							(NOT IsNumeric(RequisicionDetalle.nu_Cantidad)) OR
							(RequisicionDetalle.nu_Cantidad EQ 0)>
							<!---El registro es erroneo--->
							<cfset Result.SUCCESS = false>
							<cfset Result.MESSAGE = "El documento tiene registros erroneos.">
							<cfset Result.TipoError &= "<br />- El registro #i# es incorrecto.">
						<cfelse>
							<cfset ArrayAppend(Result.Detalles, RequisicionDetalle)>
						</cfif>
					</cfif>
				</cfloop>
			<cfelse>
				<cfset Result.SUCCESS = false>
				<cfset Result.TipoError = "Error al leer el archivo de Excel.">
                <cfset Result.MESSAGE = "El documento no tiene registros.">
			</cfif>


			<cfif Result.SUCCESS> 
				<!---Si entra a esta condicion significa que el documento no tiene errores en la estructura de los datos (por tanto ahora revisaremos la validez de los datos)
					Realizamos una consulta para determinar que insumos no son validos en el documento--->
	    		<cfset SQLDetailErrors = "SELECT i.id_Insumo FROM (">
	    		<cfloop array="#Result.Detalles#" index="Detail">
	    			<cfset SQLDetailErrors &= " SELECT '#Detail.id_Insumo#' AS id_Insumo UNION">
	    		</cfloop>
				<cfset SQLDetailErrors = MID( #SQLDetailErrors#, 1, LEN( #SQLDetailErrors# ) - 5 )><!--- Quita el ultimo 'UNION' --->
	    		<cfset SQLDetailErrors &= ') i EXCEPT SELECT id_Insumo FROM Insumos (NOLOCK) WHERE sn_Activo = 1'>
	    		<cfquery name="RSDetallesErrores" datasource="#session.cnx#">                
					#PreserveSingleQuotes(SQLDetailErrors)#
	            </cfquery>
	            <cfif RSDetallesErrores.RecordCount GT 0>
	            	<cfset Result.SUCCESS = false>
	            	<cfset Result.TipoError = "Insumos invalidos.">
                	<cfset Result.MESSAGE &= "El documento contiene insumos invalidos.">
	            	<cfset Result.RSErrores = RSDetallesErrores>
	            </cfif>
            </cfif>

            <cfif Result.SUCCESS>
        		<!--- Si entra a esta condicion significa que los insumos son validos en el documento --->
        		<cfset SQLDetailErrors = "SELECT i.*, ins.de_Insumo, um.de_UnidadMedida FROM (">
	    		<cfloop array="#Result.Detalles#" index="Detail">
	    			<cfset SQLDetailErrors &= " SELECT 
	    											'#Detail.id_Insumo#' AS id_Insumo, 
	    											'#Detail.nu_Cantidad#' AS nu_Cantidad, 
	    											'#Detail.de_MedidaEspecial#' as de_MedidaEspecial,
	    											'#Detail.nu_Tramos#' as nu_Tramos,
	    											'#Detail.nu_LongitudCom#' as nu_LongitudCom
	    										UNION">
	    		</cfloop>
				<cfset SQLDetailErrors = MID( #SQLDetailErrors#, 1, LEN( #SQLDetailErrors# ) - 5 )><!--- Quita el ultimo 'UNION' --->
	    		<cfset SQLDetailErrors &= ') i 
				INNER JOIN Insumos ins (NOLOCK) ON ins.id_Insumo = i.id_Insumo
				INNER JOIN UnidadesMedida um ON um.id_UnidadMedida = ins.id_UnidadMedida'>
	    		<cfquery name="RSDetalles" datasource="#session.cnx#">                
					#PreserveSingleQuotes(SQLDetailErrors)#
	            </cfquery>
	            <cfset Result.RS = RSDetalles>
	            <cfreturn Result>
            </cfif>
    		
        	<!--- Capturamos los errores para mostrarlos en pantalla --->
            <cfcatch type="any">
				<cfset Result.SUCCESS = false>
				<cfset Result.TipoError = "Error al leer el archivo de Excel.">
                <cfset Result.MESSAGE &= "Error Fatal => #cfcatch.Detail#">
                <cfreturn Result>
			</cfcatch>
		</cftry>
		<cfreturn Result>
	</cffunction>



	<!---Funcion para eliminar borradores de requisicion que no tengan detalles---------->
    <!---Desarrollador: Miguel Tiznado ----------------------------------------->
    <!---Fecha: 18/06/2015------------------------------------------------------>
	<cffunction name="Requisiciones_LimpiarBorrador" access="remote" returntype="string" returnformat="JSON"> 
		<cfargument name="id_Empresa" 		type="string" required="true">
		<cfargument name="id_Obra" 			type="string" required="true">
		<cfargument name="id_Requisicion" 	type="string" required="true">
		<cftry>
			<cfquery name="RS" Datasource="#Session.cnx#" >
				DECLARE @nu_CantidadDetalles INT = (SELECT 
														COUNT(*) 
													FROM 
														RequisicionesDetalle
													WHERE
														id_Empresa = #id_Empresa#
														AND id_Obra = #id_Obra#
														AND id_Requisicion = #id_Requisicion#),
						@id_EstatusRequisicion INT = 	(SELECT
															id_Estatus
														FROM 
															Requisiciones
														WHERE
															id_Empresa = #id_Empresa#
															AND id_Obra = #id_Obra#
															AND id_Requisicion = #id_Requisicion#)
				IF @nu_CantidadDetalles = 0 AND @id_EstatusRequisicion = 201
					DELETE FROM 
						Requisiciones
					WHERE 
						id_Empresa = #id_Empresa#
						AND id_Obra = #id_Obra#
						AND id_Requisicion = #id_Requisicion#
			</cfquery>
			<cfcatch type="any">
				<cfreturn 'Error'>
			</cfcatch>
		</cftry>
		<cfreturn 'Exito'>
	</cffunction>
	
	<cffunction name="RS_RequisicionesDetalles_Listado" access="remote">
		 <cfargument name="id_Empresa"   type="string" required="yes" default="">
		 <cfargument name="id_obra"      type="string" required="yes" default="">
		 <cfargument name="id_Estatus"   type="string" required="yes" default="">
		 <cfargument name="id_Agrupador" type="string" required="yes" default="">
		 <cfargument name="fh_inicio" 	 type="string" required="yes" default="">
		 <cfargument name="fh_fin"		 type="string" required="yes" default="">
		 <cfargument name="id_Empleado"  type="string" required="yes" default="">
		 <cfargument name="id_Solicitante"  type="string" required="yes" default="">
		 <cfargument name="page"         type="numeric" required="no" default="1">
		
		 <!---paginado--->
		 <cfset rows=30>
		 <cfset startRow=((page*rows)-rows)+1>
		 <cfset endRow=page*rows>
		 
	      <cfif #fh_fin# NEQ '' AND #fh_inicio# NEQ ''>
		 	
		 	 <cfif #dateformat(fh_inicio,'dd')# GT 12>
				<cfset fh_inicio = #dateformat(fh_inicio,'yyyy-mm-dd 00:00:00')#>
   			 <cfelse>
        		<cfset fh_inicio = #dateformat(fh_inicio,'yyyy-dd-mm 00:00:00')#>
    		</cfif>
		    <!---Fecha Fin--->
		    <cfif #dateformat(fh_fin,'dd')# GT 12>
				<cfset fh_fin = #dateformat(fh_fin,'yyyy-mm-dd 23:59:59')#>
		    <cfelse>
		        <cfset fh_fin = #dateformat(fh_fin,'yyyy-dd-mm 23:59:59')#>
		    </cfif>
	    	
		 </cfif>	

		
		 

	 <cfquery name="RS" datasource="#cnx#">
		 		Listado_Requisiciones 

				<cfif #id_Empresa# NEQ "">
					#id_Empresa#,
				<cfelse>
					NULL,

                </cfif>
                
                <cfif #id_obra# NEQ "">
					#id_obra#,
				<cfelse>
					NULL,
					
                </cfif>

                <cfif #id_Estatus# NEQ "">
					#id_Estatus#,
				<cfelse>
					NULL,
					
                </cfif>
                #startRow#,
                #endRow#,

				<cfif #id_Agrupador# NEQ "">
					'#id_Agrupador#',
				<cfelse>
					NULL,	
					
                </cfif>
                <cfif #fh_inicio# NEQ "">
					'#fh_inicio#',
				<cfelse>
					NULL,
					
                </cfif>
                <cfif #fh_fin# NEQ "">
					'#fh_fin#',
				<cfelse>
					NULL,
				</cfif>
                <cfif #id_Empleado# NEQ "">
					#id_Empleado#,
				<cfelse>
					NULL,
					
                </cfif>
                <cfif #id_Solicitante# NEQ "">
					#id_Solicitante#
				<cfelse>
					NULL
					
                </cfif>

		 </cfquery>

	     <cfquery name="total" datasource="#cnx#">
			 		select count(*) as total
			 		from requisiciones
			 		where id_Empresa=#id_Empresa# AND id_Obra=#id_Obra# 
			 		<cfif #id_Estatus# NEQ "">
						AND id_Estatus = #id_Estatus#
					</cfif>
			 		<cfif #id_Empleado# NEQ "">
						AND (id_EmpleadoSolicito=#id_Empleado#)
					</cfif>
               		<cfif #id_Solicitante# NEQ "">
						OR id_EmpleadoSolicito=#id_Solicitante#)
					</cfif>	
			 		<cfif #fh_inicio# NEQ "">
						AND fh_Requisicion >= '#fh_inicio#'
					</cfif>
	                <cfif #fh_fin# NEQ "">
						AND fh_Requisicion <= '#fh_fin#'
					</cfif>
		</cfquery>


			
		<cfset json='['>
		<cfset aux=0>
		<cfquery name="req" dbtype="query">
			 	select id_Requisicion
			 	from RS
				group by id_Requisicion
				
		</cfquery>
		 
		
		<cfoutput query="RS" group="id_Requisicion">

		    
		 	<cfset fecha=MID(RS.fh_Requisicion,1,10)>
		 	<cfset json =json&'{"Total":'&total.total&',"Empresa":"'&RS.nb_Empresa&'","Obra":"'&RS.de_obra&'","No_Requisicion":'&RS.id_Requisicion&',"fecha_req":"'&fecha&'","solicitante":"'&RS.nb_solicitante&'","tipo_obra":"'&RS.de_tipoObra&'","tipo_requisicion":"'&RS.de_tipoRequisicion&'","Estatus":"'&RS.de_Estatus&'","de_Comentarios":"'&replace(Ltrim(RS.de_Comentarios),'"','¦','all')&'","insumos":['>
		 	 <cfquery name="insumos" dbtype="query">
			 	select id_insumo
			 	from RS
				where id_Requisicion=#RS.id_Requisicion#
				group by id_insumo
			 </cfquery>
		 	<cfset aux1=0>
		 	<cfoutput group="id_insumo">
		 		<!--- <b>
		 		contaodr:#aux1#	
		 		id_insumo:#id_insumo#
		 		id_Requisicion:#id_Requisicion#
		 		contador:#insumos.recordCount#</b>
		 		<br> --->
		 		<cfif #id_insumo# NEQ ''>
		 			<cfset UME=''>	
		 			<cfif #RS.de_MedidaEspecial# NEQ ' '>
		 				<cfset UME=RS.de_MedidaEspecial>
		 			<cfelse>
		 				<cfset UME='N/A'>	
		 			</cfif>
		 			
		 			<cfif #aux1# EQ #insumos.recordCount#-1>
		 				<cfset json=json&'{"id":"'&RS.id_insumo&'","nombre":"'&replace(RS.de_insumo,'"','¦','all')&'","UM":"'&RS.de_UnidadMedida&'","Medida_Especial":"'&replace(Ltrim(UME),'"','¦','all')&'","cantidad":'&RS.nu_cantidad&',"comprado":'&RS.nu_comprado&'}'>
		 			<cfelse>
		 				<cfset json=json&'{"id":"'&RS.id_insumo&'","nombre":"'&replace(RS.de_insumo,'"','¦','all')&'","UM":"'&RS.de_UnidadMedida&'","Medida_Especial":"'&replace(Ltrim(UME),'"','¦','all')&'","cantidad":'&RS.nu_cantidad&',"comprado":'&RS.nu_comprado&'},'>
		 			</cfif>
		 			
		 		</cfif>	
		 			
		 			<cfset aux1 = aux1+1>
		 			

		 	</cfoutput>
		 	<cfset json=json&']'>
		 	<cfif #aux# EQ #req.recordCount#-1>
		 		<cfset json=json&'}'>
		 	<cfelse>
		 		<cfset json=json&'},'>	
		 		
		 	</cfif>
		 	<cfset aux=aux+1>

		 </cfoutput>
		 <cfset json=json&']'>
	


		 <cfoutput>
		 	#json#
		 </cfoutput>


	</cffunction>
	<cffunction name="RS_RequisicionesDetalles_Listado_rechazadas" access="remote">
		 <cfargument name="id_Empresa"   type="string" required="yes" default="">
		 <cfargument name="id_obra"      type="string" required="yes" default="">
		 <cfargument name="id_Estatus"   type="string" required="yes" default="">
		 <cfargument name="id_Agrupador" type="string" required="yes" default="">
		 <cfargument name="fh_inicio" 	 type="string" required="yes" default="">
		 <cfargument name="fh_fin"		 type="string" required="yes" default="">
		 <cfargument name="id_Empleado"  type="string" required="yes" default="">

		 <cfif #fh_fin# NEQ '' AND #fh_inicio# NEQ ''>
		 	
		 	 <cfif #dateformat(fh_inicio,'dd')# GT 12>
				<cfset fh_inicio = #dateformat(fh_inicio,'yyyy-mm-dd')#>
   			 <cfelse>
        		<cfset fh_inicio = #dateformat(fh_inicio,'yyyy-dd-mm')#>
    		</cfif>
		    <!---Fecha Fin--->
		    <cfif #dateformat(fh_fin,'dd')# GT 12>
				<cfset fh_fin = #dateformat(fh_fin,'yyyy-mm-dd')#>
		    <cfelse>
		        <cfset fh_fin = #dateformat(fh_fin,'yyyy-dd-mm')#>
		    </cfif>
	    	
		</cfif>	
		 
		
	 <cfquery name="RS" datasource="#cnx#">
		 		Listado_Requisiciones_rechazadas 

				<cfif #id_Empresa# NEQ "">
					#id_Empresa#,
				<cfelse>
					NULL,

                </cfif>
                
                <cfif #id_obra# NEQ "">
					#id_obra#,
				<cfelse>
					NULL,
					
                </cfif>

                <cfif #id_Estatus# NEQ "">
					#id_Estatus#,
				<cfelse>
					NULL,
					
                </cfif>

				<cfif #id_Agrupador# NEQ "">
					'#id_Agrupador#',
				<cfelse>
					NULL,	
					
                </cfif>
                <cfif #fh_inicio# NEQ "">
					'#fh_inicio#',
				<cfelse>
					NULL,
					
                </cfif>
                <cfif #fh_fin# NEQ "">
					'#fh_fin#',
				<cfelse>
					NULL,
					
                </cfif>
                <cfif #id_Empleado# NEQ "">
					#id_Empleado#
				<cfelse>
					NULL
					
                </cfif>


				

		 </cfquery>

		
		 
		 <cfset json='['>
		 <cfset aux=0>
		 
		 <cfquery name="req" dbtype="query">
			 	select id_Requisicion
			 	from RS
				group by id_Requisicion
		 </cfquery>

		 <cfoutput query="RS" group="id_Requisicion">
		 	<cfset fecha=MID(RS.fh_Requisicion,1,10)>
		 	<cfset json =json&'{"Empresa":"'&RS.nb_Empresa&'","Obra":"'&RS.de_obra&'","No_Requisicion":'&RS.id_Requisicion&',"fecha_req":"'&fecha&'","solicitante":"'&RS.nb_solicitante&'","tipo_obra":"'&RS.de_tipoObra&'","tipo_requisicion":"'&RS.de_tipoRequisicion&'","Estatus":"'&RS.de_Estatus&'","insumos":['>
		 	 <cfquery name="insumos" dbtype="query">
			 	select id_insumo
			 	from RS
				where id_Requisicion=#RS.id_Requisicion#
				group by id_insumo
			 </cfquery>
		 	<cfset aux1=0>
			 	

		 	<cfoutput group="id_insumo">
			 
			 		<!--- <b>
			 		contador:#aux1#	
			 		id_insumo:#id_insumo#
			 		id_Requisicion:#id_Requisicion#
			 		contador:#insumos.recordCount#</b>
			 		<br> --->
			 	
				 		<cfif #id_insumo# NEQ ''>
				 			<cfset UME=''>	
				 			<cfif #RS.de_MedidaEspecial# NEQ ' '>
				 				<cfset UME=RS.de_MedidaEspecial>
				 			<cfelse>
				 				<cfset UME='N/A'>	
				 			</cfif>
				 			
				 			<cfif #aux1# EQ #insumos.recordCount#-1>
				 				<cfset json=json&'{"id":"'&RS.id_insumo&'","nd_requisicion":'&RS.nd_requisicion&',"nombre":"'&replace(RS.de_insumo,'"','¦','all')&'","UM":"'&RS.de_UnidadMedida&'","Medida_Especial":"'&replace(UME,'"','¦','all')&'","cantidad":'&RS.nu_cantidad&'}'>
				 			<cfelse>
				 				<cfset json=json&'{"id":"'&RS.id_insumo&'","nd_requisicion":'&RS.nd_requisicion&',"nombre":"'&replace(RS.de_insumo,'"','¦','all')&'","UM":"'&RS.de_UnidadMedida&'","Medida_Especial":"'&replace(UME,'"','¦','all')&'","cantidad":'&RS.nu_cantidad&'},'>
				 			</cfif>
				 		</cfif>	
			 			
			 			<cfset aux1 = aux1+1>
			 	

			 
			</cfoutput> 	
		 	<cfset json=json&']'>
		 	<cfif #aux# EQ #req.recordCount#-1>
		 		<cfset json=json&'}'>
		 	<cfelse>
		 		<cfset json=json&'},'>	
		 		
		 	</cfif>
		 	<cfset aux=aux+1>

		 </cfoutput>
		 <cfset json=json&']'>
	


		 <cfoutput>
		 	#json#
		 </cfoutput>


	</cffunction>




	
	<cffunction name="datosRequisicion" access="remote" returntype="query">
			<cfargument name="id_obra" type="string" default="" required="yes">
			<cfargument name="id_Empresa" type="string" default="" required="yes">
			<cfargument name="id_Requisicion" type="string" default="" required="yes">

			<cftry>
				<cfquery name="RS_datos" datasource="#cnx#">
					select 
						id_empresa,id_obra,de_DatosContacto,de_Comentarios,de_DireccionBodega,nb_PersonaRecibe 
					from 
						requisiciones 
					where 
						id_obra=#id_obra# and id_requisicion=#id_Requisicion# and id_empresa=#id_Empresa#
				</cfquery>	
					
					<cfreturn RS_datos>
				<cfcatch type='database'>
					<cfset RS.OK=false>
					<cfset RS.mensaje='Ha ocurrido un error (#cfcatch.Detail#)'>
					<cfreturn RS>				
				</cfcatch>
			</cftry>
		
	
	</cffunction>
	<cffunction name="rechazarInsumoParcialmente" access="remote" returntype="struct" returnformat="JSON">
			<cfargument name="id_Empresa" 		type="string" default="" required="yes">
			<cfargument name="id_Obra" 			type="string" default="" required="yes">
			<cfargument name="id_Requisicion" 	type="string" default="" required="yes">
			<cfargument name="nd_requisicion" 	type="string" default="" required="yes">
			<cfargument name="id_insumo" 	    type="string" default="" required="yes">
			<cfargument name="nu_cantidad" 		type="string" default="" required="yes">
			<cfargument name="id_usuario" 	    type="string" default="" required="yes">

			<cftry>
		
			
				<cfquery name="RS_Actualiza_Estatus" datasource="#cnx#">
					rechazar_parcialmente @id_Empresa=#id_Empresa#,
										  @id_Obra=#id_Obra#,
										  @id_Requisicion=#id_Requisicion#,
										  @nd_requisicion=#nd_requisicion#,
										  @id_insumo='#id_insumo#',
										  @nu_rechazado=#nu_cantidad#,
										  @id_usuario=#id_usuario#
				</cfquery>	
					<cfset RS.OK=true>
					<cfset RS.mensaje='Se ha rechazado #nu_cantidad# del insumo: #id_insumo#'>
					<cfreturn RS>
				<cfcatch type='database'>
					<cfset RS.OK=false>
					<cfset RS.mensaje='Al momento de Actualizar ha ocurrido un error'>
					<cfreturn RS>				
				</cfcatch>
			</cftry>
		
	
	</cffunction>
	<cffunction name="CancelarRequisicion" access="remote" returntype="struct" returnformat="JSON">
			<cfargument name="id_obra" type="string" default="" required="yes">
			<cfargument name="id_Empresa" type="string" default="" required="yes">
			<cfargument name="id_Requisicion" type="string" default="" required="yes">

			<cftry>
				
				<cfquery name="RS_Actualiza_Estatus" datasource="#cnx#">
					Requisiciones_cancelar #id_Empresa#,#id_obra#,#id_Requisicion#
				</cfquery>	
					<cfset RS.OK=true>
					<cfset RS.mensaje='Actualizacion del estatus correctamente'>
					<cfreturn RS>
				<cfcatch type='database'>
					<cfset RS.OK=false>
					<cfset RS.mensaje='Al momento de Actualizar ha ocurrido un error'>
					<cfreturn RS>				
				</cfcatch>
			</cftry>
		
	
	</cffunction>
	<cffunction name="Verificar_disponible" access="remote" returntype="struct" returnformat="JSON">
			<cfargument name="id_obra" 			type="string" default="" required="yes">
			<cfargument name="id_Empresa"   	type="string" default="" required="yes">
			<cfargument name="id_requisicion" 	type="string" default="" required="yes">

			<cftry>
				<cfquery datasource='#cnx#' name='RsTipo'>
		      		select 
		      			count(*) as tipo 
		      		from 
		      			Obras 
		      		where 
		      			id_tipoobra=0 AND id_Empresa=#id_Empresa# AND id_obra=#id_obra#

      			</cfquery>
				<cfquery name="Rs_Insumos" datasource="#cnx#">
					SELECT 
							rd.id_Insumo, id_Agrupador
						FROM 
							RequisicionesDetalle rd
						INNER JOIN
							Insumos i ON
								i.id_Insumo = rd.id_Insumo
						WHERE 
							id_obra=#id_obra# and id_Empresa=#id_Empresa# and id_Requisicion=#id_requisicion#
				</cfquery>	

				<cfset insumos = ''>
				<cfset bandera=false>

				<cfloop query="Rs_Insumos">
						<cfquery name="valida" datasource="#cnx#">
							DECLARE @IM_PRESUPUESTO money=(
							  SELECT  
							
								ISNULL(SUM(ei.im_SubTotal),0) as im_Presupuestado
							  FROM explosioninsumos ei
							  WHERE 
								id_Empresa=#id_Empresa#
								AND id_Obra=#id_Obra#
								AND id_Insumo=CASE 
												WHEN (	
														SELECT 
															count(*) 
														FROM 
															ExplosionInsumos 
														WHERE 
															id_Empresa=#id_Empresa# AND id_Obra=#id_Obra#
															AND id_Insumo='#id_Insumo#'
													  ) =0 
												 THEN (
														  SELECT 
															id_Agrupador 
														  FROM
															Insumos
														  WHERE
															id_Insumo='#id_Insumo#'	
														)
												 ELSE
														'#id_Insumo#'	
												 END 		
							 )
							DECLARE @im_Solicitado money =(	
															SELECT 
																isnull(SUM(rd.nu_Cantidad) * (SELECT  
																								AVG(ei.im_InsumoPrecio)as im_precio
																							  FROM explosioninsumos ei
																							  WHERE 
																									id_Empresa=#id_Empresa# 
																									AND id_Obra=#id_Obra#
																									AND id_Insumo=CASE 
																												WHEN (	SELECT 
																															count(*) 
																														FROM 
																															ExplosionInsumos 
																														WHERE 
																															id_Empresa=#id_Empresa# AND id_Obra=#id_Obra#
																															AND id_Insumo='#id_insumo#'
																													  ) =0 
																												 THEN (
																														  SELECT 
																															id_Agrupador 
																														  FROM
																															Insumos
																														  WHERE
																															id_Insumo='#id_insumo#'
																														)
																												 ELSE
																														'#id_insumo#'
																												 END 		),0)
															FROM 
															RequisicionesDetalle rd	
															WHERE 
																id_Empresa=#id_Empresa# AND 
																id_Obra=#id_Obra# 
																AND id_Insumo='#id_insumo#'
																AND rd.id_Estatus not in(102,100)										

														)	
								DECLARE @im_Solicitud money =(	
															SELECT 
																isnull(SUM(rd.nu_Cantidad) * (SELECT  
																								AVG(ei.im_InsumoPrecio)as im_precio
																							  FROM explosioninsumos ei
																							  WHERE 
																									id_Empresa=#id_Empresa# 
																									AND id_Obra=#id_Obra#
																									AND id_Insumo=CASE 
																												WHEN (	SELECT 
																															count(*) 
																														FROM 
																															ExplosionInsumos 
																														WHERE 
																															id_Empresa=#id_Empresa# AND id_Obra=#id_Obra#
																															AND id_Insumo='#id_insumo#'
																													  ) =0 
																												 THEN (
																														  SELECT 
																															id_Agrupador 
																														  FROM
																															Insumos
																														  WHERE
																															id_Insumo='#id_insumo#'
																														)
																												 ELSE
																														'#id_insumo#'
																												 END 		),0)
															FROM 
															RequisicionesDetalle rd	
															WHERE 
																id_Empresa=#id_Empresa# AND 
																id_Obra=#id_Obra# 
																AND id_Insumo='#id_insumo#'
																AND rd.id_requisicion=#id_Requisicion#									

														  )									
														  
								SELECT @IM_PRESUPUESTO-(@im_Solicitado+@im_Solicitud) AS im_disponible							  					 
														 							  					 
							 
                  --         	SELECT
                  --               ei.id_Agrupador,
                  --               SUM(rd.nu_Cantidad) as nu_Solicitado,
                  --               AVG(ei.im_InsumoPrecio)as im_precio,
                  --               SUM(ei.im_SubTotal) as im_Presupuestado,
                  --               SUM(rd.nu_cantidad) * MAX(ei.im_InsumoPrecio) as im_Solicitado,
                  --               SUM(ei.im_SubTotal) - (SUM(rd.nu_cantidad) * MAX(ei.im_InsumoPrecio)) as im_Disponible
                  --           FROM
                  --               (select id_Empresa, id_Obra, i.id_Agrupador, AVG(im_InsumoPrecio)as im_InsumoPrecio, SUM(im_SubTotal)as im_SubTotal from ExplosionInsumos e
                  --               inner join Insumos i on i.id_Insumo =e.id_insumo
                  --               WHERE id_Empresa=1 and id_Obra=5555 and e.id_Insumo= CASE 
																		-- WHEN (	SELECT 
																		-- 			count(*) 
																		-- 		FROM 
																		-- 			ExplosionInsumos 
																		-- 		WHERE 
																		-- 			id_Empresa=#id_Empresa# AND id_Obra=#id_obra#
																		-- 			AND id_Insumo='#id_Insumo#'
																		-- 	  ) =0 
																		--  THEN (
																		-- 		  SELECT 
																		-- 			id_Agrupador 
																		-- 		  FROM
																		-- 			Insumos
																		-- 		  WHERE
																		-- 			id_Insumo='#id_Insumo#'	
																		-- 		)
																		--  ELSE
																		-- 		'#id_Insumo#'	
																		--  END 		
                  --               group by id_Empresa, id_Obra, i.id_Agrupador)ei
                  --               LEFT JOIN 
                  --               (select id_Empresa, id_Obra, i.id_Agrupador,rd.id_Requisicion, SUM(nu_Cantidad)as nu_Cantidad from RequisicionesDetalle rd
                  --               inner join Insumos i on i.id_Insumo = rd.id_Insumo
                  --               where rd.id_Estatus <> 102
                  --               group by id_Empresa, id_Obra, i.id_Agrupador,rd.id_Requisicion)rd ON
                  --                   ei.id_Empresa = rd.id_Empresa AND
                  --                   ei.id_Obra = rd.id_Obra AND
                  --                   ei.id_Agrupador = rd.id_Agrupador
                  --           WHERE
                  --               rd.id_Empresa =#id_Empresa# AND rd.id_Obra =#id_obra#
                                
                  --           GROUP BY 
                  --               ei.id_Agrupador
                  --           ORDER BY
                  --               ei.id_Agrupador    
                                
                                   
                                                   
								<!--- SELECT(

									SELECT 
										ISNULL(MAX( eia.im_SubtotalAgrupador ) - (SUM( nu_Cantidad ) * MAX( im_InsumoPrecio )), 0 )
									FROM
										RequisicionesDetalle rd
									INNER JOIN
										Insumos i ON
											i.id_Insumo = rd.id_Insumo
									INNER JOIN
										(
											SELECT 
												id_Empresa, id_Obra, id_Insumo, MAX( im_InsumoPrecio ) im_InsumoPrecio
											FROM
												ExplosionInsumos 
											GROUP BY
												id_Empresa, id_Obra, id_Insumo

										) ei ON
											ei.id_Empresa = rd.id_Empresa AND
											ei.id_Obra = rd.id_Obra AND
											ei.id_Insumo  = rd.id_Insumo 
									INNER JOIN
										(

											SELECT 
												ei.id_Empresa,ei.id_Obra,id_Agrupador, SUM( im_SubTotal ) im_SubtotalAgrupador
											FROM 
												ExplosionInsumos ei
											INNER JOIN
												Insumos i ON
													i.id_Insumo = ei.id_Insumo
											GROUP BY 
												ei.id_Empresa,ei.id_Obra,id_Agrupador

										) eia ON
											eia.id_Empresa = ei.id_Empresa AND
											eia.id_Obra = ei.id_Obra AND
											eia.id_Agrupador = i.id_Agrupador
									WHERE
										rd.id_Empresa = #id_Empresa# AND
										rd.id_Obra = #id_Obra# ANd
										i.id_Agrupador = '#id_Agrupador#'	AND
										rd.id_Estatus <> 102 

								)
								-
								(

									SELECT 
										SUM( nu_Cantidad ) * MAX( im_InsumoPrecio ) 
									FROM
										RequisicionesDetalle rd
									INNER JOIN
										(
											SELECT 
												id_Empresa, id_Obra, id_Insumo, MAX( im_InsumoPrecio ) im_InsumoPrecio
											FROM
												ExplosionInsumos 
											GROUP BY
												id_Empresa, id_Obra, id_Insumo

										) ei ON
											ei.id_Empresa = rd.id_Empresa AND
											ei.id_Obra = rd.id_Obra AND
											ei.id_Insumo  = rd.id_Insumo 
									WHERE
										rd.id_Empresa = #id_Empresa# AND
										rd.id_Obra = #id_Obra# ANd
										rd.id_Insumo = '#id_Insumo#'	AND
										rd.id_Estatus <> 102 

								) as cantidad

								/*select (select SUM(nu_cantidad) * im_InsumoPrecio from ExplosionInsumos where id_Obra=#id_obra# 
										and id_Empresa=#id_Empresa# and id_Insumo= CASE 
							               WHEN  (
								                  SELECT COUNT(*)
								                  from ExplosionInsumos
								                  WHERE id_Obra=#id_Obra# AND id_Empresa=#id_Empresa#
								                  AND id_Insumo='#id_Insumo#'
								                  
							                              
							                 ) = 0 THEN (
								                     SELECT 
								                     distinct I.id_Agrupador
								                     from 
								                     insumos I inner join Agrupadores_fe A ON
								                     I.id_Agrupador=i.id_Agrupador
								                     where I.id_Insumo='#id_Insumo#'   
							                    ) 
							               ELSE '#id_Insumo#'
							              END  
									)
									-
									(
										select (
												SELECT ISNULL(SUM(nu_PorComprar),0)
												FROM RequisicionesDetalle
												WHERE id_Obra=#id_obra# and id_Empresa=#id_Empresa# and id_Insumo='#id_Insumo#'
											)+
											(
												SELECT ISNULL(SUM(ocd.nu_Cantidad),0)
												FROM 
													OrdenesCompraDetalle ocd
													INNER JOIN OrdenesCompra oc ON
													ocd.id_Empresa=oc.id_Empresa AND
													ocd.id_Obra=oc.id_Obra AND
													ocd.id_OrdenCompra=oc.id_OrdenCompra
												WHERE ocd.id_Obra=#id_obra# and ocd.id_Empresa=#id_Empresa# and ocd.id_Insumo='#id_Insumo#' and oc.id_OrdenCompra<>202 
											)
									) as cantidad*/ --->
						</cfquery>
						<cfif valida.im_disponible LT 0>
							<cfset bandera=true>
							<cfset insumos =insumos&',Se ha excedido del presupuesto  #id_insumo#,'>
						</cfif>	
				</cfloop>
					<cfset RS.mensaje=insumos>
					<cfset RS.bandera=bandera>
                    <!--- <cfdump var="#RS#"><cfabort> --->
					<cfreturn RS>
				<cfcatch type='database'>
					<cfset RS.mensaje='ha ocurrido un error en la base de datos'>
					<cfset RS.bandera=false>
					<cfreturn RS>				
				</cfcatch>
			</cftry>
	</cffunction>

	<cffunction name="rechazarInsumo" access="remote" returntype="struct" returnformat="JSON">
			<cfargument name="id_obra" 		  type="string" default="" required="yes">
			<cfargument name="id_Empresa" 	  type="string" default="" required="yes">
			<cfargument name="id_Requisicion" type="string" default="" required="yes">
			<cfargument name="id_insumo"      type="string" default="" required="yes">

			<cftry>
			
				<cfoutput>
					rechazar_Insumo #id_Empresa#,#id_obra#,#id_Requisicion#,'#id_insumo#'
				</cfoutput>
				<cfquery name="RS_Actualiza_Estatus" datasource="#cnx#">
					rechazar_Insumo #id_Empresa#,#id_obra#,#id_Requisicion#,'#id_insumo#'
				</cfquery>	

				
					<cfset RS.OK=true>
					<cfset RS.mensaje='Actualizacion del estatus correctamente'>
					<cfreturn RS>
				<cfcatch type='database'>
					<cfset RS.OK=false>
					<cfset RS.mensaje='Al momento de Actualizar ha ocurrido un error'>
					<cfreturn RS>				
				</cfcatch>
			</cftry>
		
	
	</cffunction>

	<cffunction name="guardarRequisicion" access="remote" returntype="struct" returnformat="JSON">
			<cfargument name="id_obra" 				type="string" default="" required="yes">
			<cfargument name="id_Empresa" 			type="string" default="" required="yes">
			<cfargument name="id_Requisicion" 		type="string" default="" required="yes">
			<cfargument name="de_Telefono" 			type="string" default="" required="yes">
			<cfargument name="de_Comentario" 		type="string" default="" required="yes">
			<cfargument name="nb_Recibe" 			type="string" default="" required="yes">
			<cfargument name="de_DomicilioBodega" 	type="string" default="" required="yes">

			<cftry>
				<cfquery name="RS_Actualiza_Estatus" datasource="#cnx#">
					 Requisiciones_editar #id_obra#,#id_Empresa#,#id_Requisicion#,'#de_Telefono#','#de_Comentario#','#nb_Recibe#','#de_DomicilioBodega#'
				</cfquery>	
					<cfset RS.OK=true>
					<cfset RS.mensaje='Actualizacion de la requisicion correctamente'>
					<cfreturn RS>
				<cfcatch type='database'>
					<cfset RS.OK=false>
					<cfset RS.mensaje='Al momento de Actualizar ha ocurrido un error'>
					<cfreturn RS>				
				</cfcatch>
			</cftry>
		
	
	</cffunction>
	 <cffunction name="RS_DetallesRequisicion" access="remote" returntype="struct" returnformat="JSON">
			<cfargument name="id_obra" 			type="string" default="" required="yes">
			<cfargument name="id_Empresa" 		type="string" default="" required="yes">
			<cfargument name="id_Requisicion" 	type="string" default="" required="yes">
			<cfargument name="id_Agrupador" 	type="string" default="" required="yes">
			<cfargument name="id_insumo" 	    type="string" default="" required="yes">

			<cftry>
						
			    <cfquery datasource='#cnx#' name='RsTipo'>
			            select 
			              count(*) as tipo 
			            from 
			              Obras 
			            where 
			              id_tipoobra=0 AND id_Empresa=#id_Empresa# AND id_obra=#id_obra#

			    </cfquery>

			    <cfif RsTipo.tipo EQ 1>
			    	
			    	<cfquery name="RS_Detalles" datasource="#cnx#">
						SELECT 

							rd.id_Insumo,i.de_Insumo,UM.de_UnidadMedida,'GAO-001' as id_Agrupador,'INDIRECTOS' as de_Agrupador,RD.nu_Cantidad,
							rd.de_MedidaEspecial
						FROM 
							RequisicionesDetalle rd
	    				INNER JOIN insumos i      
							ON i.id_Insumo=rd.id_Insumo       
						INNER JOIN UnidadesMedida UM      
							ON Um.id_UnidadMedida=i.id_UnidadMedida           
						INNER JOIN Agrupadores_fe A      
							ON A.id_Agrupador=i.id_Agrupador      
						WHERE 
							id_Obra=#id_obra# AND id_Requisicion=#id_Requisicion# AND id_Empresa=#id_Empresa# 
							<cfif #id_Agrupador# NEQ ''>
								AND A.id_Agrupador='#id_Agrupador#'	
							</cfif>    
							<cfif #id_insumo# NEQ ''>
								AND rd.id_insumo='#id_insumo#'	
							</cfif>    


					</cfquery>	

			    <cfelse>
		    
		  			<cfquery name="RS_Detalles" datasource="#cnx#">
						SELECT 

							rd.id_Insumo,i.de_Insumo,UM.de_UnidadMedida,i.id_Agrupador,A.de_Agrupador,RD.nu_Cantidad,
							rd.de_MedidaEspecial
						FROM 
							RequisicionesDetalle rd
	    				INNER JOIN insumos i      
							ON i.id_Insumo=rd.id_Insumo       
						INNER JOIN UnidadesMedida UM      
							ON Um.id_UnidadMedida=i.id_UnidadMedida           
						INNER JOIN Agrupadores_fe A      
							ON A.id_Agrupador=i.id_Agrupador      
						WHERE 
							id_Obra=#id_obra# AND id_Requisicion=#id_Requisicion# AND id_Empresa=#id_Empresa# 
							<cfif #id_Agrupador# NEQ ''>
								AND A.id_Agrupador='#id_Agrupador#'	
							</cfif>    
							<cfif #id_insumo# NEQ ''>
								AND rd.id_insumo='#id_insumo#'	
							</cfif>    
					</cfquery>
		
			
		    	</cfif>

				<cfset RS.OK=true>
				<cfset RS.QUERY=RS_Detalles>
				<cfreturn RS>
					<cfcatch type='database'>
					<cfset RS.OK=false>
					<cfset RS.mensaje='ha ocurrido un error en el servidor'>
					<cfreturn RS>				
				</cfcatch>
			</cftry>
		
	
	</cffunction> 
	<cffunction name="Agregar_insumo_requisicion" access="remote" returntype="struct" returnformat="JSON">
			<cfargument name="id_Requisicion" 			type="string" default="" required="yes">
			<cfargument name="id_obra" 					type="string" default="" required="yes">
			<cfargument name="id_Empresa" 				type="string" default="" required="yes">
			<cfargument name="id_Insumo" 				type="string" default="" required="yes">
			<cfargument name="nu_cantidad" 				type="string" default="" required="yes">
			<cfargument name="Unidad_Medida_Especial" 	type="string" default="" required="yes">
			<cfargument name="Estatus_det"              type="string" default="" required="yes">
		

			<cftry>

					
					 <cfquery name="TipoPresupuesto" datasource="#session.cnx#">
			            SELECT 
			              COUNT(*) as contador 
			            FROM 
			              ExplosionInsumos e 
			              INNER JOIN insumos i on e.id_insumo=i.id_insumo
			              INNER JOIN Agrupadores_fe A on a.id_Agrupador=i.id_Agrupador
			            WHERE id_empresa=#id_Empresa# and id_Obra=#id_obra#
	    			</cfquery>
	    			
	    			<cfquery datasource='#cnx#' name='RsTipo'>
			      		select 
			      			count(*) as tipo 
			      		from 
			      			Obras 
			      		where 
			      			id_tipoobra=0 AND id_Empresa=#id_Empresa# AND id_obra=#id_obra#

      				</cfquery>
      			
      <cfif #RsTipo.tipo# EQ 1>
      					<cfquery name="RSCC" datasource="#cnx#">

									SELECT TOP 1
									    EI.id_CentroCosto,EI.id_Frente,EI.id_Partida
									 FROM 
		       							ExplosionInsumos EI
								        LEFT JOIN RequisicionesDetalle RD   ON 
											Ei.id_Obra=RD.id_Obra AND 
											Ei.id_Empresa=RD.id_Empresa AND 
											RD.id_CentroCosto=EI.id_CentroCosto AND 
											RD.id_Frente=EI.id_Frente AND 
											RD.id_Partida=EI.id_Partida AND 
											EI.id_Insumo=RD.id_insumo
									WHERE 
										EI.id_Insumo='GAO-001' and Ei.id_Obra=#id_obra# and Ei.id_Empresa=#id_Empresa#
											
						</cfquery>

						<cfquery name="RS_Detalles" datasource="#cnx#">
							 	Requisiciones_Guardar_Detalle  #id_Requisicion#, #id_obra#,#id_Empresa#, '#id_Insumo#', #nu_cantidad#, '#UCASE(Replace(Unidad_Medida_Especial,"'",""))#',#Estatus_det# ,#RSCC.id_CentroCosto#,#RSCC.id_Frente#,#RSCC.id_Partida#    
						</cfquery>	

      				<cfelse>

	      					<cfif #TipoPresupuesto.contador# GT 1>
							
								<cfquery name="RSCC" datasource="#cnx#">
									SELECT TOP 1
									    EI.id_CentroCosto,EI.id_Frente,EI.id_Partida
									 FROM 
		       							ExplosionInsumos EI
								        LEFT JOIN RequisicionesDetalle RD   ON 
											Ei.id_Obra=RD.id_Obra AND 
											Ei.id_Empresa=RD.id_Empresa AND 
											RD.id_CentroCosto=EI.id_CentroCosto AND 
											RD.id_Frente=EI.id_Frente AND 
											RD.id_Partida=EI.id_Partida AND 
											EI.id_Insumo=RD.id_insumo
									WHERE 
										EI.id_Insumo='#id_Insumo#' and Ei.id_Obra=#id_obra# and Ei.id_Empresa=#id_Empresa# 
										AND EI.nu_Cantidad > ISNULL((select SUM(nu_cantidad) 
																	 from RequisicionesDetalle 
																	 where id_Empresa=RD.id_Empresa and id_Obra=Rd.id_Obra and 
																	       id_CentroCosto=Ei.id_CentroCosto and id_Frente=Ei.id_Frente and
																			id_Partida=Ei.id_Partida and id_Insumo=Ei.id_Insumo AND
																			id_estatus <> 102
																	),0)
								</cfquery>
      						

								
								<cfif #RSCC.recordCount# EQ 0>
									
									<cfquery name="RSCC" datasource="#cnx#">

										SELECT TOP 1
										    EI.id_CentroCosto,EI.id_Frente,EI.id_Partida
										 FROM 
			       							ExplosionInsumos EI
									        LEFT JOIN RequisicionesDetalle RD   ON 
												Ei.id_Obra=RD.id_Obra AND 
												Ei.id_Empresa=RD.id_Empresa AND 
												RD.id_CentroCosto=EI.id_CentroCosto AND 
												RD.id_Frente=EI.id_Frente AND 
												RD.id_Partida=EI.id_Partida AND 
												EI.id_Insumo=RD.id_insumo
										WHERE 
											EI.id_Insumo='#id_Insumo#' and Ei.id_Obra=#id_obra# and Ei.id_Empresa=#id_Empresa#
										
									</cfquery>
										

									<cfif #RSCC.recordCount# EQ 0>
								
											
										<cfquery name="Agrupador" datasource="#cnx#">
											SELECT 
												distinct I.id_Agrupador
											 from 
												insumos I inner join Agrupadores_fe A	ON
												I.id_Agrupador=i.id_Agrupador
											 where I.id_Insumo='#id_Insumo#'

										</cfquery>

										<cfquery name="RSCC" datasource="#cnx#">

											SELECT TOP 1
											    EI.id_CentroCosto,EI.id_Frente,EI.id_Partida
											 FROM 
				       							ExplosionInsumos EI
										        LEFT JOIN RequisicionesDetalle RD   ON 
													Ei.id_Obra=RD.id_Obra AND 
													Ei.id_Empresa=RD.id_Empresa AND 
													RD.id_CentroCosto=EI.id_CentroCosto AND 
													RD.id_Frente=EI.id_Frente AND 
													RD.id_Partida=EI.id_Partida AND 
													EI.id_Insumo=RD.id_insumo
											WHERE 
												EI.id_Insumo='#Agrupador.id_agrupador#' and Ei.id_Obra=#id_obra# and Ei.id_Empresa=#id_Empresa# 
												AND EI.nu_Cantidad > ISNULL((select SUM(nu_cantidad) 
																			 from RequisicionesDetalle 
																			 where id_Empresa=RD.id_Empresa and id_Obra=Rd.id_Obra and 
																			       id_CentroCosto=Ei.id_CentroCosto and id_Frente=Ei.id_Frente and
																					id_Partida=Ei.id_Partida and id_Insumo=Ei.id_Insumo  AND
																					id_estatus <> 102
																			),0)
										</cfquery>

										
										<cfif #RSCC.recordCount# EQ 0>
													
											<cfquery name="RSCC" datasource="#cnx#">

												SELECT TOP 1
												    EI.id_CentroCosto,EI.id_Frente,EI.id_Partida
												 FROM 
					       							ExplosionInsumos EI
											        LEFT JOIN RequisicionesDetalle RD   ON 
														Ei.id_Obra=RD.id_Obra AND 
														Ei.id_Empresa=RD.id_Empresa AND 
														RD.id_CentroCosto=EI.id_CentroCosto AND 
														RD.id_Frente=EI.id_Frente AND 
														RD.id_Partida=EI.id_Partida AND 
														EI.id_Insumo=RD.id_insumo
												WHERE 
													EI.id_Insumo='#Agrupador.id_agrupador#' and Ei.id_Obra=#id_obra# and Ei.id_Empresa=#id_Empresa#
												
										</cfquery>

									
										</cfif>

									</cfif>
								

								</cfif>
								
								

								<cfquery name="RS_Detalles" datasource="#cnx#">
								 	Requisiciones_Guardar_Detalle  #id_Requisicion#, #id_obra#,#id_Empresa#, '#id_Insumo#', #nu_cantidad#, '#Replace(Unidad_Medida_Especial,"'","")#',#Estatus_det# ,#RSCC.id_CentroCosto#,#RSCC.id_Frente#,#RSCC.id_Partida#    
								</cfquery>	
								

							<cfelse>
						
								<cfquery name="RS_Detalles" datasource="#cnx#">
								 	Requisiciones_Guardar_Detalle  #id_Requisicion#, #id_obra#,#id_Empresa#, '#id_Insumo#', #nu_cantidad#, '#Replace(Unidad_Medida_Especial,"'","")#',#Estatus_det# ,900,901,1    
								</cfquery>
								

							</cfif>
							

      					</cfif>
					
					<cfif not isDefined( 'RS_Detalles' )>
						<cfset mensaje='El insumo ya existe en esta requisicion!'>
						<cfset repetido=true>
					<cfelse>
						<cfset mensaje='El insumo ha sido agregado correctamente!'>
						<cfset repetido=false>
					</cfif>
			
					<cfset RS.OK=true>
					<cfset RS.repetido=repetido>
					<cfset RS.mensaje=mensaje>
					<cfreturn RS>
				<cfcatch type='database'>
					<cfset RS.OK=false>
					<cfset RS.mensaje='ha ocurrido un error en el servidor'>
					<cfreturn RS>				
				</cfcatch>
			</cftry>
		
	
	</cffunction> 
	<cffunction name="Medios" access="remote" returntype="QUERY" returnformat="JSON">
			
		

		

				<cfquery name="RS_Detalles" datasource="#cnx#">
				 	select * from MediosEnvio
				</cfquery>
				
				<cfreturn RS_Detalles>
				
			
	
	</cffunction> 
		<cffunction name="Almacenes" access="remote" returntype="QUERY" returnformat="JSON">
			<cfargument name="id_Empresa" 				type="string" default="" required="yes">
			<cfargument name="id_obra" 					type="string" default="" required="yes">
			
		

	
				<cfquery name="RS_Detalles" datasource="#cnx#">
				 	select id_almacen,de_almacen from almacenes where id_Empresa=#id_Empresa# and id_Obra=#id_obra#
				</cfquery>
				
				<cfreturn RS_Detalles>
				
			
	
	</cffunction> 
	<cffunction name="Iva" access="remote" returntype="QUERY" returnformat="JSON">
			
		

	
				<cfquery name="RS_Detalles" datasource="#cnx#">
				 	SELECT pj_iva FROM IVA WHERE sn_Activo = 1
				</cfquery>
				
				<cfreturn RS_Detalles>
				
			
	
	</cffunction> 

	<cffunction name="RS_Presupuesto" access="remote" returntype="struct" returnformat="JSON">
			<cfargument name="id_Agrupador" 			type="string" default="" required="yes">
			<cfargument name="id_obra" 					type="string" default="" required="yes">
			<cfargument name="id_Empresa" 				type="string" default="" required="yes">
			
			

			<cftry>




				<cfquery name="Presupuesto" datasource="#cnx#">
				
					



						SELECT 
							ROW_NUMBER() OVER( ORDER BY EA.id_Agrupador DESC ) AS fila, EA.id_Agrupador,A.de_Agrupador,ea.im_subtotal,
							(ISNULL((SELECT ISNULL(SUM(nu_Cantidad),0)
						FROM OrdenesCompraDetalle rd 
							INNER JOIN Insumos i ON i.id_Insumo = rd.id_Insumo 
							INNER JOIN Agrupadores_fe a ON a.id_Agrupador = i.id_Agrupador  
							WHERE 
						
							 			a.id_Agrupador = EA.id_Agrupador
									    <cfif #id_obra# NEQ ''>
											AND rd.id_obra = #id_obra#
										</cfif>
										<cfif #id_Empresa# NEQ ''>
											AND rd.id_Empresa=#id_Empresa#
										</cfif>
									
									GROUP BY
									 a.id_Agrupador),0)) AS nu_comprado,
							(ISNULL((SELECT ISNULL(SUM(im_CantidadPrecio),0)
						FROM OrdenesCompraDetalle rd 
							INNER JOIN Insumos i ON i.id_Insumo = rd.id_Insumo 
							INNER JOIN Agrupadores_fe a ON a.id_Agrupador = i.id_Agrupador  
							WHERE 
						
							 			a.id_Agrupador = EA.id_Agrupador
									    <cfif #id_obra# NEQ ''>
											AND rd.id_obra = #id_obra#
										</cfif>
										<cfif #id_Empresa# NEQ ''>
											AND rd.id_Empresa=#id_Empresa#
										</cfif>
									
									GROUP BY
									 a.id_Agrupador),0)) AS im_subtotal_comprado,
									 ( ISNULL((SELECT ISNULL(SUM(nu_PorComprar),0)
						FROM RequisicionesDetalle rd 
							INNER JOIN Insumos i ON i.id_Insumo = rd.id_Insumo 
							INNER JOIN Agrupadores_fe a ON a.id_Agrupador = i.id_Agrupador  
							
									WHERE 
										RD.id_estatus =101 and
							 			a.id_Agrupador = EA.id_Agrupador
									    <cfif #id_obra# NEQ ''>
											AND rd.id_obra = #id_obra#
										</cfif>
										<cfif #id_Empresa# NEQ ''>
											AND rd.id_Empresa=#id_Empresa#
										</cfif>
									GROUP BY
									 a.id_Agrupador),0)) AS nu_porComprar,EA.nu_Cantidad,EA.im_precio
									 

						FROM 
							explosioninsumos_agrupadores_fe EA 
							INNER JOIN Agrupadores_fe A ON EA.id_agrupador=A.id_agrupador
						WHERE 
							1=1	
						<cfif #id_Agrupador# NEQ ''>
								AND EA.id_Agrupador='#id_Agrupador#'
									
						</cfif>
					    <cfif #id_obra# NEQ ''>
								AND EA.id_obra = #id_obra#
						</cfif>
						<cfif #id_Empresa# NEQ ''>
									AND EA.id_Empresa=#id_Empresa#
						</cfif>

						GROUP by EA.id_Agrupador,A.de_Agrupador,ea.im_subtotal,EA.nu_Cantidad,EA.im_precio
						ORDER by A.de_Agrupador
						


				
				</cfquery>
				
			
					
					<cfset RS.OK=true>
					<cfset RS.Presupuesto=Presupuesto>
					<cfreturn RS>
				<cfcatch type='database'>
					<cfset RS.OK=false>
					<cfset RS.mensaje='ha ocurrido un erro en el servidor'>
					<cfreturn RS>				
				</cfcatch>
			</cftry>
		
	
	</cffunction> 	
	<cffunction name="RS_PresupuestoMixto" access="remote" returntype="struct" returnformat="JSON">
			<cfargument name="id_Agrupador" 			type="string" default="" required="yes">
			<cfargument name="id_obra" 					type="string" default="" required="yes">
			<cfargument name="id_Empresa" 				type="string" default="" required="yes">
			
			

			<cftry>

				<cfquery name="Presupuesto" datasource="#cnx#">
							SELECT 
								ROW_NUMBER() OVER( ORDER BY EA.id_Agrupador DESC ) AS fila,
								Ea.id_Agrupador,
								Ea.de_Agrupador,
								SUM(EI.im_SubTotal) as im_subtotal,
								(ISNULL((SELECT ISNULL(SUM(nu_Cantidad),0)
											FROM OrdenesCompraDetalle ocd 
											INNER JOIN Insumos i ON i.id_Insumo = ocd.id_Insumo 
											INNER JOIN Agrupadores_fe a ON a.id_Agrupador = i.id_Agrupador  
									
											WHERE 	a.id_Agrupador = EA.id_Agrupador 
											 <cfif #id_obra# NEQ ''>
												AND ocd.id_obra = #id_obra#
											</cfif>
											<cfif #id_Empresa# NEQ ''>
												AND ocd.id_Empresa=#id_Empresa#
											</cfif>
										),0)
								
								
								)as nu_comprado,
								(ISNULL((SELECT ISNULL(SUM(im_CantidadPrecio),0)
											FROM OrdenesCompraDetalle ocd 
											INNER JOIN Insumos i ON i.id_Insumo = ocd.id_Insumo 
											INNER JOIN Agrupadores_fe a ON a.id_Agrupador = i.id_Agrupador  
									
											WHERE a.id_Agrupador = EA.id_Agrupador 
											 <cfif #id_obra# NEQ ''>
												AND ocd.id_obra = #id_obra#
											 </cfif>
											 <cfif #id_Empresa# NEQ ''>
												AND ocd.id_Empresa=#id_Empresa#
											</cfif>
								),0)) as im_subtotal_comprado,
								(
								ISNULL((SELECT ISNULL(SUM(nu_PorComprar),0)
											FROM RequisicionesDetalle rd 
												INNER JOIN Insumos i ON i.id_Insumo = rd.id_Insumo 
												INNER JOIN Agrupadores_fe a ON a.id_Agrupador = i.id_Agrupador  
							
									WHERE 
										RD.id_estatus =101 and
							 			a.id_Agrupador = EA.id_Agrupador
										 <cfif #id_obra# NEQ ''>
											AND rd.id_obra = #id_obra#
										</cfif>
										<cfif #id_Empresa# NEQ ''>
											AND rd.id_Empresa=#id_Empresa#
										</cfif>
												
										
										),0)
								) nu_porComprar,SUM(EI.nu_Cantidad) as nu_cantidad,AVG(im_InsumoPrecio) as im_Precio
							FROM 
								ExplosionInsumos EI 
								INNER JOIN Insumos i ON EI.id_Insumo=I.id_Insumo 
								INNER JOIN Agrupadores_fe EA ON i.id_Agrupador=EA.id_Agrupador
							WHERE 
								1=1	
							<cfif #id_Agrupador# NEQ ''>
								AND EA.id_Agrupador='#id_Agrupador#'
									
								</cfif>
							    <cfif #id_obra# NEQ ''>
										AND Ei.id_obra = #id_obra#
								</cfif>
								<cfif #id_Empresa# NEQ ''>
											AND Ei.id_Empresa=#id_Empresa#
								</cfif>
							GROUP by Ea.id_Agrupador,Ea.de_Agrupador
							ORDER by Ea.de_Agrupador
						
								


						


				
				</cfquery>
				
				
						
						
			
					
					<cfset RS.OK=true>
					<cfset RS.Presupuesto=Presupuesto>
					<cfreturn RS>
				<cfcatch type='database'>
					<cfset RS.OK=false>
					<cfset RS.mensaje='ha ocurrido un erro en el servidor'>
					<cfreturn RS>				
				</cfcatch>
			</cftry>
		
	
	</cffunction> 	
	<cffunction name="Eliminar_Insumo_requisicion" access="remote" returntype="struct" returnformat="JSON">
			<cfargument name="id_Requisicion" 			type="string" default="" required="yes">
			<cfargument name="id_obra" 					type="string" default="" required="yes">
			<cfargument name="id_Empresa" 				type="string" default="" required="yes">
			<cfargument name="id_Insumo" 				type="string" default="" required="yes">
			

			<cftry>

				<cfquery name="RS_Detalles" datasource="#cnx#">
					Requisiciones_Eliminar_Detalle #id_Requisicion#, #id_obra#,#id_Empresa#, '#id_Insumo#'

				</cfquery>
				
				<cfif #RS_Detalles.recordCount# GT 0>
					<cfset mensaje='No se ha podido Eliminar el insumo'>
					<cfset repetido=true>
				<cfelse>
					<cfset mensaje='El insumo ha sido Eliminado correctamente!'>
					<cfset repetido=false>
				</cfif>
			
					
					<cfset RS.OK=true>
					<cfset RS.repetido=repetido>
					<cfset RS.mensaje=mensaje>
					<cfreturn RS>
				<cfcatch type='database'>
					<cfset RS.OK=false>
					<cfset RS.mensaje='ha ocurrido un erro en el servidor'>
					<cfreturn RS>				
				</cfcatch>
			</cftry>
		
	</cffunction> 
	<cffunction name="Editar_Insumo_requisicion" access="remote" returntype="struct" returnformat="JSON">
			<cfargument name="id_Requisicion" 			type="string" default="" required="yes">
			<cfargument name="id_obra" 					type="string" default="" required="yes">
			<cfargument name="id_Empresa" 				type="string" default="" required="yes">
			<cfargument name="id_Insumo" 				type="string" default="" required="yes">
			<cfargument name="nu_cantidad" 				type="string" default="" required="yes">
			<cfargument name="Unidad_Medida_Especial" 	type="string" default="" required="yes">
			

			<cftry>
				
				<cfquery name="RS_Detalles" datasource="#cnx#">
					Requisiciones_Editar_Detalle #id_Requisicion#, #id_obra#,#id_Empresa#, '#id_Insumo#',#nu_cantidad#,'#Replace(Unidad_Medida_Especial,"'","")#' 

				</cfquery>
				
				<cfif #RS_Detalles.contador# GT 0>
					<cfset mensaje='El insumo ha sido actualizado con exito!'>
					<cfset repetido=false>
				<cfelse>
					<cfset mensaje='El insumo no se ha podido actualizar!'>
					<cfset repetido=true>
				</cfif>
			
					
					<cfset RS.OK=true>
					<cfset RS.mensaje=mensaje>
					<cfreturn RS>
				<cfcatch type='database'>
					<cfset RS.OK=false>
					<cfset RS.mensaje='ha ocurrido un erro en el servidor'>
					<cfreturn RS>				
				</cfcatch>
			</cftry>
		
	
	</cffunction> 
	<cffunction name="Enviar_requisicion" access="remote" returntype="struct" returnformat="JSON">
			<cfargument name="id_Requisicion" 			type="string" default="" required="yes">
			<cfargument name="id_obra" 					type="string" default="" required="yes">
			<cfargument name="id_Empresa" 				type="string" default="" required="yes">
			<cfargument name="sn_ComprarPlaza"			type="string" default="0" required="false">
			<cftry>
				<cfquery name="RSCompraPlaza" datasource="#cnx#">
					UPDATE 
						RequisicionesDetalle
					SET 
						sn_ComprarPlaza = #sn_ComprarPlaza#
					WHERE
						id_Empresa 			= #id_Empresa#
						AND id_Obra 		= #id_Obra#
						AND id_Requisicion 	= #id_Requisicion#
				</cfquery>
				<cfquery name="RS_Detalles" datasource="#cnx#">
					Requisicion_enviar #id_Requisicion#,#id_obra#,#id_Empresa#
				</cfquery>
                
                <cfif #sn_ComprarPlaza# EQ 0>
                	<cfquery name="Compradores" datasource="#session.cnx#">
                        SELECT 
                            DISTINCT oc.id_Empleado,
                            e.nb_Nombre + ' ' + e.nb_ApellidoPaterno + ' ' + e.nb_ApellidoMaterno as nb_comprador,
                            e.de_eMail
                        FROM  
                            Obras_Compradores oc
                            inner join Empleados e on e.id_Empleado = oc.id_Empleado and e.sn_Activo=1
                        WHERE
                        oc.id_Obra = #id_Obra#  and oc.sn_Activo = 1
                    </cfquery>
                    
                    <cfoutput query="Compradores">
                        <cfquery name="Nuevos_Pedidos" datasource="#session.cnx#">
                        SELECT 
                                r.id_Empresa,r.id_Obra,r.id_Requisicion,fh_Requisicion,e.nb_Empresa,o.de_Obra,r.id_EmpleadoSolicito
                            FROM
                                Requisiciones r 
                                INNER JOIN Empresas e ON
                                    e.id_Empresa = r.id_Empresa 
                                INNER JOIN Obras o ON
                                    o.id_Empresa = r.id_Empresa AND
                                    o.id_Obra = r.id_Obra
                                INNER JOIN RequisicionesDetalle rd ON
                                    rd.id_Empresa = r.id_Empresa AND
                                    rd.id_Obra = r.id_Obra AND
                                    rd.id_Requisicion = r.id_Requisicion
                            WHERE
                                r.id_Empresa = #id_Empresa# and
                                r.id_Obra = #id_obra# AND
                                r.id_Requisicion= #id_Requisicion# 
                                 
                            GROUP BY
                                r.id_Empresa,r.id_Obra,nb_Empresa,de_Obra,r.id_Requisicion,fh_Requisicion,r.id_EmpleadoSolicito				
                        </cfquery>
                        <cfsavecontent variable="content">
                            <table border="1">
                                <tr>
                                    <td align="center">Empresa</td>
                                    <td align="center">Obra</td>
                                    <td align="center">Requisicion</td>
                                    <td align="center">Fecha Requisicion</td>
                                </tr>
                                <cfloop query="Nuevos_Pedidos">
                                    <tr>
                                        <td align="center">#nb_Empresa#</td>
                                        <td align="center">#id_Obra# - #de_Obra#</td>
                                        <td align="center">#id_Requisicion#</td>
                                        <td align="center">#DateFormat(fh_Requisicion,'DD/MM/YYYY')#</td>
                                    </tr>
                                </cfloop>
                            </table>
                        </cfsavecontent>
                     
                        <cfif isDefined('Compradores.de_Email') AND #Compradores.de_Email# NEQ ''>
                            <cfinvoke 	component				=	"#Application.componentes#.funciones"
                                        method					=	"cfMail"
                                        Destinatario			=	"#Compradores.de_Email#"
                                        Asunto					=	"Nuevos Pedidos"
                                        EncabezadoContenido 	=	"Estimado, #Compradores.nb_comprador#"
                                        DescripcionContenido	=	"Ha recibido nuevos pedidos:"
                                        Contenido				= 	"#content#"
                            >
                        </cfif>
                    </cfoutput>
                
                    
                 
					
				</cfif>
                
                
					<cfset RS.OK=true>
					<cfset RS.mensaje='La Requisicion ha sido enviada.'>
					<cfreturn RS>
				<cfcatch type='database'>
					<cfset RS.OK=false>
					<cfset RS.mensaje='No se ha podido enviar la requisicion intentelo mas tarde. #cfcatch.detail#'>
					<cfreturn RS>				
				</cfcatch>
			</cftry>
		
	
	</cffunction> 
	<cffunction name="EliminarRequisicion" access="remote" returntype="struct" returnformat="JSON">
			<cfargument name="id_Requisicion" 			type="string" default="" required="yes">
			<cfargument name="id_obra" 					type="string" default="" required="yes">
			<cfargument name="id_Empresa" 				type="string" default="" required="yes">
			<cftransaction>
				
				<cftry>
					
						 
					<cfquery name="RS_OrdenesCompra" datasource="#cnx#">
						SELECT 
							COUNT(*) as cont
						FROM	 	
							requisiciones_ordenescompra
						WHERE 
							id_Empresa=#id_Empresa# AND id_Obra=#id_Obra# AND id_Requisicion=#id_Requisicion#
							AND id_Estatus=103	

					</cfquery>
					
					<cfif #RS_OrdenesCompra.cont# EQ 0>
						<cfquery name="RS_Detalles" datasource="#cnx#">
								
								Requisicion_detalle_Eliminar #id_Requisicion#,#id_obra#,#id_Empresa#

						</cfquery>
						<cfquery name="RS_Detalles" datasource="#cnx#">
								
								Requisicion_Eliminar #id_Requisicion#,#id_obra#,#id_Empresa#
							
						</cfquery>

						<cfset RS.mensaje='La Requisicion ha sido eliminada.'>
						<cfset RS.OK=true>
					<cfelse>
						<cfset RS.mensaje='La Requisicion No puede ser eliminada Ya que existen OC ligadas a esa requisicion'>
						<cfset RS.OK=false>

					</cfif> 

							
						

						
					
						
							
							<cfreturn RS>
						<cfcatch type='database'>
							<cfset RS.OK=false>
							<cfset RS.mensaje='No se ha podido Eliminar la requisicion intentelo mas tarde.'>
							<cfreturn RS>				
						</cfcatch>
				</cftry>

			</cftransaction>
			
		
	
	</cffunction> 
	
	<cffunction name="RsEmpleadosSolicitantes" returntype="Struct" access="remote" returnformat="JSON">
		<cfargument name="id_Empresa" type="numeric" required="true">
		<cfargument name="id_Obra" type="numeric" required="true">

		<cftry>
			
			<cfquery name="RS" datasource="#session.cnx#">
				SELECT
					DISTINCT e.id_Empleado, nb_Nombre + ' ' + nb_ApellidoPaterno + ' ' + nb_ApellidoMaterno AS nb_Empleado
				FROM
					Requisiciones r
				INNER JOIN
					Empleados e ON
						e.id_Empleado = r.id_EmpleadoSolicito
				WHERE
					r.id_Empresa = #id_Empresa# AND
					id_Obra = #id_Obra#
			</cfquery>

			<cfset Result.SUCCESS = true>
			<cfset Result.RS = RS>

			<cfcatch type="database">
				<cfset Result.SUCCESS = true>
				<cfset Result.MESSAGE = "Error en acceso a base de datos, #cfcatch.detail#">
			</cfcatch>

		</cftry>

		<cfreturn Result>

	</cffunction>
	 <cffunction name="RS_RequisicionesDetalles_Listado_excel" access="public" returntype="query">
         <cfargument name="id_Empresa"   type="string" required="yes" default="">
         <cfargument name="id_obra"      type="string" required="yes" default="">
         <cfargument name="id_Estatus"   type="string" required="yes" default="">
         <cfargument name="id_Agrupador" type="string" required="yes" default="">
         <cfargument name="fh_inicio"    type="string" required="yes" default="">
         <cfargument name="fh_fin"       type="string" required="yes" default="">
         <cfargument name="id_Empleado"  type="string" required="yes" default="">
         <cfargument name="id_Solicitante"  type="string" required="yes" default="">
         <cfargument name="page"         type="numeric" required="no" default="1">

		 <cfset startRow='NUll'>
		 <cfset endRow='NUll'>
		 
	      <cfif #fh_fin# NEQ '' AND #fh_inicio# NEQ ''>
		 	
		 	 <cfif #dateformat(fh_inicio,'dd')# GT 12>
				<cfset fh_inicio = #dateformat(fh_inicio,'yyyy-mm-dd 00:00:00')#>
   			 <cfelse>
        		<cfset fh_inicio = #dateformat(fh_inicio,'yyyy-dd-mm 00:00:00')#>
    		</cfif>
		    <!---Fecha Fin--->
		    <cfif #dateformat(fh_fin,'dd')# GT 12>
				<cfset fh_fin = #dateformat(fh_fin,'yyyy-mm-dd 23:59:59')#>
		    <cfelse>
		        <cfset fh_fin = #dateformat(fh_fin,'yyyy-dd-mm 23:59:59')#>
		    </cfif>
	    	
		 </cfif>	
         <cfquery name="RS" datasource="#cnx#">
                Listado_Requisiciones_Excel 

                <cfif #id_Empresa# NEQ "">
                    #id_Empresa#,
                <cfelse>
                    NULL,

                </cfif>
                
                <cfif #id_obra# NEQ "">
                    #id_obra#,
                <cfelse>
                    NULL,
                    
                </cfif>

                <cfif #id_Estatus# NEQ "">
                    #id_Estatus#,
                <cfelse>
                    NULL,
                    
                </cfif>
                #startRow#,
                #endRow#,

                <cfif #id_Agrupador# NEQ "">
                    '#id_Agrupador#',
                <cfelse>
                    NULL,   
                    
                </cfif>
                <cfif #fh_inicio# NEQ "">
                    '#fh_inicio#',
                <cfelse>
                    NULL,
                    
                </cfif>
                <cfif #fh_fin# NEQ "">
                    '#fh_fin#',
                <cfelse>
                    NULL,
                </cfif>
                <cfif #id_Empleado# NEQ "">
                    #id_Empleado#,
                <cfelse>
                    NULL,
                    
                </cfif>
                <cfif #id_Solicitante# NEQ "">
                    #id_Solicitante#
                <cfelse>
                    NULL
                    
                </cfif>

           </cfquery>
           <cfreturn  RS>
    </cffunction>     

</cfcomponent>