<cfcomponent extends="conexion">
	<cffunction name="Reporte_General" access="public" returntype="struct">
		<cfargument name="id_Empresa" type="string" default="">
        <cfargument name="id_Obras" type="string" default="">
        <cfargument name="id_Agrupador" type="string" default="">
        
        <cfif #id_Agrupador# EQ ''>
			<cfset #id_Agrupador# = 'NULL'>
		<cfelse>
			<cfset id_Agrupador = "'#id_Agrupador#'">
		</cfif>
        <cfif #id_Obras# EQ ''>
			<cfset #id_Obras# = 'NULL'>
		</cfif>
        <cftry>
    	    <cfquery datasource='#cnx#' name='RsTipo'>
                select 
                    count(*) as tipo 
                from 
                    Obras 
                where 
                    id_tipoobra=0 AND id_Empresa=#id_Empresa# AND id_obra in (#id_Obras#)

            </cfquery>
            <cfif RsTipo.tipo EQ 1>
                   <cfquery name="rsAux" datasource="#cnx#">
                      
                        EXEC Reporte_General_ExplosionInsumos_INDIRECTOS #id_Empresa#, NULL ,#PreserveSingleQuotes(id_Agrupador)#
                   </cfquery>  

                
            <cfelse>
                 <cfquery name="rsAux" datasource="#cnx#">
                        IF EXISTS(SELECT id_Obra FROM ExplosionInsumos_Agrupadores_fe WHERE id_Empresa = #id_Empresa# AND id_Obra in (#id_Obras#))
                        BEGIN
                            EXEC Reporte_General #id_Empresa#, NULL ,#PreserveSingleQuotes(id_Agrupador)#
                        END
                        ELSE
                        BEGIN
                            EXEC Reporte_General_ExplosionInsumos #id_Empresa#,NULL, #PreserveSingleQuotes(id_Agrupador)#
                        END
                 </cfquery>    
            </cfif>
          
          <cfquery name="RSDatos" dbtype="query">                
            SELECT 
                * 
            FROM
                rsAux
            <cfif id_Obras NEQ ''>
                WHERE 
                    id_Obra IN (#id_Obras#)
            </cfif>
        </cfquery>
            <!--- <cfquery dbtype="query" name="rsAux">
				SELECT 
					id_Empresa, id_Obra, id_Agrupador, id_Insumo,
					
					MAX(de_Agrupador) AS de_Agrupador, 
					MAX(de_UnidadMedidaAgrupador) AS de_UnidadMedidaAgrupador,
					MAX(de_Insumo) AS de_Insumo, 
					MAX(de_UnidadMedida) AS de_UnidadMedida,         
					MAX(nu_CantidadAgrupador) AS nu_CantidadAgrupador, 
					MAX(im_PrecioAgrupador) as im_PrecioAgrupador,  
					MAX(im_SubtotalAgrupador) as im_SubtotalAgrupador ,        
					MAX(nu_CantidadExplosion) as nu_CantidadExplosion, 
					MAX(im_PrecioExplosion) as im_PrecioExplosion,     
					MAX(im_SubtotalExplosion) as im_SubtotalExplosion,        
					MAX(nu_Cantidad) as nu_Cantidad, 
					MAX(im_precio) as im_precio,         
					MAX(nu_CantidadCompra) AS nu_CantidadCompra,         
					MAX(im_PrecioCompra) as im_PrecioCompra,         
					MAX(im_CantidadPrecioCompra) AS im_CantidadPrecioCompra,        
					MAX(nu_Entrada) AS nu_Entrada,         
					MAX(nu_Salida) AS nu_Salida,         
					MAX(nu_EntradaAgrupador) AS nu_EntradaAgrupador,         
					MAX(nu_SalidaAgrupador) AS nu_SalidaAgrupador,        
					MAX(nu_CantidadTotalAgrupador) as nu_CantidadTotalAgrupador,         
					MAX(nu_CantidadCompraTotalAgrupador) as nu_CantidadCompraTotalAgrupador,        
					MAX(im_CantidadPrecioCompraTotalAgrupador) as im_CantidadPrecioCompraTotalAgrupador,        
					MAX(nu_DisponibleAgrupador) AS nu_DisponibleAgrupador,         
					MAX(im_DisponibleAgrupador) AS im_DisponibleAgrupador,         
					MAX(nu_Disponible) AS nu_Disponible,         
					MAX(im_Disponible) AS im_Disponible,        
					MAX(nu_AplicadoEjecutado_ot) AS nu_AplicadoEjecutado_ot,        
					MAX(im_AplicadoEjecutado_ot) AS im_AplicadoEjecutado_ot,        
					MAX(nu_AplicadoEjecutadoAgrupador_ot) AS nu_AplicadoEjecutadoAgrupador_ot,        
					MAX(im_AplicadoEjecutadoAgrupador_ot) AS im_AplicadoEjecutadoAgrupador_ot,        
					MAX(nu_AplicadoEjecutado_dst) AS nu_AplicadoEjecutado_dst,        
					MAX(im_AplicadoEjecutado_dst) AS im_AplicadoEjecutado_dst,        
					MAX(nu_AplicadoEjecutadoAgrupador_dst) AS nu_AplicadoEjecutadoAgrupador_dst,        
					MAX(im_AplicadoEjecutadoAgrupador_dst) AS im_AplicadoEjecutadoAgrupador_dst
				FROM
					RS
				GROUP BY 
					id_Empresa,
					id_Obra,
					id_Agrupador,
					id_insumo
			</cfquery>  --->
           
        	<!---<cfstoredproc procedure="Reporte_General" datasource="#cnx#">
            	<cfprocparam cfsqltype="cf_sql_integer" type="in" dbvarname="@id_Empresa" value="#id_Empresa#" null="#iif(isNumeric(id_Empresa),false,true)#">
                <cfprocparam cfsqltype="cf_sql_integer" type="in" dbvarname="@id_Obra" value="#id_Obra#" null="#iif(isNumeric(id_Obra),false,true)#">
                <cfprocparam cfsqltype="cf_sql_varchar" type="in" dbvarname="@id_Agrupador" value="#id_Agrupador#" null="#not(len(id_Agrupador))#">
                <cfprocresult name="Rs" resultset="1">
            </cfstoredproc>--->
			<cfset myResult.SUCCESS = true>	
            <cfset myResult.RS = RSDatos>
            <cfcatch type="database">
            	<cfset myResult.SUCCESS = false>	
            	<cfset myResult.MESSAGE = 'Error en acceso a base de datos, #cfcatch.Detail#'>
            </cfcatch>	
        </cftry>
		<cfreturn myResult>
	</cffunction>

    <cffunction name="Reporte_Compras_insumos" access="public" returntype="struct">
        <cfargument name="id_Empresa"    type="string" default="">
        <cfargument name="id_Obra"       type="string" default="">
        <cfargument name="id_Proveedor"  type="string" default="">
        <cfargument name="id_insumo"     type="string" default="">
        <cfargument name="fh_inicio"     type="string" default="">
        <cfargument name="fh_fin"        type="string" default="">



         <cfif #fh_fin# NEQ '' AND #fh_inicio# NEQ ''>
            
             <cfif #dateformat(fh_inicio,'dd')# GT 12>
                <cfset fh_inicio = #dateformat(fh_inicio,'yyyy-mm-dd')#>
             <cfelse>
                <cfset fh_inicio = #dateformat(fh_inicio,'yyyy-dd-mm')#>
            </cfif>
            <!---Fecha Fin--->
            <cfif #dateformat(fh_fin,'dd')# GT 12>
                <cfset fh_fin = #dateformat(fh_fin,'yyyy-mm-dd')#>
            <cfelse>
                <cfset fh_fin = #dateformat(fh_fin,'yyyy-dd-mm')#>
            </cfif>
            
        </cfif> 
         

        <cfif #id_Proveedor# EQ ''>
            <cfset #id_Proveedor# = 'NULL'>
        </cfif>
        
        <cftry>
            <cfquery name="RS" datasource="#cnx#">
              
                    EXEC Reporte_Compras #id_Empresa#, #id_Obra#, #id_Proveedor#,<cfif #id_insumo# EQ ''>
                                                                                            NULL,                
                                                                                <cfelse>
                                                                                        '#id_insumo#',
                                                                                </cfif>
                                                                                <cfif #fh_inicio# EQ ''>
                                                                                    NULL,
                                                                                <cfelse>
                                                                                    '#fh_inicio#',    
                                                                                </cfif>
                                                                                <cfif #fh_fin# EQ ''>
                                                                                    NULL
                                                                                <cfelse>
                                                                                     '#fh_fin#'

                                                                                </cfif>

            
            </cfquery>

            <cfset myResult.SUCCESS = true> 
            <cfset myResult.RS = Rs>
            <cfcatch type="database">
                <cfset myResult.SUCCESS = false>    
                <cfset myResult.MESSAGE = 'Error en acceso a base de datos, #cfcatch.Detail#'>
            </cfcatch>  
        </cftry>
        <cfreturn myResult>
    </cffunction>
     <cffunction name="Reporte_Compras_proveedor" access="public" returntype="struct">
        <cfargument name="id_Empresa"    type="string" default="">
        <cfargument name="id_Obra"       type="string" default="">
        <cfargument name="id_Proveedor"  type="string" default="">
        <cfargument name="fh_inicio"     type="string" default="">
        <cfargument name="fh_fin"        type="string" default="">
        <cfargument name="id_Estatus"    type="string" default="">




         <cfif #fh_fin# NEQ '' AND #fh_inicio# NEQ ''>
            
             <cfif #dateformat(fh_inicio,'dd')# GT 12>
                <cfset fh_inicio = #dateformat(fh_inicio,'yyyy-mm-dd')#>
             <cfelse>
                <cfset fh_inicio = #dateformat(fh_inicio,'yyyy-dd-mm')#>
            </cfif>
            <!---Fecha Fin--->
            <cfif #dateformat(fh_fin,'dd')# GT 12>
                <cfset fh_fin = #dateformat(fh_fin,'yyyy-mm-dd')#>
            <cfelse>
                <cfset fh_fin = #dateformat(fh_fin,'yyyy-dd-mm')#>
            </cfif>
            
        </cfif> 
         

        <cfif #id_Proveedor# EQ ''>
            <cfset #id_Proveedor# = 'NULL'>
        </cfif>
        
        <cftry>
            <cfquery name="RS" datasource="#cnx#">
              
                    EXEC Reporte_Compras_proveedor #id_Empresa#, #id_Obra#, #id_Proveedor#,
                                                                                <cfif #fh_inicio# EQ ''>
                                                                                    NULL,
                                                                                <cfelse>
                                                                                    '#fh_inicio#',    
                                                                                </cfif>
                                                                                <cfif #fh_fin# EQ ''>
                                                                                    NULL,
                                                                                <cfelse>
                                                                                     '#fh_fin#',

                                                                                </cfif>
                                                                                <cfif #id_Estatus# EQ ''>
                                                                                    NULL
                                                                                <cfelse>
                                                                                     #id_Estatus#

                                                                                </cfif>

            
            </cfquery>

            <cfset myResult.SUCCESS = true> 
            <cfset myResult.RS = Rs>
            <cfcatch type="database">
                <cfset myResult.SUCCESS = false>    
                <cfset myResult.MESSAGE = 'Error en acceso a base de datos, #cfcatch.Detail#'>
            </cfcatch>  
        </cftry>
        <cfreturn myResult>
    </cffunction>
	
	<cffunction name="OC_por_Insumos" access="remote" returntype="string" returnFormat="JSON">
        <cfargument name="id_Empresa" type="numeric" default="">
        <cfargument name="id_Obra" type="numeric" default="">
        <cfargument name="id_Insumo" type="string" default="">
       
            <cfquery name="RS" datasource="#cnx#">                
                 EXEC Ordenes_Compra_por_insumo #id_Empresa#, #id_Obra#, '#id_Insumo#'
            </cfquery> 
            
            <cfinvoke component="#Application.componentes#/funciones" method="queryToJson" returnVariable="JSON" query="#RS#">
        
            <cfreturn JSON>
            
    </cffunction>
    <cffunction name="OC_por_Agrupador" access="remote" returntype="string" returnFormat="JSON">
        <cfargument name="id_Empresa" type="numeric" default="">
        <cfargument name="id_Obra" type="string" default="">
        <cfargument name="id_Agrupador" type="string" default="">

            <cfquery name="RS" datasource="#cnx#">                
                 EXEC Ordenes_Compra_por_Agrupador #id_Empresa#, <cfif #id_Obra# eq ''>NULL,<cfelse>#id_Obra#,</cfif>'#id_Agrupador#'
            </cfquery> 
            
            <cfinvoke component="#Application.componentes#/funciones" method="queryToJson" returnVariable="JSON" query="#RS#">
        
            <cfreturn JSON>
            
    </cffunction>
    
    <cffunction name="OC_por_Factura" access="remote" returntype="string" returnFormat="JSON">
        <cfargument name="id_Empresa" type="string" default="">
        <cfargument name="id_Obra" type="string" default="">
        <cfargument name="id_Agrupador" type="string" default="">
        <cfargument name="id_ordenCompra" type="string" default="">
            <cfquery name="RS" datasource="#cnx#">                
                 EXEC Ordenes_Compra_Facturas #id_Empresa#, <cfif #id_Obra# eq ''>NULL,<cfelse>#id_Obra#,</cfif> '#id_Agrupador#',#id_ordenCompra#
            </cfquery> 
            <cfdump var="RS">
            <cfinvoke component="#Application.componentes#/funciones" method="queryToJson" returnVariable="JSON" query="#RS#">
        
            <cfreturn JSON>
            
    </cffunction>
	
</cfcomponent>