<cfcomponent extends='conexion'>
	<!---Listado de Agrupadores-------------------------------------->
    <!---Desarrollador: Miguel Tiznado ------------------------------>
    <!---Fecha: 19/04/2015------------------------------------------->
    
   <cffunction name="get_Agrupadores_Fe_Listado" access="public" returntype="struct">
		<cfargument name="id_Agrupador"	type="string" required="false" default="NULL">
		<cfif id_Agrupador EQ ''>
			<cfset id_Agrupador = 'NULL'>
		</cfif>
		<cftry>
			<cfset Result.SQL = "
			SELECT 
				* 
			FROM 
				Agrupadores_Fe 
			WHERE 
				id_Agrupador = ISNULL( #id_Agrupador#, id_Agrupador)">
			<cfquery name="RSDatos" datasource="#session.cnx#">                
				#PreserveSingleQuotes(Result.SQL)#
            </cfquery>
            <cfset Result.RS = RSDatos>
            <cfset Result.SUCCESS = true>
            <cfset Result.MESSAGE = 'Datos obtenidos con exito'>
            <cfreturn result>
         	<cfcatch type='database'>
                <cfset Result.SUCCESS = false>
				<cfset Result.TipoError = "Error en la consulta de base de datos.">
                <cfset Result.MESSAGE = "Error al recuperar los datos => #cfcatch.Detail#">    
                <cfreturn Result>
            </cfcatch>
        </cftry>
    </cffunction>
    
	
	<!---Funcion para obtener listado de detalles de cotizaciones---->
    <!---Desarrollador: Miguel Tiznado ------------------------------>
    <!---Fecha: 19/04/2015------------------------------------------->
	<cffunction name="get_RequisicionesCotizacionesDetalle_Listado" access="public" returntype="struct">
        <cfargument name="id_Empresa" 		type="string" required="true">
		<cfargument name="id_Obra" 			type="string" required="true">
		<cfargument name="id_Requisicion"	type="string" default="">
		<cfargument name="id_Proveedores" 	type="string" required="true"><!---ids separados por una coma ',' --->
		<cfargument name="id_Agrupador" 	type="string" required="true">
		<cftry>
			<cfset id_Agrupador = IIF(id_Agrupador EQ '', DE('NULL'), DE("'#id_Agrupador#'"))>
			<cfset Result.SQL = "
				EXECUTE RequisicionesCotizacionesDetalle_Listado
							@id_Empresa		= #id_Empresa#,
							@id_Obra		= #id_Obra#,
							@id_Requisicion = NULL,
							@id_Proveedor	= NULL,
							@id_Agrupador	= #id_Agrupador#
				">
            <cfquery name="RSAux" datasource="#session.cnx#">                
                #PreserveSingleQuotes(Result.SQL)#
            </cfquery>
            <!--- Filtro de proveedores --->
            <cfquery name="RSDatos" dbtype="query">                
                SELECT 
					* 
				FROM
					RSAux
				<cfif id_Proveedores NEQ ''>
					WHERE 
						id_Proveedor IN (#id_Proveedores#)
				</cfif>
            </cfquery>
            <cfset Result.RS = RSDatos>
			<cfset Result.SUCCESS = true>
            <cfset Result.MESSAGE = 'Datos obtenidos con exito'>
            <cfreturn result>
         	<cfcatch type='database'>
                <cfset Result.SUCCESS = false>
				<cfset Result.TipoError = "Error en la consulta de base de datos.">
                <cfset Result.MESSAGE = "Error al recuperar los datos => #cfcatch.Detail#">    
                <cfreturn Result>
            </cfcatch>
        </cftry>
    </cffunction>


	
	<cffunction name="Rs_Listado_compras" access="remote">
		<cfargument name="id_Empresa"   type="string" required="yes" default="">
		<cfargument name="id_obra"      type="string" required="yes" default="">
		<cfargument name="tipo_obra" 	type="string" required="yes" default="">
		<cfargument name="Estatus" 		type="string" required="yes" default="">
		<cfargument name="id_Agrupador" type="string" required="yes" default="">
	    <cfargument name="insumo" 		type="string" required="yes" default="">
	    <cfargument name="id_Empleado"  type="string" required="yes" default="">

		
		<cfquery datasource='#session.cnx#' name='RsTipo'>
            select 
              count(*) as tipo 
            from 
              Obras 
            where 
              id_tipoobra=0 AND id_Empresa=#id_Empresa# AND id_obra=#id_obra#

    	</cfquery>
    	<cfif #RsTipo.tipo# EQ 1>
    		<cfquery name="RS" datasource="#cnx#">
				EXECUTE LISTADO_ORDENES_COMPRA_Indirectos 
					@id_Obra = #IIF(id_obra NEQ "",id_obra,DE('NULL'))#,                                                
					@id_Empresa = #IIF(id_Empresa NEQ "",id_Empresa,DE('NULL'))#,                                                 
					@id_TipoObra = #IIF(tipo_obra NEQ "",tipo_obra,DE('NULL'))#,                                               
					@id_Estatus = #IIF(Estatus NEQ "",Estatus,DE('NULL'))#,                                                
					@id_Agrupador = #IIF(id_Agrupador NEQ "",DE("'#id_Agrupador#'"),DE('NULL'))#,
					@insumo=#IIF(insumo NEQ "",DE("'#insumo#'"),DE('NULL'))#,
					@id_Empleado=#id_Empleado#

			</cfquery>
    	<cfelse>
    		<cfquery name="RS" datasource="#cnx#">
				EXECUTE LISTADO_ORDENES_COMPRA 
					@id_Obra = #IIF(id_obra NEQ "",id_obra,DE('NULL'))#,                                                
					@id_Empresa = #IIF(id_Empresa NEQ "",id_Empresa,DE('NULL'))#,                                                 
					@id_TipoObra = #IIF(tipo_obra NEQ "",tipo_obra,DE('NULL'))#,                                               
					@id_Estatus = #IIF(Estatus NEQ "",Estatus,DE('NULL'))#,                                                
					@id_Agrupador = #IIF(id_Agrupador NEQ "",DE("'#id_Agrupador#'"),DE('NULL'))#,
					@insumo=#IIF(insumo NEQ "",DE("'#insumo#'"),DE('NULL'))#,
					@id_Empleado=#id_Empleado#
			</cfquery>	
    	</cfif>
		
		<cfset json='['>
		<cfquery name="req" dbtype="query">
			select id_agrupador
			from RS
			WHERE 1 = 1
		 	<cfif Arguments.id_Agrupador NEQ ''><!---Esta condicion es para que el filtro de agrupador se respete--->
				AND id_Agrupador = '#id_Agrupador#'
			</cfif>
			group by id_agrupador
		</cfquery>
		<cfset aux=0>	
		<cfset index=0>  
		<cfoutput query="RS" group="id_Agrupador">
			<!---Esta condicion es para que el filtro de agrupador se respete--->
			<cfif (Arguments.id_Agrupador EQ '') OR (Arguments.id_Agrupador EQ RS.id_Agrupador)>
				<cfset index=index+1>	
				<cfquery name="agru" dbtype="query">
					select sum(nu_solicitado) as nu_solicitado_agrup,
						sum(im_subtotal_solicitado) as im_solicitado_agrup,
						sum(nu_comprado) as nu_comprado_agrup,
						sum(im_subtotal_com) as im_comp_agrup,AVG(im_precio_com) as im_comp_precio_agrup,
						sum(nu_pendiente)as nu_pendiente_agrup,sum(im_subtotal_pendiente) as im_pendiente_agrup
					from RS
					where id_Agrupador='#RS.id_Agrupador#'
					group by id_Agrupador
				</cfquery>
				<cfquery name="agru2" dbtype="query">
					select AVG(im_precio_com) as im_comp_precio_agrup
					from RS
					where id_Agrupador='#RS.id_Agrupador#' AND im_precio_com > 0
					group by id_Agrupador
				</cfquery>
				
				<cfif #agru2.im_comp_precio_agrup# EQ ''>
					<cfset im_com=0>	
					
				<cfelse>	
					<cfset im_com=agru2.im_comp_precio_agrup>
				</cfif>
				
				<cfset json =json&'{"id":'&index&',"clave":"'&RS.id_Agrupador&'","descripcion":"'&RS.de_Agrupador&'","UM":"'&replace(RS.UM,'"','�','all')&'","Cantida_ppto":'&RS.nu_cantidad&',"im_precio_ppto":'&RS.im_precio&',"im_subtotal_ppto":'&RS.im_subtotal&',"nu_solicitado":'&agru.nu_solicitado_agrup&',"nu_comprado":'&agru.nu_comprado_agrup&',"im_precio_com":'&im_com&',"im_subtotal_com":'&agru.im_comp_agrup&',"nu_pendiente":'&agru.nu_pendiente_agrup&',"nu_disponible":'&RS.nu_cantidad-agru.nu_comprado_agrup&',"insumos":['>
				<cfquery name="insumos" dbtype="query">
					select id_insumo
					from RS
					where id_agrupador='#RS.id_agrupador#'
				</cfquery>
				<cfset aux1=0>
				<cfoutput group="id_insumo">
				<cfif #aux1# EQ #insumos.recordCount#-1>
					<cfset json=json&'{"id":"'&RS.id_insumo&'","requisicion":"'&RS.id_requisicion&'","Centros":"'&RS.id_CC&'","nombre":"'&replace(RS.de_insumo,'"','¦','all')&'","UM":"'&replace(RS.UM,'"','¦','all')&'","de_MedidaEspecial":"'&LTrim(replace(RS.de_MedidaEspecial,'"','¦','all'))&'","Nu_Tramos":'&RS.Nu_Tramos&',"nu_solicitado":'&RS.nu_solicitado&',"imp_solicitado":'&RS.im_subtotal_solicitado&',"nu_comprado":'&RS.nu_comprado&',"im_precio_com":'&RS.im_precio_com&',"im_subtotal_com":'&RS.im_subtotal_com&',"nu_pendiente":'&RS.nu_pendiente&',"im_subtotal_pendiente":'&RS.im_subtotal_pendiente&'}'>
				<cfelse>
					<cfset json=json&'{"id":"'&RS.id_insumo&'","requisicion":"'&RS.id_requisicion&'","Centros":"'&RS.id_CC&'","nombre":"'&replace(RS.de_insumo,'"','¦','all')&'","UM":"'&replace(RS.UM,'"','¦','all')&'","de_MedidaEspecial":"'&LTrim(replace(RS.de_MedidaEspecial,'"','¦','all'))&'","Nu_Tramos":'&RS.Nu_Tramos&',"nu_solicitado":'&RS.nu_solicitado&',"imp_solicitado":'&RS.im_subtotal_solicitado&',"nu_comprado":'&RS.nu_comprado&',"im_precio_com":'&RS.im_precio_com&',"im_subtotal_com":'&RS.im_subtotal_com&',"nu_pendiente":'&RS.nu_pendiente&',"im_subtotal_pendiente":'&RS.im_subtotal_pendiente&'},'>
				</cfif>
					<cfset aux1 = aux1+1>			
				</cfoutput>
				<cfset json=json&']'>
				<cfif #aux# EQ #req.recordCount#-1>
					<cfset json=json&'}'>
				<cfelse>
					<cfset json=json&'},'>
				</cfif>
				<cfset aux=aux+1>
			</cfif>
		</cfoutput>
		<cfset json=json&']'>
		<cfoutput>
			#json#
		</cfoutput>
	</cffunction>
	
	<cffunction name="proveedores" access="remote" returntype="QUERY" returnformat="JSON">
		<cfargument name="id_insumo" type="string" required="yes">
		<cfquery name="RS_PROV" datasource="#cnx#">
			SELECT P.id_Proveedor,P.nb_Proveedor
			FROM PROVEEDORES P 
			inner join InsumosProveedores IP on  P.id_Proveedor=IP.id_Proveedor
			where IP.id_Insumo='#id_insumo#' AND P.id_Proveedor>0 and p.sn_activo=1
			ORDER BY nb_Proveedor ASC
		</cfquery>
		<cfreturn RS_PROV>
	</cffunction>

	 <cffunction name="obtenerDetallesInsumo" access="remote" returntype="string" returnformat="JSON">
	 			<cfargument name="id_Empresa" type="string" required="yes">
	 			<cfargument name="id_Obra" type="string" required="yes">
	 			<cfargument name="id_requisicion" type="string" required="yes">
	 			<cfargument name="id_insumo" type="string" required="yes">

			
				<cfquery name="RS" datasource="#cnx#">
					SELECT 
						rd.id_Requisicion,rd.id_Insumo,r.fh_Requisicion,SUM(rd.nu_Cantidad) as nu_cantidad,SUM(rd.nu_PorComprar) as nu_PorComprar,
						E.nb_Nombre+' '+E.nb_ApellidoPaterno+' '+E.nb_ApellidoMaterno as solicitante 
					FROM 
						requisiciones r 
						INNER JOIN requisicionesdetalle rd on
							r.id_Empresa=rd.id_Empresa AND
							r.id_Obra=rd.id_Obra AND
							r.id_Requisicion=rd.id_Requisicion
						INNER JOIN Empleados E on
							r.id_Empleadosolicito=E.id_Empleado
					WHERE 
						r.id_Empresa=#id_Empresa# AND
						r.id_obra=#id_Obra# AND
						r.id_requisicion in(#id_requisicion#) AND
						rd.id_Insumo='#id_insumo#' AND
						rd.id_estatus not in (102) AND
						r.id_estatus not in(202,201)
					GROUP BY 
						R.ID_TIPOREQUISICION,rd.id_Requisicion,rd.id_Insumo,r.fh_Requisicion,E.nb_Nombre,E.nb_ApellidoPaterno,E.nb_ApellidoMaterno 	
						
				</cfquery>
				
				<cfinvoke component="#Application.componentes#/funciones" method="queryToJson" returnVariable="JSON" query="#RS#">
				

				<cfreturn JSON>
				
		
	
	</cffunction> 

	<cffunction name="RS_Inserta_Orden_compra" access="remote" returntype="struct" returnformat="JSON" output="yes">
		<cfargument name="id_Empresa"     		type="string" required="true">
		<cfargument name="id_Obra"        		type="string" required="true">
		<cfargument name="id_empleado"    		type="string" required="true">
		<cfargument name="nb_Recibe"     		type="string" required="true">
		<cfargument name="nu_Telefono"   		type="string" required="true">
		<cfargument name="de_Domicilio"   		type="string" required="true">
		<cfargument name="de_Comentarios" 		type="string" default="">
		<cfargument name="fh_entrega" 	  		type="string" required="true">
		<cfargument name="Medio"          		type="string" required="true">
		<cfargument name="G_Maniobra"     		type="string" required="true">
		<cfargument name="tipo_cambio"    		type="string" required="true">
		<cfargument name="pj_anticipo"    		type="string" required="true">
		<cfargument name="iva"            		type="string" required="true">
		<cfargument name="id_Almacen"     		type="string" required="true">
		<cfargument name="id_moneda"     		type="string" required="true">
		<cfargument name="proveedores" 			type="string" required="true" hint="Arreglo con todos los proveedores."/>
		<cfargument name="insumos" 				type="string" required="true" hint="Arreglo con todos los insumos."/>
		<cfargument name="cantidades" 			type="string" required="true" hint="Arreglo con todos las cantidades."/>
		<cfargument name="precios" 				type="string" required="true" hint="Arreglo con todos los precios."/>
		<cfargument name="subtotal" 			type="string" required="true" hint="Arreglo con todos los subtotales."/>
		<cfargument name="Centros" 	       		type="string" required="true" hint="Arreglo con todos los centros costos."/>
		<cfargument name="Requisiciones" 		type="string" required="true" hint="Arreglo con todos las requisiciones."/>
		<cfargument name="de_Detalles" 			type="string" required="true">
		<cfargument name="id_TipoOrdenCompra"	type="string" required="true"> 
		<cfargument name="im_Retencion" 	  	type="string" default="0"> 
		
		<cfset de_Details = ListToArray( de_Detalles, '¦')>
		
		<cfset centros=listToArray(Centros, "_")>
		
		<cfset prov= ListToArray( proveedores, ',' )>
		<cfset ins= ListToArray( insumos, ',' )>	
		<cfset cant=listToArray(cantidades,',')>
		<cfset prec=listToArray(precios,',')>
		<cfset array_requisiciones=listToArray(Requisiciones,'-')>
		
			
		<cfset im_Retencion = (LSParseNumber(im_Retencion))>
		
	 	<cfif #dateformat(fh_entrega,'dd')# GT 12>
			<cfset fh_final = #dateformat(fh_entrega,'yyyy-mm-dd')#>
		<cfelse>
    		<cfset fh_final = #dateformat(fh_entrega,'yyyy-dd-mm')#>
		</cfif>
		<cfif #pj_anticipo# EQ ''>
			<cfset pj_anticipo=0>
		</cfif>
		<cfif #G_Maniobra# EQ ''>
			<cfset G_Maniobra=0>
		</cfif>
		
		
		<cftransaction>
			<!--- 	Calculamos el total ya con iva incluido y la suma de los gastos maniobra --->
			<cfset subtotal_iva=(LSParseNumber(subtotal)+LSParseNumber(G_Maniobra))*(LSParseNumber(iva)/100)>
			<cfset total=subtotal_iva+(LSParseNumber(subtotal)+LSParseNumber(G_Maniobra))>
			<cfset anticipo=((LSParseNumber(pj_anticipo)/100)*LSParseNumber(total))>
			
			
			<cfquery name="inserta_OC" datasource="#cnx#">
				DECLARE @id_OrdenCompra INT,@id_EmpleadoSolicito INT
				SET @id_EmpleadoSolicito = (select top 1 id_Responsable from obras where id_Obra = #id_Obra#)
				SET @id_OrdenCompra = (select isnull(max(id_OrdenCompra)+1,1) from OrdenesCompra where id_Empresa = #id_Empresa# and id_Obra = #id_Obra#)
				INSERT INTO OrdenesCompra([id_Empresa],[id_Obra],[id_OrdenCompra],[id_Requisicion],[fh_OrdenCompra],[id_Proveedor],[id_EmpleadoComprador]
					,[id_EmpleadoSolicito],[id_EmpleadoAutorizo],[id_EmpleadoVoBo],[id_Almacen],[id_TipoOrdenCompra],[id_Moneda]
					,[im_TipoCambio],[im_SubTotal],[pj_IVA],[im_Retencion],[im_Total],[im_Ajustado],[id_Estatus],
					[nb_Recibe],
					[nu_Telefono],
					[de_Domicilio],
					[de_Comentarios],
					[id_MedioEnvio],
					[im_GastosManiobra],
					[pj_Anticipo],
					[im_Anticipo],
					[id_proveedor_contratista])
				VALUES(#id_Empresa#,#id_Obra#,@id_OrdenCompra,NULL,getDate(),#prov[1]#,
					#id_empleado#,@id_EmpleadoSolicito,#id_empleado#,#id_empleado#,#id_Almacen#,#id_TipoOrdenCompra#,#id_moneda#,#tipo_cambio#,
					#subtotal#,#iva#,#im_Retencion#,(#total#*#tipo_cambio#)-#im_Retencion#,NULL,203,'#UCASE(nb_Recibe)#', #UCASE(nu_Telefono)#,'#UCASE(de_Domicilio)#','#UCASE(de_Comentarios)#',#medio#,#G_Maniobra#,#pj_anticipo#,#anticipo#,NULL)
				
				SELECT @id_OrdenCompra as id_OrdenCompra
			</cfquery>
			<cfset aux = 1>
			<cfset ind = 0>
			<!--- Recorremos los insumos que se han comprado --->
			<cfloop array="#ins#" index="i">
				<cfset ind +=1 >

				
				<cfset detail=replace("#de_Details[ind]#",'Â','','all')>
				<!--- Insertamos el detalle y la orden de compra segun el insumo que va en el recorrido --->
				<cfquery name="inserta_OCD" datasource="#cnx#">
					DECLARE @nd_OrdenCompra INT
					SET @nd_OrdenCompra = (select isnull(max(nd_OrdenCompra)+1,1) from OrdenesCompraDetalle
					where id_Empresa = #id_Empresa# and id_Obra = #id_Obra# and id_OrdenCompra = #inserta_OC.id_OrdenCompra#)	
					
					INSERT INTO OrdenesCompraDetalle
						([id_Empresa],[id_Obra],[id_OrdenCompra],[nd_OrdenCompra],[id_CentroCosto],[id_Insumo],[fh_Entrega],
						[nu_Cantidad],[nu_CantidadAjustada],[im_Precio],[im_PrecioPresupuesto],[im_CantidadPrecio],[id_Marca], [id_Frente], [id_Partida], de_Detalle)
					VALUES
						(#id_Empresa#,#id_Obra#,#inserta_OC.id_OrdenCompra#,@nd_OrdenCompra,900,'#i#','#fh_final#',
						#cant[aux]#,0,#prec[aux]#,NULL, #cant[aux]# * #prec[aux]#,NULL, 901 , 1, '#UCASE( IIF( de_Details[ind] EQ '!', DE(''), DE('#de_Details[ind]#') ) )#')	
																																							
				</cfquery>																																				
				
				<!--- Creamos un array de los centros costos frentes y partidas que corresponden al insumo --->
				<cfset cen=listToArray(  centros[#aux#], "-")>
				<!--- Creamos un array de las requisiciones que corresponden al insumo --->
				<cfset Req=listToArray(array_requisiciones[#aux#],',')>
				<!--- 	<cfset nu_comprado =#cant[aux]#> --->
				<cfset aux2=1>
				<!--- inserta en requisiciones_ordenesCompra para hacer el barrido de cancelacion.--->
				<cfset comprado=#cant[aux]#>
				<cfloop array="#Req#" index="j">
					<cfif comprado LTE 0>
						<cfbreak>
					</cfif>
					<cfset centros_insertar=listToArray(#cen[aux2]#, ",")>
					<cfset aux2=aux2+1>
					<cfquery name="actualiza_nu_porComprar" datasource="#cnx#">
							SELECT 
								nd_Requisicion,id_Requisicion,nu_cantidad,nu_PorComprar,fh_requerido 
							FROM 
								requisicionesdetalle 
							WHERE 
								id_Obra=#id_Obra# 
								AND id_Empresa=#id_Empresa# 
								AND id_Insumo='#i#' 
								AND id_requisicion=#j# 
								AND id_centroCosto=#centros_insertar[1]# 
								AND id_Partida=#centros_insertar[3]# 
								AND id_Frente=#centros_insertar[2]#
								AND id_Frente=#centros_insertar[2]# 
								AND id_Estatus in (101,104)
								AND nu_PorComprar > 0	
							ORDER BY fh_Requerido ASC	
					</cfquery>
					
					<!--- Calculamos el nd detalle que insertaremos en Requisiciones_ordenesCompra --->
					<cfquery name="nd_detalle" datasource="#cnx#">
							SELECT 
								ISNULL(MAX(nd_requisicion)+1,1) as nd_requisicion 
							FROM 
								Requisiciones_OrdenesCompra
							WHERE id_Empresa=#id_Empresa# AND id_Obra=#id_Obra# AND id_OrdenCompra=#inserta_OC.id_OrdenCompra#
					</cfquery>
					
					<cfloop query="actualiza_nu_porComprar">
							<cfif #actualiza_nu_porComprar.nu_PorComprar# GT 0>
								<!--- variable auxiliar si preguntara si ya se acabo lo comprado para ese insumo si no se ha terminado seguira con el siguiente registro con disponible para  comprar--->
								<cfset requisicion_OC=comprado>
								<cfset comprado=(comprado-actualiza_nu_porComprar.nu_PorComprar)>
								<cfif #comprado# LT 0>	
								<!--- 	Si la diferencia de lo comprado con lo disponible para solicitar es menor que 0 se significa que quedo cantidad disponible para esa requisicion centro costo, frente,partida e insumo --->
								<!---   Le aplicamos un absoluto ya que la diferencia daria negativa y guardaremos la cantidad e nu_insertar para actualizar el nu_PorComprar del detalle --->
									<cfset nu_insertar=#abs(comprado)#>
									<cfset id_estatus=104>

									<cfquery name="inserta_nu_por_comprar" datasource="#cnx#">
										UPDATE requisicionesdetalle set nu_PorComprar=#nu_insertar#,id_Estatus=#id_estatus#
										where 
											id_Obra=#id_Obra# AND
											id_Empresa=#id_Empresa# AND
											id_Insumo='#i#' AND
											nd_Requisicion=#actualiza_nu_porComprar.nd_Requisicion# AND 
											id_Requisicion=#actualiza_nu_porComprar.id_requisicion# 
										
										UPDATE            
											Requisiciones           
											   SET  id_Estatus=          
											        
											   -- proceso orginal:            
											   -- CASE            
											   --  WHEN (nu_PorComprar - ocd.nu_Cantidad) > 0 THEN (nu_PorComprar - ocd.nu_Cantidad)            
											   --  ELSE 0            
											   -- END  ,
												CASE 
														WHEN
															(
															   SELECT sum(rde.nu_Cantidad)
															   FROM            
																	RequisicionesDetalle rde          
														  	
																WHERE
																--Filtro de id_requisicion            
																rde.id_Empresa= #id_Empresa# AND            
																rde.id_Obra=#id_Obra# AND            
																rde.id_Requisicion=r.id_Requisicion  
															 ) is null
														THEN 
															201
														WHEN 
															( select SUM(rde.nu_Cantidad)-SUM(rde.nu_PorComprar)
															   FROM            
																	RequisicionesDetalle rde          
														  			---Filtro requisiciones
																WHERE
																	--Filtro de id_requisicion            
																	rde.id_Empresa=#id_Empresa#  AND            
																	rde.id_Obra=#id_Obra# AND            
																	rde.id_Requisicion=r.id_Requisicion   
																 
																) = 0
														THEN 203          
														WHEN 
														( SELECT ISNULL(SUM(rde.nu_PorComprar),0)
														   FROM            
																RequisicionesDetalle rde          
														  	
															WHERE
																--Filtro de id_requisicion            
																rde.id_Empresa=#id_Empresa# AND            
																rde.id_Obra=#id_Obra# AND            
																rde.id_Requisicion=r.id_Requisicion  
															) = 0
														THEN 103
														ELSE
															104
																		
													  END  
											   FROM            
												
												Requisiciones r left join
												RequisicionesDetalle rde on 
												r.id_Requisicion=rde.id_Requisicion  AND
												r.id_Empresa=rde.id_Empresa AND
												r.id_Obra=rde.id_Obra    
											   where
											    --Filtro de requisiciones
											    r.id_Empresa=#id_Empresa# AND
											    r.id_Obra=#id_Obra# AND
											    r.id_requisicion=#actualiza_nu_porComprar.id_requisicion# 
												
									

									</cfquery>
									
								<cfelse>
									<cfset nu_insertar=0>
									<cfset id_estatus=103>

									<cfquery name="inserta_nu_por_comprar" datasource="#cnx#">
										UPDATE requisicionesdetalle set nu_PorComprar=#nu_insertar#,id_Estatus=#id_estatus#
										where 
											id_Obra=#id_Obra# AND
											id_Empresa=#id_Empresa# AND
											id_Insumo='#i#' AND
											nd_Requisicion=#actualiza_nu_porComprar.nd_Requisicion# AND 
											id_Requisicion=#actualiza_nu_porComprar.id_requisicion# 
										
											UPDATE            
												Requisiciones           
											   SET  id_Estatus=          
											        
											   -- proceso orginal:            
											   -- CASE            
											   --  WHEN (nu_PorComprar - ocd.nu_Cantidad) > 0 THEN (nu_PorComprar - ocd.nu_Cantidad)            
											   --  ELSE 0            
											   -- END  ,
												CASE 
														WHEN
															(
															   SELECT sum(rde.nu_Cantidad)
															   FROM            
																	RequisicionesDetalle rde          
														  	
																WHERE
																--Filtro de id_requisicion            
																rde.id_Empresa= #id_Empresa# AND            
																rde.id_Obra=#id_Obra# AND            
																rde.id_Requisicion=r.id_Requisicion  
															 ) is null
														THEN 
															201
														WHEN 
															( select SUM(rde.nu_Cantidad)-SUM(rde.nu_PorComprar)
															   FROM            
																	RequisicionesDetalle rde          
														  			---Filtro requisiciones
																WHERE
																	--Filtro de id_requisicion            
																	rde.id_Empresa=#id_Empresa#  AND            
																	rde.id_Obra=#id_Obra# AND            
																	rde.id_Requisicion=r.id_Requisicion   
																 
																) = 0
														THEN 203          
														WHEN 
														( SELECT ISNULL(SUM(rde.nu_PorComprar),0)
														   FROM            
																RequisicionesDetalle rde          
														  	
															WHERE
																--Filtro de id_requisicion            
																rde.id_Empresa=#id_Empresa# AND            
																rde.id_Obra=#id_Obra# AND            
																rde.id_Requisicion=r.id_Requisicion  
															) = 0
														THEN 103
														ELSE
															104
																		
													  END  
											   FROM            
												
												Requisiciones r left join
												RequisicionesDetalle rde on 
												r.id_Requisicion=rde.id_Requisicion  AND
												r.id_Empresa=rde.id_Empresa AND
												r.id_Obra=rde.id_Obra    
											   where
											    --Filtro de requisiciones
											    r.id_Empresa=#id_Empresa# AND
											    r.id_Obra=#id_Obra# AND
											    r.id_requisicion=#actualiza_nu_porComprar.id_requisicion#   
									
									</cfquery>
									<cfbreak>
									
								</cfif>
						</cfif>

					</cfloop>
					
					<cfif #comprado# GTE 0>
						<!--- Si es mayor que 0 significa que se ha comprado un excedente del insumo
								se inserta el insumo comprar de ese insumo en esa requisicion CC Frente y partida
							   y continua en caso de de que quede comprado dispoonible por quemar  --->
						<cfquery name="inserta_nu_por_comprar" datasource="#cnx#">
								INSERT INTO Requisiciones_OrdenesCompra
									(
									[id_Empresa],
									[id_Obra],
									[id_OrdenCompra],
									[id_requisicion],
									[nd_requisicion],
									[id_insumo],
									[nu_comprado],
									[id_centroCosto],
									[id_Frente],
									[id_Partida],
									[im_Precio],
									[id_Estatus]
									)
								VALUES(	
									#id_Empresa#,
									#id_Obra#,
									#inserta_OC.id_OrdenCompra#,
									#j#,
									#nd_detalle.nd_requisicion#,
									'#i#',
									#actualiza_nu_porComprar.nu_PorComprar#,
									#centros_insertar[1]#,
									#centros_insertar[2]#,
									#centros_insertar[3]#,
									#prec[aux]#,
									103
									)
						</cfquery>
						
					<cfelseif #comprado# LT 0 >

						<cfquery name="inserta_nu_por_comprar" datasource="#cnx#">

								INSERT INTO Requisiciones_OrdenesCompra
									(
									[id_Empresa],
									[id_Obra],
									[id_OrdenCompra],
									[id_requisicion],
									[nd_requisicion],
									[id_insumo],
									[nu_comprado],
									[id_centroCosto],
									[id_Frente],
									[id_Partida],
									[im_Precio],
									[id_Estatus]
									)
								VALUES(	
									#id_Empresa#,
									#id_Obra#,
									#inserta_OC.id_OrdenCompra#,
									#j#,
									#nd_detalle.nd_requisicion#,
									'#i#',
									#requisicion_OC#,
									#centros_insertar[1]#,
									#centros_insertar[2]#,
									#centros_insertar[3]#,
									#prec[aux]#,
									103
								)
						</cfquery>  
						<cfbreak>
					</cfif>		
				
				</cfloop>
				<cfset aux += 1>					
			</cfloop>
			
			<cfquery datasource='#cnx#' name='RsTipo'>
	      		select 
	      			count(*) as tipo 
	      		from 
	      			Obras 
	      		where 
	      			id_tipoobra=0 AND id_Empresa=#id_Empresa# AND id_obra=#id_obra#

      		</cfquery>

      		<cfif #RsTipo.tipo# EQ 1>
	      		<cfquery name="actualiza_nuCompradoContratado" datasource="#cnx#">
							

					UPDATE ExplosionInsumos SET

					   nu_CompradoContratado = 
					         ISNULL(nu_CompradoContratado, 0) 
					         + 
					           (
						          SELECT 
						           SUM(nu_comprado) 
						          FROM requisiciones_ordenesCompra
						          WHERE 
						           id_Empresa= explo.id_Empresa AND
						           id_Obra=explo.id_Obra AND
						           id_OrdenCompra=#inserta_OC.id_OrdenCompra# AND
						           id_CentroCosto= explo.id_CentroCosto AND
						           id_Frente=explo.id_Frente  AND
						           id_Partida=explo.id_Partida AND
						           id_Insumo = ocd.id_Insumo AND
						           id_Estatus not in(102)
					         	)
					         ,
					         
					   im_CompradoContratado = 
					         
					         ISNULL(im_CompradoContratado, 0) 
					         + 
					         ( 
						          SELECT 
						           SUM(nu_comprado) 
						          FROM 
						           requisiciones_ordenesCompra
						          WHERE 
						           id_Empresa= explo.id_Empresa AND
						           id_Obra=explo.id_Obra AND
						           id_OrdenCompra=#inserta_OC.id_OrdenCompra# AND
						           id_CentroCosto= explo.id_CentroCosto AND
						           id_Frente=explo.id_Frente  AND
						           id_Partida=explo.id_Partida AND
						           id_Insumo = ocd.id_insumo AND
						           id_Estatus not in(102)

					         )  * ocd.im_precio   
					      FROM 
					       ExplosionInsumos explo
					      INNER JOIN requisiciones_ordenesCompra ocd ON
					       explo.id_Empresa = ocd.id_Empresa AND
					       explo.id_Obra = ocd.id_Obra AND
					       explo.id_CentroCosto= ocd.id_CentroCosto and 
					       explo.id_Frente = ocd.id_Frente and 
					       explo.id_Partida = ocd.id_Partida and 
					       explo.id_Insumo =  'GAO-001'
					              
					      WHERE
					       ocd.id_Empresa= #id_Empresa# AND
					       ocd.id_Obra= #id_Obra# AND
					       ocd.id_OrdenCompra =#inserta_OC.id_OrdenCompra#
				</cfquery>
      		<cfelse>	
      			<cfquery name="actualiza_nuCompradoContratado" datasource="#cnx#">
						

				UPDATE ExplosionInsumos SET

				   nu_CompradoContratado = 
				         ISNULL(nu_CompradoContratado, 0) 
				         + 
				           (
					          SELECT 
					           SUM(nu_comprado) 
					          FROM requisiciones_ordenesCompra
					          WHERE 
					           id_Empresa= explo.id_Empresa AND
					           id_Obra=explo.id_Obra AND
					           id_OrdenCompra=#inserta_OC.id_OrdenCompra# AND
					           id_CentroCosto= explo.id_CentroCosto AND
					           id_Frente=explo.id_Frente  AND
					           id_Partida=explo.id_Partida AND
					           id_Insumo = ocd.id_Insumo AND
					           id_Estatus not in(102)
				         	)
				         ,
				         
				   im_CompradoContratado = 
				         
				         ISNULL(im_CompradoContratado, 0) 
				         + 
				         ( 
					          SELECT 
					           SUM(nu_comprado) 
					          FROM 
					           requisiciones_ordenesCompra
					          WHERE 
					          id_Empresa= explo.id_Empresa AND
					           id_Obra=explo.id_Obra AND
					           id_OrdenCompra=#inserta_OC.id_OrdenCompra# AND
					           id_CentroCosto= explo.id_CentroCosto AND
					           id_Frente=explo.id_Frente  AND
					           id_Partida=explo.id_Partida AND
					           id_Insumo = ocd.id_Insumo AND
					           id_Estatus not in(102) 
				         )  * ocd.im_precio   
				      FROM 
				       ExplosionInsumos explo
				      INNER JOIN requisiciones_ordenesCompra ocd ON
				       explo.id_Empresa = ocd.id_Empresa AND
				       explo.id_Obra = ocd.id_Obra AND
				       explo.id_CentroCosto= ocd.id_CentroCosto and 
				       explo.id_Frente = ocd.id_Frente and 
				       explo.id_Partida = ocd.id_Partida and 
				       explo.id_Insumo =  CASE 
				               WHEN  (
				                  SELECT COUNT(*)
				                  from ExplosionInsumos
				                  WHERE id_Obra=#id_Obra# AND id_Empresa=#id_Empresa#
				                  AND id_Insumo=ocd.id_insumo  AND
				                  id_CentroCosto= ocd.id_CentroCosto and 
				                  id_Frente = ocd.id_Frente and 
				                  id_Partida = ocd.id_Partida
				                              
				                 ) = 0 THEN (
					                     SELECT 
					                     distinct I.id_Agrupador
					                     from 
					                     insumos I inner join Agrupadores_fe A ON
					                     I.id_Agrupador=i.id_Agrupador
					                     where I.id_Insumo=ocd.id_insumo   
				                    ) 
				               ELSE ocd.id_insumo  
				              END  
				              
				      WHERE
				       ocd.id_Empresa= #id_Empresa# AND
				       ocd.id_Obra= #id_Obra# AND
				       ocd.id_OrdenCompra =#inserta_OC.id_OrdenCompra#
				</cfquery>
      		</cfif>

			
		</cftransaction>	
		
			<cftry>
				<cfif #proveedores# NEQ ''>
    					<cfquery name="RsObra" datasource="#session.cnx#">
    						SELECT id_Obra,de_Obra FROM Obras (noLock) WHERE id_Empresa = #id_Empresa# AND id_Obra = #id_Obra#
    					</cfquery>
    					<cfquery name="RsEmpleado" datasource="#session.cnx#">
    						SELECT id_Empleado,nb_Nombre+' '+nb_ApellidoPaterno+' '+nb_ApellidoMaterno AS nb_Nombre, de_eMail FROM Empleados(noLock) WHERE id_Empleado = #session.id_Empleado#
   						</cfquery>

    				<cfloop array="#prov#" index="i">
        					<cfquery name="RsProveedor" datasource="#session.cnx#">
           						 SELECT de_eMail FROM Proveedores(noLock) WHERE id_Proveedor = #i#
        					</cfquery>
        					<cfsavecontent variable="contenido">
        						<table cellpadding="0" cellspacing="0" border="1">
            						<tr>
                						<td style="background-color:##999999" align="center" colspan="2">Solicitud de Orden de Compra</td>  
                					</tr>
                					<tr>
                						<td style="background-color:##CCCCCC" align="center" colspan="2">Tiene una nueva solicitud de Orden de Compra</td>
                					</tr>
                					<tr>
                						<td align="right" width="10%">Obra:&nbsp;</td>
                    					<td align="left"><cfoutput>#RsObra.id_Obra# - #RsObra.de_Obra#</cfoutput></td>
                					</tr>
                					<tr>
                						<td align="right" width="10%">Notas de Insumo</td>
                    					<td align="left"><cfoutput>#de_Detalles#</cfoutput></td>
                					</tr>
                					<tr>
                						<td align="right" width="10%">Solicitante:&nbsp;</td>
                    					<td align="left"><cfoutput>#RsEmpleado.nb_Nombre#</cfoutput></td>
                					</tr>
           					 	</table>
        					</cfsavecontent>
         					<cfinvoke component	=	"#Application.componentes#.funciones"
          					  	method					=	"cfMailMesagge"
            					Destinatario			=	"#RsProveedor.de_eMail#,#RsEmpleado.de_eMail#"
            					Asunto					=	"Solicitud de Orden de Compra"
            					Contenido				= 	"#contenido#"
            					EncabezadoContenido 	=	""
            					DescripcionContenido	=	"Solicitud de Orden de Compra"
            					returnvariable="CORREO"
            
        					>
    						</cfloop> 
    						
    						<cfif CORREO.SUCCESS EQ false>
    							<cfset mesagge='Ha ocurrido un error al enviar el correo'>
    						<cfelse>
    							<cfset mesagge='Se ha enviado correctamente el correo'>
    						</cfif>

					</cfif>
				
					<cfset RS.ErrorCorreo=CORREO.SUCCESS>
					<cfset RS.message=mesagge>
					<cfset RS.OK=true>
					<cfreturn RS>
				<cfcatch type='database'>
				<cfset RS.mesagge='Ha ocurrido un error al enviar el correo'>
				<cfset RS.OK=false>
				<cfset RS.TipoError = "Error en la consulta de base de datos.">
	            <cfset RS.MESSAGE = "Error al enviar el correo => #cfcatch.Detail#">  
            </cfcatch>	
		</cftry>
				
	</cffunction> 
	
	 <cffunction name="comprar_plaza" access="remote" returntype="struct" returnformat="JSON">
        <cfargument name="id_Empresa" 		type="string" required="true">
		<cfargument name="id_Obra" 			type="string" required="true">
		<cfargument name="insumos"			type="string" required="true">

		<cfset ins= ListToArray( insumos, ',' )>

		<cftransaction>
			<cftry>
				<cfloop array="#ins#" index="i">
					
					<cfquery name="actualizar_plaza" datasource="#cnx#">
						UPDATE RequisicionesDetalle 
						SET sn_ComprarPlaza=1 , fh_comprarplaza = getDate()
						WHERE id_Empresa=#id_Empresa# AND id_Obra=#id_Obra# and id_insumo='#i#'
						
					</cfquery>
					
			
				</cfloop>
				
				<cfset RS.OK=true>
				
				<cfcatch type='database'>
	                <cfset RS.OK = false>
					<cfset RS.TipoError = "Error en la consulta de base de datos.">
	                <cfset RS.MESSAGE = "Error al recuperar los datos => #cfcatch.Detail#">    
                
            	</cfcatch>	
			</cftry>
			

		</cftransaction>
		<cfreturn RS>
		
		
    </cffunction>
  <cffunction name="Verificar_disponible_traspaso" access="remote" returntype="struct" returnformat="JSON">
			<cfargument name="id_obra" 			type="string" default="" required="yes">
			<cfargument name="id_Empresa"   	type="string" default="" required="yes">
			<cfargument name="id_insumo" 		type="string" default="" required="yes">
			<cfargument name="im_subtotal"		type="string" default="" required="yes">
	


			<cftry>
				<cfset bandera=true>
						<cfquery name="tipoObra" datasource="#cnx#" username='#user_sql#' password='#password_sql#'>
								SELECT
									id_TipoObra
								FROM
									OBRAS
								WHERE id_Empresa=#id_Empresa# and id_obra=#id_Obra#		
						</cfquery>
						
						<cfif tipoObra.id_TipoObra EQ 0>
							<cfset importe=0>
						<cfelse>		
							<cfquery name="valida" datasource="#cnx#" username='#user_sql#' password='#password_sql#'>
								select (
										select ISNULL(SUM(nu_cantidad),0) * ISNULL(AVG(im_InsumoPrecio),0)
										from ExplosionInsumos 
										where id_Obra=#id_obra# and id_Empresa=#id_Empresa# 
											and id_Insumo= CASE 
											               WHEN  (
												                  SELECT COUNT(*)
												                  from ExplosionInsumos
												                  WHERE id_Obra=#id_Obra# AND id_Empresa=#id_Empresa#
												                  AND id_Insumo='#id_Insumo#'
												                  
											                              
											                 ) = 0 THEN (
												                     SELECT 
												                     distinct I.id_Agrupador
												                     from 
												                     insumos I inner join Agrupadores_fe A ON
												                     I.id_Agrupador=i.id_Agrupador
												                     where I.id_Insumo='#id_Insumo#'   
											                    ) 
											               ELSE '#id_Insumo#'
											              END  
									)
									-
									(
										select (
												
											SELECT ISNULL(SUM(ocd.nu_Cantidad),0)*ISNULL(AVG(ocd.im_precio),0)
												FROM 
													OrdenesCompraDetalle ocd
													INNER JOIN OrdenesCompra oc ON
													ocd.id_Empresa=oc.id_Empresa AND
													ocd.id_Obra=oc.id_Obra AND
													ocd.id_OrdenCompra=oc.id_OrdenCompra
												WHERE ocd.id_Obra=#id_obra# and ocd.id_Empresa=#id_Empresa# and 
												ocd.id_Insumo='#id_Insumo#' and oc.id_OrdenCompra<>202 
											)
									) as im_subtotal
							</cfquery>
							
							
							<cfset importe=valida.im_subtotal-im_subtotal>
						
						
							<cfif importe LT 0>
								<cfset bandera=false>
								
							</cfif>		
						</cfif>
						
						
						
					<cfset RS.importe=importe>
					<cfset RS.bandera=bandera>
					<cfreturn RS>
				<cfcatch type='database'>
					<cfset RS.mensaje='ha ocurrido un error en la base de datos'>
					<cfset RS.bandera=false>
					<cfreturn RS>				
				</cfcatch>
			</cftry>
		
	
	</cffunction>
</cfcomponent>	