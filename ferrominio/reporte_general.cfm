<cfparam name="Filtro_id_Empresa" default="">
<cfparam name="Filtro_id_Agrupador" default="">
<cfparam name="id_Obras" default="">
<cfparam name="id_Agrupador" default="">
<cfparam name="sn_Filtrar" default="">
<cfquery name="ObrasSeleccionadas" datasource="#Session.cnx#">
		select id_Obra,de_Obra
		from Obras
		<cfif id_Obras NEQ ''>
			where id_Obra in (#id_Obras#)
		<cfelse>
			where id_Obra=NULL	
	    </cfif>	
	    <cfif Filtro_id_Empresa neq ''>
	    	and id_Empresa=#Filtro_id_Empresa#
	    	
	    </cfif>

</cfquery>



<cfif sn_Filtrar NEQ ''>

	<cfinvoke component="#session.componentes#.reportes" method="Reporte_General" returnvariable="RsListado" 
    	id_Empresa="#Filtro_id_Empresa#"
        id_Obras="#id_Obras#"
        id_Agrupador="#Filtro_id_Agrupador#"
    >
	<!---<cfdump var="#RsListado#"><cfabort>
--->    <cfif #RsListado.SUCCESS# EQ false>
		<cf_error_manejador mensajeError="#RsListado.MESSAGE#">                                                                         
	</cfif>   
</cfif>
<cfquery name="RsEmpresas" datasource="#session.cnx#">
	SELECT id_Empresa, nb_Empresa FROM Empresas
</cfquery>
<cfquery name="RsAgrupadores" datasource="#session.cnx#">
	SELECT id_Agrupador,de_Agrupador FROM Agrupadores_fe 
</cfquery>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="../css/style.css" type="text/css">
<link rel="stylesheet" type="text/css" href="../css/box.css">
<title>Untitled Document</title>
<script language="javascript" src="../js/jquery.js"></script>
<script language="javascript" src="../js/funciones.js"></script>
<style>
	.multiselect {
			    width:20em;
			    height:15em;
			    border:solid 1px #c0c0c0;
			    overflow:auto;
				background-color:white;
			}
			 
			.multiselect label {
			    display:block;
			}
			 
			.multiselect-on {
			    /*color:#ffffff;
			    background-color:#c0c0c0;*/
			}
		</style>
</head>

<body onLoad="">
	<cfinclude template="menu.cfm">&nbsp;
	<cfform name="form1" enctype="multipart/form-data">
		<table width="1000px" align="center">
			<tr class="titulo">
				<td align="left">
					REPORTE COMPARATIVO
		        </td>
			</tr>
		</table>
    	<table width="1000" cellpadding="0" cellspacing="0" align="center">
            <tr><td class="x-box-tl" width="1px"></td><td class="x-box-tc"></td><td class="x-box-tr" width="1px"></td></tr>
            <tr>
            	<td class="x-box-ml"></td>
            	<td class="x-box-mc" width="99%">
             
                

                	<table width="100%">
                		 <tr>
                			<td>
	                			<fieldset>
	                   			<legend align="center">Empresa Y Agrupador:</legend>
	                   			<br />
								<table>
									<tr>
										<td align="right" width="12%">Empresa:&nbsp;</td>
		                           		<td align="left" width="30%">
		                            	<cfselect name="Filtro_id_Empresa" id="Filtro_id_Empresa" style="width:100%;" required="yes" message="Seleccione Empresa" onchange="bind_Obra()">
		                                	<option value="">SELECCIONE EMPRESA</option>
		                                    <cfoutput query="RsEmpresas">
												<option value="#id_Empresa#" <cfif #Filtro_id_Empresa# EQ #id_Empresa#>selected="selected"</cfif>>#nb_Empresa#</option>
											</cfoutput>
		                                </cfselect>
		                           	    </td>
			                            <td width="50px">&nbsp;</td>
			                            <td align="right">Agrupador:&nbsp;</td>
			                            <td align="left">
			                            	<cfselect name="Filtro_id_Agrupador" id="Filtro_id_Agrupador" style="width:80%;">
			                                	<option value="">SELECCIONE AGRUPADOR</option>
			                                    <cfoutput query="RsAgrupadores">
													<option value="#id_Agrupador#" <cfif #id_Agrupador# EQ #Filtro_id_Agrupador#>selected="selected"</cfif>>#de_Agrupador#</option>
												</cfoutput>
			                                </cfselect>
			                           </td>	
			                		</tr>
			                    	<tr>
		                    			<td><br /></td>
		                    		</tr>
									
                				</table>
                			</td>

                		</tr>
                		<tr>
                			<td colspan="5">
                			<fieldset>
                   			<legend align="center">Obras:</legend>
                				<br />
            					<table>
            						<tr>
										<td align="left" width="350px;">
							               <input type="text" name="" id="filtro_obra" placeholder="FILTRO OBRAS" style="width:300px;" title="Filtro" class="contenido">
											<button type="button"  style="vertical-align: bottom;" onclick="filtrar_obra()"><img src="../images/iverify.gif" title="Clic para filtrar"></button>	<br />
											<input type="checkbox" name="CheckAll" id="CheckAll" style="vertical-align: middle;" onchange="check();"/> Seleccionar Todos<br />			
											<div id="div_obras_multiselect" class="multiselect contenido" style="margin-top:5px;width:300px; max-height:50px;">
											
											</div>
							            </td>
							           
							            <td align="center">
							            		<a id="obras" onclick="pasar_obras()" href="##" width:"20px"><img src="../images/siguiente.png" alt="" /></a><br /><br />
							            <a href="#" id="removeAll" onclick="removeSelect()"><img src="../images/anterior.png"></a></td>
							            <td>
											<div id="obras_select" class="multiselect contenido" style="margin-top:5px;width:300px; max-height:100px;">
												<cfoutput query="ObrasSeleccionadas">
													<cfset checked = IIF( ArrayContains(ListToArray(id_Obra), id_Obra ), DE('checked="checked"'), DE('') )>
													<cfset checked = IIF(NOT isDefined('sn_Guardar') OR sn_Guardar NEQ 'S',  DE('checked="checked"'), checked)>
													<label>&nbsp;<input type="checkbox" id="chk_obras_#id_Obra#" #checked# name="chk_obras_#id_Obra#" value="#id_Obra#" style="vertical-align: middle;" />&nbsp;#de_Obra#</label>
												</cfoutput>
											</div>
											
							            </td>
			                		</tr>
			                		<tr><td><br /></td></tr>	
						 	
			                        <tr>
			                        	<td colspan="4" align="center">
			                            	<button type="button" class="boton_imagen" onclick="Filtrar()"><img src="../images/filtro_azul.png"></button>
			                                <cfoutput>
			                                	<button type="button" class="boton_imagen" style="width:40px" onClick="imprimr_pdf(Filtro_id_Empresa.value,Filtro_id_Agrupador.value)"><img src="../images/imprimir.png"></button>
			                                    <button type="button" class="boton_imagen" style="width:40px" onClick="imprimr_exel(Filtro_id_Empresa.value,Filtro_id_Agrupador.value)"><img src="../images/excel.png"></button>
			                                </cfoutput>
			                            </td>
			                        </tr>
									</fieldset>			
            					</table>
            					<br />
            				
                			</td>
                        
                        </tr>
                      	
                    </table>
                 
                </td>
                <td class="x-box-mr"></td>
            </tr>
            <tr><td class="x-box-bl" width="1px"></td><td class="x-box-bc"></td><td class="x-box-br" width="1px"></td>
        </table> <br>
        <div id="cargando" style="display: none;">
			<center>
				<img src="../images/cargando_procesando.gif" >
			</center>
        </div>
		<cfif #sn_Filtrar# EQ 'S'>
            <table cellpadding="0" cellspacing="0" width="95%" id="tbl_Principal">
                <tr class="encabezado_grande">
                    <td class="cuadricula"></td>
                    <td class="cuadricula" colspan="3"></td>
                    <td class="cuadricula" colspan="3">PRESUPUESTO</td>
                    <td class="cuadricula" colspan="2">SOLICITADO</td>
                    <td class="cuadricula" colspan="3">COMPRADO</td>
                    <td class="cuadricula" colspan="2">EJECUTADO</td>
                    <td class="cuadricula" colspan="2">DISPONIBLE</td>
					<td class="cuadricula" colspan="2">M.O. SUBCONTRATO</td>
					<td class="cuadricula" colspan="2">M.O. NOMINA</td>
                    <td class="cuadricula"></td>
                </tr>
                <tr class="encabezado_grande">
                    <td class="cuadricula"></td>
                    <td class="cuadricula" align="center">CLAVE</td>
                    <td class="cuadricula" align="center">DESCRIPCION</td>
                    <td class="cuadricula" align="center">UM</td>
                    <td class="cuadricula" align="center">CANTIDAD</td>
                    <td class="cuadricula" align="center">PRECIO</td>
                    <td class="cuadricula" align="center">IMPORTE</td>
                    <td class="cuadricula" align="center">CANTIDAD</td>
                    <td class="cuadricula" align="center">IMPORTE</td>
                    <td class="cuadricula" align="center">CANTIDAD</td>
                    <td class="cuadricula" align="center">PRECIO</td>
                    <td class="cuadricula" align="center">IMPORTE</td>
                    <td class="cuadricula" align="center">ENTRADA</td>
                    <td class="cuadricula" align="center">SALIDA</td>
                    <td class="cuadricula" align="center">CANTIDAD</td>
                    <td class="cuadricula" align="center">IMPORTE</td>
					<td class="cuadricula" align="center">CANTIDAD</td>
                    <td class="cuadricula" align="center">IMPORTE</td>
					<td class="cuadricula" align="center">CANTIDAD</td>
                    <td class="cuadricula" align="center">IMPORTE</td>
                    <td class="cuadricula"></td>
                </tr> 
            
                <cfoutput query="RsListado.RS" group="id_Agrupador">
                	<cfif id_Agrupador NEQ ''>
						<cfquery dbtype="query"  name="RSTotalesAgrupador">
							SELECT 
								SUM(nu_CantidadExplosion) AS nu_CantidadAgrupador,
								SUM(im_PrecioExplosion) AS im_PrecioExplosion,
								SUM(im_SubtotalExplosion) AS im_SubtotalAgrupador,
								SUM(nu_AplicadoEjecutado_ot) AS nu_AplicadoEjecutado_ot_Agrupador,
								SUM(im_AplicadoEjecutado_ot) AS im_AplicadoEjecutado_ot_Agrupador,
								SUM(nu_AplicadoEjecutado_dst) AS nu_AplicadoEjecutado_dst_Agrupador,
								SUM(im_AplicadoEjecutado_dst) AS im_AplicadoEjecutado_dst_Agrupador,
								SUM(nu_CantidadCompra) AS nu_CantidadCompra_agrupador,
								SUM(im_CantidadPrecioCompra) AS im_CantidadPrecioCompra_agrupador,
								SUM(im_Disponible) AS im_DisponibleAgrupador,
								sum(nu_Disponible) AS nu_DisponibleAgrupador,
								SUM(nu_Cantidad) AS nu_SolicitadoAgrupador,

								SUM(nu_Entrada) AS nu_EntradaAgrupador,
								SUM(nu_Salida) AS nu_SalidaAgrupador,

								AVG(im_PrecioExplosion) AS im_PrecioExplosion_Agrupador,
								AVG(im_PrecioCompra) AS im_PrecioCompra_Agrupador
							FROM
								RsListado.RS
							WHERE 
								id_Agrupador = '#id_Agrupador#'
						</cfquery>
						<tr onClick="mostrar_ocultar_tr(#currentrow#)" style="cursor:pointer;">
	                        <td class="tabla_grid_orilla_izquierda"></td>
	                        <td class="cuadricula" style="background:##999999;" align="center">#id_Agrupador#</td>
	                        <td class="cuadricula" style="background:##999999;" align="center">#de_Agrupador#</td>
	                        <td class="cuadricula" style="background:##999999;" align="center">#de_UnidadMedidaAgrupador#</td>
							<!---Presupuestado --->
	                        <td class="cuadricula" style="background:##999999;" align="right">#NumberFormat(RSTotalesAgrupador.nu_CantidadAgrupador,',____.__')#</td>
	                        <td class="cuadricula" style="background:##999999;" align="right">$#NumberFormat(RSTotalesAgrupador.im_PrecioExplosion_Agrupador,',____.__')#</td>
	                        <td class="cuadricula" style="background:##999999;" align="right">$#NumberFormat(RSTotalesAgrupador.im_SubtotalAgrupador,',____.__')#</td>
	                        <!---SOLICITADO--->
							<td class="cuadricula" style="background:##999999;" align="right">#NumberFormat(RSTotalesAgrupador.nu_SolicitadoAgrupador,',____.__')#</td>
	                        <td class="cuadricula" style="background:##999999;" align="right"></td>
	                        <!---Comprado --->
							<td class="cuadricula" style="background:##999999;" align="right">#NumberFormat(RSTotalesAgrupador.nu_CantidadCompra_agrupador,',____.__')#</td>
	                        <td class="cuadricula" style="background:##999999;" align="right">$#NumberFormat(RSTotalesAgrupador.im_PrecioCompra_Agrupador,',____.__')#</td>
	                        <td class="cuadricula" style="background:##999999;" align="right">$#NumberFormat(RSTotalesAgrupador.im_CantidadPrecioCompra_agrupador,',____.__')#</td>
	                        <!---Ejecutado --->
							<td class="cuadricula" style="background:##999999;" align="right">#NumberFormat(RSTotalesAgrupador.nu_EntradaAgrupador,',____.__')#</td>
	                        <td class="cuadricula" style="background:##999999;" align="right">#NumberFormat(RSTotalesAgrupador.nu_SalidaAgrupador,',____.__')#</td>
	                        <!---Disponible --->
							<td class="cuadricula" style="background:##999999;" align="right">#NumberFormat(RSTotalesAgrupador.nu_DisponibleAgrupador,',____.__')#</td>
	                        <td class="cuadricula" style="background:##999999;" align="right">$#NumberFormat(RSTotalesAgrupador.im_DisponibleAgrupador,',____.__')#</td>
							<!---MO SUBCONTRATO--->
							<td class="cuadricula" style="background:##999999;" align="right">#NumberFormat(RSTotalesAgrupador.nu_AplicadoEjecutado_ot_Agrupador,',____.__')#</td>
						    <td class="cuadricula" style="background:##999999;" align="right">$#NumberFormat(RSTotalesAgrupador.im_AplicadoEjecutado_ot_Agrupador,',____.__')#</td>
							<!---MO NOMINA--->
							<td class="cuadricula" style="background:##999999;" align="right">#NumberFormat(RSTotalesAgrupador.nu_AplicadoEjecutado_dst_Agrupador,',____.__')#</td>
						    <td class="cuadricula" style="background:##999999;" align="right">$#NumberFormat(RSTotalesAgrupador.im_AplicadoEjecutado_dst_Agrupador,',____.__')#</td>
	                        <td class="tabla_grid_orilla_derecha"></td>
	                    </tr>
	                    <cfset tr = #currentrow#>
	                    <cfoutput group="id_Insumo">
	                    	<cfif #id_Insumo# NEQ ''>
		                        <tr onMouseOver="this.style.backgroundColor='##99CCCC'" onMouseOut="this.style.backgroundColor=''" class="tr_#tr#" id="id_#id_Empresa#_#id_Obra#_#TRIM(id_Insumo)#_#tr#" click="0" onClick="OrdenesCompra(#id_Empresa#,#id_Obra#,'#TRIM(id_Insumo)#',#tr#);"   style="display:none;cursor:pointer;">
		                            <td class="tabla_grid_orilla_izquierda" ></td>
		                            <td class="cuadricula" align="center">#id_Insumo#</td>
		                            <td class="cuadricula" align="center">#de_Insumo#</td>
		                            <td class="cuadricula" align="center">#de_UnidadMedida#</td>
		                            <td class="cuadricula" align="right">#NumberFormat(nu_CantidadExplosion,',____.__')#</td>
		                            <td class="cuadricula" align="right">$#NumberFormat(im_PrecioExplosion,',____.__')#</td>
		                            <td class="cuadricula" align="right">$#NumberFormat(im_SubtotalExplosion,',____.__')#</td>
		                            <!--- solicitado --->
		                            <td class="cuadricula" align="right">#NumberFormat(nu_Cantidad,',____.__')#</td>
		                            <td class="cuadricula" align="right">$#NumberFormat(im_Precio,',____.__')#</td>
		                            <!---Coomprado --->
									<td class="cuadricula" align="right">#NumberFormat(nu_CantidadCompra,',____.__')#</td>
		                            <td class="cuadricula" align="right">$#NumberFormat(im_PrecioCompra,',____.__')#</td>
		                            <td class="cuadricula" align="right">$#NumberFormat(im_CantidadPrecioCompra,',____.__')#</td>
		                            <!---Ejecutado --->
									<td class="cuadricula" align="right">#NumberFormat(nu_Entrada,',____.__')#</td>
		                            <td class="cuadricula" align="right">#NumberFormat(nu_Salida,',____.__')#</td>
		                            <!---Disponible --->
									<td class="cuadricula" align="right">#NumberFormat(nu_Disponible,',____.__')#</td>
		                            <td class="cuadricula" align="right">$#NumberFormat(im_Disponible,',____.__')#</td>
									<!---M.O. SUBCONTRATO	 --->
									<td class="cuadricula" align="right">#NumberFormat(nu_AplicadoEjecutado_ot,',____.__')#</td>
								    <td class="cuadricula" align="right">$#NumberFormat(im_AplicadoEjecutado_ot,',____.__')#</td>
									<!---M.O. NOMINA --->
									<td class="cuadricula" align="right">#NumberFormat(nu_AplicadoEjecutado_dst,',____.__')#</td>
								    <td class="cuadricula" align="right">$#NumberFormat(im_AplicadoEjecutado_dst,',____.__')#</td>
		                            <td class="tabla_grid_orilla_derecha"></td>
		                        </tr>
						   </cfif> 
	                    </cfoutput>
                    </cfif>
                </cfoutput>
                <cfquery dbtype="query"  name="RSTotales">
					SELECT 
						SUM(nu_CantidadExplosion) AS nu_CantidadExplosion,
						SUM(im_PrecioExplosion) AS im_PrecioExplosion,
						SUM(im_SubtotalExplosion) AS im_SubtotalExplosion,
						SUM(nu_Cantidad) AS nu_Cantidad,
						SUM(im_Precio) AS im_Precio,
						SUM(nu_CantidadCompra) AS nu_CantidadCompra,
						SUM(im_PrecioCompra) AS im_PrecioCompra,
						SUM(im_CantidadPrecioCompra) AS im_CantidadPrecioCompra,
						SUM(nu_Entrada) AS nu_Entrada,
						SUM(nu_Salida) AS nu_Salida,
						SUM(nu_Disponible) AS nu_Disponible,
						SUM(im_Disponible) AS im_Disponible,
						SUM(nu_AplicadoEjecutado_ot) AS nu_AplicadoEjecutado_ot,
						SUM(im_AplicadoEjecutado_ot) AS im_AplicadoEjecutado_ot,
						SUM(nu_AplicadoEjecutado_dst) AS nu_AplicadoEjecutado_dst,
						SUM(im_AplicadoEjecutado_dst) AS im_AplicadoEjecutado_dst
					FROM
						RsListado.RS
				</cfquery>


				<cfoutput query="RSTotales">
					<tr class="encabezado_grande">
                        <td class="tabla_grid_orilla_izquierda" ></td>
                        <td class="cuadricula" align="right" colspan="3">Totales</td>
                        
                        <td class="cuadricula" align="right"><!---#NumberFormat(nu_CantidadExplosion,',____.__')#---></td>
                        <td class="cuadricula" align="right"><!---$#NumberFormat(im_PrecioExplosion,',____.__')#---></td>
                        <td class="cuadricula" align="right">$#NumberFormat(im_SubtotalExplosion,',____.__')#</td>
                        <td class="cuadricula" align="right"><!---#NumberFormat(nu_Cantidad,',____.__')#---></td>
                        <td class="cuadricula" align="right">$#NumberFormat(im_Precio,',____.__')#</td>
                        <!---Coomprado --->
						<td class="cuadricula" align="right"><!---#NumberFormat(nu_CantidadCompra,',____.__')#---></td>
                        <td class="cuadricula" align="right"><!---$#NumberFormat(im_PrecioCompra,',____.__')#---></td>
                        <td class="cuadricula" align="right">$#NumberFormat(im_CantidadPrecioCompra,',____.__')#</td>
                        <!---Ejecutado --->
						<td class="cuadricula" align="right"><!---#NumberFormat(nu_Entrada,',____.__')#---></td>
                        <td class="cuadricula" align="right"><!---#NumberFormat(nu_Salida,',____.__')#---></td>
                        <!---Disponible --->
						<td class="cuadricula" align="right"><!---#NumberFormat(nu_Disponible,',____.__')#---></td>
                        <td class="cuadricula" align="right">$#NumberFormat(im_Disponible,',____.__')#</td>
						<!---M.O. SUBCONTRATO	 --->
						<td class="cuadricula" align="right"><!---$#NumberFormat(nu_AplicadoEjecutado_ot,',____.__')#---></td>
						<td class="cuadricula" align="right">$#NumberFormat(im_AplicadoEjecutado_ot,',____.__')#</td>
						<!---M.O. NOMINA --->
						<td class="cuadricula" align="right"><!---$#NumberFormat(nu_AplicadoEjecutado_dst,',____.__')#---></td>
						<td class="cuadricula" align="right">$#NumberFormat(im_AplicadoEjecutado_dst,',____.__')#</td>
                        <td class="tabla_grid_orilla_derecha"></td>
                    </tr>
				</cfoutput>
			
            </table><br />
        </cfif> 
        <cfinput name="sn_Filtrar" type="hidden" value="S">  	
        <cfinput  type="hidden" name="id_Obras" id="id_Obras" value="#id_Obras#">
    </cfform>
</body>
<script language="javascript" src="js/modal/modal.js"></script>
<link rel="stylesheet" href="js/modal/modal.css" type="text/css">
<script language="javascript">
	function mostrar_ocultar_tr(id){
		if ($('.tr_'+id)[0] && $('.tr_'+id)[0].style.display == 'none'){
			$('tr[class=tr_'+id+']').show();
		}else if ($('.tr_'+id)[0]){
			$('tr[class^=tr_'+id+']').hide();
			$('tr[id^=tr_'+id+']').remove();
			$('tr[class^=tr_'+id+']').attr('click',0);
		}
	}
	function de_obra(id_Obra){
		if(id_Obra == ''){
			$('#de_Obra').val('');
			return false;
		}
		$.ajax({
			url:'componentes/obras.cfc?method=RsObras_ajax&id_Empresa='+1+'&id_Obra='+id_Obra+'',
			type:'get',
			dataType:'json',
			success:function(response){
				if(response.RS.DATA != ''){
					$('#de_Obra').val(response.RS.DATA[0][1])
				}else{
					alert('La calve de obra que ingreso es invalida')
					$('#de_Obra').val('');
				}	
			}
			
		});
	}
	
	function OrdenesCompra(id_Empresa,id_Obra,id_Insumo,currentrow){
	 var click = $('#id_'+id_Empresa+'_'+id_Obra+'_'+id_Insumo+'_'+currentrow).attr('click');
	
		
	if(click ==0){
		
		$('#id_'+id_Empresa+'_'+id_Obra+'_'+id_Insumo+'_'+currentrow).attr('click',1);
        
		
		$.ajax({
			url:'componentes/reportes.cfc',
			method:'POST',
				cache:true,
				async:false,
			dataType:'JSON',
			data:{
				method:'OC_por_Insumos',
				id_Empresa:id_Empresa,
				id_Obra:id_Obra,
				id_Insumo:id_Insumo
			},
			success:function(data){
				
				if(data != ''){
				
				var contenido="<tr class='tr_"+id_Empresa+"_"+id_Obra+"_"+id_Insumo+"_"+currentrow+" encabezado_grande' id='tr_"+currentrow+"'>"+ 
									"<td class='cuadricula' colspan='3' align='center'>O.C.</td>"+
									"<td class='cuadricula' colspan='5' align='center'>PROVEEDOR</td>"+
									"<td class='cuadricula' colspan='2' align='center'>FECHA</td>"+
									"<td class='cuadricula' colspan='2' align='center'>NOTAS</td>"+
									"<td class='cuadricula' colspan='2' align='center'>UM</td>"+
									"<td class='cuadricula' colspan='2' align='center'>CANTIDAD</td>"+
									"<td class='cuadricula' colspan='2' align='center'>PRECIO</td>"+
									"<td class='cuadricula' colspan='2' align='center'>IMPORTE</td>"+
								"</tr>";
								
		for(var i=0;i<data.length;i++){
					
					contenido+= "<tr class='tr_"+id_Empresa+"_"+id_Obra+"_"+id_Insumo+"_"+currentrow+"' style='background-color:#B4CBE6;' id='tr_"+currentrow+"'>"+
									"<td class='cuadricula' colspan='3' align='center'>"+data[i].ID_ORDENCOMPRA+"</td>"+
									"<td class='cuadricula' colspan='5' align='center'>"+data[i].NB_PROVEEDOR+"</td>"+
									"<td class='cuadricula' colspan='2' align='center'>"+data[i].FH_ORDENCOMPRA+"</td>"+
									"<td class='cuadricula' colspan='2' align='center'>"+data[i].DE_MEDIDAESPECIAL+"</td>"+
									"<td class='cuadricula' colspan='2' align='center'>"+data[i].DE_UNIDADMEDIDA+"</td>"+
									"<td class='cuadricula' colspan='2' align='right'>"+formatea(data[i].NU_CANTIDAD,2)+"</td>"+
									"<td class='cuadricula' colspan='2' align='right'>$"+formatea(data[i].IM_PRECIO,2)+"</td>"+
									"<td class='cuadricula' colspan='2' align='right'>$"+formatea(data[i].IM_CANTIDADPRECIO,2)+"</td>"+
								"</tr>";		
						}
					$('tr[id^=id_'+id_Empresa+'_'+id_Obra+'_'+id_Insumo+'_'+currentrow+']').after(contenido);
						
				}else{
					var contenido="<tr class='tr_"+id_Empresa+"_"+id_Obra+"_"+id_Insumo+"_"+currentrow+" encabezado_grande' id='tr_"+currentrow+"'>"+ 
									"<td class='cuadricula' colspan='3' align='center'>O.C.</td>"+
									"<td class='cuadricula' colspan='5' align='center'>PROVEEDOR</td>"+
									"<td class='cuadricula' colspan='2' align='center'>FECHA</td>"+
									"<td class='cuadricula' colspan='2' align='center'>NOTAS</td>"+
									"<td class='cuadricula' colspan='2' align='center'>UM</td>"+
									"<td class='cuadricula' colspan='2' align='center'>CANTIDAD</td>"+
									"<td class='cuadricula' colspan='2' align='center'>PRECIO</td>"+
									"<td class='cuadricula' colspan='2' align='center'>IMPORTE</td>"+
								"</tr>";
					
					 contenido +="<tr class='tr_"+id_Empresa+"_"+id_Obra+"_"+id_Insumo+"_"+currentrow+"' style='background-color:#B4CBE6;' id='tr_"+currentrow+"'>"+
										"<td class='cuadricula' colspan='20' align='center'>No Existen Ordenes de Compra para este Insumo</td>"+
									"</tr>";
					
					$('tr[id^=id_'+id_Empresa+'_'+id_Obra+'_'+id_Insumo+'_'+currentrow+']').after(contenido);
				}
			}
			
		});
	  }
	  else{
	  	$('.tr_'+id_Empresa+'_'+id_Obra+'_'+id_Insumo+'_'+currentrow).remove();
		$('#id_'+id_Empresa+'_'+id_Obra+'_'+id_Insumo+'_'+currentrow).attr('click',0);
	  }	  
	}
	
	function modal_obra(){
		modal('Listado de Obras','obras_listado_modal.cfm','style="background-image:url(../images/bg_gris.gif);color:#007670"');
	}
	function imprimr_pdf(id_Empresa,id_Agrupador){
		if(id_Empresa == ''){
			alert('Seleccione Empresa');
			$('#Filtro_id_Empresa').focus();
			return false;
		}
		var id_Obras = '';
		if ($('#obras_select > label > [type="checkbox"][id^="chk_obras_"]:checked' ).length > 0)
		{
			$('#obras_select > label > [type="checkbox"][id^="chk_obras_"]:checked' ).each(function()
			{
				id_Obras += $(this).val() + ',' ;
			});
			
		}
		id_Obras = id_Obras.substring(0, id_Obras.length - 1); //Quito ultima coma
		window.open('reporte_general_pdf.cfm?id_Empresa='+id_Empresa+'&id_Obras='+id_Obras+'&id_Agrupador='+id_Agrupador+'')
	}
	function imprimr_exel(id_Empresa,id_Agrupador){
		var id_Obras = '';
		if ($('#obras_select > label > [type="checkbox"][id^="chk_obras_"]:checked' ).length > 0)
		{
			$('#obras_select > label > [type="checkbox"][id^="chk_obras_"]:checked' ).each(function()
			{
				id_Obras += $(this).val() + ',' ;
			});
			
		}
		id_Obras = id_Obras.substring(0, id_Obras.length - 1); //Quito ultima coma
		if(id_Empresa == ''){
			alert('Seleccione Empresa');
			$('#Filtro_id_Empresa').focus();
			return false;
		}
		window.open('reporte_general_excel.cfm?id_Empresa='+id_Empresa+'&id_Obras='+id_Obras+'&id_Agrupador='+id_Agrupador+'','_Self')
	}
	function Filtrar()
	{
		$('#cargando').fadeIn(); 
		$('#tbl_Principal').hide();
		var id_Obras = '';
		if ($('#obras_select > label > [type="checkbox"][id^="chk_obras_"]:checked' ).length > 0)
		{
			$('#obras_select > label > [type="checkbox"][id^="chk_obras_"]:checked' ).each(function()
			{
				id_Obras += $(this).val() + ',' ;
			});
			
		}
		id_Obras = id_Obras.substring(0, id_Obras.length - 1); //Quito ultima coma
		$('#id_Obras').val(id_Obras);	
		
		var id_Empresa = $('#Filtro_id_Empresa').val();
		
		
		if(id_Empresa == '' )
		{
			jAlert('Seleccione una empresa');
		}
		else
		{
			form1.submit();
		}
	}
	jQuery.fn.multiselect = function() {
		    $(this).each(function() {
		        var checkboxes = $(this).find("input:checkbox");
		        checkboxes.each(function() {
		            var checkbox = $(this);
		            // Highlight pre-selected checkboxes
		            if (checkbox.prop("checked"))
		                checkbox.parent().addClass("multiselect-on");
		 
		            // Highlight checkboxes that the user selects
		            checkbox.click(function() {
		                if (checkbox.prop("checked"))
		                    checkbox.parent().addClass("multiselect-on");
		                else
		                    checkbox.parent().removeClass("multiselect-on");
		            });
		        });
		    });
	};
			
	$(function(){
		 $(".multiselect").multiselect();
	});	

	function check()
	{
		$('[type="checkbox"][id^="chk_obras_"]' ).attr('checked', $('#CheckAll').prop('checked'));
	}
	function filtrar_obra(filtro){
		 var id_Empresa=$('#Filtro_id_Empresa').val();
		 if(id_Empresa == ''){
		 	alert('Seleccione Empresa');
		 	return false;
		 }
		 var de_Obra=$('#filtro_obra').val();
		 
		 $.ajax({
		 			url:'../componentes/obras.cfc',
		 			method:'POST',
		 			cache:false,
		 			data:{
		 				method:'get_obras_ajax',
		 				de_Obra:de_Obra,
		 				id_Empresa:$('#Filtro_id_Empresa').val()
		 			},
		 			success:function(data){
		 				var arreglo=JSON.parse(data);				
		 				var html=''
		 				for(var i=0;i<arreglo.length;i++){
		 					 html+='<label>&nbsp;<input type="checkbox" id="chk_obras_'+arreglo[i].ID_OBRA+'" true name="chk_obras_'+arreglo[i].ID_OBRA+'" value="'+arreglo[i].ID_OBRA+'" style="vertical-align: middle;" />&nbsp;'+arreglo[i].DE_OBRA+'</label>';
		 				}

		 				$('#div_obras_multiselect').html(html);
		 			},
		 			error:function(error){

		 			}
		 })
	}
	function bind_Obra(){
		 var id_Empresa=$('#Filtro_id_Empresa').val();
		 if(id_Empresa == ''){
		 	alert('Seleccione Empresa');
		 	return false;
		 }

		 $.ajax({
		 			url:'../componentes/obras.cfc',
		 			method:'POST',
		 			cache:false,
		 			data:{
		 				method:'get_obras_ajax',
		 				de_Obra:'',
		 				id_Empresa:$('#Filtro_id_Empresa').val()
		 			},
		 			success:function(data){
		 				var arreglo=JSON.parse(data);				
		 				var html=''
		 				for(var i=0;i<arreglo.length;i++){
		 					 html+='<label>&nbsp;<input type="checkbox" id="chk_obras_'+arreglo[i].ID_OBRA+'" true name="chk_obras_'+arreglo[i].ID_OBRA+'" value="'+arreglo[i].ID_OBRA+'" style="vertical-align: middle;" />&nbsp;'+arreglo[i].DE_OBRA+'</label>';
		 				}

		 				$('#div_obras_multiselect').html(html);
		 			},
		 			error:function(error){

		 			}
		 })
	}
	function pasar_obras(){
		$('#div_obras_multiselect > label > [id^="chk_"]').each(function(){
			if($(this).prop('checked')){
				$('#obras_select').append($(this).parent().clone());
			}
		})
	}
	
	function removeSelect(){
		$('#obras_select').html('');
	}
	
</script>
</html>
