// JavaScript Document
//Crea listado sin usuar submit, con opcion de agrupados y paginado
function listado_ajax(object){
	var id = object.id;
	var url = object.url;
	var startrow = object.startrow;
	var long = $('#'+id).children().length;
	var dataSet = new Array();
	var atributos = new Array();
	var attr = '';
	var NextPage = '';
	var EndPage = '';
	var BackPage = '';
	var FirstPage = '';
	var group_temp = new Array();
	var varCols = new Array();
	var varRows = new Array();
	try{
		var maxrows = $('#'+id)[0].attributes.maxrows.value;
	}catch(err){
		var maxrows = null;	
	}
/*=================================Envia peticion al componente y convierte el rs en un arreglo=================================================================================*/
	$.ajax({
		type:'get',
		url:url,
		dataType:'json',
		async:false,
		success:function(response){
			if(response.SUCCESS == true){
				for(var x=0;x<response.RS.DATA.length;x++)
				{					
					var obj = {}
					for(i=0;i<response.RS.DATA[x].length;i++){
						var valor = response.RS.DATA[x][i];
						var propiedad = response.RS.COLUMNS[i];						
						obj[propiedad] = valor;		
					}
					dataSet.push(obj);
				}
			}else{
				console.log(response)	
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {
        	console.log(xhr.success());
			console.log(ajaxOptions);
       	 	console.log(thrownError);
      }
	});
	$('[id*=tb_listado_'+id+']').remove();
	if(dataSet.length == 0)return false;
	if(maxrows == null || maxrows > dataSet.length || maxrows == ''){
		 maxrows = dataSet.length;	
	}
	if(startrow == null)startrow=0;
	maxrows = (parseInt(maxrows) + startrow);
	for(var x=0;x<long;x++){
		var atributos_td = $('#'+id).children()[x].attributes;
		for(var i=0;i<atributos_td.length;i++){
			if(attr != ''){
				attr = attr+' '+atributos_td[i].name+'="'+atributos_td[i].value+'"';
			}else{
				attr = ''+atributos_td[i].name+'="'+atributos_td[i].value+'"';
			} 
			
			atributos[x] = attr;
			if(i == atributos_td.length -1)attr='';
		}
		
	}	
	for( x=startrow;x<maxrows;x++){
		if(x == dataSet.length){
			maxrows = x
			break;
		}
		varCols = new Array();
		record = dataSet[x];
		try{
			var groups = $('#'+id)[0].attributes.groups;
		}catch(err){
			var groups = null;
		}
		try{
			var groupsattr = $('#'+id)[0].attributes.groupsattr;	
		}catch(err){
			var groupsattr = null;
		}
		
		if(groups != null){	
			groups = groups.value.split('||');
			groupsattr = groupsattr.value.split('||');
			for(var y=0;y<groups.length;y++){
				group = groups[y].split(/[{}]+/);
				for(var i=0;i<group.length;i++){
					group_value = group[i];
					if(!group_value.indexOf(id+'.')){
						group_value = group_value.replace(id+'.','');
						var val = record[group_value.toUpperCase()];
						if(val == null)val='';
						groups[y] = groups[y].replace(group_value,val);
						groups[y] = groups[y].replace('{','');
						groups[y] = groups[y].replace('}','');
						groups[y] = groups[y].replace(id+'.','');
					}
				}
				if(group_temp[y] != groups[y]){
					group_temp[y] = groups[y];
					$('#'+id).before('<tr id="tb_listado_'+id+'"><td colspan='+long+' '+groupsattr[y]+'>'+groups[y]+'</td></tr>');
				}
			}
		}
		for(var j=0;j<long;j++){
			var html = $('#'+id).children()[j].innerHTML;
			var decimal = null;
			if($('#'+id).children()[j].attributes.decimal != null ){
				var decimal = $('#'+id).children()[j].attributes.decimal.value
			}
			var value = html.split(/[{}]+/);
			for(var i=0;i<value.length;i++){
				str = value[i];
				if(!str.indexOf(id+'.')){
					html = html.replace(str, str.toUpperCase());
					var val = record[str.replace(id+'.','').toUpperCase()]
					
					if(val == null)val='';
					if(decimal != null)val=val.toFixed(decimal); 
					html = html.replace(str.toUpperCase(), val)
					html = html.replace('{','');
					html = html.replace('}','');
				}
			}
			varCols.push('<td '+atributos[j]+'>'+html+'</td>');
		}
		$('#'+id).before('<tr id="tb_listado_'+id+'">'+varCols+'</tr>');
	}
	
	if(maxrows < dataSet.length){
		var NextPage = '<button type="button" id="NextPage_'+id+'" style="border:none;background:none;">'+
							'<img style="width:100%;height:100%"  src="../images/siguiente.png" />'+
					   '</button>';
		var EndPage = '<button type="button" id="EndPage_'+id+'" style="border:none;background:none;">'+
							'<img style="width:100%;height:100%"  src="../images/ultimo.png" />'+
					  '</button>';			   
	}
	if(startrow > 0){
		var BackPage = 	'<button type="button" id="BackPage_'+id+'" style="border:none;background:none;">'+
							'<img style="width:100%;height:100%"  src="../images/anterior.png" />'+
						'</button>';
		var FirstPage = '<button type="button" id="FirstPage_'+id+'" style="border:none;background:none;">'+
							'<img style="width:100%;height:100%"  src="../images/primero.png" />'+
						'</button>';				
	}
	$('#'+id).after('<tr id="tb_listado_'+id+'">'+
						'<td colspan="'+j+'" align="center" style="font-weight:bold;">'+
							'<table width="100%" height="100%" style="border:1px solid;">'+
								'<tr >'+
									'<td width="20%" style="border:none;" align="right">'+FirstPage+'</td>'+
									'<td width="10%" style="border:none;" align="right">'+BackPage+'</td>'+
									'<td width="30%" align="center" style="border:none;">'+
										'Registros: '+(startrow + 1)+' al '+maxrows+' de '+dataSet.length+' &nbsp;'+
									'</td>'+	
									'<td width="10%" style="border:none;">'+NextPage+'</td>'+
									'<td width="20%" style="border:none;">'+EndPage+'</td>'+
								'</tr>'+
							'</table>'+	
						'</td>'+
					'</tr>');
	$('#NextPage_'+id+'').click(function(){
		listado_ajax({
					 	id:id,
						url:url,
						startrow:x
					 });
	});
	$('#EndPage_'+id+'').click(function(){
		if(dataSet.length > $('#'+id)[0].attributes.maxrows.value){
			startrow = dataSet.length - $('#'+id)[0].attributes.maxrows.value
		}else if(dataSet.length < $('#'+id)[0].attributes.maxrows.value){
			startrow = $('#'+id)[0].attributes.maxrows.value - dataSet.length
		}
		listado_ajax({
					 	id:id,
						url:url,
						startrow:dataSet.length - $('#'+id)[0].attributes.maxrows.value
					 });
	});
	$('#BackPage_'+id+'').click(function(){
		startrow = 	startrow - parseInt($('#'+id)[0].attributes.maxrows.value);
		if(startrow <= 0){
			startrow = 0	
		}
		listado_ajax({
					 	id:id,
						url:url,
						startrow:startrow 
					 });
	});
	$('#FirstPage_'+id+'').click(function(){
		listado_ajax({
					 	id:id,
						url:url,
						startrow:0
					 });
	});
}
function validar_numero( event, t )
{
	if ((event.which != 46 || $(t).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) 
	{
		event.preventDefault();
	}
}