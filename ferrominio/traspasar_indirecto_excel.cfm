<cfparam name="sn_Mostrar" default="">
<cfparam name="id_EmpresaOrigen" default="">
<cfparam name="id_EmpresaDestino" default="">
<cfparam name="id_ObraOrigen" default="">
<cfparam name="id_ObraDestino" default="">

<cfinvoke component="#Session.componentes#.traspasar_indirecto" method="Indirectos_Montos_Listado" returnvariable="RsIndirectos"
    id_EmpresaOrigen="#id_EmpresaOrigen#"
    id_ObraOrigen="#id_ObraOrigen#"
    id_EmpresaDestino="#id_EmpresaDestino#"
    id_ObraDestino="#id_ObraDestino#"
    >
    <cfif #RsIndirectos.tipoError# NEQ "">
        <cf_error_manejador tipoError="#RsIndirectos.tipoError#" mensajeError="#RsIndirectos.mensaje#">
    </cfif>
    
<cfsavecontent variable="contenido"> 
<table cellpadding="0" cellspacing="0" width="100%" id="tbl_Principal" class="fondo_listado" border="1">
    <tr class="encabezado">
        <td class="cuadricula" align="center">EMPRESA ORIGEN</td>
        <td class="cuadricula" align="center">OBRA ORIGEN</td>
        <td class="cuadricula" align="center">EMPRESA DESTINO</td>
        <td class="cuadricula" align="center">OBRA DESTINO</td>
        <td class="cuadricula" align="center">MONTO TRASPASADO</td>
        <td class="cuadricula" align="center">USUARIO REGISTRO</td>
        <td class="cuadricula" align="center">FECHA REGISTRO</td>
    </tr>
    <cfoutput query="RsIndirectos.rs">
        <tr>
            <td class="cuadricula" align="left">#nb_EmpresaOrigen#</td>
            <td class="cuadricula" align="left">#id_ObraOrigen# - #de_ObraOrigen#</td>
            <td class="cuadricula" align="left">#nb_EmpresaDestino#</td>
            <td class="cuadricula" align="left">#id_ObraDestino# - #de_ObraDestino#</td>
            <td class="cuadricula" align="right">$#NumberFormat(im_Traspasado,',_.__')#</td>
            <td class="cuadricula" align="left">#nb_Usuario#</td>
            <td class="cuadricula" align="center">#DateFormat(fh_Registro,'DD/MM/YYYY')#</td>
        </tr>
    </cfoutput>
</table>
</cfsavecontent>
<cfoutput>
	#contenido#
</cfoutput>

<cfheader name="Content-Disposition" value="inline; filename=Reporte_traspasos_#DateFormat(Now(),'DD_MM_YYYY')#.xls" >
<cfcontent variable="#ToBinary(ToBase64(contenido))#" type="application/msexcel" > 
