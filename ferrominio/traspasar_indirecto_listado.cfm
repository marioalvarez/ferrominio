<cfset CurrentPage=GetFileFromPath(GetTemplatePath())>
<cfparam name="sn_Mostrar" default="">
<cfparam name="id_EmpresaOrigen" default="">
<cfparam name="id_EmpresaDestino" default="">
<cfparam name="id_ObraOrigen" default="">
<cfparam name="id_ObraDestino" default="">
<!---Invokes --->
<cfinvoke component="#Session.componentes#.traspasar_indirecto" method="get_listado_Obras_Indirectos" returnvariable="RsObras">
<cfinvoke component="#Session.componentes#.traspasar_indirecto" method="get_listado_Empresas" id_Empresa="" returnvariable="RsEmpresas">
<!---Preguntar por errores --->
<cfif NOT (RsObras.SUCCESS AND RsEmpresas.SUCCESS) >
	<cf_error_manejador  tipoError="" mensajeError="Error en la consulta de datos.">
</cfif>
<html>
<head>
<title></title>
<link rel="stylesheet" href="../css/style.css" type="text/css">
<link rel="stylesheet" type="text/css" href="../css/box.css">
<script language="javascript" src="../js/jquery.js"></script> 
<script language="javascript" src="../js/funciones.js"></script>
<script>
    function imprimir(opcion)
	{
		if(opcion == 1)
		{
			$('#cargando').fadeIn();
			$('#tbl_Principal').hide();
			form1.action = 'traspasar_indirecto_listado.cfm';
			form1.submit();
		}
		else
		{
			$('#tbl_Principal').hide();
			window.open('traspasar_indirecto_excel.cfm?id_EmpresaOrigen='+$('#id_EmpresaOrigen').val() +'&id_EmpresaDestino='+$('#id_EmpresaDestino').val()+'&id_ObraOrigen='+$('#id_ObraOrigen').val()+'&id_ObraDestino='+$('#id_ObraDestino').val(),'_Self')
		}
	}
</script>

</head>
<body onLoad="$('#id_EmpresaOrigen').focus()">
    <cfinclude template="menu.cfm">&nbsp;
    <cfform name="form1" onsubmit="$('##cargando').fadeIn(); $('##tbl_Principal').hide();">
        <table width="900" cellpadding="0" cellspacing="0" align="center">
            <tr><td class="x-box-tl" width="1px"></td><td class="x-box-tc"></td><td class="x-box-tr" width="1px"></td></tr>
            <tr>
                <td class="x-box-ml"></td>
                <td class="x-box-mc" width="99%">
                    <table width="100%">
                        <tr>
                            <td align="right">
                            <button type="button" class="boton_menu_superior" onClick="javascript:location='traspasar_indirecto.cfm'"><img src="../images/nuevo.png" alt="Clic para agregar" width="24" style="cursor:pointer;"></button>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            Empresa Origen:</td>
                            <td>
                                <cfselect name="id_EmpresaOrigen" id="id_EmpresaOrigen" selected="#id_EmpresaOrigen#" onchange="traerMontos();" query="RsEmpresas.RS" queryposition="below" display="nb_Empresa" value="id_Empresa" class="contenido" style="width:300px;" required="true" message="La empresa de origen no es v\u00e1lida.">
                                    <option value="">SELECCIONE</option>
                                </cfselect>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">Obra Origen:</td>
                            <td>
                                <cfinput  type="text" class="contenido_numerico" name="id_ObraOrigen" id="id_ObraOrigen" style="width:80px;vertical-align: middle;" validate="integer" size="10" value="#id_ObraOrigen#" onBlur="traerMontos();">
                                <cfinput type="text" class="contenido_readOnly"  name="de_ObraOrigen" id="de_ObraOrigen" style="width:300px;vertical-align: middle;" bind="cfc:#Application.componentes#.obras.getObraPorID(1,{id_ObraOrigen@blur})" onFocus="cambiarFocus($(this).before());" bindonload="yes" message="La obra de origen no es v\u00e1lida.">
                                <button type="button" id="btnBuscarObra" onClick="javascript:AbrirPopUpBusquedaChrome('id_ObraOrigen', 'pop_obras_busqueda_indirectos.cfm', 'id_Obra=1', '700px', '600px')" class="boton_popup" value=""><img src="../images/buscar.png" style="width:18px;"></button>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">Empresa Destino:</td>
                            <td>
                                <cfselect name="id_EmpresaDestino" id="id_EmpresaDestino" selected="#id_EmpresaDestino#" query="RsEmpresas.RS" queryposition="below" display="nb_Empresa" value="id_Empresa" class="contenido" style="width:300px;" required="true" message="La empresa de origen no es v\u00e1lida.">
                                    <option value="">SELECCIONE</option>
                                </cfselect>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">Obra Destino:</td>
                            <td>
                                <cfselect name="id_ObraDestino" id="id_ObraDestino" class="contenido" style="width:300px;" required="true" message="La obra de destino no es v\u00e1lida.">
                                    <option value="">SELECCIONE</option>
                                    <cfoutput query="RsObras.RS">
                                        <option value="#id_Obra#" <cfif #RsObras.RS.id_Obra# EQ #id_ObraDestino#>selected</cfif> >#id_Obra# - #de_Obra#</option>
                                    </cfoutput>
                                </cfselect>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" align="center">
                                <button type="button" class="boton_imagen" onClick="imprimir(1);"><img src="../images/filtro_azul.png"></button>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
								<button  type="button" class="boton_menu_superior" onClick="imprimir(2);">
                                    <img src="../images/excel.png" alt="Enviar a Excel" title="clic para generar excel" width="24" border="0">
                                </button>
                            </td>
                        </tr>
                    </table>
                </td>
                <td class="x-box-mr"></td>
            </tr>
            <tr><td class="x-box-bl" width="1px"></td><td class="x-box-bc"></td><td class="x-box-br" width="1px"></td>
        </table><br>
        <div id="cargando" style="display:none;">
        	<center><img src="../images/cargando_procesando.gif"></center>
        </div>
        <cfinput name="sn_Mostrar" type="hidden" value="S">
        <cfif sn_Mostrar EQ 'S'>
            <cfinvoke component="#Session.componentes#.traspasar_indirecto" method="Indirectos_Montos_Listado" returnvariable="RsIndirectos"
                id_EmpresaOrigen="#id_EmpresaOrigen#"
                id_ObraOrigen="#id_ObraOrigen#"
                id_EmpresaDestino="#id_EmpresaDestino#"
                id_ObraDestino="#id_ObraDestino#"
                >
				<cfif #RsIndirectos.tipoError# NEQ "">
                    <cf_error_manejador tipoError="#RsIndirectos.tipoError#" mensajeError="#RsIndirectos.mensaje#">
                </cfif>
       
            <table cellpadding="0" cellspacing="0" width="100%" id="tbl_Principal" class="fondo_listado">
                <tr class="encabezado">
                	<td width="3px"><img src="../images/esquina_izquierda.gif"></td>
                    <td class="cuadricula" align="center">EMPRESA ORIGEN</td>
                    <td class="cuadricula" align="center">OBRA ORIGEN</td>
                    <td class="cuadricula" align="center">EMPRESA DESTINO</td>
                    <td class="cuadricula" align="center">OBRA DESTINO</td>
                    <td class="cuadricula" align="center">MONTO TRASPASADO</td>
                    <td class="cuadricula" align="center">USUARIO REGISTRO</td>
                    <td class="cuadricula" align="center">FECHA REGISTRO</td>
                    <td width="3px"><img src="../images/esquina_derecha.gif"></td>
                </tr>
                <cfoutput query="RsIndirectos.rs">
                    <tr onMouseOver="this.style.backgroundColor='##99CCCC'" onMouseOut="this.style.backgroundColor=''">
                        <td background="../images/orilla_izquierda.gif" style="background-repeat:repeat-y" width="3px"></td>
                        <td class="cuadricula" align="left">#nb_EmpresaOrigen#</td>
                        <td class="cuadricula" align="left">#id_ObraOrigen# - #de_ObraOrigen#</td>
                        <td class="cuadricula" align="left">#nb_EmpresaDestino#</td>
                        <td class="cuadricula" align="left">#id_ObraDestino# - #de_ObraDestino#</td>
                        <td class="cuadricula" align="right">$#NumberFormat(im_Traspasado,',_.__')#</td>
                        <td class="cuadricula" align="left">#nb_Usuario#</td>
                        <td class="cuadricula" align="center">#DateFormat(fh_Registro,'DD/MM/YYYY')#</td>
                        <td background="../images/orilla_derecha.gif" style="background-repeat:repeat-y" width="3px"></td>
                    </tr>
                </cfoutput>
            </table>
        </cfif>
    </cfform>
</body>
</html>

