<cfparam name="Filtro_id_Empresa" default="">
<cfparam name="id_Obra" default="">
<cfparam name="id_Estatus" default="">
<cfparam name="id_Agrupador" default="">
<cfparam name="fh_inicio" default="">
<cfparam name="fh_fin" default="">
<cfparam name="id_Empleado" default="">
<cfparam name="id_Solicitante" default="">


<cfinvoke component="#session.componentes#.Requisiciones" method="RS_RequisicionesDetalles_Listado_excel" returnvariable="RsSolicitudes"
	id_Empresa="#Filtro_id_Empresa#"
	id_obra="#id_Obra#"
	id_Estatus="#id_Estatus#"
	id_Agrupador="#id_Agrupador#"
	fh_inicio="#fh_inicio#"
	fh_fin="#fh_fin#"
	id_Empleado="#id_Empleado#"
	id_Solicitante="#id_Solicitante#"

>

<cfsavecontent variable="contenido"> 
	<table cellpadding="0" cellspacing="0" border="1" style="width:auto">
        <tr>
            <td class="cuadricula" align="center" style="font-size:14px;font-weight:bold;width:auto;background:#000099; color:#FFFFFF">No. requisicion</td>
            <td class="cuadricula" align="center"  style="font-size:14px;font-weight:bold;width:auto;background:#000099; color:#FFFFFF">Fecha Requisicion</td>
            <td class="cuadricula" align="center"  style="font-size:14px;font-weight:bold;width:auto;background:#000099; color:#FFFFFF">Solicitante</td>
            <td class="cuadricula" align="center"  style="font-size:14px;font-weight:bold;width:auto;background:#000099; color:#FFFFFF">Tipo de Requisicion</td>
            <td class="cuadricula" align="center"  style="font-size:14px;font-weight:bold;width:auto;background:#000099; color:#FFFFFF">Estatus</td>
        	
        
    	</tr>
    	<cfoutput query="RsSolicitudes" group="id_Requisicion">

			<tr>
				<td style="font-size:14px;font-weight:bold;width:auto;background:##808080; color:##000000">#id_Requisicion#</td>
				<td style="font-size:14px;font-weight:bold;width:auto;background:##808080; color:##000000">#dateformat(fh_Requisicion,'DD/MM/YYYY')#</td>
				<td style="font-size:14px;font-weight:bold;width:auto;background:##808080; color:##000000">#nb_Solicitante#</td>
				<td style="font-size:14px;font-weight:bold;width:auto;background:##808080; color:##000000">#de_TipoRequisicion#</td>
				<td style="font-size:14px;font-weight:bold;width:auto;background:##808080; color:##000000">#de_Estatus#</td>
			</tr> 
			<cfoutput group="id_insumo">
				<cfif id_insumo NEQ ''>
					<tr>
						<td>#id_Insumo#</td>
						<td>#de_Insumo#</td>
						<td>#de_UnidadMedida#</td>
						<td>#nu_Cantidad#</td>
						<td>#nu_Comprado#</td>
				    </tr>
				<cfelse>
					
				</cfif>
				
				
        	</cfoutput>
        </cfoutput>
    </table>
		
</cfsavecontent>

<cfoutput>
	#contenido#
</cfoutput>
<cfheader name="Content-Disposition" value="inline; filename=Reporte_solicitudes_#DateFormat(Now(),'DD_MM_YYYY')#.xls" >

<cfcontent variable="#ToBinary(ToBase64(contenido))#" type="application/msexcel" >