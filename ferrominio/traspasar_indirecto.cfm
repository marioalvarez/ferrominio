<cfset CurrentPage=GetFileFromPath(GetTemplatePath())>
<cfif isDefined('sn_Guardar') AND sn_Guardar EQ "S">
	<cftransaction>
		<cfinvoke component="#Session.componentes#.traspasar_indirecto" method="Indirectos_Montos_Traspasar" returnvariable="RSTraspaso"
			id_EmpresaOrigen="#id_EmpresaOrigen#"
			id_ObraOrigen="#id_ObraOrigen#"
			id_EmpresaDestino="#id_EmpresaDestino#"
			id_ObraDestino="#id_ObraDestino#"
			im_MontoTraspaso="#nu_MontoTraspaso#"
			id_Empleado="#Session.id_Empleado#">
		<!---Preguntar por errores --->
		<cfif NOT RSTraspaso.SUCCESS>
			<cf_error_manejador  tipoError="" mensajeError="Error en la consulta de datos. #RSTraspaso.MESSAGE#">
		</cfif>
		
		<!--- Genera Solicitud en la obra origen para poder comprar de nuevo los insumos--->
		<cfquery name="RsEncabezado_ObraOrigen" datasource="#session.cnx#" username='#session.user_sql#' password='#session.password_sql#'>
		     Requisiciones_Guardar  #id_ObraOrigen#,#id_EmpresaOrigen#, #Session.id_Empleado# , '', '','','',103,13,NULL
	    </cfquery>

		<!--- GENERA OC EN LA OBRA ORIGEN --->
		<cfif id_EmpresaDestino EQ 1>
			<cfset id_Proveedor = 152>
		<cfelseif id_EmpresaDestino EQ 2>
			<cfset id_Proveedor = 45>
		<cfelse>
			<cfset id_Proveedor = 0>
		</cfif>
	    <cfinvoke method="RSInserta_ordenCompra_transferencia" component="#Session.componentes#.traspasar_indirecto" returnvariable="RSOrdenCompra_origen"
	    	id_Empresa = "#id_EmpresaOrigen#"
	        id_Obra = "#id_ObraOrigen#"
	        id_empleado="#Session.id_Empleado#"
	        id_Almacen = ""
	        id_requisicion="#RsEncabezado_ObraOrigen.id_Requisicion#"
			id_Proveedor="#id_Proveedor#">
		
		<!---Genera detalles de la requisicion en la obra origen --->
		<cfinvoke method="Agregar_insumo_requisicion" component="#Session.componentes#.traspasar_indirecto" returnvariable="Rsdetalles_ObraOrigen"
	    	id_Requisicion = "#RsEncabezado_ObraOrigen.id_Requisicion#"
	     	id_Obra = "#id_ObraOrigen#"
	        id_Empresa="#id_EmpresaOrigen#"
	        id_insumo = "GAO-001"
	        nu_cantidad="1"
	        Unidad_Medida_Especial=""
	        Estatus_det="103">
		 
		<!--- GENERA LOS DETALLES PARA LA OC DE LA OBRA ORIGEN --->
		<cfinvoke method="RSInserta_ordenCompradetalle_transferencia" component="#Session.componentes#.traspasar_indirecto" returnvariable="RSOrdenCompradetalle_origen"
	    	id_Empresa = "#id_EmpresaOrigen#"
	    	id_ordenCompra="#RSOrdenCompra_origen.id_OrdenCompra#"
	        id_Obra = "#id_ObraOrigen#"
	        id_empleado="#Session.id_Empleado#"
	        id_insumo = "GAO-001"
	        im_precio="#nu_MontoTraspaso#"
	        cantidad="1">

		 <!---ACTUALIZA EL TOTAL DE LA OC DE LA OBRA ORIGEN --->
		 <cfinvoke method="RS_actualizaTotales_OC" component="#Session.componentes#.traspasar_indirecto" returnvariable="TotalOC"
	    	id_Empresa = "#id_EmpresaOrigen#"
	    	id_ordenCompra="#RSOrdenCompra_origen.id_OrdenCompra#"
	        id_Obra = "#id_ObraOrigen#"
	        id_empleado="#Session.id_Empleado#"
			id_EmpresaDestino="#id_EmpresaDestino#">
	</cftransaction>
	<cf_location_msg  url="#CurrentPage#" mensaje="La operaci&oacute;n se realiz&oacute; correctamente.">
</cfif>

<!---Invokes --->
<cfinvoke component="#Session.componentes#.traspasar_indirecto" method="get_listado_Obras_Indirectos" returnvariable="RsObras">
<cfinvoke component="#Session.componentes#.traspasar_indirecto" method="get_listado_Empresas" id_Empresa="" returnvariable="RsEmpresas">
<!---Preguntar por errores --->
<cfif NOT (RsObras.SUCCESS AND RsEmpresas.SUCCESS) >
	<cf_error_manejador  tipoError="" mensajeError="Error en la consulta de datos.">
</cfif>

<html>
	<head>
		<title></title>
		<link rel="stylesheet" href="../css/style.css" type="text/css">
		<link rel="stylesheet" type="text/css" href="../css/box.css">
		<script language="javascript" src="../js/jquery.js"></script> 
		<script language="javascript" src="../js/funciones.js"></script>
	</head>
	<body>
		<cfinclude template="menu.cfm">&nbsp;
		
		<cfform name="form1" >
			<table width="600px"  border="0" class="tabla_grid" cellpadding="0" cellspacing="0">
				<tr class="tabla_grid_encabezado">
					<td width="3px"><img src="../images/esquina_izquierda.gif"></td>
			        <td width="100%" align="center" valign="baseline" colspan="2">Traspasar Monto Indirecto</td>
			        <td width="3px"><img src="../images/esquina_derecha.gif"></td>
				</tr>
				<tr>
					<td class="tabla_grid_orilla_izquierda"></td>
					<td colspan="2">&nbsp;</td>
					<td class="tabla_grid_orilla_derecha"></td>
				</tr>
				<tr>
					<td class="tabla_grid_orilla_izquierda"></td>
					<td align="right">
                        Empresa Origen:&nbsp;&nbsp;
					</td>
					<td>
						<cfselect name="id_EmpresaOrigen" id="id_EmpresaOrigen" onchange="traerMontos();" query="RsEmpresas.RS" queryposition="below" display="nb_Empresa" value="id_Empresa" class="contenido" style="width:300px;" required="true" message="La empresa de origen no es v\u00e1lida.">
							<option value="">SELECCIONE</option>
						</cfselect>
                    </td>
					<td class="tabla_grid_orilla_derecha"></td>
				</tr>
				<tr>
					<td class="tabla_grid_orilla_izquierda"></td>
					<td colspan="2">&nbsp;</td>
					<td class="tabla_grid_orilla_derecha"></td>
				</tr>
				<tr>
					<td class="tabla_grid_orilla_izquierda"></td>
					<td align="right">
                        Obra Origen:&nbsp;&nbsp;
					</td>
					<td>
						<cfinput  type="text" class="contenido_numerico" name="id_ObraOrigen" id="id_ObraOrigen" style="width:80px;vertical-align: middle;" validate="integer" size="10" value="" onBlur="traerMontos();">
						<cfinput type="text" class="contenido_readOnly"  name="de_ObraOrigen" id="de_ObraOrigen" style="width:300px;vertical-align: middle;" bind="cfc:#Application.componentes#.obras.getObraPorID(1,{id_ObraOrigen@blur})" onFocus="cambiarFocus($(this).before());" bindonload="yes" message="La obra de origen no es v\u00e1lida.">
						<button type="button" id="btnBuscarObra" onClick="javascript:AbrirPopUpBusquedaChrome('id_ObraOrigen', 'pop_obras_busqueda_indirectos.cfm', 'id_Obra=1', '700px', '600px')" class="boton_popup" value=""><img src="../images/buscar.png" style="width:18px;"></button>
                    </td>
					<td class="tabla_grid_orilla_derecha"></td>
				</tr>
				<tr>
					<td class="tabla_grid_orilla_izquierda"></td>
					<td colspan="2">&nbsp;</td>
					<td class="tabla_grid_orilla_derecha"></td>
				</tr>
				<tr>
					<td class="tabla_grid_orilla_izquierda"></td>
					<td align="right">
                        Monto PPTO.:&nbsp;$
					</td>
					<td>
						<cfinput type="text" name="nu_MontoPPTO" id="nu_MontoPPTO" disabled="disabled" class="contenido_readOnly" style="text-align:right;">
                    </td>
					<td class="tabla_grid_orilla_derecha"></td>
				</tr>
				<tr>
					<td class="tabla_grid_orilla_izquierda"></td>
					<td colspan="2">&nbsp;</td>
					<td class="tabla_grid_orilla_derecha"></td>
				</tr>
				<tr>
					<td class="tabla_grid_orilla_izquierda"></td>
					<td align="right">
                        Monto Gastado:&nbsp;$
					</td>
					<td>
						<cfinput type="text" name="nu_MontoGastado" id="nu_MontoGastado" disabled="disabled" class="contenido_readOnly" style="text-align:right;">
                    </td>
					<td class="tabla_grid_orilla_derecha"></td>
				</tr>
				<tr>
					<td class="tabla_grid_orilla_izquierda"></td>
					<td colspan="2">&nbsp;</td>
					<td class="tabla_grid_orilla_derecha"></td>
				</tr>
				<tr>
					<td class="tabla_grid_orilla_izquierda"></td>
					<td align="right">
                        Monto Disponible:&nbsp;$
					</td>
					<td>
						<cfinput type="text" name="nu_MontoDisponible" id="nu_MontoDisponible" disabled="disabled" class="contenido_readOnly" style="text-align:right;">
                    </td>
					<td class="tabla_grid_orilla_derecha"></td>
				</tr>
				<tr>
					<td class="tabla_grid_orilla_izquierda"></td>
					<td colspan="2">&nbsp;</td>
					<td class="tabla_grid_orilla_derecha"></td>
				</tr>
				<tr>
					<td class="tabla_grid_orilla_izquierda"></td>
					<td align="right">
                        Monto a traspasar:&nbsp;$
					</td>
					<td>
						<cfinput type="text" name="nu_MontoTraspaso" id="nu_MontoTraspaso" class="contenido_flotante" style="text-align:right;">
                    </td>
					<td class="tabla_grid_orilla_derecha"></td>
				</tr>
				<tr>
					<td class="tabla_grid_orilla_izquierda"></td>
					<td colspan="2">&nbsp;</td>
					<td class="tabla_grid_orilla_derecha"></td>
				</tr>
				<tr>
					<td class="tabla_grid_orilla_izquierda"></td>
					<td align="right">
                        Empresa Destino:&nbsp;&nbsp;
					</td>
					<td>
						<cfselect name="id_EmpresaDestino" id="id_EmpresaDestino" query="RsEmpresas.RS" queryposition="below" display="nb_Empresa" value="id_Empresa" class="contenido" style="width:300px;" required="true" message="La empresa de origen no es v\u00e1lida.">
							<option value="">SELECCIONE</option>
						</cfselect>
                    </td>
					<td class="tabla_grid_orilla_derecha"></td>
				</tr>
				<tr>
					<td class="tabla_grid_orilla_izquierda"></td>
					<td colspan="2">&nbsp;</td>
					<td class="tabla_grid_orilla_derecha"></td>
				</tr>
				<tr>
					<td class="tabla_grid_orilla_izquierda"></td>
					<td align="right">
                        Obra Destino:&nbsp;&nbsp;
					</td>
					<td>
						<cfselect name="id_ObraDestino" id="id_ObraDestino" class="contenido" style="width:300px;" required="true" message="La obra de destino no es v\u00e1lida.">
							<option value="">SELECCIONE</option>
							<cfoutput query="RsObras.RS">
								<option value="#id_Obra#">#id_Obra# - #de_Obra#</option>
							</cfoutput>
						</cfselect>
                    </td>
					<td class="tabla_grid_orilla_derecha"></td>
				</tr>
				<tr>
					<td class="tabla_grid_orilla_izquierda"></td>
					<td colspan="2">&nbsp;</td>
					<td class="tabla_grid_orilla_derecha"></td>
				</tr>
				<tr>
					<td class="tabla_grid_orilla_izquierda"></td>
					<td align="center" colspan="2">
                        <button type="submit" name="guardar" id="guardar" class="boton_imagen" title="Guardar" onClick="return validar()"><img src="../images/guardar.png"></button>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <button type="button" class="boton_imagen" onClick="history.go(-1);" title="Cancelar"><img src="../images/regresar.png"></button>
                    </td>
					<td class="tabla_grid_orilla_derecha"></td>
				</tr>
				<tr class="tabla_grid_footer">
				    <td width="3px"><img src="../images/abajo_izquierda.gif" /></td>
				    <td colspan="2"></td>
				    <td width="3px"><img src="../images/abajo_derecha.gif" /></td>
				</tr>
			</table>
			<cfinput type="hidden" name="sn_Guardar" id="sn_Guardar" value="S"> 
		</cfform>
	</body>
	<script>
		
		$( document ).ready(
			function ()
			{
				
			}
		);
		
		function traerMontos()
		{
			$('#nu_MontoPPTO').val('');
			$('#nu_MontoGastado').val('');
			$('#nu_MontoDisponible').val('');
			if ($('#id_ObraOrigen').val() == '' || $('#id_EmpresaOrigen').val() == '')
				return false;
			$.ajax({
				type: 'GET',
				async: true,
				dataType: 'json',
				url: 'componentes/traspasar_indirecto.cfc',
				data: {
					method: 			'get_Indirectos_Montos',
					id_Empresa:			$('#id_EmpresaOrigen').val(),
					id_Obra:			$('#id_ObraOrigen').val()
				},
				success: function(response){
					if (response.SUCCESS) 
					{
						var im_Presupuesto = response.RS.DATA[0][response.RS.	COLUMNS.indexOf("IM_PRESUPUESTO")],
							im_Gastado = Math.abs(response.RS.DATA[0][response.RS.COLUMNS.indexOf("IM_GASTADO")]);
						$('#nu_MontoPPTO').val(Number(im_Presupuesto).format(2));
						$('#nu_MontoGastado').val(Number(im_Gastado).format(2));
						$('#nu_MontoDisponible').val( (Number(im_Presupuesto)-Number(im_Gastado)).format(2)  );
					}
				},
				error: function()
				{
					alert('Error al traer los datos.\n\n\nFavor de intentar mas tarde.');
				}
			});
		}
		
		function validar()
		{
			if ( $('#nu_MontoTraspaso').val().split(',').join('') == '' || $('#nu_MontoTraspaso').val().split(',').join('') == '.' || isNaN($('#nu_MontoTraspaso').val().split(',').join('')) || Number($('#nu_MontoTraspaso').val().split(',').join('')) == 0)
			{
				alert('El monto del traspaso no es valido.\n\n\nFavor de verificar.');
				return false;
			}
			var nu_MontoTraspaso = Number($('#nu_MontoTraspaso').val().split(',').join(''));
			
			if ( $('#nu_MontoDisponible').val() == '' || $('#nu_MontoDisponible').val().split(',').join('') == '.' || isNaN($('#nu_MontoDisponible').val().split(',').join('')) )
			{
				alert('El monto disponible no es valido.\n\n\nFavor de verificar.');
				return false;
			}
			var nu_MontoDisponible = Number($('#nu_MontoDisponible').val().split(',').join(''));
			
			if ( nu_MontoDisponible <= 0 )
			{
				alert('El monto disponible para traspasar es menor o igual que 0.\n\n\nFavor de verificar.');
				return false;
			}
			else if ( nu_MontoTraspaso > nu_MontoDisponible )
			{
				alert('El monto del traspaso debe de ser menor o igual que el monto disponible.\n\n\nFavor de verificar.');
				return false;
			}
			
			return true;
		}
		
		/**
		 * Number.prototype.format(n, x)
		 * 
		 * @param integer n: length of decimal
		 * @param integer x: length of sections
		 */
		Number.prototype.format = function(n, x) {
		    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
		    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
		};
	</script>
</html>

