	function CargarPerfiles(sync)
	{
		$('#nu_Peso').val('');
		$('#nu_LongitudCom').val('');
		
		//Limpiamos el select para decirle al usuario que esta cargando los datos 
		document.getElementById("id_Perfil").length = 0;
		$( '#id_Perfil' ).append( '<option value=""></option>' );
		$( '#id_Perfil' ).append( '<option value=""></option>' );
		$( '#id_Perfil' ).append( '<option value=""></option>' );
		$( '#id_Perfil' ).append( '<option value=""></option>' );
		$( '#id_Perfil' ).append( '<option value="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Cargando datos ...</option>' );
		
		if ( $('#id_TipoPerfil').val() != '' )
		{
			$.ajax({
				type: 'GET',
				async: sync == 1 ? false : true,
				dataType: 'JSON',
				url: 'componentes/generadores.cfc',
				data: {
					method:			'get_listado_Perfiles_TipoPerfil',
					id_TipoPerfil: 	$('#id_TipoPerfil').val()
				},
				success: function(response) {
					//Limpiamos el select
					document.getElementById("id_Perfil").length = 0;
					$( '#id_Perfil' ).append( '<option value="">SELECCIONE UN PERFIL</option>' );
					
					if (response.SUCCESS)
					{
						//Recorremos el query
						for(var i=0;i<response.RS.DATA.length;i++)
						{
							var value = response.RS.DATA[i][0];
							var DisplayValue = response.RS.DATA[i][1];
							
							$( '#id_Perfil' ).append( '<option value="'+ value + '">' + DisplayValue + '</option>' );
						}
					}
				}	
			});
		}
		else
		{
			//Limpiamos el select
			document.getElementById("id_Perfil").length = 0;
		}
	}
	
	function filtrarPerfil( descripcion )
	{
		$("#id_Perfil option").each(
			function()
			{
				descripcion_Option = $(this).text().toUpperCase();
				if  ( descripcion_Option.search(descripcion.toUpperCase()) != -1  )
					$(this).show();
				else
					$(this).hide();
			}
		);
	}
	
	function CancelarFiltroPerfil()
	{
		$("#txt_FiltroPerfil").val('');
		$("#id_Perfil option").each(
			function()
			{
				$(this).show();
			}
		);
	}
	
	function filtrarTipoPerfil( descripcion )
	{
		$("#id_TipoPerfil option").each(
			function()
			{
				descripcion_Option = $(this).text().toUpperCase();
				if  ( descripcion_Option.search(descripcion.toUpperCase()) != -1  )
					$(this).show();
				else
					$(this).hide();
			}
		);
	}

	function CancelarFiltroTipoPerfil()
	{
		$("#txt_FiltroTipoPerfil").val('');
		$("#id_TipoPerfil option").each(
			function()
			{
				$(this).show();
			}
		);
	}

	function CerrarModal()
	{
		$('#modal_InsumosDetalle').hide();
	}
	
	function CerrarModal_Password()
	{
		$('#modal_Pass').hide();
	}
	
	function CerrarModal_LongitudCom()
	{
		$('#modal_LongitudComercial').hide();
	}