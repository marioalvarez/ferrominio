<cfparam name="id_Empresa" default="">
<cfparam name="id_Obras" default="">
<cfparam name="id_Agrupador" default="">
<cfdump var="#id_Obras#">
<cfinvoke component="#session.componentes#.reportes" method="Reporte_General" returnvariable="RsListado" 
    id_Empresa="#id_Empresa#"
    id_Obras="#id_Obras#"
    id_Agrupador="#id_Agrupador#"
>
<cfif #RsListado.SUCCESS# EQ false>
    <cf_error_manejador mensajeError="#RsListado.MESSAGE#">                                                                         
</cfif> 
<cfquery name="RsObra" datasource="#session.cnx#">
	SELECT id_Obra,de_Obra FROM Obras WHERE id_Obra in (#id_Obras#) AND id_Empresa = #id_Empresa# 
</cfquery> 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
<cfform name="form1">
<cfsavecontent variable="contenido">
	<style>
		@page {
        		size:auto;
        		margin:2%;
        		@top-right {
                  	content: "Pagina " counter(page) " de " counter(pages);
               			}
    				}
		table{size:auto; width:100%; font-size:8px;}
    	.cuadricula{
			border-right:1px solid;
			border-bottom:1px solid;
		}
		.encabezado_grande{
			background-color:#CCCCCC;
		}
		.tabla_grid_orilla_izquierda{
			border-left:1px solid;
		}
		.tabla{
			font-size:11px;
		}
    </style>
    <table border="1" align="center" cellpadding="0" cellspacing="0" width="100%">
    	<tr class="rpt_fondo_encabezado" height="30px">
            <td align="center" colspan="5" style="font-size:14px; font-weight:bold;">
            	Reporte Comparativo
            </td>
        </tr>
        <tr class="rpt_fondo_encabezado">
            <td align="center" colspan="5" style="font-size:14px; font-weight:bold;">Obra:&nbsp;
              <cfoutput query="RsObra"> #RsObra.id_Obra# - #RsObra.de_Obra#</cfoutput>
                
            </td>
        </tr>
     </table>  <br>                 
	 <table cellpadding="0" cellspacing="0" width="100%" class="tabla">
        <tr class="encabezado_grande" style="font-size:10px;">
            <td class="cuadricula" colspan="3"></td>
            <td class="cuadricula" colspan="3" align="center">PRESUPUESTO</td>
            <td class="cuadricula" colspan="2" align="center">SOLICITADO</td>
            <td class="cuadricula" colspan="3"align="center">COMPRADO</td>
            <td class="cuadricula" colspan="2" align="center">EJECUTADO</td>
            <td class="cuadricula" colspan="2" align="center">DISPONIBLE</td>
        </tr>
        <tr class="encabezado_grande" style="font-size:10px;">
            <td class="cuadricula" align="center" nowrap="nowrap" width="11%">CLAVE</td>
            <td class="cuadricula" align="center" nowrap="nowrap" width="11%">DESCRIPCION</td>
            <td class="cuadricula" align="center" nowrap="nowrap" width="6%">UM</td>
            <td class="cuadricula" align="center" nowrap="nowrap" width="6%">CANTIDAD</td>
            <td class="cuadricula" align="center" nowrap="nowrap" width="6%">PRECIO</td>
            <td class="cuadricula" align="center" nowrap="nowrap" width="6%">IMPORTE</td>
            <td class="cuadricula" align="center" nowrap="nowrap" width="6%">CANTIDAD</td>
            <td class="cuadricula" align="center" nowrap="nowrap" width="6%">IMPORTE</td>
            <td class="cuadricula" align="center" nowrap="nowrap" width="6%">CANTIDAD</td>
            <td class="cuadricula" align="center" nowrap="nowrap" width="6%">PRECIO</td>
            <td class="cuadricula" align="center" nowrap="nowrap" width="6%">IMPORTE</td>
            <td class="cuadricula" align="center" nowrap="nowrap" width="6%">ENTRADA</td>
            <td class="cuadricula" align="center" nowrap="nowrap" width="6%">SALIDA</td>
            <td class="cuadricula" align="center" nowrap="nowrap" width="6%">CANTIDAD</td>
            <td class="cuadricula" align="center" nowrap="nowrap" width="6%">IMPORTE</td>
        </tr>
        <cfoutput query="RsListado.RS" group="id_Agrupador">
            <cfquery dbtype="query"  name="RSTotalesAgrupador">
                SELECT 
                    SUM(nu_CantidadExplosion) AS nu_CantidadAgrupador,
                    SUM(im_PrecioExplosion) AS im_PrecioExplosion,
                    SUM(im_SubtotalExplosion) AS im_SubtotalAgrupador,
                    SUM(nu_AplicadoEjecutado_ot) AS nu_AplicadoEjecutado_ot_Agrupador,
                    SUM(im_AplicadoEjecutado_ot) AS im_AplicadoEjecutado_ot_Agrupador,
                    SUM(nu_AplicadoEjecutado_dst) AS nu_AplicadoEjecutado_dst_Agrupador,
                    SUM(im_AplicadoEjecutado_dst) AS im_AplicadoEjecutado_dst_Agrupador,
                    SUM(nu_CantidadCompra) AS nu_CantidadCompra_agrupador,
                    SUM(im_CantidadPrecioCompra) AS im_CantidadPrecioCompra_agrupador,
                    SUM(im_Disponible) AS im_DisponibleAgrupador,
                    sum(nu_Disponible) AS nu_DisponibleAgrupador,
                    SUM(nu_Cantidad) AS nu_SolicitadoAgrupador,

                    SUM(nu_Entrada) AS nu_EntradaAgrupador,
                    SUM(nu_Salida) AS nu_SalidaAgrupador,

                    AVG(im_PrecioExplosion) AS im_PrecioExplosion_Agrupador,
                    AVG(im_PrecioCompra) AS im_PrecioCompra_Agrupador
                FROM
                    RsListado.RS
                WHERE 
                    id_Agrupador = '#id_Agrupador#'
            </cfquery>
            <tr style="font-size:10px;">
                <td class="cuadricula" style="background:##999999;" align="center">#id_Agrupador#</td>
                <td class="cuadricula" style="background:##999999;" align="left">#de_Agrupador#</td>
                <td class="cuadricula" style="background:##999999;" align="center">#de_UnidadMedidaAgrupador#</td>
                <td class="cuadricula" style="background:##999999;" align="right">#NumberFormat(RSTotalesAgrupador.nu_CantidadAgrupador,',____.__')#</td>
                <td class="cuadricula" style="background:##999999;" align="right">$#NumberFormat(RSTotalesAgrupador.im_PrecioExplosion_Agrupador,',____.__')#</td>
                <td class="cuadricula" style="background:##999999;" align="right">$#NumberFormat(RSTotalesAgrupador.im_SubtotalAgrupador,',____.__')#</td>
                <td class="cuadricula" style="background:##999999;" align="right">#NumberFormat(RSTotalesAgrupador.nu_SolicitadoAgrupador,',____.__')#</td>
                <td class="cuadricula" style="background:##999999;"></td>
                <td class="cuadricula" style="background:##999999;" align="right">#NumberFormat(RSTotalesAgrupador.nu_CantidadCompra_agrupador,',____.__')#</td>
                <td class="cuadricula" style="background:##999999;" align="right">$#NumberFormat(RSTotalesAgrupador.im_PrecioCompra_Agrupador,',____.__')#</td>
                <td class="cuadricula" style="background:##999999;" align="right">$#NumberFormat(RSTotalesAgrupador.im_CantidadPrecioCompra_agrupador,',____.__')#</td>
                <td class="cuadricula" style="background:##999999;" align="right">#NumberFormat(RSTotalesAgrupador.nu_EntradaAgrupador,',____.__')#</td>
                <td class="cuadricula" style="background:##999999;" align="right">#NumberFormat(RSTotalesAgrupador.nu_SalidaAgrupador,',____.__')#</td>
                <td class="cuadricula" style="background:##999999;" align="right">#NumberFormat(RSTotalesAgrupador.nu_DisponibleAgrupador,',____.__')#</td>
                <td class="cuadricula" style="background:##999999;" align="right">$#NumberFormat(RSTotalesAgrupador.im_DisponibleAgrupador,',____.__')#</td>
            </tr>
            <cfoutput group="id_Insumo">
            	<cfif #id_Insumo# NEQ ''>
                <tr >
                    <td class="cuadricula" align="center">#id_Insumo#</td>
                    <td class="cuadricula" align="left">#de_Insumo#</td>
                    <td class="cuadricula" align="center">#de_UnidadMedida#</td>
                    <td class="cuadricula" align="right">#NumberFormat(nu_CantidadExplosion,',____.__')#</td>
                    <td class="cuadricula" align="right">$#NumberFormat(im_PrecioExplosion,',____.__')#</td>
                    <td class="cuadricula" align="right">$#NumberFormat(im_SubtotalExplosion,',____.__')#</td>
                    <td class="cuadricula" align="right">#NumberFormat(nu_Cantidad,',____.__')#</td>
                    <td class="cuadricula" align="right">$#NumberFormat(im_Precio,',____.__')#</td>
                    <td class="cuadricula" align="right">#NumberFormat(nu_CantidadCompra,',____.__')#</td>
                    <td class="cuadricula" align="right">$#NumberFormat(im_PrecioCompra,',____.__')#</td>
                    <td class="cuadricula" align="right">$#NumberFormat(im_CantidadPrecioCompra,',____.__')#</td>
                    <td class="cuadricula" align="right">#NumberFormat(nu_Entrada,',____.__')#</td>
                    <td class="cuadricula" align="right">#NumberFormat(nu_Salida,',____.__')#</td>
                    <td class="cuadricula" align="right">#NumberFormat(nu_Disponible,',____.__')#</td>
                    <td class="cuadricula" align="right">$#NumberFormat(im_Disponible,',____.__')#</td>
                </tr>
                </cfif>
            </cfoutput>
             
        </cfoutput>
    </table>
</cfsavecontent>
</cfform>
<cfinvoke component="#Application.componentes#.javaLoader"
    method="generatePDF" 
    content="#contenido#"
    forceDownload="false"
    pdf="Matriz de Seguimiento - Autorizar"
>
</body>
</html>
