<cfparam name="Filtro_id_TipoInsumo" default="0">
<cfparam name="buscar" default="">
<cfparam name="clave" default="">
<cfparam name="sn_Mostrar" default="N">
<cfparam name="sn_Activo" default="">
<cfparam name="id_Agrupador" default="">
<cfparam name="id_TipoPieza" default="">
<cfparam name="Filtro_id_Agrupador" default="">

<cfinvoke component="#session.componentes#.insumos" method="RSMostrarInsumosFiltrado" returnvariable="RSInsumos" 
    id_insumo="#clave#" 
    de_insumo="#buscar#" 
    id_Agrupador="#Filtro_id_Agrupador#" 
    sn_Activo="#sn_Activo#"
>
<cfsavecontent variable="contenido"> 
<table border="1" cellpadding="0" cellspacing="0" width="80%" class="fondo_listado" align="center">
	<tr class="encabezado">
		<td align="center">Clave</td>
		<td align="center" width="30%">Nombre</td>
		<td align="center">U.M.</td>
		<td align="center">Agrupador</td>
        <td align="center">Tipo Insumo</td>
		<!--- <td align="center">Grupo</td>
		<td align="center">SubGrupo</td> --->
		<td align="center">Activo</td>
	</tr>
	<cfoutput query="RSInsumos.rs">
		<tr>
			<td class="cuadricula" align="center">#RSInsumos.rs.id_Insumo#</td>
		  	<td class="cuadricula">#RSInsumos.rs.de_Insumo#</td>
		    <td class="cuadricula" align="center">#RSInsumos.rs.de_UnidadMedida#</td>
		    <td class="cuadricula" align="center"><cfif #RSInsumos.rs.de_agrupador# EQ ''>
		    										N/A
		    									  <cfelse>			
		    										#RSInsumos.rs.de_Agrupador#			
		    									  </cfif>
			</td>
			<td class="cuadricula" align="center">#RSInsumos.rs.de_TipoInsumo#</td>
			<!--- <td class="cuadricula" align="center">#RSInsumos.rs.de_GrupoInsumo#</td>
			<td class="cuadricula" align="center">#RSInsumos.rs.de_SubGrupoInsumo#</td> --->
			<td class="cuadricula" align="center">
				<cfswitch expression="#RSInsumos.rs.sn_Activo#">
					<cfcase value="1">SI</cfcase>
					<cfcase value="0">NO</cfcase>
					<cfdefaultcase>NO</cfdefaultcase>
				</cfswitch>
			</td>
		</tr>
	</cfoutput>
</table>
</cfsavecontent>
<cfoutput>
	#contenido#
</cfoutput>

<cfheader name="Content-Disposition" value="inline; filename=Reporte_insumos_#DateFormat(Now(),'DD_MM_YYYY')#.xls" >
<cfcontent variable="#ToBinary(ToBase64(contenido))#" type="application/msexcel" >