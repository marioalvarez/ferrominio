<cfset CurrentPage=GetFileFromPath(GetTemplatePath())>
<cfparam name="Filtro_id_Empresa" default="">
<cfparam name="id_Obra" default="">
<cfparam name="id_Almacen" default="">
<cfparam name="de_Comentario" default="">
<cfparam name="nb_Recibe" default="">
<cfparam name="de_DomicilioBodega" default="">
<cfparam name="de_Telefono" default="">
<cfparam name="listaInsumos" default="">
<cfparam name="listaCantidades" default="">
<cfparam name="sn_Enviar" default="N">
<cfparam name="fh_inicio" default="">
<cfparam name="fh_fin" default="">
<cfparam name="Filtro_fh_fin" default="#fh_fin#">
<cfparam name="Filtro_id_Empleado" default="">
<cfparam name="sn_MenuResidente" default="0">

<cfinvoke component="#session.componentes#.insumos" method="obtenerAgrupadores" returnvariable="RS_MostrarAgrupadores">

<cfinvoke component="#session.componentes#.empresas" method="RSMostrarTodosEmpresas" returnvariable="RsEmpresa">

<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="../css/style.css" type="text/css">
        <link rel="stylesheet" type="text/css" href="../css/box.css">
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <title>Untitled Document</title>
        <!---Refs de Calendario --->
        <script type="text/javascript" src="../calendario/calendar.js"></script>
        <script type="text/javascript" src="../calendario/calendar-setup.js"></script>
        <script type="text/javascript" src="../calendario/lang/calendar-es.js"></script>
        <style type="text/css"> @import url("../calendario/calendar-blue2.css");</style>
        <!---Termina Refs de Calendario--->
        <script language="javascript" src="../js/jquery.js"></script>
        <script language="javascript" src="../js/funciones.js"></script>
        <script type="text/javascript">
            
            var inicio=function(){
                var last,first=false;
                $('#Filtro_id_Empresa').focus();
            var validarFechas=function(fh_inicio,fh_fin)
            {
                var fechain=fh_inicio;
                var fechafn=fh_fin;
                var fecha_inicial=fechain.split('/').reverse().join('');
                var fecha_final=fechafn.split('/').reverse().join('');
                bandera=false;
                
                if(!(parseInt(fecha_inicial) > parseInt(fecha_final))){
                   bandera=true;
                 
                }

               return bandera;
            }       
            var id_Empresa_static;
            var id_obra_static;

            var Listado=function(){
                
                var page;
                $('#tbl1').addClass('mostrar');
                var id_Empresa= $('#Filtro_id_Empresa').val();
                var id_obra=$('#Filtro_id_Obra').val();  
                var id_Estatus=$('#id_Estatus').val();
                var id_Solicitante=$('#id_Solicitante').val();  
                
                if(first){
                    page=$('#Primera_'+id_Empresa+'_'+id_obra).attr('page');
                }else if(last){
                    page=$('#Ultimo_'+id_Empresa+'_'+id_obra).attr('page');
                }else{
                    if(typeof($('#antes_'+id_Empresa+'_'+id_obra).attr('page')) === "undefined" && typeof($('#siguiente_'+id_Empresa+'_'+id_obra).attr('page')) === "undefined"){
                        page=1;
                   
                     }else{
                        page=$('#antes_'+id_Empresa+'_'+id_obra).attr('page');
                     }
                }
                

                var id_Agrupador=$('#id_Agrupador').val();
                var fh_inicio=$('#fh_inicio').val();
                var fh_fin=$('#fh_fin').val();
                var puesto=<cfoutput>#session.id_puesto#</cfoutput>;
                if(puesto==209 || puesto==111 || puesto == 104){
                     var id_Empleado='';
                }else{
                      var id_Empleado=<cfoutput>#session.id_Empleado#</cfoutput>;  
                }
        

                id_Empresa_static=id_Empresa;
                id_obra_static=id_obra;


                    if(id_Empresa!="" && id_obra!=""){
                        
                        if(!((fh_inicio!='' && fh_fin=='') || (fh_inicio=='' && fh_fin!=''))){
                            
                            if(validarFechas(fh_inicio,fh_fin)){    
                                
                                $('#cargando').html('<img src="../images/cargando_procesando.gif" /></p>');

                                $.ajax({
                                    url:"componentes/Requisiciones.cfc?method=RS_RequisicionesDetalles_Listado",
                                    method:'POST',
                                    data:{
                                            id_Empresa:id_Empresa,
                                            id_obra:id_obra,
                                            id_Estatus:id_Estatus,
                                            id_Agrupador:id_Agrupador,
                                            fh_inicio:fh_inicio,
                                            fh_fin:fh_fin,
                                            id_Empleado:id_Empleado,
                                            id_Solicitante:id_Solicitante,
                                            page:page
                                    },
                                    cache:false,
                                    success:function(response){
                                        var arreglo=JSON.parse(response);
                                        var id_renglon=0;
                                        var renglones=""; 
                                        
                                        if(arreglo.length>0){
                                                var UME="";
                                                for(var i=0;i<arreglo.length;i++){

                                                    
                                                    id_renglon++;
                                                    var fecha=arreglo[i].fecha_req.split('-');
                                                    var fecha_format=fecha[2]+'/'+fecha[1]+'/'+fecha[0];
                                                    if(puesto==209 || puesto==111 ){
                                                         if(arreglo[i].Estatus=='Autorizada'){
                                                                renglones+="<tr id='id_"+id_renglon+"' class='cuadricula' border='5px' title='click para ver detalle'>"+
                                                                               "<td align='right' class='cuadricula Requisicion' id='id_td_"+id_renglon+"'>"+arreglo[i].No_Requisicion+"</td>"+
                                                                                "<td align='center' class='cuadricula Requisicion' id='id_td_"+id_renglon+"'>"+fecha_format+"</td>"+
                                                                                "<td align='center' class='cuadricula Requisicion' id='id_td_"+id_renglon+"'>"+arreglo[i].solicitante+"</td>"+
                                                                                "<td align='center' class='cuadricula Requisicion' id='id_td_"+id_renglon+"'>"+arreglo[i].tipo_requisicion+"</td>"+
                                                                                "<td align='center' class='cuadricula Requisicion' id='id_td_"+id_renglon+"' style='width:125px'>"+arreglo[i].Estatus+"</td>"+
                                                                                "<td align='center' class='cuadricula Requisicion' id='id_td_"+id_renglon+"' style='width:125px'>"+arreglo[i].de_Comentarios+"</td>"+
                                                                                "<td align='center' class='cuadricula' width='1%' id='Acciones_"+id_renglon+"' style='cursor:pointer;height:25px;font-weight:bold;'>"+
                                                                                    "<a href='#'> <img src='../images/edit_gris.png' id='Editar_"+id_renglon+"' class='editar' alt='' title='Editar Requisicion'></a>"+
                                                                                    "<a href='#'> <img src='../images/delete_bak.png' id='cancelar_"+id_renglon+"' class='cancelar' alt='' title='Cancelar Requisicion'></a>"+
                                                                                    "<a href='#'> <img src='../images/eliminar.png' id='eliminar_"+id_renglon+"' class='Eliminar' alt='' title='Eliminar Requisicion' style='width:15px;'></a>"+
                                                                                "</td>"+
                                                                              
                                                                            "</tr>"+
                                                                            "<tr class='id_"+id_renglon+"' style='background-color:#B4CBE6;height:15px;font-weight:bold;display:none;' title='click para ver detalle'>"+
                                                                                "<td></td>"+
                                                                                "<td align='center' class='cuadricula' style='background-color:#B4CBE6;'>Id insumo</td>"+
                                                                                "<td align='center' class='cuadricula'  style='background-color:#B4CBE6;'>Descripcion</td>"+
                                                                                "<td align='center' class='cuadricula' style='background-color:#B4CBE6;'>Unidad de Medida</td>"+
                                                                                "<td align='center' class='cuadricula' style='background-color:#B4CBE6;'>Medida Especial</td>"+
                                                                                "<td align='center' class='cuadricula' style='background-color:#B4CBE6;'>Cantidad</td>"+
                                                                                "<td align='center' class='cuadricula' style='background-color:#B4CBE6;'>Comprado</td>"+
                                                                              
                                                                            "</tr>"; 

                                                            }else if(arreglo[i].Estatus=='Cancelada'){
                                                                renglones+="<tr id='id_"+id_renglon+"' class='cuadricula' border='5px' title='click para ver detalle'>"+
                                                                              
                                                                                "<td align='right' class='cuadricula Requisicion' id='id_td_"+id_renglon+"'>"+arreglo[i].No_Requisicion+"</td>"+
                                                                                "<td align='center' class='cuadricula Requisicion' id='id_td_"+id_renglon+"'>"+fecha_format+"</td>"+
                                                                                "<td align='center' class='cuadricula Requisicion' id='id_td_"+id_renglon+"'>"+arreglo[i].solicitante+"</td>"+
                                                                                "<td align='center' class='cuadricula Requisicion' id='id_td_"+id_renglon+"'>"+arreglo[i].tipo_requisicion+"</td>"+
                                                                                "<td align='center' class='cuadricula Requisicion' id='id_td_"+id_renglon+"' style='width:125px'>"+arreglo[i].Estatus+"</td>"+
                                                                                "<td align='center' class='cuadricula Requisicion' id='id_td_"+id_renglon+"' style='width:125px'>"+arreglo[i].de_Comentarios+"</td>"+
                                                                                "<td align='center' class='cuadricula' width='1%' id='Acciones_"+id_renglon+"' style='cursor:pointer;height:25px;font-weight:bold;'>"+
                                                                                    "<a href='#'> <img src='../images/edit_gris.png' id='Editar_"+id_renglon+"' class='editar' alt='' title='Editar Requisicion'></a>"+
                                                                                    "<a href='#'> <img src='../images/delete_gris.png' id='cancelar_"+id_renglon+"' class='cancelar' alt='' title='Cancelar Requisicion'></a>"+
                                                                                    "<a href='#'> <img src='../images/eliminar.png' id='eliminar_"+id_renglon+"' class='Eliminar' alt='' title='Eliminar Requisicion' style='width:15px;'></a>"+
                                                                                "</td>"+
                                                                                
                                                                            "</tr>"+
                                                                            "<tr class='id_"+id_renglon+"' style='background-color:#B4CBE6;height:15px;font-weight:bold;display:none;' title='click para ver detalle'>"+
                                                                                "<td></td>"+
                                                                                  "<td align='center' class='cuadricula' style='background-color:#B4CBE6;'>Id insumo</td>"+
                                                                                    "<td align='center' class='cuadricula'  style='background-color:#B4CBE6;'>Descripcion</td>"+
                                                                                    "<td align='center' class='cuadricula' style='background-color:#B4CBE6;'>Unidad de Medida</td>"+
                                                                                    "<td align='center' class='cuadricula' style='background-color:#B4CBE6;'>Medida Especial</td>"+
                                                                                    "<td align='center' class='cuadricula' style='background-color:#B4CBE6;'>Cantidad</td>"+
                                                                                    "<td align='center' class='cuadricula' style='background-color:#B4CBE6;'>Comprado</td>"+
                                                                               
                                                                            "</tr>";   
                                                            }
                                                            else if(arreglo[i].Estatus=='Comprado'){
                                                                renglones+="<tr id='id_"+id_renglon+"' class='cuadricula' border='5px' title='click para ver detalle'>"+
                                                                              
                                                                                "<td align='right' class='cuadricula Requisicion' id='id_td_"+id_renglon+"'>"+arreglo[i].No_Requisicion+"</td>"+
                                                                                "<td align='center' class='cuadricula Requisicion' id='id_td_"+id_renglon+"'>"+fecha_format+"</td>"+
                                                                                "<td align='center' class='cuadricula Requisicion' id='id_td_"+id_renglon+"'>"+arreglo[i].solicitante+"</td>"+
                                                                                "<td align='center' class='cuadricula Requisicion' id='id_td_"+id_renglon+"'>"+arreglo[i].tipo_requisicion+"</td>"+
                                                                                "<td align='center' class='cuadricula Requisicion' id='id_td_"+id_renglon+"' style='width:125px'>"+arreglo[i].Estatus+"</td>"+
                                                                                "<td align='center' class='cuadricula Requisicion' id='id_td_"+id_renglon+"' style='width:125px'>"+arreglo[i].de_Comentarios+"</td>"+
                                                                                "<td align='center' class='cuadricula' width='1%' id='Acciones_"+id_renglon+"' style='cursor:pointer;height:25px;font-weight:bold;'>"+
                                                                                    "<a href='#'> <img src='../images/edit_gris.png' id='Editar_"+id_renglon+"' class='editar' alt='' title='Editar Requisicion'></a> "+
                                                                                    "<a href='#'> <img src='../images/delete_gris.png' id='' class='cancelar' alt='' title='Cancelar Requisicion'></a> "+
                                                                                    "<a href='#'> <img src='../images/eliminar.png' id='eliminar_"+id_renglon+"' class='Eliminar' alt='' title='Eliminar Requisicion' style='width:15px;'></a>"+
                                                                                "</td>"+
                                                                              
                                                                                "</tr>"+
                                                                                "<tr class='id_"+id_renglon+"' style='background-color:#B4CBE6;height:15px;font-weight:bold;display:none;' title='click para ver detalle'>"+
                                                                                    "<td></td>"+
                                                                                    "<td align='center' class='cuadricula' style='background-color:#B4CBE6;'>Id insumo</td>"+
                                                                                    "<td align='center' class='cuadricula'  style='background-color:#B4CBE6;'>Descripcion</td>"+
                                                                                    "<td align='center' class='cuadricula' style='background-color:#B4CBE6;'>Unidad de Medida</td>"+
                                                                                    "<td align='center' class='cuadricula' style='background-color:#B4CBE6;'>Medida Especial</td>"+
                                                                                    "<td align='center' class='cuadricula' style='background-color:#B4CBE6;'>Cantidad</td>"+
                                                                                    "<td align='center' class='cuadricula' style='background-color:#B4CBE6;'>Comprado</td>"+
                                                                                    
                                                                                "</tr>";   


                                                            }else{
                                                                renglones+="<tr id='id_"+id_renglon+"' class='cuadricula' border='5px' title='click para ver detalle'>"+
                                                                               
                                                                                "<td align='right' class='cuadricula Requisicion' id='id_td_"+id_renglon+"'>"+arreglo[i].No_Requisicion+"</td>"+
                                                                                "<td align='center' class='cuadricula Requisicion' id='id_td_"+id_renglon+"'>"+fecha_format+"</td>"+
                                                                                "<td align='center' class='cuadricula Requisicion' id='id_td_"+id_renglon+"'>"+arreglo[i].solicitante+"</td>"+
                                                                                "<td align='center' class='cuadricula Requisicion' id='id_td_"+id_renglon+"'>"+arreglo[i].tipo_requisicion+"</td>"+
                                                                                "<td align='center' class='cuadricula Requisicion' id='id_td_"+id_renglon+"' style='width:125px'>"+arreglo[i].Estatus+"</td>"+
                                                                                "<td align='center' class='cuadricula Requisicion' id='id_td_"+id_renglon+"' style='width:125px'>"+arreglo[i].de_Comentarios+"</td>"+
                                                                                "<td align='center' class='cuadricula' width='1%' id='Acciones_"+id_renglon+"' style='cursor:pointer;height:25px;font-weight:bold;'>"+
                                                                                    "<a href='#'> <img src='../images/edit.png' id='Editar_"+id_renglon+"' class='editar' alt='' title='Editar Requisicion'></a>"+
                                                                                    "<a href='#'> <img src='../images/delete_bak.png' id='cancelar_"+id_renglon+"' class='cancelar' alt='' title='Cancelar Requisicion'></a> "+
                                                                                    "<a href='#'> <img src='../images/eliminar.png' id='eliminar_"+id_renglon+"' class='Eliminar' alt='' title='Eliminar Requisicion' style='width:15px;'></a> "+
                                                                                "</td>"+
                                                                                
                                                                            "</tr>"+
                                                                            "<tr class='id_"+id_renglon+"' style='background-color:#B4CBE6;height:15px;font-weight:bold;display:none;' title='click para ver detalle'>"+
                                                                                "<td></td>"+
                                                                                  "<td align='center' class='cuadricula' style='background-color:#B4CBE6;'>Id insumo</td>"+
                                                                                "<td align='center' class='cuadricula'  style='background-color:#B4CBE6;'>Descripcion</td>"+
                                                                                "<td align='center' class='cuadricula' style='background-color:#B4CBE6;'>Unidad de Medida</td>"+
                                                                                "<td align='center' class='cuadricula' style='background-color:#B4CBE6;'>Medida Especial</td>"+
                                                                                "<td align='center' class='cuadricula' style='background-color:#B4CBE6;'>Cantidad</td>"+
                                                                                "<td align='center' class='cuadricula' style='background-color:#B4CBE6;'>Comprado</td>"+
                                                                              
                                                                            "</tr>";   

                                                            }

                                                        

                                                    }else{
                                                       
                                                             if(arreglo[i].Estatus=='Autorizada'){
                                                                renglones+="<tr id='id_"+id_renglon+"' class='cuadricula' border='5px' title='click para ver detalle'>"+
                                                                              
                                                                                "<td align='right' class='cuadricula Requisicion' id='id_td_"+id_renglon+"'>"+arreglo[i].No_Requisicion+"</td>"+
                                                                                "<td align='center' class='cuadricula Requisicion' id='id_td_"+id_renglon+"'>"+fecha_format+"</td>"+
                                                                                "<td align='center' class='cuadricula Requisicion' id='id_td_"+id_renglon+"'>"+arreglo[i].solicitante+"</td>"+
                                                                                "<td align='center' class='cuadricula Requisicion' id='id_td_"+id_renglon+"'>"+arreglo[i].tipo_requisicion+"</td>"+
                                                                                "<td align='center' class='cuadricula Requisicion' id='id_td_"+id_renglon+"' style='width:125px'>"+arreglo[i].Estatus+"</td>"+
                                                                                "<td align='center' class='cuadricula Requisicion' id='id_td_"+id_renglon+"' style='width:125px'>"+arreglo[i].de_Comentarios+"</td>"+
                                                                                "<td align='center' class='cuadricula' width='1%' id='Acciones_"+id_renglon+"' style='cursor:pointer;height:25px;font-weight:bold;'>"+
                                                                                    "<a href='#'> <img src='../images/edit_gris.png' id='Editar_"+id_renglon+"' class='editar' alt='' title='Editar Requisicion'></a> "+
                                                                                    "<a href='#'> <img src='../images/delete_bak.png' id='cancelar_"+id_renglon+"'class='cancelar' alt='' title='Cancelar Requisicion'></a> "+
                                                                                "</td>"+
                                                                              
                                                                            "</tr>"+
                                                                            "<tr class='id_"+id_renglon+"' style='background-color:#B4CBE6;height:15px;font-weight:bold;display:none;' title='click para ver detalle'>"+
                                                                                "<td></td>"+
                                                                                 "<td align='center' class='cuadricula' style='background-color:#B4CBE6;'>Id insumo</td>"+
                                                                                "<td align='center' class='cuadricula'  style='background-color:#B4CBE6;'>Descripcion</td>"+
                                                                                "<td align='center' class='cuadricula' style='background-color:#B4CBE6;'>Unidad de Medida</td>"+
                                                                                "<td align='center' class='cuadricula' style='background-color:#B4CBE6;'>Medida Especial</td>"+
                                                                                "<td align='center' class='cuadricula' style='background-color:#B4CBE6;'>Cantidad</td>"+
                                                                                "<td align='center' class='cuadricula' style='background-color:#B4CBE6;'>Comprado</td>"+
                                                                              
                                                                            "</tr>"; 

                                                            }else if(arreglo[i].Estatus=='Cancelada'){
                                                                renglones+="<tr id='id_"+id_renglon+"' class='cuadricula' border='5px' title='click para ver detalle'>"+
                                                                                
                                                                                "<td align='right' class='cuadricula Requisicion' id='id_td_"+id_renglon+"'>"+arreglo[i].No_Requisicion+"</td>"+
                                                                                "<td align='center' class='cuadricula Requisicion' id='id_td_"+id_renglon+"'>"+fecha_format+"</td>"+
                                                                                "<td align='center' class='cuadricula Requisicion' id='id_td_"+id_renglon+"'>"+arreglo[i].solicitante+"</td>"+
                                                                                "<td align='center' class='cuadricula Requisicion' id='id_td_"+id_renglon+"'>"+arreglo[i].tipo_requisicion+"</td>"+
                                                                                "<td align='center' class='cuadricula Requisicion' id='id_td_"+id_renglon+"' style='width:125px'>"+arreglo[i].Estatus+"</td>"+
                                                                                "<td align='center' class='cuadricula Requisicion' id='id_td_"+id_renglon+"' style='width:125px'>"+arreglo[i].de_Comentarios+"</td>"+
                                                                                "<td align='center' class='cuadricula' width='1%' id='Acciones_"+id_renglon+"' style='cursor:pointer;height:25px;font-weight:bold;'>"+
                                                                                    "<a href='#'> <img src='../images/edit_gris.png' id='Editar_"+id_renglon+"' class='editar' alt='' title='Editar Requisicion'></a>"+
                                                                                    "<a href='#'> <img src='../images/delete_gris.png' id='cancelar_"+id_renglon+"' class='cancelar' alt='' title='Cancelar Requisicion'></a> </td>"+
                                                                                   
                                                                                "</tr>"+
                                                                                "<tr class='id_"+id_renglon+"' style='background-color:#B4CBE6;height:15px;font-weight:bold;display:none;' title='click para ver detalle'>"+
                                                                                    "<td></td>"+
                                                                                     "<td align='center' class='cuadricula' style='background-color:#B4CBE6;'>Id insumo</td>"+
                                                                                "<td align='center' class='cuadricula'  style='background-color:#B4CBE6;'>Descripcion</td>"+
                                                                                "<td align='center' class='cuadricula' style='background-color:#B4CBE6;'>Unidad de Medida</td>"+
                                                                                "<td align='center' class='cuadricula' style='background-color:#B4CBE6;'>Medida Especial</td>"+
                                                                                "<td align='center' class='cuadricula' style='background-color:#B4CBE6;'>Cantidad</td>"+
                                                                                "<td align='center' class='cuadricula' style='background-color:#B4CBE6;'>Comprado</td>"+ 
                                                                                "</tr>";  
                                                                                

                                                            }
                                                            else if(arreglo[i].Estatus=='Comprado'){
                                                                renglones+="<tr id='id_"+id_renglon+"' class='cuadricula' border='5px' title='click para ver detalle'>"+
                                                                               
                                                                                "<td align='right' class='cuadricula Requisicion' id='id_td_"+id_renglon+"'>"+arreglo[i].No_Requisicion+"</td>"+
                                                                                "<td align='center' class='cuadricula Requisicion' id='id_td_"+id_renglon+"'>"+fecha_format+"</td>"+
                                                                                "<td align='center' class='cuadricula Requisicion' id='id_td_"+id_renglon+"'>"+arreglo[i].solicitante+"</td>"+
                                                                                "<td align='center' class='cuadricula Requisicion' id='id_td_"+id_renglon+"'>"+arreglo[i].tipo_requisicion+"</td>"+
                                                                                "<td align='center' class='cuadricula Requisicion' id='id_td_"+id_renglon+"' style='width:125px'>"+arreglo[i].Estatus+"</td>"+
                                                                                "<td align='center' class='cuadricula Requisicion' id='id_td_"+id_renglon+"' style='width:125px'>"+arreglo[i].de_Comentarios+"</td>"+
                                                                                "<td align='center' class='cuadricula' width='1%' id='Acciones_"+id_renglon+"' style='cursor:pointer;height:25px;font-weight:bold;'> "+
                                                                                    "<a href='#'> <img src='../images/edit_gris.png' id='Editar_"+id_renglon+"' class='editar' alt='' title='Editar Requisicion'></a>"+
                                                                                    "<a href='#'> <img src='../images/delete_gris.png' id='cancelar_"+id_renglon+"' class='cancelar' alt='' title='Cancelar Requisicion'></a> "+
                                                                                "</td>"+
                                                                              
                                                                            "</tr>"+
                                                                            "<tr class='id_"+id_renglon+"' style='background-color:#B4CBE6;height:15px;font-weight:bold;display:none;' title='click para ver detalle'>"+
                                                                                "<td></td>"+
                                                                                 "<td align='center' class='cuadricula' style='background-color:#B4CBE6;'>Id insumo</td>"+
                                                                                "<td align='center' class='cuadricula'  style='background-color:#B4CBE6;'>Descripcion</td>"+
                                                                                "<td align='center' class='cuadricula' style='background-color:#B4CBE6;'>Unidad de Medida</td>"+
                                                                                "<td align='center' class='cuadricula' style='background-color:#B4CBE6;'>Medida Especial</td>"+
                                                                                "<td align='center' class='cuadricula' style='background-color:#B4CBE6;'>Cantidad</td>"+
                                                                                "<td align='center' class='cuadricula' style='background-color:#B4CBE6;'>Comprado</td>"+
                                                                               
                                                                            "</tr>";   


                                                            }else{
                                                                renglones+="<tr id='id_"+id_renglon+"' class='cuadricula' border='5px' title='click para ver detalle'>"+
                                                                             
                                                                                "<td align='right' class='cuadricula Requisicion' id='id_td_"+id_renglon+"'>"+arreglo[i].No_Requisicion+"</td>"+
                                                                                "<td align='center' class='cuadricula Requisicion' id='id_td_"+id_renglon+"'>"+fecha_format+"</td>"+
                                                                                "<td align='center' class='cuadricula Requisicion' id='id_td_"+id_renglon+"'>"+arreglo[i].solicitante+"</td>"+
                                                                                "<td align='center' class='cuadricula Requisicion' id='id_td_"+id_renglon+"'>"+arreglo[i].tipo_requisicion+"</td>"+
                                                                                "<td align='center' class='cuadricula Requisicion' id='id_td_"+id_renglon+"' style='width:125px'>"+arreglo[i].Estatus+"</td>"+
                                                                                "<td align='center' class='cuadricula Requisicion' id='id_td_"+id_renglon+"' style='width:125px'>"+arreglo[i].de_Comentarios+"</td>"+
                                                                                "<td align='center' class='cuadricula' width='1%' id='Acciones_"+id_renglon+"' style='cursor:pointer;height:25px;font-weight:bold;'> "+
                                                                                    "<a href='#'> <img src='../images/edit.png' id='Editar_"+id_renglon+"' class='editar' alt='' title='Editar Requisicion'></a> "+
                                                                                    "<a href='#'> <img src='../images/delete_bak.png' id='cancelar_"+id_renglon+"' class='cancelar' alt='' title='Cancelar Requisicion'></a>"+
                                                                                "</td>"+
                                                                             
                                                                            "</tr>"+
                                                                            "<tr class='id_"+id_renglon+"' style='background-color:#B4CBE6;height:15px;font-weight:bold;display:none;' title='click para ver detalle'>"+
                                                                                "<td></td>"+
                                                                                "<td align='center' class='cuadricula' style='background-color:#B4CBE6;'>Id insumo</td>"+
                                                                                "<td align='center' class='cuadricula'  style='background-color:#B4CBE6;'>Descripcion</td>"+
                                                                                "<td align='center' class='cuadricula' style='background-color:#B4CBE6;'>Unidad de Medida</td>"+
                                                                                "<td align='center' class='cuadricula' style='background-color:#B4CBE6;'>Medida Especial</td>"+
                                                                                "<td align='center' class='cuadricula' style='background-color:#B4CBE6;'>Cantidad</td>"+
                                                                                "<td align='center' class='cuadricula' style='background-color:#B4CBE6;'>Comprado</td>"+
                                                                                
                                                                            "</tr>";   

                                                            }
                                                    }
                                                            
                                                     if(arreglo[i].insumos.length==0){
                                                      renglones+="<tr class='id_"+id_renglon+"' style='display:none;' title='click para ver detalle'><td align='center' class='cuadricula' colspan='7'>Esta Requisicion no tiene insumos asignados.</td></tr>";
                                                     }
                                                    
                                                    for(var j=0;j<arreglo[i].insumos.length;j++){
                                                        UME=arreglo[i].insumos[j].Medida_Especial.split('¦').join('"');
                                                        var de_insumo=arreglo[i].insumos[j].nombre.split('¦').join('"');
                                                        var UM=arreglo[i].insumos[j].UM.split('¦').join('"');
                                                        
                                                              renglones+="<tr class='id_"+id_renglon+"' style='display:none;' title='click para ver detalle'>"+
                                                                            "<td style='border-top:none;'></td>"+
                                                                            "<td align='center' class='cuadricula'>"+arreglo[i].insumos[j].id+"</td>"+
                                                                            "<td align='left' class='cuadricula' >"+de_insumo+"</td>"+
                                                                            "<td align='left' class='cuadricula'>"+UM+"</td>"+
                                                                            "<td align='left' class='cuadricula' >"+UME+"</td>"+
                                                                            "<td align='right' class='cuadricula'>"+formatea(arreglo[i].insumos[j].cantidad,4)+"</td>"+
                                                                            "<td align='right' class='cuadricula'>"+formatea(arreglo[i].insumos[j].comprado,4)+"</td>"+
                                                                         "</tr>";
                                                    }

                                               }

                                                var pages=Math.ceil(((arreglo[0].Total)/30));

                                                $('#tbl1').removeClass('mostrar');
                                                $('#tbl2').html(renglones);
                                                $('#paginacion').html('<tr>'+ 
                                                                        '<td colspan="7" valign="bottom">'+
                                                                            '<a href="#" id="Primera_'+id_Empresa+'_'+id_obra+'" page="1" total="'+pages+'" title="ir a la ultima pagina">'+
                                                                                '<img src="../images/first.png" border="0">'+
                                                                            '</a>'+
                                                                            '<a href="#" id="antes_'+id_Empresa+'_'+id_obra+'" page="'+page+'" total="'+pages+'" title="ir a la pagina anterior">'+
                                                                                '<img src="../images/anterior.png" border="0">'+
                                                                            '</a>'+
                                                                            '&nbsp;&nbsp;&nbsp;&nbsp;<label><b valign="top">Pagina '+page+' De '+pages+'</b></label>&nbsp;&nbsp;&nbsp;&nbsp;'+
                                                                            '<a href="#" id="siguiente_'+id_Empresa+'_'+id_obra+'" page="'+page+'" total="'+pages+'" title="ir a la siguiente pagina">'+
                                                                                '<img src="../images/siguiente.png" border="0">'+
                                                                            '</a>'+
                                                                            '<a href="#" id="Ultimo_'+id_Empresa+'_'+id_obra+'" page="'+pages+'" total="'+pages+'" title="ir a la ultima pagina">'+
                                                                                '<img src="../images/ultimo.png" border="0">'+
                                                                            '</a>'+
                                                                        '</td>'+
                                                                    '<tr>');
                                                $('#cargando').html('');
                            
                                          }else{
                                               $('#cargando').html('');
                                               $('#tbl1').removeClass('mostrar');
                                               $('#tbl2').html("<tr><td align='center' colspan='6' style='font-size:14px; font-weigth:bold;'>No existen Registros</td></tr>");
                                          }     
                                      

                                    },
                                    error:function(){
                                                alert('Ha ocurrido un error en el servidor');
                                                $('#cargando').html('');
                                          }
                                     });
                            
                            }else{
                                alert("La Fecha de inicio no debe ser mayor a la Fecha de Fin ");

                            }

                        }else{
                            alert('Es Necesario Ingresar Fecha Inicio y Fecha Fin');
                        }    
                    
                    }else{
                        var mensaje="";
                        if(id_Empresa==""){
                             mensaje+='\nEs Necesario Ingresar Una Empresa';
                        }
                        if(id_obra==""){
                             mensaje+='\nEs Necesario Ingresar Una Obra';
                        } 

                        alert(mensaje);

                    }
                    first=false;
                    last=false;
             } 
             var apertura= function (){ 
                var id_ren=$(this).attr('id').substring(6);
                var agrupado="id_"+id_ren;
               
                /*alert('Agrupador:'+agrupado);*/
                $('.'+agrupado).toggle(function(){

                    if( $('#img_'+agrupado).hasClass('icon-resize-full') ){
                        $('#img_'+agrupado).removeClass('icon-resize-full');
                        $('#img_'+agrupado).addClass('icon-resize-small');

                    }else{
                        $('#img_'+agrupado).removeClass('icon-resize-small');
                        $('#img_'+agrupado).addClass('icon-resize-full');

              }
        
                });
            }
            var eliminar=function(event){
                   event.preventDefault();
                   var id=$(this).attr('id').substring(9); 
                   var id_renglon="id_"+id;
                   var req=$('#'+id_renglon).find("td").eq(0).html();
                   var estatus=$('#'+id_renglon).find("td").eq(4).html();
                  
                      if(estatus!='Comprado'){   
                         
                         if(confirm('\u00bfDesea Eliminar la requisicion?')){
                                  $.ajax({
                                       url:"componentes/Requisiciones.cfc?method=EliminarRequisicion",
                                       method:"POST",
                                       data:{
                                                id_Empresa:id_Empresa_static,
                                                id_obra:id_obra_static,
                                                id_Requisicion:req
                                            },
                                       cache:false,
                                       success:function(response){
                                          var arreglo=JSON.parse(response);
                                      
                                          if(arreglo.OK){

                                              alert(arreglo.MENSAJE);
                                              $('#'+id_renglon).remove();
                                          
                                          }else{
                                             alert(arreglo.MENSAJE);
                                          }
                                         

                                       },
                                       error:function(){
                                              alert("Ha ocurrido un error al cancelar la Requisicion");    
                                       }


                                  });

                         }else{
                             
                        }
                    }else{
                       alert('Esta requisicion ya ha sido comprada');

                    }    
            }
            var editar=function(){

                var id=$(this).attr('id').substring(7);
                var id_renglon="id_"+id;
                var estatus=$('#'+id_renglon).find("td").eq(4).html();
                  if(estatus=='Captura'){
                        
                        var req=$('#'+id_renglon).find("td").eq(0).html();
                        location.href="solicitud_materiales_editar.cfm?sn_MenuResidente=<cfoutput>#sn_MenuResidente#</cfoutput>&id_Empresa="+id_Empresa_static+"&id_obra="+id_obra_static+"&id_Requisicion="+req;

                       /* location.href="solicitud_materiales.cfm?id_Empresa="+id_Empresa_static+"&isEditable="+true+"&id_obra="+id_obra_static+"&id_Requisicion="+req;*/

                  }else if(estatus=='Cancelada'){
                     alert('Esta requisicion ya ha sido rechazada');

                  }else{
                    alert('Esta requisicion ya ha sido enviada y autorizada');
                  }
              
            }
            var cancelar=function(){
                   var id=$(this).attr('id').substring(9); 
                   var id_renglon="id_"+id;
                   var req=$('#'+id_renglon).find("td").eq(0).html();
                   var estatus=$('#'+id_renglon).find("td").eq(4).html();
                         
                      if(estatus=='Autorizada' || estatus=='Captura' ){   
                         if(confirm('\u00bfDesea Cancelar la requisicion?')){
                                  $.ajax({
                                       url:"componentes/Requisiciones.cfc?method=CancelarRequisicion",
                                       method:"POST",
                                       data:{
                                                id_Empresa:id_Empresa_static,
                                                id_obra:id_obra_static,
                                                id_Requisicion:req
                                            },
                                       cache:false,
                                       success:function(response){
                                          var arreglo=JSON.parse(response);
                                      
                                          if(arreglo.OK){
                                              alert('La Requisicion ha sido cancelada');
                                              $('#'+id_renglon).find("td").eq(4).html('Cancelada');
                                              $('#Editar_'+id).attr('src','../images/edit_gris.png');
                                              $('#cancelar_'+id).attr('src','../images/delete_gris.png');
                                          }
                                         

                                       },
                                       error:function(){
                                              alert("Ha ocurrido un error al cancelar la Requisicion");    
                                       }


                                  });

                         }else{
                             
                        }
                    }else{
                       alert('Esta requisicion no puede ser cancelada!');


                    }    
            }
            var antes=function(event){
                event.preventDefault();
                var id_Empresa= $('#Filtro_id_Empresa').val();
                var id_obra=$('#Filtro_id_Obra').val();    
                var page=$(this).attr('page');


                if(page==1){
                    alert('No se puede retorceder esta en la primer pagina!')

                }else{
                    $(this).attr('page',parseInt(page)-1);
                    $('#siguiente_'+id_Empresa+'_'+id_obra).attr('page',parseInt(page)-1);
                    
                    $('#boton').click();
                }
                
               
                
            }
            var siguiente=function(event){
                event.preventDefault();
         
                var id_Empresa= $('#Filtro_id_Empresa').val();
                var id_obra=$('#Filtro_id_Obra').val();    
                var page=$(this).attr('page');
                var total=$(this).attr('total');
                if(page==total){
                    alert('No se puede adelantar esta en la ultima pagina!')

                }else{
                     $(this).attr('page',parseInt(page)+1);
                    $('#antes_'+id_Empresa+'_'+id_obra).attr('page',parseInt(page)+1);
                   
                    $('#boton').click();
                }
               
               
                
            }
            var ultima=function(event){
                event.preventDefault();
                last=true;
                 $('#boton').click();
                
             }
            var primera=function(event){
                event.preventDefault();
                first=true;
                 $('#boton').click();
                
             }

            var solicitantes = function(){

                var id_Empresa= $('#Filtro_id_Empresa').val();
                var id_obra=$('#Filtro_id_Obra').val();

                if( !id_Empresa || !id_obra){
                    $('#id_Solicitante').append('<option value="">SELECCIONE...</option>');
                    return false;
                }
                $('#id_Solicitante').html('');
                $.ajax({
                    url:"componentes/Requisiciones.cfc",
                    method:"GET",
                    dataType:'json',
                    data:{
                        method:'RsEmpleadosSolicitantes',
                        id_Empresa:id_Empresa,
                        id_obra:id_obra
                    },
                    cache:false,
                    success:function(response){
                        
                        if( response.SUCCESS && response.RS.DATA.length > 0 ){
                            $('#id_Solicitante').append('<option value="">SELECCIONE...</option>');
                            for( var x = 0; x < response.RS.DATA.length; x ++ ){
                                
                              
                                    $('#id_Solicitante').append('<option value="'+response.RS.DATA[x][0]+'">'+response.RS.DATA[x][1]+'</option>');
                                
                            }
                        }
                    }
                });

            }
             var impresion=function(){
                console.log('entro');
                var id_Empresa= $('#Filtro_id_Empresa').val();
                var id_obra=$('#Filtro_id_Obra').val();  
                var id_Estatus=$('#id_Estatus').val();
                var id_Solicitante=$('#id_Solicitante').val();  
                var id_Agrupador="";
                var fh_inicio=$('#fh_inicio').val();
                var fh_fin=$('#fh_fin').val();
                var puesto=<cfoutput>#session.id_puesto#</cfoutput>;
                if(puesto==209 || puesto==111 || puesto == 104){
                     var id_Empleado='';
                }else{
                      var id_Empleado=<cfoutput>#session.id_Empleado#</cfoutput>;  
                }
                if(id_Empresa==''){
                    alert('Es necesario ingresar una Empresa');
                    return;
                }
                if(id_obra==''){
                    alert('Es necesario ingresar una Obra');
                    return;
                }
                window.open('solicitud_materiales_listado_excel.cfm?id_Empresa='+id_Empresa +'&id_Obra='+id_obra+'&id_Estatus='+id_Estatus+'&id_Agrupador='+id_Agrupador+'&fh_inicio='+fh_inicio+'&fh_fin='+fh_fin+'&id_Empleado='+id_Empleado+'&id_Solicitante='+id_Solicitante,'_Self');

            }

            $('#imprimir').on('click',impresion);

            $('#boton').on('click',Listado);
            $('#tbl2').on('click','.Requisicion',apertura);
            $('#tbl2').on('click','.cancelar',cancelar);
            $('#tbl2').on('click','.editar',editar);
            $('#tbl2').on('click','.Eliminar',eliminar);
            
            $('#tbl1').on('click','[id^="antes_"]',antes);
            $('#tbl1').on('click','[id^="siguiente_"]',siguiente);

            $('#tbl1').on('click','[id^="Primera_"]',primera);
            $('#tbl1').on('click','[id^="Ultimo_"]',ultima);
            
            $('#Filtro_id_Empresa').on('change',solicitantes);
            $('#Filtro_id_Obra').on('blur',solicitantes);

                Calendar.setup({
                    inputField     :    "fh_inicio",      // id del campo de texto
                    ifFormat       :    "%d/%m/%Y",       // formato de la fecha, cuando se escriba en el campo de texto
                    button         :    "lanzador"   // el id del botón que lanzará el calendario
                });
                
                    Calendar.setup({
                    inputField     :    "fh_fin",      // id del campo de texto
                    ifFormat       :    "%d/%m/%Y",       // formato de la fecha, cuando se escriba en el campo de texto
                    button         :    "lanzador2"   // el id del botón que lanzará el calendario
                });

            }

            $(document).on('ready',inicio);

        </script>
        <style type="text/css">
            .mostrar{
                display:none;
            }
            .Requisicion{
               cursor:pointer;
               background-color:#79A8CB;
               height:25px;
               font-weight:bold;

            }

        </style>
        

                           
    </head>

    <body>
    	<cfif sn_MenuResidente EQ 0>
        	<cfinclude template="menu.cfm">&nbsp;
		<cfelse>
			<cfinclude template="../residente/menu.cfm">&nbsp;
		</cfif>
        <cfform name="form1" method="post" action="#CurrentPage#" enctype="multipart/form-data" onsubmit="enviar(event);">
         
          <cfinput type="hidden" name="sn_Enviar" id="sn_Enviar" value="N">
          <table style="width:900px" align="center">
            <tr class="titulo">
                <td align="left">LISTADO SOLICITUDES DE MATERIALES</td>
            </tr>
          </table>
                
            <table width="900" cellpadding="0" cellspacing="0" align="center" >
                 <tr>
                    <td class="x-box-tl" width="1px"></td>
                    <td class="x-box-tc"></td>
                    <td class="x-box-tr" width="1px"></td>
                </tr>
                <tr>
                    <td class="x-box-ml"></td>
                    <td class="x-box-mc" width="99%"> 
                        <table width="900px" align="left" border="0">
                            
                            <tr></br></tr>
                            <tr>
                                <td align="right" valign="middle" rowspan="3">
                                    <button type="button" class="boton_menu_superior" onClick="javascript:location='solicitud_materiales_agregar.cfm?sn_MenuResidente=<cfoutput>#sn_MenuResidente#</cfoutput>'"><img src="../images/nuevo.png" alt="Clic Crear una Nueva Solicitud" width="24" style="cursor:pointer;"></button>
                                </td>   
                                <td align="right">Empresa:</td>
                                <td align="left">
                                    <cfselect name="Filtro_id_Empresa" id="Filtro_id_Empresa" style="width: 220px">
                                        <option value="">SELECCIONE UNA EMPRESA</option>
                                        
                                        <cfoutput query="RsEmpresa.Rs">
                                            <option value="#RsEmpresa.Rs.id_Empresa#"
                                                <cfif RsEmpresa.Rs.id_Empresa EQ Filtro_id_Empresa>selected="selected"</cfif>>#RsEmpresa.Rs.nb_Empresa#
                                            </option>
                                        </cfoutput>
                                    </cfselect>
                                </td>  

                                <td align="right">Obra:</td>
                                <td align="left">                      
                                     <cfinput  type="text" class="contenido_numerico" name="Filtro_id_Obra" id="Filtro_id_Obra" validate="integer" size="10" value="">
                                </td>
                                <td>        
                                     <cfinput type="text" bind="cfc:#Application.componentes#.obras.getObraPorID(1,{Filtro_id_Obra@blur})" onFocus="cambiarFocus($(this).before());" bindonload="yes" name="de_Obra" class="contenido_readOnly" style="width:300px" message="La Clave de Obra no es válida.">
                                </td>
                                <td width="35px">
                                    <button type="button" id="btnBuscarObra" onClick="javascript:AbrirPopUpBusquedaChrome('Filtro_id_Obra', 'pop_obras_busqueda.cfm', 'id_Obra=1', '700px', '600px')" class="boton_popup" value=""><img src="../images/buscar.png" width="24"></button>
                                </td>
                        

                            </tr>  
                            <tr>
                               
                            </tr>
           
                            <tr>
                                 <td align="right">Fecha Inicio:</td>
                                 <td align="left">
                                    <cfinput type="text" name="fh_inicio" id="fh_inicio"  value=""  size="27" mask="99/99/9999" readonly="true" validate="eurodate" class="contenido"  required="yes" message="La Fecha Inicio es requerida">
                                  <input type="button" id="lanzador" value="..." / class="boton">
                                </td>
                                <td align="right">Estatus:</td>
                                <td colspan="2">
                                    <select name="id_Estatus" id="id_Estatus" style="width:93%">
                                        <option value="">SELECCIONE..</option>
                                        <option value="203">AUTORIZADA</option>
                                        <option value="103">COMPRADA</option>
                                        <option value="104">COMPRADA PARCIALMENTE</option>
                                        <option value="202">CANCELADA</option>
                                        <option value="201">CAPTURA</option>
                                    </select>
                                </td>
                            
                            </tr>
                            <tr>
                                <td></td>
                                <td align="right">Fecha Fin:</td>
                                <td align="left">
                                    <cfinput type="text" name="fh_fin" id="fh_fin"  value=""  size="27" mask="99/99/9999" readonly="true" validate="eurodate" class="contenido"  required="yes" message="La Fecha Fin es requerida" >
                                    <input type="button" id="lanzador2" value="..." / class="boton">                
                                </td>
                                <td align="right">Solicitante:</td>
                                <td colspan="2">
                                    <select name="id_Solicitante" id="id_Solicitante" style="width:93%">
                                        <option value="">SELECCIONE..</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
            
                            <tr>
                                <td colspan="7" align="center">
                                    <button type="button" class="boton_imagen" id="boton"><img src="../images/filtro_azul.png" alt="Clic para buscar insumos"></button>
                                     <button  type="button" class="boton_menu_superior" id="imprimir">
                                        <img src="../images/excel.png" alt="Enviar a Excel" title="clic para generar excel" width="24" border="0">
                                    </button>
                                </td>           
                            </tr>   
                        </table>
                    </td>
                    <td class="x-box-mr"></td>
                </tr>
                <tr>
                    <td class="x-box-bl" width="1px"></td>
                    <td class="x-box-bc"></td>
                    <td class="x-box-br" width="1px"></td>
                </tr>
            </table>
            <br>
            <table   cellpadding="0" cellspacing="0" width="85%" class="Tabla_Azul mostrar" align="center" id="tbl1" >
                <thead class="encabezado">
                   
                   
                  
                    <td align="center"><b>No. Requisicion</b></td>
                    <td align="center"><b>Fecha de Requisicion</b></td>
                    <td align="center"><b>Solicitante</b></td>
                    <td align="center"><b>Tipo de Requisicion</b></td>
                    <td align="center" style='width:125px'><b>Estatus</b></td>
                    <td align="center" style='width:125px'><b>Comentarios</b></td>
                    <td align="center"><b>Acciones</b></td>
                  
                </thead>
                <tbody id='tbl2' border="2px">


                </tbody>
                <tfoot id="paginacion">
                    
                </tfoot>
                


                    
               
            </table>
            
        
            <center>
                    <div id="cargando"></div>
            </center>
            <cfinput type="hidden" name="sn_MenuResidente" id="sn_MenuResidente" value="#sn_MenuResidente#">
        </cfform>
        <br>

    </body>
</html>
