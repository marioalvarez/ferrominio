<cfset CurrentPage=GetFileFromPath(GetTemplatePath())>
<cfparam name="sn_Enviar" default="N">

<cfif sn_Enviar EQ 'S'>
    <cfinvoke component="#Session.componentes#.Requisiciones" method="RequsicionesExcel_Enviar"  returnvariable="RsOperacion"
    id_Empresa="#id_Empresa#"
    id_Obra="#id_Obra#"
    Json_Details="#JSON_Detalles#">
    <!---Preguntar por errores --->
    <cfif NOT (RsOperacion.SUCCESS)>
        <cf_error_manejador  tipoError="" mensajeError="Error en la consulta de datos.">
    </cfif>
    <cf_location_msg  url="requisiciones_excel.cfm" mensaje="La operaci&oacute;n se realiz&oacute; correctamente.">
</cfif>
<cfif isDefined('form.ar_Upload')>
    <!--- Cargamos el excel--->
    <cfinvoke component="#Session.componentes#.Requisiciones" method="Upload_Excel" returnvariable="RSExcel"
        fileField="ar_Upload">
    <!---Invokes --->
    <cfinvoke component="#Application.componentes#.empresas" method="RSMostrarTodosEmpresas" id_Empresa="" returnvariable="RsEmpresas">
    <!---Preguntar por errores --->
    <cfif NOT (RsEmpresas.listado)>
        <cf_error_manejador  tipoError="" mensajeError="Error en la consulta de datos.">
    </cfif>
</cfif>

<html>
    <head>
        <title></title>
        <link rel="stylesheet" href="../css/style.css" type="text/css">
        <link rel="stylesheet" type="text/css" href="../css/box.css">
        <script language="javascript" src="../js/jquery.js"></script> 
        <script language="javascript" src="../js/funciones.js"></script>
    </head>
    <body>
        <cfinclude template="menu.cfm">&nbsp;
        <cfif NOT RSExcel.Success>
            <table width="600px"  border="0" class="tabla_grid" cellpadding="0" cellspacing="0">
                <tr class="tabla_grid_encabezado">
                    <td width="3px"><img src="../images/esquina_izquierda.gif"></td>
                    <td width="100%" align="center" valign="baseline" colspan="2">Se encontraron los errores al cargar archivo de Excel</td>
                    <td width="3px"><img src="../images/esquina_derecha.gif"></td>
                </tr>
                <tr>
                    <td class="tabla_grid_orilla_izquierda"></td>
                    <td colspan="2">
                        <cfoutput><b>#RSExcel.MESSAGE#</b></cfoutput>
                    </td>
                    <td class="tabla_grid_orilla_derecha"></td>
                </tr>
                <tr>
                    <td class="tabla_grid_orilla_izquierda"></td>
                    <td colspan="2">
                        <cfoutput>#RSExcel.TipoError#</cfoutput>
                        <cfif isDefined('RSExcel.RSErrores')>
                            <cfoutput query="RSExcel.RSErrores">
                                <br> - '#id_Insumo#'
                            </cfoutput>
                        </cfif>
                    </td>
                    <td class="tabla_grid_orilla_derecha"></td>
                </tr>
                <tr>
                    <td class="tabla_grid_orilla_izquierda"></td>
                    <td colspan="2" align="center">
                        <button type="button" class="boton_imagen" title="Regresar" onClick="location='requisiciones_excel.cfm'"><img src="../images/regresar.png"></button>
                    </td>
                    <td class="tabla_grid_orilla_derecha"></td>
                </tr>
                <tr class="tabla_grid_footer">
                    <td width="3px"><img src="../images/abajo_izquierda.gif" /></td>
                    <td colspan="2"></td>
                    <td width="3px"><img src="../images/abajo_derecha.gif" /></td>
                </tr>
            </table>
            <br /><br />
            <cfif isDefined("RSExcel.RSexcel_Original")>
                <table width="auto" border="0" cellpadding="0" cellspacing="0" class="fondo_listado" align="center">
                    <tr class="encabezado_abajo">
                        <td width="3px"><img src="../images/esquina_izquierda.gif" /></td>
                        <td colspan="6">Datos del documento</td>
                        <td width="3px"><img src="../images/esquina_derecha.gif"></td>
                    </tr>
                    <cfoutput query="RSExcel.RSexcel_Original">
                        <tr class="renglon_grid">
                            <td background="../images/orilla_izquierda.gif" style="background-repeat:repeat-y" width="3px"></td>
                            <td class="cuadricula">#CurrentRow#</td>
                            <td class="cuadricula" align="left">#Column1#</td>
                            <td class="cuadricula" align="right">#Column2#</td>
                            <td class="cuadricula" align="left">#Column3#</td>
                            <td class="cuadricula" align="right">#Column4#</td>
                            <td class="cuadricula" align="right">#Column5#</td>
                            <td background="../images/orilla_derecha.gif" style="background-repeat:repeat-y" width="3px"></td>
                        </tr>
                    </cfoutput>
                    <tr style="background-image:url(../images/abajo.gif)">
                        <td width="3px"><img src="../images/abajo_izquierda.gif"></td>
                        <td colspan="5"></td>
                        <td width="3px"><img src="../images/abajo_derecha.gif"></td>
                    </tr>
                </table>
            </cfif>
        <cfelse>      
            <cfform name="form1" onsubmit="enviar();">
            <table width="600px"  border="0" class="tabla_grid" cellpadding="0" cellspacing="0">
                <tr class="tabla_grid_encabezado">
                    <td width="3px"><img src="../images/esquina_izquierda.gif"></td>
                    <td width="100%" align="center" valign="baseline" colspan="2">Cargar Requisicion Mediante Excel</td>
                    <td width="3px"><img src="../images/esquina_derecha.gif"></td>
                </tr>
                <tr>
                    <td class="tabla_grid_orilla_izquierda"></td>
                    <td colspan="2">&nbsp;</td>
                    <td class="tabla_grid_orilla_derecha"></td>
                </tr>
                <tr>
                    <td class="tabla_grid_orilla_izquierda"></td>
                    <td align="right">
                        Empresa:&nbsp;&nbsp;
                    </td>
                    <td valign="middle">
                        <cfselect name="id_Empresa" id="id_Empresa" class="contenido" style="width:384px;" query="RsEmpresas.RS" display="nb_Empresa" value="id_Empresa" queryposition="below" required="true" message="La empresa es requerida.">
                            <option value="">SELECCIONE</option>
                        </cfselect>
                    </td>
                    <td class="tabla_grid_orilla_derecha"></td>
                </tr>
                <tr>
                    <td class="tabla_grid_orilla_izquierda"></td>
                    <td align="right">
                        Obra:&nbsp;&nbsp;
                    </td>
                    <td valign="middle">
                        <cfinput  type="text" class="contenido_numerico" name="id_Obra" id="id_Obra" style="width:80px;vertical-align: middle;" validate="integer" size="10" value="" >
                        <cfinput type="text" class="contenido_readOnly"  name="de_Obra" id="de_Obra" style="width:300px;vertical-align: middle;" bind="cfc:#Application.componentes#.obras.getObraPorID(1,{id_Obra@blur})" onFocus="cambiarFocus($(this).before());" bindonload="yes" required="true" message="La obra es requerida.">
                        <button type="button" id="btnBuscarObra" onClick="javascript:AbrirPopUpBusquedaChrome('id_Obra', 'pop_obras_busqueda.cfm', 'id_Obra=1', '700px', '600px')" class="boton_popup" value=""><img src="../images/buscar.png" style="width:18px;vertical-align: middle;"></button></td>
                    <td class="tabla_grid_orilla_derecha"></td>
                </tr>
                <tr>
                    <td class="tabla_grid_orilla_izquierda"></td>
                    <td colspan="2">&nbsp;</td>
                    <td class="tabla_grid_orilla_derecha"></td>
                </tr>
                <tr>
                    <td class="tabla_grid_orilla_izquierda"></td>
                    <td align="center" colspan="2">
                </tr>
                <tr class="tabla_grid_footer">
                    <td width="3px"><img src="../images/abajo_izquierda.gif" /></td>
                    <td colspan="2"></td>
                    <td width="3px"><img src="../images/abajo_derecha.gif" /></td>
                </tr>
            </table>
            <br /><br />
            <table width="90%" border="0" cellpadding="0" cellspacing="0" class="fondo_listado" align="center">
                <tr class="encabezado_abajo">
                    <td width="3px"><img src="../images/esquina_izquierda.gif" /></td>

                    <td align="center" nowrap="nowrap" style="width:175px;">Clave</td>
                    <td align="center" nowrap="nowrap" style="width:Auto;">Insumo</td>
                    <td align="center" nowrap="nowrap" style="width:120px;">U.M.</td>
                    <td align="center" nowrap="nowrap" style="width:120px;">Cantidad</td>
                    <td align="center" style="width:Auto;">Medida Especial</td>
                    <td align="center" nowrap="nowrap" style="width:125px;">Tramos</td>
                    <td align="center" nowrap="nowrap" style="width:120px;">L. Com.</td>
                    
                    <td width="3px"><img src="../images/esquina_derecha.gif"></td>
                </tr>
                <cfoutput query="RSExcel.RS">
                    <tr class="renglon_grid">
                        <td background="../images/orilla_izquierda.gif" style="background-repeat:repeat-y" width="3px"></td>
                        <td class="cuadricula" align="center">#id_Insumo#&nbsp;</td>
                        <td class="cuadricula" align="left">#de_Insumo#</td>
                        <td class="cuadricula" align="center">#de_UnidadMedida#</td>
                        <td class="cuadricula" align="right">#NumberFormat(nu_Cantidad,",_.__")#</td>
                        <td class="cuadricula" align="left">#de_MedidaEspecial#</td>
                        <td class="cuadricula" align="right">#NumberFormat(nu_Tramos,',_.__')#</td>
                        <td class="cuadricula" align="right">#NumberFormat(nu_LongitudCom,',_.__')#</td>
                        <td background="../images/orilla_derecha.gif" style="background-repeat:repeat-y" width="3px"></td>
                    </tr>
                </cfoutput>
                <tr style="background-image:url(../images/abajo.gif)">
                    <td width="3px"><img src="../images/abajo_izquierda.gif"></td>
                    <td colspan="7"></td>
                    <td width="3px"><img src="../images/abajo_derecha.gif"></td>
                </tr>
            </table>

            <br/><br/>
        
            <div id="enviando" align="center" style="display: none;">
                <center>
                    <img src="../images/cargando_procesando.gif"  border="0" id="imgCargandoDatos" title="Cargando" />
                </center>
            </div>
            <div id="controles">
                <center>
                    <button type="submit" class="boton_imagen" title="Guardar"><img src="../images/guardar.png"></button>
                    &nbsp;&nbsp;&nbsp;
                    <button type="button" class="boton_imagen" onClick="location='requisiciones_excel.cfm'" title="Cancelar"><img src="../images/cancelar.png"></button>
                </center>
            </div>
            <cfinput type="hidden" name="sn_Enviar"     value="S">
            <cfinput type="hidden" name="JSON_Detalles" value="#SerializeJSON(RSExcel.Detalles)#">
        </cfform>
        <script>
            function enviar()
            {
                $('#enviando').show();
                $('#controles').hide();
            }
        </script>
        </cfif>
    </body>
</html>

