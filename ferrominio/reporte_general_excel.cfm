<cfparam name="id_Empresa" default="">
<cfparam name="id_Obras" default="">
<cfparam name="id_Agrupador" default="">
<cfinvoke component="#session.componentes#.reportes" method="Reporte_General" returnvariable="RsListado" 
    id_Empresa="#id_Empresa#"
    id_Obras="#id_Obras#"
    id_Agrupador="#id_Agrupador#"
>
<cfif #RsListado.SUCCESS# EQ false>
    <cf_error_manejador mensajeError="#RsListado.MESSAGE#">                                                                         
</cfif> 
<cfquery name="RsObra" datasource="#session.cnx#">
	SELECT id_Obra,de_Obra FROM Obras WHERE id_Obra in (#id_Obras#) AND id_Empresa = #id_Empresa# 
</cfquery>

<cfsavecontent variable="contenido">
    <table cellpadding="0" cellspacing="0" border="1" style="width:auto;font-size:9px">
        <tr>
            <td colspan="19" align="center">REPORTE COMPARATIVO</td>
		</tr>
		<tr>
            <td colspan="19" align="center">OBRA: <cfoutput query="RsObra"> #RsObra.id_Obra# - #RsObra.de_Obra#</cfoutput></td>
        </tr>
        <tr>
            <td colspan="3" style="background-color:#666666;"></td>
            <td colspan="3" align="center" style="background-color:#666666;">PRESUPUESTO</td>
            <td colspan="2" align="center" style="background-color:#666666;">SOLICITADO</td>
            <td colspan="3" align="center" style="background-color:#666666;">COMPRADO</td>
            <td colspan="2" align="center" style="background-color:#666666;">EJECUTADO</td>
            <td colspan="2" align="center" style="background-color:#666666;">DISPONIBLE</td>
            <td colspan="2" align="center" style="background-color:#666666;">MO SUBCONTRATO</td>
            <td colspan="2" align="center" style="background-color:#666666;">MO NOMINA</td>
        </tr>
        <tr>
            <td align="center" style="background-color:#666666;">CLAVE</td>
            <td align="center" style="background-color:#666666;">DESCRIPCION</td>
            <td align="center" style="background-color:#666666;">UM</td>
            <td align="center" style="background-color:#666666;">CANTIDAD</td>
            <td align="center" style="background-color:#666666;">PRECIO</td>
            <td align="center" style="background-color:#666666;">IMPORTE</td>
            <td align="center" style="background-color:#666666;">CANTIDAD</td>
            <td align="center" style="background-color:#666666;">IMPORTE</td>
            <td align="center" style="background-color:#666666;">CANTIDAD</td>
            <td align="center" style="background-color:#666666;">PRECIO</td>
            <td align="center" style="background-color:#666666;">IMPORTE</td>
            <td align="center" style="background-color:#666666;">ENTRADA</td>
            <td align="center" style="background-color:#666666;">SALIDA</td>
            <td align="center" style="background-color:#666666;">CANTIDAD</td>
            <td align="center" style="background-color:#666666;">IMPORTE</td>
            <td align="center" style="background-color:#666666;">CANTIDAD</td>
            <td align="center" style="background-color:#666666;">IMPORTE</td>
            <td align="center" style="background-color:#666666;">CANTIDAD</td>
            <td align="center" style="background-color:#666666;">IMPORTE</td>
        </tr>
        <cfset id = 4>
        <cfoutput query="RsListado.RS" group="id_Agrupador">
            <cfquery dbtype="query"  name="RSTotalesAgrupador">
                SELECT 
                	id_Agrupador,
                    SUM(nu_CantidadExplosion) AS nu_CantidadAgrupador,                   
                    SUM(im_PrecioExplosion) AS im_PrecioExplosion, 
                    SUM(im_SubtotalExplosion) AS im_SubtotalAgrupador,
                    SUM(nu_AplicadoEjecutado_ot) AS nu_AplicadoEjecutado_ot_Agrupador,
                    SUM(im_AplicadoEjecutado_ot) AS im_AplicadoEjecutado_ot_Agrupador,
                    SUM(nu_AplicadoEjecutado_dst) AS nu_AplicadoEjecutado_dst_Agrupador,
                    SUM(im_AplicadoEjecutado_dst) AS im_AplicadoEjecutado_dst_Agrupador,
                    SUM(nu_CantidadCompra) AS nu_CantidadCompra_agrupador,
                    SUM(im_CantidadPrecioCompra) AS im_CantidadPrecioCompra_agrupador,
                    SUM(im_Disponible) AS im_DisponibleAgrupador,
                    sum(nu_Disponible) AS nu_DisponibleAgrupador,
                    SUM(nu_Cantidad) AS nu_SolicitadoAgrupador,
    
                    SUM(nu_Entrada) AS nu_EntradaAgrupador,
                    SUM(nu_Salida) AS nu_SalidaAgrupador,
    
                    AVG(im_PrecioExplosion) AS im_PrecioExplosion_Agrupador,
                    AVG(im_PrecioCompra) AS im_PrecioCompra_Agrupador
                FROM
                    RsListado.RS
                WHERE 
                    id_Agrupador = '#RsListado.RS.id_Agrupador#'
				GROUP BY
                	id_Agrupador
            </cfquery>
            <!--- <cfdump var="#RSTotalesAgrupador#"> --->
            <tr>
                <td style="background-color:##666666;">#id_Agrupador#</td>
                <td style="background-color:##666666;">#de_Agrupador#</td>
                <td align="center" style="background-color:##666666;">#de_UnidadMedidaAgrupador#</td>
                <td align="right" style="background-color:##666666;">#NumberFormat(RSTotalesAgrupador.nu_CantidadAgrupador,',_.00')#</td>


                <cfset im_PrecioExplosionAgrupador = 0>
                <cfif #RSTotalesAgrupador.nu_CantidadAgrupador# NEQ 0 AND #RSTotalesAgrupador.im_SubtotalAgrupador# NEQ 0>
                    <cfset im_PrecioExplosionAgrupador = RSTotalesAgrupador.im_SubtotalAgrupador / RSTotalesAgrupador.nu_CantidadAgrupador>
                </cfif>
                <cfset id = id +1>
    			<!--- =F#id#/D#id# --->
                <td align="right" style="background-color:##666666;">#NumberFormat(im_PrecioExplosionAgrupador,'$,_.00')#</td>
                <td align="right" style="background-color:##666666;">#NumberFormat(RSTotalesAgrupador.im_SubtotalAgrupador,'$,_.00')#</td>
                
                <td align="right" style="background-color:##666666;">#NumberFormat(RSTotalesAgrupador.nu_SolicitadoAgrupador,',_.00')#</td>
                <td align="right" style="background-color:##666666;"></td>
                
                <td align="right" style="background-color:##666666;">#NumberFormat(RSTotalesAgrupador.nu_CantidadCompra_agrupador,',_.00')#</td>
                
                <cfset im_PrecioOC = 0>
                <cfif RSTotalesAgrupador.nu_CantidadCompra_agrupador NEQ 0 AND RSTotalesAgrupador.im_CantidadPrecioCompra_agrupador NEQ 0>
                    <cfset im_PrecioOC = RSTotalesAgrupador.im_CantidadPrecioCompra_agrupador / RSTotalesAgrupador.nu_CantidadCompra_agrupador>
                </cfif>
                
                <td align="right" style="background-color:##666666;">#NumberFormat(im_PrecioOC,'$,_.00')#</td>
                <td align="right" style="background-color:##666666;">#NumberFormat(RSTotalesAgrupador.im_CantidadPrecioCompra_agrupador,'$,_.00')#</td>
                
                <td align="right" style="background-color:##666666;">#NumberFormat(RSTotalesAgrupador.nu_EntradaAgrupador,',_.00')#</td>
                <td align="right" style="background-color:##666666;">#NumberFormat(RSTotalesAgrupador.nu_SalidaAgrupador,',_.00')#</td>
                
                <td align="right" style="background-color:##666666;">#NumberFormat(RSTotalesAgrupador.nu_DisponibleAgrupador,',_.00')#</td>
                <td align="right" style="background-color:##666666;">#NumberFormat(RSTotalesAgrupador.im_DisponibleAgrupador,'$,_.00')#</td>
    
                <td align="right" style="background-color:##666666;">#NumberFormat(RSTotalesAgrupador.nu_AplicadoEjecutado_ot_Agrupador,',_.00')#</td>
                <td align="right" style="background-color:##666666;">#NumberFormat(RSTotalesAgrupador.im_AplicadoEjecutado_ot_Agrupador,'$,_.00')#</td>
    
                <td align="right" style="background-color:##666666;">#NumberFormat(RSTotalesAgrupador.nu_AplicadoEjecutado_dst_Agrupador,',_.00')#</td>
                <td align="right" style="background-color:##666666;">#NumberFormat(RSTotalesAgrupador.im_AplicadoEjecutado_dst_Agrupador,'$,_.00')#</td>
            </tr>
            
            <cfoutput>
                <cfif #id_Insumo# NEQ ''>
                    <tr>
                        <td>#id_Insumo#</td>
                        <td>#de_Insumo#</td>
                        <td align="center">#de_UnidadMedida#</td>
                        
                        <td align="right">#NumberFormat(nu_CantidadExplosion,',_.__')#</td>
                        <td align="right">#NumberFormat(im_PrecioExplosion,'$,_.00')#</td>
                        <td align="right">#NumberFormat(im_SubtotalExplosion,'$,_.00')#</td>
                        
                        <td align="right">#NumberFormat(nu_Cantidad,',_.00')#</td>
                        <td align="right">#NumberFormat(im_Precio,'$,_.00')#</td>
                        
                        <td align="right">#NumberFormat(nu_CantidadCompra,',_.00')#</td>
                        <td align="right">#NumberFormat(im_PrecioCompra,'$,_.00')#</td>
                        <td align="right">#NumberFormat(im_CantidadPrecioCompra,'$,_.00')#</td>
                        
                        <td align="right">#NumberFormat(nu_Entrada,',_.00')#</td>
                        <td align="right">#NumberFormat(nu_Salida,',_.00')#</td>
                        
                        <td align="right">#NumberFormat(nu_Disponible,',_.00')#</td>
                        <td align="right">#NumberFormat(im_Disponible,'$,_.00')#</td>
            
                        <td align="right">#NumberFormat(nu_AplicadoEjecutado_ot,',_.00')#</td>
                        <td align="right">#NumberFormat(im_AplicadoEjecutado_ot,'$,_.00')#</td>
            
                        <td align="right">#NumberFormat(nu_AplicadoEjecutado_dst,',_.00')#</td>
                        <td align="right">#NumberFormat(im_AplicadoEjecutado_dst,'$,_.00')#</td>
                    </tr>
                </cfif>
                <cfset id = id +1>
            </cfoutput>	
        </cfoutput>
    </table>
</cfsavecontent>
<cfoutput>
	#contenido#
</cfoutput>

<cfheader name="Content-Disposition" value="inline; filename=ReporteComparativo_#DateFormat(Now(),'DD_MM_YYYY')#.xls" >
<cfcontent variable="#ToBinary(ToBase64(contenido))#" type="application/msexcel" >
 