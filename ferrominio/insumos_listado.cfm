<cfparam name="Filtro_id_TipoInsumo" default="0">
<cfparam name="buscar" default="">
<cfparam name="clave" default="">
<cfparam name="sn_Mostrar" default="N">
<cfparam name="sn_Activo" default="">
<cfparam name="id_Agrupador" default="">
<cfparam name="id_TipoPieza" default="">
<cfparam name="Filtro_id_Agrupador" default="">

<cfif sn_Mostrar EQ 'S'>	
	<cfinvoke component="#session.componentes#.insumos" method="RSMostrarInsumosFiltrado" returnvariable="RSInsumos" 
    	id_insumo="#clave#" de_insumo="#buscar#" id_Agrupador="#Filtro_id_Agrupador#" id_TipoPieza="#id_TipoPieza#" sn_Activo="#sn_Activo#">
	
	
	<!---Paginado--->
	<cfparam name="PageNum_RSInsumos" default="1">
	<cfset CurrentPage=GetFileFromPath(GetTemplatePath())>
	<cfset MaxRows_RSInsumos=50>
	<cfset StartRow_RSInsumos=Min((PageNum_RSInsumos-1)*MaxRows_RSInsumos+1,Max(RSInsumos.rs.RecordCount,1))>
	<cfset EndRow_RSInsumos=Min(StartRow_RSInsumos+MaxRows_RSInsumos-1,RSInsumos.rs.RecordCount)>
	<cfset TotalPages_RSInsumos=Ceiling(RSInsumos.rs.RecordCount/MaxRows_RSInsumos)>
	<cfset QueryString_RSInsumos=Iif(CGI.QUERY_STRING NEQ "",DE("&"&XMLFormat(CGI.QUERY_STRING)),DE(""))>
	<cfset tempPos=ListContainsNoCase(QueryString_RSInsumos,"PageNum_RSInsumos=","&")>
	<cfif tempPos NEQ 0>
	  <cfset QueryString_RSInsumos=ListDeleteAt(QueryString_RSInsumos,tempPos,"&")>
	</cfif>
</cfif>

<cfinvoke component="#session.componentes#.insumos" method="obtenerAgrupadores" returnvariable="RS_MostrarAgrupadores">
<cfinvoke component="#session.componentes#.insumos" method="obtenerPiezas" returnvariable="RS_MostrarPiezas">

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<link rel="stylesheet" href="../css/style.css" type="text/css">
<link rel="stylesheet" type="text/css" href="../css/box.css">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Untitled Document</title>
<script language="javascript" src="../js/jquery.js"></script>
<script language="javascript" src="../js/funciones.js"></script>
<script language="javascript" src="../js/validar_selects.js"></script>
<script>
    function imprimir(opcion)
	{
		if(opcion == 1)
		{
			$('#cargando').fadeIn();
			$('#tbl_Principal').hide();
			form1.action = 'insumos_listado.cfm';
			form1.submit();
		}
		else
		{
			$('#tbl_Principal').hide();
			window.open('insumos_excel.cfm?clave='+$('#clave').val() +'&buscar='+$('#buscar').val()+'&id_Agrupador='+$('#Filtro_id_Agrupador').val()+'&sn_Activo='+$('#sn_Activo').val(),'_Self');
		}
	}

</script>

</head>
<body onload="form1.clave.focus();">
<cfinclude template="menu.cfm">&nbsp;
<table style="width:550px" align="center">
	<tr class="titulo">
		<td align="left">LISTADO DE INSUMOS</td>
	</tr>
</table>
<cfform name="form1" onsubmit="$('##cargando').fadeIn(); $('##tbl_Principal').hide();">
<table width="550" cellpadding="0" cellspacing="0" align="center">
	<tr><td class="x-box-tl" width="1px"></td><td class="x-box-tc"></td><td class="x-box-tr" width="1px"></td></tr>
	<tr><td class="x-box-ml"></td><td class="x-box-mc" width="99%">	
		<table width="550px" align="left" border="0">
            <tr>
            	<td width="">
					<button type="button" class="boton_menu_superior" onClick="javascript:location='insumos_agregar.cfm'"><img src="../images/nuevo.png" alt="Clic para agregar insumos" width="24" style="cursor:pointer;"></button>
				</td>
                <td align="right" nowrap="nowrap">Clave:</td>
                <td align="left">
	                <cfinput type="text" name="clave" class="contenido_filtro" value="#clave#" size="20" maxlength="20" validate="" message="El campo Clave admite solo números.">
                	Nombre:					
				    <cfinput type="text" name="buscar" class="contenido_filtro" value="#buscar#" maxlength="100">					
					<input type="hidden" name="sn_Mostrar" value="S">
				</td>
             
            </tr>
            <tr>
            	<td></td>
				<td nowrap align="right">Agrupador:</td>
                <td align="left">
					<cfselect name="Filtro_id_Agrupador" id="Filtro_id_Agrupador" class="contenido" style="width:350px" required="no" message="Seleccione un Agrupador" title="Seleccione un Agrupador">
                    	<option value="">SELECCIONE UN AGRUPADOR</option>
                    	<cfoutput query="RS_MostrarAgrupadores">
                    		<option value="#RS_MostrarAgrupadores.id_Agrupador#"<cfif #Filtro_id_Agrupador# EQ #RS_MostrarAgrupadores.id_Agrupador#>selected</cfif> >#de_Agrupador#</option>	


                    	</cfoutput>
                    </cfselect>                
                </td>
                
            </tr>
		<!--- 	<tr valign="baseline">
						<td></td>
						<td nowrap="nowrap" align="right">Tipo pieza:</td>
						<td>
							<cfselect name="id_TipoPieza" id="id_TipoPieza" class="contenido" style="width:350px" required="no" message="Seleccione un tipo de pieza" title="Seleccione un Agrupador">
	                    	<option value="">SELECCIONE UN TIPO DE PIEZA</option>
	                    	<cfoutput query="RS_MostrarPiezas">
	                    		<option value="#id_TipoPieza#">#de_TipoPieza#</option>	


                    		</cfoutput>
                    		</cfselect>  
						</td>
			</tr> --->	

           	<tr>

				<td></td>
				<td align="right">Activo:</td>
				<td align="left">
					<cfselect name="sn_Activo" id="sn_Activo" class="contenido" selected="#sn_Activo#">
						<option value="">TODOS</option>
                        <option value="0" <cfif #sn_Activo# EQ 0>selected="selected"</cfif>>INACTIVOS</option>
                        <option value="1" <cfif #sn_Activo# EQ 1>selected="selected"</cfif>>ACTIVOS</option>
					</cfselect>
                   
                </td>

           	</tr>
            <tr>
				 <td></td>
				 <td></td>
				 <td>
                 	<button type="button" class="boton_imagen" onclick="return imprimir(1);"><img src="../images/filtro_azul.png" alt="Clic para buscar insumos"></button>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                    <button  type="button" class="boton_menu_superior" onClick="imprimir(2);">
                        <img src="../images/excel.png" alt="Enviar a Excel" title="clic para generar excel" width="24" border="0">
                    </button>                   
				</td>			
            </tr>	
		</table>
		</td><td class="x-box-mr"></td></tr><tr><td class="x-box-bl" width="1px"></td><td class="x-box-bc"></td><td class="x-box-br" width="1px"></td>
	</tr>
</table>
<div id="cargando" style="display:none;">
    <center><img src="../images/cargando_procesando.gif"></center>
</div>

</cfform>
<br>
<cfif sn_Mostrar EQ 'S'>
<table border="0" cellpadding="0" cellspacing="0" width="80%" class="fondo_listado" align="center" id="tbl_Principal">
	<tr class="encabezado">
		<td width="3px"><img src="../images/esquina_izquierda.gif"></td>
		<td align="center">Clave</td>
		<td align="center" width="30%">Nombre</td>
		<td align="center">U.M.</td>
		<td align="center">Agrupador</td>
		<!--- <td align="center">Tipo pieza</td> --->
		<td align="center">Grupo</td>
		<td align="center">SubGrupo</td>
		<td align="center">Activo</td>
      <!---   <td align="center" width="11%">Enviar a Compras</td> --->
         <td align="center" width="11%">Acciones</td>
		
	    <td width="3px"><img src="../images/esquina_derecha.gif"></td>
	</tr>
	<cfoutput query="RSInsumos.rs" startRow="#StartRow_RSInsumos#" maxRows="#MaxRows_RSInsumos#">
		<tr onMouseOver="this.style.backgroundColor='##99CCCC'" onMouseOut="this.style.backgroundColor=''">
			<td background="../images/orilla_izquierda.gif" style="background-repeat:repeat-y" width="3px"></td>
			<td class="cuadricula" align="center">#RSInsumos.rs.id_Insumo#</td>
		  	<td class="cuadricula">#RSInsumos.rs.de_Insumo#</td>
		   
		    <td class="cuadricula" align="center">#RSInsumos.rs.de_UnidadMedida#</td>
		    <td class="cuadricula" align="center"><cfif #RSInsumos.rs.de_agrupador# EQ ''>
		    										N/A
		    									  <cfelse>			
		    										#RSInsumos.rs.de_Agrupador#			
		    									  </cfif>
			</td>
			<!--- <td class="cuadricula" align="center">#RSInsumos.rs.de_TipoPieza#</td> --->
			<td class="cuadricula" align="center">#RSInsumos.rs.de_GrupoInsumo#</td>
			<td class="cuadricula" align="center">#RSInsumos.rs.de_SubGrupoInsumo#</td>
			<td class="cuadricula" align="center">
				<cfswitch expression="#RSInsumos.rs.sn_Activo#">
					<cfcase value="1">SI</cfcase>
					<cfcase value="0">NO</cfcase>
					<cfdefaultcase>NO</cfdefaultcase>
				</cfswitch>
			</td>
           <!---  <td class="cuadricula" align="center">
				<cfswitch expression="#RSInsumos.rs.sn_EnviarACompras#">
					<cfcase value="1">Si</cfcase>
					<cfcase value="0">No</cfcase>
					<cfdefaultcase>No</cfdefaultcase>
				</cfswitch>
			</td> --->
            <cfset insumo = URLEncodedFormat(#RSInsumos.rs.id_Insumo#)>
            
			<td width="1%" class="cuadricula">
				<table width="100%">
					<tr>
						<td width="1%" align="center"><a href="insumos_editar.cfm?id_Insumo=#insumo#&Filtro_id_TipoInsumo=#Filtro_id_TipoInsumo#&sn_EnviarACompras=#RSInsumos.rs.sn_EnviarACompras#"><img src="../images/edit.png" alt="Clic para editar insumo" border="0"></a></td>
						<td width="1%">&nbsp;</td>
						<td width="1%" align="center"><a href="insumos_borrar.cfm?id_Insumo=#insumo#&Filtro_id_TipoInsumo=#Filtro_id_TipoInsumo#" onclick="return confirm('Esta seguro que desea eliminar este insumo?');"><img src="../images/delete.png" alt="Clic para eliminar insumo" border="0"></a></td>
					</tr>
				</table>
			</td>
			<td background="../images/orilla_derecha.gif" style="background-repeat:repeat-y" width="3px"></td>
		</tr>
	</cfoutput>
	<tr>
		<td background="../images/orilla_izquierda.gif" style="background-repeat:repeat-y" width="3px"></td>
		<td align="center" colspan="10">
			<table border="0" width="100%" cellpadding="0" cellspacing="0">
			  <cfoutput>
				<tr>
				  <td width="20%" align="center">
				  <cfif PageNum_RSInsumos GT 1>
					<a href="insumos_listado.cfm?Filtro_id_Agrupador=#Filtro_id_Agrupador#&sn_Activo=#sn_Activo#&Filtro_id_TipoInsumo=#Filtro_id_TipoInsumo#&sn_Mostrar=#sn_Mostrar#&buscar=#buscar#&PageNum_RSInsumos=1"><img src="../images/primero.png" border="0"></a>
				  </cfif>
				  </td>
				  <td width="20%" align="center">
				  <cfif PageNum_RSInsumos GT 1>
					<a href="insumos_listado.cfm?Filtro_id_Agrupador=#Filtro_id_Agrupador#&sn_Activo=#sn_Activo#&Filtro_id_TipoInsumo=#Filtro_id_TipoInsumo#&sn_Mostrar=#sn_Mostrar#&buscar=#buscar#&PageNum_RSInsumos=#Max(DecrementValue(PageNum_RSInsumos),1)#"><img src="../images/anterior.png" border="0"></a>
				  </cfif>
				  </td>
  				  <td width="20%" align="center">	
					Registro #StartRow_RSInsumos# al #EndRow_RSInsumos# de #RSInsumos.rs.RecordCount# 
				  </td>
				  <td width="20%" align="center">
				  <cfif PageNum_RSInsumos LT TotalPages_RSInsumos>
					<a href="insumos_listado.cfm?Filtro_id_Agrupador=#Filtro_id_Agrupador#&sn_Activo=#sn_Activo#&Filtro_id_TipoInsumo=#Filtro_id_TipoInsumo#&sn_Mostrar=#sn_Mostrar#&buscar=#buscar#&PageNum_RSInsumos=#Min(IncrementValue(PageNum_RSInsumos),TotalPages_RSInsumos)#"><img src="../images/siguiente.png" border="0"></a> <!--#QueryString_RSInsumos#--->
				  </cfif>
				  </td>
				  <td width="20%" align="center">
				  <cfif PageNum_RSInsumos LT TotalPages_RSInsumos>
					<a href="insumos_listado.cfm?Filtro_id_Agrupador=#Filtro_id_Agrupador#&sn_Activo=#sn_Activo#&Filtro_id_TipoInsumo=#Filtro_id_TipoInsumo#&sn_Mostrar=#sn_Mostrar#&buscar=#buscar#&PageNum_RSInsumos=#TotalPages_RSInsumos#"><img src="../images/ultimo.png" border="0"></a>
				  </cfif>
				  </td>
				</tr>
			  </cfoutput>
			</table>
		</td>
		<td background="../images/orilla_derecha.gif" style="background-repeat:repeat-y" width="3px"></td>
	</tr>
	<tr style="background-image:url(../images/abajo.gif)">
		<td width="3px"><img src="../images/abajo_izquierda.gif"></td>
		<td colspan="10"></td>
		<td width="3px"><img src="../images/abajo_derecha.gif"></td>
	</tr>
</table>
</cfif>
</body>
</html>