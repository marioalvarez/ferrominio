function obtenerFrentes( fila, opcion, idu_Frente, idu_Partida )
{
	document.getElementById( 'idu_Empresa_' + fila ).setAttribute( 'title', '' );
	$( '#idu_Empresa_' + fila ).css({ borderColor: '' });
	
	var idu_Empresa = $( '#idu_Empresa_' + fila ).val();
	
	
	if( idu_Empresa == '' )
	{
		if( opcion > 1 )
			alert( 'Es necesario que seleccione una empresa para el insumo' );
		document.getElementById( 'idu_Empresa_' + fila ).setAttribute( 'title', 'Es necesario seleccionar una empresa' );
		$( '#idu_Empresa_' + fila ).css({ borderColor: 'red' });
		
		document.getElementById( 'idu_Frente_' + fila ).options.length = 0;
		var obj = document.getElementById( 'idu_Frente_' + fila );
		var contador = 0;
				
		var opt = document.createElement( 'option' );
		opt.value = '';
		opt.text='SELECCIONE ...';
		obj.appendChild( opt );
		return;
	}
		
	var ruta = '../componentes/cuantificaciones.cfc?method=CuantificacionListadoFrentesResidenteAjax&idu_Empresa='+ idu_Empresa +'&idu_Area=' + $( '#idu_Area' ).val();
	$.getJSON
	(
		ruta,
		function( data )
		{
			document.getElementById( 'idu_Frente_' + fila ).options.length = 0;
			var obj = document.getElementById( 'idu_Frente_' + fila );
			var contador = 0;						
			var opt = document.createElement( 'option' );
									
			$( '#idu_Frente_' + fila ).append( '<option value="">SELECCIONE ...</option>' );
			
			$.each
			(
				data,
				function( i, j )
				{
					if( i == 'DATA' )
					{
						$.each
						(
							j,
							function( k, l )
							{
								if( l[ 0 ] == idu_Frente )
								{
									$( '#idu_Frente_' + fila ).append( '<option value="'+ l[ 0 ] +'" selected="selected">'+ l[ 0 ] +'</option>' );
									if( idu_Partida != null )
										obtenerPartidas( fila, opcion, idu_Partida );
								}
								else
									$( '#idu_Frente_' + fila ).append( '<option value="'+ l[ 0 ] +'">'+ l[ 0 ] +'</option>' );
								
							}
						);
					}
				}
			);
			
			
		}
	);
	
	
	
}

function obtenerPartidas( fila, opcion, idu_Partida )
{
	document.getElementById( 'idu_Frente_' + fila ).setAttribute( 'title', '' );
	$( '#idu_Frente_' + fila ).css({ borderColor: '' });
	
	var idu_Empresa = $( '#idu_Empresa_' + fila ).val();
	var idu_Frente = $( '#idu_Frente_' + fila ).val();
	
	if( idu_Frente == '' )
	{
		if( opcion ==1 )
			alert( 'Es necesario que seleccione una sub analisis para el insumo' );
		document.getElementById( 'idu_Frente_' + fila ).setAttribute( 'title', 'Es necesario seleccionar un sub analisis' );
		$( '#idu_Frente_' + fila ).css({ borderColor: 'red' });
		
		document.getElementById( 'idu_Partida_' + fila ).options.length = 0;
		var obj = document.getElementById( 'idu_Partida_' + fila );
		var contador = 0;
				
		var opt = document.createElement( 'option' );
		opt.value = '';
		opt.text='SELECCIONE ...';
		obj.appendChild( opt );
		$( '#idu_Partida_' + fila ).append( '<option value="">SELECCIONE ...</option>' );
		return;
	}
	
	var ruta = '../componentes/cuantificaciones.cfc?method=CuantificacionListadoPartidasResidenteAjax&idu_Empresa='+ idu_Empresa +'&idu_TipoCuantificacion=' + $( '#idu_Tipo' ).val() + '&idu_Frente=' + idu_Frente;
	$.getJSON
	(
		ruta,
		function( data )
		{
			document.getElementById( 'idu_Partida_' + fila ).options.length = 0;
			var obj = document.getElementById( 'idu_Partida_' + fila );
			var contador = 0;						
			var opt = document.createElement( 'option' );
									
			$( '#idu_Partida_' + fila ).append( '<option value="">SELECCIONE ...</option>' );
			
			$.each
			(
				data,
				function( i, j )
				{
					if( i == 'DATA' )
					{
						$.each
						(
							j,
							function( k, l )
							{
								if( l[ 0 ] == idu_Partida )
									$( '#idu_Partida_' + fila ).append( '<option value="'+ l[ 0 ] +'" selected="selected">'+ l[ 0 ] +' - '+ l[ 1 ] +'</option>' );
								else
									$( '#idu_Partida_' + fila ).append( '<option value="'+ l[ 0 ] +'">'+ l[ 0 ] +' - '+ l[ 1 ] +'</option>' );
								
							}
						);
					}
				}
			);
			
		}
	);
	
	/*$.ajax
	({
		url: 		ruta,
		type: 		'GET',
		dataType:	'json',
		success:	function( resultado )
					{			
						document.getElementById( 'idu_Partida_' + fila ).options.length = 0;
						var obj = document.getElementById( 'idu_Partida_' + fila );
						var contador = 1;
						
						var opt = document.createElement( 'option' );
						opt.value = '';
						opt.text='SELECCIONE ...';
						obj.add( opt );
						
						$( '#idu_Partida_' + fila ).append( '<option value="">SELECCIONE ...</option>' );
						
						/*$.each
						(
							resultado,
							function( i, j ) // Recorre los elementos del resultado
							{
								if( i == 'DATA' )
								{
									
									$.each
									(
										j,
										function( k, l )
										{
											var opt = document.createElement( 'option' );
											opt.value = l[ 0 ];
		                					opt.text= l[ 0 ] +' - '+ l[ 1 ];
											obj.appendChild( opt );
											
											if( obj[ contador ].value == idu_Partida )
											{
												
												obj.selectedIndex = contador;
											}
											contador ++;
										}
									);
									if( opcion == 2 )
										$( '#idu_Partida_' + fila ).prop('disabled', 'disabled');	
								}										
							}
						);								
					}
	});*/
	
}
            
function buscarInsumo( opcion, idu_Empresa, idu_Frente, idu_Partida, idu_Insumo, num_Cantidad,des_Medida, motivo  )
{
	
	var ruta = '../componentes/cuantificaciones.cfc?method=InsumoListadoAjax&id_Insumo=' + idu_Insumo;	
	$.ajax
	({	
		url: 	 ruta,					
		type: 	'GET',
		dataType:	'json',
		success:function( resultado )
				{
					//alert( resultado );
					$.each
					(
						resultado,
						
						function( i, j)
						{
							if( i == 'DATA' )
							{
								if( j.length < 1 )
									creaFila( opcion, idu_Empresa, idu_Frente, idu_Partida, idu_Insumo, '', '', num_Cantidad,des_Medida, motivo );
								switch( opcion )
								{
									case 1:
										
										if( $( '#sn_Error' ).val() == 'S' )
										{
											alert( 'No se pueden agregar insumos a la cuantificacion porque existen errores' );
											return;
										}
										creaFila( opcion, null, null, null, idu_Insumo, j[0][0], j[0][1], null, null, null );
										return;
										break;
										
									case 2:
										
										creaFila( opcion, idu_Empresa, idu_Frente, idu_Partida, idu_Insumo, j[0][0], j[0][1], num_Cantidad,des_Medida, motivo );
										return;
										break;
								}
							}
						}
					);
						
				}
	});
}

function creaFila( opcion, idu_Empresa, idu_Frente, idu_Partida, idu_Insumo, des_Insumo, des_Unidad, num_Cantidad, des_Medida, motivo )
{
	
	var tabla	= document.getElementById( 'tbInsumos' );
	var id 		= tabla.rows.length;
	
	var fila	= tabla.insertRow( id ); 	
	var celda0	= fila.insertCell(0);
	var celda1	= fila.insertCell(1);
	var celda2	= fila.insertCell(2);
	var celda3	= fila.insertCell(3);
	var celda4	= fila.insertCell(4);
	var celda5	= fila.insertCell(5);
	var celda6	= fila.insertCell(6);
	var celda7	= fila.insertCell(7);
	var celda8	= fila.insertCell(8);
	var celda9  = fila.insertCell(9);
	var celda10 = fila.insertCell(10);
	
	fila.id 	= 'fila_' 	+ id;				
	celda0.id	= '0_' 		+ id;
	celda1.id	= '1_' 		+ id;
	celda2.id	= '2_' 		+ id;
	celda3.id	= '3_' 		+ id;
	celda4.id	= '4_' 		+ id;
	celda5.id	= '5_' 		+ id;
	celda6.id	= '6_' 		+ id;
	celda7.id	= '7_' 		+ id;
	celda8.id	= '8_' 		+ id;
	celda9.id	= '9_' 		+ id;
	celda10.id	= '10_' 	+ id;
	
	celda0.className	= 'cuadricula';		
	celda1.className	= 'cuadricula';
	celda2.className	= 'cuadricula';
	celda3.className	= 'cuadricula';
	celda4.className	= 'cuadricula';
	celda5.className	= 'cuadricula';
	celda6.className	= 'cuadricula';
	celda7.className	= 'cuadricula';
	celda8.className	= 'cuadricula';
	celda9.className	= 'cuadricula';
	celda10.className	= 'cuadricula';
	
	$( '#' + fila.id ).attr("title", motivo);
	
	var cboEmpresa 	='<select id="idu_Empresa_'+ id +'" class="comboMultilinea cajaTexto" onchange="obtenerFrentes( '+ id +', '+ opcion +', '+ idu_Frente +', null )">';
	
	if( idu_Empresa == null )
	{
		cboEmpresa 	+='<option value="" selected="selected">SELECCIONE ...</option>';
		cboEmpresa 	+='<option value="1">COPPEL S.A. DE C.V.</option>';
		cboEmpresa 	+='<option value="3">CONSTRUCTORA</option>';
	}
	else
	{
		
		cboEmpresa 	+='<option value="">SELECCIONE ...</option>';
		if( idu_Empresa == 1  )
		{
			cboEmpresa 	+='<option value="1" selected="selected">COPPEL S.A. DE C.V.</option>';
			cboEmpresa 	+='<option value="3">CONSTRUCTORA</option>';
		}
		else
		{
			cboEmpresa 	+='<option value="1">COPPEL S.A. DE C.V.</option>';
			cboEmpresa 	+='<option value="3" selected="selected">CONSTRUCTORA</option>';
		}
	}
	cboEmpresa 	+='</select>';
	
	celda0.innerHTML 	= '<td background="../images/orilla_izquierda.gif" style="background-repeat:repeat-y" width="3px"></td>'; 
	celda1.innerHTML	= cboEmpresa;
	celda2.innerHTML	= '<select id="idu_Frente_'+ id +'" class="comboMultilinea cajaTexto" onchange="obtenerPartidas( '+ id +', '+ opcion +', '+ idu_Partida +' )"><option value="">SELECCIONE ...</option></select>';
	celda3.innerHTML	= '<select id="idu_Partida_'+ id +'" class="comboMultilinea cajaTexto" style="width: 100px;"><option value="">SELECCIONE ...</option></select>';
	celda4.innerHTML	= '<center><label id="idu_Insumo_'+ id +'">'+ idu_Insumo +'</label></center>';
	celda5.innerHTML	= '<label id="des_Insumo_'+ id +'">'+ des_Insumo +'</label>';
	celda6.innerHTML	= '<center><label id="des_UnidadMedida_'+ id +'">'+ des_Unidad +'</label></center>';
	celda7.innerHTML	= '<input type="text" id="num_Cantidad_'+ id +'" style="width:90px; text-align:right;" class="cajaTexto" onkeypress="validarNumero( event, this )"/>';
	celda8.innerHTML	= '<input type="text" id="des_MedidaEspecial_'+ id +'" class="cajaTexto" value=""/>';
	celda9.innerHTML	= '<center><button type="button" onclick="borrarFila('+ id +')" title="Clic aqui para borrar la fila"><img src="../images/delete.png"/></button></center>';
	celda10.innerHTML	= '<td background="../images/orilla_derecha.gif" style="background-repeat:repeat-y" width="3px"></td>';
	
	if( opcion > 1 )
	{
		obtenerFrentes( id, opcion, idu_Frente, idu_Partida );
		$( '#idu_Empresa_' + id ).prop('disabled', 'disabled');
		$( '#idu_Frente_' + id ).prop('disabled', 'disabled');
		$( '#idu_Partida_' + id ).prop('disabled', 'disabled');	
		$( '#num_Cantidad_' + id ).val( num_Cantidad );
		$( '#des_MedidaEspecial_' + id ).val( des_Medida );
	}
}

function validarNumero(  evt, t )
{
	var theEvent = evt || window.event;
	
	if( theEvent.keyCode == 8 || theEvent.keyCode == 9 || theEvent.keyCode == 13 || theEvent.keyCode == 37 || theEvent.keyCode == 38 || theEvent.keyCode == 39 || theEvent.keyCode == 40 || theEvent.keyCode == 116 )
		return;
		
		var key = theEvent.keyCode || theEvent.which;
	
		key = String.fromCharCode( key );
		
		var regex = /[0-9]|\./;
		if( !regex.test(key) ) 
		{
		theEvent.returnValue = false;
		if(theEvent.preventDefault) 
			theEvent.preventDefault();
		}
		else
		{
		  	var decimal = t.value.split( '.' );	
		  	if( decimal[1].length > 5 )
		  	{
		  	  	theEvent.returnValue = false;
			if(theEvent.preventDefault) 
				theEvent.preventDefault();		  	
		  	}
		}
}

function borrarFila( id )
{	
	if( confirm( 'Esta seguro de eliminar este insumo de la lista?' ) )
	{
		var cc = $( '#idu_Frente_' + id ).val().substring( 0, 1 ) + '00';
		
		if ($('#idu_Empresa_' + id).val() != '' && $('#idu_Obra').val() != '' && $('#idu_Cuantificacion').val() != '' && $('#idu_Frente_' + id).val() != '' && $('#idu_Partida_' + id).val() != '' && $('#idu_Insumo_' + id).text() != '') 
		{
			$('#detalles').val($('#idu_Empresa_' + id).val() + ',' + $('#idu_Obra').val() + ',' + $('#idu_Cuantificacion').val() + ',' + cc + ',' + $('#idu_Frente_' + id).val() + ',' + $('#idu_Partida_' + id).val() + ',' + $('#idu_Insumo_' + id).text());
			$( '#sn_Borrar' ).val( 'S' );
			$( '#sn_Enviar' ).val( 'N' );
			borrarDetalle( $( '#detalles' ).val(), id );
		}
		
		$( '#fila_' + id ).remove();
	}
}

function borrarDetalle( registro, fila )
{
	
	var ruta = '../componentes/cuantificaciones.cfc?method=CuantificacionBorrarDetalleAjax&detalle=' + registro;
	$.ajax
	({
		url: 		ruta,
		type: 		'GET',
		dataType:	'json',
		success:	function( resultado )
					{			
						console.log( resultado );
						
						if( resultado == '' )
						{
							
							alert( 'El insumo ya fue eliminado de la cuantificacion' );
						}
						else
							alert( resultado );						
					}
	});
}

function cargarArchivo()
{
	$( '#sn_Cargar' ).val( 'S' );
	$( '#sn_Enviar' ).val( 'N' );
	form1.submit();
}

function limpiar()
{
	$('input[type="text"][id^="des_MedidaEspecial_"]' ).each
	(
		function()
		{
			if( $( this ).val() == 'undefined' )
				$( this ).val( '' );
		}
	);
}

function validar( id_Obra )
{
	var datos 	= '';
	var msg 	= ''; 
	var valido 	= true;
	var tmp 	= new Array();
	
	$('select[id^="idu_Frente_"]' ).each
	(
		function()
		{
			var aux 	= new Array();
			var campo 	= $( this ).attr( 'id' ).split('_');
			var id 		= campo[ campo.length - 1 ]; 
			var obj 	= '';
						
			if( $( '#idu_Empresa_' + id ).val() == '' )
			{
				valido = false;
				$( '#idu_Empresa_' + id ).css({ borderColor: 'red' });
			}
			else
			{
				$( '#idu_Empresa_' + id ).css({ borderColor: '' });
				aux.push( $( '#idu_Empresa_' + id ).val() );
				obj += $( '#idu_Empresa_' + id ).val() + '_';
			}
			
			aux.push( id_Obra );
			
			if( $( '#idu_Frente_' + id ).val() == '' )
			{
				valido = false;
				$( '#idu_Frente_' + id ).css({ borderColor: 'red' });
			}
			else
			{
				$( '#idu_Frente_' + id ).css({ borderColor: '' });
				aux.push( $( '#idu_Frente_' + id ).val().substring( 0, 1 ) + '00' );
				aux.push( $( '#idu_Frente_' + id ).val() );
				obj += $( '#idu_Frente_' + id ).val() + '_';
			}
			
			if( $( '#idu_Partida_' + id ).val() == '' )
			{
				valido = false;
				$( '#idu_Partida_' + id ).css({ borderColor: 'red' });
			}
			else
			{
				$( '#idu_Partida_' + id ).css({ borderColor: '' });
				aux.push( $( '#idu_Partida_' + id ).val() );
				obj += $( '#idu_Partida_' + id ).val() + '_';
			}
			
			if( $( '#idu_Insumo_' + id ).text() == '' )
			{
				valido = false;
				$( '#idu_Insumo_' + id ).css({ borderColor: 'red' });
			}
			else
			{
				$( '#idu_Insumo_' + id ).css({ borderColor: '' });
				aux.push( $( '#idu_Insumo_' + id ).text() );
				obj += $( '#idu_Insumo_' + id ).text();
			}
			
			if( $( '#num_Cantidad_' + id ).val() == '' )
			{
				valido = false;
				$( '#num_Cantidad_' + id ).css({ borderColor: 'red' });
			}
			else
			{
				$( '#num_Cantidad_' + id ).css({ borderColor: '' });
				aux.push( $( '#num_Cantidad_' + id ).val() );
			}
			
			if( $( '#des_MedidaEspecial_' + id ).val() == '' || $( '#des_MedidaEspecial_' + id ).val() =='undefined' )
				aux.push( '!' );
			else
				aux.push( $( '#des_MedidaEspecial_' + id ).val() + " " );
			
			if( $( '#de_Comentarios' ).val() == '' )
				aux.push( '!' );
			else
				aux.push( $( '#de_Comentarios' ).val() + " " );
				
			var posicion = 	tmp.indexOf( obj );	
			if( posicion == -1 )
			{
				tmp.push( obj );
				$( '#fila_' + id ).css({ borderColor: '', background: '' });
				$( '#fila_' + posicion ).css({ borderColor: '', background: '' });
			}
			else
			{
				$( '#fila_' + id ).css({ borderColor: 'red', background: 'red' });
				$( '#fila_' + posicion ).css({ borderColor: 'red', background: 'red' });
				valido = false;
			}
			
			datos += aux + '|';
			
		}
	);
	
	if( $( '#sn_Error' ).val() == 'S' )
	{
		alert( 'El archivo de la cuantificacion contiene errores' );
		return;
	}
	
	if( $( '#ar_Generador' ).val() == '' && $( '#sn_GuardarBorrador' ).val() == 0 )
	{
		alert( 'Es necesario cargar el generador' );
		valido = false;
		return;
	}
	
	if( $( '#sn_GuardarBorrador' ).val() == '' )
	{
		alert( 'Es especificar como se va aguardar' );
		valido = false;
		return;
	}
	
	if( !valido )
	{
		alert( 'Existen errores en la lista de insumos, los campos con borde rojo contienen errores.\nSi la fila entera esta en rojo quiere decir que se repitio el insumo.' );
		return;
	}
	
	if( $( '#sn_GuardarBorrador' ).val() == 1 )
	{
		$( '#opc_Borrador' ).val( '1' );
	}
	else
		$( '#opc_Borrador' ).val( '0' );
	
	$( '#detalles' ).val( datos );
	$( '#sn_Enviar' ).val( 'S' );
	$( '#sn_Cargar' ).val( 'N' );
	
	form1.submit();
}
