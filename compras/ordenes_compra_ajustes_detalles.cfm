<cfset CurrentPage=GetFileFromPath(GetTemplatePath())>
<cfparam name="Filtro_id_Proveedor" default="">
<cfquery name="RSProveedor" datasource="#Session.cnx#">
    SELECT id_Proveedor FROM OrdenesCompra WHERE id_Empresa = #id_Empresa# AND id_Obra = #id_Obra# AND id_OrdenCompra = #id_OrdenCompra#
</cfquery>

<cfquery datasource='#session.cnx#' name='rsSuma2'>
            EXECUTE OC_FACTURAS_TOTALOC #id_empresa# , #id_Obra#, #RSProveedor.id_Proveedor#, #id_OrdenCompra#<!---TOTAL DE FACTURAS YA AGREGADAS--->
</cfquery>

<cfif #rsSuma2.RESTA# GT 0>
    <cfset Facturas=1>
<cfelse>
    <cfset Facturas=0>

</cfif>

<cfif IsDefined("FORM.MM_InsertRecord") AND FORM.MM_InsertRecord EQ "form_detalles">

    <cfparam name="fh_Ajuste" default="#DateFormat(Now(),'yyyy/mm/dd')#">
    <cfset nd_OrdenesCompra= ArrayNew(1)>
    <cfset nu_Cantidades= ArrayNew(1)>
    <cfset id_Insumos= ArrayNew(1)>
    <cfset de_Detalles= ArrayNew(1)>
    <cfset im_Precios= ArrayNew(1)>
    <cfinvoke component="#Application.componentes#.ordenescompra" method="RSMostrarDetallesAjuste" returnvariable="RS_MostrarDetalles" 
    id_Empresa="#id_Empresa#" id_Obra="#id_Obra#" id_OrdenCompra="#id_OrdenCompra#">    
    
    <cfloop query="RS_MostrarDetalles.rs">
        <cfif isDefined("FORM.nu_CantidadPorAjustar_#CurrentRow#") AND ( Evaluate("FORM.nu_CantidadPorAjustar_#CurrentRow#") NEQ "" AND Evaluate("FORM.nu_CantidadPorAjustar_#CurrentRow#") NEQ "0")>
            <cfset ArrayAppend(nd_OrdenesCompra, #RS_MostrarDetalles.rs.id_Insumo#)>
            <cfset ArrayAppend(nu_Cantidades, Evaluate("FORM.nu_CantidadPorAjustar_#CurrentRow#") )>
        </cfif>
        <cfset ArrayAppend(id_Insumos, #RS_MostrarDetalles.rs.id_Insumo# )>
        <cfset ArrayAppend(de_Detalles, Evaluate("FORM.de_Detalle_#CurrentRow#") )>
        <cfif Evaluate("FORM.im_PrecioNvo_#CurrentRow#") NEQ "" AND Evaluate("FORM.im_PrecioNvo_#CurrentRow#") NEQ "0" AND  Evaluate("FORM.im_PrecioNvo_#CurrentRow#") NEQ #RS_MostrarDetalles.rs.im_Precio#>
            <cfset ArrayAppend(im_Precios, Evaluate("FORM.im_PrecioNvo_#CurrentRow#") )>
        <cfelse>
            <cfset ArrayAppend(im_Precios,#RS_MostrarDetalles.rs.im_Precio# )>
        </cfif>
    </cfloop>
    <!--- <cfif ArrayLen(nd_OrdenesCompra) LT 1>
        <cf_error_manejador tipoError="validaciones" mensajeError="Al menos debe ajustar un insumo con cantidad distinta a cero.">      
    </cfif> --->
    
    <cfif #FORM.id_EmpleadoAutoriza# EQ ''>
        <cfset id_EmpleadoAutoriza = 1>
    <cfelse>
        <cfset id_EmpleadoAutoriza = #FORM.id_EmpleadoAutoriza#>    
    </cfif>
    
    <cfif #gastos# EQ ''>
        <cfset gastos=0>
        
    </cfif>
    

    <cfinvoke component="#Application.componentes#.ordenescompraajustes" method="AjustarDetalles" returnvariable="RSAgregar" id_Empresa="#FORM.id_Empresa#" id_Obra="#FORM.id_Obra#" id_OrdenCompra="#FORM.id_OrdenCompra#" id_Proveedor="#FORM.Filtro_id_Proveedor#" fh_Ajuste="#fh_Ajuste#" id_EmpleadoAutoriza="#id_EmpleadoAutoriza#" id_EmpleadoRealizo="#FORM.id_EmpleadoRealizo#" nd_OrdenesCompra="#nd_OrdenesCompra#" nu_Cantidades="#nu_Cantidades#" im_Precios="#im_Precios#" de_Detalles="#de_Detalles#" id_Insumos="#id_Insumos#" de_Motivo="#de_Motivo#" Gastos="#gastos#">
    <cfif #RSAgregar.tipoError# NEQ "">
        <cf_error_manejador tipoError="#RSAgregar.tipoError#" mensajeError="#RSAgregar.mensaje#">
    </cfif>
    
    <!--- COMPRADOR --->
    <cfinvoke component ="#Application.componentes#.empleados" method="RSMostrarPorIDEmpleados" returnvariable="Comprador"
        id_Empresa="1"
        id_Empleado="#Session.id_Empleado#"
    >             
       
    <cfquery name="RsEncabezado" datasource="#Session.cnx#">
            SELECT 
                o.de_Obra,
                o.cl_Construccion, 
                ( e.nb_Nombre + ' ' + e.nb_ApellidoPaterno + ' ' + e.nb_ApellidoPaterno ) AS nb_Empleado , 
                e.de_eMail, 
                oca.fh_Ajuste,
                em.nb_Empresa,
                p.nb_Proveedor
            FROM    
                OrdenesCompra oc
            INNER JOIN OrdenesCompraAjustes oca
                ON  oc.id_Empresa = oca.id_Empresa
                AND oc.id_Obra = oca.id_Obra 
                AND oc.id_OrdenCompra = oca.id_OrdenCompra 
            INNER JOIN Obras o 
                ON  o.id_Empresa = oc.id_Empresa 
                AND o.id_Obra = oc.id_Obra  
            INNER JOIN Empleados e
                ON e.id_Empleado = oc.id_EmpleadoComprador 
            INNER JOIN Empresas em
                ON  em.id_Empresa = o.id_Empresa 
            INNER JOIN Proveedores p
                ON  p.id_Proveedor = oc.id_Proveedor
            WHERE 
                o.id_Empresa = #Form.id_Empresa#
            AND
                o.id_Obra = #Form.id_Obra#
            AND 
                oc.id_OrdenCompra = #Form.id_OrdenCompra#               
    </cfquery>
    
    <cfquery name="RsDetalles" datasource="#Session.cnx#">
        SELECT 
            oca.id_Insumo,
            i.de_Insumo,
            u.de_UnidadMedida,
            m.de_Marca,
            SUM( oca.nu_Cantidad ) AS nu_Cantidad ,
            MAX( oca.im_Precio ) AS im_Precio, 
            ( SUM( oca.nu_Cantidad ) * MAX( oca.im_Precio ) ) AS im_CantidadPrecio
        FROM 
            OrdenesCompra oc
        INNER JOIN OrdenesCompraAjustesDetalle oca
            ON  oc.id_Empresa = oca.id_Empresa 
            AND oc.id_Obra = oca.id_Obra 
            AND oc.id_OrdenCompra = oca.id_OrdenCompra 
         INNER JOIN Insumos i
            ON  oca.id_Insumo = i.id_Insumo 
        LEFT JOIN InsumosMarcas im
            ON  im.id_Insumo = i.id_Insumo 
        INNER JOIN UnidadesMedida u
            ON  u.id_UnidadMedida = i.id_UnidadMedida 
        LEFT JOIN Marcas m
            ON  m.id_Marca = im.id_Marca
        WHERE 
            oca.id_Empresa = #Form.id_Empresa#
        AND
            oca.id_Obra = #Form.id_Obra#
        AND
            oca.id_OrdenCompra = #Form.id_OrdenCompra#
        AND
            oca.id_Ajuste = #RSAgregar.id_Ajuste#
        GROUP BY
            oca.id_Insumo,
            i.de_Insumo,
            u.de_UnidadMedida,
            m.de_Marca
    </cfquery> 
    
    <cfsavecontent variable="msgAjuste">
            <cfoutput>
                
                <table cellpadding="0" cellspacing="0" border="1" style="width:600px;">
                    <tr style="background-color:##999999">
                    
                        <td colspan="6" align="center" style="text-align:center;font-weight:bold">ORDEN DE COMPRA</td>
                        
                        <cfloop query="RsEncabezado">
                            <tr><td >Empresa:</td><td colspan="5">#RsEncabezado.nb_Empresa#</td></tr>
                            <tr><td >Obra:</td><td colspan="5">#id_Obra# - #RsEncabezado.de_Obra#</td></tr>
                            <tr><td >Orden de Compra:</td><td colspan="5">#id_OrdenCompra#</td></tr>
                            <tr><td >Proveedor:</td><td colspan="5">#RsEncabezado.nb_Proveedor#</td></tr>
                            <tr><td >Fecha Movimiento:</td><td colspan="5">#DateFormat(fh_Ajuste,'dd/mm/yyyy')#</td></tr>
                        </cfloop>
                        
                        <tr style="background-color:##CCCCCC">
                            <td style="text-align:center">Insumo</td>
                            <td style="text-align:center">UM</td>
                            <td style="text-align:center">Marca</td>                            
                            <td style="text-align:center">Cantidad</td>
                            <td style="text-align:center">Precio</td>
                            <td style="text-align:center">Importe</td>
                        </tr>
                        
                        <cfset im_Total = 0>
                        <cfloop query="RsDetalles">
                            <tr>
                                <td>#RsDetalles.id_Insumo# - #RsDetalles.de_Insumo#</td>
                                <td>#RsDetalles.de_UnidadMedida#</td>
                                <td>#RsDetalles.de_Marca#&nbsp;</td>
                                <td style="text-align:right">#RsDetalles.nu_Cantidad#</td>
                                <td style="text-align:right">#NumberFormat(RsDetalles.im_Precio,'$,_.__')#</td>
                                <td style="text-align:right">#NumberFormat(RsDetalles.im_CantidadPrecio,'$,_.__')#</td>
                            </tr>
                            <cfset im_Total = im_Total + #RsDetalles.im_CantidadPrecio#>
                        </cfloop>
                        <tr>
                            <td colspan="5" style="text-align:right; background-color:##CCCCCC">Total:&nbsp;</td>
                            <td style="text-align:right">#NumberFormat(im_Total,'$,_.__')#</td>
                        </tr>   
                    
                    </tr>
                </table>
                
            </cfoutput>
        </cfsavecontent>
    
    <!---<cfinvoke component="#Application.componentes#.empleados" method="ObtenerEMail" returnvariable="correo" id_Empleado="#Session.id_Empleado#">
    
     Proveedor.de_eMail
    <cfif #correoProveedor# NEQ ''>
        <cfinvoke   component       =   "#Application.componentes#.funciones"
            method                  =   "cfMail"
            Destinatario            =   "#Comprador.rs.de_eMail#"
            Asunto                  =   "Orden de Compra Ajustada"
            EncabezadoContenido     =   "Envia: #Session.nb_Usuario#"
            Contenido               =   "#msgAjuste#"
        > 
        <cf_location_msg url="ordenes_compra_ajustes_listado.cfm?id_Empresa=#FORM.id_Empresa#&id_Obra=#FORM.id_Obra#&Filtro_id_OrdenCompra=#FORM.id_OrdenCompra#&sn_Mostrar=S" operacion="operacion" mensaje="#RSAgregar.mensaje#">
    <cfelse>
        <cf_location_msg url="compras_bandeja_comprador.cfm" mensaje="Se realizó el ajuste,<br />pero el proveedor no tiene correo configurado">
    </cfif>--->
</cfif>

<cfquery name="RsAjustes" datasource="#session.cnx#">
    
    SELECT 
        id_Ajuste,fh_Ajuste,de_Motivo, im_Total
    FROM
        OrdenesCompraAjustes
    WHERE
        id_Empresa = #id_Empresa# AND
        id_Obra = #id_Obra# AND
        id_OrdenCompra = #id_OrdenCompra# AND
        id_Estatus <> 202

 </cfquery>

<cfinvoke component="#Application.componentes#.ordenescompra" method="RSMostrarPorIDOrdenesCompra" returnvariable="RS_MostrarOrdenCompra" id_Empresa="#id_Empresa#" id_Obra="#id_Obra#" id_OrdenCompra="#id_OrdenCompra#">
<cfinvoke component="#Application.componentes#.ordenescompra" method="RSMostrarDetallesAjuste" returnvariable="RS_MostrarDetalles" id_Empresa="#id_Empresa#" id_Obra="#id_Obra#" id_OrdenCompra="#id_OrdenCompra#">
<cfinvoke component="#Application.componentes#.obras" method="RSMostrarPorIDObras" returnvariable="RS_Obras" id_Empresa="#id_Empresa#" id_Obra="#id_Obra#">


<cfinvoke component =   "#Application.componentes#.proveedores" method="RSMostrarPorIDProveedores" returnvariable="Proveedor"
    id_Proveedor="#RSProveedor.id_Proveedor#"
>  



<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <link rel="stylesheet" href="../css/style.css" type="text/css">
        <title>Untitled Document</title>
        <script language="javascript" src="../js/jquery.js"></script>
        <script language="javascript" src="../js/funciones.js"></script>
        <script type="text/javascript" src="modal.js"></script>
        <!---Refs de Calendario --->
        <script type="text/javascript" src="../calendario/calendar.js"></script>
        <script type="text/javascript" src="../calendario/calendar-setup.js"></script>
        <script type="text/javascript" src="../calendario/lang/calendar-es.js"></script>
        <style type="text/css"> @import url("../calendario/calendar-blue2.css");</style>
        <link rel="stylesheet" type="text/css" href="modal_pasword.css">

        <!---Termina Refs de Calendario--->
        
        <script>
            
              
                 var inicio=function(){


                    var checar= function(){
                
                     
                          if( Autorizar($("#password").val())){
                              form1.submit();

                          }

                        
                    }
                
                var validar=function()
                {
                    var msg = '';
                    var msgInsumoError = '';
                    var cantInsumosError = 0;
                    var total = 0;
                    var insumo = 0;
                    var almacen = 0;
                    
                    $('input[type="text"][id^="nu_CantidadPorAjustar_"]').each
                    (
                        function(e)
                        {
                            if($( this ).val() != 0){

                                
                                insumo = ( this.id.substring(22, (this.id  + '').length ) );                        
                                almacen  = parseFloat( $( '#nu_RecibidoAlmacen_' + insumo ).text().replace( ',', '' ) );
                                // if( parseFloat( $( this ).val() ) > almacen )
                                // {
                                //     cantInsumosError ++;
                                //     msgInsumoError += '\n-Esta intentando hacer un ajuste para el insumo '+ insumo +', por una cantidad mayor a lo recibido en almacen.';
                                // }
                                if( parseFloat( $( this ).val() ) < almacen )
                                {
                                    cantInsumosError ++;
                                    msg += '\n-Esta intentando hacer un ajuste para el insumo '+ insumo +', por una cantidad menor a lo recibido en almacen.';
                                }
                                
                                total += parseFloat( $( this ).val() );
                            }
                        }
                    );  
                       
                    if( $('#sugerida_Proveedor').val() == '' || $('#Filtro_id_Proveedor').val() == '' )
                    {
                        
                        msg += '\n- No se ha ingresado el proveedor paara la orden de compra.' ;
                    }
                    if( $('#de_Motivo').val() == '' )
                    {
                        
                        msg += '\n- No se ha ingresado el motivo del ajuste.' ;
                    }
                  
                   /* if( total == 0 )
                        msg += '\n-La cantidad de insumos del ajuste debe ser diferente de 0.';
                    */
    <!---               if( cantInsumosError > 0 )
                    {
                        if( cantInsumosError > 10 )
                            msg += 'Esta intentando hacer un ajuste por una cantidad mayor a lo recivido en almacen, \npara mas de 10 insumos';
                        else
                            msg += msgInsumoError;
                    }--->
                    
                    if( msg != '' )
                        alert( 'Han ocurrido los siguientes errores:' + msg );
                    else{
                        var facturas=<cfoutput>#Facturas#</cfoutput>;

                        if(facturas==1){
                             modal('Esta Orden de Compra Tiene Facturas Asignadas','<center style="font-size:14px;"><b>Ingrese contrase&ntilde;a de Autorizaci&oacute;n</b><br><input type="password" id="password" class="contenido" style="width:250px;height:21px;"><br><br><button type="button" class="boton_imagen" id="checar"><img src="../images/autorizar.png" alt="Clic para buscar insumos"></button><br></center>','style="background-image:url(../images/bg_gris.gif);color:#007670"');
                             $("#modal_box").css('width','40%');
                             $("#modal_box").css('height','');
                             $("#modal_box").css('height','1000px');
                             $("#modal_box").css('overflow-y','hidden');
                        }else{
                            form1.submit();
                        }

                        

                    }
                    
                }
                function Autorizar(password)
                {
                    var autorizo=false;
                    $.ajax({
                       type: 'GET',
                       async: false,
                       dataType: 'JSON',
                       url: '../componentes/ferrominio_pass.cfc',
                       data: 
                       {
                          method: 'validar_Ferrominio_Password',
                          de_Password:password
                       },
                       success: function(response) {
                          if (response) //Contraseña valida
                            autorizo=true;
                        
                          
                        
                      
                       },
                       error: function()
                       {
                        
                        alert('No se pudo comprobar la contrase\u00f1a.\n\n\nFavor de intentar mas tarde.');
                       
                       }
                    });
                    return autorizo;
                   
                 }
                
                $("body").on('click','#checar',function(){
                    
                   
                     if( Autorizar($("#password").val())){
                          form1.submit();

                      }else{
                        alert('Contrase\u00f1a incorrecta');
                      }

                    
                 });
        
                
                function Valida(cantidadporajustar, cantidadpermitida)
                {
                    var cantAjustar = parseFloat($('#'+cantidadporajustar).val());
                    var cantPermitida = parseFloat($('#'+cantidadpermitida).val());
                
                    /*if(cantAjustar > cantPermitida)
                    {
                            inlineMsg(cantidadporajustar,'Cantidad no valida');
                            return false;
                    }*/
                }
                 $("body").on('click','#validar',validar);
                 $("body").on('click','#checar',checar);
                
                 
            }
            
            $(document).ready(inicio);
        
        </script>
    </head>
    
    <body onLoad="form1.id_EmpleadoAutoriza.focus();">
        <cfinclude template="menu.cfm">&nbsp;
        <cfform method="post" name="form1" action="#CurrentPage#">
           <table align="center" width="600" cellpadding="0" cellspacing="0" border="0" bgcolor="#C7C7C7">
                    <tr class="encabezado">
                    <td width="3px"><img src="../images/esquina_izquierda.gif"></td>
                    <td width="100%" align="center" valign="middle">Ajustar Insumos a Orden de Compra</td>
                    <td width="3px"><img src="../images/esquina_derecha.gif"></td>
                </tr>
                <tr>
                    <td colspan="3">
                        <table align="center" width="99.6%" style="background:url(../images/fondo.png); background-position:bottom; background-repeat:repeat-x">
                            <tr valign="baseline">
                              <td nowrap align="right">Obra:</td>
                              <td>
                                <cfinput type="text" class="contenido_readOnly" name="id_Obra_lab" id="id_Obra_lab" validate="integer" onBlur="$x('de_Obra_lab').value='';" size="8" value="#id_Obra#">
                                <cftextarea bind="cfc:#Application.componentes#.obras.getObraPorID(#Session.id_Empresa#,#id_Obra#)" bindonload="yes" id="de_Obra_lab" name="de_Obra_lab" rows="1" cols="40" class="contenido_readOnly" style="overflow:hidden;" required="yes" message="La Clave de Obra no es válida."></cftextarea>
                              </td>
                            </tr>
                            <tr valign="baseline">
                                <td nowrap align="right">Folio:</td>
                                <td width="75%"><input type="text" class="contenido_readOnly" value="<cfoutput>#id_OrdenCompra#</cfoutput>" size="50"/></td>
                            </tr>
                            <tr valign="baseline">
                                <td nowrap align="right">Importe Actual:</td>
                                <td width="75%"><input type="text" class="contenido_readOnly" value="<cfoutput>#NumberFormat(RS_MostrarOrdenCompra.rs.im_Total,'$,_.__')#</cfoutput>" size="50"/></td>
                            </tr>
                            <tr valign="baseline">
                                <td nowrap align="right">Importe Ajustado:</td>
                                <td width="75%"><input type="text" class="contenido_readOnly" value="<cfoutput>#NumberFormat(RS_MostrarOrdenCompra.rs.im_Ajustado,'$,_.__')#</cfoutput>" size="50"/></td>
                            </tr>
                            <tr valign="baseline">
                                <td nowrap align="right">Autoriza:</td>
                                <td width="75%">
                                    <cfinput type="text" name="id_EmpleadoAutoriza" id="id_EmpleadoAutoriza" class="contenido_readOnly" readonly="true" size="8" validate="integer" value="#RS_Obras.rs.id_Contador#">
                                    <cftextarea bind="cfc:#Application.componentes#.empleados.getNombrePorID(#Session.id_Empresa#, {id_EmpleadoAutoriza@blur})" bindonload="yes" id="nb_Autorizador" name="nb_Autorizador" rows="1" cols="40" class="contenido_readOnly" style="overflow:hidden;" required="yes" message="Seleccione el Empleado que autoriza"></cftextarea>
                                </td>
                            </tr>
                            <tr>
                                <td nowrap align="right">Realiz&oacute;:</td>
                                <td width="75%">
                                    <cfinput type="text" name="id_EmpleadoRealizo" class="contenido_readOnly" readonly="true" size="8" validate="integer" value="#Session.id_Empleado#">
                                    <cftextarea bind="cfc:#Application.componentes#.empleados.getNombrePorID(#Session.id_Empresa#, {id_EmpleadoRealizo@blur})" bindonload="yes" id="nb_Comprador" name="nb_Comprador" rows="1" cols="40" class="contenido_readOnly" style="overflow:hidden;" required="yes" message="Seleccione un Comprador"></cftextarea>
                                </td>
                            </tr>
                            <tr valign="baseline">
                                <td nowrap align="right">Proveedor:</td>
                                <td width="75%">
                                    <cfinput type="text" name="sugerida_Proveedor" id="sugerida_Proveedor" class="contenido" size="8" value="#Proveedor.rs.id_Proveedor#" required="yes" message="Seleccione un proveedor de la orden de compra"/>
                                    <cfselect name="Filtro_id_Proveedor" id="Filtro_id_Proveedor" bind="cfc:#Application.componentes#.proveedores.getProveedoresSugeridasParaFiltrotmp({sugerida_Proveedor@blur})" bindonload="yes" display="nb_ProveedorCorto" value="id_Proveedor" onBlur="javascript:$x('sugerida_Proveedor').value=$x('Filtro_id_Proveedor').value;" class="contenido" style="width:200px" required="yes" message="Seleccione un Proveedor para la orden de compra">
                                    </cfselect>
                                </td>
                            </tr>
                            <tr valign="baseline">
                                <td nowrap align="right">Motivo del ajuste:</td>
                                <td><cftextarea name="de_Motivo" id="de_Motivo" class="contenido" style="width:319px; vertical-align: top;"></cftextarea></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr style="background-image:url(../images/abajo.gif)">
                    <td width="3px"><img src="../images/abajo_izquierda.gif"></td>
                    <td></td>
                    <td width="3px"><img src="../images/abajo_derecha.gif"></td>
                </tr>
            </table>
            <br>

            <table border="0" cellpadding="0" cellspacing="0" width="600" class="fondo_listado" align="center">
                <tr class="encabezado">
                    <td width="3px"><img src="../images/esquina_izquierda.gif"></td>
                    <td align="center">No. Ajuste</td>
                    <td align="center">Fecha Ajuste</td>
                    <td align="center">Motivo</td>
                    <td align="center">Importe</td>
                    <td width="3px"><img src="../images/esquina_derecha.gif"></td>
                </tr>
                <cfoutput query="RsAjustes">
                     <tr>
                        <td background="../images/orilla_izquierda.gif" style="background-repeat:repeat-y" width="3px"></td>
                        <td class="cuadricula" align="center">#id_Ajuste#</td>
                        <td class="cuadricula" align="center">#dateFormat(fh_Ajuste,"dd/mm/yyyy")#</td>
                        <td class="cuadricula" align="left">#de_Motivo#</td>
                        <td class="cuadricula" align="right">$#NumberFormat(im_Total,",_.__")#</td>
                        <td background="../images/orilla_derecha.gif" style="background-repeat:repeat-y" width="3px"></td>
                    </tr>
                </cfoutput>
            </table>

            <p>&nbsp;</p>
            <table border="0" cellpadding="0" cellspacing="0" width="1400" class="fondo_listado" align="center">
                <tr class="encabezado">
                    <td width="3px"><img src="../images/esquina_izquierda.gif"></td>
                    <!---<td align="center" width="75"># Detalle</td>
                    <td align="center" nowrap="nowrap">Clave C.C.</td>--->
                    <td align="center">Insumo</td>
                    <td align="center">U.M.</td>
                     <td align="center">Notas</td>
                    <td align="center" nowrap="nowrap">Cant. Solicitada</td>
                    <td align="center" nowrap="nowrap">Cant. Actual</td>
                    <td align="center">Precio Actual</td>
                    <td align="center"nowrap="nowrap">Importe Actual</td>
              <!--- <td align="center" width="160">Importe Ajustado</td>
                    <td align="center" width="150">Cant. Ajustada</td>  --->
                    <td align="center" nowrap="nowrap">Cant. Rec. en Almacen</td>
                    <td align="center" nowrap="nowrap">Cantidad Nueva</td>
                    <td align="center">Precio Nuevo</td>
                    <td align="center" nowrap="nowrap">Imp. por Ajustar</td>
                    <td width="3px"><img src="../images/esquina_derecha.gif"></td>
                </tr>
                
                <cfoutput query="RS_MostrarDetalles.rs">
                    <tr>
                        <td background="../images/orilla_izquierda.gif" style="background-repeat:repeat-y" width="3px"></td>
                        <td class="cuadricula" align="left">#RS_MostrarDetalles.rs.id_Insumo# - #RS_MostrarDetalles.rs.de_Insumo#</td>
                        <td class="cuadricula" align="left">#RS_MostrarDetalles.rs.de_UnidadMedida#</td>
                        <td class="cuadricula" align="left">
                        <cfinput type="text" id="de_Detalle_#CurrentRow#" name="de_Detalle_#CurrentRow#" class="contenido" value="#RS_MostrarDetalles.rs.de_Detalle#" size="18">
                        </td>
                        <td class="cuadricula" align="right">#NumberFormat(RS_MostrarDetalles.rs.nu_CantidadSolicitada,",_.______")#</td>
                        <td class="cuadricula" align="right">#NumberFormat(RS_MostrarDetalles.rs.nu_Cantidad,",_.______")#</td>
                        <td class="cuadricula" align="right">$#NumberFormat(RS_MostrarDetalles.rs.im_Precio,",_.______")#</td>
                        <td class="cuadricula" align="right">$#NumberFormat(RS_MostrarDetalles.rs.im_CantidadPrecio,",_.__")#</td>
                        <td class="cuadricula" align="right"><label id="nu_RecibidoAlmacen_#CurrentRow#">#NumberFormat(RS_MostrarDetalles.rs.nu_RecibidoAlmacen,",_.______")#</label></td>
                        <td class="cuadricula" align="center">
                            <cfif #NumberFormat(RS_MostrarDetalles.rs.nu_RecibidoAlmacen,",_.______")# GTE #NumberFormat(RS_MostrarDetalles.rs.nu_Cantidad,",_.______")#>
                                <cfinput type="text" disabled="disabled" name="nu_CantidadPorAjustar_#CurrentRow#" class="contenido_flotante_negativo" size="15" value="0" required="no" validate="float"  message="La cantidad por ajustar del detalle #RS_MostrarDetalles.rs.id_Insumo# no es válida" >
                            <cfelse>
                                <cfinput type="text" name="nu_CantidadPorAjustar_#CurrentRow#" class="contenido_flotante" size="15" value="0" required="no" validate="float" placeholder="Cantidad Nueva" tooltip="Este campo va a remplazar la cantidad actual de la orden de">
                            </cfif>
                        </td>
                        <td class="cuadricula" align="right">
                        <cfinput type="text" id="im_PrecioNvo_#CurrentRow#" name="im_PrecioNvo_#CurrentRow#" class="contenido_flotante" value="0" size="18" required="yes" message="el precio es requerido" style="text-align:right;">
                        </td>
                        <td class="cuadricula" align="center">      
                             <cfinput type="text" name="im_PorAjustar_#CurrentRow#" class="contenido_readOnly" size="16" value="$#NumberFormat(0,',_.____')#" bind="cfc:#Application.componentes#.math.multiplicar2({nu_CantidadPorAjustar_#CurrentRow#@blur}, {im_PrecioNvo_#CurrentRow#@blur},#RS_MostrarDetalles.rs.nu_Cantidad#,#RS_MostrarDetalles.rs.im_Precio#, 'moneda')" style="text-align:right" onchange="alert('hola mundo')">  
                        </td>
                        <td class="cuadricula" align="center">
                          <a href="ordenes_compra_ajustes_detalles_borrar.cfm?id_Empresa=#id_Empresa#&id_Obra=#id_Obra#&id_OrdenCompra=#id_OrdenCompra#&id_Insumo=#RS_MostrarDetalles.rs.id_Insumo#" title="Clic para eliminar el insumo a la orden de compra"><img src="../images/delete.png" alt="Clic para realizar ajustes a la orden de compra" border="0"></a>
                        </td>
                        <td background="../images/orilla_derecha.gif" style="background-repeat:repeat-y" width="3px"></td>
                    </tr>
                    
                    <cfset cantidadpermitida = #RS_MostrarDetalles.rs.nu_Cantidad#-#RS_MostrarDetalles.rs.nu_RecibidoAlmacen#>
                    
                    <cfinput type="hidden" id="cantidadpermitida_#CurrentRow#" name="cantidadpermitida_#CurrentRow#" value="#cantidadpermitida#">
                </cfoutput>
                <tr>
                   <td background="../images/orilla_izquierda.gif" style="background-repeat:repeat-y" width="3px"></td>
                    <td align="right" colspan="10" class="cuadricula">
                        Otros Gastos:

                  </td>
                  <td td align="center" colspan="1" class="cuadricula">
                   <input type="text" name="gastos" id="gastos" class="contenido_flotante" value="<cfoutput>#RS_MostrarOrdenCompra.rs.im_GastosManiobra#</cfoutput>" size="14" style="text-align:right;"/> 
                  </td> 
                  <td td align="center" colspan="1" class="cuadricula">

                    </td>
                   <td background="../images/orilla_derecha.gif" style="background-repeat:repeat-y" width="3px"></td> 
                </tr>
                
                <tr>
                    <td background="../images/orilla_izquierda.gif" style="background-repeat:repeat-y" width="3px"></td>
                    <td align="center" colspan="12">
                        <button type="button" class="boton_imagen" id="validar"><img src="../images/guardar.png"></button>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <button type="button" class="boton_imagen" onClick="javascript:location='ordenes_compra_ajustes_listado.cfm'"><img src="../images/regresar.png"></button>
                    </td>
                    <td background="../images/orilla_derecha.gif" style="background-repeat:repeat-y" width="3px"></td>
                </tr>
                <tr style="background-image:url(../images/abajo.gif)">
                    <td width="3px"><img src="../images/abajo_izquierda.gif"></td>
                    <td colspan="12"></td>
                    <td width="3px"><img src="../images/abajo_derecha.gif"></td>
                </tr>
            </table> 
          <input type="hidden" name="MM_InsertRecord" value="form_detalles">
            <input type="hidden" name="id_Empresa" value="<cfoutput>#id_Empresa#</cfoutput>">
            <input type="hidden" name="id_Obra" value="<cfoutput>#id_Obra#</cfoutput>">
            <input type="hidden" name="id_OrdenCompra" value="<cfoutput>#id_OrdenCompra#</cfoutput>">
        </cfform>
    </body>
</html>