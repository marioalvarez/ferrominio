<cfset CurrentPage=GetFileFromPath(GetTemplatePath())>
<cfparam name="sn_Mostrar" default="S">
<cfparam name="id_Empresa" default="">
<cfparam name="id_EmpresaCambio" default="">
<cfparam name="operacion" default="">
<cfinvoke component="#Application.componentes#.presupuestos" method="ObtenEstatusPresupuestoDeObra" returnvariable="RSEstatusPresupuesto"
	id_Empresa="#Session.id_Empresa#"
	id_Obra="#Session.id_Obra#"
>
<cfif #RSEstatusPresupuesto.tipoError# NEQ "">
    <cf_error_manejador tipoError="#RSEstatusPresupuesto.tipoError#" mensajeError="#RSEstatusPresupuesto.mensaje#">
</cfif>
<cfif #RSEstatusPresupuesto.estatus# NEQ 1>
	<cf_error_manejador mensajeError="Presupuesto no AUTORIZADO">
</cfif>

<cfinvoke component="#Application.componentes#.presupuestos" method="RSTotalesExplosionado" returnvariable="RSTotalExplosion"
    id_Empresa ="#id_Empresa#"
    id_Obra="#Session.id_Obra#"
>
<cfinvoke component="#Application.componentes#.presupuestos" method="RSMostrarCentroCostoConTotal" returnvariable="RS_MostrarCentros"
    id_Empresa="#id_Empresa#"
    id_Obra="#Session.id_Obra#"
>
<cfif #RS_MostrarCentros.tipoError# NEQ "">
    <cf_error_manejador tipoError="#RS_MostrarCentros.tipoError#" mensajeError="#RS_MostrarCentros.mensaje#">
</cfif>
<cfinvoke component="#Application.componentes#.presupuestos" method="RSObtenImporteTotalParametrizado" returnvariable="RSTotalPresupuesto"
	id_Empresa="#id_Empresa#"
	id_Obra="#Session.id_Obra#"
>

<cfinvoke component="#Application.componentes#.tiposejecuciontarjeta" method="RSMostrarTodosTiposEjecucionTarjeta" returnvariable="RS_MostrarTiposEjecucion">

<cfinvoke component="#Application.componentes#.empresas" method="RSMostrarDinamicoEmpresas" returnvariable="RSEmpresa">
<cfif #RSEmpresa.tipoError# NEQ "">
	<cf_error_manejador tipoError="#RSEmpresa.tipoError#" mensajeError="#RSEmpresa.mensaje#">
</cfif>

<cfif isDefined('Session.ExplosionInsumos') AND #operacion# NEQ ''>
    <cfquery name="Session.ExplosionInsumos" dbtype="query">
    	SELECT
        	id_Empresa,id_Obra,id_CentroCosto,id_Frente,id_Partida,nivel
        FROM
        	Session.ExplosionInsumos
        GROUP BY
        	id_Empresa,id_Obra,id_CentroCosto,id_Frente,id_Partida,nivel
        ORDER BY
        	id_Empresa,id_Obra,id_CentroCosto,id_Frente,id_Partida
    </cfquery>
<cfelse>
	<cfset Session.ExplosionInsumos = QueryNew('id_Empresa,id_Obra,id_CentroCosto,id_Frente,id_Partida,nivel','varchar,integer,integer,varchar,varchar,varchar')>
	<cfoutput query="RS_MostrarCentros.rs">
		<cfset QueryAddRow(Session.ExplosionInsumos)>
        <cfset QuerySetCell(Session.ExplosionInsumos, 'id_Empresa', 	#id_Empresa# )>
        <cfset QuerySetCell(Session.ExplosionInsumos, 'id_Obra', 		#id_Obra# )>
        <cfset QuerySetCell(Session.ExplosionInsumos, 'id_CentroCosto', #id_CentroCosto# )>
        <cfset QuerySetCell(Session.ExplosionInsumos, 'id_Frente', 		'' )>
        <cfset QuerySetCell(Session.ExplosionInsumos, 'id_Partida', 	'' )>
        <cfset QuerySetCell(Session.ExplosionInsumos, 'nivel', 			'centros' )>
	</cfoutput>
</cfif>
<cfif IsDefined("operacion") AND operacion NEQ "">
    <cfquery name="Session.ExplosionInsumos" dbtype="query">
    	SELECT
        	id_Empresa,id_Obra,id_CentroCosto,id_Frente,id_Partida,nivel
        FROM
        	Session.ExplosionInsumos
        GROUP BY
        	id_Empresa,id_Obra,id_CentroCosto,id_Frente,id_Partida,nivel
        ORDER BY
        	id_Empresa,id_Obra,id_CentroCosto,id_Frente,id_Partida
    </cfquery>

	<cfoutput query="Session.ExplosionInsumos">
        <cfswitch expression="#nivel#">
            <!--- tarjetas --->
            <cfcase value="tarjetas">
                <cfset nd_PresupuestosEjecucion= arrayNew(1)>
                <cfset id_TiposEjecucionTarjetas= arrayNew(1)>
                <cfset nd_PresupuestosExplosion= arrayNew(1)>
                <cfset id_Empresas= arrayNew(1)>

                <cfinvoke component="#Application.componentes#.presupuestos" method="RSMostrarDetallePresupuesto" returnvariable="RS_MostrarPresupuestoDetalle"
                    id_Empresa="#id_Empresa#"
                    id_Obra="#id_Obra#"
                    id_CentroCosto="#id_CentroCosto#"
                    id_Frente="#id_Frente#"
                    id_Partida="#id_Partida#"
                >
                <cfloop query="RS_MostrarPresupuestoDetalle.rs">
					<cfif isDefined("id_TipoEjecucion_#id_CentroCosto##id_Frente##id_Partida#_#RS_MostrarPresupuestoDetalle.rs.nd_Presupuesto#")>
                        <cfset ArrayPrepend(nd_PresupuestosEjecucion, #RS_MostrarPresupuestoDetalle.rs.nd_Presupuesto#)>
                        <cfset ArrayPrepend(id_TiposEjecucionTarjetas, Evaluate("id_TipoEjecucion_#id_CentroCosto##id_Frente##id_Partida#_#RS_MostrarPresupuestoDetalle.rs.nd_Presupuesto#"))>
                    </cfif>
                    <cfif isDefined("chk_#id_CentroCosto##id_Frente##id_Partida#_#RS_MostrarPresupuestoDetalle.rs.nd_Presupuesto#")>
                        <cfset ArrayPrepend(nd_PresupuestosExplosion, #RS_MostrarPresupuestoDetalle.rs.nd_Presupuesto#)>
                    </cfif>
                </cfloop>
                <cfif operacion EQ 'cambiar_empresa'>
                	<cfset id_EmpresaSP = #id_EmpresaNueva#>
				<cfelse>
                	<cfset id_EmpresaSP = 'NULL'>
				</cfif>
                <cfif #FORM.id_Empresa# EQ ''>
                    <cfquery name="Empresas" datasource="#session.cnx#">
                        select distinct id_empresa from presupuestos where id_Obra = #Session.id_Obra# and id_CentroCosto = #id_CentroCosto# and id_Frente = #id_Frente# and id_Partida = #id_Partida# --and nd_Presupuesto in(#ArrayToList(nd_PresupuestosExplosion,',')#)
                    </cfquery>
                	<cfset id_Empresas = ListToArray(ValueList(Empresas.id_Empresa,','))>
				<cfelse>
                	<cfset ArrayPrepend(id_Empresas, #FORM.id_Empresa#)>
				</cfif>
                <cfinvoke component="#Application.componentes#.presupuestos" method="GuardaOperacionExplosionInsumosTarjetas" returnvariable="RSGuardar"
                	id_Empresas="#id_Empresas#"
                    id_Empresa="#id_EmpresaSP#"
                    id_Obra="#id_Obra#"
                    id_CentroCosto="#id_CentroCosto#"
                    id_Frente="#id_Frente#"
                    id_Partida="#id_Partida#"
                    nd_PresupuestosEjecucion="#nd_PresupuestosEjecucion#"
                    id_TiposEjecucionTarjetas="#id_TiposEjecucionTarjetas#"
                    nd_PresupuestosExplosion="#nd_PresupuestosExplosion#"
                    sn_MultiplicaFactorFrente="1"
                    cl_Operacion="#operacion#"
                >
                <cfif #RSGuardar.tipoError# NEQ "">
                    <cf_error_manejador tipoError="#RSGuardar.tipoError#" mensajeError="#RSGuardar.mensaje#">
                </cfif>
            </cfcase>

            <!--- partidas --->
            <cfcase value="partidas">
                <cfset id_PartidasEjecucion= arrayNew(1)>
                <cfset id_TiposEjecucionPartidas= arrayNew(1)>
                <cfset id_PartidasExplosion= arrayNew(1)>
                <cfset id_Empresas= arrayNew(1)>

                <cfinvoke component="#Application.componentes#.presupuestos" method="RSMostrarPartidasConTotal" returnvariable="RS_MostrarPartidas"
                    id_Empresa="#id_Empresa#"
                    id_Obra="#id_Obra#"
                    id_CentroCosto="#id_CentroCosto#"
                    id_Frente="#id_Frente#"
                >
                <cfloop query="RS_MostrarPartidas.rs">
                    <cfif isDefined("id_TipoEjecucion_#id_CentroCosto##id_Frente##id_Partida#")>
                        <cfset ArrayPrepend(id_PartidasEjecucion, #RS_MostrarPartidas.rs.id_Partida#)>
                        <cfset ArrayPrepend(id_TiposEjecucionPartidas, Evaluate("id_TipoEjecucion_#id_CentroCosto##id_Frente##id_Partida#"))>
                    </cfif>
                    <cfif isDefined("chk_#RS_MostrarPartidas.rs.id_CentroCosto##RS_MostrarPartidas.rs.id_Frente##RS_MostrarPartidas.rs.id_Partida#")>
                        <cfset ArrayPrepend(id_PartidasExplosion, #RS_MostrarPartidas.rs.id_Partida#)>
                    </cfif>
                </cfloop>
                <cfif operacion EQ 'cambiar_empresa'>
                	<cfset id_EmpresaSP = #id_EmpresaNueva#>
				<cfelse>
                	<cfset id_EmpresaSP = 'NULL'>
				</cfif>
                <cfif #FORM.id_Empresa# EQ ''>
                    <cfquery name="Empresas" datasource="#session.cnx#">
                        select distinct id_empresa from presupuestos where id_Obra = #Session.id_Obra# and id_CentroCosto = #id_CentroCosto# and id_Frente = #id_Frente# --and id_Partida in(#ValueList(Session.ExplosionInsumos.id_Partida,',')#)
                    </cfquery>
                	<cfset id_Empresas = ListToArray(ValueList(Empresas.id_Empresa,','))>
				<cfelse>
                	<cfset ArrayPrepend(id_Empresas, #FORM.id_Empresa#)>
				</cfif>
                <cfinvoke component="#Application.componentes#.presupuestos" method="GuardaOperacionExplosionInsumosPartidas" returnvariable="RSGuardar"
                	id_Empresas="#id_Empresas#"
                    id_Empresa="#id_EmpresaSP#"
                    id_Obra="#id_Obra#"
                    id_CentroCosto="#id_CentroCosto#"
                    id_Frente="#id_Frente#"
                    id_PartidasEjecucion="#id_PartidasEjecucion#"
                    id_TiposEjecucionPartidas="#id_TiposEjecucionPartidas#"
                    id_PartidasExplosion="#id_PartidasExplosion#"
                    sn_MultiplicaFactorFrente="1"
                    cl_Operacion="#operacion#"
                >
                <cfif #RSGuardar.tipoError# NEQ "">
                    <cf_error_manejador tipoError="#RSGuardar.tipoError#" mensajeError="#RSGuardar.mensaje#">
                </cfif>
            </cfcase>

            <!--- frentes --->
            <cfcase value="frentes">
                <cfset id_FrentesEjecucion= arrayNew(1)>
                <cfset id_TiposEjecucionFrentes= arrayNew(1)>
                <cfset id_FrentesExplosion= arrayNew(1)>
                <cfset id_Empresas= arrayNew(1)>

                <cfinvoke component="#Application.componentes#.presupuestos" method="RSMostrarFrentesConTotal" returnvariable="RS_MostrarFrentes"
                    id_Empresa="#id_Empresa#"
                    id_Obra="#id_Obra#"
                    id_CentroCosto="#id_CentroCosto#"
                >
                <cfloop query="RS_MostrarFrentes.rs">
                    <cfif isDefined("id_TipoEjecucion_#id_CentroCosto##id_Frente#")>
                        <cfset ArrayPrepend(id_FrentesEjecucion, #RS_MostrarFrentes.rs.id_Frente#)>
                        <cfset ArrayPrepend(id_TiposEjecucionFrentes, Evaluate("id_TipoEjecucion_#id_CentroCosto##id_Frente#"))>
                    </cfif>
                    <cfif isDefined("chk_#RS_MostrarFrentes.rs.id_CentroCosto##RS_MostrarFrentes.rs.id_Frente#")>
                        <cfset ArrayPrepend(id_FrentesExplosion, #RS_MostrarFrentes.rs.id_Frente#)>
                    </cfif>
                </cfloop>
                <cfif operacion EQ 'cambiar_empresa'>
                	<cfset id_EmpresaSP = #id_EmpresaNueva#>
				<cfelse>
                	<cfset id_EmpresaSP = 'NULL'>
				</cfif>
                <cfif #FORM.id_Empresa# EQ ''>
                    <cfquery name="Empresas" datasource="#session.cnx#">
                        select distinct id_empresa from presupuestos where id_Obra = #Session.id_Obra# and id_CentroCosto = #id_CentroCosto# --and id_Frente in(#ValueList(Session.ExplosionInsumos.id_Frente,',')#)
                    </cfquery>
                	<cfset id_Empresas = ListToArray(ValueList(Empresas.id_Empresa,','))>
				<cfelse>
                	<cfset ArrayPrepend(id_Empresas, #FORM.id_Empresa#)>
				</cfif>
                <cfinvoke component="#Application.componentes#.presupuestos" method="GuardaOperacionExplosionInsumosFrentes" returnvariable="RSGuardar"
                	id_Empresas="#id_Empresas#"
                    id_Empresa="#id_EmpresaSP#"
                    id_Obra="#id_Obra#"
                    id_CentroCosto="#id_CentroCosto#"
                    id_FrentesEjecucion="#id_FrentesEjecucion#"
                    id_TiposEjecucionFrentes="#id_TiposEjecucionFrentes#"
                    id_FrentesExplosion="#id_FrentesExplosion#"
                    sn_MultiplicaFactorFrente="1"
                    cl_Operacion="#operacion#"
                >
                <cfif #RSGuardar.tipoError# NEQ "">
                    <cf_error_manejador tipoError="#RSGuardar.tipoError#" mensajeError="#RSGuardar.mensaje#">
                </cfif>
            </cfcase>

            <!--- centros --->
            <cfcase value="centros">
                <cfset id_CentrosEjecucion= arrayNew(1)>
                <cfset id_TiposEjecucionCentros= arrayNew(1)>
                <cfset id_CentrosExplosion= arrayNew(1)>
                <cfset id_Empresas= arrayNew(1)>

                <cfinvoke component="#Application.componentes#.presupuestos" method="RSMostrarCentroCostoConTotal" returnvariable="RS_MostrarCentros"
                    id_Empresa="#id_Empresa#"
                    id_Obra="#Session.id_Obra#"
                >
                <cfif #RS_MostrarCentros.tipoError# NEQ "">
                    <cf_error_manejador tipoError="#RS_MostrarCentros.tipoError#" mensajeError="#RS_MostrarCentros.mensaje#">
                </cfif>

                <cfloop query="RS_MostrarCentros.rs">
					<cfif isDefined("id_TipoEjecucion_#RS_MostrarCentros.rs.id_CentroCosto#")>
                        <cfset ArrayPrepend(id_CentrosEjecucion, #RS_MostrarCentros.rs.id_CentroCosto#)>
                        <cfset ArrayPrepend(id_TiposEjecucionCentros, Evaluate("id_TipoEjecucion_#RS_MostrarCentros.rs.id_CentroCosto#"))>
                    </cfif>
                    <cfif isDefined("chk_#RS_MostrarCentros.rs.id_CentroCosto#")>
                        <cfset ArrayPrepend(id_CentrosExplosion, #RS_MostrarCentros.rs.id_CentroCosto#)>
                    </cfif>
                </cfloop>
                <cfif operacion EQ 'cambiar_empresa'>
                	<cfset id_EmpresaSP = #id_EmpresaNueva#>
				<cfelse>
                	<cfset id_EmpresaSP = 'NULL'>
				</cfif>
                <cfif #FORM.id_Empresa# EQ ''>
                    <cfquery name="Empresas" datasource="#session.cnx#">
                        select distinct id_empresa from presupuestos where id_Obra = #Session.id_Obra# --and id_CentroCosto in(#ValueList(Session.ExplosionInsumos.id_CentroCosto,',')#)
                    </cfquery>
                	<cfset id_Empresas = ListToArray(ValueList(Empresas.id_Empresa,','))>
				<cfelse>
                	<cfset ArrayPrepend(id_Empresas, #FORM.id_Empresa#)>
				</cfif>
                <cfinvoke component="#Application.componentes#.presupuestos" method="GuardaOperacionExplosionInsumosCentros" returnvariable="RSGuardar"
                    id_Empresas="#id_Empresas#"
                    id_Empresa="#id_EmpresaSP#"
                    id_Obra="#id_Obra#"
                    id_CentrosEjecucion="#id_CentrosEjecucion#"
                    id_TiposEjecucionCentros="#id_TiposEjecucionCentros#"
                    id_CentrosExplosion="#id_CentrosExplosion#"
                    sn_MultiplicaFactorFrente="1"
                    cl_Operacion="#operacion#"
                >
                <cfif #RSGuardar.tipoError# NEQ "">
                    <cf_error_manejador tipoError="#RSGuardar.tipoError#" mensajeError="#RSGuardar.mensaje#">
                </cfif>
            </cfcase>
        </cfswitch><!--- nivel --->
    </cfoutput>
    <cfif isDefined('Session.ExplosionInsumos')>
    	<cfset StructDelete(Session, "ExplosionInsumos")>
    </cfif>
    <cf_location_msg url="presupuestos_explosiones.cfm" operacion="operacion">
</cfif>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<link rel="stylesheet" href="../css/style.css" type="text/css">
<link rel="stylesheet" type="text/css" href="../css/box.css">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Presupuestos</title>
<script language="javascript" src="../js/jquery.js"></script>
<script language="javascript" src="../js/funciones.js"></script>
<script language="javascript" src="../js/funciones_ajax.js"></script>
<script language="javascript">

	function mostrar_detalle(empresa,obra,centrocosto,frente,partida,tarjeta,nd,nivel,radio)
	{
		if($x('tr_'+centrocosto+frente+partida).style.display == 'none')
		{
			$x('tr_'+centrocosto+frente+partida).style.display='';
			cargar_pagina('presupuestos_explosiones_'+nivel+'.cfm?id_Empresa='+empresa+'&id_Obra='+obra+'&id_CentroCosto='+centrocosto+'&id_Frente='+frente+'&id_Partida='+partida+'&id_Tarjeta='+tarjeta+'&nd_Prespuesto='+nd+'&nivel='+nivel,$x('div_'+centrocosto+frente+partida),'loader.gif');
		}
		else
		{
			$x('tr_'+centrocosto+frente+partida).style.display='none';
			radio.checked = false;
		}
	}
	function cambiar_ejecucion(centrocosto,frente,partida,valor)
	{
		$('input[class^='+valor+'_ejecucion_'+centrocosto+frente+partida+']').attr('checked', true);
	}

	function cambiar_checado(checar,centrocosto,frente,partida,clase)
	{
		switch(clase)
		{
			case 'tarjetas':
				$('input[class='+clase+'_'+centrocosto+frente+partida+']').attr('checked', checar);
			break;
			case 'partidas':
				$('input[class='+clase+'_'+centrocosto+frente+']').attr('checked', checar);
				$('input[class^=tarjetas_'+centrocosto+frente+']').attr('checked', checar);
			break;
			case 'frentes':
				$('input[class='+clase+'_'+centrocosto+']').attr('checked', checar);
				$('input[class^=partidas_'+centrocosto+']').attr('checked', checar);
				$('input[class^=tarjetas_'+centrocosto+']').attr('checked', checar);
			break;
			case 'centros':
				$('input[class='+clase+']').attr('checked', checar);
				$('input[class^=frentes]').attr('checked', checar);
				$('input[class^=partidas]').attr('checked', checar);
				$('input[class^=tarjetas]').attr('checked', checar);
			break;
		}
	}

	function operaciones(operacion,texto)
	{
		$x('operacion').value = operacion;
		$('button[class=boton_imagen]').attr('disabled', true);
		$('select[name=id_EmpresaCambio]').attr('disabled', true);
		$x('imgCargando').style.visibility= 'visible';
		$('#lblCargando').html(texto+', por favor espere...');
		$x('lblCargando').style.visibility= 'visible';
		document.form1.submit();
	}

	function guardar(opcion)
	{
		switch (opcion)
		{
			case 'guardar_ejecucion':
				if (confirm('Esta seguro que desea guardar el tipo de ejecucion?'))
					operaciones(opcion,'Guardando tipos de ejecucion');
			break;
			case 'explosion':
				if ($('input[type="checkbox"][name^="chk_"]:checked').length > 0)
				{
					if(confirm('Esta seguro que desea realizar la explosion de insumos?'))
						operaciones(opcion,'Generando explosion');
				}
				else
					alert('Debe seleccionar para realizar la operacion');
			break;
			case 'desexplosion':
				if ($('input[type="checkbox"][name^="chk_"]:checked').length > 0)
				{
					if (confirm('Esta seguro que desea realizar la desexplosion de insumos?'))
						operaciones(opcion,'Generando Desexplosion');
				}
				else
					alert('Debe seleccionar para realizar la operacion');
			break;
			case 'cambiar_empresa':
			if ($('input[type="checkbox"][name^="chk_"]:checked').length > 0)
			{
				if($x('id_EmpresaCambio').value != '')
					if(confirm('Esta seguro que desea cambiar la empresa?'))
						operaciones(opcion,'Cambiando empresa');
			}
			else
			{	alert('Debe seleccionar para realizar la operacion');$x('id_EmpresaCambio').value = ''}
			break;
		}
	}
</script>
</head>

<body>
<cfinclude template="menu.cfm">&nbsp;
<table style="width:950px" align="center">
	<tr class="titulo">
		<td align="justify">EXPLOSI&Oacute;N DE PRESUPUESTOS EN <cfoutput>#Session.de_Obra#</cfoutput></td>
	</tr>
</table>
<br>
<cfform name="form1" id="form1" action="#CurrentPage#" preservedata="yes" method="post" enctype="multipart/form-data">
    <table width="950px" cellpadding="0" cellspacing="0" align="center">
        <tr><td class="x-box-tl" width="1px"></td><td class="x-box-tc"></td><td class="x-box-tr" width="1px"></td></tr>
        <tr><td class="x-box-ml"></td><td class="x-box-mc" width="99%">
            <table width="950px" align="left">
                <tr>
                    <td align="right">Empresa:</td>
                    <td align="left">
                        <cfselect name="id_Empresa" value="id_Empresa" display="nb_Empresa" style="width:300px" query="RSEmpresa.rs" selected="#id_Empresa#"
                            class="contenido" message="Seleccione una Empresa" title="Seleccione una Empresa" queryPosition="below">
                            <option value="">TODAS</option>
                        </cfselect>
                        <button type="submit" class="boton_imagen"><img src="../images/filtro_azul.png" alt="Clic para buscar" ></button>
                    </td>
                </tr>
            </table>
            </td><td class="x-box-mr"></td></tr><tr><td class="x-box-bl" width="1px"></td><td class="x-box-bc"></td><td class="x-box-br" width="1px"></td>
        </tr>
    </table><br />
	<table width="950px" cellpadding="0" cellspacing="0" align="center">
    	<tr>
    		<td align="center">
        		<button id="btn_Guardar" type="button" class="boton_imagen" onClick="guardar('guardar_ejecucion');"><img src="../images/guardar.png" border="0"/></button>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <button id="btn_Explosion" type="button" class="boton_imagen" onClick="guardar('explosion');"><img src="../images/explosion.png" border="0"/></button>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <label>Cambiar Empresa: </label>
                <cfselect id="id_EmpresaCambio" name="id_EmpresaCambio" style="font-size:12px; width:auto;" onChange="$x('id_EmpresaNueva').value = this.value;guardar('cambiar_empresa');" class="contenido"
                query="RSEmpresa.rs" value="id_Empresa" display="nb_Empresa" queryPosition="below">
                    <option value="" >SELECCIONE UNA EMPRESA</option>
                </cfselect>
				<cfinput type="hidden" name="id_EmpresaNueva" id="id_EmpresaNueva" value="">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <button id="btn_DesExplosion" type="button" class="boton_imagen" onClick="guardar('desexplosion');"><img src="../images/desexplosion.png" border="0"/></button>
                <cfinput type="hidden" name="operacion" id="operacion" value="">
			</td>
        </tr>
        <br />
    	<tr>
    		<td>
            	<div align="center" style="width:950px">
                    <img src="../images/cargando.gif" border="0" id="imgCargando" style="visibility:hidden"/>
                    <span id="lblCargando" class="error" style="visibility:collapse"></span>
				</div>
			</td>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" width="950px" class="fondo_listado" align="center">
        <tr class="encabezado">
            <td width="3px"><img src="../images/esquina_izquierda.gif"></td>
            <td width="1%">Clave</td>
            <td align="center">Centros de Costos</td>
            <td width="60px">Ejecuci&oacute;n</td>
            <td align="center" width="20%" colspan="4">Total</td>
            <td width="1%" align="center">
				<input type="checkbox" onClick="cambiar_checado(this.checked,'','','','centros');" title="Checar todos los Centros de Costo"/>
            </td>
            <td></td>
            <td width="3px"><img src="../images/esquina_derecha.gif"></td>
        </tr>
        <cfoutput query="RS_MostrarCentros.rs">
            <tr onMouseOver="this.style.backgroundColor='##99CCCC'" onMouseOut="this.style.backgroundColor=''">
                <td background="../images/orilla_izquierda.gif" style="background-repeat:repeat-y" width="3px"></td>
                <td class="cuadricula" align="center">#RS_MostrarCentros.rs.id_CentroCosto#</td>
                <td class="cuadricula">
                    <input type="radio" name="id_CentroCosto_#id_CentroCosto#" value="#RS_MostrarCentros.rs.id_CentroCosto#"
                    onclick="mostrar_detalle($x('id_Empresa').value,#RS_MostrarCentros.rs.id_Obra#,#RS_MostrarCentros.rs.id_CentroCosto#,'','','','','frentes',this);"
                    >#RS_MostrarCentros.rs.de_CentroCosto#
                </td>
                <td class="cuadricula" nowrap="nowrap" align="center">
                    <input type="radio" class="0_ejecucion_#id_CentroCosto#" onClick="cambiar_ejecucion(#id_CentroCosto#,'','',this.value)" name="id_TipoEjecucion_#RS_MostrarCentros.rs.id_CentroCosto#" id="0id_TipoEjecucion_#RS_MostrarCentros.rs.id_CentroCosto#" value="0" /> M
                    <input type="radio" class="1_ejecucion_#id_CentroCosto#" onClick="cambiar_ejecucion(#id_CentroCosto#,'','',this.value)" name="id_TipoEjecucion_#RS_MostrarCentros.rs.id_CentroCosto#" id="1id_TipoEjecucion_#RS_MostrarCentros.rs.id_CentroCosto#" value="1" /> D
                    <input type="radio" class="2_ejecucion_#id_CentroCosto#" onClick="cambiar_ejecucion(#id_CentroCosto#,'','',this.value)" name="id_TipoEjecucion_#RS_MostrarCentros.rs.id_CentroCosto#" id="2id_TipoEjecucion_#RS_MostrarCentros.rs.id_CentroCosto#" value="2" /> S
                </td>
                <td class="cuadricula" align="right" colspan="4">$#NumberFormat(RS_MostrarCentros.rs.im_SubTotal,",_.____")#</td>
                <td width="3%" class="cuadricula" align="center">
                    <input type="checkbox" class="centros" name="chk_#RS_MostrarCentros.rs.id_CentroCosto#"/>
                </td>
                <td width="1%" class="cuadricula" align="center">
                    <cfif #RS_MostrarCentros.rs.sn_Explosion# EQ 1>
                        &nbsp;E&nbsp;
                    </cfif>
                </td>
                <td background="../images/orilla_derecha.gif" style="background-repeat:repeat-y" width="3px"></td>
            </tr>

            <tr id="tr_#RS_MostrarCentros.rs.id_CentroCosto#" style="display:none;">
                <td background="../images/orilla_izquierda.gif" style="background-repeat:repeat-y" width="3px"></td>
                <td colspan="15">
                    <div id="div_#RS_MostrarCentros.rs.id_CentroCosto#" style="width:100%" align="center"></div>
                </td>
                <td background="../images/orilla_derecha.gif" style="background-repeat:repeat-y" width="3px"></td>
             </tr>
		</cfoutput>
        <cfif #RSTotalExplosion.rs.Total# EQ '' >
			<cfset TotalExplosion = 0 >
		<cfelse>
			<cfset TotalExplosion = #RSTotalExplosion.rs.Total# >
		</cfif>
        <cfquery name="im_Indirectos" datasource="#session.cnx#">
        	select sum(im_Traspasado) as im_Traspasado from Presupuestos_Indirectos where id_ObraOrigen = #Session.id_Obra#
        </cfquery>
        <cfset im_Indirecto = #iif(im_Indirectos.im_Traspasado eq '',0,im_Indirectos.im_Traspasado)#>
        <tr style="font-weight:bold">
            <td background="../images/orilla_izquierda.gif" style="background-repeat:repeat-y" width="3px"></td>
            <td align="right" colspan="6">TOTAL PRESUPUESTADO:</td>
            <td align="right"><span id="lblTotalPresupuesto">$<cfoutput>#NumberFormat(RSTotalPresupuesto.rs.im_SubTotal,",_.____")#</cfoutput></span></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td background="../images/orilla_derecha.gif" style="background-repeat:repeat-y" width="3px"></td>
        </tr>
        <tr style="font-weight:bold">
            <td align="right" colspan="7">TOTAL EXPLOSIONADO:</td>
            <td align="right"><span id="lblTotalExplosion">$<cfoutput>#NumberFormat(RSTotalExplosion.rs.Total,",_.____")#</cfoutput></span></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr style="font-weight:bold" >
            <td align="right" colspan="7" >DIFERENCIA:</td>
            <cfif RSTotalPresupuesto.rs.im_SubTotalPorFactor EQ '' >
                <cfset im_SubTotalPorFactor = 0>
                <cfset TotalExplosion = 0 >
                <cfelse>
                <cfset im_SubTotalPorFactor = #RSTotalPresupuesto.rs.im_SubTotalPorFactor#>
            </cfif>

            <cfset im_SubTotal = IIF(#RSTotalPresupuesto.rs.im_SubTotal# EQ '',0,#RSTotalPresupuesto.rs.im_SubTotal#)>
            <!--- <cfset im_SubTotal = #im_SubTotal# - #im_Indirecto#> --->
            <td align="right" ><span id="lblTotalExplosion" style="border-top:solid;border-top-color:#666666">$<cfoutput>#NumberFormat(abs(im_SubTotal - TotalExplosion),",_.____")#</cfoutput></span></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <!--- <tr style="font-weight:bold" >
        	<td align="right" colspan="7" >Traspaso de indirectos:</td>
            <td align="right" ><span id="ll">$<cfoutput>#NumberFormat(im_Indirecto,",_.____")#</cfoutput></span></td>
        </tr> --->
		<tr>
			<td background="../images/orilla_izquierda.gif" style="background-repeat:repeat-y" width="3px"></td>
			<td align="center" colspan="10"></td>
			<td background="../images/orilla_derecha.gif" style="background-repeat:repeat-y" width="3px"></td>
		</tr>
		<tr style="background-image:url(../images/abajo.gif)">
			<td width="3px"><img src="../images/abajo_izquierda.gif"></td>
			<td colspan="10"></td>
			<td width="3px"><img src="../images/abajo_derecha.gif"></td>
		</tr>
    </table>
    <br /><br />

</cfform>
</body>
</html>
