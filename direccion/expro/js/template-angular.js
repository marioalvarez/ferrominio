﻿var app = angular.module('rrExpro',[]);

/* CONTROLLERS */

	app.controller('ProveedoresController', function($scope,FabricaExpediente){

		// Declaraciones
		FabricaExpediente.obtenerProveedores();
		$scope.proveedores = FabricaExpediente.proveedores;
		$scope.proveedoresMostrar = false;

		// Funciones
		$scope.proveedoresFiltroLimpiar = function(){
			$scope.proveedoresMostrar = false;
			$scope.proveedoresFiltro = '';
			FabricaExpediente.quitarAnios();
			FabricaExpediente.quitarExpedientes();
		}
		$scope.proveedoresInputCambio = function(){

			$scope.proveedoresMostrar = true;

			if($scope.proveedoresFiltro == ''){
				$scope.proveedoresMostrar = false;
				FabricaExpediente.quitarAnios();
				FabricaExpediente.quitarExpedientes();
			}
		}
		$scope.mostrarAnios = function(objProveedor){
			$scope.proveedoresFiltro = objProveedor.nombre;
			FabricaExpediente.obtenerAnios(objProveedor);
		}
		

	});
	app.controller('AniosController', function($scope,FabricaExpediente){

		// Declaraciones
		$scope.anios = FabricaExpediente.anios;
		$scope.aniosContenedor = FabricaExpediente.banderas;
		$scope.aniosLimite = 5;

		// Funciones
		$scope.aniosFiltroLimpiar = function(){
			$scope.aniosFiltro = '';
			FabricaExpediente.quitarExpedientes();
		}
		$scope.aniosInputCambio = function(){
			if($scope.aniosFiltro == ''){
				FabricaExpediente.quitarExpedientes();
			}
		}
		$scope.mostrarExpedientes = function(objAnio){
			$scope.aniosFiltro = objAnio.anio;
			FabricaExpediente.obtenerExpedientes(objAnio);
		}

	});
	
	app.controller('ExpedientesController', function($scope,FabricaExpediente){

		$scope.expedientes = FabricaExpediente.expedientes;

		$scope.expedientesContenedor = FabricaExpediente.banderas;

	});
	

/* FACTORY */

	app.factory('FabricaExpediente', function (ServicioProveedores,ServicioAnios,ServicioExpedientes) {
		var fabrica = {

			proveedor : '',
			proveedores : {
				registros : ''
			},
			anios : {
				registros : ''
			},
			expedientes : {
				registros : ''
			},
			banderas : {
				aniosMostrar : '',
				expedientesMostrar : ''
			},
			obtenerProveedores : function(){
				
				ServicioProveedores.obtenerProveedores().
					success(function(data){
						fabrica.proveedores.registros = data;
					}).
					error(function(data){
						alert('Se produjo un error al obtener proveedores, consultarlo con soporte');
					});
				
			},
			obtenerAnios : function(objProveedor){
				
				fabrica.proveedor = objProveedor;
				ServicioAnios.obtenerAnios(objProveedor.id).
					success(function(data){
						fabrica.anios.registros = data;
					}).
					error(function(data){
						alert('Se produjo un error al obtener los años, consultarlo con soporte');
					});
				fabrica.banderas.aniosMostrar = true;

			},
			quitarAnios : function(){
				fabrica.proveedor = '';
				fabrica.anios.registros = '';
				fabrica.banderas.aniosMostrar = false;
			},
			obtenerExpedientes : function(objAnio){
				
				ServicioExpedientes.obtenerExpedientes(fabrica.proveedor.id,objAnio.anio).
					success(function(data){
						fabrica.expedientes.registros = data;
					}).
					error(function(data){
						alert('Se produjo un error al obtener los expedientes, consultarlo con soporte');
					});
				fabrica.banderas.expedientesMostrar = true;

			},
			quitarExpedientes : function(){
				fabrica.expedientes.registros = '';
				fabrica.banderas.expedientesMostrar = false;	
			}

		};
		return fabrica;
	});


/* SERVICIOS */

	app.service('ServicioProveedores',function($http){
	    this.obtenerProveedores = function(){

	    	return $http.get('../../componentes/proveedoresexp.cfc?method=obtenerProveedores');
	  
	    }
	});

	app.service('ServicioAnios',function($http){
		this.obtenerAnios = function(id){

			return $http.get('../../componentes/proveedoresexp.cfc?method=obteneranios&id_proveedor='+id);

		}
	});

	app.service('ServicioExpedientes',function($http){
	    this.obtenerExpedientes = function(id,anio){

	    	return $http.get('../../componentes/proveedoresexp.cfc?method=obtenerExpedientes&id_proveedor='+id+'&anio='+anio);

	    }
	});
