/* Document JavaScript */

// VARIABLES

	// Velocidades de Efectos 1000 = 1s
	var instante = 0;
	var velocidad = 300;

	// Tamaños de pantalla
	var width_actual = $(window).width();
	var height_actual = $(window).height(); 


// FUNCIONES 

	// Funcion inicial ////////////////////////////////
	$(document).ready(function() {
		mac_si_no();
		mobile_si_no();
		actualizar_medidas();
	});
	///////////////////////////////////////////////////

	// Funcion medidas del navegador //////////////////
	$(window).resize(function() {
		mac_si_no();
		mobile_si_no();
		actualizar_medidas();
	});
	///////////////////////////////////////////////////

	// Funcion mobile_si_no ///////////////////////////
	function mobile_si_no(){
		if(width_actual >= 767){
			$('section').removeClass('mobile');
			$('section').addClass('desktop');
		}else{
			$('section').removeClass('desktop');
			$('section').addClass('mobile');
		}
	}
	///////////////////////////////////////////////////

	// Funcion actualizar_medidas /////////////////////
	function actualizar_medidas(){
		width_actual = $(window).width();
		height_actual = $(window).height();
	}
	///////////////////////////////////////////////////

	// Funcion If Mac /////////////////////////////////
	function mac_si_no(){
		if (navigator.userAgent.indexOf('Mac OS X') != -1) {
			$("body").addClass("mac");
		}else{
		  	$("body").addClass("pc");
		}
	}
	///////////////////////////////////////////////////

