/* Document JS */
var app = angular.module('rrDireccion',['ngRoute','rrmDesglose','rrmExpro','rrmRCP','rrmMonitor']);

	app.config(['$routeProvider', function($routeProvider) {
		$routeProvider.
			when('/', {
				templateUrl: 'reportes/reportes.html'
			}).
			when('/configuracion', {
				templateUrl: 'reportes/configuracion.html'
			}).
			when('/desglose', {
				templateUrl: 'reportes/desglose/index.html'
			}).
			when('/expro', {
				templateUrl: 'reportes/expro/index.html'
			}).
			when('/rcp', {
				templateUrl: 'reportes/rcp/index.html'
			}).
			when('/flujo', {
				templateUrl: 'reportes/flujo/index.html'
			}).
			when('/monitor', {
				templateUrl: 'reportes/monitor/index.html'
			}).
			otherwise('/');
  	}]);

/* CONTROLLER */
	app.controller('direccionController',function($scope, $location){

		// CangeView
		$scope.changeView = function(view){
            $location.path(view); // path not hash
        }

	});

	app.controller('configuracionController', function($scope){
		
		$scope.correos_desglose = [{correo:'sergio_mcr91@hotmail.com'},{correo:'mario@hotmail.com'},{correo:'carreon@hotmail.com'},{correo:'roman@hotmail.com'}];
		$scope.correos_expro = [{correo:'sergio_mcr91@hotmail.com'},{correo:'mario@hotmail.com'},{correo:'carreon@hotmail.com'},{correo:'roman@hotmail.com'}];
		
		$scope.agregar_registro = function(lista,input){
			
		}

		$scope.borrar_registro = function(lista,index){
			alert(lista+'-'+index);
		}

	});
