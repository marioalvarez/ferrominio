<?php

$centros = '
	[
    	{
    		"centro" : 1,
            "nombre" : "hola mundo",
            "frentes" : [
                {
                    "frente" : 1,
                    "partidas" : [
                        {
                            "partida" : 1,
                            "datos" : [
                                {
                                    "codigo" : 1010101,
                                    "insumo" : "Vainilla Vainilla Vainilla Vainilla Vainilla Vainilla Vainilla Vainilla",
                                    "um" : "Pieza",
                                    "cant_ppto_base" : 10,
                                    "aditivas" : 4,
                                    "deductivas" : 6,
                                    "ppto_actual" : 12345,
                                    "cant_comprada" : 10,
                                    "por_comprar" : 0,
                                    "cant_recibida" : 8,
                                    "por_recibir" : 0,
                                    "cant_aplicada" : 8,
                                    "cant_almacen" : 67890
                                },
                                {
                                    "codigo" : 1010102,
                                    "insumo" : "Vainilla",
                                    "um" : "Pieza",
                                    "cant_ppto_base" : 10,
                                    "aditivas" : 4,
                                    "deductivas" : 6,
                                    "ppto_actual" : 12345,
                                    "cant_comprada" : 10,
                                    "por_comprar" : 0,
                                    "cant_recibida" : 8,
                                    "por_recibir" : 0,
                                    "cant_aplicada" : 8,
                                    "cant_almacen" : 67890
                                },
                                {
                                    "codigo" : 1010103,
                                    "insumo" : "Vainilla",
                                    "um" : "Pieza",
                                    "cant_ppto_base" : 10,
                                    "aditivas" : 4,
                                    "deductivas" : 6,
                                    "ppto_actual" : 12345,
                                    "cant_comprada" : 10,
                                    "por_comprar" : 0,
                                    "cant_recibida" : 8,
                                    "por_recibir" : 0,
                                    "cant_aplicada" : 8,
                                    "cant_almacen" : 67890
                                }
                            ]
                        },
                        {
                            "partida" : 2,
                            "datos" : [
                                {
                                    "codigo" : 1010201,
                                    "insumo" : "Chocolate",
                                    "um" : "Pieza",
                                    "cant_ppto_base" : 20,
                                    "aditivas" : 14,
                                    "deductivas" : 6,
                                    "ppto_actual" : 12345,
                                    "cant_comprada" : 20,
                                    "por_comprar" : 0,
                                    "cant_recibida" : 15,
                                    "por_recibir" : 0,
                                    "cant_aplicada" : 13,
                                    "cant_almacen" : 67890
                                }
                            ]
                        },
                        {
                            "partida" : 3,
                            "datos" : [
                                {
                                    "codigo" : 1010301,
                                    "insumo" : "Limon",
                                    "um" : "Pieza",
                                    "cant_ppto_base" : 30,
                                    "aditivas" : 4,
                                    "deductivas" : 26,
                                    "ppto_actual" : 12345,
                                    "cant_comprada" : 30,
                                    "por_comprar" : 0,
                                    "cant_recibida" : 8,
                                    "por_recibir" : 0,
                                    "cant_aplicada" : 8,
                                    "cant_almacen" : 67890
                                }
                            ]
                        }
                    ]
                },
                {
                    "frente" : 2,
                    "partidas" : [
                        {
                            "partida" : 1,
                            "datos" : [
                                {
                                    "codigo" : 1020101,
                                    "insumo" : "Vainilla",
                                    "um" : "Pieza",
                                    "cant_ppto_base" : 10,
                                    "aditivas" : 4,
                                    "deductivas" : 6,
                                    "ppto_actual" : 12345,
                                    "cant_comprada" : 10,
                                    "por_comprar" : 0,
                                    "cant_recibida" : 8,
                                    "por_recibir" : 0,
                                    "cant_aplicada" : 8,
                                    "cant_almacen" : 67890
                                }
                            ]
                        },
                        {
                            "partida" : 2,
                            "datos" : [
                                {
                                    "codigo" : 1020201,
                                    "insumo" : "Chocolate",
                                    "um" : "Pieza",
                                    "cant_ppto_base" : 20,
                                    "aditivas" : 14,
                                    "deductivas" : 6,
                                    "ppto_actual" : 12345,
                                    "cant_comprada" : 20,
                                    "por_comprar" : 0,
                                    "cant_recibida" : 15,
                                    "por_recibir" : 0,
                                    "cant_aplicada" : 13,
                                    "cant_almacen" : 67890
                                }
                            ]
                        },
                        {
                            "partida" : 3,
                            "datos" : [
                                {
                                    "codigo" : 1020301,
                                    "insumo" : "Limon",
                                    "um" : "Pieza",
                                    "cant_ppto_base" : 30,
                                    "aditivas" : 4,
                                    "deductivas" : 26,
                                    "ppto_actual" : 12345,
                                    "cant_comprada" : 30,
                                    "por_comprar" : 0,
                                    "cant_recibida" : 8,
                                    "por_recibir" : 0,
                                    "cant_aplicada" : 8,
                                    "cant_almacen" : 67890
                                }
                            ]
                        }
                    ]
                },
                {
                    "frente" : 3,
                    "partidas" : [
                        {
                            "partida" : 1,
                            "datos" : [
                                {
                                    "codigo" : 1030101,
                                    "insumo" : "Vainilla",
                                    "um" : "Pieza",
                                    "cant_ppto_base" : 10,
                                    "aditivas" : 4,
                                    "deductivas" : 6,
                                    "ppto_actual" : 12345,
                                    "cant_comprada" : 10,
                                    "por_comprar" : 0,
                                    "cant_recibida" : 8,
                                    "por_recibir" : 0,
                                    "cant_aplicada" : 8,
                                    "cant_almacen" : 67890
                                }
                            ]
                        },
                        {
                            "partida" : 2,
                            "datos" : [
                                {
                                    "codigo" : 1030201,
                                    "insumo" : "Chocolate",
                                    "um" : "Pieza",
                                    "cant_ppto_base" : 20,
                                    "aditivas" : 14,
                                    "deductivas" : 6,
                                    "ppto_actual" : 12345,
                                    "cant_comprada" : 20,
                                    "por_comprar" : 0,
                                    "cant_recibida" : 15,
                                    "por_recibir" : 0,
                                    "cant_aplicada" : 13,
                                    "cant_almacen" : 67890
                                }
                            ]
                        },
                        {
                            "partida" : 3,
                            "datos" : [
                                {
                                    "codigo" : 1030301,
                                    "insumo" : "Limon",
                                    "um" : "Pieza",
                                    "cant_ppto_base" : 30,
                                    "aditivas" : 4,
                                    "deductivas" : 26,
                                    "ppto_actual" : 12345,
                                    "cant_comprada" : 30,
                                    "por_comprar" : 0,
                                    "cant_recibida" : 8,
                                    "por_recibir" : 0,
                                    "cant_aplicada" : 8,
                                    "cant_almacen" : 67890
                                }
                            ]
                        }
                    ]
                }
            ]
    	},
        {
            "centro" : 2,
            "frentes" : [
                {
                    "frente" : 1,
                    "partidas" : [
                        {
                            "partida" : 1,
                            "datos" : [
                                {
                                    "codigo" : 2010101,
                                    "insumo" : "Vainilla",
                                    "um" : "Pieza",
                                    "cant_ppto_base" : 10,
                                    "aditivas" : 4,
                                    "deductivas" : 6,
                                    "ppto_actual" : 12345,
                                    "cant_comprada" : 10,
                                    "por_comprar" : 0,
                                    "cant_recibida" : 8,
                                    "por_recibir" : 0,
                                    "cant_aplicada" : 8,
                                    "cant_almacen" : 67890
                                }
                            ]
                        },
                        {
                            "partida" : 2,
                            "datos" : [
                                {
                                    "codigo" : 2010201,
                                    "insumo" : "Chocolate",
                                    "um" : "Pieza",
                                    "cant_ppto_base" : 20,
                                    "aditivas" : 14,
                                    "deductivas" : 6,
                                    "ppto_actual" : 12345,
                                    "cant_comprada" : 20,
                                    "por_comprar" : 0,
                                    "cant_recibida" : 15,
                                    "por_recibir" : 0,
                                    "cant_aplicada" : 13,
                                    "cant_almacen" : 67890
                                }
                            ]
                        },
                        {
                            "partida" : 3,
                            "datos" : [
                                {
                                    "codigo" : 2010301,
                                    "insumo" : "Limon",
                                    "um" : "Pieza",
                                    "cant_ppto_base" : 30,
                                    "aditivas" : 4,
                                    "deductivas" : 26,
                                    "ppto_actual" : 12345,
                                    "cant_comprada" : 30,
                                    "por_comprar" : 0,
                                    "cant_recibida" : 8,
                                    "por_recibir" : 0,
                                    "cant_aplicada" : 8,
                                    "cant_almacen" : 67890
                                }
                            ]
                        }
                    ]
                },
                {
                    "frente" : 2,
                    "partidas" : [
                        {
                            "partida" : 1,
                            "datos" : [
                                {
                                    "codigo" : 2020101,
                                    "insumo" : "Vainilla",
                                    "um" : "Pieza",
                                    "cant_ppto_base" : 10,
                                    "aditivas" : 4,
                                    "deductivas" : 6,
                                    "ppto_actual" : 12345,
                                    "cant_comprada" : 10,
                                    "por_comprar" : 0,
                                    "cant_recibida" : 8,
                                    "por_recibir" : 0,
                                    "cant_aplicada" : 8,
                                    "cant_almacen" : 67890
                                }
                            ]
                        },
                        {
                            "partida" : 2,
                            "datos" : [
                                {
                                    "codigo" : 2020201,
                                    "insumo" : "Chocolate",
                                    "um" : "Pieza",
                                    "cant_ppto_base" : 20,
                                    "aditivas" : 14,
                                    "deductivas" : 6,
                                    "ppto_actual" : 12345,
                                    "cant_comprada" : 20,
                                    "por_comprar" : 0,
                                    "cant_recibida" : 15,
                                    "por_recibir" : 0,
                                    "cant_aplicada" : 13,
                                    "cant_almacen" : 67890
                                }
                            ]
                        },
                        {
                            "partida" : 3,
                            "datos" : [
                                {
                                    "codigo" : 2020301,
                                    "insumo" : "Limon",
                                    "um" : "Pieza",
                                    "cant_ppto_base" : 30,
                                    "aditivas" : 4,
                                    "deductivas" : 26,
                                    "ppto_actual" : 12345,
                                    "cant_comprada" : 30,
                                    "por_comprar" : 0,
                                    "cant_recibida" : 8,
                                    "por_recibir" : 0,
                                    "cant_aplicada" : 8,
                                    "cant_almacen" : 67890
                                }
                            ]
                        }
                    ]
                },
                {
                    "frente" : 3,
                    "partidas" : [
                        {
                            "partida" : 1,
                            "datos" : [
                                {
                                    "codigo" : 2030101,
                                    "insumo" : "Vainilla",
                                    "um" : "Pieza",
                                    "cant_ppto_base" : 10,
                                    "aditivas" : 4,
                                    "deductivas" : 6,
                                    "ppto_actual" : 12345,
                                    "cant_comprada" : 10,
                                    "por_comprar" : 0,
                                    "cant_recibida" : 8,
                                    "por_recibir" : 0,
                                    "cant_aplicada" : 8,
                                    "cant_almacen" : 67890
                                }
                            ]
                        },
                        {
                            "partida" : 2,
                            "datos" : [
                                {
                                    "codigo" : 2030201,
                                    "insumo" : "Chocolate",
                                    "um" : "Pieza",
                                    "cant_ppto_base" : 20,
                                    "aditivas" : 14,
                                    "deductivas" : 6,
                                    "ppto_actual" : 12345,
                                    "cant_comprada" : 20,
                                    "por_comprar" : 0,
                                    "cant_recibida" : 15,
                                    "por_recibir" : 0,
                                    "cant_aplicada" : 13,
                                    "cant_almacen" : 67890
                                }
                            ]
                        },
                        {
                            "partida" : 3,
                            "datos" : [
                                {
                                    "codigo" : 2030301,
                                    "insumo" : "Limon",
                                    "um" : "Pieza",
                                    "cant_ppto_base" : 30,
                                    "aditivas" : 4,
                                    "deductivas" : 26,
                                    "ppto_actual" : 12345,
                                    "cant_comprada" : 30,
                                    "por_comprar" : 0,
                                    "cant_recibida" : 8,
                                    "por_recibir" : 0,
                                    "cant_aplicada" : 8,
                                    "cant_almacen" : 67890
                                }
                            ]
                        }
                    ]
                }
            ]
        },
        {
            "centro" : 3,
            "frentes" : [
                {
                    "frente" : 1,
                    "partidas" : [
                        {
                            "partida" : 1,
                            "datos" : [
                                {
                                    "codigo" : 3010101,
                                    "insumo" : "Vainilla",
                                    "um" : "Pieza",
                                    "cant_ppto_base" : 10,
                                    "aditivas" : 4,
                                    "deductivas" : 6,
                                    "ppto_actual" : 12345,
                                    "cant_comprada" : 10,
                                    "por_comprar" : 0,
                                    "cant_recibida" : 8,
                                    "por_recibir" : 0,
                                    "cant_aplicada" : 8,
                                    "cant_almacen" : 67890
                                }
                            ]
                        },
                        {
                            "partida" : 2,
                            "datos" : [
                                {
                                    "codigo" : 3010201,
                                    "insumo" : "Chocolate",
                                    "um" : "Pieza",
                                    "cant_ppto_base" : 20,
                                    "aditivas" : 14,
                                    "deductivas" : 6,
                                    "ppto_actual" : 12345,
                                    "cant_comprada" : 20,
                                    "por_comprar" : 0,
                                    "cant_recibida" : 15,
                                    "por_recibir" : 0,
                                    "cant_aplicada" : 13,
                                    "cant_almacen" : 67890
                                }
                            ]
                        },
                        {
                            "partida" : 3,
                            "datos" : [
                                {
                                    "codigo" : 3010301,
                                    "insumo" : "Limon",
                                    "um" : "Pieza",
                                    "cant_ppto_base" : 30,
                                    "aditivas" : 4,
                                    "deductivas" : 26,
                                    "ppto_actual" : 12345,
                                    "cant_comprada" : 30,
                                    "por_comprar" : 0,
                                    "cant_recibida" : 8,
                                    "por_recibir" : 0,
                                    "cant_aplicada" : 8,
                                    "cant_almacen" : 67890
                                }
                            ]
                        }
                    ]
                },
                {
                    "frente" : 2,
                    "partidas" : [
                        {
                            "partida" : 1,
                            "datos" : [
                                {
                                    "codigo" : 3020101,
                                    "insumo" : "Vainilla",
                                    "um" : "Pieza",
                                    "cant_ppto_base" : 10,
                                    "aditivas" : 4,
                                    "deductivas" : 6,
                                    "ppto_actual" : 12345,
                                    "cant_comprada" : 10,
                                    "por_comprar" : 0,
                                    "cant_recibida" : 8,
                                    "por_recibir" : 0,
                                    "cant_aplicada" : 8,
                                    "cant_almacen" : 67890
                                }
                            ]
                        },
                        {
                            "partida" : 2,
                            "datos" : [
                                {
                                    "codigo" : 3020201,
                                    "insumo" : "Chocolate",
                                    "um" : "Pieza",
                                    "cant_ppto_base" : 20,
                                    "aditivas" : 14,
                                    "deductivas" : 6,
                                    "ppto_actual" : 12345,
                                    "cant_comprada" : 20,
                                    "por_comprar" : 0,
                                    "cant_recibida" : 15,
                                    "por_recibir" : 0,
                                    "cant_aplicada" : 13,
                                    "cant_almacen" : 67890
                                }
                            ]
                        },
                        {
                            "partida" : 3,
                            "datos" : [
                                {
                                    "codigo" : 3020301,
                                    "insumo" : "Limon",
                                    "um" : "Pieza",
                                    "cant_ppto_base" : 30,
                                    "aditivas" : 4,
                                    "deductivas" : 26,
                                    "ppto_actual" : 12345,
                                    "cant_comprada" : 30,
                                    "por_comprar" : 0,
                                    "cant_recibida" : 8,
                                    "por_recibir" : 0,
                                    "cant_aplicada" : 8,
                                    "cant_almacen" : 67890
                                }
                            ]
                        }
                    ]
                },
                {
                    "frente" : 3,
                    "partidas" : [
                        {
                            "partida" : 1,
                            "datos" : [
                                {
                                    "codigo" : 3030101,
                                    "insumo" : "Vainilla",
                                    "um" : "Pieza",
                                    "cant_ppto_base" : 10,
                                    "aditivas" : 4,
                                    "deductivas" : 6,
                                    "ppto_actual" : 12345,
                                    "cant_comprada" : 10,
                                    "por_comprar" : 0,
                                    "cant_recibida" : 8,
                                    "por_recibir" : 0,
                                    "cant_aplicada" : 8,
                                    "cant_almacen" : 67890
                                }
                            ]
                        },
                        {
                            "partida" : 2,
                            "datos" : [
                                {
                                    "codigo" : 3030201,
                                    "insumo" : "Chocolate",
                                    "um" : "Pieza",
                                    "cant_ppto_base" : 20,
                                    "aditivas" : 14,
                                    "deductivas" : 6,
                                    "ppto_actual" : 12345,
                                    "cant_comprada" : 20,
                                    "por_comprar" : 0,
                                    "cant_recibida" : 15,
                                    "por_recibir" : 0,
                                    "cant_aplicada" : 13,
                                    "cant_almacen" : 67890
                                }
                            ]
                        },
                        {
                            "partida" : 3,
                            "datos" : [
                                {
                                    "codigo" : 3030301,
                                    "insumo" : "Limon",
                                    "um" : "Pieza",
                                    "cant_ppto_base" : 30,
                                    "aditivas" : 4,
                                    "deductivas" : 26,
                                    "ppto_actual" : 12345,
                                    "cant_comprada" : 30,
                                    "por_comprar" : 0,
                                    "cant_recibida" : 8,
                                    "por_recibir" : 0,
                                    "cant_aplicada" : 8,
                                    "cant_almacen" : 67890
                                }
                            ]
                        }
                    ]
                }
            ]
        }
	]
';

echo $centros;

?>