var app = angular.module('rrRCP',[]);

/* CONTROLLERS */

	app.controller('CentrosController', function($scope,FabricaCentros){

		// Declaraciones
		$scope.centros = FabricaCentros.centros;
		
		$scope.buscar_centro = function(){
			$scope.titulo = $scope.centro;
			FabricaCentros.obtenerCentros($scope.centro);
		}

		$scope.mostrar_nivel2 = function(centro){
			console.log('.centro-'+centro);
			$('.centro-'+centro).toggle();
		}
		
		$scope.mostrar_nivel3 = function(centro,frente){
			console.log('.centro-'+centro+'-frente-'+frente);
			$('.centro-'+centro+'-frente-'+frente).toggle();
		}
	
		$scope.mostrar_tabla = function(centro,frente,partida){
			$('.centro-'+centro+'-frente-'+frente+'-partida-'+partida).toggle();
		}

		$scope.mostrar_detalle = function(clase){
			$(clase+'.expandir').toggle();

			if($(clase+' td.icono').hasClass('activo')){
				$(clase+' td.icono').removeClass('activo');
				$(clase+' td.icono').html('<i class="fa fa-toggle-off"></i>');
			}else{
				$(clase+' td.icono').addClass('activo');
				$(clase+' td.icono').html('<i class="fa fa-toggle-on"></i>');
			}
		}

	});

	

/* FACTORY */

	app.factory('FabricaCentros', function (ServicioCentros) {
		var fabrica = {

			centros : {
				registros : ''
			},
			frentes : {
				registros : ''
			},
			partidas : {
				registros : ''
			},
			banderas : {
				mostrar : false
			},
			obtenerCentros : function(nombre){
				ServicioCentros.obtenerCentros(nombre).
					success(function(data){
						fabrica.centros.registros = data;
					}).
					error(function(data){
						fabrica.centros.registros = data;
					});
			}

		};
		return fabrica;
	});


/* SERVICIOS */

	app.service('ServicioCentros',function($http){
	    this.obtenerCentros = function(nombre){1
			return $http.get('componentes/centros.php');
	  
	    }
	});

