
var app = angular.module('rrmMonitor',[]);

/* CONTROLLERS */

	app.controller('MonitorController', function($scope,ServicioObra,ServicioDetalle){

		// Declaraciones
		$scope.obra = {};
		$scope.filtro = {};
		$scope.alerta = {};
		
		$scope.buscar_obra = function(){

			ServicioObra.obtenerObra($scope.filtro.id).
				success(function(data){
					if(data){

						// Obra encontrada
						$scope.alerta = {};
						$scope.obra = data;

					}else{
						
						// No se encontro Obra
						$scope.obra = {};
						$scope.alerta = {};
						$scope.alerta.warning = 'No se encontro Obra con el ID proporcionado.';

					}
				}).
				error(function(error){
					console.log(error);
					$scope.obra = {};
					$scope.alerta = {};
					$scope.alerta.error = error;
				});

		}

		$scope.buscar_detalle = function(){
			ServicioDetalle.obtenerDetalle($scope.obra.id).
				success(function(data){

					if(data){
						
						// Detalle encontrado
						$scope.alerta = {};
						$scope.obra.detalle = data;

					}else{

						// No se encontro Obra
						$scope.obra = {};
						$scope.alerta = {};
						$scope.alerta.warning = 'No se encontro Detalle de la Obra proporcionada.';

					}

				}).
				error(function(error){
					console.log(error);
					$scope.obra.detalle = {};
					$scope.alerta = {};
					$scope.alerta.error = error;
				});

		}

	
	});

	// AQUI !!!!
	/*
	app.controller("PieCtrl", function ($scope) {

		$scope.labels = ["Download Sales", "In-Store Sales", "Mail-Order Sales"];
		$scope.data = [300, 500, 100];

	});
	*/

/* SERVICIOS */

	app.service('ServicioObra',function($http){
		this.obtenerObra = function(id){

			return $http.get('componentes/monitor_obra.php?id='+id);

		}
	});

	app.service('ServicioDetalle',function($http){
	    this.obtenerDetalle = function(id){

	    	return $http.get('componentes/monitor_detalle.php?id='+id);
	  
	    }
	});

