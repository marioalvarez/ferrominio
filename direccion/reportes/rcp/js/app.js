var app = angular.module('rrmRCP',[]);

/* CONTROLLERS */

	app.controller('CentrosController', function($scope,FabricaCentros){

		// Declaraciones
		$scope.centros = FabricaCentros.centros;
		
		$scope.buscar_centro = function(){
			$scope.titulo = $scope.centro;
			FabricaCentros.obtenerCentros($scope.centro);
		}

		$scope.mostrar_nivel2 = function(centro){
			$('.centro-'+centro).toggle();
		}
		
		$scope.mostrar_nivel3 = function(centro,frente){
			$('.centro-'+centro+'-frente-'+frente).toggle();
		}
	
		$scope.mostrar_tabla = function(centro,frente,partida){
			$('.centro-'+centro+'-frente-'+frente+'-partida-'+partida).toggle();
		}

		$scope.mostrar_desglose = function(centro,frente,partida,tr){
			$('.centro-'+centro+'-frente-'+frente+'-partida-'+partida+'-tr'+tr+'-mostrar').toggle();

			if($('.centro-'+centro+'-frente-'+frente+'-partida-'+partida+'-tr'+tr).hasClass('activo')){
				$('.centro-'+centro+'-frente-'+frente+'-partida-'+partida+'-tr'+tr).removeClass('activo');
				$('.centro-'+centro+'-frente-'+frente+'-partida-'+partida+'-tr'+tr+' .boton').html('<i class="fa fa-toggle-off"></i>');
			}else{
				$('.centro-'+centro+'-frente-'+frente+'-partida-'+partida+'-tr'+tr).addClass('activo');
				$('.centro-'+centro+'-frente-'+frente+'-partida-'+partida+'-tr'+tr+' .boton').html('<i class="fa fa-toggle-on"></i>');
			}
		}

	});

	

/* FACTORY */

	app.factory('FabricaCentros', function (ServicioCentros) {
		var fabrica = {

			centros : {
				registros : ''
			},
			frentes : {
				registros : ''
			},
			partidas : {
				registros : ''
			},
			banderas : {
				mostrar : false
			},
			obtenerCentros : function(nombre){
				ServicioCentros.obtenerCentros(nombre).
					success(function(data){
						fabrica.centros.registros = data;
					}).
					error(function(data){
						fabrica.centros.registros = data;
					});
			}

		};
		return fabrica;
	});


/* SERVICIOS */

	app.service('ServicioCentros',function($http){
	    this.obtenerCentros = function(nombre){1
			return $http.get('componentes/rcp_centros.php');
	  
	    }
	});

