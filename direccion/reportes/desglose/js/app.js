/* Document JS */
var app = angular.module('rrmDesglose',[]);


/* CONTROLLER */
	app.controller('obrasController',function($scope,FabricaProyectos){

		FabricaProyectos.obtenerObras();
		$scope.obras = FabricaProyectos.obras;
		$scope.contenedor = true;
		$scope.cantidad = 4;

		$scope.mostrarTipos = function(id_obra){
			FabricaProyectos.desasignarGrupos();
			FabricaProyectos.obtenerTipos(id_obra);
		};


		/* JS */

		if($(window).width() > 767){
			$('section').css('height','100%');
		}

	});

	app.controller('tiposController',function($scope,FabricaProyectos){

		$scope.tipos = FabricaProyectos.tipos;
		$scope.contenedor = FabricaProyectos.banderas;

		$scope.mostrarGrupos = function(id_tipo){
			FabricaProyectos.obtenerGrupos(id_tipo);
		};

	});

	app.controller('gruposController',function($scope,FabricaProyectos){

		$scope.grupos = FabricaProyectos.grupos;
		$scope.contenedor = FabricaProyectos.banderas;

	});

	app.factory('FabricaProyectos', function (ServicioObras,ServicioTipos,ServicioGrupos) {
		var fabrica = {

			id_obra : '',
			obras : {
				registros : ''
			},
			tipos : {
				registros : ''
			},
			grupos : {
				registros : ''
			},
			banderas : {
				contenedor_tipos : false,
				contenedor_grupos : false
			},
			obtenerObras : function(){ 
				ServicioObras.obtenerObras().
					success(function(data){
						fabrica.obras.registros = data;
					}).
					error(function(data){
						alert('Se produjo un error al obtener las obras, consultarlo con soporte');
					})

			},
			obtenerTipos : function(id_obra){
				ServicioTipos.obtenerTipos(id_obra).
					success(function(data){
						fabrica.tipos.registros = data;
						fabrica.banderas.contenedor_tipos = true;
						fabrica.id_obra = id_obra;
					}).
					error(function(data){
						alert('Se produjo un error al obtener los tipos, consultarlo con soporte');
					})
			},
			desasignarTipos : function(){
				fabrica.tipos.registros = '';
				fabrica.banderas.contenedor_tipos = false;
			},
			obtenerGrupos : function(id_tipo){
				ServicioGrupos.obtenerGrupos(fabrica.id_obra,id_tipo).
					success(function(data){
						fabrica.grupos.registros = data;
						fabrica.banderas.contenedor_grupos = true;
					}).
					error(function(data){
						alert('Se produjo un error al obtener los grupos, consultarlo con soporte');
					})
			},
			desasignarGrupos : function(){
				fabrica.grupos.registros = '';
				fabrica.banderas.contenedor_grupos = false;
			}

		};
		return fabrica;
	});


/* SERVICE */
	app.service('ServicioObras',function($http){
	    this.obtenerObras = function(){
	    	
			return $http.get('componentes/desglose_obras.php');
	  
	    }
	});
	app.service('ServicioTipos',function($http){
	    this.obtenerTipos = function(id_obra){
	    	
			return $http.get('componentes/desglose_tipos.php?obra='+id_obra);
	  
	    }
	});
	app.service('ServicioGrupos',function($http){
	    this.obtenerGrupos = function(id_obra,id_tipo){
	    	
			return $http.get('componentes/desglose_grupos.php?obra='+id_obra+'&tipo='+id_tipo);
	  
	    }
	});
