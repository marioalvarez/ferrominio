<?php

$obras = '

[
	{
		"id"			 	 : "1",
		"nb_obra"		 : "SEARS CULIACÁN",
		"nu_presupuestado" : "$ 10,000,000.00",
		"nu_utilidad" 	 : "$  6,000,000.00",
		"nu_gastado" 		 : "$  4,000,000.00",
		"de_semaforo"		 : 1
	},
	{
		"id" 	 			 : "2",
		"nb_obra"      	 : "LIVERPOOL GUADALAJARA",
		"nu_presupuestado" : "$ 10,000,000.00",
		"nu_utilidad" 	 : "$ 3,000,000.00",
		"nu_gastado" 		 : "$ 7,000,000.00",
		"de_semaforo"		 : 2
	},
	{
		"id" 	 			 : "3",
		"nb_obra"      	 : "SAMS SONORA",
		"nu_presupuestado" : "$ 10,000,000.00",
		"nu_utilidad" 	 : "$  -2,000,000.00",
		"nu_gastado" 		 : "$  12,000,000.00",
		"de_semaforo"		 : 3
	}
]

';

echo $obras;