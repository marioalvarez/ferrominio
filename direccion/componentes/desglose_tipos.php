<?php

$id_obra = $_GET['obra'];

$indicadores = '';

switch($id_obra){

	case 1:

		$indicadores = array(1,1,1);
		$presupuestado = array("$ 4,000,000.00","$ 5,000,000.00","$ 1,000,000.00");
		$utilidad = array("$ 2,500,000.00","$ 3,000,000.00","$ 500,000.00");
		$gastado = array("$ 1,500,000.00","$ 2,000,000.00","$ 500,000.00");

	break;
	case 2:

		$indicadores = array(2,2,1);
		$presupuestado = array("$ 4,000,000.00","$ 5,000,000.00","$ 1,000,000.00");
		$utilidad = array("$ 1,000,000.00","$ 1,200,000.00","$ 800,000.00");
		$gastado = array("$ 3,000,000.00","$ 3,800,000.00","$ 200,000.00");

	break;
	case 3:

		$indicadores = array(3,3,2);
		$presupuestado = array("$ 4,000,000.00","$ 5,000,000.00","$ 1,000,000.00");
		$utilidad = array("$ -600,000.00","$ -1,500,000.00","$ 100,000.00");
		$gastado = array("$ 4,600,000.00","$ 6,500,000.00","$ 900,000.00");

	break;
	default:

}

$tipos = '

[
	{
		"id" 	 			 	: "'.$id_obra.'01",
		"nb_tipo"		 	 	: "MATERIALES",
		"nu_presupuestado" 		: "'.$presupuestado[0].'",
		"nu_utilidad" 	 		: "'.$utilidad[0].'",
		"nu_gastado" 		 	: "'.$gastado[0].'",
		"de_semaforo"		 	: '.$indicadores[0].'
	},
	{
		"id" 	 			 	: "'.$id_obra.'02",
		"nb_tipo"		 	 	: "MANO DE OBRA",
		"nu_presupuestado" 		: "'.$presupuestado[1].'",
		"nu_utilidad" 	 		: "'.$utilidad[1].'",
		"nu_gastado" 		 	: "'.$gastado[1].'",
		"de_semaforo"		 	: '.$indicadores[1].'
	},
	{
		"id" 	 			 	: "'.$id_obra.'03",
		"nb_tipo"		 	 	: "HERRAMIENTA Y EQUIPO",
		"nu_presupuestado" 		: "'.$presupuestado[2].'",
		"nu_utilidad" 	 		: "'.$utilidad[2].'",
		"nu_gastado" 		 	: "'.$gastado[2].'",
		"de_semaforo"		 	: '.$indicadores[2].'
	}
]

';

echo $tipos;