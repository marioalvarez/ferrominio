<?php

$centros = '
	[
    	{
    		"centro" : 1,
            "nombre" : "hola mundo",
            "frentes" : [
                {
                    "frente" : 1,
                    "partidas" : [
                        {
                            "partida" : 1,
                            "datos" : [
                                {
                                    "codigo" : 101001,
                                    "insumo" : "VarillaVarillaVarillaVarillaVarillaVarilla",
                                    "um" : "Pieza",
                                    "cant_ppto_base" : 10,
                                    "aditivas" : 4,
                                    "deductivas" : 6,
                                    "cant_comprada" : 10,
                                    "por_comprar" : 0,
                                    "cant_recibida" : 8,
                                    "por_recibir" : 0,
                                    "cant_aplicada" : 8
                                },
                                {
                                    "codigo" : 101001,
                                    "insumo" : "Cemento",
                                    "um" : "Pieza",
                                    "cant_ppto_base" : 30,
                                    "aditivas" : 4,
                                    "deductivas" : 26,
                                    "cant_comprada" : 30,
                                    "por_comprar" : 0,
                                    "cant_recibida" : 22,
                                    "por_recibir" : 0,
                                    "cant_aplicada" : 22
                                },
                                {
                                    "codigo" : 101001,
                                    "insumo" : "Viga",
                                    "um" : "Pieza",
                                    "cant_ppto_base" : 15,
                                    "aditivas" : 0,
                                    "deductivas" : 15,
                                    "cant_comprada" : 15,
                                    "por_comprar" : 0,
                                    "cant_recibida" : 15,
                                    "por_recibir" : 0,
                                    "cant_aplicada" : 15
                                }
                            ]
                        },
                        {
                            "partida" : 2,
                            "datos" : [
                                {
                                    "codigo" : 101002,
                                    "insumo" : "Varilla",
                                    "um" : "Pieza",
                                    "cant_ppto_base" : 10,
                                    "aditivas" : 4,
                                    "deductivas" : 6,
                                    "cant_comprada" : 10,
                                    "por_comprar" : 0,
                                    "cant_recibida" : 8,
                                    "por_recibir" : 0,
                                    "cant_aplicada" : 8
                                }
                            ]
                        },
                        {
                            "partida" : 3,
                            "datos" : [
                                {
                                    "codigo" : 101003,
                                    "insumo" : "Cemento",
                                    "um" : "Pieza",
                                    "cant_ppto_base" : 30,
                                    "aditivas" : 4,
                                    "deductivas" : 26,
                                    "cant_comprada" : 30,
                                    "por_comprar" : 0,
                                    "cant_recibida" : 22,
                                    "por_recibir" : 0,
                                    "cant_aplicada" : 22
                            }
                        ]
                        }
                    ]
                },
                {
                    "frente" : 2,
                    "partidas" : [
                        {
                            "partida" : 1,
                            "datos" : [
                                {
                                    "codigo" : 102001,
                                    "insumo" : "Varilla",
                                    "um" : "Pieza",
                                    "cant_ppto_base" : 10,
                                    "aditivas" : 4,
                                    "deductivas" : 6,
                                    "cant_comprada" : 10,
                                    "por_comprar" : 0,
                                    "cant_recibida" : 8,
                                    "por_recibir" : 0,
                                    "cant_aplicada" : 8
                                }
                            ]
                        },
                        {
                            "partida" : 2,
                            "datos" : [
                                {
                                    "codigo" : 102002,
                                    "insumo" : "Cemento",
                                    "um" : "Pieza",
                                    "cant_ppto_base" : 30,
                                    "aditivas" : 4,
                                    "deductivas" : 26,
                                    "cant_comprada" : 30,
                                    "por_comprar" : 0,
                                    "cant_recibida" : 22,
                                    "por_recibir" : 0,
                                    "cant_aplicada" : 22
                                }
                            ]
                        },
                        {
                            "partida" : 3,
                            "datos" : [
                                {
                                    "codigo" : 102003,
                                    "insumo" : "Viga",
                                    "um" : "Pieza",
                                    "cant_ppto_base" : 15,
                                    "aditivas" : 0,
                                    "deductivas" : 15,
                                    "cant_comprada" : 15,
                                    "por_comprar" : 0,
                                    "cant_recibida" : 15,
                                    "por_recibir" : 0,
                                    "cant_aplicada" : 15
                            }
                        ]
                        }
                    ]
                },
                {
                    "frente" : 3,
                    "partidas" : [
                        {
                            "partida" : 1,
                            "datos" : [
                                {
                                    "codigo" : 103001,
                                    "insumo" : "Varilla",
                                    "um" : "Pieza",
                                    "cant_ppto_base" : 10,
                                    "aditivas" : 4,
                                    "deductivas" : 6,
                                    "cant_comprada" : 10,
                                    "por_comprar" : 0,
                                    "cant_recibida" : 8,
                                    "por_recibir" : 0,
                                    "cant_aplicada" : 8
                                }
                            ]
                        },
                        {
                            "partida" : 2,
                            "datos" : [
                                {
                                    "codigo" : 103002,
                                    "insumo" : "Cemento",
                                    "um" : "Pieza",
                                    "cant_ppto_base" : 30,
                                    "aditivas" : 4,
                                    "deductivas" : 26,
                                    "cant_comprada" : 30,
                                    "por_comprar" : 0,
                                    "cant_recibida" : 22,
                                    "por_recibir" : 0,
                                    "cant_aplicada" : 22
                                }
                            ]
                        },
                        {
                            "partida" : 3,
                            "datos" : [
                                {
                                    "codigo" : 103003,
                                    "insumo" : "Viga",
                                    "um" : "Pieza",
                                    "cant_ppto_base" : 15,
                                    "aditivas" : 0,
                                    "deductivas" : 15,
                                    "cant_comprada" : 15,
                                    "por_comprar" : 0,
                                    "cant_recibida" : 15,
                                    "por_recibir" : 0,
                                    "cant_aplicada" : 15
                            }
                        ]
                        }
                    ]
                }
            ]
    	},
        {
            "centro" : 2,
            "frentes" : [
                {
                    "frente" : 1,
                    "partidas" : [
                        {
                            "partida" : 1,
                            "datos" : [
                                {
                                    "codigo" : 201001,
                                    "insumo" : "Varilla",
                                    "um" : "Pieza",
                                    "cant_ppto_base" : 10,
                                    "aditivas" : 4,
                                    "deductivas" : 6,
                                    "cant_comprada" : 10,
                                    "por_comprar" : 0,
                                    "cant_recibida" : 8,
                                    "por_recibir" : 0,
                                    "cant_aplicada" : 8
                                }
                            ]
                        },
                        {
                            "partida" : 2,
                            "datos" : [
                                {
                                    "codigo" : 201002,
                                    "insumo" : "Cemento",
                                    "um" : "Pieza",
                                    "cant_ppto_base" : 30,
                                    "aditivas" : 4,
                                    "deductivas" : 26,
                                    "cant_comprada" : 30,
                                    "por_comprar" : 0,
                                    "cant_recibida" : 22,
                                    "por_recibir" : 0,
                                    "cant_aplicada" : 22
                                }
                            ]
                        },
                        {
                            "partida" : 3,
                            "datos" : [
                                {
                                    "codigo" : 201003,
                                    "insumo" : "Viga",
                                    "um" : "Pieza",
                                    "cant_ppto_base" : 15,
                                    "aditivas" : 0,
                                    "deductivas" : 15,
                                    "cant_comprada" : 15,
                                    "por_comprar" : 0,
                                    "cant_recibida" : 15,
                                    "por_recibir" : 0,
                                    "cant_aplicada" : 15
                            }
                        ]
                        }
                    ]
                },
                {
                    "frente" : 2,
                    "partidas" : [
                        {
                            "partida" : 1,
                            "datos" : [
                                {
                                    "codigo" : 202001,
                                    "insumo" : "Varilla",
                                    "um" : "Pieza",
                                    "cant_ppto_base" : 10,
                                    "aditivas" : 4,
                                    "deductivas" : 6,
                                    "cant_comprada" : 10,
                                    "por_comprar" : 0,
                                    "cant_recibida" : 8,
                                    "por_recibir" : 0,
                                    "cant_aplicada" : 8
                                }
                            ]
                        },
                        {
                            "partida" : 2,
                            "datos" : [
                                {
                                    "codigo" : 202002,
                                    "insumo" : "Cemento",
                                    "um" : "Pieza",
                                    "cant_ppto_base" : 30,
                                    "aditivas" : 4,
                                    "deductivas" : 26,
                                    "cant_comprada" : 30,
                                    "por_comprar" : 0,
                                    "cant_recibida" : 22,
                                    "por_recibir" : 0,
                                    "cant_aplicada" : 22
                                }
                            ]
                        },
                        {
                            "partida" : 3,
                            "datos" : [
                                {
                                    "codigo" : 202003,
                                    "insumo" : "Viga",
                                    "um" : "Pieza",
                                    "cant_ppto_base" : 15,
                                    "aditivas" : 0,
                                    "deductivas" : 15,
                                    "cant_comprada" : 15,
                                    "por_comprar" : 0,
                                    "cant_recibida" : 15,
                                    "por_recibir" : 0,
                                    "cant_aplicada" : 15
                            }
                        ]
                        }
                    ]
                },
                {
                    "frente" : 3,
                    "partidas" : [
                        {
                            "partida" : 1,
                            "datos" : [
                                {
                                    "codigo" : 203001,
                                    "insumo" : "Varilla",
                                    "um" : "Pieza",
                                    "cant_ppto_base" : 10,
                                    "aditivas" : 4,
                                    "deductivas" : 6,
                                    "cant_comprada" : 10,
                                    "por_comprar" : 0,
                                    "cant_recibida" : 8,
                                    "por_recibir" : 0,
                                    "cant_aplicada" : 8
                                }
                            ]
                        },
                        {
                            "partida" : 2,
                            "datos" : [
                                {
                                    "codigo" : 203002,
                                    "insumo" : "Cemento",
                                    "um" : "Pieza",
                                    "cant_ppto_base" : 30,
                                    "aditivas" : 4,
                                    "deductivas" : 26,
                                    "cant_comprada" : 30,
                                    "por_comprar" : 0,
                                    "cant_recibida" : 22,
                                    "por_recibir" : 0,
                                    "cant_aplicada" : 22
                                }
                            ]
                        },
                        {
                            "partida" : 3,
                            "datos" : [
                                {
                                    "codigo" : 203003,
                                    "insumo" : "Viga",
                                    "um" : "Pieza",
                                    "cant_ppto_base" : 15,
                                    "aditivas" : 0,
                                    "deductivas" : 15,
                                    "cant_comprada" : 15,
                                    "por_comprar" : 0,
                                    "cant_recibida" : 15,
                                    "por_recibir" : 0,
                                    "cant_aplicada" : 15
                            }
                        ]
                        }
                    ]
                }
            ]
        },
        {
            "centro" : 3,
            "frentes" : [
                {
                    "frente" : 1,
                    "partidas" : [
                        {
                            "partida" : 1,
                            "datos" : [
                                {
                                    "codigo" : 301001,
                                    "insumo" : "Varilla",
                                    "um" : "Pieza",
                                    "cant_ppto_base" : 10,
                                    "aditivas" : 4,
                                    "deductivas" : 6,
                                    "cant_comprada" : 10,
                                    "por_comprar" : 0,
                                    "cant_recibida" : 8,
                                    "por_recibir" : 0,
                                    "cant_aplicada" : 8
                                }
                            ]
                        },
                        {
                            "partida" : 2,
                            "datos" : [
                                {
                                    "codigo" : 301002,
                                    "insumo" : "Cemento",
                                    "um" : "Pieza",
                                    "cant_ppto_base" : 30,
                                    "aditivas" : 4,
                                    "deductivas" : 26,
                                    "cant_comprada" : 30,
                                    "por_comprar" : 0,
                                    "cant_recibida" : 22,
                                    "por_recibir" : 0,
                                    "cant_aplicada" : 22
                                }
                            ]
                        },
                        {
                            "partida" : 3,
                            "datos" : [
                                {
                                    "codigo" : 301003,
                                    "insumo" : "Viga",
                                    "um" : "Pieza",
                                    "cant_ppto_base" : 15,
                                    "aditivas" : 0,
                                    "deductivas" : 15,
                                    "cant_comprada" : 15,
                                    "por_comprar" : 0,
                                    "cant_recibida" : 15,
                                    "por_recibir" : 0,
                                    "cant_aplicada" : 15
                            }
                        ]
                        }
                    ]
                },
                {
                    "frente" : 2,
                    "partidas" : [
                        {
                            "partida" : 1,
                            "datos" : [
                                {
                                    "codigo" : 302001,
                                    "insumo" : "Varilla",
                                    "um" : "Pieza",
                                    "cant_ppto_base" : 10,
                                    "aditivas" : 4,
                                    "deductivas" : 6,
                                    "cant_comprada" : 10,
                                    "por_comprar" : 0,
                                    "cant_recibida" : 8,
                                    "por_recibir" : 0,
                                    "cant_aplicada" : 8
                                }
                            ]
                        },
                        {
                            "partida" : 2,
                            "datos" : [
                                {
                                    "codigo" : 302002,
                                    "insumo" : "Cemento",
                                    "um" : "Pieza",
                                    "cant_ppto_base" : 30,
                                    "aditivas" : 4,
                                    "deductivas" : 26,
                                    "cant_comprada" : 30,
                                    "por_comprar" : 0,
                                    "cant_recibida" : 22,
                                    "por_recibir" : 0,
                                    "cant_aplicada" : 22
                                }
                            ]
                        },
                        {
                            "partida" : 3,
                            "datos" : [
                                {
                                    "codigo" : 302003,
                                    "insumo" : "Viga",
                                    "um" : "Pieza",
                                    "cant_ppto_base" : 15,
                                    "aditivas" : 0,
                                    "deductivas" : 15,
                                    "cant_comprada" : 15,
                                    "por_comprar" : 0,
                                    "cant_recibida" : 15,
                                    "por_recibir" : 0,
                                    "cant_aplicada" : 15
                            }
                        ]
                        }
                    ]
                },
                {
                    "frente" : 3,
                    "partidas" : [
                        {
                            "partida" : 1,
                            "datos" : [
                                {
                                    "codigo" : 303001,
                                    "insumo" : "Varilla",
                                    "um" : "Pieza",
                                    "cant_ppto_base" : 10,
                                    "aditivas" : 4,
                                    "deductivas" : 6,
                                    "cant_comprada" : 10,
                                    "por_comprar" : 0,
                                    "cant_recibida" : 8,
                                    "por_recibir" : 0,
                                    "cant_aplicada" : 8
                                }
                            ]
                        },
                        {
                            "partida" : 2,
                            "datos" : [
                                {
                                    "codigo" : 303002,
                                    "insumo" : "Cemento",
                                    "um" : "Pieza",
                                    "cant_ppto_base" : 30,
                                    "aditivas" : 4,
                                    "deductivas" : 26,
                                    "cant_comprada" : 30,
                                    "por_comprar" : 0,
                                    "cant_recibida" : 22,
                                    "por_recibir" : 0,
                                    "cant_aplicada" : 22
                                }
                            ]
                        },
                        {
                            "partida" : 3,
                            "datos" : [
                                {
                                    "codigo" : 303003,
                                    "insumo" : "Viga",
                                    "um" : "Pieza",
                                    "cant_ppto_base" : 15,
                                    "aditivas" : 0,
                                    "deductivas" : 15,
                                    "cant_comprada" : 15,
                                    "por_comprar" : 0,
                                    "cant_recibida" : 15,
                                    "por_recibir" : 0,
                                    "cant_aplicada" : 15
                            }
                        ]
                        }
                    ]
                }
            ]
        }
	]
';

echo $centros;

?>