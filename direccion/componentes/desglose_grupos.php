<?php

$id_obra = $_GET['obra'];
$id_tipo = $_GET['tipo'];


/* ID 1 */
	if($id_obra == 1 && $id_tipo == 101){
		$tipos = '
		[
			{
				"id" 	 			 	: "'.$id_obra.'0'.$id_tipo.'001",
				"nb_grupo"		 	 	: "ACERO",
				"nu_presupuestado" 		: "$  1,300,000.00",
				"nu_utilidad" 	 		: "$  800,000.00",
				"nu_gastado" 		 	: "$  500,000.00",
				"de_semaforo"		 	: 1
			},
			{
				"id" 	 			 	: "'.$id_obra.'0'.$id_tipo.'002",
				"nb_grupo"		 	 	: "PINTURA",
				"nu_presupuestado" 		: "$  1,700,000.00",
				"nu_utilidad" 	 		: "$  1,000,000.00",
				"nu_gastado" 		 	: "$  700,000.00",
				"de_semaforo"		 	: 1
			},
			{
				"id" 	 			 	: "'.$id_obra.'0'.$id_tipo.'003",
				"nb_grupo"		 	 	: "ACABADOS",
				"nu_presupuestado" 		: "$  1,000,000.00",
				"nu_utilidad" 	 		: "$  700,000.00",
				"nu_gastado" 		 	: "$  300,000.00",
				"de_semaforo"		 	: 1
			}
		]
		';
	}else if($id_obra == 1 && $id_tipo == 102){
		$tipos = '
		[
			{
				"id" 	 			 	: "'.$id_obra.'0'.$id_tipo.'001",
				"nb_grupo"		 	 	: "DESTAJOS",
				"nu_presupuestado" 		: "$  5,000,000.00",
				"nu_utilidad" 	 		: "$  3,000,000.00",
				"nu_gastado" 		 	: "$  2,000,000.00",
				"de_semaforo"		 	: 1
			}
		]
		';
	}else if($id_obra == 1 && $id_tipo == 103){
		$tipos = '
		[
			{
				"id" 	 			 	: "'.$id_obra.'0'.$id_tipo.'001",
				"nb_grupo"		 	 	: "HERRAMIENTAS",
				"nu_presupuestado" 		: "$  1,000,000.00",
				"nu_utilidad" 	 		: "$  5,000,000.00",
				"nu_gastado" 		 	: "$  5,000,000.00",
				"de_semaforo"		 	: 1
			}
		]
		';
	}else if($id_obra == 2 && $id_tipo == 201){
/* ID 2 */
		$tipos = '
		[
			{
				"id" 	 			 	: "'.$id_obra.'0'.$id_tipo.'001",
				"nb_grupo"		 	 	: "ACERO",
				"nu_presupuestado" 		: "$  1,300,000.00",
				"nu_utilidad" 	 		: "$  100,000.00",
				"nu_gastado" 		 	: "$  1,200,000.00",
				"de_semaforo"		 	: 2
			},
			{
				"id" 	 			 	: "'.$id_obra.'0'.$id_tipo.'002",
				"nb_grupo"		 	 	: "PINTURA",
				"nu_presupuestado" 		: "$  1,700,000.00",
				"nu_utilidad" 	 		: "$  700,000.00",
				"nu_gastado" 		 	: "$  1,000,000.00",
				"de_semaforo"		 	: 1
			},
			{
				"id" 	 			 	: "'.$id_obra.'0'.$id_tipo.'003",
				"nb_grupo"		 	 	: "ACABADOS",
				"nu_presupuestado" 		: "$  1,000,000.00",
				"nu_utilidad" 	 		: "$  200,000.00",
				"nu_gastado" 		 	: "$  800,000.00",
				"de_semaforo"		 	: 2
			}
		]
		';
	}else if($id_obra == 2 && $id_tipo == 202){
		$tipos = '
		[
			{
				"id" 	 			 	: "'.$id_obra.'0'.$id_tipo.'001",
				"nb_grupo"		 	 	: "DESTAJOS",
				"nu_presupuestado" 		: "$  5,000,000.00",
				"nu_utilidad" 	 		: "$  1,200,000.00",
				"nu_gastado" 		 	: "$  3,800,000.00",
				"de_semaforo"		 	: 2
			}
		]
		';
	}else if($id_obra == 2 && $id_tipo == 203){
		$tipos = '
		[
			{
				"id" 	 			 	: "'.$id_obra.'0'.$id_tipo.'001",
				"nb_grupo"		 	 	: "HERRAMIENTAS",
				"nu_presupuestado" 		: "$  1,000,000.00",
				"nu_utilidad" 	 		: "$  800,000.00",
				"nu_gastado" 		 	: "$  200,000.00",
				"de_semaforo"		 	: 1
			}
		]
		';
	}else if($id_obra == 3 && $id_tipo == 301){
/* ID 3 */
		$tipos = '
		[
			{
				"id" 	 			 	: "'.$id_obra.'0'.$id_tipo.'001",
				"nb_grupo"		 	 	: "ACERO",
				"nu_presupuestado" 		: "$  1,300,000.00",
				"nu_utilidad" 	 		: "$  300,000.00",
				"nu_gastado" 		 	: "$  1,000,000.00",
				"de_semaforo"		 	: 2
			},
			{
				"id" 	 			 	: "'.$id_obra.'0'.$id_tipo.'002",
				"nb_grupo"		 	 	: "PINTURA",
				"nu_presupuestado" 		: "$  1,700,000.00",
				"nu_utilidad" 	 		: "$  -1,100,000.00",
				"nu_gastado" 		 	: "$  2,800,000.00",
				"de_semaforo"		 	: 3
			},
			{
				"id" 	 			 	: "'.$id_obra.'0'.$id_tipo.'003",
				"nb_grupo"		 	 	: "ACABADOS",
				"nu_presupuestado" 		: "$  1,000,000.00",
				"nu_utilidad" 	 		: "$  200,000.00",
				"nu_gastado" 		 	: "$  800,000.00",
				"de_semaforo"		 	: 2
			}
		]
		';
	}else if($id_obra == 3 && $id_tipo == 302){
		$tipos = '
		[
			{
				"id" 	 			 	: "'.$id_obra.'0'.$id_tipo.'001",
				"nb_grupo"		 	 	: "DESTAJOS",
				"nu_presupuestado" 		: "$  5,000,000.00",
				"nu_utilidad" 	 		: "$  -1,500,000.00",
				"nu_gastado" 		 	: "$  6,500,000.00",
				"de_semaforo"		 	: 3
			}
		]
		';
	}else if($id_obra == 3 && $id_tipo == 303){
		$tipos = '
		[
			{
				"id" 	 			 	: "'.$id_obra.'0'.$id_tipo.'001",
				"nb_grupo"		 	 	: "HERRAMIENTAS",
				"nu_presupuestado" 		: "$  1,000,000.00",
				"nu_utilidad" 	 		: "$  100,000.00",
				"nu_gastado" 		 	: "$  900,000.00",
				"de_semaforo"		 	: 2
			}
		]
		';
	}

echo $tipos;