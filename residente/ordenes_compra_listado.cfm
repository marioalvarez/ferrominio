<cfparam name="id_Obra" default="#Session.id_Obra#">
<cfparam name="Filtro_id_OrdenCompra" default="">
<cfparam name="Filtro_id_Estatus" default="">
<cfparam name="Filtro_id_Proveedor" default="">
<cfparam name="Filtro_fh_OrdenCompra" default="">
<cfparam name="sn_Mostrar" default="N">
<cfparam name="Filtro_id_empresa" default="0">

<cfquery name="Empresas" datasource="#session.cnx#">
	Select id_Empresa, nb_Empresa from Empresas
</cfquery>

<cfquery name="Estatus" datasource="#session.cnx#">
	Select id_estatus, de_Estatus from Estatus 
    where id_estatus in (202,203,204)
</cfquery>


<cfif sn_Mostrar EQ 'S'>
	<cfset args= StructNew()>

	<cfif #Filtro_id_empresa# NEQ 0>
         <cfset args.id_Empresa=  #Filtro_id_empresa#>
    </cfif>
   
    
	<!---<cfset args.id_Empresa= #Session.id_Empresa#>--->
	<cfset args.id_Obra= #id_Obra#>
	<cfif #Filtro_id_OrdenCompra# NEQ "">
		<cfset args.id_OrdenCompra = #Filtro_id_OrdenCompra#>
	</cfif>
	<cfif #Filtro_id_Estatus# NEQ "">
		<cfset args.id_Estatus = #Filtro_id_Estatus#>
	</cfif>
	<cfif #Filtro_id_Proveedor# NEQ "">
		<cfset args.id_Proveedor = #Filtro_id_Proveedor#>
	</cfif>
	<cfif #Filtro_fh_OrdenCompra# NEQ "">
		<cfinvoke component="#Application.componentes#.funciones" method="formateaFecha" formato_original="d/m/y" formato_nuevo="y/m/d"  fecha="#Filtro_fh_OrdenCompra#" separador="/" returnvariable="fechaSQL">
		<cfset args.fh_OrdenCompra = #fechaSQL#>
	</cfif>
	<cfset args.id_ModuloTipoOrdenCompra= 2>
	<cfinvoke component="#Application.componentes#.ordenescompra" method="OrdenesCompra_CompraPlaza_listado" returnvariable="RSOrdenesCompra" argumentcollection="#args#">
	<!---Paginado--->
<!---	<cfparam name="PageNum_RSOrdenesCompra" default="1">
	<cfset CurrentPage=GetFileFromPath(GetTemplatePath())>
	<cfset MaxRows_RSOrdenesCompra=25>
	<cfset StartRow_RSOrdenesCompra=Min((PageNum_RSOrdenesCompra-1)*MaxRows_RSOrdenesCompra+1,Max(RSOrdenesCompra.rs.RecordCount,1))>
	<cfset EndRow_RSOrdenesCompra=Min(StartRow_RSOrdenesCompra+MaxRows_RSOrdenesCompra-1,RSOrdenesCompra.rs.RecordCount)>
	<cfset TotalPages_RSOrdenesCompra=Ceiling(RSOrdenesCompra.rs.RecordCount/MaxRows_RSOrdenesCompra)>
	<cfset QueryString_RSOrdenesCompra=Iif(CGI.QUERY_STRING NEQ "",DE("&"&XMLFormat(CGI.QUERY_STRING)),DE(""))>
	<cfset tempPos=ListContainsNoCase(QueryString_RSOrdenesCompra,"PageNum_RSOrdenesCompra=","&")>
	<cfif tempPos NEQ 0>
	  <cfset QueryString_RSOrdenesCompra=ListDeleteAt(QueryString_RSOrdenesCompra,tempPos,"&")>
	</cfif>--->
</cfif>

<cfinvoke component="#Application.componentes#.funciones" method="StructToListStruct" returnvariable="SessionStruct" estructura="#Session#" separador_lista="|" separador_valores="=">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<link rel="stylesheet" href="../css/style.css" type="text/css">
<link rel="stylesheet" type="text/css" href="../css/box.css">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Untitled Document</title>
<!---Refs de Calendario --->
<script type="text/javascript" src="../calendario/calendar.js"></script>
<script type="text/javascript" src="../calendario/calendar-setup.js"></script>
<script type="text/javascript" src="../calendario/lang/calendar-es.js"></script>
<style type="text/css"> @import url("../calendario/calendar-blue2.css");</style>
<!---Termina Refs de Calendario--->
<script language="javascript" src="../js/jquery.js"></script>
<script language="javascript" src="../js/funciones.js"></script>
</head>
<body onload="form1.id_Obra.focus();">
<cfinclude template="menu.cfm">&nbsp;

 <cfform name="form1" method="post" action="ordenes_compra_listado.cfm">
<table style="width:800px" align="center">
	<tr class="titulo">
		<td align="left">LISTADO DE &Oacute;RDENES DE COMPRA</td>
         <!---<td align="right">DESCARGAR MANUAL:</td>
                        	<td>
                            			
                            <a id="abrir_documentoRespuesta" 
                            href="Manuales/OrdenesdeCompra/ordenesDeCompra.zip"
                            target="_self"
                            title="Adjunto(IFE)"><img src="../images/descargar.png" border="0" title="Descargar Manual" width="20"><a>
                      
                            </td>--->
          </tr>
</table>
<table width="800px" cellpadding="0" cellspacing="0" align="center">
	<tr><td class="x-box-tl" width="1px"></td><td class="x-box-tc"></td><td class="x-box-tr" width="1px"></td></tr>
	<tr><td class="x-box-ml"></td><td class="x-box-mc" width="99%">	
		<table width="800px" align="left">
		      <tr>
              		<td></td>
                	<td align="right">Empresa:</td>
					<td align="left">
					<cfselect name="Filtro_id_empresa" style="width:300px" class="contenido" required="no">
                        <option value="0">TODAS</option>
                            <cfoutput query="Empresas">
                                <option value="#Empresas.id_empresa#"  <cfif #Empresas.id_empresa# EQ #Filtro_id_empresa#>selected="selected"</cfif>>
                                #Empresas.nb_empresa# 
                                </option>
                            </cfoutput>
                        </cfselect> 
                	</td>
                    <td align="right">Estatus:</td>
                    <td align="left">
                    <cfselect name="Filtro_id_estatus" style="width:150px" class="contenido" required="no">
                        <option value="">TODAS</option>
                            <cfoutput query="Estatus">
                                <option value="#Estatus.id_estatus#"  <cfif #Estatus.id_estatus# EQ #Filtro_id_estatus#>selected="selected"</cfif>>
                                #Estatus.de_Estatus# 
                                </option>
                            </cfoutput>
                        </cfselect>                    
                    </td> 
                </tr>
				<tr> 
					<!---<cfif #session.id_puesto# Eq 104>
                    	<td width="30px"><button type="button" class="boton_menu_superior" title="Clic para agregar �rdenes de compra" onClick="javascript:location='ordenes_compra_agregar.cfm?Filtro_id_OrdenCompra=<cfoutput>#Filtro_id_OrdenCompra#&Filtro_id_Estatus=#Filtro_id_Estatus#&Filtro_id_Proveedor=#Filtro_id_Proveedor#&Filtro_fh_OrdenCompra=#Filtro_fh_OrdenCompra#</cfoutput>'"><img src="../images/nuevo.png" alt="Clic para agregar �rdenes de compra" width="24"></button></td>
					<cfelse>--->
                    	<td width="30px">&nbsp;</td>
                  <!---  </cfif>--->
                    <td align="right">Obra:</td>
					<td align="left">
						<cfinput type="text" class="contenido_readOnly" readonly="yes" name="id_Obra" id="id_Obra" validate="integer" onBlur="$x('de_Obra_lab').value='';" size="8" value="#session.id_Obra#">
						<cftextarea bind="cfc:#Application.componentes#.obras.getObraPorID(1,{id_Obra@blur})" bindonload="yes" id="de_Obra_lab" name="de_Obra_lab" rows="1" cols="30" class="contenido_readOnly" readonly="yes" style="overflow:hidden;" required="yes" message="La Clave de Obra no es v�lida."></cftextarea>
						<button type="button" id="btnBuscarObra" onClick="javascript:AbrirPopUpBusquedaChrome('id_Obra', 'pop_obras_busqueda.cfm', 'id_Empresa=<cfoutput>#Session.id_Empresa#</cfoutput>',  '700px', '600px')" class="boton_popup" value="" title="Clic para buscar obra"><img src="../images/buscar.png" width="24"></button>
					</td>					
					<td align="right">Folio:</td>
					<td align="left">
						<cfinput type="text" name="Filtro_id_OrdenCompra" id="Filtro_id_OrdenCompra" class="contenido_numerico" value="#Filtro_id_OrdenCompra#" size="11" validate="integer" message="El folio de la orden de compra no es v�lido">
					</td>
				</tr>
				<tr>
					<td></td>
					<td align="right">Proveedor:</td>
					<td align="left">
						<cfinput type="text" name="sugerida_Proveedor" id="sugerida_Proveedor" class="contenido" size="8" value="#Filtro_id_Proveedor#"/>
						<cfselect name="Filtro_id_Proveedor" id="Filtro_id_Proveedor" bind="cfc:#Application.componentes#.proveedores.getProveedoresSugeridasParaFiltrotmp({sugerida_Proveedor@blur})" bindonload="yes" display="nb_ProveedorCorto" value="id_Proveedor" onBlur="javascript:$x('sugerida_Proveedor').value=$x('Filtro_id_Proveedor').value;" class="contenido" style="width:200px">
						</cfselect>
					</td>
					<td align="right">Fecha:</td>
					<td align="left">
						<cfinput type="text" name="Filtro_fh_OrdenCompra" id="Filtro_fh_OrdenCompra" mask="99/99/9999" readonly="true" validate="eurodate" message="La fecha de la orden de compra no es v�lida" class="contenido" value="#Filtro_fh_OrdenCompra#" size="11"> 
						<input type="button" id="lanzador" value="..." / class="boton" onKeyDown="javascript:escanear_enter();" />
					</td>
					<td width="40px"><button type="submit" class="boton_imagen"><img src="../images/filtro_azul.png" alt="Clic para buscar �rdenes de compra"></button></td>
				</tr>
				<input type="hidden" name="sn_Mostrar" value="S">
		 
		</table>
		</td><td class="x-box-mr"></td></tr><tr><td class="x-box-bl" width="1px"></td><td class="x-box-bc"></td><td class="x-box-br" width="1px"></td>
	</tr>
</table>
</cfform>
<br>
<cfif #sn_Mostrar# EQ "S">
	<table border="0" cellpadding="0" cellspacing="0" width="800px" class="fondo_listado" align="center">
		<tr class="encabezado">
			<td width="3px"><img src="../images/esquina_izquierda.gif"></td>
			<td width="100" align="center">Folio</td>
			<td align="center" width="75">Fecha</td>
			<td align="left" width="350">Proveedor</td>
			<td align="center" width="75">Estatus</td>
			<td align="center" width="200">Comentario</td>
			<td align="center" width="200">Cancelacion</td>
			<td align="center" width="100">Importe</td>
         	<td width="1%" align="center">&nbsp;</td>
			
        	<td width="3px"><img src="../images/esquina_derecha.gif"></td>
		</tr>
		<cfoutput query="RSOrdenesCompra.rs" <!---startRow="#StartRow_RSOrdenesCompra#" maxRows="#MaxRows_RSOrdenesCompra#"---> group="id_Empresa">
        	<tr style="background-color:##999999">
                <td align="left" colspan="11" style="font-weight:bold;font-size:14px">#nb_Empresa#</td>								
            </tr >
           <cfoutput group="id_OrdenCompra">
			<tr class="renglon_grid">
				<td background="../images/orilla_izquierda.gif" style="background-repeat:repeat-y" width="3px"></td>
				<td class="cuadricula" align="right">#RSOrdenesCompra.rs.id_OrdenCompra#&nbsp;</td>
				<td class="cuadricula" align="center">#DateFormat(RSOrdenesCompra.rs.fh_OrdenCompra,"dd/mm/yyyy")#</td>
				<td class="cuadricula" align="left">#RSOrdenesCompra.rs.nb_ProveedorCorto#</td>
				<td class="cuadricula" align="center">#RSOrdenesCompra.rs.de_Estatus#</td>
				<td class="cuadricula" align="center">#RSOrdenesCompra.rs.de_Comentarios#</td>
				<td class="cuadricula" align="center">#RSOrdenesCompra.rs.de_Cancelacion#</td>
				<td class="cuadricula" align="right">$#NumberFormat(RSOrdenesCompra.rs.im_Total,",_.__")#</td>
				<td width="1%" class="cuadricula">
					<table width="100%">
						<tr>
							
							<td width="1%">&nbsp;</td>
							<td width="1%" align="center">
							 	<cfif #RSOrdenesCompra.rs.id_EmpleadoComprador# EQ #session.id_empleado#>
									<a href="ordenes_compra_operaciones.cfm?id_Empresa=#RSOrdenesCompra.rs.id_Empresa#&id_Obra=#RSOrdenesCompra.rs.id_Obra#&id_OrdenCompra=#RSOrdenesCompra.rs.id_OrdenCompra#&Filtro_id_OrdenCompra=#Filtro_id_OrdenCompra#&Filtro_id_Estatus=#Filtro_id_Estatus#&Filtro_id_Proveedor=#Filtro_id_Proveedor#&Filtro_fh_OrdenCompra=#Filtro_fh_OrdenCompra#"><img src="../images/key.png" alt="Clic para realizar operaciones de la orden de compra" border="0"></a>
								<cfelse>
                                	<img src="../images/key_gris.png" border="0">
                                </cfif>
						
							</td>
							<td width="1%">&nbsp;</td>
							<td width="1%" align="center">
								<cfif #RSOrdenesCompra.rs.id_Estatus# EQ 203 OR #RSOrdenesCompra.rs.id_Estatus# EQ 204>
									<!---<a href="rpt_impresion_orden_compra.cfr?id_Empresa=#Session.id_Empresa#&id_Obra=#id_Obra#&id_OrdenCompraInicial=#RSOrdenesCompra.rs.id_OrdenCompra#&id_OrdenCompraFinal=#RSOrdenesCompra.rs.id_OrdenCompra#&Session_ObtenerValores=#URLEncodedFormat(Encrypt(SessionStruct, '#Application.ApplicationName#'))#" target="_blank"><img src="../images/printer.png" alt="Clic para imprimir la orden de compra" border="0"></a>--->
									<a href="rpt_impresion_orden_compra.cfr?id_Empresa=#RSOrdenesCompra.rs.id_Empresa#&id_Obra=#id_Obra#&id_OrdenCompra=#RSOrdenesCompra.rs.id_OrdenCompra#" target="_blank"><img src="../images/printer.png" alt="Clic para imprimir la orden de compra" border="0"></a>                                    
								<cfelse>
									<img src="../images/printer_gris.gif" width="16" height="16" border="0">
								</cfif>                             
							</td>
                            <td>
                            </td>
                            <cfif #RSOrdenesCompra.rs.id_tipoOrdenCompra# EQ 7 and #Session.id_puesto# EQ 104>
                                <td width="1%">&nbsp;</td>
                                <td width="1%" align="center">                            	
                                    <a href="rpt_impresion_orden_compra_viaticos.cfm?id_Empresa=#RSOrdenesCompra.rs.id_Empresa#&id_Obra=#id_Obra#&id_OrdenCompra=#RSOrdenesCompra.rs.id_OrdenCompra#" target="_self"><img src="../images/Modify.png" alt="Clic para imprimir el Detalle" border="0" width="24" height="24"></a>
                                </td>
                            </cfif>   
<!--- 							<td width="1%">&nbsp;</td>
							<td width="1%" align="center">
                            <cfif #RSOrdenesCompra.rs.id_Estatus# NEQ 202>
                            <cfset ruta='compras/cotizaciones/' >
                            <cfset archivo='#RSOrdenesCompra.rs.id_Empresa##RSOrdenesCompra.rs.id_Obra##RSOrdenesCompra.rs.id_OrdenCompra#'>
 								<a href="descargas.cfm?ruta=#ruta#&archivo=#archivo#&Filtro_id_Proveedor=#Filtro_id_Proveedor#&Filtro_id_OrdenCompra=#Filtro_id_OrdenCompra#&Filtro_fh_OrdenCompra=#Filtro_fh_OrdenCompra#">Descargar</a>
                            <cfelse>
                            	<a style="color:##CCCCCC">Descargar</a>
                            </cfif>
							</td>  --->                           
						</tr>
					</table>
				</td>
				<td background="../images/orilla_derecha.gif" style="background-repeat:repeat-y" width="3px"></td>
			</tr>
            </cfoutput>
		</cfoutput>
		<!---<tr>
			<td background="../images/orilla_izquierda.gif" style="background-repeat:repeat-y" width="3px"></td>
			<td align="center" colspan="7">
			<table border="0" width="100%" cellpadding="0" cellspacing="0">
			  <cfoutput>
					<tr>
						<td width="20%" align="center">
							<cfif PageNum_RSOrdenesCompra GT 1>
								<a href="ordenes_compra_listado.cfm?id_Obra=#id_Obra#&Filtro_id_OrdenCompra=#Filtro_id_OrdenCompra#&Filtro_id_Estatus=#Filtro_id_Estatus#&Filtro_id_Proveedor=#Filtro_id_Proveedor#&Filtro_fh_OrdenCompra=#Filtro_fh_OrdenCompra#&sn_Mostrar=#sn_Mostrar#&PageNum_RSOrdenesCompra=1"><img src="../images/primero.png" border="0"></a>
							</cfif>
						</td>
						<td width="20%" align="center">
							<cfif PageNum_RSOrdenesCompra GT 1>
								<a href="ordenes_compra_listado.cfm?id_Obra=#id_Obra#&Filtro_id_OrdenCompra=#Filtro_id_OrdenCompra#&Filtro_id_Estatus=#Filtro_id_Estatus#&Filtro_id_Proveedor=#Filtro_id_Proveedor#&Filtro_fh_OrdenCompra=#Filtro_fh_OrdenCompra#&sn_Mostrar=#sn_Mostrar#&PageNum_RSOrdenesCompra=#Max(DecrementValue(PageNum_RSOrdenesCompra),1)#"><img src="../images/anterior.png" border="0"></a>
							</cfif>
						</td>
						 <td width="20%" align="center">	
							Registro #StartRow_RSOrdenesCompra# al #EndRow_RSOrdenesCompra# de #RSOrdenesCompra.rs.RecordCount# 
						</td>
						<td width="20%" align="center">
							<cfif PageNum_RSOrdenesCompra LT TotalPages_RSOrdenesCompra>
								<a href="ordenes_compra_listado.cfm?id_Obra=#id_Obra#&Filtro_id_OrdenCompra=#Filtro_id_OrdenCompra#&Filtro_id_Estatus=#Filtro_id_Estatus#&Filtro_id_Proveedor=#Filtro_id_Proveedor#&Filtro_fh_OrdenCompra=#Filtro_fh_OrdenCompra#&sn_Mostrar=#sn_Mostrar#&PageNum_RSOrdenesCompra=#Min(IncrementValue(PageNum_RSOrdenesCompra),TotalPages_RSOrdenesCompra)#"><img src="../images/siguiente.png" border="0"></a>
							</cfif>
						</td>
						<td width="20%" align="center">
							<cfif PageNum_RSOrdenesCompra LT TotalPages_RSOrdenesCompra>
								<a href="ordenes_compra_listado.cfm?id_Obra=#id_Obra#&Filtro_id_OrdenCompra=#Filtro_id_OrdenCompra#&Filtro_id_Estatus=#Filtro_id_Estatus#&Filtro_id_Proveedor=#Filtro_id_Proveedor#&Filtro_fh_OrdenCompra=#Filtro_fh_OrdenCompra#&sn_Mostrar=#sn_Mostrar#&PageNum_RSOrdenesCompra=#TotalPages_RSOrdenesCompra#"><img src="../images/ultimo.png" border="0"></a>
							</cfif>
						</td>
					</tr>
			  </cfoutput>
			</table>
			<td background="../images/orilla_derecha.gif" style="background-repeat:repeat-y" width="3px"></td>
		</tr>--->
		<tr style="background-image:url(../images/abajo.gif)">
			<td width="3px"><img src="../images/abajo_izquierda.gif"></td>
			<td colspan="7"></td>
			<td width="3px"><img src="../images/abajo_derecha.gif"></td>
		</tr>
	</table>
</cfif> 
<!--Lanzador Fecha-->
<script type="text/javascript">
	Calendar.setup({
		inputField     :    "Filtro_fh_OrdenCompra",      // id del campo de texto
		ifFormat       :    "%d/%m/%Y",       // formato de la fecha, cuando se escriba en el campo de texto
		button         :    "lanzador"   // el id del bot�n que lanzar� el calendario
	});
</script>
</body>
</html>