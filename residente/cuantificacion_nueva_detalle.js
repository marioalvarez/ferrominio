var datos = new Array();

function obtenerPartidas( celda, opcion, id_Tipo, fila, idu_Partida )
{
	var ruta = '../componentes/cuantificaciones.cfc?method=CuantificacionListadoPartidasAjax&idu_TipoCuantificacion=' + id_Tipo + '&sn_Residente=S';
	
	$.ajax
	({
		url: 		ruta,
		type: 		'GET',
		dataType:	'json',
		success:	function( resultado )
					{			
						
						$.each
						(
							resultado,
							function( i, j ) // Recorre los elementos del resultado
							{
								if( i == 'DATA' )
								{
									var control = '<select id="id_Partida_'+ fila +'" style="width: 140px; " class="comboMultilinea">';									
										control += '<option value="" selected="selected">SELECCIONE UN GRUPO</option>';
									
									$.each
									(
										j,
										function( k, l ) // Recorre las filas del query
										{
											
											if( idu_Partida != 'undefined' && l[ 0 ] == idu_Partida )
												control += '<option value="'+ l[ 0 ] +'" selected="selected">'+ l[ 0 ] +' - '+ l[ 1 ] +'</option>';
											else
												control += '<option value="'+ l[ 0 ] +'">'+ l[ 0 ] +' - '+ l[ 1 ] +'</option>';
										}										
									);
									control += '</select>';
									celda.innerHTML = control;
									
									if( opcion > 1 )
										$( '#id_Partida_'+ fila ).attr('disabled', 'disabled').button('disable');
								}										
							}
						);									
					}
				});
				
}

/*function obtenerFrentes( id_Area, id_Empresa, id_Frente, fila )
{
	document.getElementById( 'id_Empresa_' + fila ).setAttribute( 'title', '' );
	$( '#id_Empresa_' + fila ).css({ borderColor: '' });

	if( id_Empresa == '' )
	{
		alert( 'Es necesario que seleccione una empresa para el insumo' );
		document.getElementById( 'id_Empresa_' + fila ).setAttribute( 'title', 'Es necesario seleccionar una empresa' );
		$( '#id_Empresa_' + fila ).css({ borderColor: 'red' });
		return;
	}
	
	var ruta = '../componentes/cuantificaciones.cfc?method=CuantificacionListadoFrentesAjax&idu_Area='+ id_Area +'&idu_Empresa='+ id_Empresa +'&idu_Frente=' + id_Frente;
	$.ajax
	({
		url: 		ruta,
		type: 		'GET',
		dataType:	'json',
		success:	function( resultado )
					{			
						
						$.each
						(
							resultado,
							function( i, j ) // Recorre los elementos del resultado
							{
								if( i == 'DATA' )
								{
									if( j < 1 )
									{
										alert( 'El sub análisis que ingreso no es válido' );
										$( '#id_Frente_' + fila ).val( '' ); 
										return
									}
								}										
							}
						);									
					}
	});
				
}*/

function obtenerFrentes( numFilas, id_Area, id_Frente, id_Empresa )
{
	if( typeof id_Empresa === 'undefined' )
		id_Empresa = $( '#id_Empresa_'+ numFilas ).val();
	
	var ruta = '../componentes/cuantificaciones.cfc?method=CuantificacionListadoFrentesResidenteAjax&idu_Area='+ id_Area +'&idu_Empresa='+ id_Empresa;
	
	
	$.ajax
	({
		url: 		ruta,
		type: 		'GET',
		dataType:	'json',
		success:	function( resultado )
					{									
						document.getElementById( 'id_Frente_' + numFilas ).options.length = 0;
						var obj = document.getElementById( 'id_Frente_' + numFilas );
						var contador = 0;
						$.each
						(
							resultado,
							function( i, j ) // Recorre los elementos del resultado
							{
								if( i == 'DATA' )
								{
									
									$.each
									(
										j,
										function( k, l )
										{
											var opt = document.createElement( 'option' );
											opt.value = l[ 0 ];
		                					opt.text=l[ 0 ];
											obj.appendChild( opt );
											if( obj[ contador ].value == id_Frente )
											{
												obj.selectedIndex = contador;
											}
											contador ++;
										}
									);
																	
								}										
							}
						);	
						
											
					}
	});
}

function borrarDetalle( registro, fila )
{
	
	var ruta = '../componentes/cuantificaciones.cfc?method=CuantificacionBorrarDetalleAjax&detalle=' + registro;
	$.ajax
	({
		url: 		ruta,
		type: 		'GET',
		dataType:	'json',
		success:	function( resultado )
					{			
						console.log( resultado );
						
						if( resultado == '' )
						{
							
							alert( 'El insumo ya fue eliminado de la cuantificacion' );
						}
						else
							alert( resultado );						
					}
	});
}

function creaFila( opcion, idu_Empresa, idu_Frente, idu_Partida, idu_Insumo, des_Insumo, des_UnidadMedida, nu_Cantidad, id_Tipo, opc_Valido, idu_Area, des_MedidaEspecial, sn_Borrador )
{
	var tabla=document.getElementById("tbInsumos");
	var numFilas = tabla.rows.length;
	
	var fila=tabla.insertRow(numFilas); // Crea la fila	
	var celda0	= fila.insertCell(0);
	var celda1	= fila.insertCell(1);
	var celda2	= fila.insertCell(2);
	var celda3	= fila.insertCell(3);
	var celda4	= fila.insertCell(4);
	var celda5	= fila.insertCell(5);
	var celda6	= fila.insertCell(6);
	var celda7	= fila.insertCell(7);
	var celda8	= fila.insertCell(8);
	var celda9  = fila.insertCell(9);
	var celda10 = fila.insertCell(10);
	
	fila.id = 'fila_' + numFilas; // Asigna IDs a la fila y sus celdas
	celda0.id 	= numFilas + '0'
	celda1.id 	= numFilas + '1';
	celda2.id 	= numFilas + '2';
	celda3.id 	= numFilas + '3';
	celda4.id 	= numFilas + '4';
	celda5.id 	= numFilas + '5';
	celda6.id 	= numFilas + '6';
	celda7.id 	= numFilas + '7';
	celda8.id 	= numFilas + '8';
	celda9.id 	= numFilas + '9';
	celda10.id	= numFilas + '10';
	
	celda0.className	= 'cuadricula'; // Aplica el estilo
	celda1.className	= 'cuadricula';
	celda2.className	= 'cuadricula';
	celda3.className	= 'cuadricula';
	celda4.className	= 'cuadricula';
	celda5.className	= 'cuadricula';
	celda6.className	= 'cuadricula';
	celda7.className	= 'cuadricula';
	celda8.className	= 'cuadricula';
	celda9.className	= 'cuadricula';
	celda10.className	= 'cuadricula';
	//alert( 'area: ' + idu_Area );
	
	var aux_Frente = null;	
	if( idu_Frente.length != 0 )
		aux_Frente = idu_Frente;
	else
		aux_Frente = 0;
		
		//alert( idu_Frente + '\n' + aux_Frente );
	
	var comboEmpresa 	='<select id="id_Empresa_'+ numFilas +'" class="comboMultilinea cajaTexto" onchange="obtenerFrentes( '+ numFilas +', '+ idu_Area +', '+ aux_Frente +', '+ $( '#id_Empresa_' + numFilas ).val() +' )">';
	if( idu_Empresa == '' )
	{
		comboEmpresa 	+='<option value="" selected="selected">SELECCIONE ...</option>';
		comboEmpresa 	+='<option value="1">COPPEL S.A. DE C.V.</option>';
		comboEmpresa 	+='<option value="3">CONSTRUCTORA</option>';
	}
	else
	{
		comboEmpresa 	+='<option value="">SELECCIONE ...</option>';
		if( idu_Empresa == 1  )
		{
			comboEmpresa 	+='<option value="1" selected="selected">COPPEL S.A. DE C.V.</option>';
			comboEmpresa 	+='<option value="3">CONSTRUCTORA</option>';
			
		}
		else
		{
			comboEmpresa 	+='<option value="1">COPPEL S.A. DE C.V.</option>';
			comboEmpresa 	+='<option value="3" selected="selected">CONSTRUCTORA</option>';
			
		}
	}
	comboEmpresa 	+='</select>';
	
	if( idu_Frente.length != 0 )
				obtenerFrentes( numFilas, idu_Area, idu_Frente, idu_Empresa );
	
	celda0.innerHTML 	= '<td background="../images/orilla_izquierda.gif" style="background-repeat:repeat-y" width="3px"></td>'; 
	celda1.innerHTML	= comboEmpresa;
	celda2.innerHTML	= '<select id="id_Frente_'+ numFilas +'" class="comboMultilinea cajaTexto" style="width:100%;"><option value="">SELECCIONE ...</option></select>';
	//celda2.innerHTML	= '<input type="text" id="id_Frente_'+ numFilas +'" value="'+ idu_Frente +'" style="width:95%; text-align:center;" class="cajaTexto"  onblur="validarFrente( this, '+ idu_Area +' )"/>'; 
	celda3.innerHTML	= '<select id="id_Partida_'+ numFilas +'" style="width:180px; class="cajaTexto"><option value="">SELECCIONE ...</option>'+ $( '#id_Base' ).html() +'</select>';
	celda4.innerHTML	= '<center><label id="id_Insumo_'+ numFilas +'">'+ idu_Insumo +'</label></center>';
	celda5.innerHTML	= '<label id="de_Insumo_'+ numFilas +'">'+ des_Insumo +'</label>';
	celda6.innerHTML	= '<center><label id="de_UnidadMedida_'+ numFilas +'">'+ des_UnidadMedida +'</label></center>';
	celda7.innerHTML	= '<input type="text" id="nu_Cantidad_'+ numFilas +'" style="width:90px; text-align:right;" class="cajaTexto" onkeypress="validarNumero( event, this )"/>';
	
	celda8.innerHTML	= '<input type="text" id="de_MedidaEspecial_'+ numFilas +'" class="cajaTexto" value=""/>';
	celda9.innerHTML	= '<center><button type="button" onclick="borrarFila('+ numFilas +')" title="Clic aqui para borrar la fila"><img src="../images/delete.png"/></button></center>';
	celda10.innerHTML	= '<td background="../images/orilla_derecha.gif" style="background-repeat:repeat-y" width="3px"></td>';
		
	if( opcion == 1 )
	{
		obtenerPartidas( celda3, 1, id_Tipo, numFilas, ''. opcion );
	}
	else
	{
		obtenerPartidas( celda3, opcion, id_Tipo, numFilas, idu_Partida, opcion );
		$( '#nu_Cantidad_' + numFilas ).val( nu_Cantidad );
		
		//if( sn_Borrador != 'S' )
		{
			
			$( '#id_Empresa_' + numFilas ).prop('disabled', 'disabled');
			$( '#id_Frente_' + numFilas ).prop('disabled', 'disabled');
			$( '#id_Partida_' + numFilas ).prop('disabled', 'disabled');
		}
		
	}
	
	
	switch( opc_Valido )
	{
		case 1:
			document.getElementById( fila.id ).setAttribute( 'title', 'El insumo no existe o esta inactivo' );
			break;
			
		case 2:
			document.getElementById( fila.id ).setAttribute( 'title', 'La partida no es valida para este tipo de cuantificacion' );
			break;
			
		default:
			document.getElementById( fila.id ).setAttribute( 'title', '' );
			break;
	}
}

function borrarFila( idFila )
{	
	if( confirm( 'Esta seguro de eliminar este insumo de la lista?' ) )
	{
		var cc = $( '#id_Frente_' + idFila ).val().substring( 0, 1 ) + '00';
		
		if ($('#id_Empresa_' + idFila).val() != '' && $('#idu_Obra').val() != '' && $('#idu_Cuantificacion').val() != '' && $('#id_Frente_' + idFila).val() != '' && $('#id_Partida_' + idFila).val() != '' && $('#id_Insumo_' + idFila).text() != '') {
			$('#detalles').val($('#id_Empresa_' + idFila).val() + ',' + $('#idu_Obra').val() + ',' + $('#idu_Cuantificacion').val() + ',' + cc + ',' + $('#id_Frente_' + idFila).val() + ',' + $('#id_Partida_' + idFila).val() + ',' + $('#id_Insumo_' + idFila).text());
			$( '#sn_Borrar' ).val( 'S' );
		$( '#sn_Enviar' ).val( 'N' );
		borrarDetalle( $( '#detalles' ).val(), idFila );
		}
		
		
		
		$( '#fila_' + idFila ).remove();
	}
}

function obtenerDetalleInsumo( opcion, idu_Insumo, idu_Empresa, idu_Frente, idu_Partida, nu_Cantidad, id_Tipo, opc_Valido, idu_Area, des_MedidaEspecial, sn_Borrador )
{
	var ruta = '../componentes/cuantificaciones.cfc?method=InsumoListadoAjax&id_Insumo=' + idu_Insumo;	
	
	$.ajax
	({	
		url: 	 ruta,					
		type: 	'GET',
		dataType:	'json',
		success:function( resultado )
				{
					
					$.each
					(
						resultado,
						function( i, j)
						{
							if( i == 'DATA' )
							{
								switch( opcion )
								{
									case 1:
										
										if( $( '#sn_Error' ).val() == 'S' )
										{
											alert( 'No se pueden agregar insumos a la cuantificacion porque existen errores' );
											return;
										}
										creaFila( opcion, '', '', '', idu_Insumo, j[0][0], j[0][1], '', id_Tipo, '', idu_Area, '' );
										return;
										break;
										
									case 2:
										
										creaFila( opcion, idu_Empresa, idu_Frente, idu_Partida, idu_Insumo, j[0][0], j[0][1], nu_Cantidad, id_Tipo, opc_Valido, idu_Area, des_MedidaEspecial, sn_Borrador );
										return;
										break;
								}
							}
						}
					);
						
				}
	});
	
}

function cargarArchivo()
{
	$( '#sn_Cargar' ).val( 'S' );
	$( '#sn_Enviar' ).val( 'N' );
	form1.submit();
}

function limpiar()
{
	$('input[type="text"][id^="de_MedidaEspecial_"]' ).each
	(
		function()
		{
			if( $( this ).val() == 'undefined' )
				$( this ).val( '' );
		}
	);
}

function validar( id_Obra )
{
	var datos = '';
	var msg = ''; 
	var valido = true;
	
	$('select[id^="id_Frente_"]' ).each
	(
		function()
		{
			var aux 	= new Array();
			var campo 	= $( this ).attr( 'id' ).split('_');
			var id 		= campo[ campo.length - 1 ]; 
			
			if( $( '#id_Empresa_' + id ).val() == '' )
			{
				valido = false;
				$( '#id_Empresa_' + id ).css({ borderColor: 'red' });
			}
			else
			{
				$( '#id_Empresa_' + id ).css({ borderColor: '' });
				aux.push( $( '#id_Empresa_' + id ).val() );
			}
			
			aux.push( id_Obra );
			
			if( $( '#id_Frente_' + id ).val() == '' )
			{
				valido = false;
				$( '#id_Frente_' + id ).css({ borderColor: 'red' });
			}
			else
			{
				$( '#id_Frente_' + id ).css({ borderColor: '' });
				aux.push( $( '#id_Frente_' + id ).val().substring( 0, 1 ) + '00' );
				aux.push( $( '#id_Frente_' + id ).val() );
			}
			
			if( $( '#id_Partida_' + id ).val() == '' )
			{
				valido = false;
				$( '#id_Partida_' + id ).css({ borderColor: 'red' });
			}
			else
			{
				$( '#id_Partida_' + id ).css({ borderColor: '' });
				aux.push( $( '#id_Partida_' + id ).val() );
			}
			
			if( $( '#id_Insumo_' + id ).text() == '' )
			{
				valido = false;
				$( '#id_Insumo_' + id ).css({ borderColor: 'red' });
			}
			else
			{
				$( '#id_Insumo_' + id ).css({ borderColor: '' });
				aux.push( $( '#id_Insumo_' + id ).text() );
			}
			
			if( $( '#nu_Cantidad_' + id ).val() == '' )
			{
				valido = false;
				$( '#nu_Cantidad_' + id ).css({ borderColor: 'red' });
			}
			else
			{
				$( '#nu_Cantidad_' + id ).css({ borderColor: '' });
				aux.push( $( '#nu_Cantidad_' + id ).val() );
			}
			
			if( $( '#de_MedidaEspecial_' + id ).val() == '' || $( '#de_MedidaEspecial_' + id ).val() =='undefined' )
				aux.push( '!' );
			else
				aux.push( $( '#de_MedidaEspecial_' + id ).val() + " " );
			
			aux.push( $( '#de_Comentarios' ).val() + " " );
			
			datos += aux + '|';
		}
	);
	
	if( $( '#sn_Error' ).val() == 'S' )
	{
		alert( 'El archivo de la cuantificacion contiene errores' );
		return;
	}
	
	if( $( '#ar_Generador' ).val() == '' && $( '#sn_GuardarBorrador' ).val() == 0 )
	{
		alert( 'Es necesario cargar el generador' );
		valido = false;
		return;
	}
	
	if( $( '#sn_GuardarBorrador' ).val() == '' )
	{
		alert( 'Es especificar como se va aguardar' );
		valido = false;
		return;
	}
	
	if( !valido )
	{
		alert( 'Existen errores en la lista de insumos, los campos con borde rojo contienen errores' );
		return;
	}
	
	$( '#detalles' ).val( datos );
	$( '#sn_Enviar' ).val( 'S' );
	$( '#sn_Cargar' ).val( 'N' );
	
	form1.submit();
}

function validarFrente( t, idu_Area )
{
	var aux = $( '#' + t.id ).attr( 'id' ).split( '_' );
	//alert( aux[ aux.length - 1 ] + ' : ' + idu_Area );
	obtenerFrentes( idu_Area, $( '#id_Empresa_' + aux[ aux.length - 1 ] ).val(),  $( '#' + t.id ).val(), aux[ aux.length - 1 ] );
}

function validarNumero(  evt, t )
{
	var theEvent = evt || window.event;
	
	if( theEvent.keyCode == 8 || theEvent.keyCode == 9 || theEvent.keyCode == 13 || theEvent.keyCode == 37 || theEvent.keyCode == 38 || theEvent.keyCode == 39 || theEvent.keyCode == 40 || theEvent.keyCode == 116 )
		return;
		
		var key = theEvent.keyCode || theEvent.which;
	
		key = String.fromCharCode( key );
		
		var regex = /[0-9]|\./;
		if( !regex.test(key) ) 
		{
		theEvent.returnValue = false;
		if(theEvent.preventDefault) 
			theEvent.preventDefault();
		}
		else
		{
		  	var decimal = t.value.split( '.' );	
		  	if( decimal[1].length > 5 )
		  	{
		  	  	theEvent.returnValue = false;
			if(theEvent.preventDefault) 
				theEvent.preventDefault();		  	
		  	}
		}
}