// JavaScript Document	
var tabla_ajax = function(object)
{
	thead = object.thead;
	tbody = object.tbody;
	tfoot = object.tfoot;
	url   = object.url;
	data  = object.data;
	id    = object.id;
	dataSet = [];
	tdh = [];
	tdb = [];
	tdf = [];
	head = "";
	foot = "";
	$.ajax(
		   {
				type: 'GET',
				url: url,
				data: data,
				dataType: 'JSON',
				success: function(data)
				{
					dataSet = new Array();
					/*Metodo para convertir la estructura que recibes del componente y la convierte en arreglo*/
					for(x=0;x<data.RS.DATA.length;x++)
					{					
						var obj = {}
						for(i=0;i<data.RS.DATA[x].length;i++){
							var valor = data.RS.DATA[x][i];
							var propiedad = data.RS.COLUMNS[i];						
							obj[propiedad] = valor;		
						}
						dataSet.push(obj);
					}
					DataSet(dataSet);
				}
		   });
	DataSet = function(data)
	{
		dataSet = data;
		/*Recorre el arreglo y crea las filas dependiendo la informacion que recibe*/
		for(x=0;x<dataSet.length;x++)
		{
			record = dataSet[x]
			if(thead != null)
			{
				for(h=0;h<thead.length;h++)
				{
					if(thead[h].type == "text")
					{
						style=thead[h].style;
						align=thead[h].align;
						data=record[thead[h].data.toUpperCase()];
						dataName=record[thead[h].dataName.toUpperCase()];
						tdh[h]='<td colspan="'+tbody.length+'" class="cuadricula" style="'+style+'">'+data+'-'+dataName+'</td>';
						
					}
					
				}
				if(data != head)
				{
					$("#"+id ).append("<tr>" + tdh + "</tr>"); 
					head=data;
				}
			}
			for(b=0;b<tbody.length;b++)
			{
				if(tbody[b].type == "text")
				{
					width=tbody[b].width;
					align=tbody[b].align;
					style=tbody[b].style;
					clase=tbody[b].clase;
					value = record[tbody[b].data.toUpperCase()];
					if(tbody[b].dataName != "")value = record[tbody[b].data.toUpperCase()] +'-'+record[tbody[b].dataName.toUpperCase()]
					if(value == null)value="";
					tdb[b]='<td class="'+clase+'" width="'+width+'"  align="'+align+'" style="'+style+'">'+value+'</td>';
					
				}else if(tbody[b].type == "numeric")
				{
					width=tbody[b].width;
					align=tbody[b].align;
					style=tbody[b].style;
					clase=tbody[b].clase;
					value = record[tbody[b].data.toUpperCase()];
					if(tbody[b].dataName != "")value = record[tbody[b].data.toUpperCase()] +'-'+record[tbody[b].dataName.toUpperCase()];
					value = parseFloat(value).toFixed(6);
					if(value == null)value="";
					tdb[b]='<td class="'+clase+'" width="'+width+'"  align="'+align+'" style="'+style+'" nowrap="nowrap">'+value+'</td>';
				}else if(tbody[b].type == "numberFormat")
				{
					width=tbody[b].width;
					align=tbody[b].align;
					style=tbody[b].style;
					clase=tbody[b].clase;
					value = record[tbody[b].data.toUpperCase()];
					if(tbody[b].dataName != "")value = record[tbody[b].data.toUpperCase()] +'-'+record[tbody[b].dataName.toUpperCase()];
					value = parseFloat(value).toFixed(6);
					punto = "";
					posicion = value.indexOf('.');
					cents = "";
					if(posicion != -1) 
					{
						punto = ".";
						cents = value.substring(posicion);
						if(cents.length >= 4) cents = cents.substring(0, 7);
						posicion += posicion / 3;
						value = value.split('.')[0] + cents;
					}
					value = value.toString().replace(/\$|\,/g,'');
					if(isNaN(value)) value = "0";
					sign = (value == (value = Math.abs(value)));
					value = Math.floor(value*100+0.50000000001);
					value = Math.floor(value/100).toString();
					for (var i = 0; i < Math.floor((value.length-(1+i))/3); i++)
					value = value.substring(0,value.length-(4*i+3))+','+ value.substring(value.length-(4*i+3));
					value= value + ((cents != "00" && cents.length > 0) ? cents : '');
					if(value == null)value="";
					tdb[b]='<td class="'+clase+'" width="'+width+'"  align="'+align+'" style="'+style+'" nowrap="nowrap">$'+value+'</td>';
				}else if(tbody[b].type == "check")
				{
					width=tbody[b].width;
					align=tbody[b].align;
					style=tbody[b].style;
					clase=tbody[b].clase;
					name=tbody[b].name;
					funcion=tbody[b].funcion;
					data=tbody[b].data;
					value=""
					for(d=0;d<data.length;d++)
					{
						if(d >0)value+=','+record[data[d].toUpperCase()];
						else value=record[data[d].toUpperCase()];
					}
					if(value != "")value+=','+x;
					if(value == null)value="";
					tdb[b]='<td class="'+clase+'" width="'+width+'"  align="'+align+'" style="'+style+'" nowrap="nowrap">' +
								'<input type="checkbox" style="'+style+'" id="'+name+x+'" onClick="'+funcion+'('+value+');" value="'+value+'" />' +
							'</td>';
				}else if(tbody[b].type == "input")
				{
					width=tbody[b].width;
					align=tbody[b].align;
					style=tbody[b].style;
					clase=tbody[b].clase;
					tipo=tbody[b].tipo;
					name=tbody[b].name;
					funcion=tbody[b].funcion;
					funcion2=tbody[b].funcion2;
					if(funcion2 != null){
						datos=tbody[b].datos;
						for(d=0;d<datos.length;d++)
						{
							if(d >0)valores+=','+record[datos[d].toUpperCase()];
							else valores=record[datos[d].toUpperCase()];
						}
						valores+=','+x;
						funcion2 = funcion2 +'('+valores+')';
					}
					//var dateObj = new Date();
					//var month = dateObj.getUTCMonth() + 1; //months from 1-12
					//var day = dateObj.getUTCDate();
					//var year = dateObj.getUTCFullYear();
					//newdate = day + "/" + month + "/" + year;
					newdate = $('#fh_Requerido').val();
					value = record[tbody[b].data.toUpperCase()];
					if(isNaN(value))value =value;
					else value = parseFloat(value).toFixed(6);
					if(value == null)value=newdate;
					if(tipo == 'contenido'){
						tdb[b]='<td class="'+clase+'" width="'+width+'"  align="'+align+'" nowrap="nowrap">' +
								'<input type="text" id="'+name+x+'" style="'+style+'" />' +
							'</td>';
					}else{
						tdb[b]='<td class="'+clase+'" width="'+width+'"  align="'+align+'" nowrap="nowrap">' +
								'<input type="'+tipo+'" id="'+name+x+'" style="'+style+'" onClick="this.select();" value="'+value+'" onBlur="'+funcion2+'" onKeyPress="'+funcion+'"/>' +
							'</td>';
					}
						
							
				}else if(tbody[b].type == "hidden")
				{
					width=tbody[b].width;
					align=tbody[b].align;
					style=tbody[b].style;
					clase=tbody[b].clase;
					name=tbody[b].name;
					value = record[tbody[b].data.toUpperCase()];
					if(value == null)value="";
					tdb[b]='<input type="hidden" id="'+name+x+'" style="'+style+'" value="'+value+'" />' 
				}else if(tbody[b].type == "img")
				{
					width=tbody[b].width;
					align=tbody[b].align;
					style=tbody[b].style;
					clase=tbody[b].clase;
					name=tbody[b].name;
					funcion=tbody[b].funcion;
					data=tbody[b].data;
					src=tbody[b].src;
					value="";
					numeros="1234567890";
					for(d=0;d<data.length;d++)
					{
						rec = record[data[d].toUpperCase()];
						rec = parseInt(rec);
						if(isNaN(rec))rec = "'"+record[data[d].toUpperCase()]+"'";
						if(d >0)value+=','+rec;
						else value=rec;
					}
					
					if(value != "")value+=','+x;
					if(value == null)value="";
					tdb[b]='<td class="'+clase+'" width="'+width+'"  align="'+align+'" style="'+style+'" nowrap="nowrap">' +
								'<button type="button" id="'+name+x+'" onClick="'+funcion+'('+value+');" >' +
									'<img src="'+src+'"/>'+
								'</button>'+	
							'</td>';
				}
			}
			$("#"+id).append("<tr >" + tdb + "</tr>");
			if(tfoot != null)
			{
				if(tfoot == "")
				{
					$("#"+ id ).append( "<tr id='"+id+"_"+x+"'></tr>" );
				}else
				{
					for(f=0;f<tfoot.length;f++)
					{
						if(tfoot[f].type == "text")
						{
							colspan=tbody.length - 1;
							style=tfoot[f].style;
							align=tfoot[f].align;
							data=record[tfoot[f].data.toUpperCase()];
							dataName=tfoot[f].dataName.toUpperCase();
							data = parseFloat(data).toFixed(6);
							tdf[f]='<tr ><td class="cuadricula" colspan="'+colspan+'" align="right" style="'+style+'">'+dataName+':</td>'+
										'<td class="cuadricula" align="'+align+'" style="'+style+'">'+data+'</td>';			
						}
					}
					
					group = tfoot[tfoot.length - 1];
					if(x+1 <= dataSet.length)
					{
						if(x+1 == dataSet.length)group="";else
						foot=dataSet[x+1][group.toUpperCase()];
					}
					
					group = record[group.toUpperCase()];
					if(group != foot)
					{
						$("#"+ id ).append( ""+tdf+"" );
					}
				}
				
			}
			
		}
		CargarControladores();
	}
	
}
function validarNumero( event, t )
{
	//$(t).val($(t).val().replace(/[^0-9\.]/g,''));
	if ((event.which != 46 || $(t).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) 
	{
		event.preventDefault();
	}
}

function validarFecha( event, t )
{
	if ((event.which != 46 || $(t).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) 
	{
		event.preventDefault();
	}
	if($(t).val().length == 2 || $(t).val().length == 5){
			//console.log($(t).val())
			$(t).val($(t).val()+'/')
	}
	if($(t).val().length > 9)
	{
		event.preventDefault();
	}
}
