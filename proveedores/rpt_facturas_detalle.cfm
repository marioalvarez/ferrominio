<cfinvoke component="#Application.componentes#.contrarecibosdetalle" method="RSMostrarContrarecibosReporte" returnvariable="reporte_CC"
	id_Proveedor="#id_Proveedor#"
    id_Estatus="#id_Estatus#"
>
<cfif #reporte_CC.tipoError# NEQ "">
    <cf_error_manejador tipoError="#reporte_CC.tipoError#" mensajeError="#reporte_CC.mensaje#">
</cfif>

<table cellpadding="0" cellspacing="0" width="100%" id="tbl_Principal" class="" style="font-size:9px">
    <tr class="encabezado_chico">
        <td>EMPRESA</td>
        <td>OBRA</td>
        <td>OC</td>
        <td>FOLIO FACTURA</td>

        <td>SUBTOTAL</td>
        <td>IVA</td>
        <td>RETENCION IVA</td>
        <td>ISR</td>
        <td>NOTA CREDITO</td>
        <td>TOTAL</td>
        
        <td>ESTATUS</td>
        <td>TIPO FACTURA</td>
        <td>FECHA FACTURA</td>
        <td>OBSERVACIONES</td>
    </tr>
	<cfoutput query="reporte_CC.rs" >
        <tr onMouseOver="this.style.backgroundColor=''" onMouseOut="this.style.backgroundColor=''">
            <td class="cuadricula">#nb_Empresa#</td>
            <td class="cuadricula">#id_Obra# - #de_Obra#</td>
            <td class="cuadricula" nowrap="nowrap">#id_OrdenCompra# - [#NumberFormat(im_TotalOC,'$,_.__')#]</td>
            <td class="cuadricula">#de_Factura#</td>
            
            <td class="cuadricula" align="right">#NumberFormat(im_SubTotal,'$,_.__')#</td>
            <td class="cuadricula" align="right">#NumberFormat(im_Iva,'$,_.__')#</td>
            <td class="cuadricula" align="right">#NumberFormat(im_RetencionIva,'$,_.__')#</td>
            <td class="cuadricula" align="right">#NumberFormat(isr,'$,_.__')#</td>
            <td class="cuadricula" align="right">#NumberFormat(im_NotaCredito,'$,_.__')#</td>
            <td class="cuadricula" align="right">#NumberFormat(im_Factura,'$,_.__')#</td>
            
            <td class="cuadricula" align="center">#de_Estatus#</td>
            <td class="cuadricula" align="center">#de_TipoFactura#</td>
            <td class="cuadricula" align="center">#DateFormat(fh_Factura,'DD/MM/YYYY')#</td>
            <td class="cuadricula">#de_Observaciones#</td>
        </tr>
    </cfoutput>
</table>
