<cfset CurrentPage=GetFileFromPath(GetTemplatePath())>
<cfparam name="id_Proveedor" default="">
<cfparam name="sn_Enviar" default="N">

<!---Invokes --->
<cfinvoke component="#Application.componentes#.proveedores" method="get_Proveedores_ReducirValidaciones_XML" returnvariable="RSListado"
          id_Proveedor="#id_Proveedor#">
<!---Preguntar por errores --->
<cfif NOT RSListado.SUCCESS>
    <cf_error_manejador  tipoError="#RSListado.tipoError#" mensajeError="#RSListado.MESSAGE#">
</cfif>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<link rel="stylesheet" href="../css/style.css" type="text/css">
<link rel="stylesheet" type="text/css" href="../css/box.css">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title></title>
<script language="javascript" src="../js/jquery.js"></script>
<script language="javascript" src="../js/funciones.js"></script>
</head>

<body>
<cfinclude template="menu.cfm">&nbsp;
 
    <!---Titulo --->
    <table style="width:800px" align="center">
        <tr class="titulo">
            <td align="justify">Listado de proveedores con reducciones en la validacion de XML</td>
        </tr>
    </table>
    <!--- Panel principal --->
    <table width="800px" cellpadding="0" cellspacing="0" align="center">
        <tr>
            <td class="x-box-tl" width="1px">
            </td>
            <td class="x-box-tc">
            </td>
            <td class="x-box-tr" width="1px">
            </td>
        </tr>
        <tr>
            <td class="x-box-ml">
            </td>
            <td class="x-box-mc" width="99%" nowrap="nowrap">
                <cfform name="frmFiltro" method="post">
                    <table width="100%">
                        <tr>
                            <td width="130px">
                                <button type="button" class="boton_menu_superior" onclick="location='proveedores_reducirvalidaciones_xml_agregar.cfm';" title="Click para agregar"><img src="../images/nuevo.png" alt="" width="24"></button>
                            </td>
                            <td align="right" style="vertical-align: middle;">
                                <b>Proveedores:</b>
                            </td>
                            <td align="left">
                                <cfinput type="text" class="contenido_numerico" name="id_Proveedor" id="id_Proveedor" style="width:80px;vertical-align: middle;" validate="integer" size="10" value="#id_Proveedor#"  required="false" message="El proveedor no es v\u00e1lido.">
                                <cfinput type="text" class="contenido_readOnly" name="de_Proveedor" id="de_Proveedor" style="width:300px;vertical-align: middle;" readonly bind="cfc:#Application.componentes#.proveedores.getProveedorPorID({id_Proveedor@blur})" onFocus="cambiarFocus($(this).before());" bindonload="yes">
                                <button type="button" id="btnBuscarProveedor" onClick="javascript:AbrirPopUpBusquedaChrome('id_Proveedor', 'pop_proveedores_busqueda.cfm', '', '700px', '600px')" class="boton_popup" value=""><img src="../images/buscar.png" style="width:18px;vertical-align: middle;"></button>
                            </td>
                            <td valign="center" align="center"  width="300px">
                                <button type="submit" id="btn_Filtrar" class="boton_imagen" title="Clic para aplicar filtro" onclick="" style="vertical-align: middle;">
                                    <img src="../images/filtro.png">
                                </button>
                            </td>
                        </tr>
                    </table>
                    <cfinput type="hidden" name="sn_Enviar" value="S">
                </cfform>
            </td>
            <td class="x-box-mr">
            </td>
        </tr>
        <tr>
            <td class="x-box-bl" width="1px">
            </td>
            <td class="x-box-bc">
            </td>
            <td class="x-box-br" width="1px">
            </td>
        </tr>
    </table>
    <br />
    <table width="800px" border="0" cellpadding="0" cellspacing="0" class="fondo_listado" align="center">
        <thead>
            <tr class="encabezado">
                <td width="3px"><img src="../images/esquina_izquierda.gif" /></td>
                
                <td align="center">Proveedor</td>
                <td align="center">RFC</td>
                <td align="center"></td>
                
                <td width="3px"><img src="../images/esquina_derecha.gif" /></td>
            </tr>
        </thead>
        <tbody>
            <cfoutput query="RSListado.RS">
                <tr id="tr_#id_Proveedor#" onMouseOver="this.style.backgroundColor='##99CCCC'" onMouseOut="this.style.backgroundColor=''" >
                    <td background="../images/orilla_izquierda.gif" style="background-repeat:repeat-y" width="3px"></td>
                    
                    <td class="cuadricula" align="left" nowrap="nowrap">#RSListado.RS.nb_Proveedor#</td>
                    <td class="cuadricula" align="center" nowrap="nowrap">#RSListado.RS.de_RFC#</td>
                    <td class="cuadricula" align="center" style="width:20px;">
                        <button type="button" onclick="delete_Proveedor( #id_proveedor# )" title="Clic aqui para eliminar"><img src="../images/delete.png"></button>
                    </td>
                    
                    <td background="../images/orilla_derecha.gif" style="background-repeat:repeat-y" width="3px"></td>
                </tr>
            </cfoutput>
        </tbody>
        <tfoot>
            <tr style="background-image:url(../images/abajo.gif)">
                <td background="../images/abajo_izquierda.gif" style="background-repeat:repeat-y" width="3px"></td>
                <td align="center" colspan="3" style="height:6px"></td>
                <td background="../images/abajo_derecha.gif" style="background-repeat:repeat-y" width="3px"></td>   
            </tr>
        </tfoot>
    </table>
    <br />
</body>
</html>
<script>
    function delete_Proveedor( idProveedor)
    {
        if  ( confirm('\u00bfEsta seguro que desea eliminar este proveedor del listado?') )
        {
            $.ajax({
                type: 'post',
                async: true,
                dataType: 'json',
                url: '../componentes/proveedores.cfc',
                data: {
                    method:         'Proveedores_ReducirValidaciones_XML_Eliminar',
                    id_Proveedor:   idProveedor
                },
                success: function(response){
                    if (response.SUCCESS) 
                    {
                        alert(response.MESSAGE);
                        $('#tr_'+ idProveedor).remove();
                    }
                    else
                    {
                        alert(response.MESSAGE);
                    }
                    
                },
                error: function()
                {
                    alert('No se pudo borrar el Proveedor.\n\n\nFavor de intentar mas tarde.');
                }
            });
        }
    }
</script>