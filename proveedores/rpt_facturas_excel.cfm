<cfparam name="Filtro_id_Empresa" default="">
<cfparam name="id_Obra" default="">
<cfparam name="id_Proveedor" default="">
<cfparam name="id_Proveedor" default="">
<cfparam name="id_OrdenCompra" default="">
<cfparam name="cl_TipoFactura" default="">
<cfparam name="de_Factura" default="">
<cfparam name="de_Estatus" default="511,519,521">
<cfparam name="id_Tipo" default="1">

	<cfinvoke component="#Application.componentes#.contrarecibosdetalle" method="RSMostrarContrarecibosReporte" returnvariable="reporte_CC"
		id_Empresa="#Filtro_id_Empresa#"
		id_Obra="#id_Obra#"
        id_Proveedor="#id_Proveedor#"
        id_Tipo = "#id_Tipo#"
        id_Estatus="#de_Estatus#"
	>
	<cfif #reporte_CC.tipoError# NEQ "">
		<cf_error_manejador tipoError="#reporte_CC.tipoError#" mensajeError="#reporte_CC.mensaje#">
	</cfif>

    <!--- <cfif id_Tipo EQ 1>
		<cfif #de_Estatus# EQ ''>    
            <cfset Where = '' >
        <cfelse>
            <cfset Where = 'WHERE id_EstatusFactura IN(' & #de_Estatus# &')' >
        </cfif>
        <cfquery name="reporte_CC.rs" dbtype="query">
            SELECT
               * 
            FROM
                reporte_CC.rs
            #Where#
        </cfquery>
	</cfif> --->	

<cfsavecontent variable="contenido">
		<cfif id_Tipo EQ 1>
            <table cellpadding="0" cellspacing="0" width="100%" id="tbl_Principal" class="fondo_listado" style="font-size:9px" border="1">
                <tr style="background:#999999;text-align:center">
                    <td>EMPRESA</td>
                    <td>OBRA</td>
                    <td>PROVEEDOR</td>
                    <td>OC</td>
                    <td>FOLIO FACTURA</td>
        
                    <td>SUBTOTAL</td>
                    <td>IVA</td>
                    <td>RETENCION IVA</td>
                    <td>ISR</td>
                    <td>NOTA CREDITO</td>
                    <td>TOTAL</td>
                    
                    <td>ESTATUS</td>
                    <td>TIPO FACTURA</td>
                    <td>FECHA FACTURA</td>
                    <td>OBSERVACIONES</td>
                </tr>
                    <cfoutput query="reporte_CC.rs">
                        <tr>
                            <td>#nb_Empresa#</td>
                            <td>#id_Obra# - #de_Obra#</td>
                            <td>#reporte_CC.rs.id_Proveedor# - #nb_Proveedor#</td>
                            <td nowrap="nowrap">#id_OrdenCompra# - [#NumberFormat(im_TotalOC,'$,_.__')#]</td>
                            <td>#de_Factura#</td>
                            
                            <td align="right">#NumberFormat(im_SubTotal,'$,_.__')#</td>
                            <td align="right">#NumberFormat(im_Iva,'$,_.__')#</td>
                            <td align="right">#NumberFormat(im_RetencionIva,'$,_.__')#</td>
                            <td align="right">#NumberFormat(isr,'$,_.__')#</td>
                            <td align="right">#NumberFormat(im_NotaCredito,'$,_.__')#</td>
                            <td align="right">#NumberFormat(im_Factura,'$,_.__')#</td>
                            
                            <td align="center">#de_Estatus#</td>
                            <td align="center">#de_TipoFactura#</td>
                            <td align="center">#DateFormat(fh_Factura,'DD/MM/YYYY')#</td>
                            <td>#de_Observaciones#</td>
                        </tr>
                    </cfoutput>
                    <tr style="font-weight:bold">
                        <td colspan="10" align="right">TOTAL FACTURADO: </td>
                        <td align="right"><cfoutput>#NumberFormat(ArraySum(ListToArray(ValueList(reporte_CC.rs.im_Factura))),'$,_.__')#</cfoutput></td>
                        <td colspan="4"></td>
                    </tr>
                </table>
		<cfelse>
            <table cellpadding="0" cellspacing="0" width="100%" id="tbl_Principal" class="fondo_listado" style="font-size:9px" border="1">
                <tr style="background:#999999;text-align:center">
                    <td colspan="8">PROVEEDOR</td>
                    <td>SUBTOTAL</td>
                    <td>IVA</td>
                    <td>RETENCION IVA</td>
                    <td>ISR</td>
                    <td>NOTA CREDITO</td>
                    <td>TOTAL</td>
                </tr>
				<cfoutput query="reporte_CC.rs">
                    <tr>
                        <td colspan="8">#reporte_CC.rs.id_Proveedor# - #nb_Proveedor#</td>
                        <td align="right">#NumberFormat(im_SubTotal,'$,_.__')#</td>
                        <td align="right">#NumberFormat(im_Iva,'$,_.__')#</td>
                        <td align="right">#NumberFormat(im_RetencionIva,'$,_.__')#</td>
                        <td align="right">#NumberFormat(isr,'$,_.__')#</td>
                        <td align="right">#NumberFormat(im_NotaCredito,'$,_.__')#</td>
                        <td align="right">#NumberFormat(im_Factura,'$,_.__')#</td>
                    </tr>
                    <cfinvoke component="#Application.componentes#.contrarecibosdetalle" method="RSMostrarContrarecibosReporte" returnvariable="reporte_detalle"
                        id_Proveedor="#reporte_CC.rs.id_Proveedor#"
                        id_Estatus="#de_Estatus#"
                    >
                    <cfif #reporte_detalle.tipoError# NEQ "">
                        <cf_error_manejador tipoError="#reporte_detalle.tipoError#" mensajeError="#reporte_detalle.mensaje#">
                    </cfif>

                        <tr class="encabezado_chico" style="background:##99CCCC;text-align:center">
                            <td>EMPRESA</td>
                            <td>OBRA</td>
                            <td>OC</td>
                            <td>FOLIO FACTURA</td>
                    
                            <td>SUBTOTAL</td>
                            <td>IVA</td>
                            <td>RETENCION IVA</td>
                            <td>ISR</td>
                            <td>NOTA CREDITO</td>
                            <td>TOTAL</td>
                            
                            <td>ESTATUS</td>
                            <td>TIPO FACTURA</td>
                            <td>FECHA FACTURA</td>
                            <td>OBSERVACIONES</td>
                        </tr>
                        <cfloop query="reporte_detalle.rs" >
                            <tr>
                                <td>#nb_Empresa#</td>
                                <td>#id_Obra# - #de_Obra#</td>
                                <td nowrap="nowrap">#id_OrdenCompra# - [#NumberFormat(im_TotalOC,'$,_.__')#]</td>
                                <td>#de_Factura#</td>
                                
                                <td align="right">#NumberFormat(im_SubTotal,'$,_.__')#</td>
                                <td align="right">#NumberFormat(im_Iva,'$,_.__')#</td>
                                <td align="right">#NumberFormat(im_RetencionIva,'$,_.__')#</td>
                                <td align="right">#NumberFormat(isr,'$,_.__')#</td>
                                <td align="right">#NumberFormat(im_NotaCredito,'$,_.__')#</td>
                                <td align="right">#NumberFormat(im_Factura,'$,_.__')#</td>
                                
                                <td align="center">#de_Estatus#</td>
                                <td align="center">#de_TipoFactura#</td>
                                <td align="center">#DateFormat(fh_Factura,'DD/MM/YYYY')#</td>
                                <td>#de_Observaciones#</td>
                            </tr>
                        </cfloop>
                </cfoutput>
                <tr style="font-weight:bold">
                    <td colspan="9" align="right">TOTAL FACTURADO: </td>
                    <td align="right"><cfoutput>#NumberFormat(ArraySum(ListToArray(ValueList(reporte_CC.rs.im_Factura))),'$,_.__')#</cfoutput></td>
                </tr>
            </table>                
		</cfif>
</cfsavecontent>
<cfoutput>
    #contenido#
</cfoutput>

<cfheader name="Content-Disposition" value="inline; filename=Reporte_facturas_#DateFormat(Now(),'DD_MM_YYYY')#.xls" >
<cfcontent variable="#ToBinary(ToBase64(contenido))#" type="application/msexcel" >  