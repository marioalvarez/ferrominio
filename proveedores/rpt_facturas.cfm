<cfset CurrentPage=GetFileFromPath(GetTemplatePath())>
<cfparam name="sn_Mostrar" default="N">
<cfparam name="Filtro_id_Empresa" default="">
<cfparam name="id_Obra" default="">
<cfparam name="id_Proveedor" default="">
<cfparam name="id_OrdenCompra" default="">
<cfparam name="cl_TipoFactura" default="">
<cfparam name="de_Factura" default="">
<cfparam name="de_Estatus" default="511,519,521">
<cfparam name="id_Tipo" default="1">

<cfif #sn_Mostrar# EQ 'S'>
	<cfinvoke component="#Application.componentes#.contrarecibosdetalle" method="RSMostrarContrarecibosReporte" returnvariable="reporte_CC"
		id_Empresa="#Filtro_id_Empresa#"
		id_Obra="#id_Obra#"
        id_Proveedor="#id_Proveedor#"
        id_Tipo = "#id_Tipo#"
        id_Estatus="#de_Estatus#"
	>
	<cfif #reporte_CC.tipoError# NEQ "">
		<cf_error_manejador tipoError="#reporte_CC.tipoError#" mensajeError="#reporte_CC.mensaje#">
	</cfif>
</cfif>
<cfinvoke component="#Application.componentes#.empresas" method="RSMostrarTodosEmpresas" returnvariable="RSEmpresas">

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<link rel="stylesheet" href="../css/style.css" type="text/css">
<link rel="stylesheet" type="text/css" href="../css/box.css">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>CUENTAS POR PAGAR</title>
<script language="javascript" src="../js/jquery.js"></script>
<script language="javascript" src="../js/funciones.js"></script>
<script language="javascript" src="../js/jquery.js"></script>
<script language="javascript" src="../js/funciones.js"></script>
<script language="javascript" src="../js/funciones_ajax.js"></script>
<script type="text/javascript" src="../calendario/calendar.js"></script>
<script type="text/javascript" src="../calendario/calendar-setup.js"></script>
<script type="text/javascript" src="../calendario/lang/calendar-es.js"></script>
<style type="text/css"> @import url("../calendario/calendar-blue2.css");</style>
<script language="javascript">	
	<!---Funcion que redirige el focus del campo de nombre al campo de la ID, cuyo desenfoque es el que manda a llamar el bindeo. --->
	var cambiarFocus = function(pop_up){				
		var nom_campo = $($(pop_up).prev())[0].id;
		var valor = $("#" + nom_campo).val();
		if(valor != "")
		{
			$("#" + nom_campo).focus().blur();
		}
	};

    function imprimir(opcion)
	{
		var estatus = '';	
		if(opcion == 1)
		{
			$('#cargando').fadeIn();
			$('#tbl_Principal').hide();
			form1.action = 'rpt_facturas.cfm';
			form1.submit();
		}
		else
		{
			$('#tbl_Principal').hide();
			$('input[type="checkbox"]:checked').each(function(i){
				i++;
				if(i == $('input[type="checkbox"]:checked').length)
					estatus = estatus + $(this).val();
				else
					estatus = estatus + $(this).val() + ',';
			});
			
			var tipo = 0;
			if($('#id_Tipo_1').is(':checked'))
				tipo = 1;
			else
				tipo = 2;
			window.open('rpt_facturas_excel.cfm?id_Empresa='+$('#Filtro_id_Empresa').val() +'&id_Obra='+$('#id_Obra').val()+'&id_Proveedor='+$('#id_Proveedor').val()+'&id_OrdenCompra='+$('#id_OrdenCompra').val()+'&cl_TipoFactura='+$('#cl_TipoFactura').val()+'&de_Factura='+$('#de_Factura').val()+'&de_Estatus='+estatus+'&id_Tipo='+tipo,'_Self')
		}
	}
	
    function mostrar(id,proveedor,tr)
    {	
        if($x('tr_'+id).style.display == 'none')
        {
			var estatus= '';
			$('input[type="checkbox"]:checked').each(function(i){
				i++;
				if(i == $('input[type="checkbox"]:checked').length)
					estatus = estatus + $(this).val();
				else
					estatus = estatus + $(this).val() + ',';
			});
		
            $x('tr_'+id).style.display='';
			tr.style.backgroundColor='#99CCCC';
            cargar_pagina('rpt_facturas_detalle.cfm?id_Proveedor='+proveedor+'&id_Estatus='+estatus,$x('div_'+id),'loader.gif');
        }
        else
        {
			tr.style.backgroundColor='';
            $x('tr_'+id).style.display='none';
        }
    }
</script>

</head>

<body onLoad="form1.id_Obra.focus();">
<cfinclude template="menu.cfm">&nbsp;
<table style="width:70%" align="center">
	<tr class="titulo">
		<td align="left">CUENTAS POR PAGAR</td>
	</tr>
</table>
<cfform name="form1" onsubmit="$('##cargando').fadeIn(); $('##tbl_Principal').hide();">
	<table width="70%" cellpadding="0" cellspacing="0" align="center">
		<tr><td class="x-box-tl" width="1px"></td><td class="x-box-tc"></td><td class="x-box-tr" width="1px"></td></tr>
		<tr><td class="x-box-ml"></td><td class="x-box-mc" width="99%">	
			<table width="99%" align="left" border="0">
                <tr>
                    <td align="right" style="font-weight:bold">Empresa:</td>
                    <td align="left" width="">
                        <cfselect name="Filtro_id_Empresa" id="Filtro_id_Empresa" style="width:400px;" required="no" message="Seleccione Empresa">
                            <option value="">TODAS</option>
                            <cfoutput query="RsEmpresas.rs">
                                <option value="#RsEmpresas.rs.id_Empresa#" <cfif #Filtro_id_Empresa# EQ #RsEmpresas.rs.id_Empresa#>selected="selected"</cfif>>#nb_Empresa#</option>
                            </cfoutput>
                        </cfselect>
                    </td>
				</tr>
				<tr>
					<td align="right" style="font-weight:bold">Obra:</td>
					<td align="left">
						<cfinput type="text" class="contenido_numerico" name="id_Obra" id="id_Obra" validate="integer" onBlur="$x('de_Obra_lab').value='';" size="10" value="#id_Obra#">
						<cfinput type="text" bind="cfc:#Application.componentes#.obras.getObraPorID(1,{id_Obra@blur})" bindonload="no" onFocus="cambiarFocus($(this).before());" id="de_Obra_lab" name="de_Obra_lab" class="contenido_readOnly" style="overflow:hidden; width:350px" required="no" message="La Clave de Obra no valida.">
						<button type="button" id="btnBuscarObra" class="boton_popup" value="" onClick="javascript:AbrirPopUpBusquedaChrome('id_Obra', 'pop_obras_busqueda.cfm', 'id_Empresa=<cfoutput>#Session.id_Empresa#</cfoutput>', '700px', '600px')" ><img src="../images/buscar.png" alt="Clic para buscar obra" width="24"></button>
					</td>
				</tr>
                <tr>
                    <td align="right" style="font-weight:bold">Proveedor:</td>
                    <td align="left">
                        <cfinput type="text" class="contenido_numerico" name="id_Proveedor" id="id_Proveedor" validate="integer" size="10" value="#id_Proveedor#" message="Proveedor no v�lido" maxlength="8">
                        <cfinput type="text" bind="cfc:#Application.componentes#.proveedores.getProveedorPorID({id_Proveedor@blur})" bindonload="yes" onFocus="cambiarFocus($(this).before());" id="de_Proveedor_lab" name="de_Proveedor_lab" class="contenido_readOnly" style="overflow:hidden; width:350px" readonly="yes">
                        <button type="button" id="btnBuscarProveedor" onClick="javascript:AbrirPopUpBusquedaChrome('id_Proveedor', 'pop_proveedores_busqueda.cfm', '', '700px', '600px')" class="boton_popup" value=""><img src="../images/buscar_gris.png" width="24"></button>
                    
                        <!--- <cfinput type="text" name="sugerida_Proveedor" id="sugerida_Proveedor" class="contenido" size="20" value="#id_Proveedor#" placeholder="NUMERO O NOMBRE PROVEEDOR"/>
                        <cfselect name="id_Proveedor" id="id_Proveedor" bind="cfc:#Application.componentes#.proveedores.getProveedoresSugeridasParaFiltrotmp({sugerida_Proveedor@blur})" bindonload="yes" display="nb_ProveedorCorto" value="id_Proveedor" onChange="javascript:$x('sugerida_Proveedor').value=$x('id_Proveedor').value;" class="contenido" style="width:450px">
                        </cfselect> --->
                    </td>
                </tr>
				<tr>
					<td align="right" style="font-weight:bold">Orden Compra:</td>
					<td align="left">
						<cfinput type="text" class="contenido_numerico" name="id_OrdenCompra" id="id_OrdenCompra" validate="integer" size="10" value="#id_OrdenCompra#">
						<!--- <button type="button" id="btnBuscarOrdenCompra" class="boton_popup" value="" onClick="javascript:AbrirPopUpBusquedaChrome('id_OrdenCompra', 'pop_obras_busqueda.cfm', 'id_Empresa=<cfoutput>#Session.id_Empresa#</cfoutput>', '700px', '600px')" ><img src="../images/buscar.png" alt="Clic para buscar obra" width="24"></button> --->

                        <button type="button" id="btnBuscarOrdenCompra" class="boton_popup" value="" onClick="javascript:AbrirPopUpBusquedaChrome('id_OrdenCompra', 'pop_ordenes_compra_busqueda.cfm', 'id_Empresa='+$('#Filtro_id_Empresa').val()+'&id_Obra='+$('#id_Obra').val(), '700px', '600px')">
                        	<img src="../images/buscar.png" alt="Clic para buscar obra" width="24">
                        </button>
					</td>
				</tr>
				<tr>
					<td align="right" style="font-weight:bold">Tipo Factura:</td>
					<td align="left">
                        <cfselect name="cl_TipoFactura" id="cl_TipoFactura" style="width:400px;" required="no" message="Seleccione Tipo Factura">
                            <option value="">TODAS</option>
                            <option value="1" <cfif #cl_TipoFactura# EQ 1>selected="selected"</cfif>>MATERIALES</option>
                            <option value="2" <cfif #cl_TipoFactura# EQ 2>selected="selected"</cfif>>MANO OBRA</option>
                        </cfselect>
					</td>
				</tr>
				<tr>
					<td align="right" style="font-weight:bold">Factura:</td>
					<td align="left">
						<cfinput type="text" class="contenido" name="de_Factura" id="de_Factura" size="10" value="#de_Factura#">
					</td>
				</tr>
                
				<tr>
                	<td align="right" style="font-weight:bold">Estatus Factura:</td>
                    <td align="left">
                        Ingresada: <input type="checkbox" id="de_Estatus_511" name="de_Estatus" <cfif listFind(de_Estatus,511,',') NEQ 0> checked="yes"</cfif> value="511">&nbsp;&nbsp;&nbsp;&nbsp;
                        Tramitar a pago: <input type="checkbox" id="de_Estatus_519" name="de_Estatus" <cfif listFind(de_Estatus,519,',') NEQ 0> checked="yes"</cfif> value="519">&nbsp;&nbsp;&nbsp;&nbsp;
						<!--- Pagada: <input type="checkbox" id="de_Estatus_524" name="de_Estatus" <cfif listFind(de_Estatus,524,',') NEQ 0> checked="yes"</cfif>  value="524">&nbsp;&nbsp;&nbsp;&nbsp; --->
						 Pagada Parcial: <input type="checkbox" id="de_Estatus_524" name="de_Estatus" <cfif listFind(de_Estatus,521,',') NEQ 0> checked="yes"</cfif>  value="521">
                        <!--- &nbsp;&nbsp;&nbsp;&nbsp;
						Cancelada: <input type="checkbox" id="de_Estatus_524" name="de_Estatus" <cfif listFind(de_Estatus,512,',') NEQ 0> checked="yes"</cfif>  value="512"> --->
                    </td>
                </tr>
				<tr>
                	<td align="right" style="font-weight:bold">Tipo de vista:</td>
                    <td align="left">
                        Detalle <input type="radio" id="id_Tipo_1" name="id_Tipo" <cfif id_Tipo EQ 1>checked</cfif> value="1">&nbsp;&nbsp;&nbsp;&nbsp;
                        Por proveedor <input type="radio" id="id_Tipo_1" name="id_Tipo" <cfif id_Tipo EQ 2>checked</cfif> value="2">
                    </td>
                </tr>
				<tr>
                    <td align="center" colspan="2">
                        <button type="button" class="boton_imagen" onClick="imprimir(1);"><img src="../images/filtro_azul.png" alt="Clic para mostrar tabla"></button>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                        <button  type="button" class="boton_menu_superior" onClick="imprimir(2);">
                            <img src="../images/excel.png" alt="Enviar a Excel" title="clic para generar excel" width="24" border="0">
                        </button>
                        
                    	<cfinput type="hidden" name="sn_Mostrar" value="S">
                    </td>	
                </tr>
			</table>
			</td><td class="x-box-mr"></td></tr><tr><td class="x-box-bl" width="1px"></td><td class="x-box-bc"></td><td class="x-box-br" width="1px"></td>
		</tr>
	</table>
    <br />
    <div id="cargando" style="display:none;">
        <center><img src="../images/cargando_procesando.gif"></center>
    </div>
	<cfif #sn_Mostrar# EQ 'S'>
		<cfif id_Tipo EQ 1>
            <table cellpadding="0" cellspacing="0" width="100%" id="tbl_Principal" class="fondo_listado" style="font-size:9px">
                <tr class="encabezado_grande">
                    <td width="3"><img src="../images/esquina_izquierda22.gif"></td>
                    <td>PROVEEDOR</td>
                    <td>OC</td>
                    <td>FOLIO FACTURA</td>
        
                    <td>SUBTOTAL</td>
                    <td>IVA</td>
                    <td>RETENCION IVA</td>
                    <td>ISR</td>
                    <td>NOTA CREDITO</td>
                    <td>TOTAL</td>
                    
                    <td>ESTATUS</td>
                    <td>TIPO FACTURA</td>
                    <td>FECHA FACTURA</td>
                    <td>OBSERVACIONES</td>
                    <td width="3"><img src="../images/esquina_derecha22.gif"></td>
                </tr>
                    <cfoutput query="reporte_CC.rs" group="id_Empresa">
                        <tr onMouseOver="this.style.backgroundColor='##99CCCC'" onMouseOut="this.style.backgroundColor=''">
                            <td background="../images/orilla_izquierda.gif" style="background-repeat:repeat-y" width="3"></td>
                            <td colspan="13" class="cuadricula" style="font-weight:bold">EMPRESA: #nb_Empresa#</td>
                            <td background="../images/orilla_derecha.gif" style="background-repeat:repeat-y" width="3"></td>
                        </tr>
                        <cfoutput group="id_Obra">
                            <tr onMouseOver="this.style.backgroundColor='##99CCCC'" onMouseOut="this.style.backgroundColor=''">
                                <td background="../images/orilla_izquierda.gif" style="background-repeat:repeat-y" width="3"></td>
                                <td colspan="13" class="cuadricula" style="font-weight:bold">OBRA: #id_Obra# - #de_Obra#</td>
                                <td background="../images/orilla_derecha.gif" style="background-repeat:repeat-y" width="3"></td>
                            </tr>
                            <cfoutput>
                                <tr onMouseOver="this.style.backgroundColor='##99CCCC'" onMouseOut="this.style.backgroundColor=''">
                                    <td background="../images/orilla_izquierda.gif" style="background-repeat:repeat-y" width="3"></td>
                                    <td class="cuadricula">#reporte_CC.rs.id_Proveedor# - #nb_Proveedor#</td>
                                    <td class="cuadricula" nowrap="nowrap">#id_OrdenCompra# - [#NumberFormat(im_TotalOC,'$,_.__')#]</td>
                                    <td class="cuadricula">#de_Factura#</td>
                                    
                                    <td class="cuadricula" align="right">#NumberFormat(im_SubTotal,'$,_.__')#</td>
                                    <td class="cuadricula" align="right">#NumberFormat(im_Iva,'$,_.__')#</td>
                                    <td class="cuadricula" align="right">#NumberFormat(im_RetencionIva,'$,_.__')#</td>
                                    <td class="cuadricula" align="right">#NumberFormat(isr,'$,_.__')#</td>
                                    <td class="cuadricula" align="right">#NumberFormat(im_NotaCredito,'$,_.__')#</td>
                                    <td class="cuadricula" align="right">#NumberFormat(im_Factura,'$,_.__')#</td>
                                    
                                    <td class="cuadricula" align="center">#de_Estatus#</td>
                                    <td class="cuadricula" align="center">#de_TipoFactura#</td>
                                    <td class="cuadricula" align="center">#DateFormat(fh_Factura,'DD/MM/YYYY')#</td>
                                    <td class="cuadricula">#de_Observaciones#</td>
                                    <td background="../images/orilla_derecha.gif" style="background-repeat:repeat-y" width="3"></td>
                                </tr>
                            </cfoutput>
                        </cfoutput>
                    </cfoutput>
                    <tr style="font-weight:bold">
                        <td background="../images/orilla_izquierda.gif" style="background-repeat:repeat-y" width="3"></td>
                        <td colspan="8" align="right" class="cuadricula">TOTAL FACTURADO: </td>
                        <td align="right" class="cuadricula"><cfoutput>#NumberFormat(ArraySum(ListToArray(ValueList(reporte_CC.rs.im_Factura))),'$,_.__')#</cfoutput></td>
                        <td colspan="5"></td>
                        <td background="../images/orilla_derecha.gif" style="background-repeat:repeat-y" width="3"></td>
                    </tr>
                    <tr>
                        <cfoutput><td colspan="16" align="center">Total de registros: #reporte_CC.rs.recordCount#</td></cfoutput>
                    </tr>
                </table>
		<cfelse>
            <table cellpadding="0" cellspacing="0" width="100%" id="tbl_Principal" class="fondo_listado" style="font-size:9px">
                <tr class="encabezado_grande">
                    <td width="3"><img src="../images/esquina_izquierda22.gif"></td>
                    <td>PROVEEDOR</td>
                    <td>SUBTOTAL</td>
                    <td>IVA</td>
                    <td>RETENCION IVA</td>
                    <td>ISR</td>
                    <td>NOTA CREDITO</td>
                    <td>TOTAL</td>
                    <td width="3"><img src="../images/esquina_derecha22.gif"></td>
                </tr>
				<cfoutput query="reporte_CC.rs">
                    <tr style=" cursor:pointer;" onClick="mostrar(#CurrentRow#, #reporte_CC.rs.id_Proveedor#,this)">
                        <td background="../images/orilla_izquierda.gif" style="background-repeat:repeat-y" width="3"></td>
                        <td class="cuadricula">#reporte_CC.rs.id_Proveedor# - #nb_Proveedor#</td>
                        <td class="cuadricula" align="right">#NumberFormat(im_SubTotal,'$,_.__')#</td>
                        <td class="cuadricula" align="right">#NumberFormat(im_Iva,'$,_.__')#</td>
                        <td class="cuadricula" align="right">#NumberFormat(im_RetencionIva,'$,_.__')#</td>
                        <td class="cuadricula" align="right">#NumberFormat(isr,'$,_.__')#</td>
                        <td class="cuadricula" align="right">#NumberFormat(im_NotaCredito,'$,_.__')#</td>
                        <td class="cuadricula" align="right">#NumberFormat(im_Factura,'$,_.__')#</td>
                        <td background="../images/orilla_derecha.gif" style="background-repeat:repeat-y" width="3"></td>
                    </tr>
                    <tr id="tr_#currentRow#" style="display:none;">
                        <td width="100%" colspan="9">
                            <div id="div_#CurrentRow#" style="width:100%" align="center"></div>
                        </td>
                    </tr>                    
                </cfoutput>
                <tr style="font-weight:bold">
                    <td background="../images/orilla_izquierda.gif" style="background-repeat:repeat-y" width="3"></td>
                    <td colspan="6" align="right" class="cuadricula">TOTAL FACTURADO: </td>
                    <td align="right" class="cuadricula"><cfoutput>#NumberFormat(ArraySum(ListToArray(ValueList(reporte_CC.rs.im_Factura))),'$,_.__')#</cfoutput></td>
                    <td background="../images/orilla_derecha.gif" style="background-repeat:repeat-y" width="3"></td>
                </tr>
                <tr>
                    <cfoutput><td colspan="16" align="center">Total de registros: #reporte_CC.rs.recordCount#</td></cfoutput>
                </tr>
            </table>                
		</cfif>
    </cfif>    
</cfform>
</body>
</html>