<cfset CurrentPage=GetFileFromPath(GetTemplatePath())>
<cfparam name="id_Proveedor" default="">

<cfif isDefined('sn_Enviar') AND sn_Enviar EQ 'S'>
	<cfinvoke component="#Application.componentes#.proveedores" method="Proveedores_ReducirValidaciones_XML_Agregar" returnvariable="RSOperacion"
			  id_Proveedor="#id_Proveedor#">
	<cfif NOT (RSOperacion.SUCCESS)>
		<cf_error_manejador  tipoError="#RSOperacion.tipoError#" mensajeError="#RSOperacion.MESSAGE#">
	</cfif>
	<cf_location_msg  url="proveedores_reducirvalidaciones_xml_listado.cfm" mensaje="#RSOperacion.MESSAGE#">
</cfif>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>
		</title>
		<link rel="stylesheet" href="../css/style.css" type="text/css">
		<link rel="stylesheet" type="text/css" href="../css/box.css">
		<script language="javascript" src="../js/jquery.js"></script> 
		<script language="javascript" src="../js/funciones.js"></script>
		<!---Chosen --->
		<script src="../js/chosen/chosen.jquery.js" type="text/javascript"></script>
		<link rel="stylesheet" href="../js/chosen/chosen.css">
	</head>
	<body>
		<cfinclude template="menu.cfm">&nbsp;
		<cfform name="form1">
			<table width="700px" align="center" class="tabla_grid" cellpadding="0" cellspacing="0" >
				<thead>
					<tr class="tabla_grid_encabezado">
						<td width="3px"><img src="../images/esquina_izquierda.gif"></td>
				        <td width="100%" align="center" valign="baseline" colspan="2">Agregar proveedor a lista de reduccion de validaciones XML</td>
			        	<td width="3px"><img src="../images/esquina_derecha.gif"></td>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="tabla_grid_orilla_izquierda"></td>
						<tD align="right" colspan="2">
							&nbsp;
						</tD>
						<td class="tabla_grid_orilla_derecha"></td>
					</tr>
					<tr>
						<td class="tabla_grid_orilla_izquierda"></td>
						<th align="right" style="width:200px;">
							Proveedor:&nbsp;
						</th>
						<td>
							<cfinput type="text" class="contenido_numerico" name="id_Proveedor" id="id_Proveedor" style="width:80px;vertical-align: middle;" validate="integer" size="10" value="#id_Proveedor#"  required="false">
                            <cfinput type="text" class="contenido_readOnly" name="de_Proveedor" id="de_Proveedor" style="width:300px;vertical-align: middle;" readonly bind="cfc:#Application.componentes#.proveedores.getProveedorPorID({id_Proveedor@blur})" onFocus="cambiarFocus($(this).before());" bindonload="yes" required="true" message="El proveedor no es v\u00e1lido.">
                            <button type="button" id="btnBuscarProveedor" onClick="javascript:AbrirPopUpBusquedaChrome('id_Proveedor', 'pop_proveedores_busqueda.cfm', '', '700px', '600px')" class="boton_popup" value=""><img src="../images/buscar.png" style="width:18px;vertical-align: middle;"></button>
						</td>
						<td class="tabla_grid_orilla_derecha"></td>
					</tr>
					<tr>
						<td class="tabla_grid_orilla_izquierda"></td>
						<tD align="right" colspan="2">
							&nbsp;
						</tD>
						<td class="tabla_grid_orilla_derecha"></td>
					</tr>
					
					<tr>
						<td class="tabla_grid_orilla_izquierda"></td>
						<td colspan="2" align="center">
							<button type="submit" id="btn_Aceptar" class="boton_imagen" title="Aceptar"><img src="../images/aceptar.png"></button>
							<button type="button" class="boton_imagen" onClick="location='proveedores_reducirvalidaciones_xml_listado.cfm'" title="Cancelar"><img src="../images/cancelar.png"></button>
						</td>
						<td class="tabla_grid_orilla_derecha"></td>
					</tr>
					<tr class="tabla_grid_footer">
				    <td width="3px"><img src="../images/abajo_izquierda.gif" /></td>
				    <td colspan="2"></td>
				    <td width="3px"><img src="../images/abajo_derecha.gif" /></td>
				</tr>
				</tbody>
			</table>
			<cfinput type="hidden" name="sn_Enviar" id="sn_Enviar" value="S">
		</cfform>
		<br/>
		<br/>
	</body>
</html>
<script type="text/javascript">
	
	
</script>