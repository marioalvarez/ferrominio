<center><img src="../images/cargando_procesando.gif"></center>
<cfparam name="id_Empresa" default="">
<cfparam name="Filtro_id_Obra" default="">
<cfparam name="id_Agrupador" default="">

<!--- <cfinvoke component="#Application.componentes#.bancos" method="INGRESOS_OBRA" returnvariable="RSIngresosObra" 
    id_Empresa="#id_Empresa#"
    id_Obra="#Filtro_id_Obra#"
>
<cfif #RSIngresosObra.SUCCESS# EQ false>
    <cf_error_manejador mensajeError="#RSIngresosObra.MESSAGE#">                                                                         
</cfif>   ---> 
<cfsetting requesttimeout="999999">
<cfinvoke component="#Application.componentes#.bancos" method="reporte_desglosado_saldos" returnvariable="RsSaldos" 
        id_Empresa="#id_Empresa#"
        id_Obra="#Filtro_id_Obra#"
        id_Agrupador="#id_Agrupador#"
    >
<cfif #RsSaldos.SUCCESS# EQ false>
    <cf_error_manejador mensajeError="#RsSaldos.MESSAGE#">                                                                         
</cfif>

<cfsavecontent variable="contenido"> 
    <table cellpadding="0" cellspacing="0" border="1" style="width:auto">
        <tr style="background:#000099; color:#FFFFFF">
            <td class="cuadricula" align="center" style="font-size:14px;font-weight:bold;width:auto">CLAVE</td>
            <td class="cuadricula" align="center" style="font-size:14px;font-weight:bold;width:auto">AGRUPADOR</td>
            <td class="cuadricula" align="center" style="font-size:14px;font-weight:bold;width:auto">PRESUPUESTADO</td>
            <td class="cuadricula" align="center" style="font-size:14px;font-weight:bold;width:auto">PAGADO</td>
            <td class="cuadricula" align="center" style="font-size:14px;font-weight:bold;width:auto">PORCENTAJE DE PAGO</td>
            <td class="cuadricula" align="center" style="font-size:14px;font-weight:bold;width:auto">SALDO</td>
        </tr>
		<cfset Total_im_Presupuestado = 0>
        <cfset Total_im_Pagado = 0>
        <cfset Total_pj_Pagado = 0>
        <cfset Total_im_Saldo = 0>
        
		<cfoutput query="RsSaldos.Rs" group="id_Agrupador">
            <cfquery name="RSTotales" datasource="#session.cnx#">
                EXECUTE Reporte_Saldos_Obtener_Pagado_Agrupador #RsSaldos.rs.id_Empresa#, <cfif Filtro_id_Obra NEQ ''>#Filtro_id_Obra#<cfelse>NULL</cfif>, '#RsSaldos.rs.id_Agrupador#'
            </cfquery>
            <tr style="background:##999999">
                <td class="cuadricula" align="left" style="width:inherit">#id_Agrupador#</td>
                <td class="cuadricula" align="left" style="width:inherit">#de_Agrupador#</td>
                <td class="cuadricula" align="right" style="width:inherit">$#NumberFormat(Presupuestado,',_.__')#</td>
                <td class="cuadricula" align="right" style="width:inherit">$#NumberFormat(RSTotales.pagadoAgrupador,',_.__')#</td>
                <cfset im_Saldo = Presupuestado - RSTotales.pagadoAgrupador>
                <cfif Presupuestado NEQ 0>
                    <cfset pj_Pagado=(RSTotales.pagadoAgrupador / Presupuestado) * 100>
                <cfelse>    
                    <cfset pj_Pagado=(RSTotales.pagadoAgrupador) * 100>
                </cfif>
                <td class="cuadricula" align="right" style="width:auto">#NumberFormat(pj_Pagado,',_.__')#%</td>
                <td class="cuadricula" align="right" style="width:auto">$#NumberFormat(im_Saldo,',_.__')#</td>
            </tr>
			<cfset Total_im_Presupuestado += #Presupuestado#>
            <cfset Total_im_Pagado += #RSTotales.pagadoAgrupador#>
            <cfset Total_pj_Pagado += #pj_Pagado#>
            <cfset Total_im_Saldo += #im_Saldo#>

            <cfquery name="OrdenesCompra" datasource="#session.cnx#">
                Ordenes_Compra_Agrupador_Detalle #RsSaldos.Rs.id_Empresa#, <cfif Filtro_id_Obra NEQ ''>#Filtro_id_Obra#<cfelse>NULL</cfif>, '#RsSaldos.Rs.id_Agrupador#'
            </cfquery>
            <cfloop query="OrdenesCompra">
                <cfquery name="nu_Agrupadores" datasource="#session.cnx#">
                    select  
                        ocd.id_Empresa,ocd.id_Obra,ocd.id_OrdenCompra,count(1) as nu_Agrupadores
                    from 
                        OrdenesCompra oc
                        inner join
                        (
                            select id_Empresa,id_Obra,id_OrdenCompra,id_Agrupador
                            from OrdenesCompraDetalle ocd 
                            inner join Insumos i on i.id_Insumo = ocd.id_Insumo
                            group by id_Empresa,id_Obra,id_OrdenCompra,id_Agrupador
                        )
                        ocd on oc.id_Empresa = ocd.id_Empresa and oc.id_Obra = ocd.id_Obra and oc.id_OrdenCompra = ocd.id_OrdenCompra
                    where oc.id_Empresa = #RsSaldos.Rs.id_Empresa# and oc.id_Obra = #OrdenesCompra.id_Obra# and oc.id_OrdenCompra = #OrdenesCompra.id_OrdenCompra#
                    group by ocd.id_Empresa,ocd.id_Obra, ocd.id_OrdenCompra
                </cfquery>    
                <cfset im_Gastos = 0>
                <cfif #OrdenesCompra.im_OtrosGastos# GT 0>
                    <cfquery name="oc_Insumos" datasource="#session.cnx#">
                        select SUM(im_CantidadPrecio) as im_Agrupador,id_Agrupador
                        from OrdenesCompraDetalle ocd 
                        inner join Insumos i on i.id_Insumo = ocd.id_Insumo
                        where id_Obra = #OrdenesCompra.id_Obra# and id_OrdenCompra = #OrdenesCompra.id_OrdenCompra# and id_Agrupador = '#RsSaldos.Rs.id_Agrupador#'
                        group by id_Agrupador
                    </cfquery>
                    <cfquery name="subtotal_OC" datasource="#session.cnx#">
                        select im_subtotal,im_gastosManiobra from ordenesCompra where id_Obra = #OrdenesCompra.id_Obra# and id_OrdenCompra = #OrdenesCompra.id_OrdenCompra#
                    </cfquery>
                    <cfset pj_Total = (#oc_Insumos.im_Agrupador# * 100) / #subtotal_OC.im_Subtotal#>
                    <cfset im_Gastos = (#pj_Total# / 100) * #subtotal_OC.im_gastosManiobra#>
                </cfif>
                
                <tr style="background:##99CCCC">
                    <td style="width:auto"></td>
                    <td style="width:auto">#OrdenesCompra.id_Obra# - #OrdenesCompra.de_Obra#</td>
                    <td style="width:auto">OC: #OrdenesCompra.id_OrdenCompra#</td>
                    <td style="width:auto">#OrdenesCompra.nb_Proveedor#</td>
                    <td align="center" style="width:auto">#DateFormat(OrdenesCompra.fh_OrdenCompra,'DD/MM/YYYY')#</td>
					<cfset pj_Iva_OC = #pj_Iva#>
                    <cfif #OrdenesCompra.id_TipoOrdenCompra# EQ 4 and pj_Iva EQ 0>
                        <cfset pj_Iva_OC = 16>
                    </cfif>
                    <cfset im_subtotal_OC = (#OrdenesCompra.im_CantidadPrecio# + #im_Gastos#) > 
                    <cfset im_Iva = iif(pj_Iva_OC NEQ 0,im_subtotal_OC * ( pj_Iva_OC /100),0)>
                    <cfset im_Total_OC = #im_subtotal_OC# + #im_Iva#>
                    <cfif #nu_Agrupadores.nu_Agrupadores# EQ 1>
                        <cfset im_Total_OC = #OrdenesCompra.im_Total#>
                    </cfif>				
                    
                    <td align="right" width="20%" class="cuadricula">#NumberFormat(im_Total_OC,'$,_.__')#</td>
                    
                    <!--- <td align="right" style="width:auto">#NumberFormat(OrdenesCompra.im_Total,'$,_.__')#</td> --->
                </tr>
				<cfif #RsSaldos.Rs.id_Agrupador# EQ 'DE-001' AND #OrdenesCompra.id_OrdenCompra# EQ 0>
                	<cfquery name="Facturas" datasource="#session.cnx#">
						Facturas_Destajos #RsSaldos.Rs.id_Empresa#, #OrdenesCompra.id_Obra#, #OrdenesCompra.id_Proveedor#
                	</cfquery>
				<cfelseif #OrdenesCompra.id_TipoOrdenCompra# EQ 4 OR #OrdenesCompra.id_TipoOrdenCompra# EQ 1>
                	<cfquery name="Facturas" datasource="#session.cnx#">
						Facturas_Subcontrato #RsSaldos.Rs.id_Empresa#, #OrdenesCompra.id_Obra#, #OrdenesCompra.id_OrdenCompra#
                	</cfquery>
				<cfelseif #OrdenesCompra.id_OrdenCompra# EQ -1>
                    <cfquery name="Facturas" datasource="#session.cnx#">
                        Facturas_TraspasoIndirecto #RsSaldos.Rs.id_Empresa#, #OrdenesCompra.id_Obra#
                    </cfquery>                    
				<cfelse>
                    <cfquery name="Facturas" datasource="#session.cnx#">
                        Ordenes_Compra_Facturas_Detalle #RsSaldos.Rs.id_Empresa#, #OrdenesCompra.id_Obra#, '#RsSaldos.Rs.id_Agrupador#', #OrdenesCompra.id_OrdenCompra#
                    </cfquery>
				</cfif>
                
                <cfquery name="nu_Agrupadores" datasource="#session.cnx#">
                    select  
                        ocd.id_Empresa,ocd.id_Obra,ocd.id_OrdenCompra,count(1) as nu_Agrupadores
                    from 
                        OrdenesCompra oc
                        inner join
                        (
                            select id_Empresa,id_Obra,id_OrdenCompra,id_Agrupador
                            from OrdenesCompraDetalle ocd 
                            inner join Insumos i on i.id_Insumo = ocd.id_Insumo
                            group by id_Empresa,id_Obra,id_OrdenCompra,id_Agrupador
                        )
                        ocd on oc.id_Empresa = ocd.id_Empresa and oc.id_Obra = ocd.id_Obra and oc.id_OrdenCompra = ocd.id_OrdenCompra
                    where oc.id_Empresa = #RsSaldos.Rs.id_Empresa# and oc.id_Obra = #OrdenesCompra.id_Obra# and oc.id_OrdenCompra = #OrdenesCompra.id_OrdenCompra#
                    group by ocd.id_Empresa,ocd.id_Obra, ocd.id_OrdenCompra
                </cfquery>
                
				<cfloop query="Facturas">
					<cfset im_Gastos = 0>
                    <cfif #OrdenesCompra.im_OtrosGastos# GT 0>        
                        <cfquery name="oc_Insumos" datasource="#session.cnx#">
                            select SUM(im_Analisis) as im_Agrupador,id_Agrupador
                            from ContraRecibosDetalleAnalisisInsumos ocd 
                            inner join Insumos i on i.id_Insumo = ocd.id_Insumo
                            where id_Empresa = #RsSaldos.Rs.id_Empresa# and id_Obra = #OrdenesCompra.id_Obra# and id_ContraRecibo = #Facturas.id_ContraRecibo# and id_Agrupador = '#RsSaldos.Rs.id_Agrupador#'
                            group by id_Agrupador
                        </cfquery>
                        <cfquery name="subtotal_OC" datasource="#session.cnx#">
                            select im_subtotal,im_gastosManiobra from ordenesCompra where id_Empresa = #RsSaldos.Rs.id_Empresa# and id_Obra = #OrdenesCompra.id_Obra# and id_OrdenCompra = #OrdenesCompra.id_OrdenCompra#
                        </cfquery>
                        <cfset pj_Total = (#oc_Insumos.im_Agrupador# * 100) / #subtotal_OC.im_Subtotal#>
                        <cfset im_Gastos = (#pj_Total# / 100) * #subtotal_OC.im_gastosManiobra#>
                    </cfif>    	    
                
                    <tr>
                        <td></td><td></td>
                        <td style="width:auto">FACTURA: #Facturas.de_Factura#</td>
                        <td style="width:auto" align="center">#DateFormat(Facturas.fh_Factura,'DD/MM/YYYY')#</td>
                        <td style="width:auto" align="center">#Facturas.de_Estatus#</td>

						<cfif #RsSaldos.Rs.id_Agrupador# EQ 'DE-001' AND #OrdenesCompra.id_OrdenCompra# EQ 0>
                            <cfset im_Total_OC = #Facturas.im_CantidadPrecio#>
                        <cfelseif #OrdenesCompra.id_TipoOrdenCompra# EQ 4 OR #OrdenesCompra.id_TipoOrdenCompra# EQ 1>
                            <cfset im_Total_OC = #Facturas.im_CantidadPrecio#>
                        <cfelse>
                            <cfset im_subtotal_OC = (#Facturas.im_CantidadPrecio# + #im_Gastos#) > 
                            <cfset im_Iva = iif(pj_Iva NEQ 0,im_subtotal_OC * (pj_Iva/100),0)>
                            <cfset im_Total_OC = #im_subtotal_OC# + #im_Iva#>
                                        
                            <cfif #nu_Agrupadores.nu_Agrupadores# EQ 1>
                                <cfset im_Total_OC = #im_Factura#>
                            </cfif>				
                        </cfif>
                        <td style="width:auto" align="right">#NumberFormat(im_Total_OC,'$,_.__')#</td>
                    </tr>
                </cfloop>
            </cfloop>
        </cfoutput>
		<!--- <cfoutput>
        <tr style="font-weight:bold">
            <td align="right" class="cuadricula" colspan="2">TOTALES:</td>
            <td align="right" class="cuadricula">#NumberFormat(Total_im_Presupuestado,'$,_.__')#</td>
            <td align="right" class="cuadricula">#NumberFormat(Total_im_Pagado,'$,_.__')#</td>
            <cfif Total_im_Presupuestado NEQ 0 AND Total_im_Pagado NEQ 0>
                <cfset Total_pj_Pagado = (Total_im_Pagado / Total_im_Presupuestado) * 100>
            <cfelse>    
                <cfset Total_pj_Pagado = 0>
            </cfif>
            <td align="right" class="cuadricula">#NumberFormat(Total_pj_Pagado,',_.__')#%</td>
            <cfset im_Saldo = Total_im_Presupuestado - Total_im_Pagado>
            <td align="right" class="cuadricula" style="color:
            <cfif #Total_pj_Pagado# GTE 0 AND #Total_pj_Pagado# LTE 80>
                ##00BB00 
            <cfelseif #Total_pj_Pagado# GT 80 AND #Total_pj_Pagado# LTE 100>
                ##FFAA00
            <cfelseif #Total_pj_Pagado# GT 100>
                ##FF0000
            </cfif>     
            "><b>$#NumberFormat(im_Saldo,',_.__')#</b>           
            </td>
        </tr>
        </cfoutput>        
		<cfoutput query="RSIngresosObra.rs">
        <tr onMouseOver="this.style.backgroundColor='##99CCCC'" onMouseOut="this.style.backgroundColor=''">
            <td class="cuadricula" align="right" colspan="2"><b>Ingreso de obras:</b></td>
            <td class="cuadricula" align="right" colspan="5"style="color:##00BB00;"><b>$#NumberFormat(im_movimiento,',___.__')#</b></td>
        </tr> 
        <tr onMouseOver="this.style.backgroundColor='##99CCCC'" onMouseOut="this.style.backgroundColor=''">
            <td class="cuadricula" align="right" colspan="2"><b>Total Pagado:</b></td>
            <td class="cuadricula" align="right" colspan="5"><b style="color:##FF0000;">$#NumberFormat(Total_im_Pagado,',___.__')#</b></td>
        </tr>
        <tr>
            <cfset total = RSIngresosObra.rs.im_movimiento - Total_im_Pagado>
            <td class="cuadricula" align="right" colspan="2"><b>Saldo:</b></td>
            <td class="cuadricula" align="right" colspan="5"><b <cfif total LT 0>style="color:##FF0000;"<cfelse>style="color:##00BB00;"</cfif>>$#NumberFormat(total,',___.__')#</b></td>
        </tr>         
        </cfoutput> --->
        
	</table>
</cfsavecontent>
<cfoutput>
	#contenido#
</cfoutput>

<cfheader name="Content-Disposition" value="inline; filename=Reporte_saldos_#DateFormat(Now(),'DD_MM_YYYY')#.xls" >
<cfcontent variable="#ToBinary(ToBase64(contenido))#" type="application/msexcel" >