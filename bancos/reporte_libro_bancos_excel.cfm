<cfparam name="id_Obra" default="">
<cfparam name="fh_inicio" default="">
<cfparam name="fh_fin" default="">
<cfparam name="id_Banco" default="">
<cfparam name="Filtro_Cuenta_bancaria" default="">

<cfinvoke component="#Application.componentes#.bancos" method="reporte_libro_bancos" returnvariable="RsSaldos" 
	id_Obra="#id_Obra#"
	fh_inicio="#fh_inicio#"
    fh_fin="#fh_fin#"
    id_Banco="#id_Banco#"
    id_CuentaBancaria="#Filtro_Cuenta_bancaria#"
>
<cfif #RsSaldos.SUCCESS# EQ false>
	<cf_error_manejador mensajeError="#RsSaldos.MESSAGE#">                                                                         
</cfif>   
<cfquery name="Cargos" dbtype="query">
	select sum(IM_MOVIMIENTO) as cargo
	from RsSaldos.rs
	where TIPOMOVIMIENTO='Cargo'
</cfquery>
<cfsavecontent variable="contenido"> 
	<cfoutput query="RsSaldos.Rs" group="id_moneda">
        <table border="1" cellpadding="0" cellspacing="0" <cfif #Filtro_Cuenta_bancaria# NEQ ''> width="1200"<cfelse> width="800"</cfif> class="fondo_listado" align="center">
             <thead class="encabezado">
         		<td class="cuadricula" style="font-size:14px;font-weight:bold;width:auto;background:##000099; color:##FFFFFF"align="center" <cfif #Filtro_Cuenta_bancaria# NEQ ''>colspan="10"<cfelse>colspan="3"</cfif>><cfif #id_moneda# EQ 1>CUENTA EN PESOS<cfelseif #id_moneda# EQ 2>CUENTA EN DOLARES</cfif>
         		</td>
         	 </thead>
            <tr class="encabezado">
            	<cfif #Filtro_Cuenta_bancaria# NEQ ''>
             		<td class="cuadricula"  style="font-size:14px;font-weight:bold;width:auto;background:##000099; color:##FFFFFF" align="center">PROVEEDOR</td>
             		<td class="cuadricula"  style="font-size:14px;font-weight:bold;width:auto;background:##000099; color:##FFFFFF" align="center">CLIENTE</td>
             		<td class="cuadricula"  style="font-size:14px;font-weight:bold;width:auto;background:##000099; color:##FFFFFF" align="center">BANCO</td>
             		<td class="cuadricula"  style="font-size:14px;font-weight:bold;width:auto;background:##000099; color:##FFFFFF" align="center">CUENTA</td>
             		<td class="cuadricula"  style="font-size:14px;font-weight:bold;width:auto;background:##000099; color:##FFFFFF" align="center">OBRA</td>
					<td class="cuadricula"  style="font-size:14px;font-weight:bold;width:auto;background:##000099; color:##FFFFFF" align="center">FECHA</td>
                    <td class="cuadricula"  style="font-size:14px;font-weight:bold;width:auto;background:##000099; color:##FFFFFF" align="center">CONCEPTO</td>
                    <td class="cuadricula"  style="font-size:14px;font-weight:bold;width:auto;background:##000099; color:##FFFFFF" align="center">CARGO</td>
                    <td class="cuadricula"  style="font-size:14px;font-weight:bold;width:auto;background:##000099; color:##FFFFFF" align="center">ABONO</td>
                    <td class="cuadricula"  style="font-size:14px;font-weight:bold;width:auto;background:##000099; color:##FFFFFF" align="center">SALDO</td>
                <cfelse>
                	<td class="cuadricula"  style="font-size:14px;font-weight:bold;width:auto;background:##000099; color:##FFFFFF" align="center">BANCO</td>
             		<td class="cuadricula"  style="font-size:14px;font-weight:bold;width:auto;background:##000099; color:##FFFFFF" align="center">CUENTA</td>
             		<td class="cuadricula"  style="font-size:14px;font-weight:bold;width:auto;background:##000099; color:##FFFFFF" align="center">SALDO</td>
             		
                </cfif>    
             
            </tr>
            <cfset saldo=0>
            <cfset Total_cuenta=0>
            <cfoutput group="id_banco">
            	<cfoutput group="id_CuentaBancaria">
					<cfif #Filtro_Cuenta_bancaria# EQ ''>	
						<tr <cfif #Filtro_Cuenta_bancaria# NEQ ''>style="background-color:##CBBCBC"<cfelse>class="renglon_grid"</cfif>>
								
			                	<td align="center" class="cuadricula" <cfif #Filtro_Cuenta_bancaria# NEQ ''> colspan="4" </cfif>>#DE_BANCO# </td>
			                	<td align="center" class="cuadricula"  <cfif #Filtro_Cuenta_bancaria# NEQ ''> colspan="3" </cfif>>#NU_CUENTABANCARIA#</td>
			                	<cfquery name="cargos" dbtype="query">
			                		SELECT
			                			sum(IM_MOVIMIENTO) as cargos
			                		FROM
			                			RsSaldos.Rs
			                		WHERE
			                			id_cuentaBancaria=#id_cuentaBancaria# and id_Banco=#id_Banco# and TIPOMOVIMIENTO='Cargo'
			                	</cfquery>
			                	<cfquery name="abonos" dbtype="query">
			                		SELECT
			                			sum(IM_MOVIMIENTO) as abonos
			                		FROM
			                			RsSaldos.Rs
			                		WHERE
			                			id_cuentaBancaria=#id_cuentaBancaria# and id_Banco=#id_Banco# and TIPOMOVIMIENTO='Abono'
			                	</cfquery>
									
				                <cfif cargos.cargos EQ ''>
				                	<cfset cargo=0>
					            <cfelse>
					            	<cfset cargo=cargos.cargos>  			
			                	</cfif>
								<cfif abonos.abonos EQ ''>
				                	<cfset abono=0>
				                <cfelse>
					            	<cfset abono=abonos.abonos>  	
			                	</cfif>
			                	<cfset saldo_Cuenta= abono-cargo>
			                	<cfset Total_cuenta=Total_cuenta+saldo_Cuenta>
			                	<td align="right" class="cuadricula"  <cfif #Filtro_Cuenta_bancaria# NEQ ''> colspan="3" </cfif>><b <cfif #saldo_Cuenta# LTE 0>style="color:##FF0000;"<cfelse>style="color:##00BB00;"</cfif>>$#NumberFormat(saldo_Cuenta,',____.__')#<b></td>
                	
                		</tr>
					</cfif>
      				<cfif #Filtro_Cuenta_bancaria# NEQ ''>
      					<cfoutput>
							<tr class="renglon_grid" id="id_#ID_BANCO#_#ID_CUENTABANCARIA#_#ID_MOVIMIENTO#" onClick="Mostrar_OC(#ID_BANCO#,#ID_CUENTABANCARIA#,#ID_MOVIMIENTO#,#ID_TIPOMOVIMIENTO#)" click="0" style="cursor:pointer;">
								<td align="left" class="cuadricula">#NB_PROVEEDOR#</td>
			                	<td align="left" class="cuadricula">#NB_CLIENTE#</td>
			                	<td align="center" class="cuadricula">#DE_BANCO#</td>
			                	<td align="center" class="cuadricula">#NU_CUENTABANCARIA#</td>
			                	<td align="center" class="cuadricula">#ID_OBRA#</td>
			                	<td align="center" class="cuadricula">#DATEFORMAT(FH_MOVIMIENTO,'DD/MM/YYYY')#</td>
			                	<td align="left" class="cuadricula">#DE_TIPOMOVIMIENTO#</td>
			                	<td align="right" class="cuadricula"><cfif #TIPOMOVIMIENTO# EQ 'Cargo'>$#NumberFormat(IM_MOVIMIENTO,',____.__')#</cfif></td>
			                	<td align="right" class="cuadricula"><cfif #TIPOMOVIMIENTO# EQ 'Abono'>$#NumberFormat(IM_MOVIMIENTO,',____.__')#</cfif></td>
								
								<cfif #TIPOMOVIMIENTO# EQ 'Cargo'><cfset saldo=saldo-#IM_MOVIMIENTO#></cfif>
								<cfif #TIPOMOVIMIENTO# EQ 'Abono'><cfset saldo=saldo+#IM_MOVIMIENTO#></cfif>
			                	<td align="right" class="cuadricula"><b <cfif #saldo# LTE 0>style="color:##FF0000;"<cfelse>style="color:##00BB00;"</cfif> >$#NumberFormat(saldo,',____.__')#</b></td>
                	
                			</tr>

      					</cfoutput>	
      				</cfif> 	
      			</cfoutput>		
			</cfoutput>
            	 <tfoot>
					<tr>
	                	<td <cfif #Filtro_Cuenta_bancaria# NEQ ''>colspan="9" <cfelse>colspan="2"</cfif>align="right" style="text-align:right;"><b>TOTAL:</b></td>
						<cfif #Filtro_Cuenta_bancaria# NEQ ''>
							<td align="right" style="text-align:right;"><b <cfif #saldo# LTE 0>style="color:##FF0000;"<cfelse>style="color:##00BB00;"</cfif>>$#NumberFormat(saldo,',____.__')#</b></td>
						<cfelse>	
							<td align="right" style="text-align:right;"><b <cfif #Total_cuenta# LTE 0>style="color:##FF0000;"<cfelse>style="color:##00BB00;"</cfif>>$#NumberFormat(Total_cuenta,',____.__')#</b></td>
						</cfif>
						
                	</tr>
                </tfoot>	
            
		
        </table><br />
    </cfoutput> 	
</cfsavecontent> 
<cfoutput>
	#contenido#
</cfoutput>
<cfheader name="Content-Disposition" value="inline; filename=Reporte_Libro_bancos_#DateFormat(Now(),'DD_MM_YYYY')#.xls" >
	
<cfcontent variable="#ToBinary(ToBase64(contenido))#" type="application/msexcel" >