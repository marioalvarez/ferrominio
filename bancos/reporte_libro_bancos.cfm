<cfparam name="Filtro_id_Empresa" default="">
<cfparam name="Filtro_id_Agrupador" default="">
<cfparam name="id_Obra" default="">
<cfparam name="id_Agrupador" default="">
<cfparam name="id_Agrupador" default="">
<cfparam name="sn_Filtrar" default="">
<cfparam name="fh_inicio" default="">
<cfparam name="fh_fin" default="">
<cfparam name="id_Banco" default="">
<cfparam name="Filtro_Cuenta_bancaria" default="">
<cfif sn_Filtrar NEQ ''>


	<cfinvoke component="#Application.componentes#.bancos" method="reporte_libro_bancos" returnvariable="RsSaldos" 
    	id_Obra="#id_Obra#"
    	fh_inicio="#fh_inicio#"
        fh_fin="#fh_fin#"
        id_Banco="#id_Banco#"
        id_CuentaBancaria="#Filtro_Cuenta_bancaria#"
    >
    <cfif #RsSaldos.SUCCESS# EQ false>
		<cf_error_manejador mensajeError="#RsSaldos.MESSAGE#">                                                                         
	</cfif>   
	<cfquery name="Cargos" dbtype="query">
		select sum(IM_MOVIMIENTO) as cargo
		from RsSaldos.rs
		where TIPOMOVIMIENTO='Cargo'
	</cfquery>
	<!--- <cfquery name="Abonos" dbtype="query">
		select sum(IM_MOVIMIENTO) as abono 
		from RsSaldos.rs
		where TIPOMOVIMIENTO='Abono'
	</cfquery>
	<cfdump var="#Abonos#">
	<cfdump var="#Cargos#"> --->

</cfif>

<!---<cfinvoke component="#Application.componentes#.bancos" method="RSMostrarTodosBancosCuentasBancarias" returnvariable="RSbancos">
<cfif #RSbancos.tipoError# NEQ "">
	<cf_error_manejador tipoError="#RSbancos.tipoError#" mensajeError="#RSbancos.mensaje#">
</cfif>--->
<cfinvoke component="#Application.componentes#.bancos" method="RSMostrarTodosBancos" returnvariable="RSBancos">

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="../css/style.css" type="text/css">
<link rel="stylesheet" type="text/css" href="../css/box.css">
<title>Untitled Document</title>
<script language="javascript" src="../js/jquery.js"></script>
<script language="javascript" src="../js/funciones.js"></script>
<script type="text/javascript" src="../calendario/calendar.js"></script> 
<script type="text/javascript" src="../calendario/calendar-setup.js"></script>
<script type="text/javascript" src="../calendario/lang/calendar-es.js"></script>

<style type="text/css"> @import url("../calendario/calendar-blue2.css");</style>

</head>

<body onLoad="id_Banco.focus();">
	<cfinclude template="menu.cfm">&nbsp;
	<cfform name="form1">
    	<table  width="800" cellpadding="0" cellspacing="0" align="center">
            <tr><td class="x-box-tl" width="1px"></td><td class="x-box-tc"></td><td class="x-box-tr" width="1px"></td></tr>

            <tr>
            	<td class="x-box-ml"></td>
            	<td class="x-box-mc" width="99%">
                	<fieldset>
                    <legend align="center">REPORTE LIBRO DE BANCOS</legend>
                	<table width="100%">
                		  <tr valign="baseline">
                		  		<td align="right">
                		  			Banco:
                		  		</td>
                		  		<td>
									<cfselect name="id_Banco" id="id_Banco" onchange="obtenerCuentasBancarias();" query="RSBancos.RS" queryposition="below" display="de_Banco" value="id_Banco" selected="#id_Banco#" class="contenido" style="width:200px;" required="false" message="El banco no es v\u00e1lido.">
										<option value="">SELECCIONE BANCO</option>
									</cfselect>
                		  		</td>
								<td  align="right">Cuenta bancaria:</td>
								<td>
									<cfselect name="Filtro_Cuenta_bancaria" id="Filtro_Cuenta_bancaria" onchange="" class="contenido" style="width:200px;" required="false" message="La cuenta bancaria no es v\u00e1lida.">
										<option value="">SELECCIONE CUENTA BANCARIA</option>
									</cfselect>
								<td>
						  </tr>
						  <tr>
							<td align="right" width="10%">Obra:&nbsp;</td>
                            <td align="left">
				                <cfinput type="text" class="contenido_numerico" name="id_Obra" id="id_Obra" validate="integer" size="6" value="#id_Obra#">
								<cfinput type="text" bind="cfc:#Application.componentes#.obras.getObraPorID(#Session.id_Empresa#,{id_Obra@blur})" bindonload="yes" onFocus="cambiarFocus($(this).before());" id="de_Obra_lab" name="de_Obra_lab" class="contenido_readOnly" style="overflow:hidden; width:250px" message="La clave de obra no es válida.">
								&nbsp;<button type="button" id="btnBuscarObra" onClick="javascript:AbrirPopUpBusquedaChrome('id_Obra', 'pop_obras_busqueda.cfm', 'id_Empresa=<cfoutput>#Session.id_Empresa#</cfoutput>', '700px', '600px')" class="boton_popup" value=""><img src="../images/buscar.png" alt="Clic para buscar obra" width="24"></button>
                            </td>
						  </tr>
                    	  <tr>
                                 <td align="right">Fecha Inicio:</td>
                                 <td align="left">
                                    <cfinput type="text" name="fh_inicio" id="fh_inicio"  value=""  size="11" mask="99/99/9999" readonly="true" validate="eurodate" class="contenido">
                                  <input type="button" id="lanzador" value="..." / class="boton">
                                </td>
                                <td align="right">Fecha Fin:</td>
                                <td align="left">
                                       <cfinput type="text" name="fh_fin" id="fh_fin"  value=""  size="11" mask="99/99/9999" readonly="true" validate="eurodate" class="contenido" >
                                    <input type="button" id="lanzador2" value="..." / class="boton">    

                                 </td>
                                
                        </tr>
                        <tr>
                        	<td colspan="4" align="center">
                            	<button type="submit" class="boton_imagen"><img src="../images/filtro_azul.png"></button>
                                <cfoutput>
                                 <button  type="button" class="boton_menu_superior" onClick="imprimir()">
                                        <img src="../images/excel.png" alt="Enviar a Excel" title="clic para generar excel" width="24" border="0">
                                    </button></button> 
                                </cfoutput>
                            </td>
                        </tr>
                        <tr>	
							<td>;&nbsp;</td>
                        </tr>
                    </table>
                    </fieldset>
                </td>
                <td class="x-box-mr"></td>
            </tr>
            <tr><td class="x-box-bl" width="1px"></td><td class="x-box-bc"></td><td class="x-box-br" width="1px"></td>
        </table> <br>
		<cfif #sn_Filtrar# EQ 'S'>
         	<cfoutput query="RsSaldos.Rs" group="id_moneda">
	            <table border="0" cellpadding="0" cellspacing="0" <cfif #Filtro_Cuenta_bancaria# NEQ ''> width="1200"<cfelse> width="800"</cfif> class="fondo_listado" align="center">
	                 <thead class="encabezado">
	             		<td class="cuadricula" align="center" <cfif #Filtro_Cuenta_bancaria# NEQ ''>colspan="10"<cfelse>colspan="3"</cfif>><cfif #id_moneda# EQ 1>CUENTA EN PESOS<cfelseif #id_moneda# EQ 2>CUENTA EN DOLARES</cfif>
	             		</td>
	             	 </thead>
	                <tr class="encabezado">
	                	<cfif #Filtro_Cuenta_bancaria# NEQ ''>
		             		<td class="cuadricula" align="center">PROVEEDOR</td>
		             		<td class="cuadricula" align="center">CLIENTE</td>
		             		<td class="cuadricula" align="center">BANCO</td>
		             		<td class="cuadricula" align="center">CUENTA</td>
		             		<td class="cuadricula" align="center">OBRA</td>
							<td class="cuadricula" align="center">FECHA</td>
		                    <td class="cuadricula" align="center">CONCEPTO</td>
		                    <td class="cuadricula" align="center">CARGO</td>
		                    <td class="cuadricula" align="center">ABONO</td>
		                    <td class="cuadricula" align="center">SALDO</td>
		                <cfelse>
		                	<td class="cuadricula" align="center">BANCO</td>
		             		<td class="cuadricula" align="center">CUENTA</td>
		             		<td class="cuadricula" align="center">SALDO</td>
		             		
		                </cfif>    
	                 
	                </tr>
	                <cfset saldo=0>
	                <cfset Total_cuenta=0>
	                <cfoutput group="id_banco">
	                	<cfoutput group="id_CuentaBancaria">
							<cfif #Filtro_Cuenta_bancaria# EQ ''>	
								<tr <cfif #Filtro_Cuenta_bancaria# NEQ ''>style="background-color:##CBBCBC"<cfelse>class="renglon_grid"</cfif>>
										
					                	<td align="center" class="cuadricula" <cfif #Filtro_Cuenta_bancaria# NEQ ''> colspan="4" </cfif>>#DE_BANCO# </td>
					                	<td align="center" class="cuadricula"  <cfif #Filtro_Cuenta_bancaria# NEQ ''> colspan="3" </cfif>>#NU_CUENTABANCARIA#</td>
					                	<cfquery name="cargos" dbtype="query">
					                		SELECT
					                			sum(IM_MOVIMIENTO) as cargos
					                		FROM
					                			RsSaldos.Rs
					                		WHERE
					                			id_cuentaBancaria=#id_cuentaBancaria# and id_Banco=#id_Banco# and TIPOMOVIMIENTO='Cargo'
					                	</cfquery>
					                	<cfquery name="abonos" dbtype="query">
					                		SELECT
					                			sum(IM_MOVIMIENTO) as abonos
					                		FROM
					                			RsSaldos.Rs
					                		WHERE
					                			id_cuentaBancaria=#id_cuentaBancaria# and id_Banco=#id_Banco# and TIPOMOVIMIENTO='Abono'
					                	</cfquery>
											
						                <cfif cargos.cargos EQ ''>
						                	<cfset cargo=0>
							            <cfelse>
							            	<cfset cargo=cargos.cargos>  			
					                	</cfif>
										<cfif abonos.abonos EQ ''>
						                	<cfset abono=0>
						                <cfelse>
							            	<cfset abono=abonos.abonos>  	
					                	</cfif>
					                	<cfset saldo_Cuenta= abono-cargo>
					                	<cfset Total_cuenta=Total_cuenta+saldo_Cuenta>
					                	<td align="right" class="cuadricula"  <cfif #Filtro_Cuenta_bancaria# NEQ ''> colspan="3" </cfif>><b <cfif #saldo_Cuenta# LTE 0>style="color:##FF0000;"<cfelse>style="color:##00BB00;"</cfif>>$#NumberFormat(saldo_Cuenta,',____.__')#<b></td>
		                	
		                		</tr>
							</cfif>
		      				<cfif #Filtro_Cuenta_bancaria# NEQ ''>
		      					<cfoutput>
									<tr class="renglon_grid" id="id_#ID_BANCO#_#ID_CUENTABANCARIA#_#ID_MOVIMIENTO#" onClick="Mostrar_OC(#ID_BANCO#,#ID_CUENTABANCARIA#,#ID_MOVIMIENTO#,#ID_TIPOMOVIMIENTO#)" click="0" style="cursor:pointer;">
										<td align="left" class="cuadricula">#NB_PROVEEDOR#</td>
					                	<td align="left" class="cuadricula">#NB_CLIENTE#</td>
					                	<td align="center" class="cuadricula">#DE_BANCO#</td>
					                	<td align="center" class="cuadricula">#NU_CUENTABANCARIA#</td>
					                	<td align="center" class="cuadricula">#ID_OBRA#</td>
					                	<td align="center" class="cuadricula">#DATEFORMAT(FH_MOVIMIENTO,'DD/MM/YYYY')#</td>
					                	<td align="left" class="cuadricula">#DE_TIPOMOVIMIENTO#</td>
					                	<td align="right" class="cuadricula"><cfif #TIPOMOVIMIENTO# EQ 'Cargo'>$#NumberFormat(IM_MOVIMIENTO,',____.__')#</cfif></td>
					                	<td align="right" class="cuadricula"><cfif #TIPOMOVIMIENTO# EQ 'Abono'>$#NumberFormat(IM_MOVIMIENTO,',____.__')#</cfif></td>
										
										<cfif #TIPOMOVIMIENTO# EQ 'Cargo'><cfset saldo=saldo-#IM_MOVIMIENTO#></cfif>
										<cfif #TIPOMOVIMIENTO# EQ 'Abono'><cfset saldo=saldo+#IM_MOVIMIENTO#></cfif>
					                	<td align="right" class="cuadricula"><b <cfif #saldo# LTE 0>style="color:##FF0000;"<cfelse>style="color:##00BB00;"</cfif> >$#NumberFormat(saldo,',____.__')#</b></td>
		                	
		                			</tr>

		      					</cfoutput>	
		      				</cfif> 	
		      			</cfoutput>		
					</cfoutput>
	                	 <tfoot>
							<tr>
			                	<td <cfif #Filtro_Cuenta_bancaria# NEQ ''>colspan="9" <cfelse>colspan="2"</cfif>align="right" style="text-align:right;"><b>TOTAL:</b></td>
								<cfif #Filtro_Cuenta_bancaria# NEQ ''>
									<td align="right" style="text-align:right;"><b <cfif #saldo# LTE 0>style="color:##FF0000;"<cfelse>style="color:##00BB00;"</cfif>>$#NumberFormat(saldo,',____.__')#</b></td>
								<cfelse>	
									<td align="right" style="text-align:right;"><b <cfif #Total_cuenta# LTE 0>style="color:##FF0000;"<cfelse>style="color:##00BB00;"</cfif>>$#NumberFormat(Total_cuenta,',____.__')#</b></td>
								</cfif>
								
		                	</tr>
		                </tfoot>	
	                
				
	            </table><br />
	        </cfoutput>    

        </cfif> 
        <cfinput name="sn_Filtrar" type="hidden" value="S">  	
    </cfform>
</body>
<script language="javascript" src="../ferrominio/js/modal/modal.js"></script>
<link rel="stylesheet" href="../ferrominio/js/modal/modal.css" type="text/css">
<script language="javascript">
	function obtenerCuentasBancarias(idCuentaBancaria)
	{
		document.getElementById("Filtro_Cuenta_bancaria").length = 0;
		$( '#Filtro_Cuenta_bancaria' ).append( '<option value="">SELECCIONE CUENTA BANCARIA</option>' );
		
		if ($('#id_Banco').val() == '')
			return false;
		
		$.ajax({
			type: 'GET',
			async: true,
			dataType: 'JSON',
			url: '../componentes/cuentasbancariasmovimientos_cancelaciones.cfc',
			data: {
				method:'get_Listado_CuentasBancarias_Por_Banco_Ajax',
				id_Banco: $('#id_Banco').val()
			},
			success: function(response) {
				if (response.SUCCESS)
				{
					//Recorremos el query
					for(var i=0;i<response.RS.DATA.length;i++)
					{
						var value = response.RS.DATA[i][response.RS.COLUMNS.indexOf("ID_CUENTABANCARIA")];
						var DisplayValue = response.RS.DATA[i][response.RS.COLUMNS.indexOf("DE_CUENTABANCARIA")];
						
						$( '#Filtro_Cuenta_bancaria' ).append( '<option ' + (value == idCuentaBancaria ? 'selected="selected"' : '') + ' value="'+ value + '">' + DisplayValue + '</option>' );
					}
				}
			}
		});
	}
	obtenerCuentasBancarias(<cfoutput>#Filtro_Cuenta_bancaria#</cfoutput>);
	function imprimir(){
			var id_banco=$('#id_Banco').val();
			var id_cuentaBancaria=$('#Filtro_Cuenta_bancaria').val();
			var id_obra=$('#id_Obra').val();
			var fh_inicio=$('#fh_inicio').val();
			var fh_fin=$('#fh_fin').val();

			if(id_banco==''){
				alert("Seleccione un Banco");
			}else{
				 window.open('reporte_libro_bancos_excel.cfm?id_Banco='+id_banco+'&Filtro_Cuenta_bancaria='+id_cuentaBancaria+'&obra='+id_obra+'&fh_inicio='+fh_inicio+'&fh_fin='+fh_fin,'_Self');		
			}

	}
	function mostrar_ocultar_tr(id){
		if ($('#tr_'+id)[0] && $('#tr_'+id)[0].style.display == 'none'){
			$('tr[id=tr_'+id+']').show();
		}else if ($('#tr_'+id)[0]){
			$('tr[id^=tr_'+id+']').hide();
		}
	}
	
	function Mostrar_OC(id_Banco,id_CuentaBancaria,id_Movimiento,id_TipoMovimiento){
	    var click = $('#id_'+id_Banco+'_'+id_CuentaBancaria+'_'+id_Movimiento).attr('click');

         if(click ==0){
	     var click = $('#id_'+id_Banco+'_'+id_CuentaBancaria+'_'+id_Movimiento).attr('click',1);

         	 $.ajax({
					url:'../componentes/bancos.cfc',
					type:'post',
					dataType:'JSON',
					data:{
						method:'MostrarOC_movimiento',
						id_banco:id_Banco,
						id_CuentaBancaria:id_CuentaBancaria,
						id_Movimiento:id_Movimiento,
						id_TipoMovimiento:id_TipoMovimiento
					},
					success:function(data){
						var arreglo=JSON.parse(data.QUERY)
						data.QUERY=arreglo;
						console.log(data);
						if(data.QUERY.length >0){
							var contenido="<tr class='tr_"+id_Banco+'_'+id_CuentaBancaria+'_'+id_Movimiento+" encabezado_grande''>"+ 
	                                    "<td class='cuadricula' colspan='3' align='center'>Empresa</td>"+
	                                    "<td class='cuadricula' colspan='3' align='center'>Obra</td>"+
	                                    "<td class='cuadricula' colspan='2' align='center'>Orden Compra</td>"+
	                                    "<td class='cuadricula' colspan='2' align='center'>Total</td>"+
	                                    
                                	  "</tr>";
							for(var i=0;i<data.QUERY.length;i++){
							  contenido+="<tr class='tr_"+id_Banco+'_'+id_CuentaBancaria+'_'+id_Movimiento+"' id='id_"+data.QUERY[i].ID_EMPRESA+'_'+data.QUERY[i].ID_OBRA+'_'+data.QUERY[i].ID_ORDENCOMPRA+"' onClick='Mostrar_factura("+data.QUERY[i].ID_EMPRESA+","+data.QUERY[i].ID_OBRA+","+data.QUERY[i].ID_ORDENCOMPRA+","+id_Banco+","+id_CuentaBancaria+","+id_Movimiento+")' style='cursor:pointer;' click='0'>"+ 
		                                    "<td class='cuadricula' colspan='3' align='center'>"+data.QUERY[i].ID_EMPRESA+"-"+data.QUERY[i].NB_EMPRESA+"</td>"+
		                                    "<td class='cuadricula' colspan='3' align='center'>"+data.QUERY[i].ID_OBRA+"-"+data.QUERY[i].DE_OBRA+"</td>"+
		                                    "<td class='cuadricula' colspan='2' align='center'>"+data.QUERY[i].ID_ORDENCOMPRA+"</td>"+
		                                    "<td class='cuadricula' colspan='2' align='center'>"+formatea(data.QUERY[i].IM_TOTAL,2)+"</td>"+
		                                 "</tr>";
							}
							contenido+="<tr class='tr_"+id_Banco+'_'+id_CuentaBancaria+'_'+id_Movimiento+" encabezado_grande''>"+ 
		                                    "<td class='cuadricula' colspan='10' align='center'></td>"+
		                                   
		                                    
	                                	 "</tr>";
							
							 $('tr[id=id_'+id_Banco+'_'+id_CuentaBancaria+'_'+id_Movimiento+']').after(contenido);	
						}	
						
					}

			 });
         }else{
	    	 $('.tr_'+id_Banco+'_'+id_CuentaBancaria+'_'+id_Movimiento).remove();
	    	 $('.tr_'+id_Banco+'_'+id_CuentaBancaria+'_'+id_Movimiento).remove();
			 $('#id_'+id_Banco+'_'+id_CuentaBancaria+'_'+id_Movimiento).attr('click',0);


         }	
       

	}
	function Mostrar_factura(id_Empresa,id_Obra,id_OrdenCompra,id_Banco,id_CuentaBancaria,id_Movimiento){

		 var click = $(event.path[1]).attr('click');
		   if(click ==0){
	     var click = $('#id_'+id_Empresa+'_'+id_Obra+'_'+id_OrdenCompra).attr('click',1);

         	 $.ajax({
					url:'../componentes/bancos.cfc',
					type:'post',
					dataType:'JSON',
					data:{
						method:'MostrarFacturas_movimiento',
						id_Empresa:id_Empresa,
						id_Obra:id_Obra,
						id_OrdenCompra:id_OrdenCompra
					},
					success:function(data){
						var arreglo=JSON.parse(data.QUERY)
						data.QUERY=arreglo;
						console.log(data)
						 if(data.QUERY.length >0){
						 	var contenido="<tr class='id_"+id_Empresa+'_'+id_Obra+'_'+id_OrdenCompra+" encabezado_grande tr_"+id_Banco+'_'+id_CuentaBancaria+'_'+id_Movimiento+"' >"+ 
	                                    "<td class='cuadricula' colspan='3' align='center'>Empresa</td>"+
	                                    "<td class='cuadricula' colspan='3' align='center'>Obra</td>"+
	                                    "<td class='cuadricula' colspan='2' align='center'>Factura</td>"+
	                                    "<td class='cuadricula' colspan='2' align='center'>Total</td>"+
	                                    
                                	  "</tr>";
						for(var i=0;i<data.QUERY.length;i++){
							console.log(data.QUERY[i].ID_EMPRESA);
							console.log(data.QUERY[i].NB_EMPRESA);

						  contenido+="<tr class='id_"+id_Empresa+'_'+id_Obra+'_'+id_OrdenCompra+" tr_"+id_Banco+'_'+id_CuentaBancaria+'_'+id_Movimiento+"' id='id_"+id_Empresa+'_'+data.QUERY[i].ID_CONTRARECIBO+"'  onClick='Mostrar_insumos("+data.QUERY[i].ID_EMPRESA+","+data.QUERY[i].ID_CONTRARECIBO+","+id_Banco+","+id_CuentaBancaria+","+id_Movimiento+","+id_OrdenCompra+","+id_Obra+")' style='cursor:pointer;' click='0'>"+ 
	                                    "<td class='cuadricula' colspan='3' align='center' style='background-color:#B4CBE6;'>"+data.QUERY[i].ID_EMPRESA+"-"+data.QUERY[i].NB_EMPRESA+"</td>"+
	                                    "<td class='cuadricula' colspan='3' align='center' style='background-color:#B4CBE6;'>"+data.QUERY[i].ID_OBRA+"-"+data.QUERY[i].DE_OBRA+"</td>"+
	                                    "<td class='cuadricula' colspan='2' align='center' style='background-color:#B4CBE6;'>"+data.QUERY[i].DE_FACTURA+"</td>"+
	                                    "<td class='cuadricula' colspan='2' align='center' style='background-color:#B4CBE6;''>$"+formatea(data.QUERY[i].IM_FACTURA,2)+"</td>"+
	                                 "</tr>";
						}
						contenido+="<tr class='id_"+id_Empresa+'_'+id_Obra+'_'+id_OrdenCompra+" encabezado_grande' id='tr_"+id_Banco+'_'+id_CuentaBancaria+'_'+id_Movimiento+"'>"+ 
	                                    "<td class='cuadricula' colspan='10' align='center'></td>"+
	                                   
	                                    
                                	 "</tr>";
						
						 $('tr[id=id_'+id_Empresa+'_'+id_Obra+'_'+id_OrdenCompra+']').after(contenido);	

						 }
						
					}

			 });
         }else{
	    	 $('.id_'+id_Empresa+'_'+id_Obra+'_'+id_OrdenCompra).remove();
	         $('#id_'+id_Empresa+'_'+id_Obra+'_'+id_OrdenCompra).attr('click',0);



         }	

	}
	function Mostrar_insumos(id_Empresa,id_Contrarecibo,id_banco,id_cuentaBancaria,id_Movimiento,id_OrdenCompra,id_Obra){
	

		var click = $(event.path[1]).attr('click');
		
		 $('#id_'+id_Empresa+'_'+id_Contrarecibo).attr('click',1);

		 if(click==0){
		 	$('#id_'+id_Empresa+'_'+id_Contrarecibo).attr('click',1);
		 	$.ajax({
					url:'../componentes/bancos.cfc',
					type:'post',
					dataType:'JSON',
					data:{
						method:'MostrarInsumos_Factura',
						id_Empresa:id_Empresa,
						id_Contrarecibo:id_Contrarecibo
					},
					success:function(data){
						var arreglo=JSON.parse(data.QUERY)
						data.QUERY=arreglo;
					
						 if(data.QUERY.length >0){

						 	var contenido="<tr class='id_"+id_Empresa+'_'+id_Contrarecibo+" encabezado_grande tr_"+id_Banco+'_'+id_cuentaBancaria+'_'+id_Movimiento+" id_"+id_Empresa+'_'+id_Obra+'_'+id_OrdenCompra+"' >"+ 
	                                    "<td class='cuadricula' colspan='3' align='center'>Insumo</td>"+
	                                    "<td class='cuadricula' colspan='5' align='center'>Descrpcion</td>"+
	                                    "<td class='cuadricula' colspan='2' align='center'>Pagado</td>"+
	                                  "</tr>";
						for(var i=0;i<data.QUERY.length;i++){

						  contenido+="<tr class='id_"+id_Empresa+'_'+id_Contrarecibo+" tr_"+id_banco+'_'+id_cuentaBancaria+'_'+id_Movimiento+" id_"+id_Empresa+'_'+id_Obra+'_'+id_OrdenCompra+"' id='id_"+id_Empresa+'_'+data.QUERY[i].ID_CONTRARECIBO+"'  click='0'>"+ 
	                                    "<td class='cuadricula' colspan='3' align='center' style='background-color:#B8B8B8;'>"+data.QUERY[i].ID_INSUMO+"</td>"+
	                                    "<td class='cuadricula' colspan='5' align='center' style='background-color:#B8B8B8;'>"+data.QUERY[i].DE_INSUMO+"</td>"+
	                                    "<td class='cuadricula' colspan='2' align='center' style='background-color:#B8B8B8;'>$"+formatea(data.QUERY[i].IM_ANALISIS,2)+"</td>"+
	                                 "</tr>";
						}
						contenido+="<tr class='id_"+id_Empresa+'_'+id_Contrarecibo+" encabezado_grande' id='tr_"+id_Banco+'_'+id_cuentaBancaria+'_'+id_Movimiento+"'>"+ 
	                                    "<td class='cuadricula' colspan='10' align='center'></td>"+
	                                   
	                                    
                                	 "</tr>";
						
						 $('tr[id=id_'+id_Empresa+'_'+id_Contrarecibo+']').after(contenido);	

						 }
						
					}

			 });
		 }else{
		 	$('.id_'+id_Empresa+'_'+id_Contrarecibo).remove();
		 	$('#id_'+id_Empresa+'_'+id_Contrarecibo).attr('click',0);
		 }
		
	}
	function imprimr_pdf(id_Empresa,id_Obra,id_Agrupador){
		if(id_Empresa == ''){
			alert('Seleccione Empresa');
			$('#Filtro_id_Empresa').focus();
			return false;
		}else if(id_Obra == ''){
			alert('Ingrese Obra');
			$('#id_Obra').focus();
			return false;
		}
		window.open('reporte_desglosado_saldos_pdf.cfm?id_Empresa='+id_Empresa+'&id_Obra='+id_Obra+'&id_Agrupador='+id_Agrupador+'')
	}
	function imprimr_exel(id_Empresa,id_Obra,id_Agrupador){
		if(id_Empresa == ''){
			alert('Seleccione Empresa');
			$('#Filtro_id_Empresa').focus();
			return false;
		}else if(id_Obra == ''){
			alert('Ingrese Obra');
			$('#id_Obra').focus();
			return false;
		}
		window.open('reporte_desglosado_saldos_excel.cfm?id_Empresa='+id_Empresa+'&id_Obra='+id_Obra+'&id_Agrupador='+id_Agrupador+'','_Self')
	}
	 Calendar.setup({
                    inputField     :    "fh_inicio",      // id del campo de texto
                    ifFormat       :    "%d/%m/%Y",       // formato de la fecha, cuando se escriba en el campo de texto
                    button         :    "lanzador"   // el id del botón que lanzará el calendario
                });
                
                    Calendar.setup({
                    inputField     :    "fh_fin",      // id del campo de texto
                    ifFormat       :    "%d/%m/%Y",       // formato de la fecha, cuando se escriba en el campo de texto
                    button         :    "lanzador2"   // el id del botón que lanzará el calendario
    });
</script>
</html>
