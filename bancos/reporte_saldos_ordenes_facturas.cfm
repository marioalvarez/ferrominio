<link rel="stylesheet" href="../css/style.css" type="text/css">
<link rel="stylesheet" type="text/css" href="../css/box.css">
<cfif Filtro_id_Obra EQ ''>
	<cfset Filtro_id_Obra = 'NULL'>
</cfif>
<cfif #Filtro_id_Agrupador# EQ 'DE-001' AND #Filtro_id_OrdenCompra# EQ 0>
    <cfquery name="Facturas" datasource="#session.cnx#">
        Facturas_Destajos #Filtro_id_Empresa#, #Filtro_id_Obra#, #Filtro_id_Proveedor#
    </cfquery>
<cfelseif #Filtro_id_TipoOrdenCompra# EQ 4 OR #Filtro_id_TipoOrdenCompra# EQ 1>
    <cfquery name="Facturas" datasource="#session.cnx#">
        Facturas_Subcontrato #Filtro_id_Empresa#, #Filtro_id_Obra#, #Filtro_id_OrdenCompra#
    </cfquery>
<cfelseif #Filtro_id_OrdenCompra# EQ -1>
    <cfquery name="Facturas" datasource="#session.cnx#">
        Facturas_TraspasoIndirecto #Filtro_id_Empresa#, #Filtro_id_Obra#
    </cfquery>
<cfelse>
    <cfquery name="Facturas" datasource="#session.cnx#">
        Ordenes_Compra_Facturas_Detalle #Filtro_id_Empresa#, #Filtro_id_Obra#, '#Filtro_id_Agrupador#', #Filtro_id_OrdenCompra#
    </cfquery>                
</cfif>
<cfquery name="nu_Agrupadores" datasource="#session.cnx#">
    select  
        ocd.id_Empresa,ocd.id_Obra,ocd.id_OrdenCompra,count(1) as nu_Agrupadores
    from 
        OrdenesCompra oc
        inner join
        (
            select id_Empresa,id_Obra,id_OrdenCompra,id_Agrupador
            from OrdenesCompraDetalle ocd 
            inner join Insumos i on i.id_Insumo = ocd.id_Insumo
            group by id_Empresa,id_Obra,id_OrdenCompra,id_Agrupador
        )
        ocd on oc.id_Empresa = ocd.id_Empresa and oc.id_Obra = ocd.id_Obra and oc.id_OrdenCompra = ocd.id_OrdenCompra
    where oc.id_Empresa = #Filtro_id_Empresa# and oc.id_Obra = #Filtro_id_Obra# and oc.id_OrdenCompra = #Filtro_id_OrdenCompra#
    group by ocd.id_Empresa,ocd.id_Obra, ocd.id_OrdenCompra
</cfquery>
<cfquery name="OrdenesCompra" datasource="#session.cnx#">
    Ordenes_Compra_Agrupador_Detalle #Filtro_id_Empresa#, #Filtro_id_Obra#, '#Filtro_id_Agrupador#'
</cfquery>

<table cellpadding="0" cellspacing="0" width="100%">
	<cfoutput query="Facturas">
    	<cfset im_Gastos = 0>
		<cfif #OrdenesCompra.im_OtrosGastos# GT 0>        
            <cfquery name="oc_Insumos" datasource="#session.cnx#">
                select SUM(im_Analisis) as im_Agrupador,id_Agrupador
                from ContraRecibosDetalleAnalisisInsumos ocd 
                inner join Insumos i on i.id_Insumo = ocd.id_Insumo
                where id_Empresa = #Filtro_id_Empresa# and id_Obra = #Filtro_id_Obra# and id_ContraRecibo = #Facturas.id_ContraRecibo# and id_Agrupador = '#Filtro_id_Agrupador#'
                group by id_Agrupador
            </cfquery>
			<cfquery name="subtotal_OC" datasource="#session.cnx#">
            	select im_subtotal,im_gastosManiobra from ordenesCompra where id_Empresa = #Filtro_id_Empresa# and id_Obra = #Filtro_id_Obra# and id_OrdenCompra = #Filtro_id_OrdenCompra#
            </cfquery>
			<cfset pj_Total = (#oc_Insumos.im_Agrupador# * 100) / #subtotal_OC.im_Subtotal#>
			<cfset im_Gastos = (#pj_Total# / 100) * #subtotal_OC.im_gastosManiobra#>
		</cfif>    	    
        <tr>
            <td width="20%" class="cuadricula">FACTURA: #Facturas.de_Factura#</td>
            <td align="center" width="10%" class="cuadricula">#DateFormat(Facturas.fh_Factura,'DD/MM/YYYY')#</td>
            <td align="center" width="30%" class="cuadricula">#Facturas.de_Estatus#</td>

			<cfif #Filtro_id_Agrupador# EQ 'DE-001' AND #Filtro_id_OrdenCompra# EQ 0>
				<cfset im_Total_OC = #Facturas.im_CantidadPrecio#>
            <cfelseif #Filtro_id_TipoOrdenCompra# EQ 4 OR #Filtro_id_TipoOrdenCompra# EQ 1>
				<cfset im_Total_OC = #Facturas.im_CantidadPrecio#>
            <cfelse>
				<cfset im_subtotal_OC = (#Facturas.im_CantidadPrecio# + #im_Gastos#) > 
                
                <cfset im_Iva = iif(pj_Iva NEQ 0,im_subtotal_OC * (pj_Iva/100),0)>
                <cfset im_Total_OC = #im_subtotal_OC# + #im_Iva#>
                            
				<cfif #nu_Agrupadores.nu_Agrupadores# EQ 1>
                    <cfset im_Total_OC = #im_Factura#>
                </cfif>				
            </cfif>
            <td align="right" width="30%" class="cuadricula">#NumberFormat(im_Total_OC,'$,_.__')#</td>
        </tr>
    </cfoutput>
</table>
