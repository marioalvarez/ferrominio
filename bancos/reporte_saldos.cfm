<cfparam name="Filtro_id_Empresa" default="">
<cfparam name="Filtro_id_Obra" default="">
<cfparam name="Filtro_id_Agrupador" default="">
<cfparam name="sn_Mostrar" default="">
<cfquery name="RsEmpresas" datasource="#session.cnx#">
    SELECT id_Empresa, nb_Empresa FROM Empresas
</cfquery>
<cfquery name="RsAgrupadores" datasource="#session.cnx#">
    SELECT 
        id_Agrupador
        ,id_Agrupador + ' - ' +  de_Agrupador AS de_Agrupador
    FROM 
        Agrupadores_fe 
</cfquery>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="../css/style.css" type="text/css">
<link rel="stylesheet" type="text/css" href="../css/box.css">
<title>Untitled Document</title>
<script language="javascript" src="../js/jquery.js"></script>
<script language="javascript" src="../js/funciones.js"></script>
<script language="javascript" src="../js/funciones_ajax.js"></script>
<script>
    function mostrar(id,empresa,obra,agrupador)
    {
        if($x('tr_'+id).style.display == 'none')
        {
            $x('tr_'+id).style.display='';
            cargar_pagina('reporte_saldos_ordenes.cfm?Filtro_id_Empresa='+empresa+'&Filtro_id_Obra='+obra+'&Filtro_id_Agrupador='+agrupador,$x('div_'+id),'loader.gif');
        }
        else
        {
            $x('tr_'+id).style.display='none';
        }
    }
    function mostrar_oc_facturas(id,empresa,obra,oc,proveedor,tipooc,agrupador)
    {
        if($x('tr_oc_factura_'+id+obra).style.display == 'none')
        {
            $x('tr_oc_factura_'+id+obra).style.display='';
            cargar_pagina('reporte_saldos_ordenes_facturas.cfm?Filtro_id_Empresa='+empresa+'&Filtro_id_Obra='+obra+'&Filtro_id_OrdenCompra='+oc+'&Filtro_id_Proveedor='+proveedor+'&Filtro_id_TipoOrdenCompra='+tipooc+'&Filtro_id_Agrupador='+agrupador,$x('div_oc_factura_'+id+obra),'loader.gif');
        }
        else
        {
            $x('tr_oc_factura_'+id+obra).style.display='none';
        }
    }
    function imprimir(opcion)
    {
        if($('#Filtro_id_Empresa').val() == '')
        {
            alert('Seleccione Empresa');
            $('#Filtro_id_Empresa').focus();
            return false;
        }
        if(opcion == 1)
        {
            $('#cargando').fadeIn();
            $('#tbl_Principal').hide();
            form1.action = 'reporte_saldos.cfm';
            form1.submit();
        }
        else
        {
            $('#tbl_Principal').hide();
            window.open('reporte_desglosado_saldos_excel.cfm?id_Empresa='+$('#Filtro_id_Empresa').val() +'&Filtro_id_Obra='+$('#Filtro_id_Obra').val()+'&id_Agrupador='+$('#Filtro_id_Agrupador').val()+'','_Self')
            //form1.action = 'reporte_desglosado_saldos_excel.cfm?id_Empresa='+$('#Filtro_id_Empresa').val()+'&Filtro_id_Obra='+$('#Filtro_id_Obra').val()+'&id_Agrupador='+$('#Filtro_id_Agrupador').val();    
            //form1.submit();
        }
    }
    
</script>
</head>

<body onLoad="$('#Filtro_id_Empresa').focus()">
    <cfinclude template="menu.cfm">&nbsp;
    <cfform name="form1" onsubmit="$('##cargando').fadeIn(); $('##tbl_Principal').hide();">
        <table width="900" cellpadding="0" cellspacing="0" align="center">
            <tr><td class="x-box-tl" width="1px"></td><td class="x-box-tc"></td><td class="x-box-tr" width="1px"></td></tr>
            <tr>
                <td class="x-box-ml"></td>
                <td class="x-box-mc" width="99%">
                    <fieldset>
                    <legend align="center">REPORTE SALDOS</legend>
                    <table width="100%">
                        <tr>
                            <td align="right" width="">Empresa:&nbsp;</td>
                            <td align="left" width="">
                                <cfselect name="Filtro_id_Empresa" id="Filtro_id_Empresa" style="width:98%;" required="yes" message="Seleccione Empresa">
                                    <option value="">SELECCIONE EMPRESA</option>
                                    <cfoutput query="RsEmpresas">
                                        <option value="#id_Empresa#" <cfif #Filtro_id_Empresa# EQ #id_Empresa#>selected="selected"</cfif>>#nb_Empresa#</option>
                                    </cfoutput>
                                </cfselect>
                            </td>
                            <td align="right" width="">Obra:&nbsp;</td>
                            <td align="left">
                                <cfinput type="text" class="contenido_numerico" name="Filtro_id_Obra" id="Filtro_id_Obra" validate="integer" size="6" value="#Filtro_id_Obra#">
                                <cfinput type="text" bind="cfc:#Application.componentes#.obras.getObraPorID({Filtro_id_Empresa@blur},{Filtro_id_Obra@blur})" bindonload="yes" onFocus="cambiarFocus($(this).before());" id="de_Obra_lab" name="de_Obra_lab" class="contenido_readOnly" style="overflow:hidden; width:250px" message="La clave de obra no es válida.">
                                &nbsp;<button type="button" id="btnBuscarObra" onClick="javascript:AbrirPopUpBusquedaChrome('Filtro_id_Obra', 'pop_obras_busqueda.cfm', '', '700px', '600px')" class="boton_popup" value=""><img src="../images/buscar.png" alt="Clic para buscar obra" width="24"></button>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">Agrupador:&nbsp;</td>
                            <td align="left">
                                <cfselect name="Filtro_id_Agrupador" id="Filtro_id_Agrupador" style="width:98%;" class="contenido">
                                    <option value="">TODOS</option>
                                    <cfoutput query="RsAgrupadores">
                                        <option value="#id_Agrupador#" <cfif #id_Agrupador# EQ #Filtro_id_Agrupador#>selected="selected"</cfif>>#de_Agrupador#</option>
                                    </cfoutput>
                                </cfselect>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" align="center">
                                <button type="button" class="boton_imagen" onclick="imprimir(1);"><img src="../images/filtro_azul.png"></button>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                                <button  type="button" class="boton_menu_superior" onClick="imprimir(2);">
                                    <img src="../images/excel.png" alt="Enviar a Excel" title="clic para generar excel" width="24" border="0">
                                </button>
                            </td>
                        </tr>
                    </table>
                    </fieldset>
                </td>
                <td class="x-box-mr"></td>
            </tr>
            <tr><td class="x-box-bl" width="1px"></td><td class="x-box-bc"></td><td class="x-box-br" width="1px"></td>
        </table>
        <div id="cargando" style="display:none;">
            <center><img src="../images/cargando_procesando.gif"></center>
        </div>

        <cfinput name="sn_Mostrar" type="hidden" value="S">     
    </cfform>
</body>

<cfif sn_Mostrar EQ 'S'>
    <cfinvoke component="#Application.componentes#.bancos" method="INGRESOS_OBRA" returnvariable="RSIngresosObra" 
        id_Empresa="#Filtro_id_Empresa#"
        id_Obra="#Filtro_id_Obra#"
    >
    <cfinvoke component="#Application.componentes#.bancos" method="INGRESOS_DIVERSOS" returnvariable="RSIngresosDiversos" 
        id_Empresa="#Filtro_id_Empresa#"
        id_Obra="#Filtro_id_Obra#"
    >
  
    <cfif #RSIngresosObra.SUCCESS# EQ false>
        <cf_error_manejador mensajeError="#RSIngresosObra.MESSAGE#">                                                                         
    </cfif>   

    <cfinvoke component="#Application.componentes#.bancos" method="reporte_desglosado_saldos_detalle" returnvariable="RsSaldos" 
            id_Empresa="#Filtro_id_Empresa#"
            id_Obra="#Filtro_id_Obra#"
            id_Agrupador="#Filtro_id_Agrupador#"
        >
    <cfif #RsSaldos.SUCCESS# EQ false>
        <cf_error_manejador mensajeError="#RsSaldos.MESSAGE#">                                                                         
    </cfif>
    <br />
        <table cellpadding="0" cellspacing="0" width="100%" id="tbl_Principal" class="fondo_listado">
            <tr class="encabezado">
                <td class="cuadricula" align="center">CLAVE</td>
                <td class="cuadricula" align="center">AGRUPADOR</td>
                <td class="cuadricula" align="center">PRESUPUESTADO</td>
                <td class="cuadricula" align="center">PAGADO</td>
                <td class="cuadricula" align="center">PORCENTAJE DE PAGO</td>
                <td class="cuadricula" align="center">SALDO</td>
            </tr>
            <cfset Total_im_Presupuestado = 0>
            <cfset Total_im_Pagado = 0>
            <cfset Total_pj_Pagado = 0>
            <cfset Total_im_Saldo = 0>
            <cfoutput query="RsSaldos.Rs" group="id_Agrupador">
                <cfquery name="RSTotales" datasource="#session.cnx#">
                    EXECUTE Reporte_Saldos_Obtener_Pagado_Agrupador #RsSaldos.rs.id_Empresa#, <cfif Filtro_id_Obra NEQ ''>#Filtro_id_Obra#<cfelse>NULL</cfif>, '#RsSaldos.rs.id_Agrupador#'
                </cfquery>
                <tr style=" cursor:pointer" onclick="mostrar(#CurrentRow#, #RsSaldos.Rs.id_Empresa#, <cfif Filtro_id_Obra NEQ ''>#Filtro_id_Obra#<cfelse>''</cfif>, '#RsSaldos.Rs.id_Agrupador#')">
                    <td class="cuadricula" align="left" width="10%">#id_Agrupador#</td>
                    <td class="cuadricula" align="left" width="30%">#de_Agrupador#</td>
                    <td class="cuadricula" align="right" width="15%">$#NumberFormat(Presupuestado,',_.__')#</td>
                    <td class="cuadricula" align="right" width="15%">$#NumberFormat(RSTotales.pagadoAgrupador,',_.__')#</td>
                    <cfset im_Saldo = Presupuestado - iif(RSTotales.pagadoAgrupador EQ '',0,RSTotales.pagadoAgrupador)>

                    <cfif Presupuestado NEQ 0 AND RSTotales.pagadoAgrupador NEQ 0>
                        <cfset pj_Pagado = (RSTotales.pagadoAgrupador/Presupuestado)*100>
                    <cfelse>    
                        <cfset pj_Pagado = 0>
                    </cfif>
                    <td class="cuadricula" align="right" width="20%">#NumberFormat(pj_Pagado,',____.__')#%</td>
                    <td class="cuadricula" align="right" width="20%">$#NumberFormat(im_Saldo,',____.__')#</td>
                </tr>
                <cfset Total_im_Presupuestado += #Presupuestado#>
                <cfset Total_im_Pagado += #RSTotales.pagadoAgrupador#>
                <cfset Total_pj_Pagado += #pj_Pagado#>
                <cfset Total_im_Saldo += #im_Saldo#>
                <tr id="tr_#currentRow#" style="display:none;">
                    <td width="100%" colspan="6">
                        <div id="div_#CurrentRow#" style="width:100%" align="center"></div>
                    </td>
                </tr>
            </cfoutput> 
            <cfoutput>
            <tr style="font-weight:bold">
                <td align="right" class="cuadricula" colspan="2">TOTALES:</td>
                <td align="right" class="cuadricula">#NumberFormat(Total_im_Presupuestado,'$,_.__')#</td>
                <td align="right" class="cuadricula">#NumberFormat(Total_im_Pagado,'$,_.__')#</td>
                <cfif Total_im_Presupuestado NEQ 0 AND Total_im_Pagado NEQ 0>
                    <cfset Total_pj_Pagado = (Total_im_Pagado / Total_im_Presupuestado) * 100>
                <cfelse>    
                    <cfset Total_pj_Pagado = 0>
                </cfif>
                <td align="right" class="cuadricula">#NumberFormat(Total_pj_Pagado,',_.__')#%</td>
                <cfset im_Saldo = Total_im_Presupuestado - Total_im_Pagado>
                <td align="right" class="cuadricula" style="color:
                <cfif #Total_pj_Pagado# GTE 0 AND #Total_pj_Pagado# LTE 80>
                    ##00BB00 
                <cfelseif #Total_pj_Pagado# GT 80 AND #Total_pj_Pagado# LTE 100>
                    ##FFAA00
                <cfelseif #Total_pj_Pagado# GT 100>
                    ##FF0000
                </cfif>     
                "><b>$#NumberFormat(im_Saldo,',_.__')#</b>           
                </td>
            </tr>
            </cfoutput>
            <cfoutput query="RSIngresosDiversos.rs">
                <tr onMouseOver="this.style.backgroundColor='##99CCCC'" onMouseOut="this.style.backgroundColor=''">
                <td class="cuadricula" align="right" colspan="2"><b>Ingreso Mov Diversos:</b></td>
                <td class="cuadricula" align="right" colspan="5"style="color:##00BB00;"><b>$#NumberFormat(im_movimiento,',___.__')#</b></td>
                </tr>  
            </cfoutput>
            <cfoutput query="RSIngresosObra.rs">
            <tr onMouseOver="this.style.backgroundColor='##99CCCC'" onMouseOut="this.style.backgroundColor=''">
                <td class="cuadricula" align="right" colspan="2"><b>Ingreso de obras:</b></td>
                <td class="cuadricula" align="right" colspan="5"style="color:##00BB00;"><b>$#NumberFormat(im_movimiento,',___.__')#</b></td>
            </tr> 
            <tr onMouseOver="this.style.backgroundColor='##99CCCC'" onMouseOut="this.style.backgroundColor=''">
                <td class="cuadricula" align="right" colspan="2"><b>Total Pagado:</b></td>
                <td class="cuadricula" align="right" colspan="5"><b style="color:##FF0000;">$#NumberFormat(Total_im_Pagado,',___.__')#</b></td>
            </tr>
            <tr>
                <cfset total = RSIngresosObra.rs.im_movimiento - Total_im_Pagado>
                <td class="cuadricula" align="right" colspan="2"><b>Saldo:</b></td>
                <td class="cuadricula" align="right" colspan="5"><b <cfif total LT 0>style="color:##FF0000;"<cfelse>style="color:##00BB00;"</cfif>>$#NumberFormat(total,',___.__')#</b></td>
            </tr>         
            </cfoutput>
        </table><br /><br />
</cfif>
<!--- </cfsavecontent>
<cfoutput>
    #contenido#
</cfoutput>

<cfheader name="Content-Disposition" value="inline; filename=Reporte_saldos_#DateFormat(Now(),'DD_MM_YYYY')#.xls" >
<cfcontent variable="#ToBinary(ToBase64(contenido))#" type="application/msexcel" >
 --->