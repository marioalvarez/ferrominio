<cfset CurrentPage=GetFileFromPath(GetTemplatePath())>
<cfparam name="filtro_id_obra" default="">
<cfparam name="Filtro_id_Agrupador" default="">
<cfparam name="Filtro_id_Empresa" default="">

<cfquery name="RsEmpresas" datasource="#session.cnx#">
    SELECT id_Empresa, nb_Empresa FROM Empresas
</cfquery>

<cfif isDefined("FORM.MM_InsertRecord") AND #FORM.MM_InsertRecord# EQ "form1">
	<cftransaction action="begin">
		

		<cfif #FORM.sn_entradaSalida# EQ 'S'>
			<!---Cargo--->
			<cfif #FORM.Filtro_id_Empresa# NEQ '' AND #FORM.Filtro_id_Obra# NEQ ''>

				<cfquery name="RsEncabezado" datasource="#session.cnx#">
			         	Requisiciones_Guardar  #FORM.filtro_id_obra#,#FORM.Filtro_id_Empresa#, #session.id_usuario# , 'Requisicion por Movimiento Diverso', '','','',103,13,NULL
					</cfquery>
				
				 	<cfinvoke method="Agregar_insumo_requisicion" component="#Application.componentes#.inventariostraspasosdet" returnvariable="Rsdetalles"
					    	id_Requisicion = "#RsEncabezado.id_Requisicion#"
					     	id_Obra = "#FORM.filtro_id_obra#"
					        id_Empresa="#FORM.Filtro_id_Empresa#"
					        id_insumo = "#FORM.Filtro_id_Agrupador#"
					        nu_cantidad="1"
					        Unidad_Medida_Especial="Insumo por Movimiento Diverso"
					        Estatus_det="103"
					>
				
					<cfquery name="actualiza_nuporComprar" datasource="#session.cnx#">
					 	UPDATE RequisicionesDetalle
					 	set nu_porComprar=0
					 	where id_obra=#FORM.filtro_id_obra# and id_Empresa=#FORM.Filtro_id_Empresa# 
					 	AND id_Requisicion=#RsEncabezado.id_Requisicion#
				 	</cfquery>

				 	<cfinvoke method="RSInserta_ordenCompra_diversos" component="#Application.componentes#.bancos" returnvariable="RSOrdenCompra"
				    	id_Empresa = "#FORM.Filtro_id_Empresa#"
				        id_Obra = "#FORM.filtro_id_obra#"
				        id_empleado="#session.id_usuario#"
				        id_Almacen = "1"
				        id_requisicion="#RsEncabezado.id_Requisicion#" 
					>

				 	<cfinvoke method="RSInserta_ordenCompradetalle_diversos" component="#Application.componentes#.bancos" returnvariable="RSOrdenCompradetalle"
				    	id_Empresa = "#FORM.Filtro_id_Empresa#"
				    	id_ordenCompra="#RSOrdenCompra.id_OrdenCompra#"
				        id_Obra = "#FORM.filtro_id_obra#"
				        id_empleado="#session.id_usuario#"
				        id_insumo = "#FORM.Filtro_id_Agrupador#"
				        im_precio="#FORM.im_movimiento#"
				        cantidad="1"
					 >

				 	<cfinvoke method="RS_actualizaTotales_OC" component="#Application.componentes#.bancos" returnvariable="TotalOC_destino"
				    	id_Empresa = "#FORM.Filtro_id_Empresa#"
				    	id_ordenCompra="#RSOrdenCompra.id_OrdenCompra#"
				        id_Obra = "#FORM.filtro_id_obra#"
				        id_empleado="#session.id_usuario#"
				       
		 			>

					 <!--- Agregar Factura--->
					<cfinvoke method="RSMostrarNextIDContraRecibos" component="#Application.componentes#.bancos" returnvariable="RSIDContrarecibo"
					 id_Empresa = "#FORM.Filtro_id_Empresa#"
				    >	
					
					<cfinvoke method="RSAgregarContraRecibos" component="#Application.componentes#.bancos" returnvariable="RSContrarecibo"
				    	id_Empresa = "#FORM.Filtro_id_Empresa#"
				    	id_ContraRecibo="#RSIDContrarecibo.rs.NEXTID#"
				        fh_ContraRecibo = "#dateformat(now(),'yyyy-mm-dd')#"
				        id_Proveedor="0"
				        id_Estatus = "501"
				    >

					<cfinvoke method="RSAgregarContraRecibosDetalle" component="#Application.componentes#.bancos" returnvariable="RSContrareciboDetalle"
				    	id_Empresa = "#FORM.Filtro_id_Empresa#"
				    	id_ContraRecibo="#RSIDContrarecibo.rs.NEXTID#"
				        nd_ContraRecibo = "1"
				        id_Proveedor="0"
				        id_Obra = "#FORM.filtro_id_obra#"
				        id_OrdenCompra="#RSOrdenCompra.id_OrdenCompra#"
				        de_Factura="Factura generada por movimiento Diverso"
				        fh_Factura="#dateformat(now(),'yyyy-mm-dd')#"
				        fh_FacturaVencimiento="NULL"
				        id_Modulo="6"
				        id_TipoMovimiento="NULL"
				        id_CentroCosto="NULL"
				        de_Concepto="NULL"
				        de_Referencia="NULL"
				        im_Factura="#FORM.im_movimiento#"
				        id_Moneda="1"
				        im_TipoCambio="1.00"
				        cl_FormaPago="NULL"
				        id_Estatus="501"
				        id_EmpleadoRegistro="#session.id_usuario#"
				        id_EmpleadoBloqueo="NULL"
				        id_EmpleadoAutorizo="NULL"
				        id_EstatusFactura="524"
					>			
				    <cfinvoke method="RSInsertarAnalisisInsumos" component="#Application.componentes#.bancos" returnvariable="RSContrareciboDetalleAnalisisInsumos"
				    	id_Empresa = "#FORM.Filtro_id_Empresa#"
				    	id_ContraRecibo="#RSIDContrarecibo.rs.NEXTID#"
				        nd_ContraRecibo = "1"
				        id_Obra = "#FORM.filtro_id_obra#"
				        id_CentroCosto="900"
				        id_Frente="901"
				        id_Partida="1"
				    	id_Insumo="#FORM.Filtro_id_Agrupador#"
				    	im_Analisis="#FORM.im_movimiento#"
				    	nu_Cantidad="1"
				    	im_Precio="#FORM.im_movimiento#"
					>

				</cfif>
				<cfinvoke component="#Application.componentes#.bancos" method="BancoCuentaBancaria_Rank" returnvariable="rsCampos"
					id_cuentaBancariaRank="#id_CuentaBancaria#">
				<cfif #rsCampos.tipoError# NEQ "">
					<cf_error_manejador tipoError="#rsCampos.tipoError#" mensajeError="#rsCampos.mensaje#">
				</cfif>	
				<cfif NOT IsDefined('RSIDContrarecibo.rs.NEXTID')>
					<cfset ContraRecibo=''>
				<cfelse>
					<cfset ContraRecibo=RSIDContrarecibo.rs.NEXTID>	
				</cfif> 				
			</cfif>
		<cfinvoke component="#Application.componentes#.bancos" method="movimientosDiversosAgregar" returnvariable="RSAgregar" 
			id_banco="#rsCampos.id_banco#" 
			id_cuentaBancaria= "#rsCampos.id_cuentaBancaria#" 
			de_concepto="#FORM.de_concepto#" 
			sn_entradaSalida="#FORM.sn_entradaSalida#" 
			fh_movimiento="#FORM.fh_movimiento#" 
			im_movimiento="#FORM.im_movimiento#" 
			id_estatus="601"
			id_obra="#FORM.filtro_id_obra#"
			id_Empresa = "#FORM.Filtro_id_Empresa#"
			id_agrupador="#FORM.Filtro_id_Agrupador#"
			id_contraRecibo="#ContraRecibo#"
			>

		<cfif #RSAgregar.tipoError# NEQ "">
			<cf_error_manejador tipoError="#RSAgregar.tipoError#" mensajeError="#RSAgregar.mensaje#">
		</cfif>
		<!--- se inserta el traspaso en la tabla de cuentasBancariasMovimientos  --->
		<cfinvoke component="#Application.componentes#.bancos" method="cuentasBancariasMovimientos_agregar" returnvariable="RSMovimientoAgregar" 
			id_banco="#rsCampos.id_banco#" 
			id_cuentaBancaria="#rsCampos.id_cuentaBancaria#" 
			sn_entradaSalida="#FORM.sn_entradaSalida#"
			id_tipoMovimiento="2"
			im_movimiento= "#FORM.im_movimiento#"
			fh_movimiento= "#FORM.fh_movimiento#"
			id_estatus= "#601#"
			id_usuarioRegistro= "#session.id_usuario#"
			fh_registro= "#dateformat(now(),'yyyy-mm-dd')#"
			id_traspaso="NULL"
			id_movimientoDiverso= "#RSAgregar.id_movimientoDiverso#">
		<cfif #RSMovimientoAgregar.tipoError# NEQ "">
			<cf_error_manejador tipoError="#RSMovimientoAgregar.tipoError#" mensajeError="#RSMovimientoAgregar.mensaje#">
		</cfif>
		
		
		
	</cftransaction>				

    <cf_location_msg url="movimientos_diversos_listado.cfm?sn_Mostrar=S" operacion="guardar">
</cfif>

<!--- obtiene BancoCuentaBancaria para combo --->
<cfinvoke component="#Application.componentes#.bancos" method="RSMostrarTodosBancosCuentasBancarias" returnvariable="RSbancos">
<cfif #RSbancos.tipoError# NEQ "">
	<cf_error_manejador tipoError="#RSbancos.tipoError#" mensajeError="#RSbancos.mensaje#">
</cfif>
<cfquery name="RsAgrupadores" datasource="#session.cnx#">
	SELECT id_Agrupador,de_Agrupador FROM Agrupadores_fe 
</cfquery>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="../css/style.css" type="text/css">
	<title>Untitled Document</title>
	<script language="javascript" src="../js/jquery.js"></script>
	<script language="javascript" src="../js/funciones.js"></script>
	<!---Refs de Calendario --->
	<script type="text/javascript" src="../calendario/calendar.js"></script>
	<script type="text/javascript" src="../calendario/calendar-setup.js"></script>
	<script type="text/javascript" src="../calendario/lang/calendar-es.js"></script>
	<style type="text/css"> 
	@import url("../calendario/calendar-blue2.css");
	</style>
	<!---Termina Refs de Calendario--->
</head>
<body>
<cfinclude template="menu.cfm">&nbsp;
<cfform method="post" id="form1" name="form1" action="#CurrentPage#">
	<table align="center" width="650px" cellpadding="0" cellspacing="0" class="tabla_grid">
		<tr class="tabla_grid_encabezado">
			<td width="3px"><img src="../images/esquina_izquierda.gif"></td>
			<td width="100%" align="center" valign="middle">Captura de movimientos diversos</td>
			<td width="3px"><img src="../images/esquina_derecha.gif"></td>
		</tr>
		<tr>
			<td class="tabla_grid_orilla_izquierda"></td>
			<td>
				<table align="center" width="100%" cellpadding="0" cellspacing="10" border="0">
					<tr>
						 <td align="right" width="10%">Empresa:&nbsp;</td>
                            <td align="left" width="30%">
                                <cfselect name="Filtro_id_Empresa" id="Filtro_id_Empresa" style="width:98%;">
                                    <option value="">SELECCIONE</option>
                                    <cfoutput query="RsEmpresas">
                                        <option value="#id_Empresa#" <cfif #Filtro_id_Empresa# EQ #id_Empresa#>selected="selected"</cfif>>#nb_Empresa#</option>
                                    </cfoutput>
                                </cfselect>
                            </td>
					</tr>
					<tr>
						<td align="right">Obra:</td>
						<td align="left">
							<cfinput  type="text" class="contenido_numerico" name="Filtro_id_Obra" id="Filtro_id_Obra" validate="integer" size="10" value="#Filtro_id_Obra#">
							<cfinput type="text" readonly="true" onFocus="cambiarFocus($(this).before());" bind="cfc:#Application.componentes#.obras.getObraPorID(1,{Filtro_id_Obra@blur})" bindonload="yes" name="de_Obra_lab" class="contenido_readOnly" style="width:300px" message="La Clave de Obra no es válida.">					</td>
						<td align="left"><!---<button type="button" id="btnBuscarObra" onClick="javascript:AbrirPopUpBusqueda('Filtro_id_Obra', 'pop_obras_busqueda.cfm', 'id_Empresa=1', '700px', '600px')" class="boton_popup" value=""><img src="../images/buscar.png" width="24"></button>--->
	                    	<button type="button" id="btnBuscarObra" onClick="busqueda()" class="boton_popup" value="" title="Clic para buscar obra"><img src="../images/buscar.png" width="24"></button>
	                    </td>
                    </tr>
					<tr>
                    	<td align="right">Agrupador:&nbsp;</td>
                        <td align="left">
                        	<cfselect name="Filtro_id_Agrupador" id="Filtro_id_Agrupador" style="width:98%;">
                            	<option value="">SELECCIONE</option>
                                <cfoutput query="RsAgrupadores">
									<option value="#id_Agrupador#" <cfif #id_Agrupador# EQ #Filtro_id_Agrupador#>selected="selected"</cfif>>#de_Agrupador#</option>
								</cfoutput>
                            </cfselect>
                        </td>
					</tr>
					<tr valign="baseline">
						<td nowrap align="right" valign="middle">Concepto:</td>
						<td><cftextarea name="de_concepto" id="de_concepto" rows="3" cols="40" required="yes" message="El concepto del movimiento es requerido." maxlength="255" class="contenido"> </cftextarea></td>
					</tr>	
					<tr valign="baseline" height="4px">
						<td nowrap align="right">Cuenta bancaria:</td>
						<td><cfselect name="id_cuentaBancaria" id="id_cuentaBancaria" query="RSbancos.rs" required="yes" message="La cuenta bancaria es requerida" value="id_cuentaBancariaRank" display="nu_cuentaBancaria" queryposition="below" style="width:300px">
								<option value="">SELECCIONE LA CUENTA</option>
							</cfselect>
							Moneda:<cfinput size="2" class="contenido_readOnly" readOnly="yes"  name="id_moneda" bind="cfc:#Application.componentes#.cuentasbancarias.getTipoMonedaPorIdCuenta({id_cuentaBancaria@blur})" bindonload="yes" >
						</td>
					</tr>
					<tr valign="baseline">
						<td width="30%" align="right" nowrap>Cargo/abono:</td>
						<td width="70%">
							<cfselect name="sn_entradaSalida" id="sn_entradaSalida" required="yes" message="Espefique si es cargo o abano." style="width:230px">
								<option value="">SELECCIONE UNA OPCION</option>
								<option value="E">Abono</option>
								<option value="S">Cargo</option>
							</cfselect>
						</td>
					</tr>
					<tr>	
						<td nowrap align="right">Fecha movimiento:</td>
						<td>
							<cfinput type="text" name="fh_movimiento" id="fh_movimiento" size="10" readonly="true" style="text-align:center" validate="eurodate" required="yes" message="La fecha es requerida" onChange="poner_fecha();">
                			<input type="button" id="lanzador" value="..." / class="boton">
            				<script type="text/javascript">
            					Calendar.setup({
            						inputField     :    "fh_movimiento",      // id del campo de texto
            						ifFormat       :    "%d/%m/%Y",       // formato de la fecha, cuando se escriba en el campo de texto
            						button         :    "lanzador"   // el id del botón que lanzará el calendario
            					});
            					function poner_fecha()
            					{
            						$('input[class=fh_movimiento]').val($('#fh_movimiento').val());
            					}
            				</script>
                		</td>
                	</tr>	
					<tr valign="baseline">
						<td nowrap align="right" valign="middle">Importe:</td>
						<td><cfinput type="text" name="im_movimiento" id="im_movimiento" required="yes" message="El importe es requerido." class="contenido_flotante"></td>
					</tr>                														
					<tr valign="baseline">
						<td colspan="2" align="center">
							<button type="submit" name="boton" id="boton" class="boton_imagen"><img src="../images/guardar.png"></button>
							<button type="button" class="boton_imagen" onClick="location='movimientos_diversos_listado.cfm'"><img src="../images/cancelar.png"></button>
						</td>
					</tr>
				</table>
			</td>
			<td class="tabla_grid_orilla_derecha"></td>
		</tr>
		<tr class="tabla_grid_footer">
			<td width="3px"><img src="../images/abajo_izquierda.gif" /></td>
			<td></td>
			<td width="3px"><img src="../images/abajo_derecha.gif" /></td>
		</tr>
	</table>
	<input type="hidden" name="MM_InsertRecord" value="form1">
</cfform>
</body>
<script type="text/javascript">
	function busqueda(){
		if($('#Filtro_id_Empresa').val() == '')
			alert('Seleccione empresa');
		else
			AbrirPopUpBusquedaChrome('Filtro_id_Obra', 'pop_obras_busqueda.cfm', 'id_Empresa='+$('#Filtro_id_Empresa').val(), '700px', '600px')	
	}
	function valida(){
		if($('#Filtro_id_Empresa').val()!=''){
			if($('#Filtro_id_Obra').val()==''){
				alert('Cuando Ingresa Una Empresa La Obra Es Requerida');
			}else{
				$('#form1').submit();
			}
		}else if($('#Filtro_id_Obra').val()!=''){
			if($('#Filtro_id_Empresa').val()==''){
				alert('Cuando Ingresa Una Obra La Empresa Es Requerida');
			}else{
				$('#form1').submit();
			}

		}else{
			$('#form1').submit();
		}


	}
</script>
</html>